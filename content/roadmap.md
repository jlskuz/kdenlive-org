---
title: Roadmap
hideMeta: true
layout: roadmap
aliases:
- /kdenlive-roadmap
menu:
  main:
    parent: about_menu
    weight: 50
lists:
  - title: Short term
    items:
    - label: Improve OpenTimelinIO integration (C++ backend)
      roadmap_status: wip
      roadmap_version: 25.08
      link: https://invent.kde.org/multimedia/kdenlive/-/merge_requests/561
    - label: Advanced Trimming Tools
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/1069
      roadmap_status: wip
    - label: Markers with a time span
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/614
      gsoc_year: 2025
      roadmap_status: todo
    - label: Keyframes per parameter
      roadmap_status: todo
    - label: Link sequences from multiple project files together
      roadmap_status: todo
  - title: Mid term
    items:
    - label: Optimize audio thumbnails
      roadmap_status: todo
    - label: AI effect integration
      roadmap_status: todo
    - label: Preview thumbnails for transitions
      roadmap_status: todo
    - label: LV2 support
      roadmap_status: todo
    - label: Dope sheet / Rewrite Effects Interface
      roadmap_status: todo
    - label: Audio Routing | Channel Mapping
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/754
      roadmap_status: todo
    - label: Improve GPU support
      roadmap_status: todo
    - label: 10bit/12bit Color support
      roadmap_status: todo
    - label: 'Playback: Slow speed and improve audio with speed change'
      roadmap_status: todo
    - label: Project Templates
      roadmap_status: todo
    - label: Collaborative Editing
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/1243
      roadmap_status: todo
    - label: Multiple resolution projects. (Don’t resize image, image sequences or videos when importing)
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/538
      roadmap_status: todo
  - title: Long term
    items:
    - label: Time remapping improvements
      roadmap_status: todo
    - label: Audio Ducking
      roadmap_status: todo
    - label: VST support
      roadmap_status: todo
    - label: Submix busses (Audio mixer)
      roadmap_status: todo
    - label: External monitor playback (mirrored)
      roadmap_status: todo
    - label: RAW footage support
      roadmap_status: todo
    - label: OpenColorIO integration
      roadmap_status: todo
    - label: OpenFX support
      roadmap_status: todo
    - label: Scripting support (eg. Python API)
      roadmap_status: todo
    - label: Third party integration (Natron/Blender/Ardour/…)
      roadmap_status: todo
    - label: Node-based compositing and effects
      roadmap_status: todo
  - title: Done
    items:
    - label: Nested Timelines
      roadmap_status: done
      roadmap_fundraiser: true
      roadmap_version: 23.04
    - label: Graph editor for easing modes
      roadmap_status: done
      roadmap_version: 24.08
      sok_year: 2024
    - label: Command Line Interface Rendering
      roadmap_status: done
      roadmap_version: 24.02
    - label: Qt6 and KF6 migration
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/1003
      roadmap_status: done
      roadmap_version: 24.02
    - label: Easing modes for keyframes
      roadmap_status: done
      roadmap_fundraiser: true
      roadmap_version: 24.02
    - label: Audio Recording migration to Qt6
      roadmap_status: done
      roadmap_version: 24.05
    - label: Multi-format rendering
      roadmap_status: done
      sok_year: 2024
      roadmap_version: 24.05
    - label: Group and multiple selection management
      link: https://invent.kde.org/multimedia/kdenlive/-/issues/1327
      roadmap_status: done
      roadmap_version: 24.08
    - label: Multiple Subtitle Tracks
      link: https://invent.kde.org/multimedia/kdenlive/-/merge_requests/451
      roadmap_status: done
      roadmap_version: 24.12
    - label: Efficient Effects Workflow
      roadmap_status: done
      roadmap_fundraiser: true
      roadmap_version: 24.12
    - label: Performance Boost
      roadmap_status: done
      roadmap_fundraiser: true
      roadmap_version: 24.12
---
