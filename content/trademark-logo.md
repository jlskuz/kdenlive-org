---
title: Trademark and Logo
hideMeta: true
---

<img width="100%" src="/images/kdenlive-ci-guide.svg">

<a class="button kdenlive-button-dark learn-more" href="/images/kdenlive-ci-guide.svg" target="_blank">Download SVG</a>

KDE® is a registered trademark of KDE e.V.
Kdenlive™, Kde’n’live™ and the playhead logo are trademarks of [KDE e.V.](https://ev.kde.org/)

