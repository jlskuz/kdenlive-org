---
layout: download
type: miscellaneous
title: Downloads
name: Kdenlive
aliases:
- /fr/download-fr
- /download-fr
- /de/download-de
- /download-de
- /it/download-it
- /download-it
- /zh/download-zh
- /download-zh
menu:
  main:
    weight: 2
kdenlive_version: 24.02.0
stable_sections:
- platform: windows
  text: Recommended Windows 10 or newer. Older versions might not work properly.
  sources:
  - name: Installable
    url: https://download.kde.org/stable/kdenlive/24.02/windows/kdenlive-24.02.2.exe
  - name: Standalone
    url: https://download.kde.org/stable/kdenlive/24.02/windows/kdenlive-24.02.2_standalone.exe
- platform: linux
  text: Stable Flatpak version is available in Flathub.
  sources:
  - name: AppImage
    url:
  - name: Flatpak
    url:
- platform: macos
  text: Stable Flatpak version is available in Flathub.
  sources:
  - name: Silicon
    url:
  - name: Intel
    url:
---
