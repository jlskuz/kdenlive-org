---
title: Kdenlive Sprint Recap – November 2023
author: Jean-Baptiste Mardelle
date: 2023-11-16T07:47:42+00:00
aliases:
- /en/2023/11/kdenlive-sprint-recap-november-2023/
- /fr/2023/11/kdenlive-sprint-recap-novembre-2023/
- /it/2023/11/riassunto-del-kdenlive-sprint-del-novembre-2023/
discourse_topic_id: 7387
categories:
  - Event
  - Sprint
---

![](team-in-zh.jpg)

The Kdenlive team met last weekend in Zürich for a sprint. Many topics were discussed, here is a quick overview of what we did.

## Qt6 strategy

We tried to compile the KF6/Qt6 Kdenlive version on all supported platforms. We have solved all upstream issues in our dependencies, but this is still an ongoing task as our code is not built with Qt6 on Mac yet. The Windows and Linux version start but contain some subtle bugs, mostly qml/mouse related.

Several patches were submitted and merged to KDE Frameworks to fix platform-specific issues: [patch 1][1], [patch 2][2], [patch 3][3], [patch 4][4]. We plan to move forward on this and will decide by mid-December whether the 24.02 release will be Qt6 based.

## Effects workflow

We had some in-depth discussions about how effects are displayed to the user, how to manage embedded effects (transform and volume) for timeline clips, as well as some preliminary steps on how to move towards a more "dope sheet like" management of keyframes. The current proposed changes are:

  * switch from a blacklist to a whitelist system, so that whenever a new effect is implemented in a library, it won't automatically appear in the UI, as many effects don't work without some additional layer. This will also allow us to control better which effects are displayed. Of course, the user will be able to disable this to see all available effects, for example, and be able to play with new effects
  * in the effects list, add a link to the documentation website so that you can quickly reach a page documenting each effect
  * start working on mock-ups for the embedded effects.

## Render test suite

The render test suite is a [repository of scripts][5] designed to check for regressions in Kdenlive. Some refinements still have to be made to the report UI, but the last missing steps to make it work were implemented during the sprint.

Below is a sample screenshot of a test result

![](render-suite.png "Kdenlive test suite results page")

## Group behavior

Currently, Kdenlive does not behave very consistently when you have a group of clips selected. For example, depending on how you do it, adding an effect will sometimes be applied to one clip only or all clips in the group. Several other operations have the same issues in regard to groups. Several bugs are opened related to this problem: [bug1][6], [bug2][7], [bug3][8], [bug4][9], …

After our discussion, we decided to make the following changes:

  * when operating on a grouped clip, all clips in the group will be affected
  * we will implement selecting a single item in a group with a modifier (eg. Ctrl+click). When such a selection is made, only the selected clip will be affected.

## Bug tracker/issue tracker

We closed all remaining tasks on the now deprecated [phabricator][10], and hope to organize another online event to parse the GitLab issues to close them all, limiting Gitlab to our internal team communication/discussion.

**Bugs and feature requests should be reported on the KDE Bugtracker instance:** https://bugs.kde.org

### Menu, widgets and misc

We had several complaints that rendering was not easy to find, and decided on the following:

  * rename &#8220;Render&#8221; to &#8220;Export&#8221;
  * move the &#8220;Render&#8230;&#8221; and other import/export actions from the _Project_ menu to the _File_ menu
  * we discussed renaming the &#8220;Project Bin&#8221; widget, either to &#8220;Project&#8221; or to &#8220;Assets&#8221;, but no final decision was made
  * added to our todo list: re-implement: &#8220;Audio Spectrograph&#8221; and &#8220;Loudness Meter&#8221; from MLT
  * discussed which elements we would like to show in a future [Welcome Screen][11]
  * we added a basic check for offline update options, simply comparing the version number with the current date, and suggesting the user upgrade if the version is more than 6 months old.
  * ![](kdenlive-update.png "The new offline update reminder")

## Technical side

We finally carried out the last steps to make the code fully REUSE compliant and added a CI job to prevent regressions.

## Roadmap

We reviewed and updated the [website roadmap][12].

## Fundraising status

After our successful fundraising from last year, we just started to use the funds – the Kdenlive maintainer is now able to spend a few hours more per week thanks to the fund.

Regarding the fundraising tasks:

  * the nested timelines feature is now implemented
  * improved keyframe easing modes will be implemented in the short term – but without Bèzier curves for the moment
  * an improved effects workflow is also on the short term roadmap.

## Public events

We had one public in person event where a couple of people showed up, which was the occasion to demonstrate the basic editing workflow as well as a few advanced features.

After that, an online meeting was the occasion to have some interesting exchanges with our users.

 [1]: https://invent.kde.org/frameworks/prison/-/merge_requests/66
 [2]: https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/223
 [3]: https://invent.kde.org/frameworks/kwallet/-/merge_requests/71
 [4]: https://invent.kde.org/frameworks/kstatusnotifieritem/-/merge_requests/6
 [5]: https://invent.kde.org/multimedia/kdenlive-test-suite
 [6]: https://bugs.kde.org/show_bug.cgi?id=437417
 [7]: https://bugs.kde.org/show_bug.cgi?id=421667
 [8]: https://bugs.kde.org/show_bug.cgi?id=387655
 [9]: https://bugs.kde.org/show_bug.cgi?id=442722
 [10]: https://phabricator.kde.org/project/view/40/
 [11]: https://invent.kde.org/multimedia/kdenlive/-/issues/1787
 [12]: /kdenlive-roadmap/
