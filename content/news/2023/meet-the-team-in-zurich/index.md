---
title: Meet the Kdenlive team in Zürich
author: Jean-Baptiste Mardelle
date: 2023-11-05T17:00:55+00:00
aliases:
- /en/2023/11/meet-the-kdenlive-team-in-zurich/
- /de/2023/11/triff-das-kdenlive-team-in-zuerich/
discourse_topic_id: 6852
categories:
  - Café
  - Event
---

![](kdenlive-zurich.jpg)

The Kdenlive team will hold a Sprint in Zürich, Switzerland, between the 10th and the 12th of November 2023. During these days, we will discuss our roadmap, including the Qt6 strategy, and the refactoring of the effects workflow, as part of last year's fundraising campaign, among other topics.

You will also have the opportunity to join us for a public event, **Saturday the** **11th of November between 4PM and 6PM** (CET time) at [Bitwäscherei][1], Neue Hard 12, CH-8005 Zürich. If you are interested to learn more about Kdenlive, want to meet our team or help the project, save the date and join us!

We will also host an online **Kdenlive Café around 6:30PM** on the same day for those who cannot come to Zürich. Link to the online-room: https://meet.kde.org/b/far-twm-ebr

This event is made possible thanks to KDE e.V. We are looking forwards to meet you there !


 [1]: https://bitwaescherei.ch/
