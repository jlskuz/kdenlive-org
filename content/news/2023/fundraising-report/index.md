---
title: Kdenlive news and fundraising report
author: Jean-Baptiste Mardelle
date: 2023-06-10T13:45:34+00:00
aliases:
- /en/2023/06/kdenlive-news-and-fundraising-report/
- /fr/2023/06/nouvelles-de-kdenlive-et-bilan-de-la-collecte-de-fonds/
- /it/2023/06/novita-e-rapporto-sulla-raccolta-fondi/
- /de/2023/06/kdenlive-news-und-spendenbericht/
discourse_topic_id: 2072
categories:
  - Fundraising
---

![](kdenlive-23.04.2.jpg)

As many users have noticed, our 23.04.0 release didn't go as smoothly as planned. Several major regressions affected the release, resulting in a poor user experience. We are well aware that most users want stability above all else, and with our growing user base we really need to improve on that front.

So for Kdenlive 23.08, we plan to focus only on bug reporting and regression testing to avoid repeating the mistakes of the 23.04 release. Our intention is to improve our test suite, and also to create a repository of sample project files, with automated scripts that open the files, render them, and compare the results to a reference render.

This should allow us to catch regressions and be much safer when releasing a new version with major changes, as was the case with nested timelines in 23.04.0.

It's also important to realize that Kdenlive relies on dozens of other projects and libraries, and that some of the recent problems were caused by upstream or downstream issues, so don't blame us for everything :).

## Regarding our fundraising

Here is the first report on what we are doing with the money and what is planned for the future.

We are very happy with the success of the campaign and will try to report every 4 months on what we have achieved with the money raised. At first, not everything went as planned. The reorganisation of the Kdenlive maintainer's schedule to have more time for Kdenlive is not going as fast as hoped, so he is still in a transition period. This will change by the end of the year, with the goal of having a healthier weekly schedule with two full days dedicated to Kdenlive instead of the current one day plus all available spare time.

## The figures

As of mid-February 2023, we had received 677 donations totalling: €23,170.  
Platform and processing fees were €1,067 €, and €4,420 € (20 %) will go to KDE to cover infrastructure, maintenance and support costs. This leaves €17,683 € for Kdenlive development.
€3000 has already been used to help fund the first achievement (see below).

## The first achievements

Thanks to these donations, the Kdenlive maintainer started to work a bit more on Kdenlive and we delivered the first goal of the fundraiser: support for nested timelines!

## Next steps

When our testing has improved, we will start working on improving the effects workflow and increasing the global performance of the application.

## Join us

Kdenlive is an open source project, you can help us in many ways. You can join our Matrix development channel, help with documentation or send us your (hopefully) success stories with Kdenlive!
