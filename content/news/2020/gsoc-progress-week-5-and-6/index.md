---
title: 'GSoC’ 20 Progress: Week 5 and 6'
author: Sashmita Raghav
date: 2020-07-20T04:30:59+00:00
aliases:
- /en/2020/07/gsoc-20-progress-week-5-and-6/
categories:
  - GSoC
  - Mentorship
---
Greetings!

The Second Phase began with Week 5 of the coding period. The next two weeks involved debugging the previously written code and including necessary connections for it to work. For this, I made use of the [signal – slot mechanism][1] of the splendid framework that is Qt.

For those of you who are unfamiliar with this mechanism: a signal is emitted when a particular event occurs. For example, if a new subtitle line is added, i.e., the rows in the list model have changed, the rowMoved signal will be emitted. Similarly, if the model is reset upon startup, the modelReset signal will be emitted.

I added a setup function that would be executed upon model construction, which would connect all these default signals, related to changes made in distinct parts of the Subtitle model, to emit a single modelChanged signal. If a signal, for an event pertaining to the model, is emitted, it will mean that the model has changed.

Next, the modelChanged signal was connected to respective slots in other classes. A slot is a function that is called in response to a specific signal. These slots will execute a particular function every time the subtitle list model is changed.

Once all the necessary connections were made, the parsed subtitles, including the text and its duration; were easily added to the model upon application startup with no errors. 🙂

## Kdenlive Café – July

Kdenlive Café is a monthly meeting of the developers, users and community members alike to discuss various topics related to the video editor. The future scopes and UI possibilities of editing subtitles in Kdenlive were discussed in this month’s Café. Many of the suggestions that were made were exciting, to say the least. Some of the suggestions include:

  * Create a separate subtitle track to allow easy editing in the timeline
  * Have a settings window to adjust the selected subtitle (text, font, color, position)
  * Have a list view widget displaying time, duration and text of the subtitles, allowing the user to work outside the timeline
  * Have tabs to work with different languages in the list view

In the upcoming weeks, now that the model is being created correctly, my main focus will be to add a separate track for subtitles in the timeline.

That’s it from my side :D. The code can be viewed [here][2].

~ Sashmita Raghav

 [1]: https://doc.qt.io/qt-5/signalsandslots.html
 [2]: https://invent.kde.org/sassycode/kdenlive/-/tree/subparser
