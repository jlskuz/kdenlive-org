---
title: 'GSoC’ 20 Progress: Week 1 and 2'
author: Sashmita Raghav
date: 2020-06-15T19:17:41+00:00
aliases:
- /en/2020/06/gsoc-20-progress-week-1-and-2/
categories:
  - GSoC
  - Mentorship
---
Greetings!

It's been two weeks since the coding period began and I would love to share with the community the progress I have made so far.

In the past two weeks, I focused on implementing a basic class for handling subtitles.

First, I created a class called [`SubtitleModel`][2]. This class would contain the list of subtitle content included in the uploaded subtitle file. Since the SubtitleModel class would be utilized to implement a basic model based upon a list of strings, the `QAbstractListModel` provided an ideal base class on which to build. Subtitle files are usually of two basic formats: [SubRipText][3] file (`.srt`) and [SubStation Alpha][4] (`.ass`) type. Subtitles are maintained in these files in totally different formats based on their file type, so the function ought to parse through each file type in a distinctive way.

In the first week, I worked on composing a parser function for parsing the SRT format file and the SSA or ASS format file provided by the user. As of now, the parser parses through the `.srt` or `.ass` file and extricates the subtitle data like the subtitle text and its starting and ending timings.

```cpp
void parseSubtitle()
```

Next, I worked on composing the addSubtitle() function to add the accumulated data to the list model.

```cpp
void addSubtitle (GenTime start,GenTime end, QString str)
```

I included many more fundamental functions like `getModel()` which returns a shared pointer to the Subtitle Model, and `rolenames()` and `data()` functions which help in returning values of the objects within the list based upon the custom roles.

```cpp
static std::shared_ptr<SubtitleModel> getModel()
```

In the forthcoming weeks, I will work on implementing a  Subtitle QML track in order to display the positions of the subtitles in the timeline.

The code can be viewed [here][1].

That’s all for now, please stay tuned to my progress in upcoming posts.

~ Sashmita Raghav

 [1]: https://invent.kde.org/sassycode/kdenlive/-/commits/subparser
 [2]: https://invent.kde.org/sassycode/kdenlive/-/blob/subparser/src/bin/model/subtitlemodel.hpp
 [3]: https://en.wikipedia.org/wiki/SubRip#File_format
 [4]: https://en.wikipedia.org/wiki/SubStation_Alpha
