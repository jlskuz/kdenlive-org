---
title: 'GSoC ’ 20 Progress: Week 3 and 4'
author: Sashmita Raghav
date: 2020-06-30T19:11:56+00:00
aliases:
- /en/2020/06/gsoc-20-progress-week-3-and-4/
categories:
  - GSoC
  - Mentorship
---
Greetings!

The past two weeks did not see as much progress as I would have liked because of my university exams and evaluations. Now, let’s focus on the work that I could do before I got swamped with the academic work and term exams.

I started the third week by working on drafting a basic QML display of the subtitle model items, like the position of the subtitles in the timeline.  I drafted a basic delegate QML model to display the start positions of each subtitle line.  Then I began working on integrating the back-end part (which I had mentioned in the [previous post][1]) with the basic front-end part (displaying the position of the subtitles).

In this process of integrating the subtitle model with the QML delegate model, I encountered a few logical errors with my code and some connections with the Subtitle Model which I had completely overlooked. It was also during this time that I realised I had missed out some key functions while writing the subtitle model class.

I defined a class, [SubtitledTime,][2] that will handle all things subtitle in the application, for example, comparing two different subtitles, getting the string or even the timings of each subtitle string, and assigning values to subtitles.

```cpp
class SubtitledTime
```

Next, I added functions to add the start timing of each subtitle statement in the existing [SnapModel][3]. This will enable the [snap feature][4] of Kdenlive for the subtitles, making it convenient for the users while editing subtitles in the timeline.

```cpp
void registerSnap (const std::weak_ptr<SnapInterface> &snapModel);

void addSnapPoint (GenTime startpos);
```

Finally, I added the basic functions of getting the list of subtitles and returning the total number of rows in the [SubtitleModel][5].

```cpp
int rowCount (const QModelIndex &parent = QModelIndex()) const override;

QList<SubtitledTime> getAllSubtitles() const;
```

The code can be found [here][6].

With this, phase one of the coding period has come to an end and the phase evaluation has begun. Now that my exams are finally over, I hope to make up for the lost time in the upcoming weeks.

~ Sashmita Raghav

 [1]: /news/2020/gsoc-progress-week-1-and-2/
 [2]: https://invent.kde.org/sassycode/kdenlive/-/commit/daa83782aa35c0561962800ce20d2233d7616652
 [3]: https://invent.kde.org/multimedia/kdenlive/-/blob/master/src/timeline2/model/snapmodel.cpp
 [4]: https://userbase.kde.org/Kdenlive/Manual/Timeline/Editing#Snap
 [5]: https://invent.kde.org/sassycode/kdenlive/-/blob/subparser/src/bin/model/subtitlemodel.cpp
 [6]: https://invent.kde.org/sassycode/kdenlive/-/tree/subparser
