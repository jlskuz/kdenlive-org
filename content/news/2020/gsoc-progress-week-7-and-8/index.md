---
title: 'GSoC’ 20 Progress: Week 7 and 8'
author: Sashmita Raghav
date: 2020-07-28T14:37:34+00:00
aliases:
- /en/2020/07/gsoc20-progress-week-7-and-8/
categories:
  - GSoC
  - Mentorship
---
Greetings!

With the subtitle information, i.e., the text, the start and end points of each subtitle, being correctly stored in the abstract list model, I shifted my focus in these two weeks towards the UI development of subtitles in the timeline.

First, to confirm whether the information is stored properly, I worked on displaying vertical lines across the timeline according to the start positions of each subtitle.

![](startLines.png) 

Once the lines were displayed on the timeline, confirming that all the connections I had made in the past weeks were working correctly; I modified the QML code to display each subtitle as a rectangle of width equal to its duration in the timeline and containing its respective subtitle text.

![](blueTextQML.png)

These subtitle clips are however fixed. The user can’t move them, change their width, and thereby change the duration of each subtitle, or even edit the text in each rectangle. As seen in the figure, the rectangles/clips are displayed over the video and audio tracks; a separate track to edit the subtitles also has to be created.

I spent a few days trying to understand how the application handles changes to model items and how various classes are connected to the timeline.

I began by working on adding functions to edit the model items, namely the text and the end positions of the existing subtitles.

This past week, I wrote a function in Subtitle model’s class to handle the modifications in the end positions of each subtitle. This function will update the original end position of the subtitle with the new position.

```cpp
void editEndPos (GenTime startPos, GenTime oldEndPos, GenTime newEndPos);
```

The code can be viewed [here][1].

With this, phase two of the coding period has come to an end and the second phase evaluations have begun. In the upcoming weeks, I will work on adding functions to enable editing the subtitle text which is displayed on the subtitle clips as well as the start position of a particular subtitle text.  Once these basic editing functionalities are added, I will begin to work on creating a separate track to hold the subtitle clips.

~ Sashmita Raghav

 [1]: https://invent.kde.org/sassycode/kdenlive/-/tree/subparser
