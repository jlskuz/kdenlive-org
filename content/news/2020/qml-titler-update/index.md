---
title: Kdenlive Titler Tool Rewrite – An Update
author: Akhil K Gangadharan
date: 2020-05-25T05:30:25+00:00
aliases:
- /en/2020/05/kdenlive-titler-tool-rewrite-an-update/
---
Kdenlive’s Titler Tool rewrite began with GSoC 2019 and now I am happy to announce that we have an MLT producer which can play .qml files with animations! The producer is being now integrated in Kdenlive.

These are the [QML Animation types][1] that are supported and tested so far:

  * `PropertyAnimation`
      * `NumberAnimation`
      * `ColorAnimation`
      * `RotationAnimation`
  * `PathAnimation`
  * `PauseAnimation`

Along with grouping (`ParallelAnimation` and `SequentialAnimation`)

Meaning _theoretically_ you can now create animated qml clips that can be added to your video!

## Running some examples using the producer

(All examples are run at 25 fps, so you may see a slight jerkiness in some animations)

  * **RotationAnimation with easing curves** ![](rotation.gif)
  * **PropertyAnimation (with easing curves)** ![](property.gif)
  * **ColorAnimation** ![](color.gif)
  * **Fading out an SVG image (NumberAnimation on opacity)** ![](svgdaeout.gif)
  * **PathInterpolator** [(source)][2] ![](rocketmaaann.gif)

Carl Schwan is also helping us make a UI for the titler: <https://invent.kde.org/carlschwan/libvectorgraphicsquick> – right now, this allows some basic editing capabilities for a .qml file. 🙂

![](libvectorgraphicsquick.gif)

## Status

We are now neck-deep into testing and integrating the producer with Kdenlive. The animations seem to be playing fine in the Clip Monitor, but work still remains with adding it to the Timeline and rendering it. Also, there are some known issues when playing certain animations, for instance, the last frame of the PathAnimation isn't rendered.

If you want to test out the QML MLT producer, you could do it with melt, MLT’s command-line player.  You can find the source code for the qml producer here: <https://github.com/akhilam512/mlt/tree/qmlproducer>

Note you will need to use CMake to build mlt in order to have the qml producer built. Once that is done, run:

```
melt /path/to/file.qml
```

The rendering logic can be found here: <https://invent.kde.org/akhilkgangadharan/QmlRenderer/-/tree/multithreaded-mlt>

Some major changes were made to the rendering logic. We now use a separate render thread for rendering. This took a bit of time but it is essential as rendering on the main thread was leading to a crash when a qml clip was dragged to the Timeline in Kdenlive due to conflicts between QOpenGlContext makeCurrent() calls of the Timeline and the Renderer. See this [commit][3] for details.

 [1]: https://doc.qt.io/qt-5/qml-qtquick-animation.html
 [2]: https://github.com/KDAB/kdabtv/blob/master/qml-intro/ex-pathinterpolator-animation/ex-pathinterpolator-animation.qml
 [3]: https://invent.kde.org/akhilkgangadharan/QmlRenderer/-/commit/b346a79b569ee066ccab02966505ae4f81d9e189
