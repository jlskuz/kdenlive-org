---
title: 'GSoC’20 Progress: Onward with the Third Month'
author: Sashmita Raghav
date: 2020-08-22T05:18:21+00:00
aliases:
- /en/2020/08/gsoc20-progress-onward-with-the-third-month/
categories:
  - GSoC
  - Mentorship
---
Greetings!

It’s been a while since my last update. In this post, I will describe the work I have done up until now in Phase Three of the coding period.

This phase, I worked on making the subtitles displayed on the timeline editable.

Since the text and end positions are both values for the Subtitle model item of a subtitle at a particular start position, I wrote a function that deals with customizing the text as well as the end positions.

```cpp
void editSubtitle (GenTime startPos, QString newSubtitleText, GenTime endPos);
```

For editing the subtitle text displayed on the subtitle clips in the timeline, I have made use of the [TextEdit][1] item of QML.

![](TEXT_EDITING.gif)

I made use of [MouseArea][2] item of QML to enable resizing of the subtitle clips from the timeline by dragging the clip ends.

![](end_movement_final2.gif)

Next, adjusting the start position of a subtitle clip involved removing the Subtitle model item from the previous position and adding the model item values like the text and end time position associated with the previous start position to the new start position.

For this, I wrote two functions:

  * A function to delete the model item, i.e., a subtitle at a particular position in the timeline.
    ```cpp
    void removeSubtitle (GenTime pos);
    ```
  * A function to enable the movement of subtitles within the timeline. This function first deletes the subtitle at the old start position of the subtitle (_oldPos_) through the _removeSubtitle()_ function and then adds a new subtitle to the model list at the new start position (_newPos_).
    ```cpp
    void moveSubtitle (GenTime oldPos, GenTime newPos);
    ```

My work can be viewed [here][3].

With these basic editing functionalities of the subtitle clips working, the next step will be to create a separate track for containing these subtitle clips. Also, at present, the Subtitle model takes in a default subtitle file. So, functions will have to be added to accept the subtitle file from the user, modify it according to the user's preferences and apply the Subtitle effect on this modified file.

Until the next update :D,

~Sashmita Raghav

 [1]: https://doc.qt.io/qt-5/qml-qtquick-textedit.html
 [2]: https://doc.qt.io/qt-5/qml-qtquick-mousearea.html
 [3]: https://invent.kde.org/sassycode/kdenlive/-/commits/subparser
