---
title: The Titler Revamp – The QML MLT Producer is testing ready
author: Akhil K Gangadharan
date: 2020-01-20T15:35:33+00:00
aliases:
- /en/2020/01/the-titler-revamp-the-qml-mlt-producer-is-testing-ready/
- /it/2020/01/nuova-titolatrice-produttore-qml-mlt-pronto-per-i-test/
- /de/2020/01/neuer-titler-der-qml-mlt-producer-ist-testbereit/
---

The last time I blogged about the Titler, I promised that the next update would be when we have some sort of a backend ready – and I'm happy to announce now that now we have some sort of a backend ready!

## The QML MLT Producer

### Testing

Find my MLT fork [here][1]

Let us try to run it! Starting with something simple – take a sample QML file: `test.qml`

With the content:

```qml
import QtQuick 2.0

Item {
  Rectangle {
    id: rectangle
    x:0
    y: 0
    width: 155
    height: 160
    color: "#5cd037"
  }
}
```

After you have done building, run: `melt qml:test.qml` , we get:

![](blogpost2.png)

and voila!

Which we can confirm (from running `qmlscene` on command line or from the Qt Designer Studio) which looks similar

![](blogpost1.png)

And we can have images with fancier text and images, like this one I recently tried creating

![](Screenshot_20200116_185030.png) 

Although it's not at animations yet, this works well enough for our initial testing.

### A brief explanation

`qml_wrapper` files handle the generation of the rendered images.
In order to render images, this file makes use of QmlRenderer which has endpoints to render a QML file into a given QImage.

`producer_qml.cpp` is the main producer file.

When `melt qml:test.qml` is run, the registered producer (in this case, `qml`) is queried and initialised, and then a QApplication window with the rendered frames is launched and played ( which is what we see )

I refactored a lot of code in the _QmlRenderer_ library comparing it with the code in the last blog post, for the convenience of the producer. For starters, only one method needs to be called for rendering now – `render(&image)`. The constructor takes cares of the input file and other params (height, width, format) are fed in by the `&image` param.

## What's Next?

Test this producer in Kdenlive and see if we can play `qml` files in Kdenlive. So far, there are some issues with mishandling multiple OpenGL contexts and threading, which we are looking into right now. And if that goes well, we can then start worrying about the interface. We will let you know how it goes!

**Will we have the new Titler for 20.04?**

We don't know yet. If the producer integration goes without major hiccups, 20.04 might or might not be a feasible deadline but we hope for the best.

 [1]: https://github.com/akhilam512/mlt/tree/qml
