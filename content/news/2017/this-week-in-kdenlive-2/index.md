---
title: This week in Kdenlive
author: Jean-Baptiste Mardelle
date: 2017-10-06T15:39:39+00:00
aliases:
- /en/2017/10/this-week-in-kdenlive/
categories:
  - Café
  - Release
---

![](alpha3b.png)

The refactoring branch of Kdenlive is progressing nicely and we hope to merge our code to master in the last days of october to meet the KDE Applications 17.12 release schedule. Today we updated the AppImages of both stable and refactoring branch.

## Fixes for stable include:

  * Rotate from image center in Transform effect (requires latest MLT git included in AppImage)
  * Workaround [KAutoSaveFile][1] bug (previously autosave was disabled if your project name had a space in it).
  * Fix keyframes unusable on bin clip effects

## Fixes for refactoring include:

  * Implement copy/paste of clips in timeline
  * Unselect clips when clicking in empty timeline space
  * Title clips: use the first words as clip title instead of "Title clip"
  * Various crash bugfixes

## Both AppImages also benefit these fixes:

  * Fix [no audio output issue][2]
  * Fix [performance issue in KToolBar][3] causing high CPU usage

Download links:
  * Stable 17.08.1b AppImage: (not available anymore)
  * Development version AppImage: (not available anymore)

Our next Kdenlive café will happen on the 30th of october 2017 at 9pm (CET), so see you there!

We also plan to make a few changes to our website in the next weeks, so stay tuned!


 [1]: https://bugs.kde.org/show_bug.cgi?id=385389
 [2]: https://bugs.kde.org/show_bug.cgi?id=385290
 [3]: https://bugs.kde.org/show_bug.cgi?id=365050
