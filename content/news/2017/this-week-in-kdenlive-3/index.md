---
title: Past days in Kdenlive
author: Jean-Baptiste Mardelle
date: 2017-10-23T19:38:04+00:00
aliases:
- /en/2017/10/past-days-in-kdenlive/
categories:
  - Café
  - Release
---
![](kdenlive-1712-alpha5.png)

Since last update, several missing features & stability update made it to the development branch, you can follow the progress on our [17.12 blocking issues][1]. Main changes include:

  * Keyframes are back in timeline, for example in the volume effect
  * Geometry keyframes available again, for example in transform effect
  * Track effects are reimplemented
  * Initial reimplementation of split audio
  * Fix copy of grouped elements
  * Various crash/bug fixes in keyframes, and timeline context menu

An updated development AppImage is available at (not for use in production): (not available anymore)

We also updated the laste stable AppImage to 17.08.2, you can download it here: (not available anymore)

Our next café will be on monday, 30th of October at 9pm (CET), feel free to join us and share about Kdenlive!

 [1]: https://phabricator.kde.org/T7128
