---
title: 'Kdenlive Café  #13 and #14 dates'
author: Massimo Stella
date: 2017-01-11T16:43:58+00:00
aliases:
- /en/2017/01/kdenlive-cafe-13-and-14-dates/
categories:
  - Café
---
Are you ready to participate in first person to the Kdenlive project?  
Don't loose the next 2 Kdenlive Cafés, our monthly 2 hours informal chat meeting, on:

Café #13: January the 24th at 21:00 CET  
Café #14: February the 22nd at 21:00 CET

The reference timezone is always the Central European Time zone CET, UTC+1.
