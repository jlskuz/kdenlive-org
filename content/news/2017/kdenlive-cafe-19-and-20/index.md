---
title: 'Kdenlive Café #19 and # 20'
author: Massimo Stella
date: 2017-07-11T10:28:08+00:00
aliases:
- /en/2017/07/kdenlive-cafe-19-and-20/
categories:
  - Café
---
Do you want to stay tuned on the timeline refactoring development?

Do you want to suggest some improvement to Kdenlive the Free Video Editor?

Are you just curious about how you can contribute to this amazing project?

Participate to our next Kdenlive café events.

The Kdenlive community will be online on the Freenode IRC channel #kdenlive on Monday July 17 and on Monday August 21 at 9 pm UTC+2 (CEST)

Just bring your mug and be part of the team.
