---
title: Last week in Kdenlive
author: Jean-Baptiste Mardelle
date: 2017-09-28T20:33:37+00:00
aliases:
- /en/2017/09/last-week-in-kdenlive/
categories:
  - Café
  - Release
---
![](alpha2.png)

Since last week's [17.12 alpha release][1], we have been steadily progressing on the road to stability, and can now announce the second alpha AppImage including the following changes:

  * Now uses Qt 5.9.1 instead of 5.7.0
  * Fixes wrong icon coloring in UI
  * Patched KDE Frameworks to fix a performance issue
  * Fix corruption/crash on project opening
  * Reimplement check for clips on removable drive
  * Reintroduce advanced editing features: lift/extract/insert/overwrite

The updated AppImage can be downloaded here: (not available anymore)

This release is only made available for interested testers and should not be used for production. Please report problems in the categories placed on the [phabricator page][2] regarding the AppImage / timeline refactoring branch, we will switch back to the normal bugtracker when the beta release will be ready.

See you in our next Kdenlive café on monday, 2nd of October at 9pm CEST

 [1]: /2017/09/randa-news-release-update/
 [2]: https://phabricator.kde.org/T7077
