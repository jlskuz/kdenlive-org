---
title: Kdenlive – refactoring preview and news
author: Jean-Baptiste Mardelle
date: 2017-06-20T07:06:42+00:00
aliases:
- /en/2017/06/kdenlive-refactoring-preview-and-news/
categories:
  - Akademy
  - Café
---

![](kdenlive-refactoring-1.png)

We are very happy to announce the first AppImage of the next generation Kdenlive. We have been working since the first days of 2017 to cleanup and improve the architecture of Kdenlive's code to make it more robust and clean. This also marked a move to QML for the display of the timeline.

This first AppImage is only provided for testing purposes. It crashes a lot because many features have yet to be ported to the new code, but you can already get a glimpse of the new timeline, move clips and compositions, group items and add some effects. This first Appimage can be downloaded from the KDE servers. Just download the Appimage, make the file executable and run it. This version is not appropriate for production use and due to file format changes, will not properly open previous Kdenlive project files. We are hoping to provide reliable nightly build AppImages so that our users can follow the development and provide feedback before the final release.

Today is also our 18th [Kdenlive Café][2], so you can meet us tonight -20th of june – at 9pm (CEST) in the #kdenlive channel to discuss the evolution and issues around Kdenlive.

I will also be presenting the progress of this Kdenlive version this summer (22nd of July) at [Akademy][3] in Almeria – Spain – so feel free to come visit the KDE Community in this great event.

[![](Akademy2017_banner.png)][3]

 [2]: /2017/05/meet-us-at-kdenlive-cafe-17-and-18/
 [3]: https://akademy.kde.org/2017
