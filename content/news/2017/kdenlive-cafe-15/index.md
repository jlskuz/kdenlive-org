---
title: 'Kdenlive café #15 tonight'
author: Jean-Baptiste Mardelle
type: post
date: 2017-03-21T12:46:15+00:00
aliases:
- /en/2017/03/kdenlive-cafe-15-tonight/
categories:
  - Café
---

Kdenlive development might look a bit slow these last months, but we are very busy behind the scene. You can join us tonight on our monthly café to get an insight of the current developments, follow the discussions or ask your questions.

Café will be at 21pm, european time, on irc.freenode.net, channel #kdenlive

More news on the next releases will follow soon, so keep tuned.
