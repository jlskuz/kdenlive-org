---
title: Akademy 2017 talk
author: Farid Abdelnour
date: 2017-11-21T03:51:12+00:00
aliases:
- /en/2017/11/akademy-2017-talk/
categories:
  - Event
  - Akademy
---

The talk by Jean-Baptiste Mardelle's at Akademy 2017 is released along with many [other interesting talks][1].

Akademy is the annual world summit of [KDE][2], one of the largest Free Software communities in the world. It is a free, non-commercial event organized by the KDE Community.

You can find the slides [here][3].

{{< youtube h_8YDceqE7c >}}

 [1]: https://www.youtube.com/watch?v=di1z_mahvf0&list=PLsHpGlwPdtMojYjH8sHRKSvyskPA4xtk6
 [2]: http://www.kde.org/
 [3]: kdenlive-almeria-17.pdf
