---
title: Randa news, release update
author: Jean-Baptiste Mardelle
date: 2017-09-22T09:46:16+00:00
aliases:
- /en/2017/09/randa-news-release-update/
- /news/releases/17.11.70/
categories:
  - Café
  - Randa
  - Event
---
![](randa-kdenlive.png)

A lot happened recently regarding Kdenlive. Let's start with a some news from the swiss mountains:

### Randa!

Last week, from wednesday to saturday I attended KDE's annual Randa sprint organized by wonderful people. This was an occasion to work fulltime on Kdenlive. Here is a resume of what happened during these days:

  * I submitted Dušan Hanuš's color correction improvements (allow negative lift values in Lift/Gamm/Gain effect) in Movit and MLT. They have now been merged so we should soon have better color wheel correction.
  * I made the first tests with Rafal Lalik's [TypeWriter effect][1], that would bring back this much requested feature for our users.
  * I merged my code for speed effect and committed a very preliminary qml builtin effect stack (that should in the future allow easy adjustment of basic video properties like contrast, color balance, etc).
  * I fixed a problem preventing multithreaded rendering that we discovered with Manuel when comparing Shotcut and Kdenlive.
  * I spend several hours trying to debug a performance issue and finally discovered that it was caused by a small qml animation (blinking rectangle) – a reminder that fancy effects can sometimes really get in the way of productivity!
  * I briefly exchanged with Scarlett regarding AppImage and Snap packages
  * And several small other fixes: display warning when adding images smaller than 8pixels (not fully supported by MLT), fixed import of image sequences, removed unnecessary warning on first start about missing DVD tools (stuff from another century as I was told :). 

You can still donate to KDE's [Randa fundraising][2] to help. Kdenlive was also [recently mentioned as the preferred video editor][3] in a large survey of Ubuntu Desktop users, which makes us very proud!

### First alpha testing version available

To share the result of these last months of work, I am happy to announce the first alpha version for Kdenlive 17.12, that contains the major code refactoring as well as the transition to Qml for the timeline display. This is an AppImage testing release. Just download the file, make it executable and run it. The x86_64 linux AppImage is available from the KDE servers: (not available anymore)
**Beware:** this is an experimental alpha version, allowing you to have a look at Kdenlive's future and should not be used for production. Projects created with this version cannot be opened by previous Kdenlive releases, and many issues still have to be solved before the official december release.

### Windows version updated

Vincent Pinon worked on updating the Windows version to the last stable version (17.08.2) that can be downloaded here: <https://download.kde.org/Attic/kdenlive/17.08/windows/Kdenlive-17.08.2-2-w64.7z>

We hope to improve our packaging soon with the help of some great people in the KDE community, more news will follow on that.

### Join us!

We are always welcoming people interested to contribute and exchange, so if you want to learn more and meet us, you are welcome to participate to our next [Kdenlive café][4] on IRC, channel #kdenlive, Monday, 2nd of October at 21pm (UTC+2) (CEST).

 [1]: https://www.youtube.com/watch?v=I8oyiy3YzBI
 [2]: https://www.kde.org/fundraisers/randameetings2017/
 [3]: http://blog.dustinkirkland.com/2017/09/results-of-ubuntu-desktop-applications.html
 [4]: /categories/café/
