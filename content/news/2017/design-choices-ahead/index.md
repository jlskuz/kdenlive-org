---
title: Design choices ahead
author: Farid Abdelnour
date: 2017-08-30T01:45:10+00:00
aliases:
- /en/2017/08/design-choices-ahead/
---

As many of you may know by now, we are currently doing a code refactoring which will be taking a step forward in making our software more suitable for professional use. In the process, we are facing some critical design choices, and want to hear the opinion of the editors of the community.

Currently, a clip inserted in the timeline in Kdenlive can be one of three things: video only, audio only or both audio and video. While this approach gives flexibility to the user, it is quite non-standard amongst video editing software, and may cause troubles if we try to implement some more advanced features like an audio mixer. The alternative, implemented in other softwares, is to avoid hybrid clips altogether, and only allow video only and audio only clips. Of course, in such a situation, inserting a clip from the bin to the timeline would actually create two clips on the timeline: one for the audio and one for the video.

We will be asking at forums, websites, subreddits, etc… for people to voice their opinion for one of the approaches, or outline advantages/drawbacks that we may not have thought of. We are looking forward to hearing from you!
