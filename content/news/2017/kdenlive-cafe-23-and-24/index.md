---
title: 'Kdenlive Café #23 and # 24'
author: Massimo Stella
date: 2017-11-17T15:29:56+00:00
aliases:
- /en/2017/11/kdenlive-cafe-23-and-24/
categories:
  - Café

---
* Which is the status of the Kdenlive's development and of the Timeline refactoring?
* Which are the new features do we plan to add?
* Which is the best way to install the free video editing software on my OS?

These and other topics will be discussed during the 2017 last 2 Kdenlive café.

Participate and bring your contribution.

If you've never been part of this open event, we remind you that the first 20 minutes of our meetings are dedicated to an open discussion about any topic related to Kdenlive.

If you are a user, an enthusiastic video producer, a developer or just a curious person, jump in and be part of our discussion group.

The Kdenlive community will be online on the Freenode IRC channel #kdenlive on Tuesday, November 21 and on Friday, December 15 at 9 pm UTC+1 (CET)
