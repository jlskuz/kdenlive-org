---
title: Mini Bug Squashing Day
author: Farid Abdelnour
date: 2017-11-24T10:48:02+00:00
aliases:
- /en/2017/11/mini-bug-squashing-day/
categories:
  - Event
---

![](bug-squash.png)

In preparation for the 17.12 release we will be holding a mini bug squashing day on the 1st of December, between 10:00 and 15:30 (CET time). Community members are invited to [submit their bug suggestions][1]. For developers interested in contributing to the project we have a set up a [list of low hanging bugs][2] for them to cherry pick and get acquainted with the code base. Note that this is a great opportunity for prospective participants in the [Season of KDE.][3]

During that day, we will be available on IRC in the #kdenlive channel. For those who didn't follow the recent cafés, Kdenlive's big refactoring release has been postponed to 18.04, because we haven't yet reached the required stability to satisfy our growing userbase. This means that Kdenlive 17.12 will be the last release for the old codebase. This bug squash event is an occasion to fix some small issues and improve Kdenlive's experience for the next 4 months.

Feel free to join us in that effort!

 [1]: https://phabricator.kde.org/T7481
 [2]: https://bugs.kde.org/buglist.cgi?quicksearch=product%3Akdenlive%20flag%3Alow_hanging&list_id=1477532
 [3]: https://dot.kde.org/2017/11/19/announcing-season-kde-2018
