---
title: 'Meet us at Kdenlive café #17 and #18'
author: Massimo Stella
date: 2017-05-09T18:34:44+00:00
aliases:
- /en/2017/05/meet-us-at-kdenlive-cafe-17-and-18/
categories:
  - Café
---
Are you ready for the next 2 Kdenive cafés?

During these new 2 hours meetings, we'll check the status of the timeline refactoring and we'll discuss the future of the Libre Video Editor Program.

Bring your contribution and partecipate!

We'll meet on May the 31st and on June the 20th at 9pm CEST (UTC +2) on irc.freenode.org #kdenlive.

See you there soon!
