---
title: Kdenlive development and upcoming events
author: Jean-Baptiste Mardelle
type: post
date: 2017-09-06T17:27:07+00:00
aliases:
- /en/2017/09/kdenlive-development-and-upcoming-events/
categories:
  - Event
  - Randa
---

![Kdenlive 17.12 alpha for testing will soon be released](kdenlive-1712-alpha.png)

Kdenlive's large cleanup and code refactoring will reach a major milestone with the release of Kdenlive 17.12 in december. The development team has been working hard all year to prepare for this release, and we will merge the code to the master branch in october. As part of the development process, we want to make regular alpha/beta releases to allow interested users to test the development version.

## Randa meeting

Next week, the [Randa meetings][1] will take place in Switzerland, and part of my job there will be to prepare the first AppImage version to test the new Kdenlive. I will also exchange with other KDE developers to see what we can do to have Kdenlive's development version available as daily built Snap and Flatpak packages. Other topics for this week include a UI review. If you are interested, please consider a [donation to the Randa meetings][2] that help us make Kdenlive better every year.

## What's next

When the alpha testing packages will be available, we will concentrate on making Kdenlive more stable than ever. But a few features have already been added thanks to the updated architecture, like resizable tracks, sub-groups, and more… we will add more professional features in the mid-long term. We are always interested in feedback, and recently started [a poll][3] regarding parts of the timeline behavior.

## Join Us

We always need help to make Kdenlive better, so if you want to participate, don't hesitate to contact us on the [mailing list][4] or stay tuned for our [next café][5].

 [1]: https://dot.kde.org/2017/09/02/randa-roundup-part-i
 [2]: https://www.kde.org/fundraisers/randameetings2017/
 [3]: /2017/08/design-choices-ahead/
 [4]: https://mail.kde.org/mailman/listinfo/kdenlive
 [5]: /category/cafe/
