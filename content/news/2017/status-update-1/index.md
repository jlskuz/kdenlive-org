---
title: Kdenlive status update
author: Jean-Baptiste Mardelle
date: 2017-03-31T11:01:54+00:00
aliases:
- /en/2017/03/kdenlive-status-update/
categories:
  - Inside
  - Café
---

{{< video src=timeline2.mp4 >}}

Ever since the port to QT5/KF5 in 2015, Kdenlive has seen an increasing momentum to developing its full potential in being a stable and reliable video editing tool which the FLOSS community can use to create content and democratize communication. In 2016 the project saw a redesign of its visual identity (logo, website), the reintroduction of some much requested tools like rotoscoping and a Windows port. During these couple of years we've seen a boom in the size of the community.

We've had some highs and lows during this process and are now ready to go a step further and bring professional-grade features to the FLOSS video editing world. To make this happen faster we would love to see new contributors jumping in. These are some parts that you can contribute to:

## Refactoring

Since the beginning of the year, we have been working on a big refactoring/rewrite of some of the core parts of Kdenlive. Being more than 10 years old, some parts of our code had become messy and impossible to maintain. Not to mention the difficulty in adding new features.

Part of the process involves improving the architecture of the code, adding some tests, and switching the timeline code from QGraphicsView to the more recent QML framework. This should hopefuly improve stability, allow further developments and also more flexibility in the display and user interaction of the timeline.

You can see a preview of some of the new QML timeline features in the above video.  
We plan to release the refactoring branch for the Kdenlive 17.08 release cycle.

## Packaging

  * AppImage – Thanks to the help of Scarlett Clark (KDE) a lot of progress has been made on an automated daily build system which will allow easy testing of the latest feature.
  * Flatpak – We are looking for help to achieve similar results as AppImage.
  * Snaps – A first version was made available a few months ago. Currently on hold, help welcome.
  * Windows – We are looking for help to achieve similar results as AppImage.
  * OSX – There is an initiative by the Macports team to port Kdenlive and package it but progress has been slow.

## Windows

Our initial Alpha release of Windows has been a success. Some people have switched to editing fulltime in Kdenlive and reviewers [have praised it][1]. We need developers to help find and fix some Windows specific bugs and bring Kdenlive on par with its GNU/Linux counterpart. One example is currently a bug that prevents rendering Jpeg images.

## Next release

Due to the refactoring efforts, the 17.04 release cycle, which is right down the corner, will include code cleanup and some welcome bugfixes but no major changes. More details about this release will follow soon.

Our next monthly café will be held on Tuesday, the 11th of april 2017, at 21pm (CET) on irc.freenode.net, channel #kdenlive, everyone is welcome to join and this is a great opportunity to get in touch with the dev team, which can otherwise be contacted through a good old [mailing list][2].

On a side note, the Frei0r project, which powers many Kdenlive effects just released version 1.6.0 which brings some new filters as well as crash fixes, so all packagers are encouraged to upgrade.

All in all 2017 promises to be an exciting year for Kdenlive, join us!

 [1]: http://www.softpedia.com/get/Multimedia/Video/Video-Editors/Kdenlive.shtml
 [2]: https://mail.kde.org/mailman/listinfo/kdenlive
