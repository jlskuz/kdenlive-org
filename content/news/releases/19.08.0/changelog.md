<ul id="ulkdenlive">
  <li>
    Do not setToolTip() for the same tooltip twice. <a href="http://commits.kde.org/kdenlive/ea9d42fa9632871524b7757b6f21e1d90f733d0f">Commit.</a>
  </li>
  <li>
    Use translations for asset names in the Undo History. <a href="http://commits.kde.org/kdenlive/7c9294c6e3479311fb98faae4839d206251257dd">Commit.</a>
  </li>
  <li>
    Fix dropping clip in insert/overwrite mode. <a href="http://commits.kde.org/kdenlive/299b02a0bc676018399032d13cc6fd0c67a21565">Commit.</a>
  </li>
  <li>
    Fix timeline drag in overwrite/edit mode. <a href="http://commits.kde.org/kdenlive/60c3a4dc2e66bc9aa72d6fe5a540d991d2fc1355">Commit.</a>
  </li>
  <li>
    Fix freeze deleting a group with clips on locked tracks. <a href="http://commits.kde.org/kdenlive/8f21a7254cc2d8494aa2bb5a1ac67f27c219881a">Commit.</a>
  </li>
  <li>
    Use the translated effect names for effect stack on the timeline. <a href="http://commits.kde.org/kdenlive/3275df927d623942c4778269481dc226c3017740">Commit.</a>
  </li>
  <li>
    Fix crash dragging clip in insert mode. <a href="http://commits.kde.org/kdenlive/1f157460b9a7bf52e4b3c7222b4d6b6ad64bfc68">Commit.</a>
  </li>
  <li>
    Use the translated transition names in the &#8216;Properties&#8217; header. <a href="http://commits.kde.org/kdenlive/7eceb7e5949e067c84c5fff069c6a3cc54ce8c99">Commit.</a>
  </li>
  <li>
    Fix freeze and fade ins allowed to go past last frame. <a href="http://commits.kde.org/kdenlive/4f6ccafe33e89667d74b20f990fa14ce47d049dd">Commit.</a>
  </li>
  <li>
    Fix revert clip speed failing. <a href="http://commits.kde.org/kdenlive/8d4602a14502ef7461c5505edb3fbcbdce175cc3">Commit.</a>
  </li>
  <li>
    Fix revert speed clip reloading incorrectly. <a href="http://commits.kde.org/kdenlive/8cd00620629c6262250e7bc087df23534f19a904">Commit.</a>
  </li>
  <li>
    Fix copy/paste of clip with negative speed. <a href="http://commits.kde.org/kdenlive/a8afc9db365fa995dbf77ce5ff16e3b104331217">Commit.</a>
  </li>
  <li>
    Fix issues on clip reload: slideshow clips broken and title duration reset. <a href="http://commits.kde.org/kdenlive/d48ce4a9c4c8a2284568291bb3de010eca8732be">Commit.</a>
  </li>
  <li>
    Fix slideshow effects disappearing. <a href="http://commits.kde.org/kdenlive/605b3ba2ee7d68fbe065d225cd00444b82980dee">Commit.</a>
  </li>
  <li>
    Fix track effect keyframes. <a href="http://commits.kde.org/kdenlive/84ea4be5e02af81b48d4caceb7f87fb26f98c8fe">Commit.</a>
  </li>
  <li>
    Fix track effects don&#8217;t invalidate timeline preview. <a href="http://commits.kde.org/kdenlive/37f2d6a6c6f1abda4c4c590364a64fc3a2178690">Commit.</a>
  </li>
  <li>
    Fix effect presets broken on comma locales, clear preset after resetting effect. <a href="http://commits.kde.org/kdenlive/95193a4488931a01ac63d086b35b95b2e38f1b44">Commit.</a>
  </li>
  <li>
    Fix crash in extract zone when no track is active. <a href="http://commits.kde.org/kdenlive/a8e74a7f65623efc54b472854898dbc5f66f32c2">Commit.</a>
  </li>
  <li>
    Fix reverting clip speed modifies in/out. <a href="http://commits.kde.org/kdenlive/e1c677c2aed4def6b58bd49f793e904089ff14ea">Commit.</a>
  </li>
  <li>
    Fix audio overlay showing up randomly. <a href="http://commits.kde.org/kdenlive/ae333a7aaf5f1f4c03afaddd0330dbedaec19022">Commit.</a>
  </li>
  <li>
    Fix Find clip in bin not always scrolling to correct position. <a href="http://commits.kde.org/kdenlive/e8772e2ee642ea5ef2fb30e89240f00b754da8d5">Commit.</a>
  </li>
  <li>
    Fix possible crash changing profile when cache job was running. <a href="http://commits.kde.org/kdenlive/1bce85d97f0c26826067712ba3e000a5824d4a78">Commit.</a>
  </li>
  <li>
    Fix editing bin clip does not invalidate timeline preview. <a href="http://commits.kde.org/kdenlive/7536ab6829a75af9622526942dba1836987ff5f9">Commit.</a>
  </li>
  <li>
    Fix audiobalance (MLT doesn&#8217;t handle start param as stated). <a href="http://commits.kde.org/kdenlive/35b7decc22ccbe8f3a5cb29151d64b3c779b17d1">Commit.</a>
  </li>
  <li>
    Fix target track inconsistencies:. <a href="http://commits.kde.org/kdenlive/d91c46551c151f33951afe4b343705cc98e298ba">Commit.</a>
  </li>
  <li>
    Make the strings in the settings dialog translatable. <a href="http://commits.kde.org/kdenlive/dc0d2fa4fe58363e2c9654331197e9a945e0215e">Commit.</a>
  </li>
  <li>
    Make effect names translatable in menus and in settings panel. <a href="http://commits.kde.org/kdenlive/f938a9bef295aca4e8f701290bd618b33f878987">Commit.</a>
  </li>
  <li>
    Remember last target track and restore when another clip is selected. <a href="http://commits.kde.org/kdenlive/27f98f31aa664473b8ec4026e8415ca6d1d7de9d">Commit.</a>
  </li>
  <li>
    Dont&#8217; process insert when no track active, don&#8217;t move cursor if no clip inserted. <a href="http://commits.kde.org/kdenlive/47cd52bc2cc3b52b2aaad56232a333443ed0746d">Commit.</a>
  </li>
  <li>
    Correctly place timeline toolbar after editing toolbars. <a href="http://commits.kde.org/kdenlive/a4e208b4e8972375b70c227def3d52ab36364068">Commit.</a>
  </li>
  <li>
    Lift/gamma/gain: make it possible to have finer adjustments with Shift modifier. <a href="http://commits.kde.org/kdenlive/c5d967c167fcbea3a11edea91e2502187fcaf7f2">Commit.</a>
  </li>
  <li>
    Fix MLT effects with float param and no xml description. <a href="http://commits.kde.org/kdenlive/8fe1e09d77cb180eafc223c16beb8677d2f70eec">Commit.</a>
  </li>
  <li>
    Cleanup timeline selection: rubber select works again when starting over a clip. <a href="http://commits.kde.org/kdenlive/87c797c634edc9d12f6245c88f20b21b2352b197">Commit.</a>
  </li>
  <li>
    Attempt to fix Windows build. <a href="http://commits.kde.org/kdenlive/06a199743c072fee3947f51439c199b991330110">Commit.</a>
  </li>
  <li>
    Various fixes for icon view: Fix long name breaking layout, fix seeking and subclip zone marker. <a href="http://commits.kde.org/kdenlive/72424a7369adf68b01bf431971f899fa87b751dd">Commit.</a>
  </li>
  <li>
    Fix some bugs in handling of NVidia HWaccel for proxies and timeline preview. <a href="http://commits.kde.org/kdenlive/0a0e190fba16d7e3086438decc22506e43eafffa">Commit.</a>
  </li>
  <li>
    Add 19.08 screenshot to appdata. <a href="http://commits.kde.org/kdenlive/98ca4d43acf709dbc977f38e060c162366112020">Commit.</a>
  </li>
  <li>
    Fix bug preventing sequential names when making serveral script renderings from same project. <a href="http://commits.kde.org/kdenlive/a8dd7d51578319d4814cace5066376f1801a0bfa">Commit.</a>
  </li>
  <li>
    Fix compilation with cmake < 3.5. <a href="http://commits.kde.org/kdenlive/e22da7531dd80ce5a3e1045d35b038ca3bc3b662">Commit.</a>
  </li>
  <li>
    Fix extract frame retrieving wrong frame when clip fps != project fps. <a href="http://commits.kde.org/kdenlive/dc3340cc67dfdc300a7177525fd3b808ad6c8fbc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/409927">#409927</a>
  </li>
  <li>
    Don&#8217;t attempt rendering an empty project. <a href="http://commits.kde.org/kdenlive/25b6af3add0e4c48a19ddb48186d4cf70c667330">Commit.</a>
  </li>
  <li>
    Fix incorrect source frame size for transform effects. <a href="http://commits.kde.org/kdenlive/f957c2fdbc2c9393a6ebfc2e1468851001563396">Commit.</a>
  </li>
  <li>
    Improve subclips visual info (display zone over thumbnail), minor cleanup. <a href="http://commits.kde.org/kdenlive/575506f647e131f68a155bf9d543e0cf29801910">Commit.</a>
  </li>
  <li>
    Small cleanup of bin preview thumbnails job, automatically fetch 10 thumbs at insert to allow quick preview. <a href="http://commits.kde.org/kdenlive/0d9ae632bf71cc6cbf75da836ee98142c266a152">Commit.</a>
  </li>
  <li>
    Fix project clips have incorrect length after changing project fps. <a href="http://commits.kde.org/kdenlive/c049c15c438750dd885116df00dfda586054d079">Commit.</a>
  </li>
  <li>
    Fix inconsistent behavior of advanced timeline operations. <a href="http://commits.kde.org/kdenlive/6fb5c4471463fb24e5fde6d36603f5c4bce33144">Commit.</a>
  </li>
  <li>
    Fix &#8220;Find in timeline&#8221; option in bin context menu. <a href="http://commits.kde.org/kdenlive/940053c4fa60f2fbf0d797f341bbf0e68697e652">Commit.</a>
  </li>
  <li>
    Support the new logging category directory with KF 5.59+. <a href="http://commits.kde.org/kdenlive/68a717f5f7598ef05f97373eb3d8622c8c184691">Commit.</a>
  </li>
  <li>
    Update active track description. <a href="http://commits.kde.org/kdenlive/08e457dea3871625a2270f0d8f8320e2e206e8f3">Commit.</a>
  </li>
  <li>
    Use extracted translations to translate asset descriptions. <a href="http://commits.kde.org/kdenlive/8bb8d0d0b1b808adf5e2aa65c93d170ae72733a6">Commit.</a>
  </li>
  <li>
    Fix minor typo. <a href="http://commits.kde.org/kdenlive/abf0acf14b43ef11d2777658e7dbeab290f2470f">Commit.</a>
  </li>
  <li>
    Make the file filters to be translatable. <a href="http://commits.kde.org/kdenlive/d2872eefe2361663050687e8be8670fb4c50277d">Commit.</a>
  </li>
  <li>
    Extract messages from transformation XMLs as well. <a href="http://commits.kde.org/kdenlive/45db5151c49f8be8f971532375ba2d9a1b9e42c3">Commit.</a>
  </li>
  <li>
    Don&#8217;t attempt to create hover preview for non AV clips. <a href="http://commits.kde.org/kdenlive/d5f55c98a6623e9e31af9d7895561ed48f10ac7a">Commit.</a>
  </li>
  <li>
    Add Cache job for bin clip preview. <a href="http://commits.kde.org/kdenlive/ef9cf6b904a961ca325c5986d241189f1bbbda36">Commit.</a>
  </li>
  <li>
    Preliminary implementation of Bin clip hover seeking (using shift+hover). <a href="http://commits.kde.org/kdenlive/0f45483c417c04e0c7834d0eebd74096a82effd6">Commit.</a>
  </li>
  <li>
    Translate assets names. <a href="http://commits.kde.org/kdenlive/3799402b0250ead69143ce713ea2bb7f4f837b0e">Commit.</a>
  </li>
  <li>
    Some improvments to timeline tooltips. <a href="http://commits.kde.org/kdenlive/a03c4f9a64c7410de1ffbe0e9cae12375cd13522">Commit.</a>
  </li>
  <li>
    Reintroduce extract clip zone to cut a clip whithout re-encoding. <a href="http://commits.kde.org/kdenlive/ff1fa2fdcaaa0727061e3dae42bfb9a4623ea328">Commit.</a> See bug <a href="https://bugs.kde.org/408402">#408402</a>
  </li>
  <li>
    Fix typo. <a href="http://commits.kde.org/kdenlive/969c699c33090cb2fcddf6feff543ad8cd607ef6">Commit.</a>
  </li>
  <li>
    Add basic collision check to speed resize. <a href="http://commits.kde.org/kdenlive/3647208029371930d67e363bb3fc8f9a6aca9e23">Commit.</a>
  </li>
  <li>
    Bump MLT dependency to 6.16 for 19.08. <a href="http://commits.kde.org/kdenlive/88f5f07f73430b3fc9d624720daa5c9c04feba51">Commit.</a>
  </li>
  <li>
    Exit grab mode with Escape key. <a href="http://commits.kde.org/kdenlive/f75e9a8c92ddf85ec199d49318824239a64ed400">Commit.</a>
  </li>
  <li>
    Improve main item when grabbing. <a href="http://commits.kde.org/kdenlive/b901b42ff6440c97fd46717725c2dcb6929008b1">Commit.</a>
  </li>
  <li>
    Minor improvement to clip grabbing. <a href="http://commits.kde.org/kdenlive/eb206e1d8c5b612f59e4cac37e79e40fc8f3fb6b">Commit.</a>
  </li>
  <li>
    Fix incorrect development version. <a href="http://commits.kde.org/kdenlive/e42314933f3c37f83ce7c5ec961a140010c852a3">Commit.</a>
  </li>
  <li>
    Make all clips in selection show grab status. <a href="http://commits.kde.org/kdenlive/63418f4d366219a64acf24aceafcade91e1f0e6e">Commit.</a>
  </li>
  <li>
    Fix &#8220;QFSFileEngine::open: No file name specified&#8221; warning. <a href="http://commits.kde.org/kdenlive/c1f06eaa5811b8d564db445287c9fb509f6f2db9">Commit.</a>
  </li>
  <li>
    Don&#8217;t initialize a separate Factory on first start. <a href="http://commits.kde.org/kdenlive/b8d7393bba93d1f45ff441a95b4644bb951aacaf">Commit.</a>
  </li>
  <li>
    Set name for track menu button in timeline toolbar. <a href="http://commits.kde.org/kdenlive/f34d20c8517aba557b71a784bbf9de3c0720e871">Commit.</a>
  </li>
  <li>
    Pressing Shift while moving an AV clip allows to move video part track independently of audio part. <a href="http://commits.kde.org/kdenlive/09b73be0dfe0e8e9a72ee669c52d860936d763d4">Commit.</a>
  </li>
  <li>
    Ensure audio encoding do not export video. <a href="http://commits.kde.org/kdenlive/8c91e11007a1ec62526495fe9d12393e239d881c">Commit.</a>
  </li>
  <li>
    Add option to sort audio tracks in reverse order. <a href="http://commits.kde.org/kdenlive/ae97c6094d05108b563191ae430762536cd58145">Commit.</a>
  </li>
  <li>
    Warn and try fixing clips that are in timeline but not in bin. <a href="http://commits.kde.org/kdenlive/490cd5a0268db921de857eb164eef4e35150875b">Commit.</a>
  </li>
  <li>
    Try to recover a clip if it&#8217;s parent id cannot be found in the project bin (use url). <a href="http://commits.kde.org/kdenlive/0bcf22b0e6469827cc083f1b3998adeaa367010d">Commit.</a> See bug <a href="https://bugs.kde.org/403867">#403867</a>
  </li>
  <li>
    Fix tests. <a href="http://commits.kde.org/kdenlive/fed419588ea663c82953b09a4b4ec0d565ed1c73">Commit.</a>
  </li>
  <li>
    Default fade duration is now configurable from Kdenlive Settings > Misc. <a href="http://commits.kde.org/kdenlive/075a68b1660e105fe3650c35e2d20e8c4a27c765">Commit.</a>
  </li>
  <li>
    Minor update for AppImage dependencies. <a href="http://commits.kde.org/kdenlive/063ebd6a2474703b2c804ead72202663c02b684e">Commit.</a>
  </li>
  <li>
    Change speed clip job: fix overwrite and UI. <a href="http://commits.kde.org/kdenlive/635d27a75725f862c6391caa76daedd834a5eb66">Commit.</a>
  </li>
  <li>
    Readd proper renaming for change speed clip jobs. <a href="http://commits.kde.org/kdenlive/40bb36679742cc5fbb9eecceb6757e54445ffdb8">Commit.</a>
  </li>
  <li>
    Add whole hierarchy when adding folder. <a href="http://commits.kde.org/kdenlive/5325726aa36104b5e523f387da24647cd80d57b5">Commit.</a>
  </li>
  <li>
    Fix subclip cannot be renamed. Store them in json and bump document version. <a href="http://commits.kde.org/kdenlive/18a58684f4e739e6e2d86d5a992bd35540c8726c">Commit.</a>
  </li>
  <li>
    Added audio capture channel & sample rate configuration. <a href="http://commits.kde.org/kdenlive/fb7e13a096e05ce3091cd8417b9ced5fb0d60ec7">Commit.</a>
  </li>
  <li>
    Add screen selection in screen grab widget. <a href="http://commits.kde.org/kdenlive/ad1636bd8d6ddf0065d68c3209e79de9e81ec187">Commit.</a>
  </li>
  <li>
    Initial implementation of clip speed change on Ctrl + resize. <a href="http://commits.kde.org/kdenlive/32423ba346a6b895b2f1ecd5afc631f9decb2d44">Commit.</a>
  </li>
  <li>
    Fix FreeBSD compilation. <a href="http://commits.kde.org/kdenlive/64e38903e0257f77374b83eb9551c5c0bb25a966">Commit.</a>
  </li>
  <li>
    Render dialog: add context menu to rendered jobs allowing to add rendered file as a project clip. <a href="http://commits.kde.org/kdenlive/fc32e1347fb0e485c3f242a280bbd7a6b14c4c8e">Commit.</a>
  </li>
  <li>
    Ensure automatic compositions are compositing with correct track on project opening. <a href="http://commits.kde.org/kdenlive/b5aa6d01c2b1882131e1dba9b0720be96bdb8e89">Commit.</a>
  </li>
  <li>
    Fix minor typo. <a href="http://commits.kde.org/kdenlive/1dae1c26d370c8779b720e4db0b150f71c4696eb">Commit.</a>
  </li>
  <li>
    Add menu option to reset the Kdenlive config file. <a href="http://commits.kde.org/kdenlive/f9750d6eb895762507392400560d93e8ed6a223d">Commit.</a>
  </li>
  <li>
    Motion tracker: add steps parameter. Patch by Balazs Durakovacs. <a href="http://commits.kde.org/kdenlive/3f8e18314aaef964a210ddd6f5b1334fa8b5a34b">Commit.</a>
  </li>
  <li>
    Try to make binary-factory mingw happy. <a href="http://commits.kde.org/kdenlive/4633cfc9430da83d8a60fd814a80757fafbcbe9c">Commit.</a>
  </li>
  <li>
    Remove dead code. <a href="http://commits.kde.org/kdenlive/10bb1cbab1320bc78065d238de64a53e37b35c66">Commit.</a>
  </li>
  <li>
    Add some missing bits in Appimage build (breeze) and fix some plugins paths. <a href="http://commits.kde.org/kdenlive/0f6247405619486d9f217123791a9e87ff39e5e7">Commit.</a>
  </li>
  <li>
    AppImage: disable OpenCV freetype module. <a href="http://commits.kde.org/kdenlive/1427b64d067d5441d832f4915968464583a4d8e6">Commit.</a>
  </li>
  <li>
    Docs: Unbreak menus. <a href="http://commits.kde.org/kdenlive/a9620d79c82332607676e20f8092de930f9b7deb">Commit.</a>
  </li>
  <li>
    Sync Quick Start manual with UserBase. <a href="http://commits.kde.org/kdenlive/bc2a134a510ad3671aa2f069e0468a296dd41e2d">Commit.</a>
  </li>
  <li>
    Fix transcoding crashes caused by old code. <a href="http://commits.kde.org/kdenlive/ba9f975cc5f55fbc16da4292a3577ed57dd7ad1d">Commit.</a>
  </li>
  <li>
    Reenable trancode clip functionality. <a href="http://commits.kde.org/kdenlive/8d6b54069847da4e13284caae46b24f6e4b68b1c">Commit.</a>
  </li>
  <li>
    Fix broken fadeout. <a href="http://commits.kde.org/kdenlive/a18c59ff5a560e2945ebd030e9208552fd4cd036">Commit.</a>
  </li>
  <li>
    Small collection of minor improvements. <a href="http://commits.kde.org/kdenlive/e879cd52c53d3b240be3b950d8eaaff1fe269428">Commit.</a>
  </li>
  <li>
    Search effects from all tabs instead of only the selected tab. <a href="http://commits.kde.org/kdenlive/9ab06abc44f0346eaf81bcbb23a2f68171778c5e">Commit.</a>
  </li>
  <li>
    Check whether first project clip matches selected profile by default. <a href="http://commits.kde.org/kdenlive/6be4b4c17c8b8e615a28567d8f87de5cdc48c725">Commit.</a>
  </li>
  <li>
    Improve marker tests, add abort testing feature. <a href="http://commits.kde.org/kdenlive/eddcbd7cd308386f24e73ce1a8afa913b0a4b990">Commit.</a>
  </li>
  <li>
    Revert &#8220;Trying to submit changes through HTTPS&#8221;. <a href="http://commits.kde.org/kdenlive/d9368bcf690b51dc3b605351f16621b25ad6c8ef">Commit.</a>
  </li>
  <li>
    AppImafe: define EXT_BUILD_DIR for Opencv contrib. <a href="http://commits.kde.org/kdenlive/1a2bd68ed6c6647508ec173778d5cb6788f457e1">Commit.</a>
  </li>
  <li>
    Fix OpenCV build. <a href="http://commits.kde.org/kdenlive/f89b8f92cf79c0f7b8819bee6f908f1ee39c9ca9">Commit.</a>
  </li>
  <li>
    AppImage update: do not build MLT inside dependencies so we can have more frequent updates. <a href="http://commits.kde.org/kdenlive/85f5f1baa9ba485fa886be2e28a8a96d8a680a85">Commit.</a>
  </li>
  <li>
    If a timeline operation touches a group and a clip in this group is on a track that should not be affected, break the group. <a href="http://commits.kde.org/kdenlive/d819f17bfc361a3c0ddd41579d204a287997c0d3">Commit.</a>
  </li>
  <li>
    Add tests for unlimited clips resize. <a href="http://commits.kde.org/kdenlive/960a505e4548536bd09a6dd06608db7e89c7c126">Commit.</a>
  </li>
  <li>
    Small fix in tests. <a href="http://commits.kde.org/kdenlive/e51deee1ca5169e8dbb4ea0fefc02f0323d97b9a">Commit.</a>
  </li>
  <li>
    Renderwidget: Use max number of threads in render. <a href="http://commits.kde.org/kdenlive/9e2748fb9ec0488eb6ac9564bd6e8741ac486ef1">Commit.</a>
  </li>
  <li>
    Don&#8217;t allow resizing while dragging. Fixes #134. <a href="http://commits.kde.org/kdenlive/e2c4c7c1bba43c670fe06bb63573ecceb1c28cd7">Commit.</a>
  </li>
  <li>
    Revert &#8220;Revert &#8220;Merge branch &#8216;1904&#8217;&#8221;&#8221;. <a href="http://commits.kde.org/kdenlive/c743a878efa65780c0e5f3e54ac41fd2c675b15d">Commit.</a>
  </li>
  <li>
    Revert &#8220;Merge branch &#8216;1904&#8217;&#8221;. <a href="http://commits.kde.org/kdenlive/2518ec469b7c3911d05936d166fa94c23959b9d6">Commit.</a>
  </li>
  <li>
    Update master appdata version. <a href="http://commits.kde.org/kdenlive/19e5b0e1489130a85892a634966963d8f1970ddc">Commit.</a>
  </li>
</ul>
