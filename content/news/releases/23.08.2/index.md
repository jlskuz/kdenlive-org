---
author: Farid Abdelnour
date: 2023-10-16T17:52:36+00:00
aliases:
- /en/2023/10/kdenlive-23-08-2-released/
- /it/2023/10/kdenlive-23-08-2-2/
- /de/2023/10/kdenlive-23-08-2/
discourse_topic_id: 6114
---

The second maintenance release of the 23.08 version is out with a galore of fixes. This version continues the performance optimizations introduced in last release.
