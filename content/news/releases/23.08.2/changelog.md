  * Fix Fit Zoom. [Commit][1]. Fixes bug [#472754][2].
  * Fix cannot move clip to 0 in some cases. [Commit][3].
  * Fix erratic behavior when requesting to add same track transitions (mixes) to several clips. [Commit][4].
  * Redirect Settings > Configure Kdenlive > Help to the online documentation. [Commit][5].
  * Fix multiple audio streams broken by MLT&#8217;s new astream property. [Commit][6]. Fixes bug [#474895][7].
  * Fix dialogs not correctly deleted, e.g. add track dialog, causing crash on exit. [Commit][8].
  * Ensure clips with audio (for exemple playlists) don&#8217;t block audio when inserted on video track. [Commit][9].
  * Ensure translations cannot mess with file extensions. [Commit][10].
  * Fix another case blocking separate track move. [Commit][11].
  * Fix grabbed clips cannot be moved on upper track in some cases. [Commit][12].
  * Fix move clip part of a group on another track not always working. [Commit][13].
  * Fix playlist count not correctly updated, allowing to delete last sequence. [Commit][14]. Fixes bug [#474988][15].
  * Fix motion-tracker Nano file name and links to the documentation. [Commit][16].
  * Stop installing kdenliveui.rc also as separate file, next to Qt resource. [Commit][17].
  * Fix tests and possible corruption in recent mix fix. [Commit][18].
  * Correctly highlight newly dropped files in library. [Commit][19].
  * Fix threading issue crashing in resource widget. [Commit][20].
  * Fix freeze on adding mix. [Commit][21].
  * Make Lift work as expected by most users. [Commit][22]. Fixes bug [#447948][23]. Fixes bug [#436762][24].
  * Fix load task discarding kdenlive settings (caused timeline clips to miss the &#8220;proxy&#8221; icon. [Commit][25].
  * Fix multiple issues with Lift/Gamma/Gain undo. [Commit][26]. Fixes bug [#472865][27]. Fixes bug [#462406][28].
  * Fix freeze / crash on project opening. [Commit][29].
  * Optimize RAM usage by not storing producers on which we did a get_frame operation. [Commit][30].
  * Fix guide multi-export adding an extra dot to the filename. [Commit][31].
  * Correctly update effect stack when switching timeline tab. [Commit][32].

 [1]: http://commits.kde.org/kdenlive/65f7bf7f9a106c43c5792e4153b281138ca87fd6
 [2]: https://bugs.kde.org/472754
 [3]: http://commits.kde.org/kdenlive/28efa5c1dcec0f1499f304b3ffa33f4be2ee6537
 [4]: http://commits.kde.org/kdenlive/4ea6b3bfe1f49e69b172d6fbf813d4fec5532244
 [5]: http://commits.kde.org/kdenlive/e96bf9e01b45c15350d74f64e0005f9a852e95c0
 [6]: http://commits.kde.org/kdenlive/e4b545e87a48fa35c56b471e82dfe5124ecbe875
 [7]: https://bugs.kde.org/474895
 [8]: http://commits.kde.org/kdenlive/edf3195e825d27422de10b92fc5315e43dddbe1f
 [9]: http://commits.kde.org/kdenlive/38fe02f47b38d701d6acd9e0dd8a25268a13aa35
 [10]: http://commits.kde.org/kdenlive/a23a7fb1b419c08aad7e72d2561439a420587c98
 [11]: http://commits.kde.org/kdenlive/281f2071d285f470bb88cc63d8813faf0c1373b9
 [12]: http://commits.kde.org/kdenlive/c13810cf42549c405f72d116bfe0ab2bb3d7b66b
 [13]: http://commits.kde.org/kdenlive/0085018cb95703b9286fdecc48fc9a78bd56d3f7
 [14]: http://commits.kde.org/kdenlive/c2d738f9e7cc85f049febbe65351edc3ea8832ad
 [15]: https://bugs.kde.org/474988
 [16]: http://commits.kde.org/kdenlive/ad5d2706c55810721685473154f9d664dbba26fd
 [17]: http://commits.kde.org/kdenlive/7c20d1a2e5c0dc5cfeca4c4274a0d2e2afb33bcb
 [18]: http://commits.kde.org/kdenlive/92fcb745fcaa39696c9385c74f66553b1a3a4261
 [19]: http://commits.kde.org/kdenlive/ba9ceb859a1cf82d86b737cd001048b6b9c7b6c1
 [20]: http://commits.kde.org/kdenlive/68a3e0aaad7124554cd27f32af96abeaee94cf54
 [21]: http://commits.kde.org/kdenlive/96cd6edc81f631743e9527ca853776ab174af683
 [22]: http://commits.kde.org/kdenlive/b096aed4e93ca44cdd2d8c3f1072baf881780048
 [23]: https://bugs.kde.org/447948
 [24]: https://bugs.kde.org/436762
 [25]: http://commits.kde.org/kdenlive/b152e4d93f65beda8abf8c05cba38eae656a52cf
 [26]: http://commits.kde.org/kdenlive/79dae6dcd0c81d94956aa58b17fde752cc121eb3
 [27]: https://bugs.kde.org/472865
 [28]: https://bugs.kde.org/462406
 [29]: http://commits.kde.org/kdenlive/2c5d4212a9a5323a630ededa05a2daeb84b365b8
 [30]: http://commits.kde.org/kdenlive/e29b16d73869ff1ce03ff5153c94f386bb2f282c
 [31]: http://commits.kde.org/kdenlive/4732230d57c08001bedd51238ef3b4430e3e11aa
 [32]: http://commits.kde.org/kdenlive/d4154e08a3e0eb4b0392ef2198e54890eee34ddf
