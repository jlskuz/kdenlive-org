---
title: Kdenlive 19.12.3 is out
author: Farid Abdelnour
date: 2020-03-06T15:00:51+00:00
aliases:
- /en/2020/03/kdenlive-19-12-3-is-out/
- /it/2020/03/kdenlive-19-12-3-disponibile/
- /de/2020/03/kdenlive-19-12-3-ist-freigegeben/
---

The last minor release of the 19.12 series is out with bug fixes and usability improvements. Next month we mark the one year anniversary of the refactored code base so stay tuned for many nifty features coming like pitch shifting, tagging and rating of clips in the project bin and the much anticipated preview scaling of monitors bringing a huge performance boost.
