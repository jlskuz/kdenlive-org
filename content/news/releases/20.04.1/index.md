---
author: Jean-Baptiste Mardelle
date: 2020-05-16T09:05:59+00:00
aliases:
- /en/2020/05/kdenlive-20-04-1-released/
- /de/2020/05/kdenlive-20-04-1-freigegeben/
---

![Screenshot of Kdenlive 20.04.1](screenshot.png)

We just released the first bugfix version for the 20.04 Kdenlive version. Despite our continued work, many issues were still affecting the 20.04.0 version. A lot of work has been done to fix crashes and other annoying issues, so the 20.04.1 version should be much more reliable and stable. We have a long list of fixed issues.

**Windows:** Motion Tracking effect integrated.

**AppImage:** Fix crash on older systems (remove OpenCV sse4 dependency)

Most notably, we have now fixed:

## Crash fixes:

  * Disable loading of .mlt playlists with profile not equal to project profile (caused crash)
  * Fix possible crash on subclip thumbnail creation
  * Fix crash trying to move timeline clip to another track when bin clip had some effects
  * Fix crash creating DVD chapters
  * Fix playlist profile incorrectly detected, leading to crashes when seeking in timeline

## Important fixes:

  * Fix timeline preview not invalidated on hide track
  * Proxy clips: fix vaapi_h264 profile and ensure we keep the stream order
  * Use safer QSaveFile class to ensure our document is not corrupted on disk full
  * Fix rubber selection moving with scolling
  * Fix image rendering (add %05d suffix)
  * Fix timeline preview was incorrectly disabled
  * Fix MLT 6.20 avformat slideshows not recognized on onpening (convert to standard qimage)
  * Fix template title clips in timeline resetting duration on project re-open
  * Fix paste clips/compositions sometimes not working or pasting on wrong track/position
  * Fix compositions broken on insert audio track
  * Fix audio drag from monitor broken
  * Fix "archive project" creating broken backup files
  * Fix track effect not adjusting duration when track duration changes (new clip appended)
  * Additionnaly, a fix was committed in MLT git to fix audio desync with the pitch shift effect

## UI fixes:

  * Enable audiospectrum by default
  * Make compositions use less vertical space, expand when selected
  * Fix various geometry keyframe regressions in monitor
  * Improve handling of missing clips, draw "photo" frame on image clips
  * Improve notification of missing(deleted files) and don't allow reloading a missing clip
  * Always sync all keyframeable effects position with timeline position
  * On clip move, also consider moving clip's markers for snapping
  * Delete all selected markers in clip properties dialog when requested
  * Implement timecode parsing when pasting text in Project notes
  * Shift + collapse will collapse expand all audio or video tracks
  * On clip cut, auto reselect right part of the clip if it was previously selected
  * Fix timeline sometimes not scrolling to cursor position
  * Fix aspect ratio not working on title images
  * titler: Remember to show background
  * Fix glitch in bin item selection, causing some actions to be disabled
  * Show clip speed before name so it's visible when changing speed of a clip with long name
  * Don't use drop frame timecode for 23.98
