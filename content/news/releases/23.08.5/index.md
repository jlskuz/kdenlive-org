---
author: Farid Abdelnour
date: 2024-02-19T12:37:39+00:00
aliases:
- /en/2024/02/kdenlive-23-08-5-released/
- /de/2024/02/kdenlive-23-08-5/
discourse_topic_id: 10676
---

Kdenlive 23.08.5 has been released, featuring a multitude of bug fixes, including many issues related to nested sequences and same-track transitions. This release temporarily removes Movit effects until they are stable for production. However, the primary focus of this release was to continue the ongoing efforts in transitioning to Qt6 and KF6.

_It's important to note that, due to this transition, we regret to inform our Mac users that a package for this release won't be available. We kindly request them to wait for the 24.02 release, expected by the end of the month._

