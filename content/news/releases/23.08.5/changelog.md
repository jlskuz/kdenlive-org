  * Fix undocked widgets don&#8217;t have a title bar to allow moving / re-docking. [Commit][1].
  * Multi guides export: replace slash and backslash in section names to fix rendering. [Commit][2]. Fixes bug [#480845][3].
  * Fix sequence corruption on project load. [Commit][4]. Fixes bug [#480776][5].
  * Fix multiple archiving issues. [Commit][6]. Fixes bug [#456346][7].
  * Fix possible sequence corruption. [Commit][8]. Fixes bug [#480398][9].
  * Fix sequences folder id not correctly restored on project opening. [Commit][10].
  * Fix Luma issue. [Commit][11]. See bug [#480343][12].
  * Fix subtitles not covering transparent zones. [Commit][13]. Fixes bug [#480350][14].
  * Group resize: don&#8217;t allow resizing a clip to length < 1. [Commit][15]. Fixes bug [#480348][16].
  * Fix crash cutting grouped overlapping subtitles. Don&#8217;t allow the cut anymore, add test. [Commit][17]. Fixes bug [#480316][18].
  * Fix clip monitor not updating when clicking in a bin column like date or description. [Commit][19]. Fixes bug [#480148][20].
  * Fix start playing at end of timeline. [Commit][21]. Fixes bug [#479994][22].
  * Fix save clip zone from timeline adding an extra frame. [Commit][23]. Fixes bug [#480005][24].
  * Fix clips with mix cannot be cut, add test. [Commit][25]. See bug [#479875][26].
  * Fix project monitor loop clip. [Commit][27].
  * Fix monitor offset when zooming back to 1:1. [Commit][28].
  * Fix sequence effects lost. [Commit][29]. Fixes bug [#479788][30].
  * Improved fix for center crop issue. [Commit][31].
  * Fix center crop adjust not covering full image. [Commit][32]. Fixes bug [#464974][33].
  * Disable Movit until it&#8217;s stable (should have done that a long time ago). [Commit][34].
  * Fix cannot save list of project files. [Commit][35]. Fixes bug [#479370][36].
  * Fix editing title clip with a mix can mess up the track. [Commit][37]. Fixes bug [#478686][38].
  * Fix audio mixer cannot enter precise values with keyboard. [Commit][39].
  * Prevent, detect and possibly fix corrupted project files, fix feedback not displayed in project notes. [Commit][40]. See bug [#472849][41].
  * Test project&#8217;s active timeline is not always the first sequence. [Commit][42].
  * Ensure secondary timelines are added to the project before being loaded. [Commit][43].
  * Ensure autosave is not triggered when project is still loading. [Commit][44].
  * Fix variable name shadowing. [Commit][45].
  * When switching timeline tab without timeline selection, don&#8217;t clear effect stack if it was showing a bin clip. [Commit][46].
  * Fix crash pressing del in empty effect stack. [Commit][47].
  * Ensure check for HW accel is also performed if some non essential MLT module is missing. [Commit][48].
  * Fix tests. [Commit][49].
  * Fix closed sequences losing properties, add more tests. [Commit][50].
  * Don&#8217;t attempt to load timeline sequences more than once. [Commit][51].
  * Fix timeline groups lost after recent commit on project save. [Commit][52].
  * Ensure we always use the correct timeline uuid on some clip operations. [Commit][53].
  * Add animation: remember last used folder. [Commit][54]. See bug [#478688][55].
  * Refresh effects list after downloading an effect. [Commit][56].
  * Fix audio or video only drag of subclips. [Commit][57]. Fixes bug [#478660][58].
  * Fix editing title clip duration breaks title (recent regression). [Commit][59].
  * Glaxnimate animations: use rawr format instead of Lottie by default. [Commit][60]. Fixes bug [#478685][61].
  * Fix timeline focus lost when dropping an effect on a clip. [Commit][62].
  * Fix dropping lots of clips in Bin can cause freeze on abort. [Commit][63].
  * Right click on a mix now shows a mix menu (allowing deletion). [Commit][64]. Fixes bug [#442088][65].
  * Don&#8217;t add mixes to disabled tracks. [Commit][66]. See bug [#442088][65].
  * Allow adding a mix without selection. [Commit][67]. See bug [#442088][65].
  * Remove line missing from merge commit. [Commit][68].
  * Fix proxied playlist clips (like stabilized clips) rendered as interlaced. [Commit][69]. Fixes bug [#476716][70].
  * Always keep all timeline models opened. [Commit][71]. See bug [#478745][72].

 [1]: http://commits.kde.org/kdenlive/a97b6ff81793793b485b803aaed7e5d74433c065
 [2]: http://commits.kde.org/kdenlive/c6c53a8e642304931a2ad2678db7142abfc88d58
 [3]: https://bugs.kde.org/480845
 [4]: http://commits.kde.org/kdenlive/b2a028452aba7dabd969a0adb6379037f025626d
 [5]: https://bugs.kde.org/480776
 [6]: http://commits.kde.org/kdenlive/617a8cd9fa1df0e0e15a0eb267644ae70b8fc552
 [7]: https://bugs.kde.org/456346
 [8]: http://commits.kde.org/kdenlive/f8558885255594caa5420f1f23d984b61c273305
 [9]: https://bugs.kde.org/480398
 [10]: http://commits.kde.org/kdenlive/e3ace26cc5bf376aee3d3865aae2b344f17b76f5
 [11]: http://commits.kde.org/kdenlive/138cea76db4b9064769ec6a2690832a693f9190c
 [12]: https://bugs.kde.org/480343
 [13]: http://commits.kde.org/kdenlive/d3469628132baf61efa63b45f2fa0de5ca68879e
 [14]: https://bugs.kde.org/480350
 [15]: http://commits.kde.org/kdenlive/641c89e8a8bfeabc99f255b2a0a22e683295c8e2
 [16]: https://bugs.kde.org/480348
 [17]: http://commits.kde.org/kdenlive/aa64e82c9e6d652f7581f37da800ee38df3a696f
 [18]: https://bugs.kde.org/480316
 [19]: http://commits.kde.org/kdenlive/b415f112ab79f1e15d1ca6d417ee8e3a4ef8e17e
 [20]: https://bugs.kde.org/480148
 [21]: http://commits.kde.org/kdenlive/89207f3d8df341fb343299ff1b8c47abce8ddaf1
 [22]: https://bugs.kde.org/479994
 [23]: http://commits.kde.org/kdenlive/a9147421a960a4b09e86eccad7424a7da1732744
 [24]: https://bugs.kde.org/480005
 [25]: http://commits.kde.org/kdenlive/0f0cdaca34d7a1bf3f4a4ba3dbf88d738b5947cc
 [26]: https://bugs.kde.org/479875
 [27]: http://commits.kde.org/kdenlive/24df69822b26c9718323503ce3ad1a42cde31351
 [28]: http://commits.kde.org/kdenlive/3d9c15366904084fad740c6f1fc4be2f85d97da4
 [29]: http://commits.kde.org/kdenlive/1b9dd950718959e0608682d3c8ff65abd2c8a717
 [30]: https://bugs.kde.org/479788
 [31]: http://commits.kde.org/kdenlive/c056a1fcbe8615c1eb369fda3ad0bed8248bc5ce
 [32]: http://commits.kde.org/kdenlive/89350b5f3254a7a614a6d7e95d53d5683af9c3cc
 [33]: https://bugs.kde.org/464974
 [34]: http://commits.kde.org/kdenlive/6142637f8e8a11479cd8c66e7940198dc8af93a4
 [35]: http://commits.kde.org/kdenlive/9a5bf8b39d7d989968bdce47e51d83bfb9f85a97
 [36]: https://bugs.kde.org/479370
 [37]: http://commits.kde.org/kdenlive/22eb29f8546b183f61e4c61d3ce320f646060255
 [38]: https://bugs.kde.org/478686
 [39]: http://commits.kde.org/kdenlive/0a3cb68a579a289f95e33f0d8a53ce7087df0521
 [40]: http://commits.kde.org/kdenlive/dd0c809b64c8f8ae48edb11dc56415767183a5f7
 [41]: https://bugs.kde.org/472849
 [42]: http://commits.kde.org/kdenlive/26e7c26d5842d7fc169626e372237b0aa06300e7
 [43]: http://commits.kde.org/kdenlive/2a105c73492e8c507cb74a7751a0bb25317ac8cc
 [44]: http://commits.kde.org/kdenlive/30b7b2b0df994edbabb7e44be6a9af637b2fa9d6
 [45]: http://commits.kde.org/kdenlive/fc8b2b152397bc6c3d177dda79276a6a6bc13b88
 [46]: http://commits.kde.org/kdenlive/8b14d06bc1ffae66b1671e42cb12a946ec93bda6
 [47]: http://commits.kde.org/kdenlive/b491d94d2b263f126d71aa739510baf3c839ed9f
 [48]: http://commits.kde.org/kdenlive/8aca4961e07d7df4e57132d8945e1b0904c05e53
 [49]: http://commits.kde.org/kdenlive/e0484391681d6e05dd6eb58ea0b0726f0cee8048
 [50]: http://commits.kde.org/kdenlive/64468b079f04b80dadc0067e9f43a3fe8ecc4336
 [51]: http://commits.kde.org/kdenlive/8caeb59641ca282f679ffbb50d38a43de6ef2e06
 [52]: http://commits.kde.org/kdenlive/eec349040894bf25fbe68bc9448ed38e12b94019
 [53]: http://commits.kde.org/kdenlive/e81b4be508c5d65c29a2b2aea901c0a5ae16ddfa
 [54]: http://commits.kde.org/kdenlive/86e1f68ae4f46833b7ad346262a451d5f0fbfbf7
 [55]: https://bugs.kde.org/478688
 [56]: http://commits.kde.org/kdenlive/865f6b7fa9727e21d544a994a32ada2d510a571e
 [57]: http://commits.kde.org/kdenlive/9d3702270d7baec463cf558848f0a23960ffa4cf
 [58]: https://bugs.kde.org/478660
 [59]: http://commits.kde.org/kdenlive/008e7bde11e77bd67abf403b222e94fc750906c8
 [60]: http://commits.kde.org/kdenlive/6b9dbaeb1f983ccf4bf9ffeaef6d6a70e5fd5a49
 [61]: https://bugs.kde.org/478685
 [62]: http://commits.kde.org/kdenlive/b93a61291090c4f3e0eb7c11e7797bd0ea4d1654
 [63]: http://commits.kde.org/kdenlive/bd0cb1cb3281998e533f1e2e5f21d7aa17ce84dd
 [64]: http://commits.kde.org/kdenlive/24bf262749b27c6861f822abdba3903a0edce234
 [65]: https://bugs.kde.org/442088
 [66]: http://commits.kde.org/kdenlive/5f50241808dd219fe4594b5fc318fc35db5df0d0
 [67]: http://commits.kde.org/kdenlive/2a4c4a18273415946ada131a4c5df4beaf40e29c
 [68]: http://commits.kde.org/kdenlive/84eb0a85ad87b34990c92f19be07854e0c76a028
 [69]: http://commits.kde.org/kdenlive/19838f9e1ec4bf93be10c179e59c2498f682dbc0
 [70]: https://bugs.kde.org/476716
 [71]: http://commits.kde.org/kdenlive/e66afa13c9dd7a0e8f41946799e6249345772f85
 [72]: https://bugs.kde.org/478745
