---
author: Farid Abdelnour
date: 2022-06-14T15:02:01+00:00
aliases:
- /en/2022/06/kdenlive-22-04-2-released/
- /it/2022/06/kdenlive-22-04-2-3/
- /de/2022/06/kdenlive-22-04-2-2/
---
The polishing and stability effort of this release cycle continues with the release Kdenlive 22.04.2 which comes with bug fixes to the AppImage and Mac packages, render widget, same track transitions, subtitles and project loading issues.
