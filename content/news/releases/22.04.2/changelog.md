  * Fix icon color change in some situations (eg. Appimage). [Commit.][1] Fixes bug [#450556][2]
  * Fix incorrect lambda capture leading to crash. [Commit.][3]
  * Fix AppImage icons. [Commit.][4] See bug [#451406][5]
  * Online resources: only show warning about loading time once. [Commit.][6] See bug [#454470][7]
  * Clang format fixes. [Commit.][8]
  * Fix crash clicking ok in empty transcoding dialog. [Commit.][9]
  * Fix possible crash when load task is running on exit. [Commit.][10]
  * Fix file watcher broken, changed clips were not detected anymore. [Commit.][11]
  * Fix timeremap clip always using proxies on rendering. [Commit.][12] Fixes bug [#454089][13]
  * Ensure internal effects like subtitles stay on top so that they are not affected by color or transform effects. [Commit.][14]
  * Fix crash on undo center keyframe. [Commit.][15]
  * Fix crash changing clip monitor bg color when no clip is selected. [Commit.][16]
  * Fix crash on undo selected clip insert. [Commit.][17]
  * Fix nvenc codec. [Commit.][18] See bug [#454469][19]
  * Fix clip thumbs not discarded on property change. [Commit.][20]
  * On document loading, also check images for changes. [Commit.][21]
  * Fix tests and mix direction regression. [Commit.][22]
  * Fix major corruption on undo/redo clip cut, with tests. [Commit.][23]
  * Project loading: detect and fix corruption if audio or video clips on the same track use a different producer. [Commit.][24]
  * Fix crash dropping an effect on the clip monitor. [Commit.][25]
  * Speedup maker search. [Commit.][26]
  * Fix cannot put monitor in fullscreen with mirrored screens. [Commit.][27]
  * Fix mix on very short AV clips broken, with test. [Commit.][28]
  * Fix Slide mix not correctly updated when creating a new mix on the previous clip, add tests. [Commit.][29] See bug [#453770][30]
  * Fix mix mix not correctly reversed in some cases and on undo. [Commit.][31]
  * Fix slide composition going in wrong direction (mix is still todo). [Commit.][32] See bug [#453770][30]
  * Fix several small glitches in bin selection. [Commit.][33]
  * Fix clip height not aligned to its track. [Commit.][34]
  * Fix speech to text on Mac. [Commit.][35]
  * Fix crash/corruption in overwrite mode when moving grouped clips above or below existing tracks. [Commit.][36]
  * Fix missing audio with “WebM-VP9/Opus (libre)” preset. [Commit.][37] See bug [#452950][38]
  * [Render Widget] Allow more steps for quality slider. [Commit.][39]
  * [Render Presets] Fix wrongly reversed quality with custom presets. [Commit.][40]
  * [Render Presets] Add more speed preset steps for x254 and x256. [Commit.][41]
  * Fix mixers don’t display levels if a track was added/removed with collapsed mixer. [Commit.][42]
  * Fix possible crash in transcoding dialog if there are no clips to convert. [Commit.][43]
  * [RenderWidget] Add scrollbar to improve experience on small screens. [Commit.][44]

 [1]: http://commits.kde.org/kdenlive/460910bcea5bdd9d1ecd2ca2dc3fc45e960afe0b
 [2]: https://bugs.kde.org/450556
 [3]: http://commits.kde.org/kdenlive/056d146294df253c8124594df399b015d8f75897
 [4]: http://commits.kde.org/kdenlive/9a5315d3e52c9caede81f9ccaae2b5c1cd054b1b
 [5]: https://bugs.kde.org/451406
 [6]: http://commits.kde.org/kdenlive/6c7e607f955991f64feae81efd4a40f2b6d37905
 [7]: https://bugs.kde.org/454470
 [8]: http://commits.kde.org/kdenlive/f4cdefe0e47601fcfae483884009708a21207ff5
 [9]: http://commits.kde.org/kdenlive/038b7c03acc621fdad7d02ef6d80f59990c31354
 [10]: http://commits.kde.org/kdenlive/1920dd4c15f4ec35e0d16c0e93c40468124efa8c
 [11]: http://commits.kde.org/kdenlive/16b177c6a900fea6eb2be13439005a25552f49f6
 [12]: http://commits.kde.org/kdenlive/bfab72ad7079f52516e285f985039d3044b980de
 [13]: https://bugs.kde.org/454089
 [14]: http://commits.kde.org/kdenlive/49543041f85981d731b23c07fecc1f3f2e65368f
 [15]: http://commits.kde.org/kdenlive/d9a9c00dca9b7065b551ddcc6c6c25ea94bbdc43
 [16]: http://commits.kde.org/kdenlive/ce28f4d2520edbeba455d7b23fdbeea6c42726dd
 [17]: http://commits.kde.org/kdenlive/660858733e4eeda7fc5902ca03b73cd0ae90b073
 [18]: http://commits.kde.org/kdenlive/456bb4fb76c6f1066ab637687f97658ea47abee3
 [19]: https://bugs.kde.org/454469
 [20]: http://commits.kde.org/kdenlive/338e677b54629df877d4f0091c8d08ddad8bc3fe
 [21]: http://commits.kde.org/kdenlive/937e2061349c94c3f3411013772b315d2404e3f4
 [22]: http://commits.kde.org/kdenlive/e9a62a1c0a273e07388b90743719b3c2e740a4ea
 [23]: http://commits.kde.org/kdenlive/919596ad105dd4c5d6e4f3419963c39d61732ea8
 [24]: http://commits.kde.org/kdenlive/5e9d56a96feeeac8fbf77871ea4c1a0b8eb75c27
 [25]: http://commits.kde.org/kdenlive/a7fc91dbabc05e9778698ab6aafd747a9507db49
 [26]: http://commits.kde.org/kdenlive/006c7a1761669b3fbd0000eda130cf7aae366111
 [27]: http://commits.kde.org/kdenlive/13c5f4434cf01dacec300f1a0e0490cd783c6efa
 [28]: http://commits.kde.org/kdenlive/a6c365a3d01441bf6bf77782df9db2a323498927
 [29]: http://commits.kde.org/kdenlive/019491631e59fc735fe1b3c7be33edea9314bed6
 [30]: https://bugs.kde.org/453770
 [31]: http://commits.kde.org/kdenlive/8142abe5c2754ed09945af51ffa16284c2df0542
 [32]: http://commits.kde.org/kdenlive/d332064716d3ee9eaedaaf9402c9ffcd15a42de5
 [33]: http://commits.kde.org/kdenlive/cf5e56aa4620395b0895a4885482054b489c86c1
 [34]: http://commits.kde.org/kdenlive/cf5c0a473a59f4eda17bdf367377197b0868bcba
 [35]: http://commits.kde.org/kdenlive/8ffbef7fb62a5b70b8ee31e6dcc457ae3e79886c
 [36]: http://commits.kde.org/kdenlive/b128c3b384addfba1228ab931dc08c0ee16f8d97
 [37]: http://commits.kde.org/kdenlive/57d849d69077fb0abd6faed2bfdc3d29c2d58428
 [38]: https://bugs.kde.org/452950
 [39]: http://commits.kde.org/kdenlive/5060037f97a4f487f2b9ae161d4fafa24f6ec455
 [40]: http://commits.kde.org/kdenlive/01ca6009148dd49f52823852cda17bbd65e70ca6
 [41]: http://commits.kde.org/kdenlive/854c1aef1a96ece1831048cef7ec837e0c410f24
 [42]: http://commits.kde.org/kdenlive/ee7a06a3ada559a82ad1505254b8ee817ca8dc04
 [43]: http://commits.kde.org/kdenlive/b0b13930d16402ba28ba90b987f4ef15c565094b
 [44]: http://commits.kde.org/kdenlive/8131671594ead2d318418359c0a1b7e175979fbd
