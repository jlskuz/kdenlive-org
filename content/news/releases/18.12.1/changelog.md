* Fix empty warning dialog on missing font in project. [Commit.](http://commits.kde.org/kdenlive/b112192abd33d438840f5631f4fd148431e3d96c) Fixes bug [#401903](https://bugs.kde.org/401903)
* Fix bin item description cannot be edited if it contains zone subclips. [Commit.](http://commits.kde.org/kdenlive/822d219c0691f13b657a22860a825a7b76c9b0a9) Fixes bug [#402817](https://bugs.kde.org/402817)
* Fix screengrab with audio broken. [Commit.](http://commits.kde.org/kdenlive/0839ebf4c410df0a60d3ac1f3cab8562e0c32351) 
* Move Gain effect to "Audio correction" category. [Commit.](http://commits.kde.org/kdenlive/a899240c6b15a235e750cad4d68cfcbf833b000e) 
* Ci: enable freebsd build. [Commit.](http://commits.kde.org/kdenlive/fb0a66b0793a03624cc1eb2b7bb8d2016a2f8df4) 
* QtScript is not used anymore. [Commit.](http://commits.kde.org/kdenlive/c91c8abb75896c02fcff81376ee8f96a8617992a) 
* Ci: add recipe for gitlab CI. [Commit.](http://commits.kde.org/kdenlive/3b1c2b8e5b8c45628c484ff376488d6a8e99a1aa) 
* Fix incorrect color theme correction for AppImages. [Commit.](http://commits.kde.org/kdenlive/540c8f8f9c8ba56b2ef738fdac9f38a6938515fa) 
* Fix color theme lost on AppImage. [Commit.](http://commits.kde.org/kdenlive/e2b499ff8be36a6102e9f234f2c666eda155bfb3) 
* Update AppData app version. [Commit.](http://commits.kde.org/kdenlive/a49facdf69d2596abd77996a01e369732586e0a6) 
* Fix bin/melt.exe & libmlt* loading on Windows. [Commit.](http://commits.kde.org/kdenlive/9b7b423f2acf4f7d2227f2ae70c3752bfb23e04c) 
* Necessary OpenGL headers are provided by Qt. [Commit.](http://commits.kde.org/kdenlive/2d3be73e68dbf965d4594c2e5c6a4fcfe936b608) 
* Fix keyframes import. [Commit.](http://commits.kde.org/kdenlive/94d2efb924730a98cfa25879ad85aa37578f46c8) 
