---
title: 18.12.1 released
author: Farid Abdelnour
date: 2019-01-10T21:03:07+00:00
aliases:
- /en/2019/01/18-12-1-released/
- /fr/2019/01/version-18-12-1/
---

The first dot release of the 18.12 series is out with fixes and usability improvements. The most exiting change is the fix for audio capture when recording from screen or webcam, a handy feature for people doing video tutorials.

![](screenshot.png)

You can now import keyframes to your effects and use them in other projects. On the usability front the "Gain" effect is now in the correct "Audio correction" category and theming issues in the AppImage are now fixed. Speaking of AppImage, we now have a fully automated build system ready so devs can focus on coding gain.

Don't forget to check our nightly refactoring branch version which received many fixes during the holidays and is ready for another round of testing.
