---
author: Farid Abdelnour
date: 2022-07-10T12:31:50+00:00
aliases:
- /en/2022/07/kdenlive-22-04-3-released/
- /it/2022/07/kdenlive-22-04-3-2/
- /de/2022/07/kdenlive-22-04-3/
---

The last maintenance release of the 22.04 series is out fixing issues with proxy clips, render panel parameters and timeline scrolling among other minor bugs. Oversized icons on Windows should be normal now and speech to text is working again in the Flatpak version. On Windows all downloads should working now (online resources, title templates, effects, render profile).
