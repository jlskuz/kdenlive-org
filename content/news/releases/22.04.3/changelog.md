  * Supplement to “Disable pip on Flatpak (we bundle the deps there)”. [Commit.][1]
  * Fix syntax error. [Commit.][2]
  * [Python Interface] Disable pip on Flatpak (we bundle the deps there). [Commit.][3]
  * Fix type (spotted by erjiang). [Commit.][4]
  * Fix effect parameter spin box incrementing twice on mouse wheel. [Commit.][5]
  * Fix compilation &#8211; wrong change committed. [Commit.][6]
  * Fix bug and warning calculating available mix duration when no frame is available. [Commit.][7]
  * [Scene Split] reimplement threshold. [Commit.][8]
  * Fix keyframe view seeking with effect zones. [Commit.][9]
  * Fix timeline playing autoscroll incorrectly enabled. [Commit.][10] See bug [#455512][11]
  * Fix timeline scrolling broken after opening a widget from timeline menu, like edit clip duration. [Commit.][12]
  * Fix oversized UI on Windows. [Commit.][13]
  * Fix incorrect encoding in rendered clip name on Windows. [Commit.][14] Fixes bug [#455286][15]
  * Fix incorrect ungroup when dragging selection. [Commit.][16]
  * Fix incorrect behavior of external proxies, allow multiple patterns by profile. [Commit.][17] See bug [#455140][18]
  * Fixes for external proxies. [Commit.][19] See bug [#455140][18]
  * Correctly enable current bin item proxy action after proxy is enabled/disabled in project settings. [Commit.][20]
  * Fix timeline cursor sometimes losing sync with wuler playhead. [Commit.][21]
  * Fix freeze copying proxy clips. [Commit.][22]
  * [Render Presets] Follow ffmpeg defaults. [Commit.][23]
  * Proper rounding for persistant quality slider value. [Commit.][24]
  * [Render Widget] Backend option to set speed default index. [Commit.][25]
  * [Render Widget] Don’t enable “Custom Quality” by default, remember state. [Commit.][26]
  * Fix compilation with Qt < 5.14. [Commit.][27]

 [1]: http://commits.kde.org/kdenlive/03170f45d2978fae72b45f6bb6efea072e10db6a
 [2]: http://commits.kde.org/kdenlive/1d82251684c0f37d6a9d3b78cc668b685aec5d9c
 [3]: http://commits.kde.org/kdenlive/13b46025711bbe74d47b6652e1f627905112d624
 [4]: http://commits.kde.org/kdenlive/e77e578e1f8298dce2319b3c88c94ff73e4337d1
 [5]: http://commits.kde.org/kdenlive/59a39a977d9057078a620da44227741186369c50
 [6]: http://commits.kde.org/kdenlive/5c9135c962854de074b494d264c3799c1b9e1cfd
 [7]: http://commits.kde.org/kdenlive/aefb8432540208bf9292b5819f4e596052338fac
 [8]: http://commits.kde.org/kdenlive/aa544462f2043155ee0f52fec02597d71b98d5f1
 [9]: http://commits.kde.org/kdenlive/2a44964b71949ce42eb6d4cd538a15ad7d5defe5
 [10]: http://commits.kde.org/kdenlive/388c8188b93209d3e5e01c05b07e2239f4fa627e
 [11]: https://bugs.kde.org/455512
 [12]: http://commits.kde.org/kdenlive/52e5f1bd33a6ad0e318bb333eb2d96cfb7df0310
 [13]: http://commits.kde.org/kdenlive/ba1cbea6838f0b17e3711719cec71c890d01757c
 [14]: http://commits.kde.org/kdenlive/ceb9ee08e1bcc28a206583d4c8874e89d57fc869
 [15]: https://bugs.kde.org/455286
 [16]: http://commits.kde.org/kdenlive/edede016f553f917740a192b06c2ed134333f19e
 [17]: http://commits.kde.org/kdenlive/3e871332c6cc6c3c849211f837c297fd90c3c39a
 [18]: https://bugs.kde.org/455140
 [19]: http://commits.kde.org/kdenlive/e5911772c142d0d0bc45c944ab743d93bb84f9e0
 [20]: http://commits.kde.org/kdenlive/5d7f592fba28a70054f4829c4d4aa5ccf4338cc4
 [21]: http://commits.kde.org/kdenlive/5af84df5e92e583c693e96a48546d4aa8f06ded9
 [22]: http://commits.kde.org/kdenlive/d05bb528afd1160f0be97f1c96ad5ccd27df0045
 [23]: http://commits.kde.org/kdenlive/a658c8052137d2fd4b5d9c08cc2593c57e88957d
 [24]: http://commits.kde.org/kdenlive/db6f817469e5b96f18f952e6066a060968a7a26c
 [25]: http://commits.kde.org/kdenlive/c10a0984ff145f5b2d4ecc397792cb0a66ff82e7
 [26]: http://commits.kde.org/kdenlive/7a54eea5ab7d928b1af1e1e3b13cc4bf5049d370
 [27]: http://commits.kde.org/kdenlive/4b087384a2718a17f7c643554cfdf5fe549f4521
