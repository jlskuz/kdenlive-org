* [Bug 346290: Creating audio thumbnail notification misleading](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=346290)
* [Bug 346489: 15.11.80 - "Loop Zone" playback option in Clip &amp; Project Monitors don't work -- stops playback at out point](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=346489)
* [Bug 348084: cant reattach screen after maximizing viewer window](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=348084)
* [Bug 348557: crashes after expanding the folders in the bin](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=348557)
* [Bug 348970: git master 2015-06-09 - Zone in Clip Monitor not working properly](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=348970)
* [Bug 350635: Cannot edit audio clip if it contains image inside](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=350635)
* [Bug 351065: can't add clips](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=351065)
* [Bug 351695: missing icons](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=351695)
* [Bug 352670: 15.09 - SIGABRT when trying to create renderprofile in favourites](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352670)
* [Bug 352815: 15.09.0, Right-clicking clips in timeline shows "Clip in Project Tree" -- should be renamed to Project Bin?](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352815)
* [Bug 352847: 15.09.0, Audio signal title bar does not stay hidden after restarting Kdenlive](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352847)
* [Bug 352853: 15.09.0, "Zoom Level" bar at bottom inverts scroll-wheel direction when zooming. Not congruent.](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352853)
* [Bug 352884: 15.09.0, Project Bin search field is case-sensitive. Would recommend it not be.](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352884)
* [Bug 353022: SUGGESTION: 15.11.80 - Consider renaming "Blue screen" key effect in effect list](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353022)
* [Bug 353070: 15.09.0, Switching between clips in project bin and timeline doesn't cause effect stack to switch properly](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353070)
* [Bug 353097: 15.11.80 - When clip or project monitor reach end of playback, play/pause button still displays pause icon, not play](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353097)
* [Bug 353284: 15.11.80 - Can't rename folder in Project Bin after adding clips into it. (Video Example included)](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353284)
* [Bug 353455: Unable to export image sequence due to wrong symbol interpretation](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353455)
* [Bug 353561: git master 2015-10-05 - Dissolve not working.](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353561)
* [Bug 353765: Drag &amp; drop few selected clips from project tree to timeline produce wrong tracks layout](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353765)
* [Bug 354553: Guides are not permanently deleted](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=354553)
* [Bug 354601: Project rendering always uses Proxy clips](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=354601)
* [Bug 355144: Razor (Cut/Scissors/X) tool cuts clips on right mouse button click](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355144)
* [Bug 355149: Certain zoom levels make timeline labels overlap and unreadable](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355149)
* [Bug 355151: Selecting active Project Bin clip doesn't update Effect Stack if clip in Timeline selected before](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355151)
* [Bug 355252: Deleting track removes all tracks' icons](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355252)
* [Bug 355367: Fast copy-paste of clips in the timeline crashes Kdenlive](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355367)
* [Bug 355381: Applications/15.12.0: transition selection parameters show wrong track](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355381)
* [Bug 355383: Applications/15.08-15.12: renaming track does not update transitions' track list.](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355383)
* [Bug 355572: [Typo] Expland → Expand](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355572)
* [Bug 355740: Feature Req: Home/End with selected transition reposition playhead](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355740)
* [Bug 356029: 15.11.80: deleting track switches next upper/lower track to automatic composite](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356029)
* [Bug 356032: 15.11.80: trying to editing track name moves it out of view](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356032)
* [Bug 356041: 15.11.80: bin folder context menu lacks "change folder name" action](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356041)
* [Bug 356087: 15.11.80: extract frame does overwrite existing files without asking back](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356087)
* [Bug 356100: Image Sequence is default Render profile](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356100)
* [Bug 356187: icons missing in 15.11.80](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356187)
* [Bug 356300: Moving multiple clips in timeline vertically is recorded incorrectly for undo history (undo will move in wrong direction)](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356300)
* [Bug 356310: Deleting all tracks possible and crashes Kdenlive](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356310)
* [Bug 356313: Deleting empty track while two clips in timeline are grouped causes delayed crash](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356313)
* [Bug 356316: 15.11.90: split view button does not update clip view when last action was in timeline](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356316)
* [Bug 350061: Fails to build on ubuntu armhf (OpenGL != OpenGLES)](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=350061)
* [Bug 352673: Titles with keyframeable filters (e.g. composite) show up too early](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=352673)
* [Bug 353024: 15.09.0, Can't get any transitions to work in timeline with "composite" function enabled in tracks.](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353024)
* [Bug 353260: /usr/share/mlt/profiles/ is wrong on Opensuse Tumbleweed](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=353260)
* [Bug 354548: Unable to add own wipe files (lumas) in 15.08.1](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=354548)
* [Bug 354806: kdenlive-15.08.1 Composite transition  doesn't remember empty wipe file setting](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=354806)
* [Bug 355285: Freeze when adding Corners effect](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355285)
* [Bug 355379: Applications/15.12.0: transition track settings get messed up when loading 15.08 project](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=355379)
* [Bug 356200: Composite Transition will use first-selected wipe file even if "none" or different image selected](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356200)
* [Bug 356209: Copying Effects messes up parameters (Fade from black + Fade in)](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356209)
* [Bug 356244: Track Effect (Normailze) forgotten after saving + loading project](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356244)
* [Bug 356345: Buggy interaction between "Fade from black" and "Fade in" effect: the second one will have incorrect duration if clip was cut on left side](https://web.archive.org/web/20160311001550/https://bugs.kde.org/show_bug.cgi?id=356345)
