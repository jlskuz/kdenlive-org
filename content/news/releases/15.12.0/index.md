---
title:  Kdenlive 15.12.0 information page 
date: 2015-12-16
aliases:
- /node/9451
- /discover/15.12.0
# Ported from https://web.archive.org/web/20160311001550/https://kdenlive.org/node/9451
# Ported from https://web.archive.org/web/20160304185838/https://kdenlive.org/discover/15.12.0
---

Kdenlive 15.12.0 was released with [KDE Applications 15.12.0][1] in December 2015.

For this release, we focused on stability and workflow improvments. We also continued the code refactoring and cleanup, making Kdenlive easier to maintain.

![](kdenlive-15-12-0.png)

## Several features have also been implemented:

* Option to quickly disable/enable all Bin or all Timeline effects
* New "Save selection" feature allowing to save part of a timeline as a .mlt playlist clip. That clip can later be imported in another project and expanded so that it acts like a "copy/paste" of a clip group between projects
* Track transparency can now be disabled/enabled in the track headers
* Improved effects workflow with favorite effects

Source packages can be downloaded from [KDE's servers][2] and binary packages should hopefully be prepared by distributions soon. 

  [1]: https://kde.org/de/announcements/applications/15.12.0/
  [2]: https://download.kde.org/stable/applications/15.12.0/src/
