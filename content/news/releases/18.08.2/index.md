---
author: Farid Abdelnour
date: 2018-10-12T17:37:37+00:00
aliases:
- /en/2018/10/kdenlive-18-08-2-released/
- /fr/2018/10/version-18-08-2-kdenlive/
- /it/2018/10/kdenlive-18-08-2-scaricabile/
---

Kdenlive 18.08.2 is out bringing usability improvements and a crash fix. The Windows version is also becoming more stable with every release and this version brings fixes to the translation installation and the introduction of a crash report.

In other news, the Refactoring is moving steadily ahead and we will release a wider test beta version soon, stay tuned. Also the refactoring branch is now building automatically on KDE's automated integration system (CI), and all the regressions tests pass. This means that after each change to the source code, the CI will run the tests to check that no regression happens. On the sysadmin front we are cleaning up our bug tracker in preparation for the 18.12 release.

## Get it:

* [AppImage][1]
* [Windows][2]


 [1]: https://files.kde.org/kdenlive/release/kdenlive-18.08.2-x86_64.AppImage
 [2]: https://download.kde.org/Attic/kdenlive/18.08/windows/Kdenlive-18.08.2-w64.7z
