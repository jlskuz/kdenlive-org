---
title: Kdenlive 23.08 beta available
author: Jean-Baptiste Mardelle
date: 2023-07-30T15:17:38+00:00
aliases:
- /en/2023/07/kdenlive-23-08-beta-available/
- /fr/2023/07/kdenlive-beta-disponible/
- /it/2023/07/kdenlive-23-08-beta-disponibile/
- /de/2023/07/kdenlive-23-08-beta/
discourse_topic_id: 3332
pre_downloads:
- platform: windows
  sources:
  - name: Installable
    url: https://files.kde.org/kdenlive/unstable/kdenlive-23.08-beta1-windows-gcc-x86_64.exe
- platform: linux
  sources:
  - name: AppImage
    url: https://files.kde.org/kdenlive/unstable/kdenlive-23.08-beta1-linux-gcc-x86_64.AppImage
---
After an inspiring participation to [Akademy][1], where we presented some of the actions we want to take to improve Kdenlive's stability and reliability, we are announcing the first beta version for the upcoming Kdenlive 23.08.0 version.

The Kdenlive 23.04.x version was unfortunately affected by major regressions related to the new timeline nesting feature.

We are now working on major improvements to our test suite pipeline to ensure such things don't get unnoticed before a release. In the meantime, all major issues related to nesting, as well as many other bugs, are now fixed in this new beta and we encourage all interested users to test this version to ensure we have the best possible experience for the final release.

 [1]: https://akademy.kde.org/2023/
