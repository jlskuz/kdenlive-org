---
author: Farid Abdelnour
date: 2020-10-12T04:30:08+00:00
aliases:
- /en/2020/10/kdenlive-20-08-2-released/
- /it/2020/10/kdenlive-versione-20-08-2/
- /de/2020/10/kdenlive-20-08-2-ist-freigegeben/
---
Usually the point releases are for bugfixes but Kdenlive 20.08.2 comes with a set of changes worthy of a major release. Besides some memory leak fixes and usability improvements this versions brings back the automatic scene split feature, adds for the Linux version experimental GPU rendering profiles for rendering, proxy creation and timeline preview rendering (Windows will follow at a later stage) and a new crop effect. Other noteworthy changes are better handling of projects with missing clips, improved project loading and fixes incorrect volume meters in audio mixer. The Windows version gets 45 new audio LADSPA effects (CMT) and 3 video frei0r effects (Cairo).

But the best thing really is that this release has many commits from new contributors. Come join the effort in making the best FLOSS video editor in the world. 🙂

## Automatic Scene Split

The Automatic scene split features allows you to detect different scenes of your clip and either split them into subclips or add markers to it. You can access this feature by right clicking on a clip in the project bin then go to clip jobs

![](Screenshot_20201009_150316.png) 

![](Screenshot_20201009_174248.png)

## Crop By Padding Filter

![](crop.gif)
