---
title: Kdenlive 20.12.3 is out
author: Farid Abdelnour
date: 2021-03-08T09:30:09+00:00
aliases:
- /en/2021/03/kdenlive-20-12-3-is-out/
- /it/2021/03/kdenlive-20-12-3-pronto/
- /de/2021/03/kdenlive-20-12-3-ist-da/
---

The last maintenance release of the 20.12 series is out with the usual batch of usability and bug fixes. The highlights include lots of polishing of the Subtitling Tool and adding a spell checking feature. The Titler also got a fair amount of usability improvements most notably fixing the invisible text cursor. Fixes were also made to the chroma key color picker and various clip selection issues. The Windows version received fixes to resetting the config file and finding downloaded title templates and lumas.
