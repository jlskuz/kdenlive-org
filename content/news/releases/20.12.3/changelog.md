  * Remove some debug output. [Commit.][1]
  * Fix various selection issues. [Commit.][2]
  * Fix recursive search broken on cancel. [Commit.][3] Fixes bug [#433773][4]
  * Change shortcut for Loop Zone to avoid conflict with windows system. [Commit.][5]
  * Comment out recent lost timeline focus that made things worse. [Commit.][6]
  * Improve focus handling when switching to fullscreen monitor. [Commit.][7]
  * Do not allow keyframe edit if keyframes are hidden. [Commit.][8]
  * Ensure we use an UTF-8 encoding for markers. [Commit.][9] See bug [#433615][10]
  * Don’t mark document modified when opening a project file with guides. [Commit.][11] See bug [#433615][10]
  * Fix animated param view when keyframes are hidden. [Commit.][12]
  * Make timeline tracks separator slightly more visible. [Commit.][13]
  * When focusing the app, ensure we have the correct timeline item under mouse referenced. [Commit.][14]
  * Apply !171 (typewriter effect in latest MLT, only for title clips…). [Commit.][15]
  * Titler: update tab order. [Commit.][16] Fixes bug [#433590][17]
  * Keyframes: Shift+drag now allows selecting last keyframe, fix corruption / crash on group keyframe move. [Commit.][18]
  * Transcode job: don’t silently overwrite exported files. [Commit.][19] Fixes bug [#433623][20]
  * Don’t enforce profile width multiple of 8. [Commit.][21]
  * Profile width in MLT can be a multiple of 2, not 8. [Commit.][22]
  * Don’t rebuild existing audio thumbs for playlist on project opening, display playlist audio thumbs in clip monitor. [Commit.][23]
  * Fix subtitle selection by keyboard shortcut. [Commit.][24]
  * Subtitle edit: switch to KTextEdit so we get spell check. [Commit.][25]
  * Automatically highlight text for editing when adding a subtitle. [Commit.][26]
  * FIx possible crash on subtitle resize, and allow cutting unselected subtitle. [Commit.][27]
  * Allow keyboard grab of subtitles. [Commit.][28]
  * Do not allow zero for values of a project profile (framrate, framesize,…). [Commit.][29] Fixes bug [#432016][30]
  * Remove “Create Region” menu item (not re-implemented yet) #82. [Commit.][31]
  * Titler: show correct outline color on first use. [Commit.][32]
  * Fix color picker corruption. [Commit.][33]
  * Hide keyframe mode for rotoscoping (only linear supported). [Commit.][34]
  * Subtitles: fix crash on “select clip” shortcut. [Commit.][35]
  * Fix reset config on windows #931. [Commit.][36]
  * Expanded track tag width only if >10 audio OR video tracks, not sum of. [Commit.][37]
  * Fix downloaded template titles and lumas not found on Windows. [Commit.][38]
  * Keep title text item editable if even if it is empty. [Commit.][39]
  * Fix invisible text cursor in title editor #165 and other minor tweaks. [Commit.][40] Fixes bug [#403941][41]. Fixes bug [#407497][42]
  * Fix subtitle text not updated on go to next/prev and related crash. [Commit.][43]

 [1]: http://commits.kde.org/kdenlive/d948801db962bac3e751bff3a6ed2728d8b63c80
 [2]: http://commits.kde.org/kdenlive/5297ef5953a26aaa41d43e54a6ebbbef1a7f2352
 [3]: http://commits.kde.org/kdenlive/c49d68af077321d097af5e67c9b240ed93292f4c
 [4]: https://bugs.kde.org/433773
 [5]: http://commits.kde.org/kdenlive/44a9f4d8842a549ce3377e179ea0e4b32184d01a
 [6]: http://commits.kde.org/kdenlive/3fe09bd05f2de3573c9aa5b67d077e37db7d43d7
 [7]: http://commits.kde.org/kdenlive/728d2a4ac0386fbc2dab6a643132211c75f893d6
 [8]: http://commits.kde.org/kdenlive/ed8eaeef4e6f4bfe9b7cbf68216cdabdc58d6a57
 [9]: http://commits.kde.org/kdenlive/4c8ef3d52ae4d1dccb0697dfa7d0099b84f5c6c1
 [10]: https://bugs.kde.org/433615
 [11]: http://commits.kde.org/kdenlive/74fb541eadcfff7da405efc71a7bacc8565d576a
 [12]: http://commits.kde.org/kdenlive/c7b453ae2b4efd7f09b81d78c645464456d49b77
 [13]: http://commits.kde.org/kdenlive/dc9e361318a2ad99324482dd4859355e9dfd86cc
 [14]: http://commits.kde.org/kdenlive/eaea42c6a73aa37cff48afd48a7a9923d3628761
 [15]: http://commits.kde.org/kdenlive/b221dba585eaa4b88b2767fe1eefae32a1a627c6
 [16]: http://commits.kde.org/kdenlive/6e6f36e942dd0a710eaa84046115c596725a030b
 [17]: https://bugs.kde.org/433590
 [18]: http://commits.kde.org/kdenlive/4be760b98c93cf0813fe464403d4170fc9349a67
 [19]: http://commits.kde.org/kdenlive/f1cd744c5237f890bd779f37f73031ed38959199
 [20]: https://bugs.kde.org/433623
 [21]: http://commits.kde.org/kdenlive/32c27894850512ab3e195ef1904317b3d730ff27
 [22]: http://commits.kde.org/kdenlive/c320c2f1184abcf05cf3389a1c0ce27ce1f60a86
 [23]: http://commits.kde.org/kdenlive/84e2b4ee38d59d72f35402fe82a071be8610af2b
 [24]: http://commits.kde.org/kdenlive/c661c68031d7510f203d36b005ef586aca5b31fe
 [25]: http://commits.kde.org/kdenlive/5026ce2b3c8230b57b4ed0e3ae946b957dff6613
 [26]: http://commits.kde.org/kdenlive/ee13f34ba7525a4bc0ef448e94740ca578803466
 [27]: http://commits.kde.org/kdenlive/8e417866b8229fbfc2dc7b6d81e9c2b4316e96e8
 [28]: http://commits.kde.org/kdenlive/5c13969815b0820b2fca96b3a6db8116ef0cc729
 [29]: http://commits.kde.org/kdenlive/303c2471de0ff0eb463bd3edfce30680b307801b
 [30]: https://bugs.kde.org/432016
 [31]: http://commits.kde.org/kdenlive/3081a236883f9b752f667bbbbfd57b4903fa962d
 [32]: http://commits.kde.org/kdenlive/3f496c2a92ce07f2848a757ab2ce8c73f9d23fb2
 [33]: http://commits.kde.org/kdenlive/52a4aeabf89d98a653de5b4edd6e8f0a31abc03f
 [34]: http://commits.kde.org/kdenlive/9b015768b6e66e7b091f245fe7211e2192598460
 [35]: http://commits.kde.org/kdenlive/095b5ca9f8b38fb08f411a6707ed4ec4c37c8e32
 [36]: http://commits.kde.org/kdenlive/d1e71017b08fd9dafbc876288903ca1454b87118
 [37]: http://commits.kde.org/kdenlive/ed751688dbd42f3298a99b73eb9aa991a3a70ca3
 [38]: http://commits.kde.org/kdenlive/a322674079551ce1bf3a34261c9da8c0fffebb0f
 [39]: http://commits.kde.org/kdenlive/7f303be53fac4793c820f893a71e8eb961e8f98c
 [40]: http://commits.kde.org/kdenlive/1e02108e265207ff9faf432c1bdc4938796a05a9
 [41]: https://bugs.kde.org/403941
 [42]: https://bugs.kde.org/407497
 [43]: http://commits.kde.org/kdenlive/86218927c7b1a3994ccad7d01f495174c466d424
