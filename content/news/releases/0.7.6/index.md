---
author: Jean-Baptiste Mardelle
date: 2009-10-08T20:30:00+00:00
aliases:
- /users/j-b-m/kdenlive-076-released
- /discover/0.7.6
# Ported from https://web.archive.org/web/20160324012957/https://kdenlive.org/users/j-b-m/kdenlive-076-released
# Ported from https://web.archive.org/web/20100814233200/http://kdenlive.org/discover/0.7.6
---


We are glad to announce the release of Kdenlive 0.7.6, 3 months after the 0.7.5 version.

Many crashes related to effects, transitions, and timeline corruption issues were fixed, and the latest MLT version (0.4.6) that is used for this Kdenlive release also provides an improved stability, so we encourage all our users to upgrade.

The list of fixed issues can be found on our bugtracker: [http://www.kdenlive.org/mantis/changelog\_page.php?version\_id=11][1]

The new features are:

* Title module: rewrite, now allows for basic animation (title zoom & scroll)
* Track rename: users can now rename tracks
* Composite transition: keyframes can now be moved
* Clip management: image and audio clips are automatically monitored and updated in the timeline whenever they change on disk
* User interface cleanup: cleaner look for timeline
* Capture monitor improvements: now shows available disk space & timecode, also allows to choose a name for captured files
* Project management: project settings dialog now allows you to clear the thumbnails cache and delete all videoclips that are not used in the project
* Improved clip markers: they are now displayed in the clip monitor ruler, and user can easily go to each marker from the context menu
* Shutdown computer after rendering
* Mac OS X support (Dan Dennedy)

As usual, you can download the Kdenlive sources from [our sourceforge page][2] (see [compilation instructions][3]), updated packages for your distros should be available in a few days.

We hope you will enjoy this new release and continue to give us feedback in the forums and bugtracker so that we can improve Kdenlive.

For the Kdenlive team:  
Jean-Baptiste Mardelle

  [1]: https://web.archive.org/web/20160324012957/http://www.kdenlive.org/mantis/changelog_page.php?version_id=11
  [2]: https://sourceforge.net/project/platformdownload.php?group_id=46786
  [3]: https://web.archive.org/web/20160324012957/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive/installing-source
