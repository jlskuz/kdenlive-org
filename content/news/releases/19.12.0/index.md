---
title: Kdenlive 19.12 is out
author: Farid Abdelnour
date: 2019-12-19T05:00:01+00:00
aliases:
- /en/2019/12/kdenlive-19-12-0-is-out/
- /fr/2019/12/kdenlive-19-12-est-sorti/
- /it/2019/12/e-uscito-kdenlive-19-12/
- /de/2019/12/kdenlive-19-12-ist-freigegeben/
---
![Screenshot of Kdenlive version 19.12.0](screenshot.png)

After four months of intense work and more than 200 commits, Jean-Baptiste Mardelle and the Kdenlive team are happy to announce the release of Kdenlive 19.12.0. This release comes with many changes under the hood, new features and nifty eye candy additions. The highlights include huge performance improvements resulting in a faster and smoother timeline, a new audio mixer, master effects (audio/video), and better audio waveform display to name a few. Not to mention the usual round of stability and usability fixes.

Grab the latest version from the [download section][1] and give it a spin.

## Performance

  * Improved timeline responsiveness.
  * Fixes to timeline memory consumption.
  * Improvements to clip handling and caching.
  * Fine tune rendering threads settings for faster rendering.
  * Fixed lag when adding compositions.


## Highlights

### Audiomixer

New audio mixer with mute, solo and record functions.

![](mixer.gif) 

### Master effects

Added ability to apply audio or video effects to all tracks. (Click the "Master" button above track headers to see master effect stack.)

![](master.gif) 

### Audio waveform

Improvements to audio waveform display in the Project Bin and the Clip Monitor.

![](audio-waveforms.png)

## Effects and Compositions

  * Re-implemented ability to switch between composition types by scrolling the mouse wheel.
  ![](scroll-compos.gif)

  * Allow inserting values in Lift/Gamma/Gain effect.
  ![](lgg-insert.gif)

  * Improved interface of color wheels and bezier curves.
  * Custom filters work again.
  ![](customeffect.gif)

  * Always display search bar.
  * Clean-up and fix many buggy effects.
  * Fix favorite compositions broken.
  * Removed confusing favorites folder from the effects list.
  * Fix broken split effect comparison.

![](compare-effect.gif)

## Windows

  * Fix UI corruption on Windows/Intel drivers.
  * In settings menu: Add menu on Windows to switch between OpenGL modes.
  * Fixed screengrab.

## General

  * Allow seeking on clip monitor audio thumbnail.
  * Monitor overlay: add button to move to opposite corner.
    ![](toolbar.gif)
  * Fix title clip created with incorrect duration.
  * Fix showing full-screen on dual monitor setup.
  * Fix detection of secondary screen for monitor full-screen.
  * Better audio clip colors in timeline.
  * Improve visibility of audio/video only drag icons in clip monitor.
  * Always overlay audio waveform in monitor for audio clips.
  * Fix screengrab crash.


 [1]: https://kdenlive.org/en/download/
