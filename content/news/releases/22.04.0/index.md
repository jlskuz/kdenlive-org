---
author: Farid Abdelnour
date: 2022-05-02T13:44:48+00:00
aliases:
- /en/2022/05/kdenlive-22-04-released/
- /fr/2022/05/kdenlive-22-04-2/
- /it/2022/05/kdenlive-versione-22-04/
- /de/2022/05/kdenlive-22-04/
featured_image_bg: images/features/2204.png
---
The Kdenlive team is happy to announce the release of version 22.04. This development cycle comes with more than 300 commits, mostly focused on stability and polishing, ranging from packaging all the way up to user interface enhancements.

Besides the improvements to the Windows and macOS versions, Kdenlive is now runs on Apple's M1 architecture and includes initial support for full range 10-bit color on all platforms — although note that 10-bit color does not work with effects yet. Kdenlive also automatically offers to transcode variable frame-rate videos to an editing-friendly format, and some filters, like Blur, Lift/Gama/Gain, Vignette and Mirror, are now slice threaded, which improves rendering speeds.

Encouraging support among users is one of our priorities and that's why Kdenlive introduces Effect Templates in version 22.04. Effect Templates are custom effects that can be shared with other community members through the [KDE Store][1] and can be downloaded directly into Kdenlive. The store is already open and you can contribute your effects too!

The speech recognition interface got improvements to the highlight color of selected text, font size and it has been appropriately renamed to Speech Editor. Other changes include HighDPI and low resolution screen support, improved OpenTimelineIO handling, ASS subtitle fixes and added CR2, ARW and JP2 image formats.

The render dialog received an interface rewrite, drastically improving usability while giving the user more power by adding a new custom profile creation interface.

![](renderWindow.png)
Improved render window

![](presetWindow.png)
New preset creation window

Another highlight is the ability to render multiple videos by zones using the timeline guides.

![](multiguide.png)

The Icon View mode in the Project Bin received a major facelift as well.

{{< img-comparison-slider before="bin-comp-before.png" after="bin-comp-after.png" >}}

 [1]: https://store.kde.org/browse?cat=686&ord=latest
