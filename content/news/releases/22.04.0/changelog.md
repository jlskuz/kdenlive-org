  * Ensure audio target track is remembered if we click on a bin clip with the same number of audio streams. [Commit.][2] See bug [#452495][3]
  * Fix delete render preset on windows not working. [Commit.][4]
  * Remove text label. [Commit.][5]
  * Fix show clip properties action always disabled. [Commit.][6]
  * Fix tests. [Commit.][7]
  * Embed time remap ui in a scrollview so that Kdenlive can be used on small resolution (1024&#215;768). [Commit.][8]
  * Fix speech editor using large horizontal spacing when selected bin clip has long name. [Commit.][9]
  * Fix timeline clip selection broken after a move undo operation. [Commit.][10]
  * Enable highdpi. [Commit.][11]
  * Mac OS: fix access to microphone. [Commit.][12]
  * Fix monitor image size with non integer screen scaling. [Commit.][13]
  * Fix app focus lost on Windows when exiting monitor fullscreen. [Commit.][14]
  * Switch from QQuickView to QQuickWidget &#8211; fixes broken playback on Mac OS. [Commit.][15]
  * Fix several cases of timeline losing focus. [Commit.][16]
  * Correctly update “apply” button on monitor change. [Commit.][17]
  * Make monitor detection more robust for fullscreen mode. [Commit.][18]
  * Fix resetting effect does not clear timeline keyframe view, resulting in possible crash. [Commit.][19]
  * Don’t propose rtaudio backend if not available. [Commit.][20]
  * Fix layout warning. [Commit.][21]
  * Fix play zone seeking to first frame of timeline. [Commit.][22]
  * Fix import keyframes importing outside clip out. [Commit.][23]
  * Remove confusing “autorotate” checkbox in transcode to edit friendly. [Commit.][24]
  * Code quality fixes. [Commit.][25]
  * Fix fullscreen monitor selection doesn’t work on Windows. [Commit.][26]
  * Fix possible crash on exit. [Commit.][27]
  * Don’t query producer length on each frame. [Commit.][28]
  * Fix speed not saved in custom render profiles. [Commit.][29]
  * Code quality fixes. [Commit.][30]
  * Ensure we use the breeze widget style on first run in Mac. [Commit.][31]
  * Make progress bar for effects more visible (for ex. in motion tracker). [Commit.][32]
  * Fix project duration not updated on bin clip deletion. [Commit.][33]
  * Fix timeline focus issues on drag & drop, fix mouse position and project duration in timeline toolbar not consistently updated. [Commit.][34]
  * Fix no speech end time in analysed speech. [Commit.][35]
  * Ignore audio files album art. [Commit.][36]
  * Fix typo (missing space). [Commit.][37]
  * Fix last silence analysis in speech to text, small drawing fixes. [Commit.][38]
  * Fix creating guides from project notes. [Commit.][39]
  * Fix line feed lost on project notes paste. [Commit.][40]
  * Add invert param to luma mix to allow reversing direction of transition. [Commit.][41]
  * Only save bin thumbnail on project save to avoid displaying incorrect thumb after unsaved project change. [Commit.][42]
  * Fix freeze on add clip recently introduced. [Commit.][43]
  * Fix thumbnail cache bug causing incorrect thumbs to sometimes display after saving project. [Commit.][44]
  * Speech to text: cleaner html output for project files, fix work selection not really working. [Commit.][45]
  * Fix foxus issue on effect drop. [Commit.][46]
  * Smaller drag image in bin, also show it in icon view mode. [Commit.][47]
  * Fix startup warning. [Commit.][48]
  * Fix timeline focus issues on drag and drop. [Commit.][49]
  * Fix timeline scrolling below zero and timeline drag/drop bug. [Commit.][50]
  * [Renderer] Fix wrongly inverted logic to hide “Generate Script” button. [Commit.][51]
  * Fixes for saving and editing render presets. [Commit.][52]
  * [Render Presets] GOP and B-Frames params: enable only if it makes sense. [Commit.][53]
  * Fix possible crash in bin when selecting a clip. [Commit.][54]
  * [Renderer] Reset DAR to 1:1 on resolution override to avoid errors. [Commit.][55]
  * [Render Presets] Combo Box for PAR to prevent render errors. [Commit.][56]
  * Fix rubberband selection on scroll, and don’t overlap track headers, fix move clip + mouse wheel. [Commit.][57] Fixes bug [#417209][58]
  * Fix razor tool misbehaviour on subtitle track. [Commit.][59]
  * Fix keymap info on subtitle track. [Commit.][60]
  * Fix timecode rounding for fps like 23.98. [Commit.][61] Fixes bug [#435213][62]
  * Fix double warning and duplicate folder on manage cache data deletion. [Commit.][63] Fixes bug [#434754][64]
  * Keep focus on clip monitor after editing marker. [Commit.][65] Fixes bug [#433595][66]
  * Right click on a timeline guide seeks to its position and shows context menu. [Commit.][67] Fixes bug [#441014][68]
  * Fix editing title/color clips does not invalidate timeline preview. [Commit.][69] Fixes bug [#437427][70]
  * Titler: remember and restore last used text alignment. [Commit.][71] See bug [#413572][72]
  * Bin: tree view: hide audio/video icons for audio or video only clips. icon view: hide audio/video icons depending on zoom level, fix tag color covering thumb on drop. [Commit.][73]
  * [Render Widget] Prettify by removing some frames. [Commit.][74]
  * [Render Presets] Always disable “Scanning” properly if “Progressive”. [Commit.][75]
  * [Renderer] Fix preset gets deleted if saving is canceled. [Commit.][76]
  * Fix selection in bin icon view. [Commit.][77]
  * Fix previous commit (inverted logic). [Commit.][78]
  * Fix extract frame using proxy clips. [Commit.][79]
  * Fix “Clip” menu not properly update. [Commit.][80]
  * Fix regression “Extract Audio” always hidden. [Commit.][81]
  * [Render Widget] Fix threads param not updated. [Commit.][82]
  * Make audio/video usage icons more visible. [Commit.][83]
  * Spot remover, start with a small zone, not full screen. [Commit.][84]
  * Fix crash and corruption (disappearing effect) when dropping a clip close to 0 in timeline. [Commit.][85]
  * Fix open clip in bin when multiple bins and in icon view. [Commit.][86]
  * Multiple fixes for bin. [Commit.][87]
  * Improve tooltip on mix enter/exit/resize, display number of selected clips and duration in status bar. [Commit.][88]
  * Rename “Text edit” widget to more descriptive “Speech editor”. [Commit.][89]
  * [Renderer] Fix: quality slider was reversed. [Commit.][90]
  * Fix minor typos. [Commit.][91]
  * [Render Presets] Fix: “channels” has a UI control. [Commit.][92]
  * [Render] Don’t allow to set rate control for pcm_ audio codecs. [Commit.][93]
  * Fix crash and corruption on mix resize, add tests. [Commit.][94]
  * Turn off seek on add effect on first start. [Commit.][95]
  * Fix reset tracker effect incorrectly moves analyse rectangle. [Commit.][96]
  * Proper approach to set default render preset. [Commit.][97]
  * Fix project storage folder incorrectly stored, resulting on modified project on open. [Commit.][98]
  * [Render Widget] Refactoring and feature extension. [Commit.][99] Fixes bug [#415610][100]
  * Fix clip task sometimes not performed because of older pending task. [Commit.][101]
  * Merge disable autorotate and force rotation on same line in clip properties, fix disable video stream. [Commit.][102]
  * Fix transcoding to correctly adjust to audio/video only transcode when required, adjust button text to inform user. [Commit.][103]
  * Fix effect stack cleared on clip copy. [Commit.][104]
  * Fix defects. [Commit.][105]
  * Keyframe fixes: fix keyframe view not updated on clip resize and timeline position in keyframe view not updated on clip move. [Commit.][106]
  * Fix timeline preview invalidated by composition on project load. [Commit.][107]
  * Add option to seek to clip on add effect (enabled by default, can be disabled from Kdenlive Settings > timeline). [Commit.][108]
  * Slideshow clips: display image duration and count in clip properties. [Commit.][109]
  * Slideshow animations now use external config file instead of hardcoded, remember last image type. [Commit.][110]
  * Increase the flexablilty of importing captions. See issue #1361. [Commit.][111]
  * Unbreak slideshow animation. Default animation are still quite bad, we could improve or allow custom. [Commit.][112] See bug [#438880][113]
  * Fix regression: edit clip always disabled. [Commit.][114]
  * Fix concurrency issue breaking effect parameters. [Commit.][115]
  * Fix mouse wheel behavior on effect parameters. [Commit.][116]
  * Fix guides can be move to position < 0. [Commit.][117]
  * Don’t allow editing of current profile to prevent crashes. [Commit.][118]
  * Improvements for field order option. [Commit.][119]
  * [Project Settings] Fix: open selected profile instead of first. [Commit.][120]
  * Seperate HTML and translatable strings. [Commit.][121]
  * Add field order option to project settings. [Commit.][122]
  * Move audio scrub confif from Kdenlive Settings to Monitor menu, and make it possible with a shortcut. [Commit.][123]
  * USe not deprecated KDE\_INSTALL\_PLUGINDIR variable. [Commit.][124]
  * Ensure timeline clips are reloaded on change. [Commit.][125]
  * Fix rotation param cannot be copied between keyframes. [Commit.][126]
  * Ensure monitors are raised on click in bin or timeline. [Commit.][127]
  * Fix “Locate Clip” always hidden. [Commit.][128]
  * Don’t allow creating keyframe outside clip after resizing. [Commit.][129]
  * On project load, ensure reverse mixes are correctly detected. [Commit.][130]
  * Fix timeline thumbs when changing speed of clip. [Commit.][131]
  * Fix timeline video thumbs on clips with speed effect. [Commit.][132]
  * Fix same track transition resize glitch. [Commit.][133]
  * Don’t attempt to proxy mp3 clips with cover art. [Commit.][134]
  * [Project Profile Editor] Face lift and refactoring. [Commit.][135]
  * Fix audio thumbs of reversed clips. [Commit.][136]
  * Fix timeline broken in previous commit. [Commit.][137]
  * Fix timeline audio thumbs sometimes not correcty updating. [Commit.][138]
  * Transcoding: display info for files requiring transcoding, improve handling of audio only/video only files. [Commit.][139]
  * Transcode to edit friendly format: handle audio only and video only clips. [Commit.][140]
  * Replace the “Transcode…” option from bin context menu and replace with the option to transcode to an edit friendly format. [Commit.][141]
  * Follow MLT consumer properties renaming. [Commit.][142]
  * Validate 3D LUT CUBE files to prevent crashing in FFmpeg filter. [Commit.][143]
  * Fix tags corrupting audio/video icons in bin. [Commit.][144]
  * Fix audio thumb speed not correctly initialized (broke monitor thumbs). [Commit.][145]
  * Fix audio thumbs for clips with speed effect. [Commit.][146]
  * Fix crash cutting a grouped subtitle. [Commit.][147]
  * [Code Gardening] Use qRound instead of +0.5. [Commit.][148]
  * Fix 1 pixel offset at some zoom factors. [Commit.][149]
  * Fix build with KDE Frameworks < 5.77. [Commit.][150]
  * [Effect Stack] Indicate position on drag&drop. [Commit.][151]
  * Correctly update add/remove keyframe button on undo/redo and active keyframe on speed change. [Commit.][152]
  * Fix possible crash in extract zone. [Commit.][153]
  * Fix thumbnails for playlist clips having a different resolution than project profile. [Commit.][154]
  * Fix clip target tracks and properties not properly set for first clip when creating a new project. [Commit.][155]
  * Fix header comment. [Commit.][156]
  * Fix crash updating speed before/after in time remap. [Commit.][157]
  * On project close, ensure non loaded clips don’t leak to the new project (part 2). [Commit.][158]
  * Fix proxy clips not correctly disabled on rendering. [Commit.][159]
  * On project close, ensure non loaded clips don’t leak to the new project. [Commit.][160]
  * Add debug info to better understand proxy rendering issue. [Commit.][161]
  * Fix sometimes cannot resize clip when there is a 1 frame gap. [Commit.][162]
  * Various fixes for remove space in subtitle track. [Commit.][163]
  * Time remap: only seek clip monitor if visible. [Commit.][164]
  * Fix same track transitions sometimes broken by clip resize. [Commit.][165]
  * Fix 1 frame offset in subtitles when removing space. [Commit.][166]
  * Fix timeline preview sometimes using old rendered file instead of recreating a preview. [Commit.][167]
  * Fix bin clip thumbnails sometimes not created. [Commit.][168]
  * Show clip labels as soon as there is one letter width. [Commit.][169]
  * Fix marker thumbnail size. [Commit.][170]
  * Don’t show clip thumbs on when clip is too small (<16 pixels). [Commit.][171]
  * [Titler] Fix: don’t remember outline value with factor 10. [Commit.][172]
  * [Bin] Improve/fix ToolBar overflow. [Commit.][173]
  * [Monitors] Improve/fix ToolBar overflow. [Commit.][174]
  * [Monitors] Improve audio volume widget. [Commit.][175]
  * [Wizard] Fix wrong subtitle effect detection. [Commit.][176]
  * [Effect UI] Improve/fix ToolBar overflow. [Commit.][177]
  * Missing change from last commit (fix remove space). [Commit.][178]
  * Fix “remove space” not working on 1 frame space. [Commit.][179]
  * Only create proxy clips automatically if requested. [Commit.][180]
  * Fix audio wave for non stereo clips. [Commit.][181]
  * Remove configureToolbar hack for >= KF 5.91. [Commit.][182]
  * [Titler] Fix text outline width steps (does only seem to support int). [Commit.][183]
  * Now that everything is fixed in Craft, use the new KNS dialog on windows. [Commit.][184]
  * Fix qml binding loop warning. [Commit.][185]
  * Add option in timeline preview menu to render preview using original clips, not proxies (disabled by default). [Commit.][186]
  * Add Set zone in/out to timeline ruler context menu. [Commit.][187]
  * [flatpak] update frei0r for alpha render fix. [Commit.][188]
  * Fix clip thumbnails extending past clip length. [Commit.][189]
  * Add missing vertical spacer. [Commit.][190]
  * Fix adjust to original size using proxy resolution. [Commit.][191]
  * Try to fix ghost icons on Windows. [Commit.][192]
  * Major speedup in audio thumbs drawing on high zoom levels. [Commit.][193]
  * [Nightly Flatpak] Update to ffmpeg 5.0. [Commit.][194]
  * Fix clip name not scrolling anymore. [Commit.][195]
  * Fix unusable bin icon for audio/video drag. [Commit.][196]
  * Don’t update dock title bars several times on layout change. [Commit.][197]
  * “Add Marker/Guide quickly” allow shortcut \* only on keypad (otherwise \*. [Commit.][198] Fixes bug [#434411][199]
  * Fix action name: “Save Timeline Zone to Bin” is not what it does. [Commit.][200] Fixes bug [#436386][201]
  * Fix Wayland crash on layout switch. [Commit.][202]
  * Minor optimization for audio thumbs drawing. [Commit.][203]
  * [Packaging Type] Try to detect snap automatically. [Commit.][204]
  * L10n improvements by @bellaperez: *.ui files. [Commit.][205]
  * L10n improvements by @bellaperez: Misc. [Commit.][206]
  * Show details of external proxy profile in project settings. [Commit.][207]
  * Fix .ass subtitle files not correctly read. [Commit.][208]
  * Ensure processes are in the path before starting an executable. [Commit.][209]
  * Fix timeline keyframes sometimes disappearing from view. [Commit.][210]
  * Fix wrong comparison of current settings and settings stored in the project settings dialog. [Commit.][211]
  * Fix sometimes cannot move grouped clip right when only 1 empty frame. [Commit.][212]
  * Clip stabilize: keep track of bin clip rotation. [Commit.][213]
  * Render at preview resolution: store in project file instead of global config. [Commit.][214]
  * When saving effect, show it under its name, not id in effect list. [Commit.][215]
  * Fix fade effects not correctly saved or pasted. [Commit.][216]
  * Fix vp8 with alpha render profile. [Commit.][217]
  * Fix clip monitor allowing seek past clip length with transparency background enabled. [Commit.][218]
  * Rendering: add option to render at preview resolution for faster preview rendering. [Commit.][219]
  * Fix green tint on first image extract. [Commit.][220]
  * Show package type in about dialog. [Commit.][221]
  * Improve Sandbox detection, use dedicated config files for sandbox packages. [Commit.][222]
  * Timeline preview: use compact format when calling renderer. [Commit.][223]
  * Refactor Kdenlive Settings. [Commit.][224]
  * Minor cleanup of add marker ui. [Commit.][225]
  * Ensure thumbnail preview profile is not changed by clip resolution. [Commit.][226]
  * More alpha render quality fixes. [Commit.][227]
  * PACKAGING CHANGE!! Introduce PACKAGE_TYPE envvar. [Commit.][228]
  * Try to improve alpha render quality. [Commit.][229] Fixes bug [#436879][230]. Fixes bug [#430093][231]. Fixes bug [#357153][232]
  * When transoding variable fps clips, ensure we keep all streams. [Commit.][233]
  * Fix recently introduced memory leak. [Commit.][234]
  * Fix MLT’s build for nightly AppImage following a recent MLT change. [Commit.][235]
  * Fix alpha render and add utvideo. [Commit.][236] Fixes bug [#448010][237]. See bug [#436879][230]
  * Timeline preview: when saving, store rendered chunks in a more clever list (like 0-1700 instead of listing each 0,25,50,75,… until 1700). [Commit.][238]
  * Show number of Bin clips / selected clip in status bar, fix rename / focus clip in icon view. [Commit.][239]
  * Change defualt shortcut or “Multitrack View” to F12. [Commit.][240]
  * Fix freeze trying to drag a clip that was just added to Bin. [Commit.][241]
  * Append fps to clip name when transcoding a variable fps clip. [Commit.][242]
  * Add recent MLT rotate feature to easily rotate a video clip from clip properties. [Commit.][243]
  * Fix timeline ruler not working after effect drop in some circumstances. [Commit.][244]
  * Fix possible crash on undo/redo transcoding. [Commit.][245]
  * Warn user when trying to render an empty timeline, some widget cleanup. [Commit.][246]
  * Fix various bugs in timeremap (keyframes random move, crashes). [Commit.][247]
  * [otio] In case of an error check if it is due to the installed version. [Commit.][248]
  * Add default shortcut to “Multitrack view”: Shift+0. [Commit.][249]
  * [OTIO] Differentiate between read and write adapters. [Commit.][250] Fixes bug [#448318][251]
  * Add python interface. [Commit.][252] Fixes bug [#423083][253]
  * Time Remap: don’t allow keyframe after last frame of source clip. [Commit.][254]
  * Timeremap should only be available on clips without B frames. Propose transcoding otherwise. [Commit.][255]
  * Show warnings if avfilter, especially avfilter.subtitles was not found. [Commit.][256]
  * [Setup Wizard] Show codes if there are only info messages, fix doc link. [Commit.][257]
  * Protect timeline preview list with mutex. [Commit.][258]
  * Fix minor typo. [Commit.][259]
  * At least a little bit more order in configure shortcuts dialog. [Commit.][260]
  * Fix slideshow duration not updated on profile change. [Commit.][261]
  * Fix save path for custom render profiles on Windows following commit 90b1e4. [Commit.][262]
  * Fix detection of missing timeline preview chunks on opening. [Commit.][263]
  * Don’t attempt to create audio thumbs if thumbs are disabled. [Commit.][264] Fixes bug [#448304][265]
  * Neutral background, separator and tooltip for tool name in statusbar. [Commit.][266]
  * Speedup loading of projects with timeline preview. [Commit.][267]
  * Add some default LUT files. [Commit.][268]
  * Revert fileWatcher to private one, as the global app sometimes messes with us (when opening a file dialog). [Commit.][269]
  * Fix extract frame on Windows (also used for Titler and scopes). [Commit.][270]
  * We still need the magic lantern icon. [Commit.][271]
  * Reducing to compatible subset for licensing consistency. [Commit.][272]
  * Fix dragging a composition sometimes moved it to the beginning of the clip. [Commit.][273]
  * Use a SPDX standard license identifier in Appstream data. [Commit.][274] Fixes bug [#448134][275]
  * Make it easier to drag a same track composition (include snapping point). [Commit.][276]
  * Fix inconsistencies in profile switch check. [Commit.][277]
  * Fix loading progress not disappearing and incorrect “clip already exists” message. [Commit.][278]
  * Fix bin accepting invalid clips. [Commit.][279]
  * Update build MLT >= 7.0.0. [Commit.][280]
  * When adding many clips to a project, show loading progress in status bar. [Commit.][281]
  * Deprecate and fallback icons and disable by default. [Commit.][282]
  * Fix regression after 8f445516159a06654be649d7c70a2aae9788f071. [Commit.][283]
  * Fix freeze trying to change profile while load tasks were running. [Commit.][284]
  * FIx after 7ef5187fe2d6448fc4c2ae29df6e861ddf7de41c. [Commit.][285]
  * Fix crash on layout change. [Commit.][286]
  * Fix cleanup regression (disappearing titles). [Commit.][287]
  * Fix tab widget tooltips containing ampersand. [Commit.][288] See bug [#447825][289]
  * Partly Revert 19b9a0b7 since it caused trouble. [Commit.][290]
  * Fix shortcuts sometimes broken with fullscreen monitor. [Commit.][291]
  * Qml required property is not supported in Qt < 5.15. [Commit.][292]
  * Remove unused code for codec check in wizard. [Commit.][293]
  * Add missing license headers, fix some existing. [Commit.][294]
  * Some clean up of unused code. [Commit.][295]
  * Fix bin clip not correctly reloaded on profile change, causing missing audio thumbs. [Commit.][296]
  * Massive speedup on project load (at least when working on nfs filesystem). [Commit.][297]
  * Switch to global fileWatcher. [Commit.][298]
  * Edit friendly transcoding: add option to disable autorotate during transcoding. [Commit.][299]
  * [Monitors] Ctrl+Wheel instead of Ctrl+Shift+Wheel. [Commit.][300]
  * [Clip Monitor] Fix flicker on hover of “In Point” / “Out Point” Label. [Commit.][301]
  * Refactor monitor tool bars (Pt. 1). [Commit.][302]
  * Referactoring: Move ZoomBar to a generic component. [Commit.][303]
  * Minor optimization on project load (don’t unnecessarily request a frame). [Commit.][304]
  * [Timeline Zoombar] More improvements. [Commit.][305]
  * [Timeline Zoombar] Improvments. [Commit.][306]
  * Fix shortcuts sometimes broken on fullscreen monitor. [Commit.][307]
  * Fix build after last commit. [Commit.][308]
  * [Splash] Add KDE Branding, cleanup unused files. [Commit.][309]
  * Fix minor typo. [Commit.][310]
  * Fix minor typos. [Commit.][311]
  * Fix audio thumbs not created after profile change. [Commit.][312]
  * Fix window title using custom path instead of profile description. [Commit.][313]
  * Fix compilation warnings (function type compatibility). [Commit.][314]
  * Automatically offer to transcode variable frame rate clips. [Commit.][315]
  * Happy New Year Kdenlive! (Update AboutData Copyright Year). [Commit.][316] 
      * Check for variable frame rate clips and propose transcoding (WIP). [Commit.][317]
  * [Titler] Add some tool tips for select actions. [Commit.][318]
  * Add xml ui for audiolevelgraph effect and other xml format fixes. [Commit.][319]
  * Fix compilation with KF5 < 5.89. [Commit.][320]
  * Fix multiple bins should always stay tabbed together. [Commit.][321]
  * Add JP2 image format. [Commit.][322]
  * Minor improvements for OTIO handling. [Commit.][323]
  * Update user manual link for dasiam instructions. [Commit.][324]
  * Fix profile corruption. [Commit.][325]
  * Fix possible crash working with placeholder clips with speed effect. [Commit.][326]
  * Include clip markers and effect params in test hash function for better regression tracking. [Commit.][327]
  * Ability to make image seqiences from raw photos. [Commit.][328]
  * Move default lut value to proper function. [Commit.][329]
  * CppCheck fixes Pt. 4. [Commit.][330]
  * CppCheck fixes Pt. 3. [Commit.][331]
  * CppCheck fixes Pt. 2. [Commit.][332]
  * Fix last commit always resetting lut file to first installed one on project load. [Commit.][333]
  * Ensure lut effect is initialized with a file when added. [Commit.][334]
  * Update file test, add a timeline hash function to check if a document is identical before / after save. [Commit.][335]
  * Fix inconsistencies in subtitle model leading to broken (uneditable) items. [Commit.][336]
  * Remove unused/duplicate code. [Commit.][337]
  * Fix some compiler and CppCheck warnings. [Commit.][338]
  * Re-use NegQColor. [Commit.][339]
  * Fix build with fuzzing. [Commit.][340]
  * Attempt to fix threading test crash. [Commit.][341]
  * [nightly flatpak] update dependencies. [Commit.][342]
  * Add tests to prevent project corruption on color/title/image clip resize as happened in 21.08.3. [Commit.][343]
  * Use AppLocalDataLocation on all plattform to finde KNS items. [Commit.][344]
  * Improve urllistwidget for effects. [Commit.][345]
  * Fix compile failure after last merge. [Commit.][346]
  * Make it possible to run composition tests independent from each other. [Commit.][347]
  * Fix tests. Instead of returning random profile, use dv_pal when no config file is found. [Commit.][348]
  * Uptade frei0r.scale0tilt.xml with Scale X and Y parameters now animated. [Commit.][349]
  * Make it possible to enable/disable track with a shortcut. [Commit.][350] Fixes bug [#440181][351]
  * Clean up code after 9aaf43a1. [Commit.][352]
  * Install templates to “kdenlive/effect-templates”. [Commit.][353]
  * Add option to download effect templates from store.kde.org. [Commit.][354]
  * Make it possible to export custom effect xmls. [Commit.][355]
  * Add two template effects authored by @massimostella. [Commit.][356]
  * Add infrastruture for template effects. [Commit.][357]
  * Fix display of timeline usage in clip monitor. [Commit.][358]
  * Show timeline usage in clip monitor. [Commit.][359]
  * Add some tests for spacer operations. [Commit.][360]
  * Require at least CMake 3.16. [Commit.][361]
  * Add UI for the frei0r_transparency effect. [Commit.][362]
  * Attempt to fix slideshow detection on Windows. [Commit.][363]
  * Add a position widget to mix parameters to allow sliding the mix. [Commit.][364]
  
 [2]: http://commits.kde.org/kdenlive/d035aae37c2ae46db2c2423112054f8f80a96750
 [3]: https://bugs.kde.org/452495
 [4]: http://commits.kde.org/kdenlive/c9a8354a875b23404b5f4c15f08d8f7df402c9d4
 [5]: http://commits.kde.org/kdenlive/9a5dfcc34702e8d1bc11e4a7263e5bd232364e5f
 [6]: http://commits.kde.org/kdenlive/c30e3e40500ad5d3ae14e8843d04720e32da83d9
 [7]: http://commits.kde.org/kdenlive/472be1f4f047a365b1247720ae0b0ce67ae585e5
 [8]: http://commits.kde.org/kdenlive/0389fa8af1f9909e27193a102f01e4d63aeb0814
 [9]: http://commits.kde.org/kdenlive/81d8de46e110001d67ff6565d5fc8cb25d124e59
 [10]: http://commits.kde.org/kdenlive/a582d5dbbadc24db030bbfab8f4ac8e5a67d8376
 [11]: http://commits.kde.org/kdenlive/ec997b3d3d3de56abd8c5c9d69f13027d402c11d
 [12]: http://commits.kde.org/kdenlive/8cd6a656a675e3a739feda1a1a0e787b53b0c02f
 [13]: http://commits.kde.org/kdenlive/b729c5c7f73bfd1463e30c3a8534a1597e6c4ae3
 [14]: http://commits.kde.org/kdenlive/7e0067a7e552c15330b69338d75f7d6f453883eb
 [15]: http://commits.kde.org/kdenlive/a4682ad91d2a2e8f42ef6a0a06504848e140b841
 [16]: http://commits.kde.org/kdenlive/b6731517b73091f26010f0307040252b3f462061
 [17]: http://commits.kde.org/kdenlive/62cf0613fec973b962d4812077be11352871d3a9
 [18]: http://commits.kde.org/kdenlive/5406eba019d6805e6654a16b3387ea44a288bff5
 [19]: http://commits.kde.org/kdenlive/24f760e4fd1cd3ab66023d5038665e166a6b8a1c
 [20]: http://commits.kde.org/kdenlive/ef92d8cc67046e1192d94d09ac61ffd00004cf59
 [21]: http://commits.kde.org/kdenlive/786083774541dba535e403525b7c7ee4fe939f33
 [22]: http://commits.kde.org/kdenlive/22dc6a7e5c1b650e609196ca2477437bc41afe34
 [23]: http://commits.kde.org/kdenlive/803fcbee0626e4d55fffc5b1288d48a0b1c3b00e
 [24]: http://commits.kde.org/kdenlive/001007324369af3c6c5d814578576972bded3b11
 [25]: http://commits.kde.org/kdenlive/d901abe7a994b93b1e588d769fc597e9c68d4a11
 [26]: http://commits.kde.org/kdenlive/79eade13a8af3c8c92e88dad10e45492cab8b8eb
 [27]: http://commits.kde.org/kdenlive/ea7791f54408b274c5efe872f91cdebafa15c1b1
 [28]: http://commits.kde.org/kdenlive/145bf5100f44a8ef5f98a7339d5286ba262f57c7
 [29]: http://commits.kde.org/kdenlive/3d9a1a572e983ced5eb2300fd7ca1a9be38d08fd
 [30]: http://commits.kde.org/kdenlive/52b16a742f44e42661ffbfeb99a508a83cfc0372
 [31]: http://commits.kde.org/kdenlive/185e9b9d1c3ef84b5a6ab4f3dfdc422b65d9bdda
 [32]: http://commits.kde.org/kdenlive/cde34ac0db2408bb4bc22e68d446933ff8791816
 [33]: http://commits.kde.org/kdenlive/17e85bcfb74cc56680bd307650a9fe7261d59426
 [34]: http://commits.kde.org/kdenlive/3be2b2867b24dab8aa4518fd315109d134db6958
 [35]: http://commits.kde.org/kdenlive/18a625dca0ca8330fcec86833f29cd3ca76b9b2c
 [36]: http://commits.kde.org/kdenlive/9ec8f4f4579acd87b4921cf7972c0b9a3b380432
 [37]: http://commits.kde.org/kdenlive/d6d061f277354e1c795ddc4bc668fc818295d7a8
 [38]: http://commits.kde.org/kdenlive/f44eab174a96adeec5bd0a37709a94ddce349ecc
 [39]: http://commits.kde.org/kdenlive/eef8f05a732f1ebb91f81770498bef3e0e9ebd4c
 [40]: http://commits.kde.org/kdenlive/154812c8b574afcf908231488903f0e3f68d0f73
 [41]: http://commits.kde.org/kdenlive/1e891803b9885571aed27b66c6602bf119785ac5
 [42]: http://commits.kde.org/kdenlive/78c6a435a9868b00fec6af820163099cf7065115
 [43]: http://commits.kde.org/kdenlive/ffba99db04fb7907c056f279e105f7fc29492b12
 [44]: http://commits.kde.org/kdenlive/57152173d8b773944e0f00d278cf06a6e62597ae
 [45]: http://commits.kde.org/kdenlive/2933fe30134a0e9407c1e17e970bcf0242cba47c
 [46]: http://commits.kde.org/kdenlive/7a2334cebcbb46f20de52bbc6d9bc8dded94b421
 [47]: http://commits.kde.org/kdenlive/0274866f2b1c23fa8cc6d4e63ab7210f9e65b28c
 [48]: http://commits.kde.org/kdenlive/990e53e87b6676bbd3007c2ab535045212e82bde
 [49]: http://commits.kde.org/kdenlive/603b0d6876be112fa3028c4258dc9d3711a62dbf
 [50]: http://commits.kde.org/kdenlive/6b47852a90ae9a6fe622462097950c64af025bbd
 [51]: http://commits.kde.org/kdenlive/68ad7435c3317558c73f7e4139c8882c12838d0b
 [52]: http://commits.kde.org/kdenlive/7bc34df40f6baf9fbd05d2073359f40cbe34eb17
 [53]: http://commits.kde.org/kdenlive/8a714260e1f5d2390f06a7d57914fc976c3d1aa9
 [54]: http://commits.kde.org/kdenlive/ada77e207f37a597d4d398cde9445d63ef29b1cf
 [55]: http://commits.kde.org/kdenlive/aa121f9d52c1098d6c12f03f393b2e3ab5d2337f
 [56]: http://commits.kde.org/kdenlive/9905e26c397ccf3f9304023aad55f47b3f23fde7
 [57]: http://commits.kde.org/kdenlive/70378af1e5fc3979daa725b5a8972b593653c014
 [58]: https://bugs.kde.org/417209
 [59]: http://commits.kde.org/kdenlive/e7c10c2985b003b3d176182933ddea9ec881aebf
 [60]: http://commits.kde.org/kdenlive/d93edff9c9d4af4b3663dafe2df4c58dce133390
 [61]: http://commits.kde.org/kdenlive/4b7f91db359fdb493df7a49ebc94684694192ab9
 [62]: https://bugs.kde.org/435213
 [63]: http://commits.kde.org/kdenlive/73e3994f7265337fdb596148aad8f7a975786727
 [64]: https://bugs.kde.org/434754
 [65]: http://commits.kde.org/kdenlive/0dee8d47d7f410035d86e04ac011d9dfb516a26d
 [66]: https://bugs.kde.org/433595
 [67]: http://commits.kde.org/kdenlive/6f288c8e28596796c1b3a53391f259a64b7dee8d
 [68]: https://bugs.kde.org/441014
 [69]: http://commits.kde.org/kdenlive/11b0c242b4a87c542470a5011716d7221233b21a
 [70]: https://bugs.kde.org/437427
 [71]: http://commits.kde.org/kdenlive/43f4e060a36df83c9dc8b59e6a80f821eee18210
 [72]: https://bugs.kde.org/413572
 [73]: http://commits.kde.org/kdenlive/37dfd901697ceb790a0e670ca07cc0e0325c189c
 [74]: http://commits.kde.org/kdenlive/539b48ab06ceffdd35d21a2e8ad6e6b14d5cd856
 [75]: http://commits.kde.org/kdenlive/fbb6ebf806e453618d7419d2a61bbc74838cc741
 [76]: http://commits.kde.org/kdenlive/a3b8301dd05063208a4bb37783d76462150db458
 [77]: http://commits.kde.org/kdenlive/a0827f2d20274c636ad25c21dc218c1af64aa459
 [78]: http://commits.kde.org/kdenlive/ae8f2a06a367a3cfacdb0a5452f18e0dcc9de452
 [79]: http://commits.kde.org/kdenlive/29a172a257387b1e5827f98c70bc19420ea3810e
 [80]: http://commits.kde.org/kdenlive/e39840ec6925189114facb044b60ae0955ac2f65
 [81]: http://commits.kde.org/kdenlive/60316635521d8f43890709f855d2fd877c316ff7
 [82]: http://commits.kde.org/kdenlive/230d8ca58b55f227ee5124acef703009a076b563
 [83]: http://commits.kde.org/kdenlive/67ad873256b2b28adb7537b36b9630e046a95efd
 [84]: http://commits.kde.org/kdenlive/c623db8e7afcf41c8171855b94292010625088aa
 [85]: http://commits.kde.org/kdenlive/a73d42f5309a8527072e29541033e02a4753ec95
 [86]: http://commits.kde.org/kdenlive/33634f32a11cf1abaab806944868e02955c1f4e6
 [87]: http://commits.kde.org/kdenlive/9ce629db1b473f95e72668d29648a725869acfee
 [88]: http://commits.kde.org/kdenlive/720226cf1a322e95f601cc03ab40a8f7eff46823
 [89]: http://commits.kde.org/kdenlive/2c7b01f0826f6bc675c981d3ea4d486e85c57443
 [90]: http://commits.kde.org/kdenlive/bdd549dc5bc13cd1f61e5a3bc4abf1e11cf4c99e
 [91]: http://commits.kde.org/kdenlive/971eeb6c9a189acdabf47f06db1841c48e8df848
 [92]: http://commits.kde.org/kdenlive/f167b8841335ef543d405106942d9917fa5c23fd
 [93]: http://commits.kde.org/kdenlive/fbb3077b2c0e33d312d44d1ea861ed47a6b9f6a5
 [94]: http://commits.kde.org/kdenlive/8258a85a00996a3084567eae6d20da4b16c8ae34
 [95]: http://commits.kde.org/kdenlive/42d4c08f926c155a4eb020167a298c4bc1bc9d37
 [96]: http://commits.kde.org/kdenlive/b660bb827471a3d04aab4a5798622e2e8344b2ed
 [97]: http://commits.kde.org/kdenlive/a7127961912ecaf04a5fa927220ea9ecc8d447dd
 [98]: http://commits.kde.org/kdenlive/38640cf28b8ea0a377fcbaff2a94ee65244978ce
 [99]: http://commits.kde.org/kdenlive/3480b5afe9f53ef580145e7f43e92ff88bde1311
 [100]: https://bugs.kde.org/415610
 [101]: http://commits.kde.org/kdenlive/fdc29aa45ca21791f6983075db569d5fe489ddce
 [102]: http://commits.kde.org/kdenlive/34a5cbf869801054d22c692c6b278960ec3fb197
 [103]: http://commits.kde.org/kdenlive/aa0ee902150228db2fdab20ec873964a0e46bc9d
 [104]: http://commits.kde.org/kdenlive/4ce228b2b169b6bd5d25b9d1128178001e8dcb7b
 [105]: http://commits.kde.org/kdenlive/34da6a7bfd6e187538c357112af94330f4583cea
 [106]: http://commits.kde.org/kdenlive/d29a962a2be8f8ce34e50c6376c58e44e633cc5d
 [107]: http://commits.kde.org/kdenlive/58d2b03f39aab13485ea7d65c868cd391f68415c
 [108]: http://commits.kde.org/kdenlive/bfbff8173603609123ed4e2d9205481ce0cca402
 [109]: http://commits.kde.org/kdenlive/00c3849630b2e479c91d518ef577d36cc0a6d42e
 [110]: http://commits.kde.org/kdenlive/2166b4162c47a274ef58198135f8b1a156493472
 [111]: http://commits.kde.org/kdenlive/af7f1689e0d68c2977be6a3d88e4c007bc4a309e
 [112]: http://commits.kde.org/kdenlive/ddb57d5dc34a88ce8082a24b4c58e99771ef2e79
 [113]: https://bugs.kde.org/438880
 [114]: http://commits.kde.org/kdenlive/6792cb4df6d5429af9b210a4fb0056d06022739d
 [115]: http://commits.kde.org/kdenlive/b5de156b825399b0765b9e4f6dc8e738ab31f904
 [116]: http://commits.kde.org/kdenlive/cfe9c0716d2b07a889a14843b9719281e6cda394
 [117]: http://commits.kde.org/kdenlive/dfd43a8761a4f32fcbfd7de80dd37f7cf6acfed1
 [118]: http://commits.kde.org/kdenlive/12a396a276671f88a1f514f7763765fe5eff0453
 [119]: http://commits.kde.org/kdenlive/439542011ceead57ae7b352b333087302e1bb491
 [120]: http://commits.kde.org/kdenlive/b0562678d42c276c302e3e866c91c72ef6d58fde
 [121]: http://commits.kde.org/kdenlive/e5576773763d0d92dca9b58062581d2a4b1810a4
 [122]: http://commits.kde.org/kdenlive/960268714d090f399ec4a86c0e648d89425f3734
 [123]: http://commits.kde.org/kdenlive/196eb87fd437d5307cc6a11e81b9930009f70a6e
 [124]: http://commits.kde.org/kdenlive/bb2dc9fe21c6233a5ae6c7d176667baab28d8f8e
 [125]: http://commits.kde.org/kdenlive/c1a0660727382779efc3b28a869ad79a1b3ac409
 [126]: http://commits.kde.org/kdenlive/1683e9ad5f2a77f460e7d01cf7cb81b38d008708
 [127]: http://commits.kde.org/kdenlive/4b8107cbd2570d670c7385e942630dbe06388ee8
 [128]: http://commits.kde.org/kdenlive/e45d92f23de32a9bb7b62e1856878887830d960f
 [129]: http://commits.kde.org/kdenlive/d08c6d8353643705f71c0140f837d7617de403d2
 [130]: http://commits.kde.org/kdenlive/9a1dbcc2bda1b8c58457cc0e1a5146d5340333d5
 [131]: http://commits.kde.org/kdenlive/52512aace27aba36d167b9307e9be2f1efe38f2b
 [132]: http://commits.kde.org/kdenlive/6772856a2092c1cad64d7e1c6b58692a85c23782
 [133]: http://commits.kde.org/kdenlive/6c731f2f09eeeb274250f44d9299367783621578
 [134]: http://commits.kde.org/kdenlive/3c74efa314504115d728077b5f3fbc9aac0a3954
 [135]: http://commits.kde.org/kdenlive/9dd1aed1f9d8dc2deea54be0bd2137eb8e351333
 [136]: http://commits.kde.org/kdenlive/0f53fb821ed044764ae38f70da1919d03649afb2
 [137]: http://commits.kde.org/kdenlive/06f625381ce7cc59032411cf6f11a700c0ef3a2d
 [138]: http://commits.kde.org/kdenlive/98485c670dd5989542ea981900872fab76320f86
 [139]: http://commits.kde.org/kdenlive/814dbae136bff7c6e99546836f78afba32fe917f
 [140]: http://commits.kde.org/kdenlive/71eb0f28593126306e4266c68073d50a0590340f
 [141]: http://commits.kde.org/kdenlive/bd8c6134d0b46006dcc55eae2addcbae554c73cc
 [142]: http://commits.kde.org/kdenlive/aeb70268bddd612dcc56ca77c02b6601f83fae8b
 [143]: http://commits.kde.org/kdenlive/ea9c47f9d8ed4137bfc6e7e15d581605456b4471
 [144]: http://commits.kde.org/kdenlive/e9d9beba957f3a7f2daf8915a2cf861c57606284
 [145]: http://commits.kde.org/kdenlive/a177f13370462c820eb509e3095bd80aeece0dca
 [146]: http://commits.kde.org/kdenlive/34a9b8946d32f722f1e56c610355c613ea057175
 [147]: http://commits.kde.org/kdenlive/254061075abe04f4cd8941bcb475b6a656944419
 [148]: http://commits.kde.org/kdenlive/62a49280d913806309b8ebf55c0443d7a6fa0b83
 [149]: http://commits.kde.org/kdenlive/ac19273ff7c4aa5b2e6228caf6703a5fb74a5e3f
 [150]: http://commits.kde.org/kdenlive/2dc7880570f34844c36e6ac5220ff0192772173e
 [151]: http://commits.kde.org/kdenlive/9fe5cca9dedbadd334332e1e14952754f06556c3
 [152]: http://commits.kde.org/kdenlive/bfe47a5969b53463800abb7a2a8701ced9f36a0b
 [153]: http://commits.kde.org/kdenlive/25b1d403f95f2c84c252ca264aa21e6a52b7ab63
 [154]: http://commits.kde.org/kdenlive/1185d96dd2b83329a40fddb0a23f89b0cb537ee8
 [155]: http://commits.kde.org/kdenlive/04327470c6029f3f2f75f5c8e32b2fef94a0a521
 [156]: http://commits.kde.org/kdenlive/fe4b81b506e7d60fecdbc2cf5a3483efca1cdb26
 [157]: http://commits.kde.org/kdenlive/6c3ad4bd358d53501b41d2b751e9884a770cd64f
 [158]: http://commits.kde.org/kdenlive/daac6b500457686a7a10d5417c99d7284a5481bb
 [159]: http://commits.kde.org/kdenlive/646a4d1fefcd69e9fdad01d01644a17f00d6ef2e
 [160]: http://commits.kde.org/kdenlive/f87a63f77641560cd768577c46ed8a08b72c488a
 [161]: http://commits.kde.org/kdenlive/51054fea03a0b5fd764069bf5307dad8e0e7d522
 [162]: http://commits.kde.org/kdenlive/df583cdc91383d853e70b3ba558755c269ec5c8a
 [163]: http://commits.kde.org/kdenlive/2e78912e868fa16cebb4b78dfc674b7c815fa08d
 [164]: http://commits.kde.org/kdenlive/66cf639f4617091935fcdae06ba6b1baf16771bb
 [165]: http://commits.kde.org/kdenlive/8e6feecca7c436441bbce9da282dffae04e905bd
 [166]: http://commits.kde.org/kdenlive/f7b2e168651b54602663a8afbf495298162689df
 [167]: http://commits.kde.org/kdenlive/ef813c9c0c70d9da9c73f5a0c954d76c05505d39
 [168]: http://commits.kde.org/kdenlive/76b0eeac47e66bd45c77a75283295a72970d36aa
 [169]: http://commits.kde.org/kdenlive/0be955d8cb6ed3f55c95ae25c451cbb2ddabc7fe
 [170]: http://commits.kde.org/kdenlive/0125b74d1a04b4a81ffceae4a1f913104cf3c782
 [171]: http://commits.kde.org/kdenlive/842ebb4550729ed95541533549be92727d9d89fb
 [172]: http://commits.kde.org/kdenlive/2b9fad6959370b153309d550e47b7be63361f794
 [173]: http://commits.kde.org/kdenlive/b29bb3c28fdd6c7c83c40d03c7d395a3936eef78
 [174]: http://commits.kde.org/kdenlive/1484b8b7a1fbadf972a56191e083ba597fd04d47
 [175]: http://commits.kde.org/kdenlive/cf6e31e3f3734e35b9403676e8bd83e15b74a36d
 [176]: http://commits.kde.org/kdenlive/937ff419eaf283923d22d7e8ac14c8b53c02bc07
 [177]: http://commits.kde.org/kdenlive/ead4c14c701fbeb5fe34c4aaae495955ba8f2de0
 [178]: http://commits.kde.org/kdenlive/8c349a20fd3dfa888f9479b343478ad497dafe6e
 [179]: http://commits.kde.org/kdenlive/93c565b237c38982bdfc044878eef3e18f56c1e6
 [180]: http://commits.kde.org/kdenlive/0bd5d5057aeff70d83b5b1d24c551a078402cac4
 [181]: http://commits.kde.org/kdenlive/47928d5f5570f1d9965ca72966a81985405f5185
 [182]: http://commits.kde.org/kdenlive/c066bfbb79660c7be8fe9d723ad219580398a37b
 [183]: http://commits.kde.org/kdenlive/dbcd523b85c48bc20926545bd926086105d9fa96
 [184]: http://commits.kde.org/kdenlive/df8ffac6287a1e34071f930fb9ad6939dabe3562
 [185]: http://commits.kde.org/kdenlive/3281c37c0cf415061cd70914ec44f67de9c9278a
 [186]: http://commits.kde.org/kdenlive/4c1ac074b4b028aafd95496375f3dc88adb38177
 [187]: http://commits.kde.org/kdenlive/f82b7fff9422ffba036d74c964f5f6b1c44ac12e
 [188]: http://commits.kde.org/kdenlive/a9650585c89ff2748801944df8a4cae2c4805fb6
 [189]: http://commits.kde.org/kdenlive/bdc91a633a3d6f5c7f8cf778534454e74f127672
 [190]: http://commits.kde.org/kdenlive/1158d7e0af457e319ef6d3c8895b16415fde8584
 [191]: http://commits.kde.org/kdenlive/43f904ebb49a324543259d9c04ff3b7a823d079e
 [192]: http://commits.kde.org/kdenlive/d38d34e13ddb645ed7f0a764ae2baa63e0c504a1
 [193]: http://commits.kde.org/kdenlive/eaf41d85d04c9d7f0fc00be7377293fa59ae1cf3
 [194]: http://commits.kde.org/kdenlive/ee98b99106fc7d01a92fee2ce9bed7cda2285836
 [195]: http://commits.kde.org/kdenlive/3eb6d9cda2e71f996b14698be9f4cc3675dc25b4
 [196]: http://commits.kde.org/kdenlive/754f4f21ac9077a3ba4cc9104f2b014ebc76ff40
 [197]: http://commits.kde.org/kdenlive/4d5a36581bd383823bc28cd3349b6d3dec68a699
 [198]: http://commits.kde.org/kdenlive/78b02b50570bd2a41db1082f14b1c624d9874b1e
 [199]: https://bugs.kde.org/434411
 [200]: http://commits.kde.org/kdenlive/74b819f6b1c634fd5010e0ef0eaeb52e51212ece
 [201]: https://bugs.kde.org/436386
 [202]: http://commits.kde.org/kdenlive/a6b0ac90cc76d746c8c33743cbbd09497f4605b9
 [203]: http://commits.kde.org/kdenlive/84a35b76231575a681151dcdf8434641b88e14b8
 [204]: http://commits.kde.org/kdenlive/f17aade92109d832ee9d96fc9bfa1cc13fad0e0a
 [205]: http://commits.kde.org/kdenlive/4e8f33170a7770da6ad76f2a7cee6f0733364c51
 [206]: http://commits.kde.org/kdenlive/585945098c71c01672bc571cbcfc58fe6578f4dc
 [207]: http://commits.kde.org/kdenlive/de7fe7354d51dfe852c3b3d158aeecea03cc1e79
 [208]: http://commits.kde.org/kdenlive/f834e2e57ccd02f597e1e8bae40e5987ac5dae5d
 [209]: http://commits.kde.org/kdenlive/64e6bdfc61caa517207109bfe07399ad46bd8adb
 [210]: http://commits.kde.org/kdenlive/accae0f6e661c5b4cc4b181e5f98866bd39cf959
 [211]: http://commits.kde.org/kdenlive/be2e13dff8821e61df713e655ff43fbb497d470d
 [212]: http://commits.kde.org/kdenlive/dcfdc702b1782f1b68358582039a1859c64f8be2
 [213]: http://commits.kde.org/kdenlive/d38e7a11f105090eefda8f32fd1f876807ddc4ba
 [214]: http://commits.kde.org/kdenlive/f7b1eef650530e6cfcf9868429dd31e58862b4df
 [215]: http://commits.kde.org/kdenlive/78ff0a3d29a532da3e72cac219419f6c7805983d
 [216]: http://commits.kde.org/kdenlive/026791e953dd4547a7a525958095d802bd562f3d
 [217]: http://commits.kde.org/kdenlive/14e5b345129438893ac177fc76e288d342977cfd
 [218]: http://commits.kde.org/kdenlive/f51078c52af3c1e39c088e19d5ccc8824474843a
 [219]: http://commits.kde.org/kdenlive/693ea598d327b0841a57d12a76f2bbb3935a628f
 [220]: http://commits.kde.org/kdenlive/4b57766b0e17cec36ad4406aadf1a0149b7112dc
 [221]: http://commits.kde.org/kdenlive/a5674a0869c9dab6866a11ff597e5cfbf51ac8d7
 [222]: http://commits.kde.org/kdenlive/c7ce11c2533a6565ed884e24a9e1ef09c737e0a3
 [223]: http://commits.kde.org/kdenlive/07c5ce9fc9428efbb674b098d112ba7a904b8cfd
 [224]: http://commits.kde.org/kdenlive/4ea9a73876a6a578ebf8f8f53215d1dde8173bda
 [225]: http://commits.kde.org/kdenlive/0a7697d3b0f1c76a5755adda2ad8a856cc8f1465
 [226]: http://commits.kde.org/kdenlive/ed99187e1e3ba0fc53e349d58bbe4a6cf50f2f41
 [227]: http://commits.kde.org/kdenlive/4978bf3c83617d42f458333843a45880d450ab36
 [228]: http://commits.kde.org/kdenlive/5b5a154273ae686d1b62df06f9e294f0ab7c0c18
 [229]: http://commits.kde.org/kdenlive/d6910eacb8cda27adaf63afcec76e3391da4b2b7
 [230]: https://bugs.kde.org/436879
 [231]: https://bugs.kde.org/430093
 [232]: https://bugs.kde.org/357153
 [233]: http://commits.kde.org/kdenlive/1767566f2a500ece6b50a6bdaa15ee3bb0c42d26
 [234]: http://commits.kde.org/kdenlive/46bedb4d5a3d5110f194e80be6d707da0f49094c
 [235]: http://commits.kde.org/kdenlive/7b0ffafc752f870c7d15b7cc5aee47ed91f1b719
 [236]: http://commits.kde.org/kdenlive/ed550ac73f9b1987adcc1367aacba83e369af81e
 [237]: https://bugs.kde.org/448010
 [238]: http://commits.kde.org/kdenlive/cf076ed3c59cc8ff3d7e5d5e7107ad3a56cf0b5b
 [239]: http://commits.kde.org/kdenlive/f23c89af82648dd3143888effaa79667dba18a9a
 [240]: http://commits.kde.org/kdenlive/faede6fba53c48fbb3035403c136d08ddf85f89d
 [241]: http://commits.kde.org/kdenlive/5baaa06943fe1bb5d761213b68849800cc638451
 [242]: http://commits.kde.org/kdenlive/aa399d14844741a025a9becd3c2d1aa620eab424
 [243]: http://commits.kde.org/kdenlive/bf6565da366b875e224a3989dcf98bcc664a9568
 [244]: http://commits.kde.org/kdenlive/6e0ede6403ee0c192b967f33870beff937815bd6
 [245]: http://commits.kde.org/kdenlive/26682511382e00dd4341befc2862e4bacf59dfca
 [246]: http://commits.kde.org/kdenlive/ca19f563b504bb1cea39c321f1b567fea4e566a7
 [247]: http://commits.kde.org/kdenlive/453d977abdf6c661e745ff2044b07e27bea87c24
 [248]: http://commits.kde.org/kdenlive/49d2d066c429fc7a54a2454e95cc4c4d73a21ed9
 [249]: http://commits.kde.org/kdenlive/c856dedf9dd1b22cfa0f3e32ee6bc231c680415f
 [250]: http://commits.kde.org/kdenlive/06c14aee3191536c866040f614e7df25fc229ac3
 [251]: https://bugs.kde.org/448318
 [252]: http://commits.kde.org/kdenlive/315e3bb3b5a7a476a6cd5b5d3f9e37556e26161c
 [253]: https://bugs.kde.org/423083
 [254]: http://commits.kde.org/kdenlive/4a2e40ddf6ef0f44c05e39274e9908be041adf2d
 [255]: http://commits.kde.org/kdenlive/df66189b63de356076a89652f69f13f90c345fe9
 [256]: http://commits.kde.org/kdenlive/5d121549b40cb8b4d9bd4d93143026f8e3a160e8
 [257]: http://commits.kde.org/kdenlive/cb7c5bcc453e0c6dc7a17ab54f749fd16653ca15
 [258]: http://commits.kde.org/kdenlive/9b57bb16b54b0ad9fcd749787dd32fad7129a314
 [259]: http://commits.kde.org/kdenlive/4cf79bb3287548d21ef83f225a07a1c44e42718f
 [260]: http://commits.kde.org/kdenlive/73d0cf753eee3aafe9e6dcbf2cd25cee35c01cf1
 [261]: http://commits.kde.org/kdenlive/510bc63b4f4ec15b3dd0c7e0533a53dc71ea9e1c
 [262]: http://commits.kde.org/kdenlive/d46121c45cf8c3b754e145007cd05b4a09276d5d
 [263]: http://commits.kde.org/kdenlive/2b28cbe0b9b5ef796888d3fba7685ddad196449c
 [264]: http://commits.kde.org/kdenlive/1b3a4f7b8bd275f7d64f7877168955327d0cc7b5
 [265]: https://bugs.kde.org/448304
 [266]: http://commits.kde.org/kdenlive/8e8313c488bc44471c51a8610ea331fe2649dff1
 [267]: http://commits.kde.org/kdenlive/26e0d066aae355b8d52853f637a3153b00240757
 [268]: http://commits.kde.org/kdenlive/6fab074658a0ae146c9dadbb68e821ace47639ea
 [269]: http://commits.kde.org/kdenlive/81717770a48f0c29c9c6eebe4d35592944b25541
 [270]: http://commits.kde.org/kdenlive/571afe9722d3a4ce8bc759d1f4438b16a1a58fa5
 [271]: http://commits.kde.org/kdenlive/b31a5fe7a7c3b944e1b1d215c15d060326555806
 [272]: http://commits.kde.org/kdenlive/d56af67539f73b7059daf4c131e10ae12bd0047e
 [273]: http://commits.kde.org/kdenlive/5544a82a481e8aee254316bd165c6a62ee0f6fde
 [274]: http://commits.kde.org/kdenlive/c50680f9ad118ded1d1245da433f1c86389bd5af
 [275]: https://bugs.kde.org/448134
 [276]: http://commits.kde.org/kdenlive/3ab6191ce9bd35965a7c0dbce93bdb71c35d2be7
 [277]: http://commits.kde.org/kdenlive/a74b771e398b7d156a2e2c2fde6ec95dc779c7f5
 [278]: http://commits.kde.org/kdenlive/aeee87cef4b2f2ef2c10c84e9f5b0bf2b8ca8099
 [279]: http://commits.kde.org/kdenlive/43c287ff121b8edc28f90b9d849d2eaba80acff7
 [280]: http://commits.kde.org/kdenlive/f53f8c378d33604f2390f5bac5cd2a1239d88499
 [281]: http://commits.kde.org/kdenlive/735732bfb776940199bde6da7ae50eb9e9b94c92
 [282]: http://commits.kde.org/kdenlive/154af77e857aaa7d6eaf87c5433c8104ecd0ec0c
 [283]: http://commits.kde.org/kdenlive/ed4c1530c97cd597d2b000466b198a5f49cf019e
 [284]: http://commits.kde.org/kdenlive/a1af4b70ac6a536889fbfe5164e16d1d22c382eb
 [285]: http://commits.kde.org/kdenlive/5c87fa84f468c763104d4a0378cdd9630378007c
 [286]: http://commits.kde.org/kdenlive/39ba3e482c36e613f08aeecf8b51c477698cf660
 [287]: http://commits.kde.org/kdenlive/be7f27c8654fe7c7ff8b19bcd3f966b8163e70aa
 [288]: http://commits.kde.org/kdenlive/be712e7e14b71ed58d09c8d64a9eb0dd5090fa8f
 [289]: https://bugs.kde.org/447825
 [290]: http://commits.kde.org/kdenlive/b62462a4dda69d0dc4a38fa5d01ef034476ea0de
 [291]: http://commits.kde.org/kdenlive/d178d3c9a79a55d0b9d4c9cceb1956acef1135af
 [292]: http://commits.kde.org/kdenlive/7076164685a5405de44fe1e4f4f447e5c2a43cbe
 [293]: http://commits.kde.org/kdenlive/fa33bbf274406f5e636c69b0a3897f7db83d1560
 [294]: http://commits.kde.org/kdenlive/f188e9724aef15b0c27b4fe77de6c99ba2b7a038
 [295]: http://commits.kde.org/kdenlive/eb7eebfc4fafb9b547862492afc1e103afdafb77
 [296]: http://commits.kde.org/kdenlive/3d01a69e4f3e4065b64f4b4cf3556cf10b8a426e
 [297]: http://commits.kde.org/kdenlive/887b986c48258a5ee4b36bbcc2f7c68548ff29b8
 [298]: http://commits.kde.org/kdenlive/125c7d0e4fe5cc349ae7c9f07a4a4b432fad1708
 [299]: http://commits.kde.org/kdenlive/4276ba5f5d41d421be8ab16bb437c4e3511c5f4f
 [300]: http://commits.kde.org/kdenlive/b039cccefb3a571e87d3a23c76040c5dc880d592
 [301]: http://commits.kde.org/kdenlive/a07bd4511e7139045ef85d8f9b86cb2bdf9b0763
 [302]: http://commits.kde.org/kdenlive/6c51178f746ffc11898fee810f2e471a1a1c0805
 [303]: http://commits.kde.org/kdenlive/164e8bef8cc0090cc91f96c5dc5d186a1e2a33c2
 [304]: http://commits.kde.org/kdenlive/a8f591ea7e5d4ffb6f31f7fcec65d9ca4c9ee12f
 [305]: http://commits.kde.org/kdenlive/d41fb2025ffaff38495e83e61baaa9c847398218
 [306]: http://commits.kde.org/kdenlive/06107cab8135e3bce1a4ccf6ad62fda4c7233620
 [307]: http://commits.kde.org/kdenlive/8e68b656761f8a10fa453f852efed71ebe60ab35
 [308]: http://commits.kde.org/kdenlive/97c9d076e721c70c4c19040ae33c12c4f482d281
 [309]: http://commits.kde.org/kdenlive/aca33684cc8e9be7acf19041070b18e4d30a4341
 [310]: http://commits.kde.org/kdenlive/54b6c44994868378608625e0c034bd0d030b1c6e
 [311]: http://commits.kde.org/kdenlive/c37e3656c3dd4ee29eee13078f7c7a7bf630d4d6
 [312]: http://commits.kde.org/kdenlive/cd594fe7f0dac861663c3c35d3f3ab5233da9ac9
 [313]: http://commits.kde.org/kdenlive/03e27c0a90dc8f9df5f3b95b7c1b8ce7ffa95184
 [314]: http://commits.kde.org/kdenlive/97cf59663900c95618cc3318024129c27c840574
 [315]: http://commits.kde.org/kdenlive/ab2990eb984965dd66dc9af2c5f185ad14e8e4ca
 [316]: http://commits.kde.org/kdenlive/6bf88b18a5c4077779a7051ba4a664c672b8f1fe
 [317]: http://commits.kde.org/kdenlive/f58882c11a96ce03850c71accfdb1b46ef4be692
 [318]: http://commits.kde.org/kdenlive/7ce142f26a7df5037b5b20fe20477648529bd98a
 [319]: http://commits.kde.org/kdenlive/bd1ff9cec7b1e893625d156ee62f2d0ab3403d62
 [320]: http://commits.kde.org/kdenlive/e0becd807e4bf084d8ceb1a04f281abc9a62480c
 [321]: http://commits.kde.org/kdenlive/71f4c68261f312e70df45b5f396228c79b4c30d2
 [322]: http://commits.kde.org/kdenlive/9b409fcaed71b5008b6e6739c1a8f45447709050
 [323]: http://commits.kde.org/kdenlive/67db1c96e48fee3aca02dd397a57bc3e4a252817
 [324]: http://commits.kde.org/kdenlive/effb5286d2e78b9c20e482730358a2f07c2492c8
 [325]: http://commits.kde.org/kdenlive/e28a9f2f06193e88572e84617ea272943227107b
 [326]: http://commits.kde.org/kdenlive/8b0a5ea55680044e101707b192ec4975222afa21
 [327]: http://commits.kde.org/kdenlive/7244aeb1569814b02dc11c8ac5cd063e5782e391
 [328]: http://commits.kde.org/kdenlive/7f7afe9f68f8381072d58ba9f84ac7de4bda031f
 [329]: http://commits.kde.org/kdenlive/7829ad1115012eb3cba5a2fe94c0648bd19806e2
 [330]: http://commits.kde.org/kdenlive/1cb39f73a846286871d9bba7e0b1fbb44fa51610
 [331]: http://commits.kde.org/kdenlive/cc00a839b6a889c2a8199b9b9d21f3747d8e0540
 [332]: http://commits.kde.org/kdenlive/3853a4af3a513f469e8d64c46b15a17f0c14e8c4
 [333]: http://commits.kde.org/kdenlive/128fb884b880393f7fbc02dff79cfd42ba3d9978
 [334]: http://commits.kde.org/kdenlive/4b689537da18ba063e0f511c467b3f2a2b3ec6a9
 [335]: http://commits.kde.org/kdenlive/a0663d90a33c97d6e3c87dd7ae2123df9dd20397
 [336]: http://commits.kde.org/kdenlive/d84128daf5d029a69cbef6901a2879a244b7dc7d
 [337]: http://commits.kde.org/kdenlive/1287ec34edc7da28cf7eba041435107a7f2de6d3
 [338]: http://commits.kde.org/kdenlive/b00786698af4a82123a2818a13f15eaa3c8b5d25
 [339]: http://commits.kde.org/kdenlive/024e44d23d8b75bc3236eef9f0fb5f0be50c3bd2
 [340]: http://commits.kde.org/kdenlive/3ae123bd12f7420f3f0b6d5dd06c7e3434ab9066
 [341]: http://commits.kde.org/kdenlive/5e68827d31e0f8d94fd25d1c8a4063e74737ed67
 [342]: http://commits.kde.org/kdenlive/e0ac97d04ac4dad74004bed4e09e18ad5c7ffcb9
 [343]: http://commits.kde.org/kdenlive/0492874aa3aa9cac517c3605b6739bcf9d049c41
 [344]: http://commits.kde.org/kdenlive/90b1e4f5fc63e1c727866bf12c8626af37e39001
 [345]: http://commits.kde.org/kdenlive/dde8a67488e22f7a8f00ab0223ef24c84ac8e5ed
 [346]: http://commits.kde.org/kdenlive/d64f9d3e12e3f7e7efda31854fcef2683396b0f4
 [347]: http://commits.kde.org/kdenlive/0bcfe3358bbd28e07485255a9efecb270f32a1a3
 [348]: http://commits.kde.org/kdenlive/4ede899c1ac314c73da8a470ec4a28093d8fee8a
 [349]: http://commits.kde.org/kdenlive/d5199c5f19942ec7c3b63cfd81dba9afc437d1eb
 [350]: http://commits.kde.org/kdenlive/8e5c0861b5ac0eb8e55aaacdde1cf7034bcf0c89
 [351]: https://bugs.kde.org/440181
 [352]: http://commits.kde.org/kdenlive/ea4e11ee703f0415200efa2b69c0bddf4e1d72c3
 [353]: http://commits.kde.org/kdenlive/2b265800ff9d7f88a722875d72113126ed8eee3c
 [354]: http://commits.kde.org/kdenlive/afa50d0c96784dd6b66ef1fca71843a417ec5f84
 [355]: http://commits.kde.org/kdenlive/9aaf43a144c438675e3ca72e00c67ffdd3feb6ba
 [356]: http://commits.kde.org/kdenlive/75daf56a49d9f2c640f1cd7ee625985cd26aee6e
 [357]: http://commits.kde.org/kdenlive/01c269902f7e9023b3c5cc2017a7fea655880ebd
 [358]: http://commits.kde.org/kdenlive/b08e92bcc1e264b4bfbe2180a27bfcc8b09a32bd
 [359]: http://commits.kde.org/kdenlive/bdfa69d756d2d5eb1e95567e7f82a65dfe4cab9c
 [360]: http://commits.kde.org/kdenlive/d294cb5b2e36c1d59ea856e0f2fdb1c39b442ab0
 [361]: http://commits.kde.org/kdenlive/52cb7b4c0a535a16560256372dad2080babc8472
 [362]: http://commits.kde.org/kdenlive/0e831baa4aa52808f82f3c5fb9c0cf7f29d13ee5
 [363]: http://commits.kde.org/kdenlive/21f8ddbc91e1b6f412211f4f589e27bb6b03a6dd
 [364]: http://commits.kde.org/kdenlive/125428dcdef84dd0924093290a5dd9189d543f15

