---
title: Kdenlive 16.08.0 is here
author: Farid Abdelnour
date: 2016-08-18T18:15:25+00:00
aliases:
- /en/2016/08/kdenlive-16-08-0-is-here/
featured_image: feature-img.png
---

Kdenlive 16.08.0 marks a milestone in the project's history bringing it a step closer to becoming a full-fledged professional tool. The highlights of this release are:

## 3 Point Editing

  * Insert or overwrite a timeline region with a clip region.
  * Delete or lift timeline regions.
  * Also, the long-missing insert mode is now enabled and fully functional: simply drop clips onto the timeline and existing clips will move accordingly to make room.

## Timeline effect rendering

Sometimes, you just have too much effects or transitions in your timeline to be able to play it in realtime (you can see on the video, it can only play 13 frames per seconds). This is where this new feature helps you: it pre-renders parts of the timeline so that they can be played in realtime. And you can continue working on other parts of the timeline while Kdenlive is working in the background for you.

## Stability and Improvements

Over 370 bug fixes and code commits including various improvements to:

  * Titler
  * DVD Wizard
  * Timeline refactoring
  * User interface
  * Audio mixing
  * Editing
  * Faster keyboard workflow
  * New icons
  * New profiles
  * Fix memleaks and corruptions
  * Profile Selection UI

## Library Widget

The new Library Widget allows to copy and paste sequences between different projects.

## Track header visual optimization

Ability to control the width of the track header by dragging its right border left or right.

## Track compositing preview

Individual track control has been replaced by a global option in timeline toolbar to switch compositing modes between: High Quality, Preview and None.

## Start-up wizard removal

Kdenlive's start-up wizard is now removed and will appear only if a configuration problem is detected. Kdenlive now starts up picking the best default options (including NTSC/PAL framerate depending on your region).

## Cached Data Manager

Kdenlive as a video editor can produce large amounts of temporary data. Until now, all these temporary files were saved in $HOME/kdenlive. We are now switching to an XDG compliant scheme, and try to give users more infos about how much data is stored, and allow for easy mangement.

## Krita support

Native support for Krita images was added.

## New Logo and Website

A new modern logo and website reflecting the new phase of the project.

We also celebrate this development cycle with an increased amount of community participation.

In other news more work has been done porting Kdenlive to Windows as part of a GSOC project. Also work has started by the Macports team to bring it to OSX.

Stay tuned for more information on what the future holds.
