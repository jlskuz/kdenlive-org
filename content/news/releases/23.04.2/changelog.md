  * Fix freeze on duplicate sequence. [Commit.][2] See bug [#470465][3]
  * Get rid of mocking in some more tests. [Commit.][4]
  * Re-enable timeline sequence thumbnails. [Commit.][5]
  * Fix tests. [Commit.][6]
  * Nesting: refactor timeline sequence open/close, add tests. [Commit.][7]
  * Display default folder for VOSK models in settings page when no custom folder is set. [Commit.][8]
  * Fix closing sequence in reopened project losing all recent changes. [Commit.][9]
  * Fix crash pasting subtitle in a timeline sequence without subtitles. [Commit.][10]
  * Fix dragging clip in timeline can cause out of view scrolling. [Commit.][11]
  * Fix motion tracker not working on rotated clips or clip with distort effects. [Commit.][12]
  * Fix import clip on single click in Media Browser. [Commit.][13]
  * Fix focus issue after switching from fullscreen monitor. [Commit.][14]
  * Cleaner version of previous patch. [Commit.][15]
  * Fix multiple guides export. [Commit.][16] Fixes bug [#469435][17]
  * Fix color wheel resetting color on mouse wheel. [Commit.][18] See bug [#470005][19]
  * Minor UI adjustments to timeline ruler. [Commit.][20]
  * Use better option for Media Browser. [Commit.][21]
  * Drop timeline zoom whatsthis (it interferes with zoom shortcut (Shift+Ctrl++). [Commit.][22]
  * Merge !399 with a few fixes (whisper disable FP16 on GTX 16xx). [Commit.][23]
  * Don&#8217;t allow archiving unsaved project, show subtitle files in archive widget and project files list. [Commit.][24]
  * Fix scaled rendering. [Commit.][25]
  * Titler: shadow should include text outline. [Commit.][26]
  * Thumbnailer: ensure producer is valid, don&#8217;t seek past clip end. [Commit.][27]
  * Fix create sequence from selection resulting in incorrect clip length. [Commit.][28]
  * Don&#8217;t trigger producer reinsert multiple times on change. [Commit.][29]
  
 [2]: http://commits.kde.org/kdenlive/a25516e9e2a08d7f6056a01cb11cd25c59f63bad
 [3]: https://bugs.kde.org/470465
 [4]: http://commits.kde.org/kdenlive/24eafb6bcf8691b46010cf0738dc7914832b564e
 [5]: http://commits.kde.org/kdenlive/a3c7897a0df8bd92819067c3e8b669c644d5d454
 [6]: http://commits.kde.org/kdenlive/79d65fb4dd058d7f5cd3393dff8e73bfea7259ad
 [7]: http://commits.kde.org/kdenlive/1e02d0baaa777d839f1fe9e3d12b60da9524b2fb
 [8]: http://commits.kde.org/kdenlive/785f56d78e5e6b3e1aad8ca504ee471958d793bb
 [9]: http://commits.kde.org/kdenlive/36b3d1110eb009d48426665153620a132d1c9fff
 [10]: http://commits.kde.org/kdenlive/ae3c7f60f500678764c8c05e6d6053b313718e7c
 [11]: http://commits.kde.org/kdenlive/6b56c14c56aa1710ebbf912a8514e675c9c7807d
 [12]: http://commits.kde.org/kdenlive/753b5984a2f2e76909f99f0d8ce804c08127f63c
 [13]: http://commits.kde.org/kdenlive/c65b4917be5eaaac413ecb746d7f363ea6a819a6
 [14]: http://commits.kde.org/kdenlive/99942a8752e8bc74fc3dc1bb8473137f960a524c
 [15]: http://commits.kde.org/kdenlive/155c44e4e431bb8748acb805471c0c1f544aa91e
 [16]: http://commits.kde.org/kdenlive/d88360736165c3c7c0fffa0aea6b6239d8b9f205
 [17]: https://bugs.kde.org/469435
 [18]: http://commits.kde.org/kdenlive/e1e8641c61cadb5709ce43c874a03166a750f0cf
 [19]: https://bugs.kde.org/470005
 [20]: http://commits.kde.org/kdenlive/1ff43505341da5457c0efdcf34f68cbea7af070b
 [21]: http://commits.kde.org/kdenlive/a80b08bd8561d146d64262dff456fe9d99c48f31
 [22]: http://commits.kde.org/kdenlive/58266682e5242082c5121570cf6b89a2b6bdf070
 [23]: http://commits.kde.org/kdenlive/4f3463e8256e3636adca5a2feffea898a4088bff
 [24]: http://commits.kde.org/kdenlive/f87960f015aeb589df0c2805eff034ec091c3f8e
 [25]: http://commits.kde.org/kdenlive/71f4138ac61fd2a31f0b2ddd717743fd35cb36fd
 [26]: http://commits.kde.org/kdenlive/2acc4e3d44643524d4450fab8d35e8b99cfbe96e
 [27]: http://commits.kde.org/kdenlive/97e72d574b553927649c846e0c137254ad4af607
 [28]: http://commits.kde.org/kdenlive/e9a242f32bbf38a9d71b39e9b4d6312e7b16cfef
 [29]: http://commits.kde.org/kdenlive/08a4e92cacafb459554c38338f08b9a3c086fea3
