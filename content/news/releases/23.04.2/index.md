---
author: Farid Abdelnour
date: 2023-06-15T13:01:55+00:00
aliases:
- /en/2023/06/kdenlive-23-04-2-released/
- /fr/2023/06/23-04-2/
- /it/2023/06/kdenlive-23-04-2-2/
- /de/2023/06/kdenlive-23-04-2/
discourse_topic_id:
  - 2148
---

Kdenlive 23.04.2 brings several bug fixes and enhancements to improve the stability of Timelines Sequences. Among the bug fixes, we addressed a freeze that occurred when encountering a duplicate sequence, sequence creation from selection, and re-enabling of sequence thumbnails. Other notable fixes include addressing problems with dragging clips in the timeline causing out-of-view scrolling, a crash when pasting subtitles in a timeline sequence without subtitles and timeline focus problems.

Furthermore, there are enhancements to our subtitling engines Vosk and Whisper and resolved issues related to project archiving, subtitle file display, motion tracker, color wheel resetting issue and scaled rendering.

Sysadmin news: as some of you may have perceived our website's commenting system has been integrated to the new [KDE forum][1]. So please leave your comments and feedback there.

[![](/images/banners/akademy-banner-2023.png)](https://akademy.kde.org/2023)


 [1]: https://discuss.kde.org/t/kdenlive-23-04-2-released/2148
