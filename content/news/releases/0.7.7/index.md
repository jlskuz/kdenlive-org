---
title: Kdenlive 0.7.7 information page
author: Jean-Baptiste Mardelle
date: 2010-02-10
aliases:
- /discover/0.7.7
# Ported from https://web.archive.org/web/20111228020056/http://www.kdenlive.org/discover/0.7.7
---

Kdenlive 0.7.7 was released on the 17th of february 2010.

This release fixes a lot of bugs reported against Kdenlive 0.7.6, including timeline corruption and various crashes. We also fixed a compatibility issue with Qt 4.6. We hope that this new release will make the video editing experience easier and more comfortable for everyone!

Kdenlive requires the latest release of the [MLT video framework][] (0.5.0)

## Some of the new features

* User selectable color schemes
* Improved keyboard navigation
* Timeline editing mode (normal, overwrite)
* Fix compatibility issue with Qt 4.6
* Allow shutdown after render when using Gnome Session manager
* Improved titler (now supports font outline)
* Editing properties for several clips at once (for example aspect ratio)

A complete list of the fixed issues can be found on our bugtracker.

  [1]: https://mltframework.org
