---
date: 2008-11-12
author: Jean-Baptiste Mardelle
---

![screenshot of Kdenlive 0.7](0.7.png)

We are glad to announce the immediate release of Kdenlive 0.7

This is the first release of Kdenlive for KDE 4. Here is a quick list of improvements over the previous KDE 3 version:

- Complete rewrite of the communication with the MLT video framework, which means a huge speedup in all timeline operations
- Capture from different sources: DV, HDV, webcam and screen grab
- Better KDE integration (notifications, job progress, Nepomuk annotations)
- More effects and transitons (improved support for Freior)
- Full undo support with history
- Video rendering is now completely independant from main application, you can safely work while rendering
- Initial support for Jog Shuttle devices

Kdenlive is currenlty translated in Czech, Dutch, Danish, Catalan, Spanish, German, French, and Italian.

Download Kdenlive:
Source code and info about binary packages is available on our download page.

To compile Kdenlive, make sure to check our Installing from source page. The easiest way to compile Kdenlive is to use the Builder Wizard which compiles Kdenlive, MLT and FFmpeg in a few clicks.

For more information, please check our website:
https://kdenlive.org

Bugs can be reported at:
https://kdenlive.org/mantis

For user questions, check our forums and IRC channel:
https://kdenlive.org/bbforum
IRC channel: #kdenlive on freenode
