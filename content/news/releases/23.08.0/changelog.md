  * Fix Save As behaving like Save a Copy. [Commit][1].
  * Allow speed effect on sequence/playlist items that already had it so it can be disabled in existing projects. [Commit][2].
  * Disable speed change on playlist and sequence clips, as it is unstable in MLT. [Commit][3].
  * When an MLT playlist proxy is missing, it should be reverted to a producer, not stay in a chain. [Commit][4].
  * Fix proxy clips not correctly recreated when missing on project open. [Commit][5].
  * Fix no luma files for NTSC and other non HD formats. [Commit][6].
  * Fix crash closing luma download dialog. [Commit][7].
  * Ensure saved effect position is relative for fades. [Commit][8]. Fixes bug [#473375][9].
  * Fix Save As incorrectly triggering backup file. [Commit][10].
  * Fix layout file warning. [Commit][11].
  * Fix path of LUT files on project opening, for appimage or imported projects. [Commit][12].
  * Fix composition incorrectly selected after added through clip corner shortcut. [Commit][13].
  * Fix crash closing project with mixes. [Commit][14].
  * Fix timeline keyframe view broken afer remove keyframes or undo effect deletion. [Commit][15].
  * Don&#8217;t incorrectly update timeline duration on every clip move. [Commit][16].
  * Improve highly inefficient loop. [Commit][17].
  * Fix warnings. [Commit][18].
  * Fix melt path config variable not renamed everywhere. [Commit][19].
  * Fix timeline zone not correctly loaded on project open. [Commit][20].
  * Fix timeline preview incorrectly connected when opening pre-nesting project file. [Commit][21].
  * Fix crash tring to import DJI image stream. [Commit][22]. Fixes bug [#467375][23].
  * Fix crash dragging multistream clip outside timeline. [Commit][24].
  * Fix play icon incorrectly active when playing from end, fix timeline not playing from start. [Commit][25]. Fixes bug [#473087][26].
  * Fix crash on audio record with multiple timeline sequences. [Commit][27].
  * Ensure lines and dust effects have in/out defined. [Commit][28].
  * Fix pasting keyframeble effect from one clip to another breaks if destination clip is shorter. [Commit][29].
  * Fix timeline preview (and possible rendering) crash on title clips. [Commit][30]. See bug [#472913][31].
  * Fix timeline preview not loaded on project opening. [Commit][32].
  * Fix missing effects/compositions when rendering a project with several sequences. [Commit][33].
  * Ensure Kdenlive&#8217;s renderer path is always found or fixed. [Commit][34].
  * Disable mediabrowser constantly checking for directory changes when widget is not visible. [Commit][35].
  * Fix Render Widget url dialog not working properly (cannot choose another folder). [Commit][36].
  * Fix timeline focus when a drop quickly exits an existing timeline clip and that the exit event is not triggered. [Commit][37].
  * Fix freeze effect to select current playhead frame by default. [Commit][38].
  * MLT does not support animated colors for avfilter. [Commit][39]. Fixes bug [#472722][40].
  * Fix crash on project close introduced in last commit. [Commit][41].
  * Ensure sequence name is elided in master effectstack button. [Commit][42].
  * Fix Lift Gamma Gain not updating when lift slider was moved, and color reset when slider was moved to 0. [Commit][43]. Fixes bug [#447948][44]. Fixes bug [#470005][45].
  * Fix normalize (2 pass) and motion tracker not working. [Commit][46]. Fixes bug [#444731][47].
  * Fix sequence name not appearing in Master stack button. [Commit][48].
  * Fix crash corruption on undo/redo create/close sequence. [Commit][49].
  * Fix crash when attempting to duplicate a sequence not currently opened. [Commit][50].
  * Propose the original project name when doing save as. [Commit][51]. Fixes bug [#472255][52].
  * Display sequence name in Master stack button when only 1 sequence is opened. Fixes #1727. [Commit][53].
  * Make Toggle all tracks lock consistent, toggling all tracks. [Commit][54]. Fixes bug [#472451][55].
  * Add missing suspension points. [Commit][56]. Fixes bug [#472575][57].
  * Various fixes for the custom clip job manager(fix renaming job, fix parameters containing spaces, add description field). [Commit][58].
  * Ensure we don&#8217;t attempt to connect timelines on project close. [Commit][59].
  * Fix project corruption: mapped ids should only be used on first opening of a sequence. [Commit][60].
  * Revert recent change setting in/out on all filters, breaks all keyframable effects. [Commit][61]. See bug [#471297][62].
  * For some reason, the timeline QQuickWidget does not receive EnterEvent after another dialog is opened/closed like Appp Command bar. [Commit][63]. Fixes bug [#472466][64].
  * Fix play zone breaking audio playback. [Commit][65]. Fixes bug [#472324][66].
  * Fix incorrect tab focused in Settings > Environnment. [Commit][67].
  * When a clip only has 1 audio stream, directly re-assign it on toggle track target. [Commit][68]. Fixes bug [#472400][69].
  * Add actions to focus previous / next timeline tab. [Commit][70]. Fixes bug [#472408][71].
  * Fix possible crash on item select if there is no defined active track. [Commit][72].
  * Fix sequence incorrectly initialized on project opening. [Commit][73].
  * Fix crash trying to select a transition or mix when active track is subtitle. [Commit][74].
  * Fix broken color parameter for some effects (producer color in MLT does not support color animation). [Commit][75]. Fixes bug [#472449][76].
  * Initial fix for broken effects caused by switch to MLT chains. [Commit][77]. See bug [#471297][62].
  * Refactor custom clip job manager to allow better syntax (2 parameters allowed) and work on images. [Commit][78].
  * Fix another opening crash. [Commit][79].
  * Fix crash on project open. [Commit][80].
  * Make clip monitor used sequence yellow indicator relative to active timeline only. [Commit][81].
  * Fix clip monitor not refreshed when applying bin clip effect. [Commit][82].
  * Fix tests. [Commit][83].
  * Delete unused var. [Commit][84].
  * Fix custom clip jobs not using FFmpeg. [Commit][85].
  * Fix updating template title text sometimes crashing and not updated in Project Bin. [Commit][86].
  * Fix tests. [Commit][87].
  * Subtitle edit: don&#8217;t try to apply font if not defined. [Commit][88].
  * Cleanup and disable checking pointer count until properly fixed in tests. [Commit][89].
  * Fix some models not not released on project close. [Commit][90].
  * Fix Text based edit widget width incorrectly using all space on long clip name. [Commit][91].
  * Fix Export frame incorrect dialog label. [Commit][92].
  * Subtitle: add zoom option to make edit widget (not subtitle) font larger. [Commit][93].
  * Add character count in subtitle edit. [Commit][94].
  * Ensure luma transition considers padding as transparent. [Commit][95]. Fixes bug [#456830][96].
  * Fix crash on app quit with selected effect stack. [Commit][97].
  * Fix Pan and Zoom effect moving objects after last keyframe. [Commit][98]. Fixes bug [#471954][99].
  * Fix groups not passed on duplicated sequence. [Commit][100].
  * Add GL/GH as external proxy option. [Commit][101]. See bug [#464644][102].
  * Fix duplicate sequence corrupts project file. [Commit][103].
  * Add an option to adjust timeline zone to selection (Shift Z) as default shortcut. [Commit][104]. Fixes bug [#472018][105].
  * Add GOPR as external proxy option. [Commit][106]. See bug [#464644][102].
  * Fix composition pasting. [Commit][107]. Fixes bug [#472079][108].
  * Objectid refactoring. [Commit][109].
  * Remove useless check. [Commit][110].
  * Add AV1 NVENC profile. [Commit][111].
  * Temporarily revert a change that causes crash on project opening (Correct fix pending). [Commit][112].
  * Clip monitor: correctly display last frame&#8217;s zone, ensure last frame is used when no zone is selected. [Commit][113].
  * Fix clip monitor zone out point inconsistencies. [Commit][114].
  * Fix timeline zone not correctly including last frame of the selection. [Commit][115].
  * Fix a few ASAN use after free issues. [Commit][116].
  * Rotate and Shear effect: expose and disable by default the repeat/mirror keyframe property. [Commit][117].
  * Add proxy rule for DJI .LRF files. [Commit][118].
  * Add test for disappearing timeline effects, and some fixes for a random nesting test heap after use. [Commit][119].
  * Fix missing header. [Commit][120].
  * Fix missing tests include. [Commit][121].
  * Fix tests headers broken by clang-format. [Commit][122].
  * Move timeline next_id counter to KdenliveDoc class, ensure it is reset to 0 when a new project is opened. [Commit][123].
  * Fix Qt6 compilation issue. [Commit][124].
  * Make KF Purpose a required dependency. [Commit][125].
  * [DocumentChecker] First step of refactoring, add tests. [Commit][126].
  * Fix. [Commit][127].
  * Revert wrong change. [Commit][128].
  * Port away from deprecated KUrlRequester::setFilter(). [Commit][129].
  * [Code Gardening] Remove unused and duplicated code, fix warnings. [Commit][130].
  * Fix some properties not correctly initialized in testing KdenliveDoc. [Commit][131].
  * Properly close timelines after test, fixes mix test. [Commit][132].
  * Move detection of kdenlive_renderer to Wizard. [Commit][133].
  * Fix mixes direction and add tests. [Commit][134].
  * Work/asan profile. [Commit][135].
  * Fix linking error. [Commit][136].
  * Fix compilation with KF < 5.100. [Commit][137].
  * [Renderer] More refactoring, move backend code to seperate file. [Commit][138].
  * [Renderer] Reorder code to split UI and backend code. [Commit][139].
  * Fix QString QUuid conversion (Qt6). [Commit][140].
  * Refactor code for multi-guide rendering. [Commit][141].
  * Default KF\_MAJOR to QT\_VERSION_MAJOR instead of 5. [Commit][142].
  * Try to fix Qt6 build. [Commit][143].
  * More tests fixes. [Commit][144].
  * More tests fixes. [Commit][145].
  * Fix thumbnail profile ASAN crash and recent regression in group move. [Commit][146].
  * Fix ASAN use after freed. [Commit][147].
  * Group move: don&#8217;t delete/re-add clips if move on other track is not possible, fix ASAN free after use. [Commit][148].
  * Make use of new QStringUtils. [Commit][149].
  * Add QStringUtils with common actions around strings. [Commit][150].
  * Move logic to disable subtitles to its own function in KdenliveDoc. [Commit][151].
  * Make KdenliveDoc::useOriginals() static. [Commit][152].
  * Move image sequence check to RenderPresetParams. [Commit][153].
  * Add Xml::docContentToFile to reduce code duplication. [Commit][154].
  * Get rid of mocking in some more tests. [Commit][155].
  * Re-enable timeline sequence thumbnails. [Commit][156].
  * Fix tests. [Commit][157].
  * Nesting: refactor timeline sequence open/close, add tests. [Commit][158].
  * Fix compilation with KF 5.98. [Commit][159].
  * Update file format documentation. [Commit][160].
  * Use a different assetList.qml file with Qt6. [Commit][161].
  * Simplify header names as required with KF6. [Commit][162].
  * Use std function for sleep() to work on Windows too. [Commit][163].
  * Fix QtVersionChecks does not exist in Qt5. [Commit][164].
  * Next try to fix. [Commit][165].
  * Another try to fix includes. [Commit][166].
  * Simplify includes (required with KF6). [Commit][167].
  * KNS Dialog has a new name in KF6. [Commit][168].
  * QTextCodec is gone in Qt6. [Commit][169].
  * Fix import. [Commit][170].
  * The KSelectAction trigger signal was renamed in KF6. [Commit][171].
  * In KF6 the color scheme menu was moved. [Commit][172].
  * Remove unused headers. [Commit][173].
  * PurposeMenu header was renamed in KF6. [Commit][174].
  * KDeclarative (in the way we use it) is gone in KF6. [Commit][175].
  * Make it possible to configure build with KF6. [Commit][176].
  * Hw profiles, also support -c:v format for video codec. [Commit][177].
  * Fix typo disabling hw encoders for proxy and timeline preview. [Commit][178].
  * Cleanup and improve Hardware encoders detection and selection for Timeline preview and proxy clips. [Commit][179].
  * Fix shortcut conflicts with Media Browser and double click after changing view. [Commit][180].
  * Fix broken focus after monitor was switched to fullscreen. [Commit][181].
  * Fix warnings. [Commit][182].
  * Fix broken code for KF >= 5.100. [Commit][183].
  * Fix shortcut conflicts. [Commit][184].
  * Rewrite the Media Browser, add an option in the menu to open the files in the default app instead of importing on double click. [Commit][185].
  * Add CD jobs for macOS. [Commit][186].
  * Add test for render profiles with no parameters. [Commit][187].
  * Switch from QScopedPointer to std::unique_ptr. [Commit][188].
  * Add option to automatically adjust tracks height to timeline height. [Commit][189].
  * Add an option to manually disable FP16 on Whisper in settings page. [Commit][190]. See bug [#467573][191].
  * Don&#8217;t show clip job progress bar for Bin load job. [Commit][192].
  * Enable opening https url for the doc on Mac. [Commit][193].
  * Fix starting help browser in AppImage. [Commit][194].
  * Show our documentation website instead of deprecated handbook. [Commit][195].
  * Fix minor typo. [Commit][196].
  * [Render Options] Make multi export independent of the range. [Commit][197].
  * Improve bin filters. [Commit][198].
  * Add OpenCV Nano tracker, requires latest MLT git. [Commit][199].
  * Fix incorrect conversion from QString to QUuid. [Commit][200].
  * [dev-docs] ppa package names follow ubuntu now. [Commit][201].
  * Fix capitalisation of &#8220;English&#8221;. [Commit][202].
  * Delete selection model in effectstask to avoid possibly incorrect indexes. [Commit][203]. See bug [#467515][204].
  * Make Bin filter use an OR when filtering in the same category. [Commit][205].
  * Attempt to fix thumbnailer crash. [Commit][206].
  * Made descriptions of projectclips editable again. [Commit][207].
  * Try to fix ki18n issue on win/mac. [Commit][208].
  * Try to improve audio distortion in the first frames after play. [Commit][209].
  * KUIT text needs to be xhtml. [Commit][210].
  * Small typos. [Commit][211].
  * Add &#8220;Cut subtitle after first line&#8221;. [Commit][212].

 [1]: http://commits.kde.org/kdenlive/c1cfed5aa50b7966c38b55640067a0edbb1c40ac
 [2]: http://commits.kde.org/kdenlive/4258660de2aa981eb20c928234395321f49aad0b
 [3]: http://commits.kde.org/kdenlive/24f8712f7ced7b35ff60ab84094d2e3239568f3a
 [4]: http://commits.kde.org/kdenlive/5d21d436136a81c2ba263de8949402834e4c5cb3
 [5]: http://commits.kde.org/kdenlive/352a9b8888cce9b75580b53b32c692bede9083d3
 [6]: http://commits.kde.org/kdenlive/555aefd5a8d87f1040f09bdda1911544c530d8d8
 [7]: http://commits.kde.org/kdenlive/c8846cef6c8d5de5f5f1fb4438bb5269218d9c0d
 [8]: http://commits.kde.org/kdenlive/1e60132a672e68d35342e6e31bf99984dddf784a
 [9]: https://bugs.kde.org/473375
 [10]: http://commits.kde.org/kdenlive/c1dd9e8e58a4819562c89255af9e8b17eee8b0b6
 [11]: http://commits.kde.org/kdenlive/c5c0944fa91f6d25039484b9f949f300c727f51c
 [12]: http://commits.kde.org/kdenlive/bce0c589417031b12bbe9c92a39ad66be4228289
 [13]: http://commits.kde.org/kdenlive/fde3861ed13943d1c500af25b40fc2904f8aebef
 [14]: http://commits.kde.org/kdenlive/67e7b33b8d119540dd22c285d0d8930c71c7a3c3
 [15]: http://commits.kde.org/kdenlive/86f173a86b342a7a356adcbb9375688bd367fa24
 [16]: http://commits.kde.org/kdenlive/4935790ea3e151568f5d20217c65223b69d31317
 [17]: http://commits.kde.org/kdenlive/fcb12a7469f54024aae110fc6c64d3a6e98e26dc
 [18]: http://commits.kde.org/kdenlive/ed478f8a062102ee1a331e49ae863a574290ab26
 [19]: http://commits.kde.org/kdenlive/f98ad74a2d757203024b7789fd6860286eb861b0
 [20]: http://commits.kde.org/kdenlive/e83ed7ac40a113b81ed03fadaef50f3a7f4bfa2d
 [21]: http://commits.kde.org/kdenlive/76207e8e1e23f682e1614d2f8c6f70206918872b
 [22]: http://commits.kde.org/kdenlive/04bbb2200e23cba1e956a69832826a6fbd859535
 [23]: https://bugs.kde.org/467375
 [24]: http://commits.kde.org/kdenlive/a124ab0af513553715e30a5ad984a69dd446176a
 [25]: http://commits.kde.org/kdenlive/8dcedfec03a5c8b4276c5c5d5ef772b700b8aa5e
 [26]: https://bugs.kde.org/473087
 [27]: http://commits.kde.org/kdenlive/980d950ee2c1e6d1e1a43ed0250457de768b5b8c
 [28]: http://commits.kde.org/kdenlive/c3a7cb21eb305676f88aa0876f46aee9cf18d2d7
 [29]: http://commits.kde.org/kdenlive/1c58561a7b82954f5bdb1cd17c423dcdd117eb57
 [30]: http://commits.kde.org/kdenlive/3c93355a75dedc18700fd84003f7f54e44152c91
 [31]: https://bugs.kde.org/472913
 [32]: http://commits.kde.org/kdenlive/14ffb91839a8e87fca5ce2f7b2017d31f9a4bc13
 [33]: http://commits.kde.org/kdenlive/d27ce50f086079ca7100f39a2d98584ff00279d1
 [34]: http://commits.kde.org/kdenlive/d78f3cb4b273423e7e8a73316d53f1c5a81e87ce
 [35]: http://commits.kde.org/kdenlive/dd49b921a887b60c2528fb9f5bf65ff1c505b561
 [36]: http://commits.kde.org/kdenlive/e6b562ae3fa9f8f4ac39e1ce0890b51635d8521e
 [37]: http://commits.kde.org/kdenlive/f73aa1fd59958f6304a4670a74222361d85c5689
 [38]: http://commits.kde.org/kdenlive/129d817cf82cffef52f938f14f4aef8851732a5b
 [39]: http://commits.kde.org/kdenlive/d50377eb1f832fe5b5d1cd59c5bfb350fb8eca4c
 [40]: https://bugs.kde.org/472722
 [41]: http://commits.kde.org/kdenlive/a9025c520048d615bca0dc7769330dbfc92c1393
 [42]: http://commits.kde.org/kdenlive/069a7675f7972800820250e26bfafb7ccaf9fd7b
 [43]: http://commits.kde.org/kdenlive/248fa19000aceae1eccdef569a7aed2334c20997
 [44]: https://bugs.kde.org/447948
 [45]: https://bugs.kde.org/470005
 [46]: http://commits.kde.org/kdenlive/8195ee309d6da0e81abb6df5932dd3233036993b
 [47]: https://bugs.kde.org/444731
 [48]: http://commits.kde.org/kdenlive/426180a7ec6f32014f5b74196cf53b815f28bd14
 [49]: http://commits.kde.org/kdenlive/277c4297de3e2dfeee66dc7ed59d47c795fadb50
 [50]: http://commits.kde.org/kdenlive/8fa3061100a821ef41967b44ba5ce9d23859bb87
 [51]: http://commits.kde.org/kdenlive/5b7227aab752fa1bc523339e1e9b63f5d81db731
 [52]: https://bugs.kde.org/472255
 [53]: http://commits.kde.org/kdenlive/5a742955d6bc1082614b37e469e75772ce57e4ad
 [54]: http://commits.kde.org/kdenlive/2d87a5ad197bcf55bda1b4e8f794c042a20874cb
 [55]: https://bugs.kde.org/472451
 [56]: http://commits.kde.org/kdenlive/6e2efcfa16045dfc3fbb7456b4ec0bdebf1fe8bb
 [57]: https://bugs.kde.org/472575
 [58]: http://commits.kde.org/kdenlive/11b1b8ca6fa6a3d98c005ee03a0620f3c6bb8db9
 [59]: http://commits.kde.org/kdenlive/9077dd8b4ce2d3ef0698897996107e5c078623d2
 [60]: http://commits.kde.org/kdenlive/e9d14000cdf0cebbd72c34add195e6ba0bb62152
 [61]: http://commits.kde.org/kdenlive/526e8b33082c74eaabaeb6176d7e98fbd3f6d5e7
 [62]: https://bugs.kde.org/471297
 [63]: http://commits.kde.org/kdenlive/a4385ba1380008cdd8a7c97e98999f86c48c7784
 [64]: https://bugs.kde.org/472466
 [65]: http://commits.kde.org/kdenlive/3e2f75cefb6b04f05b4dbfdb076c60ef5f20be72
 [66]: https://bugs.kde.org/472324
 [67]: http://commits.kde.org/kdenlive/5307d5ebb454873e15e71bd099cc01296ece7435
 [68]: http://commits.kde.org/kdenlive/6c55512b823c2b499a702d879b606ea2b3b095df
 [69]: https://bugs.kde.org/472400
 [70]: http://commits.kde.org/kdenlive/7354cbc336b078bc8e30c348f73719a995b70012
 [71]: https://bugs.kde.org/472408
 [72]: http://commits.kde.org/kdenlive/7935d6db539182100eb71cad75b252fe9f58a8db
 [73]: http://commits.kde.org/kdenlive/04b4203b85e4660289e61e3492cd22cb116ae7e4
 [74]: http://commits.kde.org/kdenlive/c6b698f8ee7ddaeec473fde14cb004b0cf2e1b77
 [75]: http://commits.kde.org/kdenlive/6feb13f87e6b16b9a3917ae6d66da7d876a9e444
 [76]: https://bugs.kde.org/472449
 [77]: http://commits.kde.org/kdenlive/96d5d34a107d1b64aa9f39ad118befd94e86a9e4
 [78]: http://commits.kde.org/kdenlive/68fb174c0090b8f0912d8216bdc85ccc71ef5e69
 [79]: http://commits.kde.org/kdenlive/272643aebf2a46d91a5104ebcde48f4b86b26f27
 [80]: http://commits.kde.org/kdenlive/97f80e7a3ad27e9dedbd46bebf7c01da6b960a85
 [81]: http://commits.kde.org/kdenlive/c38a7a836ded9a932bf8ad7395450f06da0102de
 [82]: http://commits.kde.org/kdenlive/bba15468833403c943ac14d23a106fe98a61f730
 [83]: http://commits.kde.org/kdenlive/7d9560800c4377939553800039c461770bb8567e
 [84]: http://commits.kde.org/kdenlive/0d45bcfda845446c0dc02d75abf5ee0fb6136cf6
 [85]: http://commits.kde.org/kdenlive/6ba385d41f19b047a592671131be8e739f14052c
 [86]: http://commits.kde.org/kdenlive/0a29e7074f0fd3b3b7c785bb2bb394ccec1fdd5c
 [87]: http://commits.kde.org/kdenlive/7565ae25b828714836141d9f2f0265ca6eff0c45
 [88]: http://commits.kde.org/kdenlive/23ebfa95e1fe5a5683a016f111bf941428f4799b
 [89]: http://commits.kde.org/kdenlive/9bc03c247c53f60669c023f3bcc779b306ad66d9
 [90]: http://commits.kde.org/kdenlive/73900e8ba8654d880c3bcee9b2ba00b79ce6a7fa
 [91]: http://commits.kde.org/kdenlive/6d89d3efa6f0dafd5951c1ace7e3cf8e3af658e4
 [92]: http://commits.kde.org/kdenlive/0c7c6354bcce616260af481301137a1043ee91df
 [93]: http://commits.kde.org/kdenlive/ffbc43e69ef80d6830883f206e91ae36000f638b
 [94]: http://commits.kde.org/kdenlive/5bcbbfe26c31603f376fd3653b274b570db73c04
 [95]: http://commits.kde.org/kdenlive/d1178a66be64026c5e8b24f70b19d33a8684875c
 [96]: https://bugs.kde.org/456830
 [97]: http://commits.kde.org/kdenlive/b8319ef39d2bfe73c67c59e24cb04cf915731bf0
 [98]: http://commits.kde.org/kdenlive/7314570a93ef4de3c0b7b21ef95b30e29525ef80
 [99]: https://bugs.kde.org/471954
 [100]: http://commits.kde.org/kdenlive/a1f3778f472f2605710238e24cca06134a6f0765
 [101]: http://commits.kde.org/kdenlive/f46975b42b4c8b530e680075a73f2bff1473e047
 [102]: https://bugs.kde.org/464644
 [103]: http://commits.kde.org/kdenlive/94b5face723b7ce86a6438e937a2363bff5af597
 [104]: http://commits.kde.org/kdenlive/7eceb4c7e9544fcc479b608c8efb8f277283c6d8
 [105]: https://bugs.kde.org/472018
 [106]: http://commits.kde.org/kdenlive/3df9c96b2995becca9b8ad42c08265ff34a2a19c
 [107]: http://commits.kde.org/kdenlive/d4c7f1bcff86f7e133b1ef459bb7921c46dfd87b
 [108]: https://bugs.kde.org/472079
 [109]: http://commits.kde.org/kdenlive/b03feb6af22a021a8cd2636689a76023c7d57fbc
 [110]: http://commits.kde.org/kdenlive/bfb2c2498786704d5e6ec8d66bf1b2ad6c0dc12d
 [111]: http://commits.kde.org/kdenlive/838a68259ae3c7992d8f2e289cf9edf6bafa1ccc
 [112]: http://commits.kde.org/kdenlive/3febbc2fd320dd6769e0b7709c7077df3e401e23
 [113]: http://commits.kde.org/kdenlive/b2a902c2ac3132f3792011d01e069bd37db2a81d
 [114]: http://commits.kde.org/kdenlive/18eaabec09f4b6c4e67b76c17f3388d075389530
 [115]: http://commits.kde.org/kdenlive/6f086c9453d68168fb3a28c68ee94bc1d22ad899
 [116]: http://commits.kde.org/kdenlive/98fac7c4cb1e09028dfe21984c0d513336e8fc6e
 [117]: http://commits.kde.org/kdenlive/aee6154a031c3dca58df20b663dde1b6dc45bd5e
 [118]: http://commits.kde.org/kdenlive/e648b25b759374727677be108c171f10738c801d
 [119]: http://commits.kde.org/kdenlive/7bb5f92100ab5ca3a951f9f2394d1409ffff2a6c
 [120]: http://commits.kde.org/kdenlive/bc00d5b86f29afd340952a7d41116c46de3842d4
 [121]: http://commits.kde.org/kdenlive/16b160b521ddaac28d71d3851eda7098e7312322
 [122]: http://commits.kde.org/kdenlive/0ad7c40eafdefe6fae8ce781740b95cc8105d51e
 [123]: http://commits.kde.org/kdenlive/dd574a2407086609277c163360047fe5e09dccc4
 [124]: http://commits.kde.org/kdenlive/1da86265fd16e694f0a6f1619a76ce8e8e597d95
 [125]: http://commits.kde.org/kdenlive/6c229184604c2c339ff9b7676b33577c9795cbc3
 [126]: http://commits.kde.org/kdenlive/40170c72919b80747a333c297f30d955fea7cfd8
 [127]: http://commits.kde.org/kdenlive/f649f0e54d78a6283b6664f5cfd42854a0e9793f
 [128]: http://commits.kde.org/kdenlive/dee72dbdd956c00da7c9628ef7f86830aa0ad0ce
 [129]: http://commits.kde.org/kdenlive/d500368f8577e46f5e56016ec281e3df6fae6fad
 [130]: http://commits.kde.org/kdenlive/08efca009e24fd83a1e985dae131de2a33e3f40f
 [131]: http://commits.kde.org/kdenlive/e81c458b197d08a1010ef3abe8f613a6451412bb
 [132]: http://commits.kde.org/kdenlive/6ff20863ade24d00f284a909bc83b08fac0ae14a
 [133]: http://commits.kde.org/kdenlive/8e4d2c4ed54df2de37f057a57e89d51cc5779274
 [134]: http://commits.kde.org/kdenlive/de09d85da093804336684e0a303d15376d91fec0
 [135]: http://commits.kde.org/kdenlive/4d3f56cf3eb013ce0319eca54fe0a742634708e6
 [136]: http://commits.kde.org/kdenlive/14c7b58bada3d25bb51a0a860ed256bad0c7c437
 [137]: http://commits.kde.org/kdenlive/688e06692cf72298f7903f71a74f7b7a5e299ae7
 [138]: http://commits.kde.org/kdenlive/1644c9eccc1e72d4fa75f5d6076bda0451699a97
 [139]: http://commits.kde.org/kdenlive/19268290daa6ed18fd436dc90134a1aa11c4cfb0
 [140]: http://commits.kde.org/kdenlive/54bf128160620ac30f1f83e30f986e6ad22c955d
 [141]: http://commits.kde.org/kdenlive/75878510a6ada7775b41f7aa41436ffc8ae51a20
 [142]: http://commits.kde.org/kdenlive/a566ed534903fa3611bf6f41e44ed9247d5a555a
 [143]: http://commits.kde.org/kdenlive/7148fb1b2586777c934058800c0ceabc886d8381
 [144]: http://commits.kde.org/kdenlive/c7820d48c2d4a3099948352d593b62295d23a247
 [145]: http://commits.kde.org/kdenlive/e793a69022c850934f418b1f0f3a1f042f03808c
 [146]: http://commits.kde.org/kdenlive/c88f6036420b72c34158b914db557ac73ca6b7db
 [147]: http://commits.kde.org/kdenlive/085946ba783d9563702cb8083f8f65e53ef68896
 [148]: http://commits.kde.org/kdenlive/5bc2166d14abb2829e9db1c95ff2283e08a35b86
 [149]: http://commits.kde.org/kdenlive/af579205d3612e4c014ddb2465dd394989b77082
 [150]: http://commits.kde.org/kdenlive/b65eb05c46a3c06dae5c8872fc5c2fa999f53671
 [151]: http://commits.kde.org/kdenlive/9099451ae1c8b6a155b603f0776307813cb84fb0
 [152]: http://commits.kde.org/kdenlive/f1a4f59ba1e547383f39d5eb1a5eba8d7e7a071e
 [153]: http://commits.kde.org/kdenlive/949e4994b97bb52031280ceaad1162db9eddb557
 [154]: http://commits.kde.org/kdenlive/8871caad7956957ede972aab6221a6091687ed0d
 [155]: http://commits.kde.org/kdenlive/c2b95ffa1d0fd92282ac84a76e6b901a5b80cd6c
 [156]: http://commits.kde.org/kdenlive/7155611d38c63e92933e5b2229b6a4d5137d0cdf
 [157]: http://commits.kde.org/kdenlive/c579d02a90f969c07ff06d16d7089583ada94493
 [158]: http://commits.kde.org/kdenlive/ccf568e7716326b3e1adef3f9785a3d4874c440f
 [159]: http://commits.kde.org/kdenlive/78ab65dbefc502217649cc568a4159ddded85d93
 [160]: http://commits.kde.org/kdenlive/d523744cad3d66f0a27eab3d39a1b9ede88bf938
 [161]: http://commits.kde.org/kdenlive/aea536a1cf970c0f598aefe7e3421ec5a1f7d73b
 [162]: http://commits.kde.org/kdenlive/26e134589a343c85c1a6219b1c5dfc576efb4ecb
 [163]: http://commits.kde.org/kdenlive/1e58609a8a54854bb6f0a2c815de722860c6c822
 [164]: http://commits.kde.org/kdenlive/7c67046535ca864c7a3f22994baa6dbfd1a465fa
 [165]: http://commits.kde.org/kdenlive/0414f8cfb1448d643a16e44c852f20ff87f6d0f1
 [166]: http://commits.kde.org/kdenlive/26f8725d0f394f7bee5e116a1abc9aa525c0b22f
 [167]: http://commits.kde.org/kdenlive/3bfcaa8bea77bbf392abe888816091d1dbfc55bc
 [168]: http://commits.kde.org/kdenlive/367af818a024849b3848fe0760ccdd136f269a52
 [169]: http://commits.kde.org/kdenlive/488508310b1aae318dcc287de38a256745427bfb
 [170]: http://commits.kde.org/kdenlive/35b96ccb039ec324684dfc1f6921d5a04c96106b
 [171]: http://commits.kde.org/kdenlive/625792b15cbadf3fc4d5ab18ec3bb07d8e576a73
 [172]: http://commits.kde.org/kdenlive/adc5512abb26f75174021c5a81690fa125468694
 [173]: http://commits.kde.org/kdenlive/1b9308a4a277ee7b4768617fbc29ce54b8513caa
 [174]: http://commits.kde.org/kdenlive/23e3451992c53058f71c70ebd93fc5f70714822d
 [175]: http://commits.kde.org/kdenlive/42671f3831121dd410878a8b08f1f9d7e863479e
 [176]: http://commits.kde.org/kdenlive/29540d2d57e2a32a465c504221804b86d3b28e4e
 [177]: http://commits.kde.org/kdenlive/90f5c6b114d56c5dbfb87408950e3a72e5170e7f
 [178]: http://commits.kde.org/kdenlive/57552c53b4c67699e4efc15be87938d4a6a8eef0
 [179]: http://commits.kde.org/kdenlive/3c2a5878a40140b1a0b405381b1a9523dbdcd0f9
 [180]: http://commits.kde.org/kdenlive/72d2515c40a47318d7f5c6a8d867546634fbdef8
 [181]: http://commits.kde.org/kdenlive/8162b770f829d4114529b0a8e55cf25fc9d471f9
 [182]: http://commits.kde.org/kdenlive/dbe517d1df3f638241e113a04eac0b10dd5e466d
 [183]: http://commits.kde.org/kdenlive/bf949edd6b792fcc66e69844b0b72494d9ad78b4
 [184]: http://commits.kde.org/kdenlive/4ebd483598bcd4a708231fc9f19dd16e15d123e7
 [185]: http://commits.kde.org/kdenlive/a5770effe2f6095233321ca2dde1136306bc1765
 [186]: http://commits.kde.org/kdenlive/b8099ea009eed6c400016817b746086e99d013cc
 [187]: http://commits.kde.org/kdenlive/f26e3e8ebe7a336c68c5205c0a2f39840a0b8d1d
 [188]: http://commits.kde.org/kdenlive/4fd03da6c8774130b9f6001420f66e14956a3045
 [189]: http://commits.kde.org/kdenlive/659c58bdc71966a78d8bb6e8edb2fbadd9efa6d1
 [190]: http://commits.kde.org/kdenlive/856fdf59a631e53aa0ce94decd5d8f921c135f28
 [191]: https://bugs.kde.org/467573
 [192]: http://commits.kde.org/kdenlive/d40e3c196362793478605c16da9c6625b2953e93
 [193]: http://commits.kde.org/kdenlive/9bc98846442b07c7aa24fde651e75089d13a1e5a
 [194]: http://commits.kde.org/kdenlive/865f2e2a1ce493f1170da51ab4a1bdc241c9a5bb
 [195]: http://commits.kde.org/kdenlive/d639e45af7b835d7760187e55a7df4e5966704fa
 [196]: http://commits.kde.org/kdenlive/80f3819d895dc55ba3131d9e7b70a0e17a902200
 [197]: http://commits.kde.org/kdenlive/fdd1cdd52996a1a8aacd852a3d96ee15a3365b9c
 [198]: http://commits.kde.org/kdenlive/bf9ff98f9764701fe39d95e8db31537ab2fa21b8
 [199]: http://commits.kde.org/kdenlive/22d9409627e4306145ffaba470818e5380d1c4ff
 [200]: http://commits.kde.org/kdenlive/69588d692d9d792b8dc9499af3ffc4317f74839f
 [201]: http://commits.kde.org/kdenlive/83397002d51b2e18ed8f0a0e0f9e979aa6e9d82e
 [202]: http://commits.kde.org/kdenlive/7bfa8193f46a3a49215e9f2e21bbe937397f84f1
 [203]: http://commits.kde.org/kdenlive/9add99b23b4afa29eb665855109ed71e72f23f87
 [204]: https://bugs.kde.org/467515
 [205]: http://commits.kde.org/kdenlive/8d407a54f3b9dea93b9e3e4c94a2087c38919faa
 [206]: http://commits.kde.org/kdenlive/01071eedc6716d1b5515d6f7dc12adabd7551872
 [207]: http://commits.kde.org/kdenlive/745b2512b9dbd7b77395ba387982bd626fd7ed5f
 [208]: http://commits.kde.org/kdenlive/6c595ddba22859ae2860a5bf1df8cdea3fc27a50
 [209]: http://commits.kde.org/kdenlive/259b9819ef8f67f17c8123a419e548d2003e0ebd
 [210]: http://commits.kde.org/kdenlive/cd7c102b5d5c74aee6ec16bde09a7bbc2365bcb0
 [211]: http://commits.kde.org/kdenlive/41742da38cd31229ef71bae65463749504c823af
 [212]: http://commits.kde.org/kdenlive/13a68bf47c57c51a53e78b3da6fcae26812ad0fb
