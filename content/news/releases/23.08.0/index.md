---
author: Farid Abdelnour
date: 2023-08-28T14:11:03+00:00
aliases:
- /en/2023/08/kdenlive-23-08-0-released/
featured_image_bg: images/features/2212.png
discourse_topic_id:
  - 4461
---

We're excited to present the latest version of Kdenlive, packed with an array of fixes, enhancements, and optimizations. Some highlights include:

  * 2 new effects, _Audio Seam_ and _Auto Fade,_ which can be added to tracks to eliminate audio cracks which might happen with some clips such as MKV and FLAC
  * New AV1 NVENC profile
  * Fixes to Whisper engine with Nvidia GTX 16xx series
  * Added GoPro _.lrv_ and DJI _.lrf_ files as external proxies
  * Fix Normalize (2 pass) effect
  * Added Nano Tracker algorithm to the Motion Tracker effect
  * Refactored Custom Clip Job Manager
  * Add _Cut subtitle after first line_ option
  * _Shift + double-click_ on the track divider resets the track back to its default height

Since this release focuses on resolving issues and enhancing stability most of the work was done under the hood but we managed to sneak in some nifty interface and usability improvements as well:

![](Peek-2023-08-27-14-28.gif "New Fit all tracks to view_ option")


![](Peek-2023-08-27-14-32.gif "Shift + z adjusts timeline zone to selected clips")


![](Peek-2023-08-27-15-39.gif "Toggle between timeline tabs using keyboard")


![](Peek-2023-08-27-15-47.gif "Added character count and zoom options to subtitle editor")
