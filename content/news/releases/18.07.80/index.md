---
title: 'Kdenlive 18.08 beta: test the future'
author: Jean-Baptiste Mardelle
date: 2018-07-04T13:12:08+00:00
aliases:
- /en/2018/07/kdenlive-test-the-future/
- /fr/2018/07/testez-le-futur-de-kdenlive/
- /it/2018/07/kdenlive-prova-il-futuro/
---

After more than 1.5 years of work, we are planning to release the refactoring version of Kdenlive in august, part of the KDE 18.08 Applications release. But taking such a decision is not easy. Most of the code was rewritten, which also means many possible regressions. So while we are very excited to have the opportunity to finally release our work to the public, it's also a bit stressful. So what now ?

![](18-08-beta.png)

Well that's where we need you. The latest refactoring code has been published as an AppImage, and we need some feedback. So you can help us by downloading the latest AppImage, and try it on your computer (just download the file, make it executable through your file manager and run it).

Improvements and new features include:

  * Clips with video and audio are now automatically separated when dropped in timeline
  * All clip types in timeline can now easily be disabled/enabled
  * Slowmotion should work reliably
  * The long standing issue where moving groups of clips corrupted timeline should be gone
  * Most effects now use a common keyframe interface
  * Insert/lift/overwrite should work reliably
  * Easily share your rendered video through KDE's Purpose library (YouTube, NextCloud, Twitter, Kde connect,…) requires a recent KF5 environment, not enabled on the AppImage.
  * It is now possible to generate lower resolution clips for the timeline preview to have a faster rendering
  * Resizable track height
  * Several overlay guides available for each monitor
  * New keyboard layouts can be installed in 1 click (we need help to create interesting layouts)

And most importantly, the code is now much cleaner and ready for new features.

You should be able to create new projects, add clips, move them in the timeline, add effects and compositions, and render your projects. But there are still a number of known issues that we are working on:

  * Compatibility with older kdenlive project files is not perfect
  * The curves effect does not work (Bézier curves works)
  * Composition settings don't update on resize
  * A performance issue in audio thumbnails causes major slowdowns on high zoom levels

Some feedback on this beta version would be really appreciated. So please help us, test this AppImage version and let us know what you think of it. You can leave comments in this post, or on our mailing list, and we will soon organize a bug squashing day to make this release as reliable as possible!

So we hope to hear from you soon!
