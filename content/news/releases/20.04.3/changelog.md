* Fix drop in timeline from clip monitor possible crash and disappearing clip (if mouse released above track headers). [Commit.](http://commits.kde.org/kdenlive/211212f70fb2813e6608d94ec7df1510ea2b2bdf) 
* Fix working on project with proxy only. [Commit.](http://commits.kde.org/kdenlive/703c0878ec9ecb153d7bf6ca331077a5f4341ee3) 
* Ensure we have a valid context before drawing keyframes. [Commit.](http://commits.kde.org/kdenlive/7ee8a90754b1f966bb5449e37a3a13192140ffeb) 
* Don't attempt activating a monitor if it is hidden. [Commit.](http://commits.kde.org/kdenlive/3051f9f235eafd2af0a4c86c361c67ad04538417) See bug [#422849](https://bugs.kde.org/422849)
