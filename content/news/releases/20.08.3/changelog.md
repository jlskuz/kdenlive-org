  * Fix on monitor displayed fps with high fps values. [Commit.][2]
  * Ensure timeline ruler is correctly updated on profile switch. [Commit.][3]
  * When switching project profile and there is only 1 clip in timeline, update the timeline clip duration accordingly to profile change. [Commit.][4]
  * When switching project profile and there is only 1 clip in timeline, update the timeline clip duration accordingly to profile change. [Commit.][5]
  * Project archiving: check after each file if archiving works, add option to use zip instead of tar.gz. [Commit.][6] See bug [#421565][7]
  * Fix opening project files with missing version number. [Commit.][8] See bug [#420494][9]
  * Fix duplicated audio from previous commit. [Commit.][10]
  * Fix playlist clips have no audio regression. [Commit.][11]
  * Fix keyframeable effect params left enabled when selecting a clip, leading to possible crash. [Commit.][12]
  * Don’t allow removing the only keyframe in an effect (was possible from the on monitor toolbar and crashing). [Commit.][13]
  * Fix crash inserting zone over grouped clips in same track. [Commit.][14]
  * Fix previous commit. [Commit.][15]
  * Check ffmpeg setting points to a file, not just isn’t empty. [Commit.][16]
  * Qtcrop effect: make radius animated. [Commit.][17]
  * Render widget: avoid misuse of parallel processing. [Commit.][18]
  * Fix resizing clip loses focus if mouse cursor did not get outside of clip boundaries. [Commit.][19]
  * Fix rounding error sometimes hiding last keyframe in effectstack. [Commit.][20]

 [2]: http://commits.kde.org/kdenlive/c29638fc49dc0d5fce695a02681f3743fc702d34
 [3]: http://commits.kde.org/kdenlive/edef6318cad39932c0253cb1df621e814a63aa91
 [4]: http://commits.kde.org/kdenlive/82dd806cd433cb735c0c4fadc758ba6c470ef11c
 [5]: http://commits.kde.org/kdenlive/da8ea89891e117e796a608243b5551769b98aa93
 [6]: http://commits.kde.org/kdenlive/1989120b658c661cc34456e235cf77c367c17f8a
 [7]: https://bugs.kde.org/421565
 [8]: http://commits.kde.org/kdenlive/e5287c0df33acbd83778b7671dc162db2040c95d
 [9]: https://bugs.kde.org/420494
 [10]: http://commits.kde.org/kdenlive/c8fe98b33af714fcf721ee5f5dc5ba19327d697b
 [11]: http://commits.kde.org/kdenlive/1f9279333724b995a25235a985ff8335c55ca67d
 [12]: http://commits.kde.org/kdenlive/a4aef884d9ad7316b2232ac2cde9c3138954e621
 [13]: http://commits.kde.org/kdenlive/cce8bcc4389ea7808f312eff80af01cc64d1f453
 [14]: http://commits.kde.org/kdenlive/e2f9b806b8dc4ea9848ecc284c824285b07a6ac6
 [15]: http://commits.kde.org/kdenlive/3b9595bd99b1d9e8b39322324b74eee7c3aab655
 [16]: http://commits.kde.org/kdenlive/b7ab436a3c66eff2aa090a46096c92e8dc33ed26
 [17]: http://commits.kde.org/kdenlive/f4f92defbee1b9914e718e64864c742a0ad364b9
 [18]: http://commits.kde.org/kdenlive/a155503246c0bbe8f64c5332727a41d5c568dfbf
 [19]: http://commits.kde.org/kdenlive/6ec0e30868a69bbd893864771f9f0090a8550a33
 [20]: http://commits.kde.org/kdenlive/86266b4ae27d8753e61cf977564f132537854482
