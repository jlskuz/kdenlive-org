---
title: Kdenlive 20.08.3 is out
author: Farid Abdelnour
date: 2020-11-23T11:41:30+00:00
aliases:
- /en/2020/11/kdenlive-20-08-3-is-out/
- /it/2020/11/kdenlive-20-08-3-pronto/
- /de/2020/11/kdenlive-20-08-3-ist-freigegeben/
---

The third and last minor release of the 20.08 series is out with the usual round of fixes and improvements. Focus is now set on finishing the same track transitions and the subtitler features for the next major release due in December. Please help test the Beta release and report any issues.
