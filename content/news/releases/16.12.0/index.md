---
author: Farid Abdelnour
date: 2016-12-15T22:15:17+00:00
aliases:
- /en/2016/12/kdenlive-16-12-released/
---

![Screenshot of Kdenlive version 16.12.0 with a rotoscoping effect currently beeing edited](screenshot.png)

The 16.12 release cycle brings the much requested Rotoscoping effect back and new effects, Library improvements, OGG render profile, UI fixes and as with every release much more stability.

This release marks the first step towards implementing Advanced Trimming Tools which will be introduced in 17.04. For that to happen we are starting a refactoring of the timeline making this feature easier to implement and preventing/fixing corruption issues.

If you want to support our work, please consider donating to KDE's [End of Year 2016 Fundraiser][1]! (KDE provides some core libraries, all infrastructure and much more making Kdenlive possible). 

**Here are some of the highlights of the 16.12 release:**

## Projects

  * Allow custom cache folder. You can now define a custom folder per project to store temporary files. This can be useful if you want to store temporary files on another drive, and also if you want to easily move your project's temporary files to another computer
  * Portable projects: to allow moving a project to another computer / drive, we now use relative paths for clips that are inside the project's directory. So if your source clips are in the same folder as your kdenlive project file (or in subfolders), you can simply move/copy the directory and the project should open without any change.

## Library

  * **When you (re) use the same image sequence clip**, title clip, or even color clip multiple times _in the same libary clip_, such image sequences and titles now will **only be added once to your project bin**. (See also our Toolbox post on [Library Clips with Image Sequences, Titles, and Color Clips][2].)
  * **The way Kdenlive expands library clips containing multiple tracks** should be working more intuitively now: 
      * Place the library clip on the _topmost_ track, it will be _expanded down_ from there when multiple tracks are required. If there aren't enough tracks below, yet there are enough tracks when also taking the tracks above into account, then Kdenlive will shuffle everthing upwards so it fits into the space.
      * Also, Kdenlive now correctly **expands clips immediately below a transition**. Before, it was reporting an error, which now has been corrected.
      * **Error reporting has been improved in general** for expanding library clips, please see also our Toolbox post on [Copy & Paste Between Projects][3].

## Effects

  * Rotoscoping effect
  * Added a UI for LUT3D effect
  * Added tripod parameter to vidstab
  * Improved motion tracker

## Rendering

  * Added OGG render profile

## User interface

  * Download render profiles, wipes and title templates directly from the interface.
    ![Screenshot of the dialog to download new wipe effects](intaller-wipes.png)
  * Added a context menu option to directly extract single frames to the project bin. See also our Toolbox post [Extract Frame to Project][4] for more details and instructions.
  * Added new configuration option for [automatically raising the properties pane when selecting certain timeline elements][5] (Toolbox post with details and instructions).
  * A new status bar toggle button that controls whether newly created transitions will be _automatic_ or _non-automatic_. Automatic transitions move and adjust with the clip they are corresponding with. Pasting transitions now correctly pastes also the automatic property from the clipboard.
  * Added context menu to set icon size in timeline.
  * The user interface now defaults to the Breeze dark theme.

## Packaging

  * 16.12 AppImage and snap packages will be available very soon, check our [Download page][6] for instructions.

Two known issues have been discovered after code freeze but have been fixed in git and will be available in 16.12.1 release due in January 2017.

  1. Project bin disappears when changing project frame rate ([bug #373534][7])
  2. Timeline guides cannot be edited ([bug #373468][8])


 [1]: https://www.kde.org/fundraisers/yearend2016/
 [2]: project/library-clips-with-image-sequences-titles-color-clips/
 [3]: project/the-library-copy-paste-between-projects/
 [4]: project/extract-frame-to-project/
 [5]: project/automatically-raising-the-properties-pane/
 [6]: /download
 [7]: https://bugs.kde.org/show_bug.cgi?id=373534
 [8]: https://bugs.kde.org/show_bug.cgi?id=373468
