  * Fix crash on copy subtitle (not implemented yet). [Commit.][1]
  * Ensure jobs for timeline clips/tracks are properly canceled when the clip/track is deleted, fix crash on audio align deleted clip. [Commit.][2]
  * Fix crash if the clip of an audio align job is deleted during calculations. [Commit.][3]
  * Fix possible crash dragging clip in timeline from a file manager. [Commit.][4]
  * Various display adjustments for compositions and clips. [Commit.][5]
  * Reset config should also delete xmlui config file. [Commit.][6]
  * Fix disabling proxy loses some clip properties. [Commit.][7]
  * Fix tests. [Commit.][8]
  * Fix some regressions in keyframe move. [Commit.][9]
  * Undo/redo on clip monitor set in/out point. [Commit.][10]
  * Don’t snap on subtitles when track is hidden. [Commit.][11]
  * Add option to delete all effects in selected clip/s. [Commit.][12]
  * Fix some more xml parameters by Eugen Mohr. [Commit.][13]
  * Fix crash when all audio streams of a clip were disabled. [Commit.][14] Fixes bug [#429997][15]
  * Fix some broken effects descriptions, spotted by Eugen Mohr. [Commit.][16]
  * Reduce latency on forwards/backwards play. [Commit.][17]
  * Fix the integer value of effect parameter’s checkbox. Fixes #880. [Commit.][18]
  * Fix various typos spotted by Kunda Ki. [Commit.][19]
  * Automatically update title clip name when we edit a duplicate title. [Commit.][20]
  * Add option to not pause the playback while seeking. [Commit.][21]
  * Fix some crashes with locked subtitle track. [Commit.][22]
  * Fix qml deprecation warning. [Commit.][23]
  * Fix track effects applying only on first playlist. [Commit.][24]
  * Fix timeline vertical scrolling too fast. [Commit.][25]
  * Fix clip move incorrectly rejected. [Commit.][26]
  * Fix regression with crash in effect stack. [Commit.][27]
  * Add preliminary support to copy a keyframe param value to other selected keyframes. [Commit.][28]
  * Move timeline tooltips in statusbar. [Commit.][29]
  * Add normalizers to MLT thumbcreator, fixing Kdeinit crash. [Commit.][30] See bug [#430122][31]
  * Effectstack: Add duplicate keyframe(s) button. [Commit.][32]
  * Effectstack: select multiple keyframes by shift-click + drag (like in timeline). [Commit.][33]
  * Improve grabbing of keyframes in effect stack. [Commit.][34]
  * Initial implementation of grouped keyframe operation (move/delete). Select multiple keyframes with CTRL+click. [Commit.][35]
  * When calculating a folder hash (to find a missing slideshow), take into accound the file hash of 2 files inside the folder. [Commit.][36]
  * Ensure subtitle track buttons are hidden when the track is hidden. [Commit.][37]
  * Fix project profile creation dialog not updating properties on profile selection. [Commit.][38]
  * Don’t change Bin horizontal scrolling when focusing an item. [Commit.][39]
  * Fix composition unselected on move. [Commit.][40]
  * Fix unwanted keyframe move on keyframe widget seek. [Commit.][41]
  * Don’t snap on subtitles when locked. [Commit.][42]
  * Show/lock subtitle track now correctly uses undo/redo. [Commit.][43]
  * Restor subtitle track state (hidden/locked) on project opening. [Commit.][44]
  * Fix qmlt typo. [Commit.][45]
  * Fix color picker offset, live preview of picked color in the button. [Commit.][46]
  * Implement subtitle track lock. [Commit.][47]
  * Add hide and lock (in progress) of subtitle track. [Commit.][48]
  * Zoom effect keyframe on CTRL + wheel, add option to move selected keyframe to current cursor position. [Commit.][49]
  * Add “unused clip” filter in Project Bin. [Commit.][50] Fixes bug [#430035][51]
  * Another small fix for image sequence on project opening. [Commit.][52]

 [1]: http://commits.kde.org/kdenlive/e6d6934ff907eeaf678226812afaabf71479458f
 [2]: http://commits.kde.org/kdenlive/fd20f747b46ab525b45c784f2027214a43f2d642
 [3]: http://commits.kde.org/kdenlive/0c2a2eaec2442e60459c0ec676fa8ef0e2e563ad
 [4]: http://commits.kde.org/kdenlive/8c53fd0f9b229f9ac1953c12296db61076733102
 [5]: http://commits.kde.org/kdenlive/02c7aef3a102bbad199fbcfed35f9f3d9eeee195
 [6]: http://commits.kde.org/kdenlive/6e5a4534ce3b9586d25db3056dccfe95c9e3a7a1
 [7]: http://commits.kde.org/kdenlive/b365d6c7e5f0f4cfb0cb7f285b0c8f9403800280
 [8]: http://commits.kde.org/kdenlive/338ef95354cc6552f1fa5642fdb3874e7a0d9cf3
 [9]: http://commits.kde.org/kdenlive/687dea161c072b5d173fcd769148f48b91358aba
 [10]: http://commits.kde.org/kdenlive/8f6fe0ce4cdf35c29cc766cc394ce3c11d134abf
 [11]: http://commits.kde.org/kdenlive/f265f7bbc3914c555f84cc882cd9763af6238f1b
 [12]: http://commits.kde.org/kdenlive/daf52f0c521acbdeea7f1d90499ee37a35806db0
 [13]: http://commits.kde.org/kdenlive/109a0954e55094c5c216382101241484bba1bc7a
 [14]: http://commits.kde.org/kdenlive/23f256c77db23a3d63db3ea792b54ea4353f0a37
 [15]: https://bugs.kde.org/429997
 [16]: http://commits.kde.org/kdenlive/24113cfedd650fe7e6ede5c31a186a3012b19cc5
 [17]: http://commits.kde.org/kdenlive/7e314223b97afb4ce646fbb07e46370198f73324
 [18]: http://commits.kde.org/kdenlive/e8a3af8121a1f98d4811fb5ed945cb74e2557d89
 [19]: http://commits.kde.org/kdenlive/237ae40b1f1d27d9fdf0c0ab119cef5ae88d7474
 [20]: http://commits.kde.org/kdenlive/a15c84131903476db4200d134340681c14ca90dc
 [21]: http://commits.kde.org/kdenlive/ebaf8d94ec5d3f07978c3da6fd2839192aa842d1
 [22]: http://commits.kde.org/kdenlive/ec0ff97c478385111d32515d47f8b4160911ed4c
 [23]: http://commits.kde.org/kdenlive/e21a4d11469965c1d6f06461834f12eea289a817
 [24]: http://commits.kde.org/kdenlive/4fdb0382395d8bd34b91a791f221fe80e15fca9a
 [25]: http://commits.kde.org/kdenlive/de46c150025f38e30426e11b51f5ddca3aa9e514
 [26]: http://commits.kde.org/kdenlive/7623b16b435f0e79195ef2c668c0b76934c5ff14
 [27]: http://commits.kde.org/kdenlive/8199a9886f32bdeeef14078e7156fc202656e6d1
 [28]: http://commits.kde.org/kdenlive/9afc4a5549b78f76405933fc2b3b9ce00973570b
 [29]: http://commits.kde.org/kdenlive/ee1d1a970a2696b11330fab96ffc2432d765da62
 [30]: http://commits.kde.org/kdenlive/a2cb2600670142c647485e2a3f1e06d7199d0b7a
 [31]: https://bugs.kde.org/430122
 [32]: http://commits.kde.org/kdenlive/2d6f0afeee138fd58d777840dea2227b78fcd906
 [33]: http://commits.kde.org/kdenlive/eb7c898dd2f39a21ab67760daef8acbf56a722c7
 [34]: http://commits.kde.org/kdenlive/59cac4915c126525056d164a96b5f64f06dd4f9d
 [35]: http://commits.kde.org/kdenlive/0edef2b0b52a7353354187aacec3e4c3bcf89af0
 [36]: http://commits.kde.org/kdenlive/b89f854bf13fb7a2db9f2c9f85b0cf1a54862761
 [37]: http://commits.kde.org/kdenlive/1d722f44a488e755bacd047501184a86f342a96b
 [38]: http://commits.kde.org/kdenlive/da8d326a27db5bdfd16e66a5be73de67d544a6c0
 [39]: http://commits.kde.org/kdenlive/901014fc1cdc67205d9d6a733e27e908488b071a
 [40]: http://commits.kde.org/kdenlive/c8739be233b99c70c0d88d45df25541e2daabbfa
 [41]: http://commits.kde.org/kdenlive/177a89746f44311b2ae3a71b50f71d6424160e0c
 [42]: http://commits.kde.org/kdenlive/b558390f6e6f2a56053f99ace532e5c1640b8b39
 [43]: http://commits.kde.org/kdenlive/0fe2d82e2d204dc7f68c736f982fba6c1ce52a6e
 [44]: http://commits.kde.org/kdenlive/263af4bfacbb30a92a02cdad9d195bf9bce3f466
 [45]: http://commits.kde.org/kdenlive/8d33fe6381538beeb279b14f5b59bf867fc0bc54
 [46]: http://commits.kde.org/kdenlive/cb718f860f29007af594d612609a8174c7032b5a
 [47]: http://commits.kde.org/kdenlive/49f477047dfb031ab08b209d5b5c16aa4de8bfdf
 [48]: http://commits.kde.org/kdenlive/bd13f3b8627714ccd5c75453d790442b0cf0f91e
 [49]: http://commits.kde.org/kdenlive/8d53e900326b290608dcdaa0e7d59227755c6eaa
 [50]: http://commits.kde.org/kdenlive/026a554ebe2e53ab30bd6f09f7fab153398c1bd0
 [51]: https://bugs.kde.org/430035
 [52]: http://commits.kde.org/kdenlive/1f05e18a1949adac57f8be6db4a8e63e97ddf3a0
