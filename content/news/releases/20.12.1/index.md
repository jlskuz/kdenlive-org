---
title: Kdenlive 20.12.1 is out
author: Farid Abdelnour
date: 2021-01-09T09:11:27+00:00
aliases:
- /en/2021/01/kdenlive-20-12-1-is-out/
- /fr/2021/01/20-12-1/
- /it/2021/01/kdenlive-20-12-1-scaricabile/
---

The first minor release of the 20.12 series is out with a huge batch of fixes and usability improvements.

## Effects

  * The ability to select and move multiple keyframes by SHIFT + click drag.
    ![](move-keyframes.gif) 
  * Select multiple keyframes with CTRL+ click.
  * Add option to move selected keyframe to current cursor position.
  * Added a duplicate keyframes button.
  * Zoom on keyframes with CTRL + wheel.
  * Add option to delete all effects in selected clip(s).
  * Fix track effects applying only on first playlist.

## Subtitling

Added ability to hide and lock subtitles.

![](subtitle-head.gif) 

## Other hightlights

  * Added undo/redo when setting in and out points.
  * Automatically update title clip name when we edit a duplicate title.
  * Appearance improvements to compositions and clips.
  * Added unused clip filter to the project bin.
  * Added option in the timeline settings to not pause playback while seeking.
  * Moved the timeline tooltips to the status bar.
