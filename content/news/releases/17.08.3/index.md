---
author: Farid Abdelnour
date: 2017-11-09T17:22:09+00:00
aliases:
- /en/2017/11/kdenlive-17-08-3-released/
---
The last dot release of the 17.08 series is out with minor fixes. We continue focus on the refactoring branch with steady progress towards a stable release.
