---
author: Jean-Baptiste Mardelle
date: 2018-05-11T08:05:19+00:00
aliases:
- /en/2018/05/kdenlive-18-04-1-released/
- /fr/2018/05/2923/
- /it/2018/05/kdenlive-18-04-1-scaricabile/
---

While our team is working on the awaited refactoring, we still managed to add 2 small usability improvements in the 18.04.1. version. First the safe zone overlay was improved so you can now easily spot the center of your frame:

![Comparison screenshots of the old and the new monitor safes zone overlay](new-overlay.png)

Then we improved the default background color for the titler so that white text can easily be read without having to make further adjustments.

![Comparison screenshots of the old and the new background of the title tool](text-bg.png)

These changes were made during our great [Paris sprint][1]. At the same time, we are improving our packaging efforts, so you can find the following download options right now (besides official distro packages):

  * _AppImage_: the latest 18.04.1 version is available, now with support for Ladspa plugins
  * _Flatpak_: we are glad to announce the availability of Kdenlive on flathub, so it can now easily be installed on distributions supporting flatpak (thanks to Peter Eszlari).
  * _Windows_: Vincent Pinon worked on an update to the Windows version, so windows users can now test the latest 18.04.1.

Head to the [download section][2] for install instructions.


 [1]: /en/2018/05/kdenlive-paris-sprint-lgm-report/
 [2]: /download
