* Update monitor safezone overlay. [Commit.](http://commits.kde.org/kdenlive/d7c51af4d84edbdbd44b25f2012af895ec5346a5) 
* Update appdata version. [Commit.](http://commits.kde.org/kdenlive/3850e34f9d311f559d690113af96a0b7b17f82c8) 
* Improve titler default background color. [Commit.](http://commits.kde.org/kdenlive/dd28daf9aa453d73f9cf621170640de018c3b802) 
* Fix build with Qt 5.11_beta3 (dropping qt5_use_modules). [Commit.](http://commits.kde.org/kdenlive/c595018a420127fb1cd2af6b676554f9960c7f79) 
* Only check color theme on first run, not each time the Wizard is called. [Commit.](http://commits.kde.org/kdenlive/ddc1dba82a833d1d5d58e8a9387733b02cd04b88) Fixes bug [#388274](https://bugs.kde.org/388274)

