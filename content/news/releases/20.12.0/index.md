---
title: Kdenlive 20.12 is out!
author: Farid Abdelnour
date: 2020-12-21T07:00:03+00:00
aliases:
- /en/2020/12/kdenlive-20-12-is-out/
- /fr/2020/12/auto-draft/
- /it/2020/12/kdenlive-20-12-e-qui/
- /de/2020/12/kdenlive-20-12-0-ist-freigegeben/
---

The team is happy to announce Kdenlive 20.12 release bringing exciting new features like same track transitions, subtitling tool, an overhauled effects layout and the usual batch of bug fixes and usability improvements. Work was done on performance optimizations (by Martin Tobias Holmedahl Sandsmark) resulting in a snappier timeline, improved thumbnail creation and faster project opening.

## Same track transitions

The long requested same track transition feature has finally landed in this release drastically improving the editing workflow. It can be activated by selecting a clip and pressing the **u** or via the icon in the timeline toolbar. _(There is a known issue where it doesn't work in an effect is applied to the track. Will be fixed in next month's release)_

![](transition.gif) 

## Subtitling tool

![](subtitle-timeline-1.gif) 

The new subtitling tool allows you to add and edit subtitles directly in the timeline on a special subtitle track or by using the new subtitle widget. You can also import (SRT/ASS) and export (SRT) subtitles. This work was implemented by Sashmita Raghav as part of GSOC.

Pro tip: It is not yet implemented in the interface but you can change the style of the subtitles using html tags like `<font color=#ffff00>` for setting the font color to yellow.

_There is a known issue with special characters not working properly, will be fixed in next month&#8217;s release._

_Subtitle track hide and lock will come in 20.12.1._

![](subtitle-widget.gif)

## Effects

All effects have been organized under a clear and comprehensive category* structure for a better experience. Under the hood, all effects were and had their parameters updated accordingly. Nonworking or buggy effects have been moved under the _Deprecated_ category for backwards compatibility but will eventually be removed in future releases, avoid using them.
_* Available audio effects depends on the OS_

![](effect-categories.png)

Another usability improvement is the ability to rename and add/edit the description of custom effects (by new contributor Vivek Yadav.)

New Pillar Echo effect for your vertical videos.

![](pillar-echo.png)

Crop by padding effect can now be keyframed.

![](crop-by-pading.gif)

New VR 360 and 3D effects for working with 360º and 3D stereoscopic footage.

![](360-effects.png)

New Video Equalizer for adjusting image brightness, contrast, saturation and gamma. 

![](vid-eq2.png)

## Usability

Besides the snappier performance due to optimizations, the timeline also received a usability boost. Clips in the timeline change color according to their tag in the project bin. (This affects all clips in a folder as well.)

![](tagging-clips-timeline.gif) 

Ability to enable/disable normalization of audio thumbnails from track header

![](normalize-track-header.gif)

Ability to delete multiple tracks at once (by Pushkar Kukde)

![](delete-multiple-tracks.png)

When archiving a project an option was added to archive only clips in the timeline as well as the option choose the compression method between TAR and ZIP.

![](archive-project.gif)

On the backend front the Online Resources tool was ported to qtwebengine (by Andreas Sturmlechner) and downloading wipes, render profiles, titles and wipes defaults to using https. 

## What's next?

The main features planned by the team for 2021 are Nested timelines, Advanced trimming tools and Audio Routing and Channel Mapping. We hope to see improvement in hardware acceleration and GPU support from recent work started in MLT, our engine, you can follow the progress [here][1]. A very exiting feature worth keeping an eye on is the recent work by Tobias Fleischer in integrating GMIC effects to video editing tools (including Kdenlive via the freIOr module). Community member Hörmet has managed to build Kdenlive on a Mac and got it to render. There are still some issues like Icons are not working.


 [1]: https://github.com/mltframework/mlt/tree/producer-avformat-hwaccel
