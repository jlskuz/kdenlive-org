  * Fix image sequences broken on project opening. [Commit.][2] Fixes bug [#429798][3]
  * Revert to previous temporary icon for subtitles until we manage to properly install the correct one. [Commit.][4]
  * Fix spacer tool sometimes moving clips when it shouldn’t. [Commit.][5]
  * Remove debug. [Commit.][6]
  * Fix crash and keyframe corruption on cli pwith multiple keyframable params. [Commit.][7]
  * Attempt to fix broken effect stack height. [Commit.][8]
  * Fix several slideshows incorrectly share same thumbnail if in same folder. [Commit.][9]
  * Fix crash moving project’s custom folder. [Commit.][10]
  * Fix possibly incorrect rendering on Windows (proxy used when they shouldn’t). [Commit.][11] See bug [#429905][12]
  * Switch KNewStuff url to https non deprecated service. [Commit.][13]
  * Fix left part of a cut cannot be selected right after processing. [Commit.][14] Fixes bug [#428544][15]
  * Fix spacer crash regression introduced in recent commit. [Commit.][16]
  * Fix crash clicking razor on timeline ruler. [Commit.][17]
  * Fix crash undoing cut when left part was selected. [Commit.][18]
  * Fix remove space not working until context menu called. [Commit.][19] Fixes bug [#429868][20]
  * Update project duration accordingly when subtitles are added/moved. [Commit.][21]
  * Fix geometry effects like obscure regression. [Commit.][22]
  * Fix regression and lift layout flicker. [Commit.][23]
  * Fix possible crashes in subtitles. [Commit.][24]
  * Cleanup some debug messages (reduce startup verbosity). [Commit.][25]
  * Improve effects layouts. [Commit.][26]
  * Hide subtitles effect from ui (used internally only). [Commit.][27]
  * Update blacklisted_effects.txt &#8211; blacklisted Pixscope which produces an error at lower preview resolutions and which is replaceable by frei0r.pr0be named Video values. [Commit.][28]
  * Update blacklisted_effects.txt &#8211; avfilter.fspp blaclisted because it is a post processing filter to be used for exporting/compressing and not on the timeline. [Commit.][29]
  * Update blacklisted_effects.txt. [Commit.][30]
  * Update kdenliveeffectscategory.rc. [Commit.][31]
  * Last fix to effect categories. [Commit.][32]
  * Revert recent OpenGL change &#8211; it was not responsible for system crashes. [Commit.][33]
  * Remove deprecated setting for automatic transitions. [Commit.][34]
  * Fix crash opening Kdenlive project files < 0.9. [Commit.][35]
  * Fix possible crash on .ass subtutle error and dropping subtitle in timeline. [Commit.][36]
  * Fix possible freeze changing monitor real time. [Commit.][37] See bug [#429228][38]
  * Make sure deprecated effects can still be used in projects for compatibility. [Commit.][39]
  * Ensure we cannot create invalid subtitles (with empty lines). [Commit.][40]
  * Do not block deprecated effects, it was causing issues for existing project files. [Commit.][41]
  * Fix active effect not remembered and not correctly focused. [Commit.][42]
  * Fix param adjustments for cartoon effect. [Commit.][43]
  * Revert openGL change causing startup crash on some systems. [Commit.][44]
  * Ensure selected subtitle always appears on top. [Commit.][45]
  * Fix typo crashing on subtitle group move. [Commit.][46]
  * Fix undo transition resize doesn’t restore keyframes. [Commit.][47]
  * Don’t double-load same track transitions as normal transitions. [Commit.][48]
  * Fix snapping not updated when track is made active/inactive. [Commit.][49]
  * Fix various subtitle selection issues. [Commit.][50]
  * Fix crash closing document with grouped subtitles. [Commit.][51]
  * Fix crash opening project with a grouped subtitle. [Commit.][52]
  * Allow creating a mix when we don’t have the full default mix duration available at clip ends (minimum of 3 frames now required). [Commit.][53]
  * Un-blacklist IIRblur. [Commit.][54]
  * Don’t blacklist subtitles effect. [Commit.][55]
  * Fix subtitles track cannot be hidden when minimized. [Commit.][56]
  * Various mix and transition fixes. [Commit.][57]
  * Update blacklisted effects. [Commit.][58]
  * Drop semi-working clip name offset in timeline. [Commit.][59]
  * Qml performance fix, patch by Martin Tobias Holmedahl Sandsmark. [Commit.][60]
  * Subtitles: work on temp files until we save the project so that each change to the subtitles is not instantly saved. [Commit.][61]
  * Fix error message about subtitle filter not initialized. [Commit.][62]
  * Fix typo in subtitles qml header. [Commit.][63]
  * Add proper icon for subtitle feature. [Commit.][64]
  * Don’t allow moving a subtitle below 0. [Commit.][65]
  * By default, move subtitles widget in clip monitor tab. [Commit.][66]
  * Center view when seeking to timeline clip. [Commit.][67]
  * After clearing bin filter line, ensure selected item is visible. [Commit.][68]
  * Fix undo import subtitle file, and improve subtitle group operations (only reload subtitle file once). [Commit.][69]
  * Fix possible crash. Related to #841. [Commit.][70]
  * Hide subtitle track name on collapse. [Commit.][71]
  * Fix export subtitle not overwriting existing one. [Commit.][72]
  * Fix group subtitle deletion undo. [Commit.][73]
  * Subtitle track: add expand button and track label. [Commit.][74]
  * Allow moving subtitle through subtitle widget. [Commit.][75]
  * Add shortcut in/out/delete buttons to subtitle widget. [Commit.][76]
  * Fix cut subtitle. [Commit.][77]
  * Subtitles: when cutting from subtitle widget, split text at cursor position. [Commit.][78]
  * Fix crash on adding first subtitle from subtitle widget. [Commit.][79]
  * Fix subtitle model used start time as index &#8211; caused issues if a subtitle if moved after another one. Add start/end position control in subtitle widget. [Commit.][80]
  * Fixuifiles. [Commit.][81]
  * Don’t move subtitle on right mouse click. [Commit.][82]
  * Add libass target for AppImage. [Commit.][83]
  * Fix crash on mix deletion. [Commit.][84]
  * Fix crash on exit caused by subtitlemodel. [Commit.][85]
  * Select subtitle item when moving between subtitles from the widget. [Commit.][86]
  * Fix various issues with subtitles (the filter was duplicated on project opening). [Commit.][87]
  * Fix crash loading some ass subtitles, add basic widget for subtitle edit otherwise the feature was not much usable. [Commit.][88]
  * Fix integration of subtitles in timeline (snap, group, cut). [Commit.][89]
  * Add option to export subtitle file. [Commit.][90]
  * Updated effects descriptions and categories. [Commit.][91]
  * Fix track offset with some tools when subtitle track is displayed. [Commit.][92]
  * Add menu for subtitle clips. [Commit.][93]
  * Allow importing subtitle file at cursor position, save subtitles on project archiving. [Commit.][94]
  * Add subtitle import function, fix crash on add subtitle on new project. [Commit.][95]
  * Add config setting for default subtitle duration, add subtitle by double clicking in subtitle track. [Commit.][96]
  * Correctly load / save subtitle file. [Commit.][97]
  * Various subtitle fixes (moving, allow selecting). [Commit.][98]
  * Fix subtitle resize undo. [Commit.][99]
  * Further progress in subtitle undo/redo. [Commit.][100]
  * Start subtitle undo/redo integration. [Commit.][101]
  * Refresh monitor on subtitle change. [Commit.][102]
  * Delete all subtitles when subtitle track is collapsed. [Commit.][103]
  * Add function to delete all subtitles from subtitle model. [Commit.][104]
  * Update status of subtitle tool button when creating subtitle track. [Commit.][105]
  * Move subtitle qml into its own file, always display text (use clipping) on lower zoom. [Commit.][106]
  * Fix compilation. [Commit.][107]
  * Add subtitle actions in Project menu. [Commit.][108]
  * Fix missing file from previous commit. [Commit.][109]
  * Fix startup crash on missing QtQuick Shapes module. [Commit.][110]
  * Add minimum limit to resizing subtitle clip and minor subtitle clip UI improvements. [Commit.][111]
  * Enable deletion of subtitle clips from timeline by right clicking on them. [Commit.][112]
  * Enable visibility of subtitle track according to state of subtitle toolbar button. [Commit.][113]
  * Correct subtitle filter name and enable writing to SRT files. [Commit.][114]
  * Fix audio mix same track transition crossfade. [Commit.][115]
  * Fix on monitor mess and possible crash with several transform effects. [Commit.][116]
  * Fix indentation. [Commit.][117]
  * Add timeline ruler menu item to create subtitle clip. [Commit.][118]
  * Add button in timeline toolbar to enable subtitle editing. [Commit.][119]
  * Add timeline tractor to Subtitle Model to attach subtitle filter. [Commit.][120]
  * Fix subtitle clip movement. [Commit.][121]
  * When moving a clip outside a mix, correctly resize the clip. [Commit.][122]
  * Fix audio thumbs not reloaded on profile fps changed. [Commit.][123]
  * Fix some compile warnings. [Commit.][124]
  * Try to improve default OpenGL format for monitor view. [Commit.][125]
  * Fix on monitor displayed fps with high fps values. [Commit.][126]
  * Remove icons that are included in Breeze icon theme by default. [Commit.][127]
  * Port to QtWebEngineWidgets. [Commit.][128]
  * Fixed ui dsiplay box and undo redo stack. [Commit.][129]
  * Fixed indentation. [Commit.][130]
  * Removed unnecessary code. [Commit.][131]
  * Added multiple track deletion feature. [Commit.][132]
  * Correctly reload mix params on project opening. [Commit.][133]
  * Ensure mix parenting is updated in case of clip cut. [Commit.][134]
  * Add new option to apply bin tag color to timeline clips, enabled by default. [Commit.][135]
  * Small update in mix look. [Commit.][136]
  * Ensure timeline ruler is correctly updated on profile switch. [Commit.][137]
  * Add missing include for some frameworks versions. [Commit.][138]
  * Move audio max level to a job (was causing ui hang on project opening). [Commit.][139]
  * When switching project profile and there is only 1 clip in timeline, update the timeline clip duration accordingly to profile change. [Commit.][140]
  * Show tooltip for clip fade corner hotspot. [Commit.][141]
  * Remove deprecated KF5 call. [Commit.][142]
  * Fix compilation. [Commit.][143]
  * Project archiving: check after each file if archiving works, add option to use zip instead of tar.gz. [Commit.][144] See bug [#421565][145]
  * Fix opening project files with missing version number. [Commit.][146] See bug [#420494][147]
  * Don’t enforce stereo output on playback. [Commit.][148]
  * Enable subtitle text editing on double click. [Commit.][149]
  * Refactor rendering timecode overlay feature. [Commit.][150]
  * Fix duplicated audio from previous commit. [Commit.][151]
  * Fix playlist clips have no audio regression. [Commit.][152]
  * Fix keyframeable effect params left enabled when selecting a clip, leading to possible crash. [Commit.][153]
  * Don’t allow removing the only keyframe in an effect (was possible from the on monitor toolbar and crashing). [Commit.][154]
  * Add build status to README.md. [Commit.][155]
  * Fix rotoscoping points not reset when adding a second rotoscoping effect to a clip. [Commit.][156]
  * Ensure only wanted items are really archived. [Commit.][157]
  * Fix tests, improve mix behavior with resize undo/redo. [Commit.][158]
  * Fix typo in header because it breaks parser. [Commit.][159]
  * Update to org.kde.Sdk 5.15. [Commit.][160]
  * Qtcrop effect: make radius animated. [Commit.][161]
  * When editing a title clip, hide it from timeline so that it doesn’t appear on background frame. [Commit.][162]
  * Update effectlistwidget.cpp. [Commit.][163]
  * Update effectlistwidget.cpp. [Commit.][164]
  * Update effecttreemodel.cpp. [Commit.][165]
  * Update effectlistwidget.cpp. [Commit.][166]
  * Update assettreemodel.cpp. [Commit.][167]
  * Delete effectlistwidget.cpp.autosave. [Commit.][168]
  * Improved last commit. [Commit.][169]
  * Edit name and description of custom effects. [Commit.][170]
  * Render widget: avoid misuse of parallel processing. [Commit.][171]
  * Implemented enhancement suggestion #561. [Commit.][172]
  * Fix qml overlay offset on monitor zoom (transform/rotoscoping). [Commit.][173]
  * Make folder to store titles and scripts configurable in Kdenlive Settings. [Commit.][174]
  * Update effectlistwidget.cpp. [Commit.][175]
  * Fix crash inserting zone over grouped clips in same track. [Commit.][176]
  * Titlewidget. [Commit.][177]
  * Timeline Archive, QCheckBox problem. [Commit.][178]
  * Check ffmpeg setting points to a file, not just isn’t empty. [Commit.][179]
  * Enable subtitle clip movement by dragging clip across subtitle track. [Commit.][180]
  * Add function to enable subtitle clips&#8217; movement in subtitle track. [Commit.][181]
  * Allow custom comments in effect groups. [Commit.][182]
  * Add effect’s MLT tag to effect description, not Kdenlive’s id, makes it easier to understand which effect is implied. [Commit.][183]
  * Qtcrop effect: make radius animated. [Commit.][184]
  * Update effectlistwidget.cpp. [Commit.][185]
  * Update effectlistwidget.cpp. [Commit.][186]
  * Resolved edit custom effect description issue. [Commit.][187]
  * Update slot for subtitle model changed signal. [Commit.][188]
  * Update subtitle parser and Add function to write to subtitle file from JSON object. [Commit.][189]
  * Add function to export subtitle model items to JSON. [Commit.][190]
  * Update to org.kde.Sdk 5.15. [Commit.][191]
  * When editing a title clip, hide it from timeline so that it doesn’t appear on background frame. [Commit.][192]
  * Update effectlistwidget.cpp. [Commit.][193]
  * Update effectlistwidget.cpp. [Commit.][194]
  * Update effecttreemodel.cpp. [Commit.][195]
  * Update effectlistwidget.cpp. [Commit.][196]
  * Update assettreemodel.cpp. [Commit.][197]
  * Fixed titlewidget.cpp again. [Commit.][198]
  * Fixed titlewidget.cpp. [Commit.][199]
  * Start showing parameters for same track mixes. [Commit.][200]
  * Delete effectlistwidget.cpp.autosave. [Commit.][201]
  * Improved last commit. [Commit.][202]
  * Edit name and description of custom effects. [Commit.][203]
  * Fix compilation with Qt < 5.15. [Commit.][204]
  * Fix some deprecation warnings. [Commit.][205]
  * Implemented suggestion #795. [Commit.][206]
  * Ensure last frame of the project is rendered (with some audio codecs like aac, last frame will be duplicated because of a bug in MLT). [Commit.][207]
  * Add method to easily reload a custom effect. [Commit.][208]
  * Comment out non working code. [Commit.][209]
  * Fix freeze on memory usage loading invalid clips. [Commit.][210]
  * Implemented suggestion #795. [Commit.][211]
  * Fix track insertion in mixed view mode. [Commit.][212] See bug [#403443][213]
  * Fix track order in mixed track view. [Commit.][214]
  * Fix compilation. [Commit.][215]
  * Fix monitor preview messing monitor zoom. [Commit.][216]
  * Restore toolbars in default editing layout. [Commit.][217]
  * Fix saving lift/gamma/gain effect results in broken ui. [Commit.][218]
  * Add some checks to max audio level calculation. [Commit.][219]
  * Add xml for qtcrop filter. [Commit.][220]
  * Remove testing stuff. [Commit.][221]
  * Fix incorrect “search aborted” message and tooltip for recovered missing clips. [Commit.][222]
  * Add option in track headers to disable normalizing of audio thumbnails. [Commit.][223]
  * Fix audio mixer track effects applied twice when reopening project, leading to incorrect volume. [Commit.][224]
  * Store color theme in a localized neutral way. [Commit.][225]
  * Fix various cases where it was not easy to find correct location of missing clips (slideshows, or clips with proxies). [Commit.][226]
  * Fix mem leak when another process was writing a clip that is included in a project. [Commit.][227]
  * Fixed automatic scene split (bug #421772). [Commit.][228]
  * Remove deprecated line. [Commit.][229]
  * Correctly display Proxy status in timeline for clips with missing source. [Commit.][230]
  * On project opening, fix detection of proxied clips with missing source and proxy. [Commit.][231]
  * Small cleanup, make placeholder timeline clips visible and allow reloading missing clip if source file is available. [Commit.][232]
  * Comment out libva stuff for CI AppImage. [Commit.][233]
  * Add some extra checks for mixes. [Commit.][234]
  * Make mix cut pos snap in timeline. [Commit.][235]
  * Ensure we use correct subplaylist producer when replacing a clip in timeline (eg. when proxying). [Commit.][236]
  * Fix appimage creation. [Commit.][237]
  * Custom effect Comments feature. [Commit.][238]
  * Added comments feature in custom effects. [Commit.][239]
  * Fix crash on undo mix deletion, use different producer for subplaylists (fixes slow transitions). [Commit.][240]
  * Nicer visual for mix, fix resizing grouped mix didn’t update grouped mix cut position. [Commit.][241]
  * Fix PreviewJob memory leak. [Commit.][242]
  * Fix mix deletion when deleting first clip of a mix. [Commit.][243]
  * Fix group of clips with mix broken after reloading project. [Commit.][244]
  * Fix incorrect hash check causing incorrect reload dialog on project opening. [Commit.][245]
  * Fix speed change effect lost when opening project with missing clip, and broken handling of missing proxied clips with speed effect. [Commit.][246]
  * Fix calling methods on null objects. [Commit.][247]
  * Ensure we check file hash on every project opening to ensure clips have not changed and an incorrect hash is not stored. [Commit.][248]
  * Fix corrupted slowmotion clips on document opening. [Commit.][249]
  * Fix speed change effect lost when opening project with missing clip, and broken handling of missing proxied clips with speed effect. [Commit.][250]
  * Fix moving clip groups with a mix. [Commit.][251]
  * Ensure we check file hash on every project opening to ensure clips have not changed and an incorrect hash is not stored. [Commit.][252]
  * Fix av clip mix. [Commit.][253]
  * Ensure a mixed clip cannot be moved further than its counterpart mix clip. [Commit.][254]
  * Correctly load and save mix cut position. [Commit.][255]
  * Easy selection and deletion of clip mix. [Commit.][256]
  * Warn user if not enough frames in the clip to create a mix. [Commit.][257]
  * Ensure mix is always on top of clips. [Commit.][258]
  * Update AppImage scripts for FFmpeg hw accel. [Commit.][259]
  * [Experimental] Added GPU profiles for rendering proxies and timeline preview … [Commit.][260]
  * Fix creating mix between color clip and AV clip. [Commit.][261]
  * Remove unused krandomsequence.h include. [Commit.][262]
  * Fix compilation. [Commit.][263]
  * Fix crash on some projects opening. [Commit.][264] Fixes bug [#409477][265]
  * Add mix clip action in timeline menu, fix undo resize on mixed clip. [Commit.][266]
  * More fixes and tests for chained mixes. [Commit.][267]
  * Fix calling methods on null objects. [Commit.][268]
  * Another batch of tests and fixes for same track transitions on grouped clips. [Commit.][269]
  * More tests and fixes for same track transitions. [Commit.][270]
  * More fixes for same track composition clips move and resize. [Commit.][271]
  * Fix OTIO error display. [Commit.][272]
  * Make same track transition correcty resize on clip resize. [Commit.][273]
  * Fix possible crash detected by tests. [Commit.][274]
  * Make resizing mix work, fix crash on clip deletion. [Commit.][275]
  * Correctly load same track transitions when opening project. [Commit.][276]
  * Fix deprecated install location. [Commit.][277]
  * Fix remaining bugs in mix move. [Commit.][278]
  * Moving clip with mixes should now work correctly. [Commit.][279]
  * Implement moving left clip in a mix transition. [Commit.][280]
  * Fix mix test. [Commit.][281]
  * Fix group move with same track transition. [Commit.][282]
  * More fixes and tests for moving a clip with same track transition. [Commit.][283]
  * Start implementing clip move when same track transition is active. [Commit.][284]
  * Tests: also check same track transition undo. [Commit.][285]
  * Add first test for same track compositing. [Commit.][286]
  * Mix all parts of grouped clips. [Commit.][287]
  * Make same track composition undoable. [Commit.][288]
  * Ensure default layout names are translatable. [Commit.][289]
  * Delay locale reset to allow correct ui translation. [Commit.][290]
  * Add Appimage and source download links in appdata. [Commit.][291]
  * Fix clicking on clip marker label moving timeline cursor to approximate position. [Commit.][292]
  * Update readme with GPL link and canonical spelling. [Commit.][293]
  * Fix tests :). [Commit.][294]
  * Use another ref on the producer when saving project (might help in case another operation is performed on the producer). [Commit.][295]
  * Add corruption check before creating backup file. [Commit.][296]
  * Fix overwrite sometimes not working on clips with multiple streams. [Commit.][297]
  * Port away from deprecated Qt::MidButton. [Commit.][298]
  * Attempt to mimic Shotcut’s locale handling. [Commit.][299]
  * Correct cursor height in timeline. [Commit.][300]
  * Add separate track for subtitle clips. [Commit.][301]
  * Another attempt to fix #780. [Commit.][302]
  * Another attempt to fix Windows locale corruption on Windows. [Commit.][303]
  * Enforce C Locale on Windows on rendering. [Commit.][304]
  * Update mainwindow.cpp. [Commit.][305]
  * Same track mix update: resize both clips and create a mix in between. [Commit.][306]
  * Fix some corruption in same track composition. [Commit.][307]
  * Add icon in timeline toolbar for same track mix. [Commit.][308]
  * Initial commit for same track transitions. [Commit.][309]
  * Add missing “=” symbol in GPU profile. [Commit.][310]
  * Correctly update project duration on group move. [Commit.][311]
  * Sort insertion order using int instead of string. [Commit.][312]
  * Raise Project Bin when a clip is dropped in timeline or created through the menu. [Commit.][313]
  * Fix frame number in rendering progress. [Commit.][314]
  * Effect Stack: focus active effect when switching between clips. [Commit.][315]
  * Fix subtitle clip duration updation during subtitle resizing. [Commit.][316]
  * Make start position of subtitle editable. [Commit.][317]
  * Correct end resizing of subtitle clips. [Commit.][318]
  * More progress info during render. [Commit.][319]
  * Add GPU profiles provided by Constantin Aanicai. <https://kdenlive.org/en/2020/08/kdenlive-20-08-is-out/#comment-5089>. [Commit.][320]
  * Fix shift click for multiple selection broken in Bin. [Commit.][321]
  * Make subtitle end position editable. [Commit.][322]
  * Add functions to move subtitles in model. [Commit.][323]
  * Add function to remove a subtitle from model. [Commit.][324]
  * Make subtitle text editable. [Commit.][325]
  * Add functions to edit existing subtitles. [Commit.][326]
  * Allow automatic keyframes on transform like effects. [Commit.][327]
  * Don’t allow adding unique effect (like fades) twice. [Commit.][328]
  * Fix crash removing a fade effect added twice. [Commit.][329] Fixes bug [#425175][330]
  * Enforce wasapi as default audio backend on Windows. [Commit.][331]
  * Fix regression in project notes from clip monitor(incorrect clazy fix). [Commit.][332]
  * Fix clip monitor zoom not reset when changing clip, and zone incorrect on zoom. [Commit.][333]
  * Add MLT’s Pillar Echo effect. [Commit.][334]
  * Restore increased range for volume effect, only limit the visible range in timeline. [Commit.][335]
  * Appimage: during image creation, delete libxcb and libxcb-dri{2,3} libraries. [Commit.][336]
  * Missing fix for FreeBSD in last commit (fix broken localization). [Commit.][337]
  * Attempt to fix UI translations broken. [Commit.][338] See bug [#424967][339]
  * Reduce range of volume effect (limit to -50 &#8211; +50 dB), and use logarithmic scale in timeline. [Commit.][340]
  * Rotoscoping: add Auto keyframe button in monitor toolbar to automatically add a keyframe when moving a point. [Commit.][341]
  * Don’t cache empty audio data (fixes missing audio thumbs on video clips). [Commit.][342]
  * Various fixes for audio thumb management, should slightly improve memory usage/performnace. [Commit.][343]
  * Add function to edit end timings of each subtitle. [Commit.][344]
  * Display subtitle text in rectangles. [Commit.][345]
  * Document changes. [Commit.][346]
  * Add header files. [Commit.][347]
  * Connect Subtitle Model signals with respective slots. [Commit.][348]
  * Add signals and slot to subtitle model. [Commit.][349]
  * Connect subtitle model with timeline. [Commit.][350]
  * Display start positions of subtitles in timeline. [Commit.][351]
  * Add functions to add the start time of each subtitle line as snaps. [Commit.][352]
  * Add basic functions for returning row count and all subtitles in list. [Commit.][353]
  * Add class to handle subtitles definition and comparison. [Commit.][354]
  * Add custom roles to model. [Commit.][355]
  * Add function to append subtitles to list. [Commit.][356]
  * Add functions to return pointer to model. [Commit.][357]
  * Add SSA Parser. [Commit.][358]
  * Add srt parser to model. [Commit.][359]
  * Rewrite audio thumbnails to only use FFmpeg’s data and optimize memory usage on creation. [Commit.][360]
  * Add avfilter eq filter (allowing to adjust image brightness, contrast, saturation, gamma all in one effect. [Commit.][361]
  * Refactor clip monitor audio thumbnails to use same data as timeline thumbnail. [Commit.][362]
  * Drop deprecated QApplication attribute. [Commit.][363]
  * Avoid confusion for one-letter translations. [Commit.][364]
  * Update Audio, Editing and Color layouts. [Commit.][365]
  * Add Effects layout. [Commit.][366]
  * Improve audiomixer layout. [Commit.][367]
  * Update AppImage dependency build scripts. [Commit.][368]
  * Logging UI proposal. [Commit.][369]
  * When trying to play a monitor that is hidden, display an info message allowing to show the monitor. [Commit.][370]
  * Manually fix clazy warnings. [Commit.][371]
  * Apply clazy fixes (range-loop). [Commit.][372]
  * Remove needless window title. [Commit.][373]
  * When dragging a clip in insert mode, don’t allow leaving empty space in timeline. [Commit.][374]
  * Restore seek in clip monitor on Shift + move mouse. [Commit.][375]
  * Don’t show monitor overlay (fpt, timecode, …) in audio seek bar. [Commit.][376]
  * Timeline: don’t allow moving a keyframe at same pos as another one. [Commit.][377]
  * In insert mode, deleting a clip should perform an extract operation. [Commit.][378]
  * Fix clip url not correctly updated when opening project with missing clips. [Commit.][379]
  * When dropping a folder from a file manager to bin, don’t create folders that contain no valid clip. [Commit.][380]
  * Expand/collapse all bin folders with Shift+click. [Commit.][381]
  * Save folder status (expanded or not). [Commit.][382]
  
 [2]: http://commits.kde.org/kdenlive/516ae4236081dde44cd9063b7f4cb8a66987f168
 [3]: https://bugs.kde.org/429798
 [4]: http://commits.kde.org/kdenlive/3dce948ed6c87d8a602d7b41b25c0a6ea1b44270
 [5]: http://commits.kde.org/kdenlive/790e96ccdec9fecb81b479561c6798875f372ccd
 [6]: http://commits.kde.org/kdenlive/371e3e43e12a838779fc0e0a52c683cd472e4870
 [7]: http://commits.kde.org/kdenlive/3ffea885fcc5061c564960fb010cd7cf07d2dedd
 [8]: http://commits.kde.org/kdenlive/997c7a707b384835cc8f0c164c544e4447201c4c
 [9]: http://commits.kde.org/kdenlive/01f6e42d192cd821278329de1251c2a17001ab8a
 [10]: http://commits.kde.org/kdenlive/bfff60a10f79c70f5e259951b28b29c50c5201e1
 [11]: http://commits.kde.org/kdenlive/bd8b6cdf60904542bd71bcfd19ab5664b10640f9
 [12]: https://bugs.kde.org/429905
 [13]: http://commits.kde.org/kdenlive/2b71bdec6054772a58fe9ccb15d2d6f257d36d2f
 [14]: http://commits.kde.org/kdenlive/fe7ce60e05d711c39a283aa8fe5a11d220c4e5a7
 [15]: https://bugs.kde.org/428544
 [16]: http://commits.kde.org/kdenlive/cee0ad80a3166a0f721484c72f315efb8cffbffd
 [17]: http://commits.kde.org/kdenlive/1e365f4278598908fbef4f4c47b8fbe01b83e612
 [18]: http://commits.kde.org/kdenlive/7502d82e7a23b4599dfaa6de0b70af58ac8fde92
 [19]: http://commits.kde.org/kdenlive/4d325cbd1bb3cc1019bea7d638a25662fc7a292a
 [20]: https://bugs.kde.org/429868
 [21]: http://commits.kde.org/kdenlive/1e93e1da6482ada1f99c6c03f0a6665acac95eed
 [22]: http://commits.kde.org/kdenlive/f1aae681bbb585b5566b655226028494a624d56b
 [23]: http://commits.kde.org/kdenlive/9194ffeec7e6984f857a32b9099a1cf68a20b070
 [24]: http://commits.kde.org/kdenlive/3cdb86cde7071a719445d55c010b76bbc18f1c6f
 [25]: http://commits.kde.org/kdenlive/42c7c0309bcc8b98ac7cc25597610b8611206197
 [26]: http://commits.kde.org/kdenlive/1c134a91e5e955fe220f810422c7207189204bd8
 [27]: http://commits.kde.org/kdenlive/07b8fffacaa7d8f727dcb780e733741a3c2278df
 [28]: http://commits.kde.org/kdenlive/b801b7b93dfa2b1ef55004aec0a21ef56516929c
 [29]: http://commits.kde.org/kdenlive/43656d496353b3a87921c67d4e7b89aa84aebfb3
 [30]: http://commits.kde.org/kdenlive/5be14ae8132e72bf40419f4f73157a6907a25247
 [31]: http://commits.kde.org/kdenlive/1298098c64b6a940c0c85dc6a7d126ec0097f8b4
 [32]: http://commits.kde.org/kdenlive/6fc3f97e8052e5fb2f9950d8f6187db4ca7df7d7
 [33]: http://commits.kde.org/kdenlive/482991a88266e5ee9f0f0f26f955a30ffcd1df3f
 [34]: http://commits.kde.org/kdenlive/558d1494bf49f5b37381c829fc972498dd4a3ad6
 [35]: http://commits.kde.org/kdenlive/7d0c6bc0aadb0a4cd10287a68ea2fa05b32d5837
 [36]: http://commits.kde.org/kdenlive/d180c984a680e7857a4c78feae652fd6b8968675
 [37]: http://commits.kde.org/kdenlive/05fda675069eda170eacda7e072c98206191895f
 [38]: https://bugs.kde.org/429228
 [39]: http://commits.kde.org/kdenlive/57a9f96f33e02bd704a856f77ee9eb63b6dd63e2
 [40]: http://commits.kde.org/kdenlive/fadbbf97ce5ce64b4c9d5734372810bc3e7ef349
 [41]: http://commits.kde.org/kdenlive/8fe8439bd9605437401da288bcb5ec9b36dc1347
 [42]: http://commits.kde.org/kdenlive/75063593b4a5ade1e056b14997d9b221d4801111
 [43]: http://commits.kde.org/kdenlive/a6e6e11f2547ac441c15238d6264f120ba5c2c70
 [44]: http://commits.kde.org/kdenlive/c30435f57a3dd780bf8984c832261b687d46fb84
 [45]: http://commits.kde.org/kdenlive/4f2ee42e6894d2a3e6b91ad3339c7724d701f20f
 [46]: http://commits.kde.org/kdenlive/54b43cb5104866d168f1fb797e231a043f696f19
 [47]: http://commits.kde.org/kdenlive/11fab9913f4c3391357cd69d2cd01bbeef1f0de6
 [48]: http://commits.kde.org/kdenlive/526bc6bef125052bda402cd752018bc2822a9371
 [49]: http://commits.kde.org/kdenlive/84d2fe9a1b0b76cd00e7d8e25840bfea6b024935
 [50]: http://commits.kde.org/kdenlive/ba457245b795aedb608bdb70961bb70f5ee6494a
 [51]: http://commits.kde.org/kdenlive/27844e3580b554600f8f56e1d31984ab1c39fb9b
 [52]: http://commits.kde.org/kdenlive/3a77973ea6ae8d0533a8ab8467613ba0a6d457e3
 [53]: http://commits.kde.org/kdenlive/45383774edf44f9d0aa506e1acf6a81978dd112b
 [54]: http://commits.kde.org/kdenlive/ab915ac34a31ecb2814157224a114798393d824d
 [55]: http://commits.kde.org/kdenlive/e2875cd5374aa62f26395dd756b053a805d9bc91
 [56]: http://commits.kde.org/kdenlive/f0508967296ec94ca5d3a70f0bce4fafcda0b382
 [57]: http://commits.kde.org/kdenlive/bab26ba709f0ca406bf1598da49e552ec6df8149
 [58]: http://commits.kde.org/kdenlive/1e89a75cc5e43dda09740a9bc375fd35aec514e5
 [59]: http://commits.kde.org/kdenlive/6bfa9a439d74bc96323aad3ac972416ccca79e03
 [60]: http://commits.kde.org/kdenlive/14a15e8958e0ed37db2b1d313d5311d8f7eb2ba9
 [61]: http://commits.kde.org/kdenlive/d2409500e164bbae2245085ff85da037c1f843de
 [62]: http://commits.kde.org/kdenlive/2cc7b347f5f7f8a4201ea3293a31740868c319f5
 [63]: http://commits.kde.org/kdenlive/2d24a30ac8663deb138fc245e799a43998ea1e67
 [64]: http://commits.kde.org/kdenlive/ef0809c99c58789243340c07830cc0256ae5b07b
 [65]: http://commits.kde.org/kdenlive/4135769a1712c0e257882068c68407c773eaeace
 [66]: http://commits.kde.org/kdenlive/7ae95a2b184615178e592ff3cd858f8a9250b94d
 [67]: http://commits.kde.org/kdenlive/25c1628ee3525204a5feb2a9efd69d67c745126e
 [68]: http://commits.kde.org/kdenlive/8fc743985ca62cfee14c8416a180697d5fecd6cc
 [69]: http://commits.kde.org/kdenlive/5e9c863de14e829f441d03ebb99ae496a183845a
 [70]: http://commits.kde.org/kdenlive/a4c5e0bf85259dae249ac0c17722438e5e9311d2
 [71]: http://commits.kde.org/kdenlive/39e07a56511147cedcf42576e117734186eda920
 [72]: http://commits.kde.org/kdenlive/ee3fa9cd32eb9a8dbba984a95008c6cec1f9504b
 [73]: http://commits.kde.org/kdenlive/c30157070386ce0d684d5f91dcbe10efd575c98c
 [74]: http://commits.kde.org/kdenlive/824741bd1237ccb65bca3602d8c94978213ce3ac
 [75]: http://commits.kde.org/kdenlive/c8440acfbc08f904a9226d9bc2902f204f83c06e
 [76]: http://commits.kde.org/kdenlive/69ca3d75c087435f85f3eae7038445876f36f836
 [77]: http://commits.kde.org/kdenlive/78f5b9dfa9e27ffc5c7b04873f4c06e2fb543ab6
 [78]: http://commits.kde.org/kdenlive/ca2c198ed06a005311cddc2d582aae49023ef7f3
 [79]: http://commits.kde.org/kdenlive/6e639f6a6c1afd5709efcb0998a3fa93d40e8b8b
 [80]: http://commits.kde.org/kdenlive/46dd76e4c1af85abdd547f7fc5539ddadf891596
 [81]: http://commits.kde.org/kdenlive/380315e39c24645244edd21d2577cfaa4ba4ce05
 [82]: http://commits.kde.org/kdenlive/a6d5dbc01cc13f172d7f65526aa528bd8afca963
 [83]: http://commits.kde.org/kdenlive/f87002ed48385c6b51a4abe1f0306815a494db02
 [84]: http://commits.kde.org/kdenlive/5baea51458d6d6f506386d8f80d349f86056dcbe
 [85]: http://commits.kde.org/kdenlive/7338455a4cceb87bab99bc52e78823e6eec08341
 [86]: http://commits.kde.org/kdenlive/f74787edb2ade88688b0fee7a64bcb990f855b29
 [87]: http://commits.kde.org/kdenlive/299cd9e33b6096dc8979d504a2be55a1396f422b
 [88]: http://commits.kde.org/kdenlive/87616c7f4c437578cd96d8aa9dd15a55e0c5e85a
 [89]: http://commits.kde.org/kdenlive/1d4228c88a1c2f217f2693facce49d178024820f
 [90]: http://commits.kde.org/kdenlive/4c86e2b50be0b239a9d7ae4d11242fc41a0d4d42
 [91]: http://commits.kde.org/kdenlive/bfbb50e13b70c971d826977de966c9af46a15da3
 [92]: http://commits.kde.org/kdenlive/28f5f1b480c0b85954a861dfeb81d437111b7e0c
 [93]: http://commits.kde.org/kdenlive/afe60bb7397793d3761d957ccdaa863cc8f98fef
 [94]: http://commits.kde.org/kdenlive/5f763e3170794f3fe8fdbc8628624fef521aaa76
 [95]: http://commits.kde.org/kdenlive/febe71730cdf76c49f21852bcd18688e52228e33
 [96]: http://commits.kde.org/kdenlive/6da8a8474b04ae27dda72331397e0eb66ff051e2
 [97]: http://commits.kde.org/kdenlive/93d2d41ec943c139b5f9dd789482c422e525d614
 [98]: http://commits.kde.org/kdenlive/f435261a2ffd4a57b1137216963c7867d1c49644
 [99]: http://commits.kde.org/kdenlive/38ce0af81780efdb13ecb383e52075e913fb9de1
 [100]: http://commits.kde.org/kdenlive/bffca1e6989a46f6e708c326cb274e04b62bfd68
 [101]: http://commits.kde.org/kdenlive/92ea9252c10c1d82fe9253f8d80137448e1144ec
 [102]: http://commits.kde.org/kdenlive/05dea89552d0b9a0e56214e0d84a57424d18bec2
 [103]: http://commits.kde.org/kdenlive/b0f67182bc48099de75d6cb8c2fa50f71eb89ed2
 [104]: http://commits.kde.org/kdenlive/feb19c838845de279b1ec8656e6594ce7c0805fe
 [105]: http://commits.kde.org/kdenlive/872b95cfd51c95d561ed38f9dd552bc40fed166a
 [106]: http://commits.kde.org/kdenlive/0805fb1edf86121d355a7dc8078f38e4b2efa486
 [107]: http://commits.kde.org/kdenlive/8d0f03d8527696b6649d85dec2e92df189af1b66
 [108]: http://commits.kde.org/kdenlive/d393fcff6a071602c079d42ea8c4fc3a45568ba4
 [109]: http://commits.kde.org/kdenlive/c4539f47bdb18874620ace8b05721d57103cf8e6
 [110]: http://commits.kde.org/kdenlive/eac46c1cbd6ccf7dd34cfdb12b9627f3a4938976
 [111]: http://commits.kde.org/kdenlive/271a8906e7885366671fe5743dedb7cc256bfd51
 [112]: http://commits.kde.org/kdenlive/ed9c7a4d9e09edb7a0fc7ac7a41414c0822a800f
 [113]: http://commits.kde.org/kdenlive/36a2cd5b679dbdc9443fd910cb6055eb5a3cedb1
 [114]: http://commits.kde.org/kdenlive/2119cd1d9c9c0138080ffb4c06012e84abad55e0
 [115]: http://commits.kde.org/kdenlive/94303d6531abdf3a5fdf6346e88b7ec1ec107f4d
 [116]: http://commits.kde.org/kdenlive/136ad56966857807183b65218cd4028e3688b679
 [117]: http://commits.kde.org/kdenlive/01f501bb02e72057e04b006df3c3d2a3be56cb05
 [118]: http://commits.kde.org/kdenlive/2480100160a21b7282be34fcfd184cae18686b2e
 [119]: http://commits.kde.org/kdenlive/46a5c8af39d0e5e84e506202b8000262d5191797
 [120]: http://commits.kde.org/kdenlive/1a743001859a153bdbe66da05b766fb85800ff92
 [121]: http://commits.kde.org/kdenlive/39a52bb4067db6e5d6157a7c9758004111f53cf5
 [122]: http://commits.kde.org/kdenlive/bc698228890d4bc0eb50d1fbdc572a37703c4064
 [123]: http://commits.kde.org/kdenlive/f6c87416c4136eec157d6c193b1114b0fafcfd0a
 [124]: http://commits.kde.org/kdenlive/735bf84b17b3a29223ec74c4893d10116d996a4d
 [125]: http://commits.kde.org/kdenlive/8f43cbb7c7cd159bbee9a2bf034a779c6e5837c2
 [126]: http://commits.kde.org/kdenlive/5dae30174dcfbd732d00e5857d9d7ba8da3b2e44
 [127]: http://commits.kde.org/kdenlive/93a40647ad91fc8f7f606f642cd000cfa074b2d5
 [128]: http://commits.kde.org/kdenlive/51feccf3cc0939d42c3ff237eb17f9a6091f1dd8
 [129]: http://commits.kde.org/kdenlive/c92da2590d41bb78ac9f730c95429a225d7f06b2
 [130]: http://commits.kde.org/kdenlive/970df3918ae8bdf4293c68dbb4e4b8354083bb33
 [131]: http://commits.kde.org/kdenlive/708592573f95d33b36067a3e96711d2bbeeef649
 [132]: http://commits.kde.org/kdenlive/791e2d060b9b9a589eb4cad997d05e84bdd1817b
 [133]: http://commits.kde.org/kdenlive/3af486f289104f7fc01d76423fdf71bac6d8ce83
 [134]: http://commits.kde.org/kdenlive/d8f08ed37cd7a6dcfd62dbd25a6a44460345467b
 [135]: http://commits.kde.org/kdenlive/20311cb59d09d69b0147324c9db0ec22e1e00eea
 [136]: http://commits.kde.org/kdenlive/65cba8aa0949809332ef1467b441f7fb7dd8c635
 [137]: http://commits.kde.org/kdenlive/2f9edda61c7725cca06483699f5b637d0fe61ff4
 [138]: http://commits.kde.org/kdenlive/f8cd54b2e4b03216d4d64feb0b7f0dc5b5fb0f14
 [139]: http://commits.kde.org/kdenlive/9c9c327bdb548cbac3cb5dd98e1f0adc0712541d
 [140]: http://commits.kde.org/kdenlive/a12ba6dfa4fe637f878af5953020d51a170cc110
 [141]: http://commits.kde.org/kdenlive/9b615fca5ef8c7c5225cbba9d0a7853074809ce0
 [142]: http://commits.kde.org/kdenlive/453cf95a621d97aa7ea52328af3135c9897d3128
 [143]: http://commits.kde.org/kdenlive/978b4225ae39a218e2c0309d22397dda2cbe4fcc
 [144]: http://commits.kde.org/kdenlive/9e077005c7922458eba95ccd6f4194ca9e3f055d
 [145]: https://bugs.kde.org/421565
 [146]: http://commits.kde.org/kdenlive/c18bf45a7031e35cbacf9da42ac3b75bc14af7a8
 [147]: https://bugs.kde.org/420494
 [148]: http://commits.kde.org/kdenlive/76b9492d05b299d87a5ef818b028715cf45ba401
 [149]: http://commits.kde.org/kdenlive/14971b3e8169b99cfbe24ea76e4c9c78f53318c7
 [150]: http://commits.kde.org/kdenlive/356cb41704331d903df7f93ad043257379025c67
 [151]: http://commits.kde.org/kdenlive/b65689cb15fc53d1dc21f2fedbb7510e7dd52d00
 [152]: http://commits.kde.org/kdenlive/8e3163b2601a468cd9bf51a3ea9478e758457ccd
 [153]: http://commits.kde.org/kdenlive/ed5e0ed3dce200521e34dedf032038020160e854
 [154]: http://commits.kde.org/kdenlive/3986e1b48e3f66c6cdfdda225f914effd1d10abd
 [155]: http://commits.kde.org/kdenlive/aaa02ac8d042ebc215d12ee32fed0b2db33b4814
 [156]: http://commits.kde.org/kdenlive/b748efa0e43ab1340f63ec5df0ebbf023012db97
 [157]: http://commits.kde.org/kdenlive/7b1d44c411c9d8488730d34fdd4b32e00f76e3fa
 [158]: http://commits.kde.org/kdenlive/a7c7e98db0dc337170bbf3baaf57af05e9208a7b
 [159]: http://commits.kde.org/kdenlive/d143ef0a1107975b9262bd970b4d528547a8f788
 [160]: http://commits.kde.org/kdenlive/9da3b3d18dba668fd65ba9d8d06d06c6caab09d7
 [161]: http://commits.kde.org/kdenlive/5bfa3af4db6e54954a6335ce7a1af367831dfa73
 [162]: http://commits.kde.org/kdenlive/273c0b34bf13d8bd47ff4131216c19246679b258
 [163]: http://commits.kde.org/kdenlive/a1909bd3f63ec91b7772a5d4e669e14ca6338b81
 [164]: http://commits.kde.org/kdenlive/14636495785176cadfd74feaafe471e12b8d43ab
 [165]: http://commits.kde.org/kdenlive/695c6b3bff72978d18a2633054258badad993a5f
 [166]: http://commits.kde.org/kdenlive/ec97c5646bb556afc19eb3fe1577876b28829e33
 [167]: http://commits.kde.org/kdenlive/33758be44b35349edf8e9e83f76d8c68d74452e7
 [168]: http://commits.kde.org/kdenlive/d9c7dcb55fc7caaa82077ae54221c46846e3b5c1
 [169]: http://commits.kde.org/kdenlive/5cc52f51264d1e487f4d0840878071906788835d
 [170]: http://commits.kde.org/kdenlive/d8c42f4aecb6f0c438fba686b25d6c3fdb8c5c3d
 [171]: http://commits.kde.org/kdenlive/ab00a0414e3086ca0ca3a0dded73f1056ff7b4fd
 [172]: http://commits.kde.org/kdenlive/c8b8909a5a441ec13b6a2622c5f66b5f2f414925
 [173]: http://commits.kde.org/kdenlive/4a3a55e83b56179ca231c1e1b00044d72d0a701b
 [174]: http://commits.kde.org/kdenlive/eeada05539cea4b508399f5da92ac0e7eaba84c0
 [175]: http://commits.kde.org/kdenlive/70223d6626bc8f9a277568df2ebc5adee0cf6cd4
 [176]: http://commits.kde.org/kdenlive/76d87f66b254bd3797e89818481b877b24bab73f
 [177]: http://commits.kde.org/kdenlive/68e2dcfb7ef20f8354cf6ecc8290323d39bc3ddf
 [178]: http://commits.kde.org/kdenlive/18d09e2f260b1f464cbb7ccaa424f15fb09f5105
 [179]: http://commits.kde.org/kdenlive/7cf1ac94dc1b673188f01b6da05a653618761fc4
 [180]: http://commits.kde.org/kdenlive/3d952925281537e1e89960b9cca1c2eabd03e956
 [181]: http://commits.kde.org/kdenlive/17a9ef37e745b26cf58dc2501c2e60273124bf83
 [182]: http://commits.kde.org/kdenlive/4403ee204e0f0d9f05a2c6764b942d245d191c8f
 [183]: http://commits.kde.org/kdenlive/c9fda75cfce98b75b08dd509e2637cb9b2f1590b
 [184]: http://commits.kde.org/kdenlive/f11aae7d69c5d4339c6a419cac48a01d9f09b7b1
 [185]: http://commits.kde.org/kdenlive/b5c088312396137c6e0181a525b2522db890e060
 [186]: http://commits.kde.org/kdenlive/9ee0157aeb09af5d59b2bf236ea173a02d6e8590
 [187]: http://commits.kde.org/kdenlive/513534e28bdf275283110ea17b71b79313481157
 [188]: http://commits.kde.org/kdenlive/27b5e631983af36fe9ed2cecbd656acf18354175
 [189]: http://commits.kde.org/kdenlive/0fd03938e2b39163a8741fdfce207640b040e46e
 [190]: http://commits.kde.org/kdenlive/5927cd3bac53c8d8e3e36e2198be886342cdfb01
 [191]: http://commits.kde.org/kdenlive/47982916a79336988b4e3851e4e2c288d670d1e7
 [192]: http://commits.kde.org/kdenlive/946c9e912fd305713f729e428a866baa2eaf8502
 [193]: http://commits.kde.org/kdenlive/c75ac2a8d290f0317b1a3d7bab0ab0375351ebd3
 [194]: http://commits.kde.org/kdenlive/03743f32d7448c513183affe80633c6373065f2a
 [195]: http://commits.kde.org/kdenlive/7b17322c07018adf3b1179c2d7ec4d9afe87d69e
 [196]: http://commits.kde.org/kdenlive/c396a58e446970012c671ed237f02d0254dbe6d8
 [197]: http://commits.kde.org/kdenlive/95585239b69623bc98c592c4906995ce3cac8bc4
 [198]: http://commits.kde.org/kdenlive/a8279d49c156b7769d662c04991c833f74043a69
 [199]: http://commits.kde.org/kdenlive/e50ed4e1e460436c03608c2c5bf35bd6077d94ee
 [200]: http://commits.kde.org/kdenlive/fc586019f44379e54b1639f4dcb60b20e91aa8d0
 [201]: http://commits.kde.org/kdenlive/e2dd30d50e0f71f328e99f74567173b7ab67f18b
 [202]: http://commits.kde.org/kdenlive/4e7f7498d5b7538150c5e4e9a2fe0a712c903120
 [203]: http://commits.kde.org/kdenlive/8ccda514d687e55147a260beba729b2f37be0afb
 [204]: http://commits.kde.org/kdenlive/7df43a547f1e4407751d3da006bd241f8da61427
 [205]: http://commits.kde.org/kdenlive/ac7fc97372175abc7ad04fb41ad0b77bc4dbcc04
 [206]: http://commits.kde.org/kdenlive/097b3ad60f8e5b36413dd8dd6e964ba2440f12e7
 [207]: http://commits.kde.org/kdenlive/bb3b42654b18c08b12b10878a7cf5e23918a57f3
 [208]: http://commits.kde.org/kdenlive/508a4b77600427a1f1309d4131a9e442e5ab317f
 [209]: http://commits.kde.org/kdenlive/4d93def3c5c3dd52d2f608f6b4a3085ca202acf6
 [210]: http://commits.kde.org/kdenlive/45c4e5e6a80fb84559cb07d3ecd8587889397841
 [211]: http://commits.kde.org/kdenlive/f8af6b85f12d5f1be1cd4ab55fd6878272c9ba9c
 [212]: http://commits.kde.org/kdenlive/25ba5eaa1b51e6e3dafaed9c5aa84a67488245f8
 [213]: https://bugs.kde.org/403443
 [214]: http://commits.kde.org/kdenlive/568cf02fcb1c0f00dbff1edfdec4da1836713453
 [215]: http://commits.kde.org/kdenlive/e053e1d0b8d707f388ec47fae848a671f1e4b4a9
 [216]: http://commits.kde.org/kdenlive/ab2691e8f6b1d2f04d23243fdc37d02a433fb25d
 [217]: http://commits.kde.org/kdenlive/eadb5bb1fe28eead5a6f06e01b939699c7e84ffe
 [218]: http://commits.kde.org/kdenlive/6b93a374315ac776b3d733fba287b052b598767c
 [219]: http://commits.kde.org/kdenlive/1b9e07254a9fb7639b09edeea29bc686eafb1346
 [220]: http://commits.kde.org/kdenlive/3770fe80634e83cf0e64cb1b5686337bc74135fd
 [221]: http://commits.kde.org/kdenlive/5b84846013cd3be9fadcd5187f82a0bdffb90146
 [222]: http://commits.kde.org/kdenlive/3d8eac623d751585c0f002b33ee1161abe37551a
 [223]: http://commits.kde.org/kdenlive/12ba744532d425fab05b5ca80e469b4898dad9cc
 [224]: http://commits.kde.org/kdenlive/41e87260718d0ecad635e32fec2f08881f7eacc5
 [225]: http://commits.kde.org/kdenlive/9a94ca4e7d230fdbfd8ef36b13e4c52e6b323bda
 [226]: http://commits.kde.org/kdenlive/0df40f210d60c8ef5c149049d54aebd64e9828cd
 [227]: http://commits.kde.org/kdenlive/88980a0641ca08d2ab56c7502a11d19e04fd38e3
 [228]: http://commits.kde.org/kdenlive/a6ea3608f3b39219f06bdcdd60ff2dfe8e3ed408
 [229]: http://commits.kde.org/kdenlive/42d2c4320dc684c2b6f4a870f4a62680ada26c71
 [230]: http://commits.kde.org/kdenlive/a81e510b72079805990ad285e7cbe38440a99561
 [231]: http://commits.kde.org/kdenlive/042ce94bf7c4eb0afb77974dc05cf756657a37e2
 [232]: http://commits.kde.org/kdenlive/ba76788bf76016542412b84f5c013686e9ed5950
 [233]: http://commits.kde.org/kdenlive/af0bd4f7e14670b84188b2bf1353b83db0b84ef1
 [234]: http://commits.kde.org/kdenlive/58fd31a5e2dca01ea0a40f3d9add02c28ea5017b
 [235]: http://commits.kde.org/kdenlive/18fae08380a22a52aab580156c1c6e6b4a796075
 [236]: http://commits.kde.org/kdenlive/5d6e7e810dfaedc6bdd38d3869039f949ebfb279
 [237]: http://commits.kde.org/kdenlive/45fbb03cb468cd28f18b9ccc04459148acda996a
 [238]: http://commits.kde.org/kdenlive/51275f6cff2cc9fddf945e2a78d3e931ff0cf2fc
 [239]: http://commits.kde.org/kdenlive/be57dcdab4300e73556a8fa627a3d5b910d4c017
 [240]: http://commits.kde.org/kdenlive/37ddf5edb4fea9b79262c7a3c49d82d58918b976
 [241]: http://commits.kde.org/kdenlive/640664e2a2e02b8e8cb984d6a3d86116f7c8d2f6
 [242]: http://commits.kde.org/kdenlive/13120af540c6319593701682bbeea2f93fdb45bc
 [243]: http://commits.kde.org/kdenlive/1432dd62d14d96cc7f86e0f538eaef822bacabb2
 [244]: http://commits.kde.org/kdenlive/dbd4ae024f98c66d5145925cf3f19deac86c5d91
 [245]: http://commits.kde.org/kdenlive/b1df9fd3489f454a548f6e7c888b840bd93c8929
 [246]: http://commits.kde.org/kdenlive/12467f19d9c11125302a50e5b85f827219c416d0
 [247]: http://commits.kde.org/kdenlive/565a6c1cbdbb6de759d3455040b5f9d2c255c243
 [248]: http://commits.kde.org/kdenlive/a3fe8a8eaaf0d15bcd62207b2310d704a1bf4023
 [249]: http://commits.kde.org/kdenlive/dff5f709388ea6f834b37f7516223c704c1d5cbf
 [250]: http://commits.kde.org/kdenlive/663fcc92e51726cd29ca1a2e5525ca510fcece87
 [251]: http://commits.kde.org/kdenlive/02e8c3edc49b395952fee956d26def1f1a59cf1b
 [252]: http://commits.kde.org/kdenlive/0430805f0a6ed6121900cb3f1d15766dc7e3f4a1
 [253]: http://commits.kde.org/kdenlive/d7c68402f1c9c706322c1eebaf20493f20c5ae4b
 [254]: http://commits.kde.org/kdenlive/0f144df5e3474b9cca497f7f140c1f41ef76020d
 [255]: http://commits.kde.org/kdenlive/c69bc854ecb4f686c04012ad1e6a99a75628b74b
 [256]: http://commits.kde.org/kdenlive/5c0f642aa833bb863aad58b332c9dc7d28d5059e
 [257]: http://commits.kde.org/kdenlive/4f81b7514a9386383726651ac5b0f14da1c42366
 [258]: http://commits.kde.org/kdenlive/0aa315c21e922f830de59179bf2be1c8d12dc771
 [259]: http://commits.kde.org/kdenlive/62c5aa6b0e5644597541680006f9102eb667d7f4
 [260]: http://commits.kde.org/kdenlive/7e8a90aea08257ae01318af04a9d1230332cca72
 [261]: http://commits.kde.org/kdenlive/40085a04636c27dac5ee6acf60a3bf721a665ad4
 [262]: http://commits.kde.org/kdenlive/8a089887972d742fcff5a2bdfb1d8b8183ed9072
 [263]: http://commits.kde.org/kdenlive/9705c9433248502eab8273be93b4129a685c61d9
 [264]: http://commits.kde.org/kdenlive/99db241c7f6ec72ebf69e23e6f1eb4c39ab16cf3
 [265]: https://bugs.kde.org/409477
 [266]: http://commits.kde.org/kdenlive/c3155b94e7c3b594d22790569158a382c6fae4a5
 [267]: http://commits.kde.org/kdenlive/a976d33fc926c04b753b765f802fb36b97b67af5
 [268]: http://commits.kde.org/kdenlive/153688e1961215bb9a2e83913e1427944e24fad1
 [269]: http://commits.kde.org/kdenlive/a3bc1439ab052d08912e8febb5a8140f4e80e8db
 [270]: http://commits.kde.org/kdenlive/0378f6d146e63f967fcc044950414b83c3eed9ab
 [271]: http://commits.kde.org/kdenlive/1e3707f12967720238b011a20f0f70a5271faba4
 [272]: http://commits.kde.org/kdenlive/f049619c4789a594e37dd88e6fc9f689704c1a65
 [273]: http://commits.kde.org/kdenlive/8991e0ee1bfd590604cc10677ca9cce6e1968f91
 [274]: http://commits.kde.org/kdenlive/38a21d0e3fa46d784a58adb4b0c1c573b39ebafb
 [275]: http://commits.kde.org/kdenlive/bc9de7c403d5e0c344c3e1910a2f766efdf363f5
 [276]: http://commits.kde.org/kdenlive/3c0a8db4acae7f21fd2cd8dd5fee98f006b5d0f7
 [277]: http://commits.kde.org/kdenlive/baa3c0de78ce924ce04dba4569f1f6f12608b40a
 [278]: http://commits.kde.org/kdenlive/f371478aeb81ceb093cce080d85afb5e0601cca8
 [279]: http://commits.kde.org/kdenlive/fc5f133ac45c49691e9b9ea1c94c6e9bc640b5da
 [280]: http://commits.kde.org/kdenlive/ee8fb1c29c97437ef01bbb6954a4d6cfa3236b08
 [281]: http://commits.kde.org/kdenlive/5d1a81b6270508b52ea340e320fe33d6d28882c1
 [282]: http://commits.kde.org/kdenlive/47fcbb2a96c7c83deb73ab6fdad3c1aa9589ac8d
 [283]: http://commits.kde.org/kdenlive/d06ec3cd0fca4db988973187bbe67f7593b9258c
 [284]: http://commits.kde.org/kdenlive/2ea5bdcde30e33b2e6ef5d04febc98bc18e7e05c
 [285]: http://commits.kde.org/kdenlive/c008bf1a741fd11d4a095102bdd2b1120cb14d5a
 [286]: http://commits.kde.org/kdenlive/2faff23e996e52651cf7b454c1e465cda28475ab
 [287]: http://commits.kde.org/kdenlive/f33a8cb4511ed7f08e9588a937aed84e31abf029
 [288]: http://commits.kde.org/kdenlive/e8510ee16e3c31b6891eecb3e5239b0fe2a4679d
 [289]: http://commits.kde.org/kdenlive/5e00c87cec6a11a0dbd4ab7e872a6509a51b077b
 [290]: http://commits.kde.org/kdenlive/491247706849e7fdef855d12f673ffcb107d16a6
 [291]: http://commits.kde.org/kdenlive/a76b04222656cebb46cb7f86939713c697473842
 [292]: http://commits.kde.org/kdenlive/4ec4cedbe2ef7b5c4c96a263151dfe79c4fd48b4
 [293]: http://commits.kde.org/kdenlive/571f59e1ec48b82cd68efff03f7dc015dfd41f67
 [294]: http://commits.kde.org/kdenlive/406c56ccbd8c34319ae7bc3bbaa1b3cd1b957329
 [295]: http://commits.kde.org/kdenlive/24f6b42d3179bfc959980073464336e703ada1ec
 [296]: http://commits.kde.org/kdenlive/2dac8e6440567ed28046f6102888dec0f66f102b
 [297]: http://commits.kde.org/kdenlive/4de7f1e4551405f3cf8baf267418dc1455513f81
 [298]: http://commits.kde.org/kdenlive/666020634980d22a387124345f1deddbab1d23a7
 [299]: http://commits.kde.org/kdenlive/75e57704e0b6df057ef2081664520fbab3eb046f
 [300]: http://commits.kde.org/kdenlive/73716f89a47b151e965920c5dd54598426855c60
 [301]: http://commits.kde.org/kdenlive/622efeeb642680e6254cef7abd1fc51f1a8f80f3
 [302]: http://commits.kde.org/kdenlive/45cbc455938173523a3c3c2c1f90c4ebf9a30ad8
 [303]: http://commits.kde.org/kdenlive/a610f581cc8f96e702f00a56853b8ec927145998
 [304]: http://commits.kde.org/kdenlive/865042c81df4cd9d1f29a8e81624655aa47fcd79
 [305]: http://commits.kde.org/kdenlive/9b08d73ee65c7a83e5e1b80bebe8ee881486d1a3
 [306]: http://commits.kde.org/kdenlive/03cf97450dc07c88f20560f0fd11cff755e682ca
 [307]: http://commits.kde.org/kdenlive/761393e4ee83d97f4f3ea338cf8c29b1a6e5b7f8
 [308]: http://commits.kde.org/kdenlive/604b8507454c38ad8e453c93e6e0bdc2d90db236
 [309]: http://commits.kde.org/kdenlive/ba564eda44f15ed03902eb367742df6f8471cab3
 [310]: http://commits.kde.org/kdenlive/f8654e66ced4c4f71c875a0a6205b5abec06930f
 [311]: http://commits.kde.org/kdenlive/e83f8dc0ca4bf03a7b921afa87e5009a0908d757
 [312]: http://commits.kde.org/kdenlive/e80ba1811a30a717b3f213853ae7f5cf1d191ebc
 [313]: http://commits.kde.org/kdenlive/1e00ab87b470dc884484fda64585c5d504d75fbf
 [314]: http://commits.kde.org/kdenlive/2d61ef598a7f237edfd2690703adf9f6c38c4d00
 [315]: http://commits.kde.org/kdenlive/2526f7af48a6652c475de0f263bcf31bda90739f
 [316]: http://commits.kde.org/kdenlive/cbee271e24d4dd301e92e60c3c3f1d5935b1af94
 [317]: http://commits.kde.org/kdenlive/3e111f5100340f33d3df3bb256a955287fc56b06
 [318]: http://commits.kde.org/kdenlive/2ad81743cc4a7c429bfa4976c378d34914064980
 [319]: http://commits.kde.org/kdenlive/fc6c822ff0079d33038a97afb2b1fa94169cf7ba
 [320]: http://commits.kde.org/kdenlive/b6f8400db91af9987aa57d006344c2b7e68ac953
 [321]: http://commits.kde.org/kdenlive/7466caef971abbe3fee8ef5218e6ac73b963dda5
 [322]: http://commits.kde.org/kdenlive/f1de3703ca8e10b7825a1aeee707d41962da74db
 [323]: http://commits.kde.org/kdenlive/05e67c2c48c60ef728eb49d6a59015e0177fb727
 [324]: http://commits.kde.org/kdenlive/1ad28e8d6097a2974b3432e135a19d521b265fb8
 [325]: http://commits.kde.org/kdenlive/1b68eb9156c0b67ddc2e167d76ffc4fc7ecd2a85
 [326]: http://commits.kde.org/kdenlive/b550a1b3b5240e7da4b2047568d0a2b7f56fe3f2
 [327]: http://commits.kde.org/kdenlive/d9fc8628cc5be715d402a79edfd7dd3ff50110eb
 [328]: http://commits.kde.org/kdenlive/e27d3f481e05b9082b6abd4a05f681b8573462c4
 [329]: http://commits.kde.org/kdenlive/e9baf89529d0ed3b6cdc1419c2597a1f8405e721
 [330]: https://bugs.kde.org/425175
 [331]: http://commits.kde.org/kdenlive/35acc609600034ac27d7240536df4422cc3c54d4
 [332]: http://commits.kde.org/kdenlive/f7137493d5284e48d8203a0016f0ff353d20da44
 [333]: http://commits.kde.org/kdenlive/44ce7aeadc264d27c7a734d3c1758c11f6a36ac2
 [334]: http://commits.kde.org/kdenlive/7c8de1f60dede0798dca87ccd1370dc0de55c364
 [335]: http://commits.kde.org/kdenlive/7d0b60c9314f2db0c1a4117cb43dffda33178a35
 [336]: http://commits.kde.org/kdenlive/186c2793f5a31fc770d625bedb78ce1a2db2f240
 [337]: http://commits.kde.org/kdenlive/e17e4f2196667a2b8c5fb101d3c73da539ca9d78
 [338]: http://commits.kde.org/kdenlive/143bf1697e84bfca3967535dcaa1e96a92f5cdc1
 [339]: https://bugs.kde.org/424967
 [340]: http://commits.kde.org/kdenlive/997a9abd3ff2d21ba100ddc91a6f97647f5be8f2
 [341]: http://commits.kde.org/kdenlive/68914726d9733881a1e7173678b5999cc08b7d11
 [342]: http://commits.kde.org/kdenlive/f4a1cdf60311262e447f16d3f34771e2bd22cd59
 [343]: http://commits.kde.org/kdenlive/9ba6eaa95475fc92185d42e572471ab8a73f05cf
 [344]: http://commits.kde.org/kdenlive/772823ac3a487cf2cde34bc2fb8fb81577e5836f
 [345]: http://commits.kde.org/kdenlive/4c318e7ce1289cec6c5dbd3cf9dba356a9986834
 [346]: http://commits.kde.org/kdenlive/4daf304377a6327c5b7278ebadf632fba65d67b2
 [347]: http://commits.kde.org/kdenlive/6935d4762673d67c67eba7806d0424bc7ccad380
 [348]: http://commits.kde.org/kdenlive/3f0ee4cbcb6867923eb57004ec80907b6550a250
 [349]: http://commits.kde.org/kdenlive/d7dfcab12c2f9d0126f7862841209eb4e7bbce24
 [350]: http://commits.kde.org/kdenlive/42147a4bd4ef63dfe78fd0251b7701ddb3450e80
 [351]: http://commits.kde.org/kdenlive/9362014e4f34c1163780b271384d026734f4fa05
 [352]: http://commits.kde.org/kdenlive/e8c6771594278b4a9db728798bca3ad3e31a7a4b
 [353]: http://commits.kde.org/kdenlive/7fe395a1a776a912b8e608ed7d45118282e3e752
 [354]: http://commits.kde.org/kdenlive/827c9e17fe505126bd01c127b343b383f1fe74f0
 [355]: http://commits.kde.org/kdenlive/077d48b97f32939b60fdeb1f8e1c0a57af7fd460
 [356]: http://commits.kde.org/kdenlive/92dc28911ed39d0fb78b4a4128a8228c82331b1d
 [357]: http://commits.kde.org/kdenlive/a9a62259d3bbf634bcd196e79a081a8bc6f1694e
 [358]: http://commits.kde.org/kdenlive/e684355895c20f444fd81dee34db94e7226b8ece
 [359]: http://commits.kde.org/kdenlive/359850766b9fcc81a46386b7e71595ffdf8af61c
 [360]: http://commits.kde.org/kdenlive/a0db1142d3fb857aece99506008d6cc6ad9f9c52
 [361]: http://commits.kde.org/kdenlive/587461abe64246537a552a2a092b7ee43817cf62
 [362]: http://commits.kde.org/kdenlive/64eb4da1ad4b0b6abb2b67e42e2fc7da4c79bb72
 [363]: http://commits.kde.org/kdenlive/8f29a8fced22774da46c4bbc4df321ccb985af22
 [364]: http://commits.kde.org/kdenlive/60fdc1e996853a8337c8f7f3e3039ad87cb6b7cf
 [365]: http://commits.kde.org/kdenlive/caa291096968c826cdaba46defd2ce81792f54fe
 [366]: http://commits.kde.org/kdenlive/8b2caa64cd6b18c774b3363c9df14cc5f376b193
 [367]: http://commits.kde.org/kdenlive/dbb3b68e7be22d9e2362711bb8b415ba0c3b9d29
 [368]: http://commits.kde.org/kdenlive/436ad5e0ba8f7b975c8da7fd6a8783eb4ae5dda1
 [369]: http://commits.kde.org/kdenlive/4719f469e53a97ba0d51ceee243fd112649a1c33
 [370]: http://commits.kde.org/kdenlive/87dc2b1d9fac51202241a6e371ec2433d3122d50
 [371]: http://commits.kde.org/kdenlive/3b9d8bfd4990c4853879cc30fb0b26b032f17a6b
 [372]: http://commits.kde.org/kdenlive/a71dddbaa5ef77eea30ecbdaf9b55fb711dbbbc3
 [373]: http://commits.kde.org/kdenlive/1301d057c8252b56b6bc80f612b86387cfd95593
 [374]: http://commits.kde.org/kdenlive/d9208074f805185ad203fad0cff5e4d59bf254e5
 [375]: http://commits.kde.org/kdenlive/e6d2c20598316b4a06c02279d86d1f9d0ce550ba
 [376]: http://commits.kde.org/kdenlive/b72f28b972bd0bee7b1a5716861ba8f3fe6ff5ab
 [377]: http://commits.kde.org/kdenlive/5736dfe8c75053509de369bde6d77d05c350ef7a
 [378]: http://commits.kde.org/kdenlive/f0e73b312d4fb9897940750dee8b28735b80332b
 [379]: http://commits.kde.org/kdenlive/a5e5af96dd593813bf4ac579284a9d20e4514992
 [380]: http://commits.kde.org/kdenlive/fdaa4321b1912b296c273ba32bea61cc31a32888
 [381]: http://commits.kde.org/kdenlive/304e5a0935b2d61ccec0cf3acd40e381a6e3e98f
 [382]: http://commits.kde.org/kdenlive/6e5bb1f5ccfd0ce93f218e281207cac96c02534f

