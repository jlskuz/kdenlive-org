---
title: Ready to edit ?
author: Jean-Baptiste Mardelle
date: 2017-12-23T19:43:12+00:00
aliases:
- /en/2017/12/ready-to-edit/
---

![](1712c2.jpg)

Following the recent Kdenlive 17.12 release, a few annoying issues were reported in the AppImage. So we decided to focus on these and you can now download an updated AppImage with these 2 nice changes:

  * Translations are now included in the AppImage, for easy editing in your native language!
  * Fixed a high CPU usage on idle due to the sound pipeline

The 17.12 updated AppImage can be downloaded here:  
**edit 25th of december** : AppImage updated following CPU issues. https://download.kde.org/Attic/kdenlive/17.12/linux/Kdenlive-17.12.0d-x86_64.AppImage

The 18.04 updated alpha (not ready for production) also includes a fix for saving/loading track state: (not available anymore)

So I wish you all a happy end of year and successful editing… and if you love our work, please consider a donation to [KDE's end of year fundraiser!][1]

 [1]: https://www.kde.org/fundraisers/yearend2017/
