---
author: Jean-Baptiste Mardelle
date: 2012-05-15T11:26:00+00:00
aliases:
- /users/j-b-m/kdenlive-09-released
- /discover/0.9
# Ported from https://web.archive.org/web/20160324133446/https://kdenlive.org/users/j-b-m/kdenlive-09-released
# Ported from https://web.archive.org/web/20160319020916/https://kdenlive.org/discover/0.9
---


![](kdenlive-090a_0.png)

Kdenlive 0.9 has just been released. We encourage all users to upgrade to this new version that fixes many small issues and should improve the overall user experience. Some of the new features include:

## Improved effects workflow

The effect stack was rewritten to allow adjusting parameters for several effects in one go. Effects can also be grouped, groups can be saved and effects or groups can be dragged and dropped onto another clip

## Automatic audio alignment

If you have been working on a scene with several camcorders, Kdenlive can now automatically align the clips in timeline using the audio.

## Easy import of online resources

Kdenlive now has an online resource browser that allows you to easily preview and import audio, graphic and video resources from archive.org, freesound audio library and open clip art.

## Usability improvements

Many small improvements and bugfixes should make your use of Kdenlive nicer and smoother, for examples:

* Recording can now be monitored through the audio and color scopes, audio normalization can analyse audio for better results
* Allow audio only recording
* Clips can be sorted by date
* New effects from MLT / frei0r: video stabilizers, IIR Blur, etc
* Offline editing (Backup the project with proxy clips only to work on less powerful computers)
* See a more complete changelog below

## Get it!

The Kdenlive 0.9 source tarball can be downloaded from the KDE servers: [download link][2].  
Binary packages will be announced in our [download page][3]

## The future

A big thank you to all the people that contributed in one way or another to improve this new version. Some bugfix 0.9.x releases will be made when necessary, but in the background we are working (well mostly Till) on a refactoring to cleanup the code and hope to reach the 1.0 milestone by the end of the year, thanks to the fantastic success of our fundraising campaign.

As usual, users are welcome to join our [forums][4] and [bugtracker][5] to report problems.  
  
  
## Changes

### Improved effect stack

![](kdenlive-090-effectstack.png)

Effects can now manipulated in a more intuitive way. You can group them, drag and drop and adjust several effects in the same widget [Read more][6]

### Browsing of online resources

![](kdenlive-090-online.png)

You can now easily browse and import graphics, audio and videos from online resources. Currently, archive.org, freesound and open clip art are supported.

  [2]: https://web.archive.org/web/20160324133446/http://download.kde.org/stable/kdenlive/0.9/src/kdenlive-0.9.tar.gz
  [3]: https://web.archive.org/web/20160324133446/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive
  [4]: https://discuss.kde.org/tag/kdenlive
  [5]: https://web.archive.org/web/20160324133446/http://kdenlive.org/mantis
  [6]: /users/j-b-m/improving-effects-workflow-kdenlive
  [7]: /users/j-b-m/kdenlive-latest-developments
