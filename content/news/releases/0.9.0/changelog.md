* Improve detection of locale issues (default to POSIX when there is a locale conflict on the system)
* Improve timeline operation for small clips (disable resizing, only allow move)
* Add background color parameter to "Rotate" and "Pan and Zoom" effects
* Allow to scroll using the mouse wheel in monitor editing scene
* Zoom to mouse when using CTRL + mouse wheel in monitor editing scene
* Add support for new slope mode in "Color Selection" effect to allow smooth alpha transition
* Introduce grouping of effects; groups can also be saved
* Allow dragging an effect to another clip/track
* Allow record monitor to go fullscreen
* Image sequences can now start at an arbitrary frame (http://kdenlive.org/mantis/view.php?id=2508)
* Add automatic clip alignment based on audio (experimental, has to be manually enabled)s
* Allow archiving for offline use (only archive proxies)
* Offline editing: Allow working on project with only proxies available (http://kdenlive.org/mantis/view.php?id=2509)
* Allow rendering to another framerate
* Check for missing locale and ask to install instead of opening corrupted project
* Allow to open project files manually extracted from archived project
* Support dropping a folder in the project tree (http://kdenlive.org/mantis/view.php?id=1288)
* Add "select all clips in track" and "select all clips in timeline" features (http://kdenlive.org/mantis/view.php?id=1950)
* Put audio effects in subcategories to avoid uberlong menus (http://kdenlive.org/mantis/view.php?id=2436)
* Support for project metadata (can be embedded in rendered file)
* Add Online Resource Widget allowing easy search and download of online services (freesound, openclipart, archive.org)
* Introduce MLT clip analysis to get auto normalize data in sox gain effect
* Connect recording to audio scopes
* Add audio only recording (works while playing)
* Add extract zone function: part of clip is copied to new file without re-encoding
* Introduce generic job framework to process clips
* Improve the Choose color widget: Use less space and make it easier to pick the average color value from an area
* Add GUI for effect Dynamic Text: Allows to display timecode/framecount and other data
* Rework effect stack: All effects are shown at once and are collapsible
* Add two different video stabilizers
* Add IIR Blur GUI
* Add date column to project tree
* Fix monitor effect scene sometimes forcing monitor minimum size
* Fix clip move sometimes giving error when it should work
* Fix custom effects not considering capital letters in name (http://kdenlive.org/mantis/view.php?id=2580)
* Fix script rendering when script name contains whitespace
* Ensure clip in project tree is visible after rename (scroll if necessary) (http://kdenlive.org/mantis/view.php?id=2563)
* Fix monitor scene never resetting scrollbars
* Fix issues with transitions when inserting track (http://kdenlive.org/mantis/view.php?id=2477)
* Fix some GUI elements not responding to color theme change
* Do not hide render profile list when there is only one item to avoid confusion (http://kdenlive.org/mantis/view.php?id=2543)
* Fix consecutive error messages overwriting each other (http://www.kdenlive.org/mantis/view.php?id=2519)
* Fix crash when editing properties of several missing clips
* Fix vectorscope for 24b RGB images (http://kdenlive.org/mantis/view.php?id=2478)
* Fix archiving sometimes not saving playlist clips and subclips (http://kdenlive.org/mantis/view.php?id=2475)
* Fix archiving feature not saving playlist clips and slowmotion clips inside them (http://kdenlive.org/mantis/view.php?id=2475)
* Fix crash recovery feature issues with long or non UTF-8 filenames (http://kdenlive.org/mantis/view.php?id=2441, http://kdenlive.org/mantis/view.php?id=2450)
* Fix rendering jobs sometimes not starting
* Fix crash on proxy creation (concurrency issue) (http://kdenlive.org/mantis/view.php?id=2471)
* Fix zone playing (http://kdenlive.org/mantis/view.php?id=2468)
* Fix click on monitor sometimes not triggering play
* Fix crash when moving a folder and a clip in project tree (http://kdenlive.org/mantis/view.php?id=2458)
* Fix color change not working in title widget (http://kdenlive.org/mantis/view.php?id=2459)
* Fix slideshow clips created with invalid frame duration
* Fix profile warning with clips that have 1088 pixels height
* Fix unnecessary proxy reload on document load
* Fix detection of broken render scripts

* [issue #2329](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2329) Sort clips by timestamp.
* [issue #2581](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2581) Dragging an effect group on an effect duplicates the group's content
* [issue #2551](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2551) Improve the french language
* [issue #264](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=264) timeline numbers overlap and are chopped off at the top
* [issue #1950](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=1950) Select all clips of all tracks
* [issue #1949](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=1949) Select all clips of a track
* [issue #1065](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=1065) Cannot move clip anymore until restart
* [issue #2499](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2499) Loading recent projects freezes interface following recent MLT fix for render failure
* [issue #2516](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2516) Crash on startup
* [issue #2515](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2515) Slideshow clip "frame duration" blank
* [issue #2503](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2503) *** buffer overflow detected ***
* [issue #2468](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2468) Zone behaviour not right
* [issue #2502](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2502) Crash upon start - openSUSE 12.1-64bit
* [issue #2271](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2271) Preview monitor stops display the image
* [issue #2521](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2521) Earlier crash event, details forgotten
* [issue #2519](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2519) Messages in the status bar are not shown at all when a new message immediately follows
* [issue #2522](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2522) Crash while playing
* [issue #2523](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2523) new MLT 0.7.8 crash segfault at end of every rendering job
* [issue #2526](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2526) pcm_s16le codec not found by kde/MLT with recent versions of FFmpeg
* [issue #2535](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2535) kdenlive blocks on opening
* [issue #2543](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2543) Usability: Render profile selector confusing when only one profile exists under category.
* [issue #2544](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2544) Crash on startup
* [issue #2573](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2573) "With track" of composite transition jumps back to auto as soon as parameters are changed
* [issue #2588](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2588) Right Hand Mouse Click On Audio Track 2 Box -&gt; CRASH
* [issue #2576](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2576) Saved effect group preset are added to a clips existing effect group of the same name
* [issue #2587](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2587) Kdenlive git fails to build with Qt&lt; 4.7 [audioCorrelation]
* [issue #2578](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2578) adding a saved effects group preset by right clicking on the strip does not work
* [issue #2580](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2580) Capitalization of effect and effect group presets' names is not recognized properly
* [issue #2597](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2597) Crash
* [issue #2596](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2596) Build Kdenlive for Ubuntu 12.04
* [issue #1322](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=1322) Rendering Option "Selected Zone" renders only the first frame of selected zone
* [issue #2603](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2603) Kdenlive fails to build with KDE&lt;4.5
* [issue #2002](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2002) Clip zones inserted in the project tree are lost after re-opening project
* [issue #2181](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2181) Kdenlive does not render composite clips correctly
* [issue #2388](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2388) Loading a lot of HD clips crashes UI after a while.
* [issue #2357](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2357) Project does not render video channel on webm
* [issue #2382](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2382) Crashes upon startup.
* [issue #2429](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2429) Kdenlive crasches
* [issue #2427](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2427) mlt from git makes UI crash.
* [issue #2456](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2456) Will not start after upgrading version. Crash handler every time.
* [issue #2448](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2448) Crash when switching profile
* [issue #2438](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2438) Crash after launch
* [issue #2459](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2459) Title Clip - change color of selected text object fails
* [issue #2457](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2457) Compilation On Debian Squeeze 32bit (KDE 4.4.4) Fails after switch from QJson to Solid
* [issue #2472](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2472) Proxies don't get queued automatically.
* [issue #2471](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2471) Kdenlive crashes while creating proxies.
* [issue #2465](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2465) crash when moving project monitor
* [issue #2473](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2473) Image clip duration reverts to 5s after reloading project
* [issue #2480](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2480) qt related crash when starting kdenlive
* [issue #2487](https://web.archive.org/web/20120723021052/http://kdenlive.org/mantis/view.php?id=2487) Crash after adding a clip
