  * Fix crash when clip is modified by external app. [Commit.][1]
  * Fix paste clip broken until close/repoen app if trying to paste an invalid clip (missing of playlist with different fps). [Commit.][2]
  * Fix double clicking mixed clip start corrupting mix. [Commit.][3]
  * Fix incorrect mutex unlock in thumbs cache. [Commit.][4]
  * Ensure tasks are properly terminated on close, fix incorrect mutex in thumbnailcache causing corruption. [Commit.][5]
  * Ensure queued tasks are not started on project or test close. [Commit.][6]
  * Don’t remove consecutive spaces in SRT subtitles. [Commit.][7] Fixes bug [#457878][8]
  * Fix archiving when a clip is added twice in a project. [Commit.][9]
  * [Mix Stack] Fix wrongly reversed position slider. [Commit.][10]

 [1]: http://commits.kde.org/kdenlive/edea6a279b07c058b41cf20eadeb5b8fcddb37cb
 [2]: http://commits.kde.org/kdenlive/6c52d311559b033fa47600a56280d0f911d6323b
 [3]: http://commits.kde.org/kdenlive/fd5b4436e85f0ac307baa49a6bd9941c277f43b3
 [4]: http://commits.kde.org/kdenlive/43cba9cc57144e331206aae85b218e27b24bf665
 [5]: http://commits.kde.org/kdenlive/bb8e6ffef87bbbdaf1808a07d35675a174c296c3
 [6]: http://commits.kde.org/kdenlive/3db8a140c41774ff5045ed7ef70e9df563447fbf
 [7]: http://commits.kde.org/kdenlive/6e6d06dd2d010ea88160783bc89f751c25a868cd
 [8]: https://bugs.kde.org/457878
 [9]: http://commits.kde.org/kdenlive/234358f5c19d9b9bcb768e8e99ff039f91ba86cb
 [10]: http://commits.kde.org/kdenlive/0b650bcf069d1b08b9e3d8a270163e63ba15b231
