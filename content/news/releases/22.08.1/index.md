---
author: Farid Abdelnour
date: 2022-09-18T15:06:48+00:00
aliases:
- /en/2022/09/kdenlive-22-08-1-released/
- /it/2022/09/kdenlive-versione-22-08-1/
- /de/2022/09/auto-draft-4/
---
The first maintenance release of the 22.08 series is out fixing issues with project archiving, same track transitions among others.
