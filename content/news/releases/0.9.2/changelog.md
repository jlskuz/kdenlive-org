* Fix freeze when reloading previously missing clip
* Fade effects lost when moving / resizing clip
* Undoing change in clip crop start breaking clip
* Make disabling of track effects possible
* Fix slideshow clips not working
* Fix crash on composite transition
* Fix crash when opening stop motion widget
* Fix rendering of projects created in another locale
* Fix Firewire capture

* [issue #1646](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=1646) Titler: Move multiple items
* [issue #1664](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=1664) Titler: Delete multiple objects at once
* [issue #1708](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=1708) Titler: Select custom background clip for positioning and better contrast
* [issue #2570](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2570) Fade in, Fade out and Fade from/to black are not working sometimes
* [issue #2600](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2600) Vignetting effect has stopped functioning.
* [issue #2601](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2601) The settings of some effects are setting to zero
* [issue #2623](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2623) Titles using composite are invisible in the movie exported.
* [issue #2607](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2607) crash when i choose: "Acquisition d'animation image par image"
* [issue #2616](https://web.archive.org/web/20130423143104/http://kdenlive.org/mantis/view.php?id=2616) Add Slideshow Clip creates an unusable clip of length 8 min 20 sec

