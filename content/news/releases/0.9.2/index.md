---
author: Jean-Baptiste Mardelle
date: 2012-05-31T00:36:00+00:00
aliases:
- /users/j-b-m/kdenlive-092-released
- /discover/0.9.2
# Ported from https://web.archive.org/web/20150905105657/https://kdenlive.org/users/j-b-m/kdenlive-092-released
# Ported from https://web.archive.org/web/20150905190307/https://kdenlive.org/discover/0.9.2
---


The 0.9.2 version of Kdenlive has been released, the source package is now available from the [KDE servers][1].  
This new release fixes the most important regressions found in the 0.9 version as well as a few other issues.

The crashes in the stop motion widget and composite transition are fixed, slideshows and firewire capture work again. For a full changelog, check below.

All users are encouraged to upgrade and report problems, we will prepare another bugfix release in a few weeks.

  [1]: https://download.kde.org/Attic/kdenlive/0.9.2/src/kdenlive-0.9.2.tar.bz2.mirrorlist
