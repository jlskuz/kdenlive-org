---
date: 2007-08-11
author: Jean-Babtiste Mardelle
---

After 8 months of hard work, we are glad to announce the release of Kdenlive 0.5. Kdenlive is a non linear video editor for the KDE environnment. Improved stability and many new features have been added since Kdenlive 0.4.

Being based on the the MLT video framework and the FFMPEG project, Kdenlive can work with image, audio and video files of various formats. All these formats can be mixed in your project on an unlimited number of audio and video tracks. Thanks to the MLT framework, Kdenlive 0.5 now supports HDV editing as well as DV and more.

## New features include:

- New image formats  
  Added support for gimp xcf and exr.  
  Other supported formats are: png, jpeg, gif (non animated), xcf, exr, tiff, svg, mp3, vorbis, wav, flash, mpeg, avi, dv, wmv,... You can even insert another Kdenlive project in the timeline!
- Effects and Transitions  
  Many fixes, and effects and transitions can now be copied from a clip to another.
- Export formats
  Theora export is now available if FFMPEG was compiled with theora support.
- Timeline  
  Improved markers, allow insertion of of several clips in one operation, added "remove empty space" feature, multi clip selection tool
- Metadata  
  Kdenlive can now read and write metadata in video and audio files
- Video preview
  Overlay project timecode on the video preview. Click in monitor to play / pause, mouse wheel to seek, multi track view (monitor is split in 4 zones, each one showing a different track),
- Project management
  User can define a frame that will be used as thumbnail for each video clip in the project
- General
  Many fixes to the undo / redo system, lots of ui fixes and enhancements

For more information:

Kdenlive web site: http://kdenlive.org

To compile Kdenlive, please follow the detailled instructions at:
http://en.wikibooks.org/wiki/Kdenlive/Getting_and_installing

You can also check our forums and development mailing list:
- Forum: "http://kdenlive.org/bbforum"  _[target does not exist anymore]_
- Mailing List: http://sourceforge.net/mail/?group_id=46786 _[note: this maling list is not active anymore]_
