---
author: Jean-Baptiste Mardelle
date: 2015-04-16T19:58:00+00:00
aliases:
- /node/9433
# Ported from https://web.archive.org/web/20160321025452/https://kdenlive.org/node/9433
---


The Kdenlive team is happy to announce the release of Kdenlive 15.04.0. While there are very few new features in this release, it is a huge step towards a bright future!

![](kdenlive-15-04-0.png)

## Kdenlive is now an official KDE application

This has several implications:

* We fully benefit from KDE's infrastructure, which means less worries for the developpers.
* We stick to KDE Applications release schedule, which means one bugfix release every month, one feature improved version every 4 months. That's a big change from the previous random release every year or so. This is possible because the KDE team takes care of the release, not our small dev team, so a big thank you to KDE.
* We now use KDE's bugtacker at [https://bugs.kde.org][2].
* We benefit from KDE's build servers and team, which means that we might in the future have Mac OS and Windows versions without too much efforts from the dev team.
* We can now be part of the Google Summer Of Code.
* We have adopted the KDE Applications numbering scheme. From now on, Kdenlive versions will be numbered with Year.Month.Bugfix. That explains the 15.04.0 version.
* Every KDE contributor can help us improve Kdenlive.

## What changes right now for Kdenlive users

Most of the work for this release was porting the code to Qt5 and KDE Frameworks 5 (KF5). While users will not see direct benefit, this makes us ready for the next big steps. Changes in this version include:

* Since we are now based on Qt5/KF5, you NEED KDE Frameworks 5 to run Kdenlive.
* Fixed video stabilization
* Auto save new projects
* Download new render profile feature fixed

You can download the [source code][3], binary packages for your distro should hopefully be prepared by distibution packagers.

## What will change in the near future

While Kdenlive 15.04.0 is mostly a Qt5/KF5 port, we have many new features/improvements in preparation for the 15.08.0 release. Here are some of the features that we are currently working on:

* Finally integrate some of Till Theato's work resulting from our Indiegogo campain. It took us 2.5 years but we are finally merging parts of the refactoring effort.
* Use OpenGL for the video display, bringing back experimental GPU display and effects
* Add effects to Project clips: for example, add color correction to the clip in the Project Bin. Every instance of the clip that is then used in timeline will have that color correction.
* Cleaning the code to make it easier to understand.

That's it for today, I probably forgot many things but that might be an excuse to blog more often :).


  [2]: https://bugs.kde.org/buglist.cgi?quicksearch=product%3Akdenlive
  [3]: https://download.kde.org/stable/applications/15.04.0/src/
