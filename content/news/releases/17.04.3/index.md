---
author: Farid Abdelnour
date: 2017-07-13T21:55:46+00:00
aliases:
- /en/2017/07/kdenlive-17-04-3-released/
---
The last point release of the 17.04 cycle is out with crash and compilation fixes and minor interface improvements.

In comparison to previous versions this was the least exciting development cycle, in terms of new features, since all focus has been on the code refactoring which will bring more stability and new features. Don't miss the next [Café][1] to keep track on the progress and share your thoughts if you like.

 [1]: /2017/07/kdenlive-cafe-19-and-20/
