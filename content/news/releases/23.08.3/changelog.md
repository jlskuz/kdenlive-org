  * Fix timeremap. [Commit][1].
  * Fix replace clip keeping audio index from previous clip, sometimes breaking audio. [Commit][2]. See bug [#476612][3].
  * Create sequence from selection: ensure we have enough audio tracks for AV groups. [Commit][4].
  * Fix timeline duration incorrect after create sequence from timeline selection. [Commit][5].
  * Fix project duration not updating when moving the last clip of a track to another non last position. [Commit][6]. See bug [#476493][7].
  * Don&#8217;t lose subtitle styling when switching to another sequence. [Commit][8]. Fixes bug [#476544][9].
  * Fix crash dropping url to Library. [Commit][10].
  * When dropping multiple files in project bin, improve import speed by not checking if every file is on a remote drive. [Commit][11].
  * Fix titler shadow incorrectly pasted on selection. [Commit][12]. Fixes bug [#476393][13].
  * Fix pasted effects not adjusted to track length. [Commit][14].
  * Fix timeline preview ignored in temporary data dialog. [Commit][15]. Fixes bug [#475980][16].
  * Speech to text: fix whisper install aborting after 30secs. [Commit][17].
  * Don&#8217;t try to generate proxy clips for audio with clipart. [Commit][18].
  * Clip loading: switch to Mlt::Producer probe() instead of fetching frame. [Commit][19].
  * Multiple fixes for time remap losing keyframes. [Commit][20].
  * Add png with alpha render profile. [Commit][21].
  * Fix Mix not correctly deleted on group track move. [Commit][22].
  * Fix rendering with alpha. [Commit][23].
  * Rotoscoping: don&#8217;t auto add a second kfr at cursor pos when creating the initial shape, don&#8217;t auto add keyframes until there are 2 keyframes created. [Commit][24].
  * Fix keyframe param not correctly enabled when selecting a clip. [Commit][25].
  * Fix smooth keyframe path sometimes incorrectly drawn on monitor. [Commit][26].
  * Properly adjust timeline clips on sequence resize. [Commit][27].
  * Remove unused debug stuff. [Commit][28].
  * Fix project duration not correctly updated on hide / show track. [Commit][29].
  * Fix resize clip with mix test. [Commit][30].
  * Fix resize clip start to frame 0 of timeline not correctly working in some zoom levels,. [Commit][31].

 [1]: http://commits.kde.org/kdenlive/7f89dc52c356550bfb419c5e9d1e388a3f0fc4b8
 [2]: http://commits.kde.org/kdenlive/aa68d9e2e5dc3c2f16edc7865a69acc9fa3ecb18
 [3]: https://bugs.kde.org/476612
 [4]: http://commits.kde.org/kdenlive/bf6c6299cca9190ce404091bb86330ac36bdb063
 [5]: http://commits.kde.org/kdenlive/28f67c08274b762dfcff746e59d0a851f4d165e2
 [6]: http://commits.kde.org/kdenlive/19c63f04fb50c81717f36142ee21f09e3d359425
 [7]: https://bugs.kde.org/476493
 [8]: http://commits.kde.org/kdenlive/528e88c9b083ac644338b7cbbcbcc06a4172d14d
 [9]: https://bugs.kde.org/476544
 [10]: http://commits.kde.org/kdenlive/e1bcabe42f33a4e8e2de9e67a62eab1e5024a38e
 [11]: http://commits.kde.org/kdenlive/7d71f50332b65d35de2e10f436f78ee36b293491
 [12]: http://commits.kde.org/kdenlive/72f45eb94496486d8fa90a9540a9e5554f72e596
 [13]: https://bugs.kde.org/476393
 [14]: http://commits.kde.org/kdenlive/01acd5fcadd238feba836e7d7579a150a517312a
 [15]: http://commits.kde.org/kdenlive/e5463c1646652f1b75120aaf0db55153c2ba3553
 [16]: https://bugs.kde.org/475980
 [17]: http://commits.kde.org/kdenlive/68d45a57a774b8b09dd3b6f2084435df229af09e
 [18]: http://commits.kde.org/kdenlive/277e8e09ae4ddef9948073079e5f737b431decfe
 [19]: http://commits.kde.org/kdenlive/707f139f5d869ec659045c0d534f90a762d4021c
 [20]: http://commits.kde.org/kdenlive/84f7373df491d7cf4a38330a70a9cb74e9d0b860
 [21]: http://commits.kde.org/kdenlive/3fb09a9c368ec25ee1e72245550766d45f8c81e1
 [22]: http://commits.kde.org/kdenlive/e4a4f3c5d5f567b1f2a0dbc15d57dc82f39654a1
 [23]: http://commits.kde.org/kdenlive/b9bf00fe63786b25179fb403954d4cf66478373c
 [24]: http://commits.kde.org/kdenlive/2f0326e48ab2f0a3b82baaa685c69e6e5774c165
 [25]: http://commits.kde.org/kdenlive/2ec04d4aaeed4c76869e77ed7ecdbeefbcfbc86e
 [26]: http://commits.kde.org/kdenlive/3e81be35d2acf4a5fda557eefce714c524f0a3cb
 [27]: http://commits.kde.org/kdenlive/f4ab1a556add5e25a181ae62d852c214cea042cf
 [28]: http://commits.kde.org/kdenlive/4bcf1a6ac433ece524ba90901b70b4a895bc443c
 [29]: http://commits.kde.org/kdenlive/84cc82268c4dc579b3d398af64b2261a881daca3
 [30]: http://commits.kde.org/kdenlive/e338f811e4429e260c9d1799f08cc1fe61f81244
 [31]: http://commits.kde.org/kdenlive/8424aa13a7977f0ecfc53bfa0891f15a736f78a9
