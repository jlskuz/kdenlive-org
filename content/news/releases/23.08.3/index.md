---
author: Farid Abdelnour
date: 2023-11-13T13:18:28+00:00
aliases:
- /en/2023/11/kdenlive-23-08-3-released/
- /it/2023/11/kdenlive-23-08-3-2/
- /de/2023/11/kdenlive-23-08-3/
discourse_topic_id:
  - 7150
---

Kdenlive 23.08.3 continues the stabilization effort of this release cycle in preparation for the Qt6 upgrade. Some highlights of this release include: Importing clips is now faster (as part of the performance improvements task); added a new PNG with alpha render profile and fixes the video with alpha render profiles; time remapping can now be applied to sequences and Whisper now works on all systems.

