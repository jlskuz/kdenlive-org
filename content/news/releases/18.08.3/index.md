---
author: Farid Abdelnour
date: 2018-11-09T13:00:46+00:00
aliases:
- /en/2018/11/kdenlive-18-08-3-released/
- /fr/2018/11/version-18-08-3-de-kdenlive/
- /it/2018/11/3290/
---

![Screenshot of Kdenlive version 18.08.3](screenshot.png)

Kdenlive 18.08.3 is out with updated build scripts as well as some compilation fixes. All work is focused on the refactoring branch so nothing major in this release. On the other hand in the Windows front some major breakthroughs were made like the fix of the play/pause lag as well as the ability to build Kdenlive directly from Windows. The next milestone is to kill the running process on exit making Kdenlive almost as stable as the Linux version.

In other news, we are organizing a bug squash day on the first days of December. If you are interested in participating this is a great opportunity since we have prepared a list of low hanging bugs to fix. See you!

[Get AppImage][1]

 [1]: https://download.kde.org/Attic/kdenlive/18.08/linux/kdenlive-18.08.3-x86_64.AppImage
