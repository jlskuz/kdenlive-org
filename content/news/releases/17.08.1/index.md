---
author: Farid Abdelnour
date: 2017-09-07T18:22:46+00:00
aliases:
- /en/2017/09/kdenlive-17-08-1-released/
---
Although the team is at full throttle getting ready for the 17.12 big refactoring release, we make available the first point release of the 17.08 series bringing various bugfixes and usability improvements. Stay tuned for testing the refactoring branch packages to be [announced soon][1].


 [1]: /2017/09/kdenlive-development-and-upcoming-events/
