  * Allow resizing unselected subtitles. [Commit.][2]
  * Remove env variable breaking UI translation. [Commit.][3]
  * Fix clip with mix transition cannot be cut in some circumstances. [Commit.][4]
  * Ensure all track tags have the same width if more than 10 tracks. [Commit.][5]
  * Fix rendering uses wrong locale, resulting in broken slowmotion in render and possibly other issues on some locales. [Commit.][6]
  * Expose proxy info in playlist clip properties (to allow delete, etc). [Commit.][7]
  * Fix proxied playlists rendering blank and missing sound. [Commit.][8]
  * Fix playlist proxies broken. [Commit.][9]
  * Fixed issue where changing speed resets audio channel of clip to channel 1. [Commit.][10]
  * Ensure color/image/title clips parent producer always has out set as the longest duration of its timeline clips. [Commit.][11]
  * Ensure clips have an “unnamed” label if name is empty. [Commit.][12]
  * Effect keyframe minor fixes (improve hover color and allow pasting param to keyframe 0). [Commit.][13]
  * Re-enable audio playback on reverse speed. [Commit.][14]
  * Fix changing speed breaks timeline focus. [Commit.][15]
  * Ensure a group/ungroup operation cannot be performed while dragging / resizing a group. [Commit.][16]
  * Cleanup monitor overlay toolbars and switch to QtQuick2 only. [Commit.][17]
  * Improve show/hide monitor toolbar (ensure it doesn’t stay visible when mouse exits monitor). [Commit.][18]
  * Correctly disable subtitle widget buttons when no subtitle is selected, add button tooltips. [Commit.][19]
  * Fix lift value incorrect on click. [Commit.][20] Fixes bug [#431676][21]
  * Update render target when saving project under a new name. [Commit.][22]
  * Improve and fix ressource manager, add option to add license attribution. [Commit.][23]
  * Fix some crashes on subtitle track action. [Commit.][24]
  * Set range for zoome of avfilter.zoompan to 1-10 (effect doesn’t support. [Commit.][25]
  * Improve subtitle track integration: add context menu, highlight on active. [Commit.][26]
  * Fix incorrect arguments parsing on app restart. [Commit.][27]
  * Fix build. [Commit.][28]
  * Attempt to fix subtitle encoding issue. [Commit.][29]
  * Fix recent regression (crash moving clip in timeline). [Commit.][30]
  * Fix subtitles not displayed on project opening. [Commit.][31]
  * Update copyright year to 2021. [Commit.][32]
  * Fix disabled clip regression (color and opacity changes were not applied anymore). [Commit.][33]
  * Fix compilation. [Commit.][34]
  * Delete equalizer.xml. [Commit.][35]
  * Delete eq.xml. [Commit.][36]
  * Delete selectivecolor.xml. [Commit.][37]
  * Delete unsharp.xml. [Commit.][38]
  * Dragging an effect from a track to another should properly activate icon and create an undo entry. [Commit.][39]
  * Always keep timeline cursor visible when seeking with keyboard, not only when “follow playhead when playing is enabled”. [Commit.][40]
  * Fix broken Freesound login and import. [Commit.][41]
  * Fix regression in subtitle resize. [Commit.][42]
  * Fix clips incorrectly resized on move with mix. [Commit.][43]
  * Fix grouped clips independently resized when resizing the group. [Commit.][44]
  * Implement missing subtitle copy/paste. [Commit.][45] Fixes bug [#430843][46]

 [2]: http://commits.kde.org/kdenlive/d4b9ee6528f7de95a77427a52f010ba2808cee96
 [3]: http://commits.kde.org/kdenlive/9619b3f5108ec99aded66d3102e844e4debcc6bf
 [4]: http://commits.kde.org/kdenlive/c3bae8e416054abc92b965c554166fadde4b0424
 [5]: http://commits.kde.org/kdenlive/5adf0dcb309353a9c2e8b58c1009c05f6460ec5a
 [6]: http://commits.kde.org/kdenlive/b9aa046df30a15315f7871971c95dffb3d0863ca
 [7]: http://commits.kde.org/kdenlive/0b60b1e999627ecd8015b83c78320af7b467d2fe
 [8]: http://commits.kde.org/kdenlive/617c3acbbb8252a50a90c0c90f14f94ea63f7fb8
 [9]: http://commits.kde.org/kdenlive/c05f98274fabc748cb3a0da28196fcaeeb8a716e
 [10]: http://commits.kde.org/kdenlive/7c85faf53e42ad2a33f7ab9ec58eabefe881e37e
 [11]: http://commits.kde.org/kdenlive/84aea416f80568e85fb1003e1278eade782fe91d
 [12]: http://commits.kde.org/kdenlive/b331578c0935b45b166d25eab9125044e7d77423
 [13]: http://commits.kde.org/kdenlive/3316f377a26c6bec3792cc53b88b2897d1aab585
 [14]: http://commits.kde.org/kdenlive/9d4018a64f81ef36d8ace73cad55585eeec1c73c
 [15]: http://commits.kde.org/kdenlive/6a4be05821365ed4d126dc75739864155e50e69f
 [16]: http://commits.kde.org/kdenlive/cb7ee64327b215ef96aea3450c0f87ca2440ffd7
 [17]: http://commits.kde.org/kdenlive/dadd06f64026feb0f2878ac37121e94f4eab7432
 [18]: http://commits.kde.org/kdenlive/cb9cba9d84628a8543f311da6a5476a5a77fc954
 [19]: http://commits.kde.org/kdenlive/4291b1113ac37b8f014a7b680e5f9247313227ea
 [20]: http://commits.kde.org/kdenlive/fb0e09bad2cc3e7a65bcc704f82050a346609677
 [21]: https://bugs.kde.org/431676
 [22]: http://commits.kde.org/kdenlive/3b5b1d4fb6121003678ab36b382c2432f04ffe39
 [23]: http://commits.kde.org/kdenlive/ed5c91d5ac307c28c84b322fcf9a394a83eb466f
 [24]: http://commits.kde.org/kdenlive/f104d8aca641a48b21f19d165face861a7953e37
 [25]: http://commits.kde.org/kdenlive/15a2ce8149bdb6759f74d003e999b2b547ea2de9
 [26]: http://commits.kde.org/kdenlive/b8afadbaba53444f5d57353dbeafd3461de81daa
 [27]: http://commits.kde.org/kdenlive/30ec7007055af454ecba3e72c04e65aac28f49f9
 [28]: http://commits.kde.org/kdenlive/92a64e79f3efe8805c24a742a934119818080f27
 [29]: http://commits.kde.org/kdenlive/6d185ecae7edf61281dd3b69767c57107ee6491e
 [30]: http://commits.kde.org/kdenlive/ab66afb851339bb002ea104fb2a821282582d8c0
 [31]: http://commits.kde.org/kdenlive/6d5bd03dc5d96748f0d01fef6e98f435445adf8d
 [32]: http://commits.kde.org/kdenlive/77a5a5cee103b917b2864759116ea06fb7059e78
 [33]: http://commits.kde.org/kdenlive/ffb46fdef81bbdce33e2278d7be874993148e807
 [34]: http://commits.kde.org/kdenlive/ad01afc905c0c14fc3a4039b40f39b19ee07ae93
 [35]: http://commits.kde.org/kdenlive/ded6dbf92d990e487cff7578692af6db92210794
 [36]: http://commits.kde.org/kdenlive/81d5acce0758385d427754013a5fa96146c05e54
 [37]: http://commits.kde.org/kdenlive/90b8665a8ffa86674563c230d7556e1a6cdc478a
 [38]: http://commits.kde.org/kdenlive/84968140418dde1d54fb70c0b6a2e77a510e7622
 [39]: http://commits.kde.org/kdenlive/e5780eee961b51938ad205bda65e38d6024decb6
 [40]: http://commits.kde.org/kdenlive/c25fa35a4cf6fa64af9da021bce152cdc9e61554
 [41]: http://commits.kde.org/kdenlive/ed9daa2089fc11b820dedff26e8f23ab649c32a1
 [42]: http://commits.kde.org/kdenlive/3cfaf1a514980571cd9a932d49fcfd7b6ee4eba8
 [43]: http://commits.kde.org/kdenlive/def2096267ef1569de0cc10703043e27c98704ff
 [44]: http://commits.kde.org/kdenlive/a7d819551b7d77109498ce570b260f962c3f230a
 [45]: http://commits.kde.org/kdenlive/fcb241737316cdec7997c1aba761e21b656d2dcd
 [46]: https://bugs.kde.org/430843
