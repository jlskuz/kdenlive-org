---
title: Kdenlive 20.12.2 available
author: Jean-Baptiste Mardelle
date: 2021-02-08T07:00:22+00:00
aliases:
- /en/2021/02/kdenlive-20-12-2-available/
- /it/2021/02/kdenlive-20-12-2-disponibile/
- /de/2021/02/kdenlive-20-12-2-steht-zur-verfuegung/
---

Kdenlive 20.12.2, part of our monthly bugfix release, is now available and fixes several important issues. Among the changes:

  * Fix copying an effect from a track to another
  * Several fixes/improvements for the newly added subtitle feature: 
      * Implement copy/paste
      * Fix broken resize
      * Fix subtitles encoding issue happening on some systems
      * Improve timeline integration (track can now be highlighted and items resized with standard shortcuts)
      * Various crash fixes
      * Windows: Subtitle with special characters like äöü are now correct viewed after re-loading the project file.
  * Fix crash when trying to group/ungroup items while performing a drag operation
  * Fix timeline click not working after a speed change operation
  * Fix effect keyframes sometimes broken on image / title clips
  * Fix speed change resetting audio channel
  * Make playlist proxy clips work again
  * Fix rendering issues on some systems, like slowmotion effect not working
  * Fix UI translations not working or only partially
  * Fix clips with mix sometimes cannot be cut or behaving incorrectly

The downloads are available as usual from our [Download page][1].

 [1]: /download
