---
date: 2006-12-04
draft: false
#layout: release-notes
author: Jean-Baptiste Mardelle
---

We are happy to announce the **release of Kdenlive 0.4**. Kdenlive is a non linear video editor for the KDE environnment. Since last version, many improvements and features have been added.

Being based on the the MLT video framework and the FFMPEG project, Kdenlive can work with image, audio and video files of various formats. All these formats can be mixed in your project on an unlimited number of audio and video tracks. Kdenlive supports PAL and NTSC standards, in 4:3 and 16:9 ratios.

- Supported input formats  
  png, jpeg, gif (non animated), svg, mp3, vorbis, wav, flash, mpeg, avi, dv, wmv, ... You can even insert another Kdenlive project in the timeline!
- Firewire capture  
  A dv firewire capture monitor has also been added.
- Supported export formats  
  dv, avi, mpeg, flash, wav, vorbis, mp3, quicktime, realvideo, png Export is now non blocking, you can continue working while exporting
- DVD wizard  
  A basic DVD wizard allows you to burn your movie to a dvd, with a simple title
- Project management  
  Your project clips can now be arranged in folders.
- Timeline  
  Many new keyboard shortcuts have been added for easier navigation. You can copy and paste clips. Magnetic guides have been added, as well as virtual clips that allow you to create "clones" of a zone. You can insert commented markers in clips, there is a new autoscroll while playing feature, and many more.
- Video effects  
  Brightness, Charcoal, Gamma, Greyscale, Invert, Mirror, Obscure, Sepia, Speed, Stroboscope, Freeze
- Audio effects  
  Mute, Volume, Declipper, Vinyl, Limiter, Equalizer, Phaser, Pitch Scaler, Pitch Shifter, Rate Scaler, Reverb, Room Reverb
- Transitions  
  Crossfade, Push, Picture in Picture, Luma Wipe.
- Languages  
  Thanks to our translation team, Kdenlive is available in english, french, german and spanish

For more information:

Kdenlive web site: "http://kdenlive.sf.net" _[target does not exist anymore]_

A more complete list of features can be found on our Wiki:
"http://kdenlive.sourceforge.net/wiki/index.php?title=Kdenlive_features" _[target does not exist anymore]_

To compile Kdenlive, please follow the detailled instructions at:
"http://kdenlive.sourceforge.net/wiki/index.php?title=Faq_Install" _[target does not exist anymore]_

You can also check our mailing list:
http://sourceforge.net/mail/?group_id=46786  _[note: this maling list is not active anymore]_
