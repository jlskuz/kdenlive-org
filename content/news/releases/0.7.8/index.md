---
author: Jean-Baptiste Mardelle
date: 2010-09-14T20:56:00+00:00
aliases:
- /users/j-b-m/kdenlive-078-released
- /discover/0.7.8
# Ported from https://web.archive.org/web/20160324091019/https://kdenlive.org/users/j-b-m/kdenlive-078-released
---


![](kdenlive-078-1.png)

We are proud to announce the release of Kdenlive 0.7.8. This new version brings many new features, including improved tools for color correction, improved UI for effects (you can now adjust some transitions and effects directly on the monitor), track effects, improved slideshows and a lot more.

The development team is also slowly growing, and Kdenlive benefits from the recent improvements in the [MLT framework][1](version 0.5.10 is required) and [Frei0r effects][2](version 1.2.0 is recommended) projects.

You can find a detailed list of changes bewlow.

Source code can be downloaded from our [sourceforge page][4], and packages will be announced on our [download page][5] when available.

Support can be found in our [forums][6] and issues reported on our [bugtracker][7].

We hope you will enjoy this release!

For the Kdenlive team:  
Jean-Baptiste Mardelle


## Changes

Some of the new features introduced in this version are:

* Color scopes allowing you to monitor your clip's color information ([read more...][8])
* Track effects, allowing you to apply an effect to an entire track ([read more...][9])
* On monitor editing of transitions ([read more...][10])
* Slideshow animation ([read more...][11])


  [1]: https://www.mltframework.org/
  [2]: https://frei0r.dyne.org/
  [3]: https://web.archive.org/web/20160324091019/https://kdenlive.org/discover/0.7.8
  [4]: https://web.archive.org/web/20160324091019/http://sourceforge.net/projects/kdenlive/
  [5]: https://web.archive.org/web/20160324091019/https://kdenlive.org/node/3150
  [6]: https://discuss.kde.org/tag/kdenlive
  [7]: https://web.archive.org/web/20160324091019/https://kdenlive.org/bug-reports
  [8]: /users/granjow/introducing-color-scopes-histogram
  [9]: /users/j-b-m/track-effects-kdenlive-078
  [10]: /news/historic/drupal/kdenlive-078-monitor-effects
  [11]: /users/ddennedy/slideshow-improvements-coming-version-078
