---
author: Farid Abdelnour
date: 2023-12-11T18:49:43+00:00
aliases:
- /en/2023/12/kdenlive-23-08-4-released/
- /it/2023/12/kdenlive-23-08-4-2/
- /de/2023/12/kdenlive-23-08-4/
discourse_topic_id:
  - 8677
---

Kdenlive 23.08.4 comes with a safeguard when working with variable framerate footage and fixes time remapping and subtitling issues. This version also brings back audio stem export support, which allows to render audio tracks as separate files. In case you are wondering why there is no 23.12 major release this month, the KDE community is gearing up for a [mega release][1] which will upgrade our software stack to [Qt6][2] and [KF6][3] frameworks.

Although these are mostly under the hood changes, it means having a more modern and stable interface with improved Wayland support for Linux users. This transition paves the way for the [upcoming performance enhancements][4] effort, such as the integration of GPU effects.


 [1]: https://kde.org/announcements/megarelease/6/
 [2]: https://www.qt.io/
 [3]: https://develop.kde.org/products/frameworks/
 [4]: https://kdenlive.org/en/kdenlive-roadmap/

