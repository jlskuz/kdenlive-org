  * Fix transparency lost on rendering nested sequences. [Commit][5]. Fixes bug [#477771][6].
  * Fix guides categories not applied on new document. [Commit][7]. Fixes bug [#477617][8].
  * Check MLT&#8217;s render profiles for missing codecs. [Commit][9]. See bug [#475029][10].
  * Fix crash on auto subtitle with subtitle track selected. [Commit][11].
  * Fix qml warning (incorrect number of args). [Commit][12].
  * Fix audio stem export. [Commit][13].
  * When pasting clips to another project, disable proxies. [Commit][14].
  * Don&#8217;t allow creating profile with non standard and non integer fps from a clip. [Commit][15].
  * Fix mix not always deleted when moving grouped clips on same track. [Commit][16].
  * Fix remap crashes. [Commit][17].
  * Ensure timeremap option is disabled when effect is deleted. [Commit][18].
  * Time remap: fix changing speed broken / crashing. [Commit][19].
  
 [5]: http://commits.kde.org/kdenlive/a9a6067bdf41024c4fb8590279895f5ce5e74adf
 [6]: https://bugs.kde.org/477771
 [7]: http://commits.kde.org/kdenlive/127584384c1112638952e51025e3551b528779fb
 [8]: https://bugs.kde.org/477617
 [9]: http://commits.kde.org/kdenlive/f7467c6433b9f5f97d274d0bb7c46c00c5b1a465
 [10]: https://bugs.kde.org/475029
 [11]: http://commits.kde.org/kdenlive/bd0645e73799cd10430de923e7946b8698568add
 [12]: http://commits.kde.org/kdenlive/360b30ac346ebc7df364402872976281d7c3dd44
 [13]: http://commits.kde.org/kdenlive/90785e76e240b231544936754fb4f4c6c8b2fa59
 [14]: http://commits.kde.org/kdenlive/d6ac6eb96cf75c271639f8a4f0f48a51bb25d894
 [15]: http://commits.kde.org/kdenlive/e6abc4331e053bfdfe9e61ac1eb4b7cb2fc27344
 [16]: http://commits.kde.org/kdenlive/9202b7593e8d8fec908107b7ecd84d26568d41cf
 [17]: http://commits.kde.org/kdenlive/3acd3166f73860241ead56f9885b2d2c45124346
 [18]: http://commits.kde.org/kdenlive/c577bf2df813929f8bf9560838bdcdf12963dd40
 [19]: http://commits.kde.org/kdenlive/f9e816fb502513254c893e9b1c01b2cd442a7001
