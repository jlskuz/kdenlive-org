---
author: Farid Abdelnour
date: 2021-06-14T16:18:55+00:00
aliases:
- /en/2021/06/kdenlive-21-04-2-released/
- /fr/2021/06/kdenlive-21-04-2-2/
- /it/2021/06/kdenlive-versione-21-04-2/
- /de/2021/06/kdenlive-21-04-2/
---

The second maintenance release of the 21.04 series is out bringing missing keyframing support to effects (like glitch0r, scratchlines and charcoal) as well as the usual batch of bug fixes and usability improvements.
