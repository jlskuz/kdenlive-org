  * Remove duplicate line from last cherry-pick. [Commit.][1]
  * Hopefully proper patch to solve “white” rendering issues. [Commit.][2]
  * Fix resize clip end does not allow touching next clip. [Commit.][3]
  * Fix clip thumbs disappearing on timeline resize. [Commit.][4]
  * Fix timeline thumbnails not saved with project. [Commit.][5]
  * Don’t discard subtitle files on project fps change. [Commit.][6]
  * Update guides position on project’s fps change. [Commit.][7]
  * Fix delete selected clips not working on project opening. [Commit.][8]
  * Fix Chroma Key: Advanced edge mode normal was reset to hard. [Commit.][9]
  * Fix various frei0r effects losing parameter settings:. [Commit.][10]
  * Next try to fix keyframe view positon for mixes. [Commit.][11]
  * Revert “Fix keyframeview position in mixes”. [Commit.][12]
  * Fix keyframeview position in mixes. [Commit.][13]
  * Make effects keyframable: scratchlines, tcolor, lumaliftgaingamma. [Commit.][14] See bug [#393668][15]
  * Make effects keyframable: charcoal, dust, oldfilm, threshold.xml. [Commit.][16] See bug [#393668][15]
  * Make glitch0r effect keyframable. [Commit.][17] See bug [#393668][15]
  * Fix profile repository not properly refreshed after change. [Commit.][18]
  * Fix marker monitor overlayer flickers on hover. [Commit.][19]
  * Ensure timeline zoombar right handle is always visible. [Commit.][20]
  * Fix issue with duplicated title clips. [Commit.][21]
  * Fix effect sliders on right to left (rtl) layouts. [Commit.][22] Fixes bug [#434981][23]
  * Fix alignment of statusbar message label. [Commit.][24] Fixes bug [#437113][25]
  * Fix crash using filter with missing MLT metadata (vidstab in MLT 6.26.1). [Commit.][26]
  * Try to fix wrongly set color in titler. [Commit.][27]

 [1]: http://commits.kde.org/kdenlive/e2da06720b9116199e6ef26ea006592f9b8734ac
 [2]: http://commits.kde.org/kdenlive/fcd82a3fdeddae478280ee098555449c8ecf46fa
 [3]: http://commits.kde.org/kdenlive/e4c9bebe80056b0e633e175af40d2d54bb798f0a
 [4]: http://commits.kde.org/kdenlive/f4a0821916263d41909aac83a680adb77e1d4dd0
 [5]: http://commits.kde.org/kdenlive/2144251aec50f46700e84a006105f962783bae89
 [6]: http://commits.kde.org/kdenlive/e0e7d0ba71b98af286e14cfc75754541e5f4c338
 [7]: http://commits.kde.org/kdenlive/42fdaec38aa3149ed8542849c3c3813d676ce5c0
 [8]: http://commits.kde.org/kdenlive/e5ed85c026a3172d29a7a4dc28cd9e4ac952e167
 [9]: http://commits.kde.org/kdenlive/22c506e5c6545b850a1df3cc5eac75723d279e53
 [10]: http://commits.kde.org/kdenlive/082258a9580486056f54111481cd5f4d700e9a0d
 [11]: http://commits.kde.org/kdenlive/b043a164dd4bce812dd6ae7dcd382cc276270941
 [12]: http://commits.kde.org/kdenlive/39c32a8d8a7bed57a93bf3c9a45f2a1b23ef708f
 [13]: http://commits.kde.org/kdenlive/0884e2475e72759ead696f1be98ac5c323b2b62c
 [14]: http://commits.kde.org/kdenlive/a93b4c33384f8c25c6f0b02ed301765832ef6b77
 [15]: https://bugs.kde.org/393668
 [16]: http://commits.kde.org/kdenlive/0c838d06b00bd705ccd7f0d6542d63a7b6d95b76
 [17]: http://commits.kde.org/kdenlive/e02dec093867bda48bbe60159aece6bf84cbf314
 [18]: http://commits.kde.org/kdenlive/97f6d0267601af947cf2396031d4bc0db35f858f
 [19]: http://commits.kde.org/kdenlive/873b2bee3976534d910bb06250c909fef5ff98b8
 [20]: http://commits.kde.org/kdenlive/441fc496f4c7cb6a7f91131624ca75f131b2ab14
 [21]: http://commits.kde.org/kdenlive/45b9448697de91cd355aaa61773f2aa18980fa2d
 [22]: http://commits.kde.org/kdenlive/06a5cb12d557b1b22d854aea6365847165a37c88
 [23]: https://bugs.kde.org/434981
 [24]: http://commits.kde.org/kdenlive/db00e238c0a91f153a7fb1d66d5ff01ee89e73b9
 [25]: https://bugs.kde.org/437113
 [26]: http://commits.kde.org/kdenlive/cf64f52756069cb13421760ce85ce49a7b18e3a3
 [27]: http://commits.kde.org/kdenlive/94e1d5b03e996e9ca5142eac0db5b8e3a33cbee3
