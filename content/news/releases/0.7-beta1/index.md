---
date: 2008-10-06
title: Kdenlive 0.7beta1 released
author: Jean-Babtiste Mardelle
---

After 10 months of work, we are proud to announce the release of the first beta version of Kdenlive for KDE4.

Kdenlive is an open source multi track video editor based on the MLT video framework and FFmpeg.

## Main Features:
* Capture video from your camcorder, webcam or screen
* Mix a large number of different formats (depending on your FFmpeg install): mpeg, flash, mp3, ogg, png, jpeg, dv, hdv, ...
* See the result of your work (effects and transitions) in realtime
* Export your work in several formats (hdv, dv, mpeg, ...)
* Create titles, slideshows and more

## Download:
You can download the 0.7beta1 source package from our sourceforge project page. Kdenlive relies on several libraries, so you will need some experience to compile from source. If you want to try it, go to our compilation page.

Other users can try the Kdenlive Builder Wizard that downloads, compiles and installs the latest development versions of FFmpeg, MLT and Kdenlive locally without touching your system libraries. Download the builder from: "http://kde-apps.org/content/show.php?content=85826" _[target does not exist anymore]_

## Translations
Kdenlive translation files have not been updated in this beta release. We hope to have several translations available for the 0.7 final release. If you want to help, check our translation page.

## Give us your feedback
We need your help to improve Kdenlive, so please leave a comment on the Feedback page or use our Bug tracker.

## Warning:
Keep in mind that this is Kdenlive version 0.7 beta1. There are still some issues, but we are hoping that you can help us solve them before the final 0.7 release which should occur at the end of October.

