---
author: Farid Abdelnour
date: 2023-07-22T12:09:07+00:00
aliases:
- /en/2023/07/kdenlive-23-04-3-released/
discourse_topic_id: 3090
---
Kdenlive 23.04.3 is out with many fixes to recent regressions and usability improvements to the audio mixer levels display and subtitle widget on smaller screens.
