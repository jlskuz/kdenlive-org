  * Fix tests. [Commit.][1]
  * Fix effects disappearing from timeline sequence and other sync issues. [Commit.][2]
  * Fix crash loading project with an unknown transition. [Commit.][3]
  * Fix crash trying to open backup for moved project file. [Commit.][4]
  * Switch vglobal_quality to vqp for nvenc (same as Shotcut). [Commit.][5]
  * Backport ASAN fix from master. [Commit.][6]
  * Fix ASAN use after freed. [Commit.][7]
  * Mixer: polish audio levels display, add scale for gain slider. [Commit.][8]
  * Fix audio levels gradient colors. [Commit.][9]
  * Mixes: ensure asset panel cleared on undo insert, fix mix inserted at wrong clip end on drop. [Commit.][10]
  * Fix mixes incorrectly saved as luma. [Commit.][11]
  * Ensure Subtitle widget can fit on smaller screens. [Commit.][12] See bug [#470498][13]
  * Fix crash pressing Home on subtitle track. [Commit.][14]

 [1]: http://commits.kde.org/kdenlive/a2aff18bea15b82f7b3c02e3defecb12b6d2ffa4
 [2]: http://commits.kde.org/kdenlive/8bb0fb6e2878294029ce659ac5fa8f8f95c8d415
 [3]: http://commits.kde.org/kdenlive/9bd025d93d7c1c76d0d8e62bcd3b75a7b689b343
 [4]: http://commits.kde.org/kdenlive/933bd53941460c2b1312e1b0cce876b204083e41
 [5]: http://commits.kde.org/kdenlive/030df2cc62244b0be5e9b4baf5d86e172ab0fa67
 [6]: http://commits.kde.org/kdenlive/d9c0ce8a28cdb7479a1e7a96ffffc038cbc62c08
 [7]: http://commits.kde.org/kdenlive/31039db057fe4e146b0d4f4a155402ac1dd82043
 [8]: http://commits.kde.org/kdenlive/dc1d3c94d58c5330c4dcbe26af130d21e19a6f31
 [9]: http://commits.kde.org/kdenlive/f6980031a3708b589e5b685c447ad9b8e2b8f8c2
 [10]: http://commits.kde.org/kdenlive/76b6e7dda88d0924ab758e43dbe797807a789740
 [11]: http://commits.kde.org/kdenlive/6456db0382b83a4ba7c2c101d9a7ffbf872cd996
 [12]: http://commits.kde.org/kdenlive/49a79c75d42126b830ffd94cdf0bd844bdf0aeec
 [13]: https://bugs.kde.org/470498
 [14]: http://commits.kde.org/kdenlive/965e73c64b722037f7aa3202cb07ad1b1686b7a8
