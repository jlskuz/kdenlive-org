---
title: Kdenlive 20.08.1 is out
author: Farid Abdelnour
date: 2020-09-11T18:55:51+00:00
aliases:
- /en/2020/09/kdenlive-20-08-1-is-out/
- /it/2020/09/kdenlive-20-08-1-pronto/
- /de/2020/09/kdenlive-20-08-1-ist-freigegeben-2/
---

The first maintenance release of the 20.08 series is out fixing a regression in the Windows version with de-synced audio and wrong effects when rendering as well as other minor fixes. Currently all work is focused on the new functionality to apply transitions between two clips on same track. This new functionality is planned for upcoming version 20.12.

