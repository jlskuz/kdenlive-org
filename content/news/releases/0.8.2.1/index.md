---
author: Jean-Baptiste Mardelle
date: 2011-12-08T23:27:00+00:00
aliases:
- /users/j-b-m/kdenlive-0821-released
- /discover/0.8.2.1
- /users/j-b-m/kdenlive-bugfix-release
# Ported from https://web.archive.org/web/20160324132547/https://kdenlive.org/users/j-b-m/kdenlive-0821-released
# Ported from https://web.archive.org/web/20150905173703/https://kdenlive.org/discover/0.8.2.1
# Ported from https://web.archive.org/web/20150905113520/https://kdenlive.org/users/j-b-m/kdenlive-bugfix-release
---

![](kdenlive-0821.png)

![](kdenlive_0821b.jpg)

The 0.8.2.1 release of the Kdenlive video editor is now available. This is a bugfix release that solves several startup crashes and other important problems in the 0.8.2 version.

All users should upgrade to that new version that provides an improved stability as well as some threading optimisations that should make some operations slighlty faster.

Source code can be downloaded from our [Sourceforge page][1], and binary packages will be announced when available.

If you are on Ubuntu, you can use the [Sunab experimental PPA][2] that will bring you the latest MLT and Kdenlive (please note that currently it is an english only release, translations should be back in a few days).

A changelog and details of the resolved issues can be found on the below.

For the Kdenlive team,  
Jean-Baptiste Mardelle


  [1]: https://sourceforge.net/projects/kdenlive/
  [2]: https://launchpad.net/~sunab/+archive/kdenlive-svn
