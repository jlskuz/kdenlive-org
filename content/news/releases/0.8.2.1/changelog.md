* Fix title text oultine transparency not working
* Make titler window fit on smaller resolutions (1024x768)
* Fix corruption when undoing and redoing a transition add + move
* Fix possible crash in thumbnails
* Fix possible crashes in clip transcoding and improve feedback when failing
* Various small optimizations (unnecessary clip reloads)
* Fix timecode widget hard to edit and sometimes giving random values
* Workaround locale issue ahappening when system C locale and Qt's locale did not give the same numeric separator
* Fix audio thumbnail concurrency issue
* Fix various video thumbnails issues (don't load several times the same thumb, load all of them, ...)
* Fix crash when opening a file dialog on KDE < 4.5
* Fix various proxy issues (missing extension, concurrency, disabling broken proxy, ...)
* Fix startup crash caused by invalid parsing of v4l data
* Fix project tree disabled after loading some projects / creating new one
* Fix corrupted timeline / monitor timecode
* Fix search path for Luma files when missing

* [issue #2522](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2522) Crash while playing
* [issue #2521](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2521) Earlier crash event, details forgotten
* [issue #2465](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2465) crash when moving project monitor
* [issue #2435](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2435) kdenlive crashes on open or new
* [issue #2373](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2373) Loading project from a few days ago, leaves Project Tree unresponsive / unusable
* [issue #2431](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2431) Title clip window on Asus Netbook
* [issue #2381](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2381) Kdenlive Crashes on start up
* [issue #2385](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2385) Timeline shows wrong time in certain cases
* [issue #2404](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2404) Added transitions do not show up in timeline
* [issue #2403](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2403) concurrent proxy generation segfaults
* [issue #2402](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2402) xvid as render format not available
* [issue #2368](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2368) Crashes when trying to open project file
* [issue #2376](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2376) Crashes on launch
* [issue #2414](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2414) Crash when using open/save as/save/add clip, etc.
* [issue #2391](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2391) Hard crash with opening file or adding clipfile(s)
* [issue #2374](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2374) Crashes when trying to add a video clip
* [issue #2369](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2369) Crashes when specifying render file location
* [issue #2420](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2420) Crash report changing projects
* [issue #2363](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2363) Crash when trying to save project as a new file using "save as"
* [issue #2432](https://web.archive.org/web/20120503213310/http://kdenlive.org/mantis/view.php?id=2432) Crash when creating a new project
