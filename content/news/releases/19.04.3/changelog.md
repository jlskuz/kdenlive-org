  * Fix tools cursor when hovering a clip in timeline. [Commit.](http://commits.kde.org/kdenlive/6657c4f6200c33596d59c883709cacddfa28ffbb)
  * Ensure we don&#8217;t put a video stream in audio streams in mp3. [Commit.](http://commits.kde.org/kdenlive/a67d946ecee7f4b1d839340aa2092383239f3d68)
  * Fix loading .mlt playlist can corrupt project profile. [Commit.](http://commits.kde.org/kdenlive/5c44370a6b61038276ac7b01df3d3cb5f93d897f)
  * When opening a project file with missing proxy and clip, don&#8217;t remove clips from timeline. [Commit.](http://commits.kde.org/kdenlive/ad990b83a79261bd175d2bd19773d4821e917a1b)
  * Improve main item when grabbing. [Commit.](http://commits.kde.org/kdenlive/0998213ec38ba9f5e75a1d44515eb6ba1c63f88f)
  * Fix reloading of title clips and others. [Commit.](http://commits.kde.org/kdenlive/ad382e0c1fece622481414761eb4918fd992c122) Fixes bug [#409569](https://bugs.kde.org/409569)
  * Update Appdata for 19.04.3 release. [Commit.](http://commits.kde.org/kdenlive/bdea7ae78a7f359c2b28a0a214867273404bff01)
  * Fix opening of project files with special character. [Commit.](http://commits.kde.org/kdenlive/d2d98d7f4404ddbf90f0c290d20db11dbe103eb9) Fixes bug [#409545](https://bugs.kde.org/409545)
  * Fix reloading playlist doesn&#8217;t update out. [Commit.](http://commits.kde.org/kdenlive/3910079344ecde7ac177bf5338d6251a6c85c42b)
  * Don&#8217;t leak Mlt repository on first run (attempt to fix Windows fail on first run). [Commit.](http://commits.kde.org/kdenlive/bbe584c698e73371af1c14496a4d15624545c541)
  * Warn and try fixing clips that are in timeline but not in bin. [Commit.](http://commits.kde.org/kdenlive/935abef1ae2a7e0d524f300e4748dfa25750296c)
  * Fix timeline tracks config button only showing menu when clicking its arrow. [Commit.](http://commits.kde.org/kdenlive/e2362f1631ca3c00b557842829480dc9be219f87)
  * Fix lambda not called regression. [Commit.](http://commits.kde.org/kdenlive/c147774f86dcd29f601662b02f30e91f71e7f1bc)
  * Don&#8217;t hardcode width of clip/composition resize handles. [Commit.](http://commits.kde.org/kdenlive/ad4b132030ae32b6e45104339f0f5b2610b0b226)
  * Fix missing luma error on project opening with AppImage. [Commit.](http://commits.kde.org/kdenlive/dead5e255dba5173a10b8701679a67eb04b15e4f)
  * Fix reloading clip doesn&#8217;t update duration. [Commit.](http://commits.kde.org/kdenlive/0c728715c24b679a236719a7cdfa10ebf08c05b7)
  * Fix overwrite/insert drop leaving audio on wrong track. [Commit.](http://commits.kde.org/kdenlive/b48dfdba29f6316dc90aa66a773118ada2d0edf1)
  * Fix error in mirror track calculation. [Commit.](http://commits.kde.org/kdenlive/9137592ab12492736535ee3548661223cc6c9754)
  * Fix overwrite clip with speed change. [Commit.](http://commits.kde.org/kdenlive/b43b737fcd7e03348368720837cb6d62dd4704ad)
  * Fix keyframe corruption on project opening (was creating unexpected keyframe at 0). [Commit.](http://commits.kde.org/kdenlive/1151ca570bc68a0087058e7ce67c38a50930fcdc)
  * Fix keyframes corruption on dragging effect onto another clip. [Commit.](http://commits.kde.org/kdenlive/a80ff117dd60c58095d457fab113a9aa00136ccd)
  * Fix composition cannot be added after deletion / if another composition is placed just after current pos. [Commit.](http://commits.kde.org/kdenlive/e0f9f71e206f7e988153ab3160485cdbfdaf20e1)
  * Fix fades broken on speed change. [Commit.](http://commits.kde.org/kdenlive/33dc4d069267d1216ad5467bc627d48a49629c67) Fixes bug [#409159](https://bugs.kde.org/409159)
  * Fix speed job overwrites without warning. [Commit.](http://commits.kde.org/kdenlive/c0b3b54c84b2421fab20c546a708f5465dce5a63)
  * Fix incorrect crash message on rendering finished. [Commit.](http://commits.kde.org/kdenlive/4708ac10d6773a61bc735656e59755671c9e4c67)
  * Fix timeline preview when fps != 25. [Commit.](http://commits.kde.org/kdenlive/775c26767b9e65e8221eb84ca4525e41865a1052)
  * Fix tests. [Commit.](http://commits.kde.org/kdenlive/487ce84fb95ab5c735bd0ffafe27955018156a52)
  * Effectstack: don&#8217;t display keyframes that are outside of clip. [Commit.](http://commits.kde.org/kdenlive/c97b7fdfbf5df9750d48f9e7444df581b0e447dd)
  * Cleanup in clip/composition resize UI update. [Commit.](http://commits.kde.org/kdenlive/9a8de2e400a3ed527bacabdd61e31b75028397d6)
  * Fix thread/cache count causing concurrency crashes. [Commit.](http://commits.kde.org/kdenlive/c415348425b6e7854e7bb97691c7d7c4ef8bab3d)
  * Don&#8217;t trigger unnecessary refresh on clip resize. [Commit.](http://commits.kde.org/kdenlive/8f533d9c94f10169f8f26a13fc21c126efa15228)
  * Fix crash deleting last track. [Commit.](http://commits.kde.org/kdenlive/faa12ab513ba21d8277e0a23a81ec849f77aad12)
  * Fix duplicate clip with speed change on comma locales. [Commit.](http://commits.kde.org/kdenlive/9d38c49c0262ba4b7fa92289fa52499bd80e4099)
  * Don&#8217;t allow undo/redo while dragging a clip in timeline. [Commit.](http://commits.kde.org/kdenlive/dad1087825956a9bbcf11a501577cadfd2f6444c)
  * Fix crash on cutting group with a composition. [Commit.](http://commits.kde.org/kdenlive/f991f15323de2c3de330cb77d2655dd82fcfcc80)
  * Fix crash on group cut. Fixes #256. [Commit.](http://commits.kde.org/kdenlive/aa6e502bede144efc8fd6e5d6bee7d8e25212c77)
  * Fix playlist duration in bin. [Commit.](http://commits.kde.org/kdenlive/d2bb75f41c3d3c15837bf7abc63553f7caac30e0)
  * Fix crash loading playlist with different fps. [Commit.](http://commits.kde.org/kdenlive/83ab6e7bb14756ad597286e6757dd083e7f5dd9f)
  * Fix thumbs not displayed in all thumbs view. [Commit.](http://commits.kde.org/kdenlive/61bde8d586589791f5904135ec85b42841fbcbbf) See bug [#408556](https://bugs.kde.org/408556)
  * Ensure no empty space between thumbs on all thumbs view in timeline. [Commit.](http://commits.kde.org/kdenlive/b480ffbee8bcca0a83ebb67821ce01cc005875e2)
  * Some cleanup in audio thumbs. Fix recent regression and bug where audio thumbs were not displayed after extending a clip in timeline. [Commit.](http://commits.kde.org/kdenlive/b37162df28806d004a0858a1c186f2a768c6ae87)
  * I18n fixes. [Commit.](http://commits.kde.org/kdenlive/1c8a88a9d828882b15dcd43ebfaa88f69e9a02cb)
  * Use i18n for QML. [Commit.](http://commits.kde.org/kdenlive/73425fa508abc44676a7b8b185dd2c73e001fd01)
  * Fix monitor image hidden after style change. [Commit.](http://commits.kde.org/kdenlive/334a58da7498d6f609c4202c7c91b391f249828d)
  * Fix resize failure leaving clip at wrong size. [Commit.](http://commits.kde.org/kdenlive/24b91d02187ee09815367237643c28f847f9af3d)
  * Fix XML translation for Generators. [Commit.](http://commits.kde.org/kdenlive/a438a5c3e795f8c966514de31077e9b11481bedb)
  * Fix some effects default params on locales with comma. [Commit.](http://commits.kde.org/kdenlive/9f353c193f799d1ed0ddd5745e87b38f1832a6b3)
  * Fix crash after undo composition deletion. [Commit.](http://commits.kde.org/kdenlive/ed02d641b7f01f6d990df80a82857b67d2aa7c9f)
  * Fix i18n for QML. [Commit.](http://commits.kde.org/kdenlive/ab231622c97a040f0de3ccb35122cc42534f38bb)
  * Fix various selection regressions. [Commit.](http://commits.kde.org/kdenlive/6df2ff232b4dd4ba02a24628516ad55e3c18958b)
  * Don&#8217;t export metadata as url encoded strings. [Commit.](http://commits.kde.org/kdenlive/bcd22090855d145d39967a081c7b9350f01a2fb4) Fixes bug [#408461](https://bugs.kde.org/408461)
  * Fix crash on project close, see #236. [Commit.](http://commits.kde.org/kdenlive/8622d3d0853a2524874f2e2a3c45567f109e09f9)
  * Fix zone rendering with updated MLT. [Commit.](http://commits.kde.org/kdenlive/2b0afc3a0230b264b62360321edd5acfc3501826)
  * After undoing deletion, item should not show up as selected. [Commit.](http://commits.kde.org/kdenlive/da564a7042088ec99f2eff2712573be1d047ad24)
  * Fix disable clip broken regression. [Commit.](http://commits.kde.org/kdenlive/c9d917a4ac0082b56443573ece3484b5032ec476)
  * Move zoom options to Timeline, remove Duplicate View. [Commit.](http://commits.kde.org/kdenlive/07988756febb4521c1fae9c95649250191a442a8)
  * Fix crash on item deletion. Fixes #235. [Commit.](http://commits.kde.org/kdenlive/81bcfb691b557974cadf30888c006d4f4f2fc244)
  * Fix fade out moving 1 frame right on mouse release. [Commit.](http://commits.kde.org/kdenlive/bcf6eae11647ddefb237bc0df301f0b774a1feeb)
  * Major speedup in clip selection that caused several seconds lag on large projects. [Commit.](http://commits.kde.org/kdenlive/0ca212cafe7a36bc4b39d4e00e5707c81ef71dc6)
  * Fix changing composition track does not replug it. [Commit.](http://commits.kde.org/kdenlive/fcbb7b7fed2d43b455dcc851d85ae13947a7bda8)
  * Update appdata version(late again sorry). [Commit.](http://commits.kde.org/kdenlive/378c1db01c1aa9675c54fced6387cf5eac6a1788)
  * Fix freeze when moving clip introduced in previous commit. [Commit.](http://commits.kde.org/kdenlive/bd4ae393d085582d2361cf477061064829c2c533)
  * Fix typo that may prevent display of transcode menu. [Commit.](http://commits.kde.org/kdenlive/015f9f9b86af54b1ba2a3eea77bbde7863bdd43c)
  * Don&#8217;t check duration each time a clip is inserted on project load,. [Commit.](http://commits.kde.org/kdenlive/b3c82da9f168741ca3aaaf9e890e553fccc1ba9b)
  * Show progress when loading a document. [Commit.](http://commits.kde.org/kdenlive/824b4cd08b92ac9a0fe62848587b7d60c738e2dc)
  * Make it possible to assign shortcut to multitrack view. [Commit.](http://commits.kde.org/kdenlive/75661730e32d9b922b61b4967179ed746a175acd)
  * Allow resizing item start/end on clip in current track if no item is selected. [Commit.](http://commits.kde.org/kdenlive/9d7abcda524a87b4797c2798eb044e85403777ad)
  * Fix profile change not applied if user doesn&#8217;t want to save current project. [Commit.](http://commits.kde.org/kdenlive/3282e56af6d926e469832eb074980571e8830dd6) Fixes bug [#408372](https://bugs.kde.org/408372)
  * Fix crash on changing project&#8217;s fps. [Commit.](http://commits.kde.org/kdenlive/ae9a18e5649d261848c0175988fe5e3e3ae0be2c) Fixes bug [#408373](https://bugs.kde.org/408373)
  * Add .kdenlive project files to the list of allowed clips in a project. [Commit.](http://commits.kde.org/kdenlive/6a1b8724f1b0400543cbb267946e125ceb530f39) Fixes bug [#408299](https://bugs.kde.org/408299)
  * Correctly save and restore rendering properties for the project. [Commit.](http://commits.kde.org/kdenlive/7c904c5a969010ebb53f55a540f463466ee57410)
  * Workaround MLT consumer scaling issue #453 by using multi consumer. [Commit.](http://commits.kde.org/kdenlive/6c6f31468519e4aabd407be9f0a6b81e811f51a1) See bug [#407678](https://bugs.kde.org/407678)
  * Fix groups keeping keyboard grab state on unselect,. [Commit.](http://commits.kde.org/kdenlive/784454e35718ce1ecdc72e33d46cced2053fe647)
  * Fix the remaining compositing issues reported by Harald (mimick the 18.x behavior). [Commit.](http://commits.kde.org/kdenlive/c6561575e027d581a284e6292e4f1ea571422495)
  * Don&#8217;t warn about missing timeline preview chunks on project opening. [Commit.](http://commits.kde.org/kdenlive/cd371679d457d13ead761d8a3f3b38a3e2345539)
  * Fix forced track composition should indicate state in timeline (yellow background + track name). [Commit.](http://commits.kde.org/kdenlive/c68ab71480cc9b3f8dd1909b3ffb94f4982dcf0e)
  * Save track compositing mode in project to restore it on load. [Commit.](http://commits.kde.org/kdenlive/c9358290cea048020486285844a03f617e1d064b) Fixes bug [#408081](https://bugs.kde.org/408081)
