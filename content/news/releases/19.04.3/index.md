---
title: Kdenlive 19.04.3 is out
author: Farid Abdelnour
date: 2019-07-12T04:00:26+00:00
aliases:
- /en/2019/07/kdenlive-19-04-3-is-out/
- /it/2019/07/kdenlive-19-04-3-scaricabile/
- /de/2019/07/kdenlive-19-04-3-ist-freigegeben/
---

While the team is out for a much deserved summer break the last minor release post-refactoring is out with another huge amount of fixes. The highlights include fixing compositing and speed effect regressions, thumbnail display issues of clips in the timeline and many Windows fixes. With this release we finished polishing the rough edges and now we can focus on adding new features while fixing other small details left. As usual you can get the latest AppImage from our [download page.][1]

Speaking of that, the next major release is less than a month away and it already has some cool new features implemented like changing the speed of a clip by ctrl + resize and pressing shift and hover over a thumb of a clip in the Project Bin to preview it. We've also bumped the Qt version to 5.12.4 and updated to the latest MLT. You can [grab it from here][2] to test it. Also planned is finishing the 3 point editing workflow and improvements to the speed effect. Stay tuned for more info soon.


 [1]: /download
 [2]: https://files.kde.org/kdenlive/unstable/kdenlive-19.07.70-beta-x86_64.appimage.mirrorlist
