---
author: Jean-Baptiste Mardelle
date: 2011-10-31T20:29:00+00:00
aliases:
- /users/j-b-m/kdenlive-082-released
# Ported from https://web.archive.org/web/20160315092244/https://kdenlive.org/users/j-b-m/kdenlive-082-released
# Ported from https://web.archive.org/web/20160805104114/https://kdenlive.org/discover/0.8.2
---

![](kdenlive-082-thumb.png)

We are proud to announce the immediate release of Kdenlive 0.8.2. We encourage all users to upgrade.

For this release, we did put a lot of energy into bug fixing and stability (more than 140 issues solved) to bring you a better editing experience. We have solved a lot of project corruption issues, and the introduction of the automatic backup feature means you should hopefully enjoy Kdenlive better than ever!

Among the features improvements, the proxy clips (clips replacing the original ones when editing a format that is too slow to edit, for example AVCHD) should work fine now, HDMI capture should also be improved (feedback welcome) and some GUI multi threading should make operations a bit smoother.

As usual, we also benefit from the latest improvements in MLT, a new version (0.7.6) was released today that improves stability and fixes important issues for users of non english locale.

We are also getting closer to the KDE community, and the first step was to start a documentation effort, which means Kdenlive is now distributed with a "Quick Start Guide" that should show you the basic steps to start working in Kdenlive. This help is also available and editable online on the [KDE Userbase Wiki][1].

For a detailed list of changes, see below.

And since we want Kdenlive everywhere, you can now also follow us on [Facebook][3], [Twitter][4], [identi.ca][5].

A big thank you to all the developers, translators and users who helped to make this great release.

Source code can be downloaded from our [sourceforge page][6], and packages will be announced as usual on our [download page][7] when available.

Support can be found in our [forums][8] and issues reported on our [bugtracker][9].


## Changes

Kdenlive 0.8.2 is mostly a bug fix release. An automatic backup feature was introduced, and the v4l / decklink capture was rewritten to use MLT instead of implementing our own capture routines, which should improve the webcam support. Apart from that, we did put lots of efforts into bug fixing and stability, the 0.8 release had many problems with corrupted project files that should now be solved. We also improved the handling of missing clips and proxy support was improved and should now work correctly.

We encourage all users to upgrade to 0.8.2 as soon as it is released.

### Automatic Backup

![](backup.png)

Keep copies of older versions of your project file. [bugtracker][10]

  [1]: https://userbase.kde.org/Kdenlive/Manual
  [3]: https://www.facebook.com/pages/Kdenlive/168566469873278
  [4]: https://twitter.com/#!/kdenlive
  [5]: https://web.archive.org/web/20160315092244/http://identi.ca/group/kdenlive
  [6]: https://sourceforge.net/projects/kdenlive/
  [7]: https://web.archive.org/web/20160315092244/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive
  [8]: https://discuss.kde.org/tag/kdenlive
  [9]: https://web.archive.org/web/20160315092244/http://kdenlive.org/bug-reports
  [10]: /users/j-b-m/feature-week-kdenlive-backups
