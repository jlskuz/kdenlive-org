---
date: 2009-02-01
author: Jean-Baptiste Mardelle
---

We are glad to announce the release of Kdenlive 0.7.2. This release focuses mostly on stability and usability issues. Several bugs were found in previous Kdenlive versions that could corrupt your project files, **so we strongly encourage all users to upgrade to the 0.7.2 version.**

## Update (2nd February 09)
A bug was just discovered in Kdenlive 0.7.2 that caused a freeze when starting the Config Wizard using MLT

## New features:

- **Dvd Wizard** Lets you create a simple dvd iso image in a few mouse clicks
- **Job tracking** All running rendering jobs can now be monitored from the Kdenlive rendering window.
- **Rendering** Several improvements: allow rescale, export only video (no audio), hide formats unsupported on your system...
- **Translations** Several new translations were added, so you can now enjoy Kdenlive in 15 different languages

## Main bugfixes:

- **Playlist corruption** Fixed several problems that could corrupt your project when moving several clips or when saving / reloading a project.
- **Effects / Transitions** Fixed several bugs that caused deletion of effects / transitions parameters.
- **File format** Small improvements: remember monitor zone for each clip, fix saving of folders and timeline zone,...


## Getting Kdenlive 0.7.2
As usual, you can download the source package from our [Sourceforge page][1], [binary package][2] will be announced when available.

## Screenshots:

![Rendering jobs](jobs.png)

 [1]: https://sourceforge.net:443/project/showfiles.php?group_id=46786
 [2]: https://web.archive.org/web/20160323125923/https://kdenlive.org/user-manual/downloading-and-installing-kdenlive/pre-compiled-packages
