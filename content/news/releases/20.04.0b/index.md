---
title: Maintenance release 20.04.0b
author: Farid Abdelnour
date: 2020-05-03T16:58:36+00:00
aliases:
- /en/2020/05/maintenance-release-20-04-0b/
- /fr/2020/05/version-de-maintenance-20-04-0b/
- /it/2020/05/versione-di-manutenzione-20-04-0b/
- /de/2020/05/wartungsrelease-20-04-0b/
---
Fast on the heels of the 20.04.0 release comes 20.04.0b. This fix corrects:

  * Several crash fixes
  * Broken timeline preview
  * Broken image sequences
  * Non working audio drag from monitor
  * Incorrect timecode in 23.98 fps
  * Broken archive feature
  * Compositions broken on insert audio track
  * Timeline autoscroll sometimes not working
  * Template tile duration reset on project opening
