---
title: Kdenlive 21.04.3 Windows release
author: Farid Abdelnour
date: 2021-07-16T11:50:25+00:00
aliases:
- /en/2021/07/kdenlive-21-04-3-windows-release/
---

Better late than never, Kdenlive 21.04.3 for Windows is out. There is also a hotfix for the AppImage version fixing missing Lumas.
