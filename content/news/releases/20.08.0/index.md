---
title: Kdenlive 20.08 is out
author: Farid Abdelnour
date: 2020-08-17T05:00:03+00:00
aliases:
- /en/2020/08/kdenlive-20-08-is-out/
- /it/2020/08/kdenlive-20-08-e-disponibile/
- /de/2020/08/kdenlive-20-08-ist-freigegeben/
---

Kdenlive 20.08 is out with nifty features like Interface Layouts, Multiple Audio Stream support, Cached data management and Zoombars in the Clip Monitor and Effects Panel but one may argue that the highlights of this release are stability and interface improvements. This version received a total of 284 commits with some major contributions from new developers. (Thanks Simon and Julius)

### Under the hood

A major refactoring of the project file fixes a long standing issue with the [decimal separator][1] (comma/point) conflict causing many crashes.

_**Projects created with 20.08 forward are not backwards compatible, that is, you won't be able to open your .kdenlive project files with older versions.**_

This version also brings a performance boost to audio thumbnail generation as well as JPG image sequence playback.

### New interface layouts

![Screenshot of Kdenlive's "logging" user interface layout](logging.jpg)
![Screenshot of Kdenlive's "editing" user interface layout](editing.jpg)
![Screenshot of Kdenlive's "audio" user interface layout](audio.jpg)
![Screenshot of Kdenlive's "effects" user interface layout](effects.jpg)
![Screenshot of Kdenlive's "color" user interface layout](color.jpg)


These workspaces aim to improve the layout for each stage of video production:

  * **Logging** for reviewing your footage
  * **Editing** to compose your story in the timeline
  * **Audio** for mixing and adjusting your audio
  * **Effects** for adding effects
  * **Color** for adjusting and color grading

Check out this [video][2] for more details.

_Note: Video scopes do not work with DirectX under Windows._

### Audio

This version brings the initial step in implementing an advanced audio workflow by adding **multiple audio stream support.** In the next releases expect [audio routing and channel mapping][3] as well. The mixer got a facelift making it more efficient in smaller heights.

![](multiple-audio.png)

![](mulitple-clip.png)

### Zoom bars

### Effects panel

Adjusting keyframes just got easier.  
![](zoombar-effects.gif)

#### Clip monitor

The Clip Monitor also received zoom bars. _Also notice the interface improvements like seeking when dragging, new layout ruler, improved overlay sizes (fps, timecode, etc)._

![](clip-monitor-zoombar.gif)

### Cache management 

A new cache management interface under settings allows you to maintain and control the size of your cached and proxied files as well as backup-ed data. You may also clean data older than a specified amount of months.

![](cache-manager.png)

### New shortcuts

  * **‘** (Apostrophe) to set audio stream to target track.
  * _**Shift + Alt**_ as alternate shortcut to move single clip to another track.
  * _**Alt + mouse**_ Windows specific shortcut to change a grouped clip’s track (Alt+mouse).
  * **_. + number_** to focus on Video tracks (i.e: **_._** **_+_** **_1_** focus on video track 1)
  * _**alt + number**_ to focus on audio tracks (i.e: **_alt + 2_** focus on audio track 2)
  * _**(**_ snaps beginning of clip to cursor in the timeline
  * _**)**_ snaps end of clip to cursor in the timeline

### Miscellaneous improvements

  * Project notes: allow creating markers from timestamps and assign timestamps to current bin clip.
  * Added option to always display clip monitor audio thumbs below video instead of an overlay.
  * Composite transitions with Lumas.
  * Add a "Save Copy" action to save a project copy.
  * Project bin improvements: Expand/collapse all bin folders with Shift+click, remember folder status (expanded/collapsed) on save, and many other fixes.
  * Add clip length adjustment to speed dialog.
  * Titler: add option to save title and add to project in in one pass (through the create button menu.
  * Add proxy icon to clips in timeline.
  * Increase monitor audio thumb resolution.
  * Ability to change colors of audio thumbnails (Go to Settings > Configure > Colors).
  * Renamed “Add Slideshow Clip” to “Add Image Sequence”.
  * Clickable clip name on top of Clip Properties widget opens a file explorer to the clip's location).
  * Windows: use compatible methods when dropping a folder in bin.

 [1]: https://invent.kde.org/multimedia/kdenlive/-/issues/78
 [2]: https://www.youtube.com/watch?v=BdHbUUjfBLk
 [3]: https://invent.kde.org/multimedia/kdenlive/-/issues/754
