  * Fix undo broken when trying to delete all tracks (don&#8217;t allow it and fix the underlying bug). [Commit.][5] Fixes bug [#462228][6]
  * Fix import keyframes broken. [Commit.][7] See bug [#456492][8]
  * Fix project duration incorrectly changed when changing speed of last clip in project. [Commit.][9]
  * Correctly stop archiving job on pressing abort. [Commit.][10] See issue [#999][11]
  * Animate track lock icon when trying to perform an operation on a locked track instead of silent failure. [Commit.][12]
  * Fix luma incorrectly flagged as missing on Windows. [Commit.][13] Fixes bug [#461849][14]
  * [Renderwidget] Fix &#8220;Edit Metadata&#8221; opens wrong page. [Commit.][15]
  * We do not necessarily need mediainfo to get the source timecode. [Commit.][16]
  * Fix crash when trying to launch second Glaxnimate instance with IPC. [Commit.][17]
  * Improve perfocmance of online resource search. [Commit.][18]
  * Better error message in some cases of preview render failure. [Commit.][19]
  * Fix incorrect loading of subtitle with two dots. [Commit.][20] Fixes bug [#461995][21]
  * Fix color picker on wayland. [Commit.][22] Fixes issue [#1417][23]
  * Port KMessageBox to twoActions where not violating string freeze. [Commit.][24]
  * Transcoding: use pcm instead of alac codec (fix possible audio artifacts). [Commit.][25]
  * Various fixes for spacer operation with subtitle track. [Commit.][26]
  * Fix image proxies not correctly applied after recovering proxy, don&#8217;t attempt to proxy small images. [Commit.][27]
  * Improve recovery for project files with missing proxy and source. [Commit.][28]
  * Clip properties: also show tooltip for image proxies. [Commit.][29]
  * Fix designer crash with Kdenlive widgets. [Commit.][30]
  * Disable parallel rendering for now (currently crashes because of an MLT regression). [Commit.][31]
  * Fix crash undoing timeremap change after unselecting the clip. [Commit.][32]
  * Fix recovering luma file paths when opening an Appimage project or from another computer. [Commit.][33]
  * Fix MaxOS compilation. [Commit.][34]
  * Ensure monitor zone out cannot go beyond clip out. [Commit.][35]
  * Switch to a proper JSon format to store guide categories instead of hacky line feed separated format. [Commit.][36]
  * Don&#8217;t attempt deleting the clip on aborting a thumbnail job. [Commit.][37]
  * Auto-call taskManager.taskDone() when run() ends. [Commit.][38]
  * Fix remove space. [Commit.][39] See issue [#1564][40]
  * Fix relocating files with proxies and image sequences. [Commit.][41]
  * Audio graph filters are keyframable now. [Commit.][42] Fixes bug [#459554][43]
  * Add IPC support for communication with Glaxnimate. [Commit.][44] Fixes issue [#1526][45]
  * Fix indentation (clang-format). [Commit.][46]
  * Show timeline instances for missing clips. [Commit.][47] Fixes issue [#1568][48]
  * Add zone-in/zone-out to contextual mouse menu for clip monitor(issue 1508). [Commit.][49]
  * Test ASS/SSA files containing commas. [Commit.][50] Fixes bug [#461486][51]
  * Handle commas in ASS subtitle files. [Commit.][52]
  * Be more clever finding resources paths when the project was relocated (for example opened from an external drive). [Commit.][53]
  * Fix crash on document open. [Commit.][54] See issue [#1571][55]
  * Fix possible crash on document open. [Commit.][56] See issue [#1571][55]
  * Revamp the keyframes copy/paste. We now have copy/paste icons in the effect stack toolbar that work as expected. Standard shortcuts also now work (Ctrl+C/V). [Commit.][57] Fixes issue [#1566][58]
  * Remember categories filter independantly for clip markers and timeline guides. [Commit.][59]
  * Better handling of seconds in import guides. [Commit.][60]
  * Add lock guide action in new guides list widget, with tooltip info. [Commit.][61]
  * Add test for insert space (WIP). [Commit.][62]
  * Fix guides not moving on insert blank. [Commit.][63]
  * Some improvements for guide export. [Commit.][64]
  * Add remove space test. [Commit.][65]
  * Make capitalization consistent for param name. [Commit.][66]
  * Fix small typo. [Commit.][67]
  * Fix possible settings dialog crash. [Commit.][68] Fixes issue [#1567][69]
  * Correctly show &#8220;auto&#8221; timeline preview profile if it was selected in settings. [Commit.][70]
  * Fix default timeline preview profile not correctly remembered. [Commit.][71]
  * Unbreak spacer/remove space. [Commit.][72] Fixes issue [#1564][40]
  * When deleting a category, allow reassigning its markers/guides to another category instead of deleting them. [Commit.][73]
  * Add action to focus guide search line, fine tune search: auto select first match, clear search line on esc/enter. [Commit.][74]
  * Add line break to messagebox. [Commit.][75]
  * Fix move guide and filtering clip monitor markers. [Commit.][76]
  * When opening a project saved with a previous Kdenlive version, ensure we recover the guides categories if missing. [Commit.][77]
  * Add new marker multi category selection button (MarkerCategoryButton), used in guides list. [Commit.][78]
  * Guides list: make it work with keyboard shortcuts, fix group deletion. [Commit.][79]
  * When deleting a guides category, also remove all clip markers using it. [Commit.][80]
  * Fix several small issues / crash in new guides widget. [Commit.][81]
  * Fix marker import, add button for default marker type. [Commit.][82]
  * Make guides list show clip markers when a bin clip is selected, allow editing several markers (only allows changing category). [Commit.][83]
  * Fix typo. [Commit.][84]
  * DEPENDENCY! Require at least MLT 7.8.0. [Commit.][85]
  * Remove code duplication (merge multiMarkerDialog with standard MarkerDialod). [Commit.][86]
  * Add filter line for guides, allow sorting them by category, timecode or comment, allow exporting JSON data. [Commit.][87]
  * Do not try to install non-existing file. [Commit.][88]
  * Fix possible UI corruption with xml uis and some animated parameters. [Commit.][89]
  * Remove xml ui for &#8220;region&#8221; effect, that does not exist anymore. [Commit.][90]
  * [Effects] Use animated rect instead of geometry, if possible. [Commit.][91]
  * [Rotate and Shear] Fix rect selection. [Commit.][92]
  * [Code Gardening] Entirely remove already unsupported (Asset) ParamTypes. [Commit.][93]
  * Better speration between animated an non-animated params in UI. [Commit.][94]
  * Fix incorrect model used for retrieving data. [Commit.][95]
  * Guide categories: use struct for better readability, move catagory deletion in markerlistmodel, add test. [Commit.][96]
  * Delete guides if their category is deleted. [Commit.][97]
  * Fix marker tests. [Commit.][98]
  * Guides update: allow managing categories, add new guides list widget, allow filtering categories. [Commit.][99]
  * Guide categories: use struct for better readability, move catagory deletion in markerlistmodel, add test. [Commit.][100]
  * Delete guides if their category is deleted. [Commit.][101]
  * Fix marker tests. [Commit.][102]
  * Guides update: allow managing categories, add new guides list widget, allow filtering categories. [Commit.][103]
  * Fix position and scale of monitor overlays on zoom. [Commit.][104]
  * Enable F2 shortcut to rename bin tags in Tag Widget. [Commit.][105]
  * [REUSE] Add license info for xml uis. [Commit.][106]
  * Add missing license header. [Commit.][107]
  * Make use of new MarkerCategoryChooser. [Commit.][108]
  * [Guides] Add new MarkerCategoryChooser widget. [Commit.][109]
  * CLeanup UI file. [Commit.][110]
  * Fix some spacer inconsistencies when used with groups, add some tests. [Commit.][111]
  * Fix click in timeline broken after switching to another app with spacer tool active. [Commit.][112]
  * [CI] Finally fix FreeBSD, pushed the wrong fix. [Commit.][113]
  * [CI] Enable CI failure on test failure for FreeBSD. [Commit.][114]
  * [CI Tests] Fix FreeBSD. [Commit.][115]
  * Drop getMainTimeline(), use getCurrentTimeline() instead. [Commit.][116]
  * [REUSE] One last try to get it correct for *.ui files. [Commit.][117]
  * [REUSE] Add (missing) license info for some files. [Commit.][118]
  * [REUSE] Use also &#8220;LicenseRef-KDE-Accepted-GPL&#8221; for *.ui files. [Commit.][119]
  * [REUSE] Add (missing) license headers for *.ui files. [Commit.][120]
  * Add 2 small track functions: remove all spaces after cursor and remove all clips after cursor, with test. [Commit.][121]
  * Loading a project with locked tracks should not insert an entry in undo history. [Commit.][122]
  * [MarkerDialog] Remember last selected category. [Commit.][123]
  * Fix compiler warning and warning in with QtDesigner plugin. [Commit.][124]
  * [Test] Show file and line number in debug output. [Commit.][125]
  * [CI: xmllint] Run for all xml files, but only if change affects any xml file. [Commit.][126]
  * [CI] Add xml linter. [Commit.][127]
  * [Effects] Fix some xml gui files. [Commit.][128]
  * Port away from deprecated KDeclarative::setupEngine() Pt.3. [Commit.][129]
  * Ensure files are open before reading to QDomDocument Pt. 2. [Commit.][130]
  * Dependency change! Require at least KDE Frameworks 5.92. [Commit.][131]
  * Disable cppcheck warnings for fakeit.hpp, catch.hpp and fakeit_standalone.hpp files. [Commit.][132]
  * Fix naming of 3D FFT Denoiser parameters. [Commit.][133] Fixes bug [#460003][134]
  * [AssetLists] Remove workaround for (fixed) Qt bug. [Commit.][135]
  * Fix layout warning. [Commit.][136]
  * Fix possible crash canceling archive. [Commit.][137]
  * Various fixes for playlist proxy: fix possible crash on error, rendering artifacts and incorrect length. [Commit.][138]
  * Fix build on KF<5.98. [Commit.][139]
  * [Qt6] Try to fix build. [Commit.][140]
  * Port away from deprecated KDeclarative::setupEngine() Pt.2. [Commit.][141]
  * Port away from deprecated KDeclarative::setupEngine(). [Commit.][142]
  * [Code Gardening] Remove unused code. [Commit.][143]
  * Port away from deprecated KNS methods, use QQC1 only for TreeView. [Commit.][144]
  * Ensure files are open before reading to QDomDocument. [Commit.][145]
  * [Tests] Use ecm\_add\_test and split to individual binaries. [Commit.][146]
  * [Qt6.4] Fix one more char* to QVariant issue. [Commit.][147]
  * Fix another char* to QVariant issue. [Commit.][148]
  * Fix possible issue with char* to QVariant conversion. [Commit.][149]
  * Dependency change! Make KFileMetaData required again. [Commit.][150]
  * Fix build broken by previous commit. [Commit.][151]
  * [CMake] Use ECM to install KDebugSettings config instead of custom file. [Commit.][152]
  * [Keyframes] Refactor code. [Commit.][153]
  * [Keyframe import/export] Fix roto and others in localized context. [Commit.][154]
  * Use TimecodeDisplay in *.ui files where possible. [Commit.][155]
  * [Qt Designer plugin] Fix include path. [Commit.][156]
  * [Code Gardening] More cleanup. [Commit.][157]
  * [Code Gardening] Remove more unused files. [Commit.][158]
  * Fix constructor should be explicit. [Commit.][159]
  * [Code Gardening] Remove more unused code. [Commit.][160]
  * Fix Qt Designer crashing with Kdenlive Widgets plugin. [Commit.][161]
  * [Wizard] Remove dead code. [Commit.][162]
  * Ensure resource providers are not listed twice. [Commit.][163] Fixes bug [#460060][164]
  * Testing if Qt6 tests are caused by qtblend. [Commit.][165]
  * Move xml out of translated string. [Commit.][166]
  * [Code Gardening] Remove unused code. [Commit.][167]
  * [Timecode Display] Reduce code duplication. [Commit.][168]
  * [dev-docs] Document recent changes. [Commit.][169]
  * Fix tests hanging if avfilter is missing on CI system (avfilter.fieldorder popped up a missing filter dialog). [Commit.][170]
  * Make timecode display listen to profile change and automatically adjust fps. [Commit.][171]
  * Typo: add line break in online resources autogenerated notes. [Commit.][172]
  * Remember effect keyframe status (show/hide). [Commit.][173] Fixes issue [#1538][174]
  * Reduce subtitle widget padding. [Commit.][175]
  * Fix tests. [Commit.][176]
  * [Kdenlive Settings] Move Monitor bg color to colors tab. [Commit.][177]
  * [Kdenlive Settings] Remove &#8220;preview volume&#8221; option. [Commit.][178]
  * [Code Gardening] Remove unused file. [Commit.][179]
  * Refactor color chooser code to reduce duplication. [Commit.][180]
  * Add Qt Designer plugin including TimecodeDisplay. [Commit.][181]
  * [Build System] Fix: feature_summary should be called last. [Commit.][182]
  * Attempt to solve subtitle Right to Left languages. [Commit.][183] See issue [#1519][184]
  * Try to fix Mac OS 10.13.x. [Commit.][185]
  * Seek to item last frame on paste. [Commit.][186]
  * Fix pasting clip from a project with different fps causes image freeze. [Commit.][187]
  * Remove unused include. [Commit.][188]
  * [Render widget] add clickable link to edit metadata, display them in tooltip. [Commit.][189] Fixes issue [#1523][190]
  * Stabilize: Focus stabilized clip when created if original clip was selected in bin. Allow replacing original in bin instead of creating an additional clip. [Commit.][191] Fixes issue [#1506][192]
  * Hide .kdenlive extension in title bar. [Commit.][193]
  * Fix effect overlay not properly scaling on monitor zoom. [Commit.][194]
  * [Compositions] Add xml uis to have allow translation. [Commit.][195] Fixes bug [#414939][196]
  * [dev-docs] Remove instruction about translations. [Commit.][197]
  * Fix minor typos. [Commit.][198]
  * [Code Gartening] Remove unused code. [Commit.][199]
  * [Kdenlive Settings] Remove unused ffmpegaudiothumbnails option. [Commit.][200]
  * [Kdenlive Settings] Remove unused usekuiserver option. [Commit.][201]
  * [Kdenlive Settings] Remove unused bypasscodeccheck option. [Commit.][202]
  * Fix merge messup. [Commit.][203]
  * [Kdenlive Settings] Improve layout and usability. [Commit.][204]
  * [Project Settings] Clean up. [Commit.][205]
  * Cleanup: improve code handling timeline preview selection. [Commit.][206]
  * Do not ignore po folder. [Commit.][207]
  * Reuse EncodingProfilesChooser in Project Settings to reduce code duplication, don&#8217;t allow selecting incompatible preview profile. [Commit.][208]
  * [Code Gardening] Improve readability of ClipThumbs.qml. [Commit.][209]
  * [Online Resources] Add provider for Pixabay Videos. [Commit.][210] Fixes bug [#435569][211]
  * [Online Resource Providers] Support object downloadUrls arrays. [Commit.][212]
  * [Resource Widget] Fix open licens and provider website. [Commit.][213]
  * Timeline preview: ensure we don&#8217;t insert chunks of the wrong size (would cause 1 on 2 chunks to fail insert), ensure the orange &#8220;working&#8221; chunks disappears on stop. [Commit.][214]
  * Fix thumbnails for loopable clips. [Commit.][215]
  * [Image Sequence] Fix wrong thumbnails. [Commit.][216]
  * [Image Sequences] Fix loop option. [Commit.][217] Fixes bug [#382432][218]
  * Fix tab order of all config pages as well as render widget. [Commit.][219] Fixes issue [#1536][220]
  * Fix possible profile corruption when switching to a never used profile. [Commit.][221] Fixes issue [#1320][222]
  * Fix crash closing proxy test dialog. [Commit.][223] Fixes issue [#1160][224]
  * Fix EncodingProfilesChooser layout. [Commit.][225]
  * Minor string fixes. [Commit.][226]
  * Fix(License): Update COPYING with original text. [Commit.][227]
  * [DocBook] Add URL to new version of the documentation. [Commit.][228]
  * Fix audio capture saved in wrong folder. [Commit.][229] Fixes issue [#1533][230]
  * Check every 2 weeks the size of cached data and warn user if it exceeds the defined limit. [Commit.][231]
  * Add a max cache size config setting so that we can check if the total cached data exceeds this limit and warn user. [Commit.][232]
  * Cache data: show warning when it exceeds 1Gb. [Commit.][233]
  * Manage cached data: add info tooltip to explain different types of cached data. Remove very small cache directories on cleanup. [Commit.][234]
  * Put metadata directly in XML from doc. [Commit.][235] Fixes bug [#458718][236]
  * Fix track audio level empty on pause. [Commit.][237]
  * Align master audio level with MLT&#8217;s audiolevel filter (use only the first 200 samples). [Commit.][238]
  * Don&#8217;t add unnecessary audio level filter on master. [Commit.][239]
  * Deprecate MLT&#8217;s old boxblur filter (replaced with new box_blur effect). [Commit.][240] Fixes issue [#932][241]
  * Partial fix for pasting to a document with a different fps (effect keyframes remain broken), display a warning. [Commit.][242] See issue [#1500][243]
  * Fix pasting effect with keyframes partially broken. [Commit.][244]
  * Correctly preselect timeline toolbar when editing it from context menu. [Commit.][245] Fixes issue [#1501][246]
  * Minor fix for updated MLT audiolevel filter (will fix track levels). [Commit.][247]
  * Fix effect stack view incorrect on hide keyframes (was still showing the timecode). [Commit.][248]
  * Display timeline color area for effects with only 1 keyframe, hide adjustable keyframe from timeline when there is only 1 keyframe. [Commit.][249] Fixes issue [#1522][250]
  * Fix ghost keyframes created when pasting an effect to a clip that has a crop start smaller than source clip and on clip speed resize. [Commit.][251] See issue [#1394][252]
  * Show a hamburger menu in the main toolbar if menu bar is hidden. [Commit.][253] Fixes bug [#358390][254]
  * Update and clean up QML imports. [Commit.][255]
  * Show message on bin item deletion failure. [Commit.][256] See bug [#459260][257]
  * Fix crash on bin clip deletion with instance on locked track. [Commit.][258] Fixes bug [#459260][257]
  * Add test for bin clip deletion with instance on locked track. [Commit.][259] See bug [#459260][257]
  * Fixed thumbnail cache not being rebuilt anymore in &#8220;Show video preview in thumbnails&#8221; mode. [Commit.][260]
  * Add option to disable countdown on audio record. [Commit.][261] Fixes issue [#1521][262]
  * Fix archiving project with no external files (color/title clips only). [Commit.][263]
  * Don&#8217;t update keyframe parameters when changing a keyframe selection state. [Commit.][264]
  * Add subtitle track type. [Commit.][265]
  * Fix tests crash. [Commit.][266]
  * Fix vp8 with alpha render crash. [Commit.][267]
  * Don&#8217;t delete audio tasks when switching profile. [Commit.][268]
  * Fix usage count column visible in bin. [Commit.][269]
  * Fix uninitialized var messing audio record and possible crash. [Commit.][270]
  * Fix sorting by date not working for newly inserted clips, other sorting issues. [Commit.][271] See bug [#458784][272]
  * Don&#8217;t mess rotation data on proxy transcoding. [Commit.][273] See issue [#1520][274]
  * Add pipewire audio driver to selection of SDL output. [Commit.][275]
  * Fix crash when clip is modified by external app. [Commit.][276]
  * Fix last commit breaking paste and tests. [Commit.][277]
  * Warn user if a paste operation is in progress. [Commit.][278]
  * Fix paste clip broken until close/repoen app if trying to paste an invalid clip (missing of playlist with different fps). [Commit.][279]
  * Don&#8217;t hardcode default track compositing (get ready for qtblend v3). [Commit.][280]
  * Fix clip start tooltip when there already is a mix. [Commit.][281]
  * Fix double clicking mixed clip start corrupting mix. [Commit.][282]
  * Fix incorrect mutex unlock in thumbs cache. [Commit.][283]
  * Ensure tasks are properly terminated on close, fix incorrect mutex in thumbnailcache causing corruption. [Commit.][284]
  * Simplify code for keyboard scheme download with KF 5.98. [Commit.][285]
  * [Qt6] Fix amiguous conversion. [Commit.][286]
  * Use KDEInstallDirs instead of KDEInstallDirs5. [Commit.][287]
  * Use static QRegularExpression. [Commit.][288]
  * Port from deprecated \*\_INSTALL\_DIR to KDE\_INSTALL\_\*. [Commit.][289]
  * [Qt6] Temporarily disable subtitle encoding test (TODO). [Commit.][290]
  * [Qt6] Fix version comparison. [Commit.][291]
  * [Qt6] Fix install paths. [Commit.][292]
  * [Qt6] Fix tests build. [Commit.][293]
  * [Qt6] Disable deprecated QQuickStyle checking. [Commit.][294]
  * [Qt6] Disable deprecated KNewStuff code (TODO: port). [Commit.][295]
  * [Qt6] Try to port OpenGL related code (not working yet). [Commit.][296]
  * [Qt6] Port deprecated itemDelegate() to itemDelegateForIndex(). [Commit.][297]
  * [Qt6] Port two more missing + to | for QKeySequence. [Commit.][298]
  * [Qt6] Port deprecated globalPos() to globalPosition(). [Commit.][299]
  * [Qt6] Port changed functions. [Commit.][300]
  * [Qt6] Fix QFont::Weight enum. [Commit.][301]
  * [Qt6] Port one missed event->y(). [Commit.][302]
  * Fix &#8220;invalid conversion from ‘char’ to ‘const char*’&#8221; (Qt6). [Commit.][303]
  * [Qt6] Try to fix. [Commit.][304]
  * [Qt6] Disable more parts of Mediacapture for Qt6 (TODO: port). [Commit.][305]
  * [Qt6] Fix parameter type. [Commit.][306]
  * [Qt6] Disable parts of Mediacapture for Qt6 (TODO: port). [Commit.][307]
  * Port QRegExp to QRegularExpression. [Commit.][308]
  * [Qt6] Fix syntax. [Commit.][309]
  * Disable already unused video capture code (needs fixing). [Commit.][310]
  * [Qt6] Port QAbstractItemView::itemDelegate to itemDelegateForIndex. [Commit.][311]
  * Port remaining + to | for QKeySequences. [Commit.][312]
  * [Qt6] Port QTextStream::setCodec to QTextStream::setEncoding. [Commit.][313]
  * [Qt6] Use QMultiMapIterator for QMultiMap iteration. [Commit.][314]
  * Fix ambiguous number -> string conversion. [Commit.][315]
  * [Qt6] Port away from deprecated QMouseEvent::x() and QMouseEvent::y(). [Commit.][316]
  * Fix QLocal::decimalPoint() returns QString in Qt6. [Commit.][317]
  * [Qt6] Port some deprecated QVariant functions. [Commit.][318]
  * Port deprecated QVariant::canConvert. [Commit.][319]
  * Dummy port away from QAudioRecorder. [Commit.][320]
  * Use versionless targets if building against Qt6. [Commit.][321]
  * Add Qt6 CI. [Commit.][322]
  * Remove duplicate headers between cpp/h. [Commit.][323]
  * Ensure queued tasks are not started on project or test close. [Commit.][324]
  * Add description to save stack effect dialog. [Commit.][325] Fixes issue [#582][326]
  * Don&#8217;t remove consecutive spaces in SRT subtitles. [Commit.][327] Fixes bug [#457878][328]
  * Fix two untranslatable UI strings. [Commit.][329]
  * [Export Guides] Fix layout. [Commit.][330]
  * Use QColorUtils instead of custom function, fix qml warnings. [Commit.][331]
  * Port away from deprecated KIO code. [Commit.][332]
  * Remove unused includes. [Commit.][333]
  * Fix several issues with QText clips. [Commit.][334]
  * Fix archiving when a clip is added twice in a project. [Commit.][335]
  * [REUSE] Add (missing) license headers for test code. [Commit.][336]
  * Add test loading broken subtitle file. [Commit.][337]
  * Remove unused include. [Commit.][338]
  * Remove wrong commit disabling subtitles test. [Commit.][339]
  * File loading: better separate model from ui to make testing easier. [Commit.][340]
  * Fix tests. [Commit.][341]
  * Ensure mix can easily be resized until clip end. [Commit.][342]
  * Fix project corruption on resize mix start. [Commit.][343] See issue [#1499][344]
  * Fix loading projects with corrupted mix (remove mix and broken clip). Related to #1499. [Commit.][345]
  * Fix incorrect profile comparison. [Commit.][346]
  * In project settings > delete unused files, don&#8217;t show color clips as url, don&#8217;t propose deleting missing files. [Commit.][347]
  * Fix error and corruption loading reverted mixes. [Commit.][348]
  * Port deprecated method. [Commit.][349]
  * Fix ambiguous widget name. [Commit.][350]
  * Fix compilation with KF5 5.86. [Commit.][351]
  * Update kdenliveeffectscategory.rc adding typewriter to Stylize. [Commit.][352]
  * Update typewriter.xml restoring type to text and updating the name of the effect. [Commit.][353]
  * Added xml UI for the avfilter CMakeLists.txt. [Commit.][354]
  * Added xml UI for the avfilter.shear. [Commit.][355]
  * Added xml UI for the avfilter.scroll. [Commit.][356]
  * Added xml UI for the avfilter.photosensitivity. [Commit.][357]
  * Added xml UI for the avfilter.monochrome. [Commit.][358]
  * Added xml UI for the avfilter.median. [Commit.][359]
  * Added xml UI for the avfilter.kirsch. [Commit.][360]
  * Added xml UI for the avfilter.exposure. [Commit.][361]
  * Added xml UI for the avfilter.epx. [Commit.][362]
  * Added xml UI for the avfilter.colortemperature. [Commit.][363]
  * Added xml UI for the avfilter.colorize. [Commit.][364]
  * Added xml UI for the avfilter.colorcorrect. [Commit.][365]
  * Added xml UI for the avfilter.colorcontrast. [Commit.][366]
  * Added xml UI for the avfilter.chromanr. [Commit.][367]
  * Added xml UI for the avfilter.cas. [Commit.][368]
  * Added xml UI for the avfilter.bilateral. [Commit.][369]
  * Update kdenliveeffectscategory.rc. [Commit.][370]
  * Updated blacklisted_effects.txt. [Commit.][371]
  * Updated CMakeLists.txt for frei0r effects. [Commit.][372]
  * Added xml interface for the frei0r\_bigsh0t\_eq\_to\_stereo. [Commit.][373]
  * Update typewriter effect make it hidden. [Commit.][374]
  * Fix cannot drag clip from monitor. [Commit.][375]
  * Render profiles: make text parameters read-only, and add a button to switch to text edit only as mixing manual editing with presets is unreliable. [Commit.][376] See issue [#1441][377]
  * KMessageBox::detailedSorry is deprecated in kf5.96. [Commit.][378]
  * It&#8217;s UTF-8 by default in qt6. [Commit.][379]
  * Add test for regression issue #1494. [Commit.][380]
  * Fix cast to double moved outside division. [Commit.][381]
  * Fix wrong use of useSourceProfile. [Commit.][382]
  * KMessageBox::sorry is deprecated in kf5.97. [Commit.][383]
  * Simplify confusing logic in ThumbnailCache::getKey(). [Commit.][384]
  * Fix spelling in render preset dialog. [Commit.][385]
  * WarningContinueCancel -> error. [Commit.][386]
  * Try to fix project profile corruption. [Commit.][387] See issue [#1494][388]
  * Fix possible crash on profile switch, based on a contribution from Ivan Sudakov. [Commit.][389] See issue [#1320][222]
  * Add tooltip for dragging audio/video component to another track (Meta). [Commit.][390]
  * Fix filtering TreeItem lists by non-ASCII strings. [Commit.][391] Fixes bug [#432699][392]
  * Add test for non-ascii list filtering (bug 432699). [Commit.][393]
  * Test histogram handling RGB/BGR. [Commit.][394]
  * Use QImage::pixel() in rgbparadegenerator.cpp. [Commit.][395]
  * Use QImage::pixel() in waveform. [Commit.][396]
  * Test waveform RGB/BGR handling. [Commit.][397]
  * Change vectorscope to use QImage::pixel(). [Commit.][398] Fixes bug [#453149][399]
  * Test vectorscope switching red and blue. [Commit.][400]
  * Fix extract frame for playlist clips. [Commit.][401]
  * Fix document folder incorrectly set on loading project with &#8220;Use parent folder as project folder&#8221;. [Commit.][402] See issue [#1492][403]
  * Extract frame: process in another frame so we don&#8217;t block the UI, make sure effects are applied. [Commit.][404] See issue [#1491][405]
  * Render last frame. [Commit.][406]
  * Better phrasing for project load error. [Commit.][407]
  * Export guides: add info button listing possible keywords. [Commit.][408]
  * Guess subtitle encoding before importing. [Commit.][409] Fixes bug [#456871][410]
  * Make monitor zone out be last frame number. [Commit.][411]
  * Fix Insert Zone to Bin out point off by 1. [Commit.][412] Fixes bug [#455883][413]
  * Itemize Flatpak instructions in \`README.md\`. [Commit.][414]
  * Get rid of KOpenWithDialog to select default external applications (doesn not work on Windows/Mac), make path to glaxnimate configurable. [Commit.][415]
  * Don&#8217;t wait for a clip to be ready to get its type. [Commit.][416] Fixes bug [#456619][417]
  
 [5]: http://commits.kde.org/kdenlive/6d0b7533b28e356a9a62875ee0e56b63cb0bc9c7
 [6]: https://bugs.kde.org/462228
 [7]: http://commits.kde.org/kdenlive/8647249503bcebf68be7d752e809a61e148795c4
 [8]: https://bugs.kde.org/456492
 [9]: http://commits.kde.org/kdenlive/395ebda459da53a655a2708fc024bd5189b9cff7
 [10]: http://commits.kde.org/kdenlive/272290c743cd0134591808cc68a9d6ce4ffba4b3
 [11]: https://invent.kde.org/multimedia/kdenlive/-/issues/999
 [12]: http://commits.kde.org/kdenlive/23f8c5c8a5059cd3bcd9ef3b962ba7586fba5fdf
 [13]: http://commits.kde.org/kdenlive/ce465115def5143b629c079d987c7b8ac4b6b4a4
 [14]: https://bugs.kde.org/461849
 [15]: http://commits.kde.org/kdenlive/a88c178dccfa8dc6a42276587ceb1bbe583c93ff
 [16]: http://commits.kde.org/kdenlive/487dc0645c503cf2af200456215149ebd3c546a6
 [17]: http://commits.kde.org/kdenlive/8021e16e8bc98e25f0a0f87d12b4d22b5fe91825
 [18]: http://commits.kde.org/kdenlive/e6a1cc154295b5d6cc5533ad761872ac296dcef4
 [19]: http://commits.kde.org/kdenlive/db528d58fbea80e4f46558fafcf427cf8e18c2c2
 [20]: http://commits.kde.org/kdenlive/45042d1a6e101b6148e43865ad28922b1e0a9ac1
 [21]: https://bugs.kde.org/461995
 [22]: http://commits.kde.org/kdenlive/b93e0b21539c29d724dfb9346bacfde75c71e2b7
 [23]: https://invent.kde.org/multimedia/kdenlive/-/issues/1417
 [24]: http://commits.kde.org/kdenlive/25edf681abf839125d17b6d78c5a5b3fbd2efd8a
 [25]: http://commits.kde.org/kdenlive/fcb24a4cf57e230efaa6ab328a64df48090b7389
 [26]: http://commits.kde.org/kdenlive/81d3e6610c9998f56dbbba80a37807fa7bd5b6c2
 [27]: http://commits.kde.org/kdenlive/323ec2c2040543a230b4ffc95d6915751f580c50
 [28]: http://commits.kde.org/kdenlive/bed54470e309c3238938738873869530ae3f0b60
 [29]: http://commits.kde.org/kdenlive/b3bc1403b6d2f7a6a6f2be29d9717cf2f8453547
 [30]: http://commits.kde.org/kdenlive/6d6cd00715e8f0f65b6aedbf83535d3194d8c665
 [31]: http://commits.kde.org/kdenlive/5b5fdfbc10d764c382f2335f214593f0f3d46baf
 [32]: http://commits.kde.org/kdenlive/1eac6b03ec0d86f6a74fa10a127174c74c2f7cd5
 [33]: http://commits.kde.org/kdenlive/42bea5dfa9b4f19eb1ffe03e8a66159359e9e7a5
 [34]: http://commits.kde.org/kdenlive/892c88c5373755ca90cfb9a7956ae7eb8efb3a10
 [35]: http://commits.kde.org/kdenlive/3ff4f881eb7f9f76e3ddbfeda7d01d81f2ffe6ad
 [36]: http://commits.kde.org/kdenlive/85cf5974849770bc9b5a14174fc225cab346360f
 [37]: http://commits.kde.org/kdenlive/da8d2d8b574fc3ed1446b4e4b63e0f432f0830e5
 [38]: http://commits.kde.org/kdenlive/fced1a0bda4cccfc16dc77234f836d7b4e16f2e9
 [39]: http://commits.kde.org/kdenlive/5de71c28e78688dd2de11707c5728a4fa65252f6
 [40]: https://invent.kde.org/multimedia/kdenlive/-/issues/1564
 [41]: http://commits.kde.org/kdenlive/deacdde4c1944de1a9a559ea7374a8fde6e5be2e
 [42]: http://commits.kde.org/kdenlive/c34cbacf8a0ad87112294e448fdf20b8e19f6244
 [43]: https://bugs.kde.org/459554
 [44]: http://commits.kde.org/kdenlive/ba1d3dec3947d9b271543b938aeeda150d3f36b9
 [45]: https://invent.kde.org/multimedia/kdenlive/-/issues/1526
 [46]: http://commits.kde.org/kdenlive/55de627a5b217d8a3e78a1068041045363a06281
 [47]: http://commits.kde.org/kdenlive/ebf6737be0f52b6b343589158b1b156eed2ffeb4
 [48]: https://invent.kde.org/multimedia/kdenlive/-/issues/1568
 [49]: http://commits.kde.org/kdenlive/601e5177eadb09a4db427f6edfe07c0859cf2251
 [50]: http://commits.kde.org/kdenlive/fa73c4a5ea9b4ea7306e8edf9fb7a7520cda051b
 [51]: https://bugs.kde.org/461486
 [52]: http://commits.kde.org/kdenlive/07744f3a61496d4a97633aa81f64a7dc9589cb3e
 [53]: http://commits.kde.org/kdenlive/705827d3c00ed02c2f31facdd36236979e8655b3
 [54]: http://commits.kde.org/kdenlive/9fee60cd658870cdc587c669c33ebae36d91f457
 [55]: https://invent.kde.org/multimedia/kdenlive/-/issues/1571
 [56]: http://commits.kde.org/kdenlive/bdd6a8dce186e19cfc838a199654f119cfe2e655
 [57]: http://commits.kde.org/kdenlive/b097aa30560356ae4a4889d95061c6efb276afac
 [58]: https://invent.kde.org/multimedia/kdenlive/-/issues/1566
 [59]: http://commits.kde.org/kdenlive/c96d8bfa41541ae371e212ed2703b519640ba016
 [60]: http://commits.kde.org/kdenlive/269ff0c57d0ac2a5e2e6c4a388ba6707b2605ca1
 [61]: http://commits.kde.org/kdenlive/bfcc5f91d205dddb88cc457bdeb15b54590ece1a
 [62]: http://commits.kde.org/kdenlive/685c75ce18005ba27ad78f19c464171950a0a310
 [63]: http://commits.kde.org/kdenlive/07b8f53b6aa8780f6245c8647d4605b8c270620f
 [64]: http://commits.kde.org/kdenlive/6eb51ae5c952261e956fc21815a1b2dc9731c772
 [65]: http://commits.kde.org/kdenlive/002d1fbd63f82aa009cda5cd63b599af972d5879
 [66]: http://commits.kde.org/kdenlive/91e56380ba2ed2c6c7272a3e197a809ee117ac5f
 [67]: http://commits.kde.org/kdenlive/a0d069d3328df94ea35a070c1a151cf60eb60e9e
 [68]: http://commits.kde.org/kdenlive/69fae3a309040a7a39fa60ee650e59582587c166
 [69]: https://invent.kde.org/multimedia/kdenlive/-/issues/1567
 [70]: http://commits.kde.org/kdenlive/b9c69e42b443ccbc0550d35ffdd3f51df01f4e8f
 [71]: http://commits.kde.org/kdenlive/649d3efb9f2214d015b0676a3ec139c3e8d09ecf
 [72]: http://commits.kde.org/kdenlive/27a2a7b6c880bc69378171576e290c8c56b33c5c
 [73]: http://commits.kde.org/kdenlive/a3730b5693d97ffb118cf5694d4d2f0761ff8714
 [74]: http://commits.kde.org/kdenlive/c33efca73f7ddd4e17fa2232436fe4f9887fe57a
 [75]: http://commits.kde.org/kdenlive/3516c3e47b9320aab726428520bdc3a6b7686aac
 [76]: http://commits.kde.org/kdenlive/e1e4a9bc87a33a5d436ac63216897218decf30fa
 [77]: http://commits.kde.org/kdenlive/8c45212ea274a7490a5f9c58042af90cd1936525
 [78]: http://commits.kde.org/kdenlive/35a4d73f78b595e31c59c875de995e980b973861
 [79]: http://commits.kde.org/kdenlive/a799905671a3527061871930b61de18db3650827
 [80]: http://commits.kde.org/kdenlive/2cb43162b3cbd3e0ffeb7e37846d0ad0a9c18989
 [81]: http://commits.kde.org/kdenlive/6c1ac094c1f7d8bc09f9b4d6745c61ddbdeb4d6d
 [82]: http://commits.kde.org/kdenlive/66216267bdd1981e486b96715f18350793c5e6af
 [83]: http://commits.kde.org/kdenlive/4c2f6bd500c47ed0a49dfdf385e8452d6b48e439
 [84]: http://commits.kde.org/kdenlive/300b71ae4c75e33e791dd1d1437b3520efc0cb3b
 [85]: http://commits.kde.org/kdenlive/21013476a17269d3e821d433a738172a5b4dfb8e
 [86]: http://commits.kde.org/kdenlive/60304ef73f1bf81cb862db38023995d49be60b6a
 [87]: http://commits.kde.org/kdenlive/52ac5f043c53db624e5921f46f0344c3de705a68
 [88]: http://commits.kde.org/kdenlive/7b85f9ecd7ae2be836f2f8c3708b46f335e95ef7
 [89]: http://commits.kde.org/kdenlive/fe94f9af75fc30b6f650b276537fa97dcfd5c52c
 [90]: http://commits.kde.org/kdenlive/9251afebf9145b8cc20cfdf55008df673a21f1b0
 [91]: http://commits.kde.org/kdenlive/814c7fd8c15b9ecda853a69a30d82b3b28f47586
 [92]: http://commits.kde.org/kdenlive/170e2030abd772de1ab362ee74f9c95663a8715b
 [93]: http://commits.kde.org/kdenlive/8872d5b4b1b08527deb06d13510ae77a035cbfa9
 [94]: http://commits.kde.org/kdenlive/b644d24738e4cd07379a884edb53ac506107d045
 [95]: http://commits.kde.org/kdenlive/4db4d42fa1aa4529d9a4b18868ffef9069a47b83
 [96]: http://commits.kde.org/kdenlive/ef2116bab31ee9a7a050c92e101d71e243c33470
 [97]: http://commits.kde.org/kdenlive/5be2288f0b90ad137affce6beb1981f51a699a1d
 [98]: http://commits.kde.org/kdenlive/87aa85e95254d9b75f9b537245396ea01e286c7e
 [99]: http://commits.kde.org/kdenlive/4bbc5bc0285c0f49331b57271d42ae840acf1c81
 [100]: http://commits.kde.org/kdenlive/fb8a1c1152d81d2a161776b9634407bfc9d728d0
 [101]: http://commits.kde.org/kdenlive/ab75eda27dc7f0455ef3adb08c68e4703a47bad6
 [102]: http://commits.kde.org/kdenlive/89f5d00a31ba65e38aa152999c66a80bcf37ace5
 [103]: http://commits.kde.org/kdenlive/62d26a506a788e86d354c7b32c88d27d690083b1
 [104]: http://commits.kde.org/kdenlive/7f0c5599f48c230bcb9e2edbdbb5a5d3d3cd7b1d
 [105]: http://commits.kde.org/kdenlive/d5345c47762841295bbc7cd427f28857e4af30fd
 [106]: http://commits.kde.org/kdenlive/9c681304f357a2c27d05a43f1a8791145ce962fd
 [107]: http://commits.kde.org/kdenlive/1ba5910716079c7d131b2617d30772e5980810af
 [108]: http://commits.kde.org/kdenlive/33440916be71ecbe568b5e6d266b4ae82e081bb1
 [109]: http://commits.kde.org/kdenlive/8e2dcd46d258738fe23106e0ec13aed26457b9de
 [110]: http://commits.kde.org/kdenlive/ce0b48e6a144ee94535b1a6ba092c2d047cd225d
 [111]: http://commits.kde.org/kdenlive/2dad6ae4bf8b2e098a11067b408bd30fc3f608ea
 [112]: http://commits.kde.org/kdenlive/407478c97a1411bca9009284ae2be8bd37bfef16
 [113]: http://commits.kde.org/kdenlive/87b4c1f69735172c3b3fce3d4fb1ef89fe68ba3d
 [114]: http://commits.kde.org/kdenlive/d5ce8429e7b263c635ff91216d4433f7624a4664
 [115]: http://commits.kde.org/kdenlive/d63e3b96ad773d0c4f4730a710b900ea249385b9
 [116]: http://commits.kde.org/kdenlive/cbaf84dedb4371edba11b9eae7d62dd3bfe874b7
 [117]: http://commits.kde.org/kdenlive/df004bad9bed7130a6899615bb5cfb4213f60d93
 [118]: http://commits.kde.org/kdenlive/8702ed9e70c9c86b0baa7ace2bff1e9dab95c0ec
 [119]: http://commits.kde.org/kdenlive/fd637a09cca94289837a392647f4e2be1f66d6e1
 [120]: http://commits.kde.org/kdenlive/f241c3a8da614836c0e6f6a23410769d723c0ff2
 [121]: http://commits.kde.org/kdenlive/8fa31397fbea94064957740cf56b4d5c4383f19d
 [122]: http://commits.kde.org/kdenlive/4b22fb81b35ce7bf81fb31fe48ef62c184e548e2
 [123]: http://commits.kde.org/kdenlive/37b5c938cd9286cfc4ed6b7ccae0f9e5f2c0ad00
 [124]: http://commits.kde.org/kdenlive/db369de91a8ee10966eb0254710bb94ff49e1e58
 [125]: http://commits.kde.org/kdenlive/7fa5c827dbf882b37a1c007cc3850addb8d880bc
 [126]: http://commits.kde.org/kdenlive/c893066e44bf6583bc10e8954826603dcc32d654
 [127]: http://commits.kde.org/kdenlive/a4548cf9b2cc9a806d4cccf1859d5251726fbc0b
 [128]: http://commits.kde.org/kdenlive/ef6ffe25cc5b5cfdac5f00e78fef39deff147791
 [129]: http://commits.kde.org/kdenlive/45b43c392625a8bf03cb1cd12c1b79ffd34cfd63
 [130]: http://commits.kde.org/kdenlive/f2083aee682f44dc415590c74ff15adf92e5480b
 [131]: http://commits.kde.org/kdenlive/0a33db3904a454d1f6e5701a38accdd1a4053fdf
 [132]: http://commits.kde.org/kdenlive/4ef157ec914c7ba53b7bdd8b79e9b4716cd36d2c
 [133]: http://commits.kde.org/kdenlive/efc36da6827f3dc3a3bedc645c4959084eac81aa
 [134]: https://bugs.kde.org/460003
 [135]: http://commits.kde.org/kdenlive/0865f2f921a016f3baf934cfb16aab4044555b2e
 [136]: http://commits.kde.org/kdenlive/01c21a578eacaa0c08ed10fa1a80f2fdfe7fca93
 [137]: http://commits.kde.org/kdenlive/d8ea44419e3db88c82fd26d93583014ef70c6b29
 [138]: http://commits.kde.org/kdenlive/0d85cef660af9ad86e61fe6b4517d2f2328f6af0
 [139]: http://commits.kde.org/kdenlive/1891a4dc39996634846d402f442c752b1ce9e6c8
 [140]: http://commits.kde.org/kdenlive/eecef0dbe52374d0833003d97ce6939eae6bd428
 [141]: http://commits.kde.org/kdenlive/f50b0b8b2f65b0a8c484f277004b3bd23b1ba23b
 [142]: http://commits.kde.org/kdenlive/721213563f72a2dd75eb4d43737a94be46dcacbf
 [143]: http://commits.kde.org/kdenlive/ba067e108d82985c414bf2fe5d49ace9491a6c75
 [144]: http://commits.kde.org/kdenlive/899951e9d4f06bb42c72c2cba37a9a04bae1e654
 [145]: http://commits.kde.org/kdenlive/469c2c03cdf20ada2e3a51f13319d98f65cc5ef5
 [146]: http://commits.kde.org/kdenlive/e16fe038ca6ab8d5fb0e5e16d6acefd515a831cd
 [147]: http://commits.kde.org/kdenlive/721ad3e6118f0a72911196652f99a80f4935b44a
 [148]: http://commits.kde.org/kdenlive/357ebcdc469015cf96674f3587ebd0ee56804b2f
 [149]: http://commits.kde.org/kdenlive/bf9524e6539946258c07387336dbba3630b83e6b
 [150]: http://commits.kde.org/kdenlive/e253ef55c47e38ac565e3c1fdc66fbcba9b3f5db
 [151]: http://commits.kde.org/kdenlive/ed387d0e17aceb22afcd2733a41d7115de06cf58
 [152]: http://commits.kde.org/kdenlive/1e1c37d9db239dfd3545240e0f389d109a04c48e
 [153]: http://commits.kde.org/kdenlive/d14f03f8d05b736a5f508a2c4a2ea43c5d8406d8
 [154]: http://commits.kde.org/kdenlive/a5b757e71fa97f5dec63efc050a40e2f2a7308cc
 [155]: http://commits.kde.org/kdenlive/59d76cdc8b68a3f417105c2f914414612a4cfd3a
 [156]: http://commits.kde.org/kdenlive/bbcc2771c74a6b31a1cce4f2e69c2c0f665a8302
 [157]: http://commits.kde.org/kdenlive/ec3e3d70890dc4c1c5ff33282b72ee0dd15c512d
 [158]: http://commits.kde.org/kdenlive/06d7f751451e22925dae1a8d73eb1388d4624d7f
 [159]: http://commits.kde.org/kdenlive/bc01ae9df708f378cb9b5565942c7be2f3726571
 [160]: http://commits.kde.org/kdenlive/2dcb568e97df0007496be10406e84ca83eb46598
 [161]: http://commits.kde.org/kdenlive/49242eecd2f7ce33fcfbb1e17cdf2540b9292b96
 [162]: http://commits.kde.org/kdenlive/59a90ede7243eeaad6f5a21a135e85b6ca48a0a1
 [163]: http://commits.kde.org/kdenlive/0e56e3818dc385e6d9ce151a765481fe96d07e44
 [164]: https://bugs.kde.org/460060
 [165]: http://commits.kde.org/kdenlive/6d90a180a2c5e6dfcb12139dff6e1595a696c90c
 [166]: http://commits.kde.org/kdenlive/e2dbcee459893653f32c57b740c1cf249bcab2bb
 [167]: http://commits.kde.org/kdenlive/10fef207c33afbee1996c7e5b83edd97c545c60d
 [168]: http://commits.kde.org/kdenlive/0bbc19c53b55b4a1d1499a6df0249a99c1cace4a
 [169]: http://commits.kde.org/kdenlive/245a91f326c3a51b7af1cd878be89d53c4df6108
 [170]: http://commits.kde.org/kdenlive/12e46dd414dbd8ebdc2a081b2f73493dc9f1dbc3
 [171]: http://commits.kde.org/kdenlive/e1ee97890e46901a5b331674e84cd2cb74429543
 [172]: http://commits.kde.org/kdenlive/b444e9e4f0c7ec1dd5be2b3f76073d2c5392fff8
 [173]: http://commits.kde.org/kdenlive/02201a4a57299f9dad8ade95676331725ab06d9e
 [174]: https://invent.kde.org/multimedia/kdenlive/-/issues/1538
 [175]: http://commits.kde.org/kdenlive/2e3a0873f3e9addb66bcf7d1136a3ad5e6ade663
 [176]: http://commits.kde.org/kdenlive/978fb8e5deb5856456002ef6b37eff6c85ffae78
 [177]: http://commits.kde.org/kdenlive/9a5ebb1237c71a94be50e09df9e7bd6a05e5b678
 [178]: http://commits.kde.org/kdenlive/b997d2429432bb665b4584860e92eb7064392107
 [179]: http://commits.kde.org/kdenlive/bc3198ef28149c398258af28710c74e8adefc7f7
 [180]: http://commits.kde.org/kdenlive/f6790b6cbd72fff89b2e428401d94ef248879e60
 [181]: http://commits.kde.org/kdenlive/2b26ce0d755397d9672f437cee8a65541def1c6c
 [182]: http://commits.kde.org/kdenlive/4360d6fc70f924fb7e08e0fca65efcb56fe2d4a2
 [183]: http://commits.kde.org/kdenlive/a93d20931aa2ec3435d330cfca683b695a23ae3b
 [184]: https://invent.kde.org/multimedia/kdenlive/-/issues/1519
 [185]: http://commits.kde.org/kdenlive/7dce38f2b7362629e102471c50a52caa7be798c9
 [186]: http://commits.kde.org/kdenlive/cf14e2237874cb67da3d7d0f3e4c2b8f721338c8
 [187]: http://commits.kde.org/kdenlive/44cae0f815c1cd57ace5db11dc359c6ad805c501
 [188]: http://commits.kde.org/kdenlive/00878dbeec81f9025d8cc7d61e6ced7533c82285
 [189]: http://commits.kde.org/kdenlive/01b125f564323e0173d237c15059355a7d0a5901
 [190]: https://invent.kde.org/multimedia/kdenlive/-/issues/1523
 [191]: http://commits.kde.org/kdenlive/8a1baef3ff7d21bd10cfb4cdc50cf49f6635edc4
 [192]: https://invent.kde.org/multimedia/kdenlive/-/issues/1506
 [193]: http://commits.kde.org/kdenlive/308568bf426cce6cf08c42982b2fb5ce1faa13b0
 [194]: http://commits.kde.org/kdenlive/f081e2cd5f412a370c55273b8caa8cff34413234
 [195]: http://commits.kde.org/kdenlive/f58fcc2fe09cb0a974b226ca6145adacd4fcd017
 [196]: https://bugs.kde.org/414939
 [197]: http://commits.kde.org/kdenlive/f7e08b968670227eb7f46cdcacab2e56e1735ef4
 [198]: http://commits.kde.org/kdenlive/140cb8bea63e5918533f68db9868eabf8ac7d9f2
 [199]: http://commits.kde.org/kdenlive/3f81e537df034aae61ba6692f2d302188ce593d0
 [200]: http://commits.kde.org/kdenlive/ee912b13b36c3be4a6c5ed7e095c0afdd0cadba7
 [201]: http://commits.kde.org/kdenlive/1d247eceacb60c35ba97187c52ab6ee2db32315d
 [202]: http://commits.kde.org/kdenlive/1841d7139898e4ecaa1f267df194bdba86038364
 [203]: http://commits.kde.org/kdenlive/c5b79ee89d70ff468b8388e2fc4b2a27529e3976
 [204]: http://commits.kde.org/kdenlive/b260fb7cf58cb983d05c18c56033f2042e7d53cd
 [205]: http://commits.kde.org/kdenlive/0a63da415f8dbc08010b00799ecbf67292b15c0b
 [206]: http://commits.kde.org/kdenlive/0ca9208a702ff22183a40124f65d8e011064489c
 [207]: http://commits.kde.org/kdenlive/cf17110cd4367603c1fbc7b02193ba80204d5ec2
 [208]: http://commits.kde.org/kdenlive/8720afb49fb2902dd14cac00b1aa5e1d02aea6d8
 [209]: http://commits.kde.org/kdenlive/79636b1ef0916b8995e11d7511eb6639fc5bc821
 [210]: http://commits.kde.org/kdenlive/55f7578b89af807a8e231845122adc64307a82a3
 [211]: https://bugs.kde.org/435569
 [212]: http://commits.kde.org/kdenlive/9c1bebf320121216680114cf7d299b1955591645
 [213]: http://commits.kde.org/kdenlive/6a8b0984cee050df06dda72546ab72623802fd80
 [214]: http://commits.kde.org/kdenlive/a3987accd57520bac8e507319963fbc3944c7cf4
 [215]: http://commits.kde.org/kdenlive/dfee6c18ab930bb6e02d296108e014bd60d31bc1
 [216]: http://commits.kde.org/kdenlive/b5efba984d464827c9887235fc9db57719879ecb
 [217]: http://commits.kde.org/kdenlive/affc9a74d9e5e73160529cb619abea1dc606872d
 [218]: https://bugs.kde.org/382432
 [219]: http://commits.kde.org/kdenlive/a65e316c06c3fb2a39db9120f6a7f3fd99d5eaf8
 [220]: https://invent.kde.org/multimedia/kdenlive/-/issues/1536
 [221]: http://commits.kde.org/kdenlive/75806ee2b24a1959877bdd76675681e478571597
 [222]: https://invent.kde.org/multimedia/kdenlive/-/issues/1320
 [223]: http://commits.kde.org/kdenlive/7941aa675299ed9c5d47b40dcb6b210aec48f6f5
 [224]: https://invent.kde.org/multimedia/kdenlive/-/issues/1160
 [225]: http://commits.kde.org/kdenlive/4bff2b20d229fb06b06b51aeeb6a70bb20f09e10
 [226]: http://commits.kde.org/kdenlive/70b9f512f50e70c942a3a9cecaefd63e48a83907
 [227]: http://commits.kde.org/kdenlive/f58feb68cf26f5ab9f8da16a53f56e4506aea0d3
 [228]: http://commits.kde.org/kdenlive/3096142fa3766cd33a430beef1a7303a4739552c
 [229]: http://commits.kde.org/kdenlive/5f22b2c3dca51f2c8996590a1093d0371e392cd9
 [230]: https://invent.kde.org/multimedia/kdenlive/-/issues/1533
 [231]: http://commits.kde.org/kdenlive/d42d98e2bf2adde26d892517a075110a3e4fb423
 [232]: http://commits.kde.org/kdenlive/b7d9ce97d9c7de6afab83887406772f666ecfe9b
 [233]: http://commits.kde.org/kdenlive/afcb0a6ac75372744b8996f2881abefb2f961cf4
 [234]: http://commits.kde.org/kdenlive/a6c29d955c216c84215ef4c78b8b8137893f905f
 [235]: http://commits.kde.org/kdenlive/a1304676a61959783884f90473fd2306a6ffd835
 [236]: https://bugs.kde.org/458718
 [237]: http://commits.kde.org/kdenlive/35814d86a26e236a543cdcfc325adde9ab9a8cbd
 [238]: http://commits.kde.org/kdenlive/e19ec6c5f2a3f96005322052037a48d022b491b1
 [239]: http://commits.kde.org/kdenlive/d1bdc451231a8371195853e22b6fcaebc928afee
 [240]: http://commits.kde.org/kdenlive/cc45f251c8c61f4bdf784d56d765624fdcc80ba5
 [241]: https://invent.kde.org/multimedia/kdenlive/-/issues/932
 [242]: http://commits.kde.org/kdenlive/eb957889cbe1015a339f58d3745a27cd7e0802e8
 [243]: https://invent.kde.org/multimedia/kdenlive/-/issues/1500
 [244]: http://commits.kde.org/kdenlive/64362c30a14aafae9733d1e3086ba0581d30f61a
 [245]: http://commits.kde.org/kdenlive/a77cecae2909c1fab815cc19e66e8e71b62446b3
 [246]: https://invent.kde.org/multimedia/kdenlive/-/issues/1501
 [247]: http://commits.kde.org/kdenlive/3ba202fbbd7fd9a4118772468cc9d49e7e31794e
 [248]: http://commits.kde.org/kdenlive/f142cade9336840131914ba05386d60886a686eb
 [249]: http://commits.kde.org/kdenlive/828283da8088c481723b089bfe674ef0774d0929
 [250]: https://invent.kde.org/multimedia/kdenlive/-/issues/1522
 [251]: http://commits.kde.org/kdenlive/c314557086d864b1558bdf3bc4a0df857ecb832a
 [252]: https://invent.kde.org/multimedia/kdenlive/-/issues/1394
 [253]: http://commits.kde.org/kdenlive/cb38d57691cfba311c0e59fd748d88a5b8690ba8
 [254]: https://bugs.kde.org/358390
 [255]: http://commits.kde.org/kdenlive/64811ea4c0086ad4372d1559d30825e335bc124f
 [256]: http://commits.kde.org/kdenlive/707628042cb53e9745f96d64550d578e23de2e1d
 [257]: https://bugs.kde.org/459260
 [258]: http://commits.kde.org/kdenlive/ac4795e727eeba7ce899e1268433dda7d8281ccd
 [259]: http://commits.kde.org/kdenlive/a5351b850ae7cb9ef5af9c548a961a9a01c8041e
 [260]: http://commits.kde.org/kdenlive/8b8687d898b09b47d523f071ca723285cede8758
 [261]: http://commits.kde.org/kdenlive/3153e6d8714b12a57239cb08cdfab106b20d5a69
 [262]: https://invent.kde.org/multimedia/kdenlive/-/issues/1521
 [263]: http://commits.kde.org/kdenlive/81e4196b221109506fb79e255ac11b0828ddbf3b
 [264]: http://commits.kde.org/kdenlive/482c87566f212256732f83bc2ea779a101850b74
 [265]: http://commits.kde.org/kdenlive/e0bee1abfbbef9ba13e0da8d1ed1c043b14a6d7b
 [266]: http://commits.kde.org/kdenlive/f9a8774c1614fa71e6380cc0ebd7ec096e91d652
 [267]: http://commits.kde.org/kdenlive/8a655527e89a364fb0cd283172afac26ccc8c32f
 [268]: http://commits.kde.org/kdenlive/0648e01b04cfa63cfee0c3a166e1e81b11668874
 [269]: http://commits.kde.org/kdenlive/22be9ed0fe0fd599354da6827792444bc2ade8f0
 [270]: http://commits.kde.org/kdenlive/da7ec1de516842bb8ec0558ae8a53bcc3ea47000
 [271]: http://commits.kde.org/kdenlive/3593888f17c7dd7c2c37e2e1b0c6f55e8f1f1fa5
 [272]: https://bugs.kde.org/458784
 [273]: http://commits.kde.org/kdenlive/a6cf702758612e6e55a6afb47d899d8dd32efa75
 [274]: https://invent.kde.org/multimedia/kdenlive/-/issues/1520
 [275]: http://commits.kde.org/kdenlive/a7a7232ae8b10caded459bdb9e7c12331557064f
 [276]: http://commits.kde.org/kdenlive/d42c36e52ad27781f1eb7c2e4b2d0c60943ffacc
 [277]: http://commits.kde.org/kdenlive/3239b0bfcae467a55217e2b695805a1ebcbc8dcf
 [278]: http://commits.kde.org/kdenlive/174b4d3f507f1e8092984b84c669770ac574620b
 [279]: http://commits.kde.org/kdenlive/72443b80dcd7dfca8a20159003aab48bcc9ccc15
 [280]: http://commits.kde.org/kdenlive/5cbc6403de27b466326e1746f6ab7d190e872f97
 [281]: http://commits.kde.org/kdenlive/ea07c60874394824d4b841bb36de257b28e60b17
 [282]: http://commits.kde.org/kdenlive/48a72bc308ee49afa1c0ab569b3b55b68884926e
 [283]: http://commits.kde.org/kdenlive/25ba2ac5c2143b10c8c3eee145d9f2d84bef6ece
 [284]: http://commits.kde.org/kdenlive/61cba5e24a36a9fc7eb1bab0ff360a22ab0140a7
 [285]: http://commits.kde.org/kdenlive/c761c6047dad97035f3dcededeb01db0617242ca
 [286]: http://commits.kde.org/kdenlive/d9512b9af9123607617b9a512dd17b3f9085cf55
 [287]: http://commits.kde.org/kdenlive/18930385313ca6e475ae288d718f463ef28cd7b0
 [288]: http://commits.kde.org/kdenlive/50255f3c79d46fcbf20b7a70163827a44670bc58
 [289]: http://commits.kde.org/kdenlive/dbdff1d096e053fc99a2c1c1b247c91f08001f18
 [290]: http://commits.kde.org/kdenlive/7e5b66b6edbbb3a5822dcc7e4b466f2ed089577c
 [291]: http://commits.kde.org/kdenlive/313101f5ab09000a1bf3251bc0e03f75e313cd1a
 [292]: http://commits.kde.org/kdenlive/ccb8b3324c3e5757396e5d846d76c270df7a1c3f
 [293]: http://commits.kde.org/kdenlive/b758fb00074d16f3ada18495a81b803e75d05d0c
 [294]: http://commits.kde.org/kdenlive/b44b86813cd1ee71f8bf73e07b47e27c8abde31d
 [295]: http://commits.kde.org/kdenlive/a16f805fce07cafd1f06654a2824584b6546ed51
 [296]: http://commits.kde.org/kdenlive/9d080e0abba192ed237d71190cbb12a19a910985
 [297]: http://commits.kde.org/kdenlive/85602f9519de472479c2f9fc24c6879be4701fe9
 [298]: http://commits.kde.org/kdenlive/cf5eeee67ada1e5e26432bd9a8db0a8e7e42a354
 [299]: http://commits.kde.org/kdenlive/74a290ab0aa82ab218175aa4ede16de73b62f31b
 [300]: http://commits.kde.org/kdenlive/06aee5ff0d8a5b61cbf7d2a6684a86886149cb20
 [301]: http://commits.kde.org/kdenlive/992cf79cadafee2a01399da6ea017beda0db4909
 [302]: http://commits.kde.org/kdenlive/471508bd645b84486ff5f4ed2eeaadb33d9e7d1d
 [303]: http://commits.kde.org/kdenlive/c21df9c2dbd7f47ff34eb4d03b8734fa0490dd27
 [304]: http://commits.kde.org/kdenlive/1dccb551e93bd81ea54f2a7edb1611a07e829ae3
 [305]: http://commits.kde.org/kdenlive/2529261a57696417bb7e2c998b5592c554c43456
 [306]: http://commits.kde.org/kdenlive/fe8532082ef9a6d56326916b349b3b20a6f33d09
 [307]: http://commits.kde.org/kdenlive/72e4e684de4c14a516379c37e6a60707a01486de
 [308]: http://commits.kde.org/kdenlive/8804f8078a93207e9e8adfdd10b8f74e2822b0ff
 [309]: http://commits.kde.org/kdenlive/3b67b27b3ef1f747702ce68268f7a64349ce5e49
 [310]: http://commits.kde.org/kdenlive/61a831717200619355805887d106702baa8418cd
 [311]: http://commits.kde.org/kdenlive/e1617c8d60f2af21542d4def8993a4bd6879ba9c
 [312]: http://commits.kde.org/kdenlive/37d7fe25753c4dbfb01fde2d63ff8702dcb3dc55
 [313]: http://commits.kde.org/kdenlive/1fc508e4789c2ae04ce0549a0b49496e9156b863
 [314]: http://commits.kde.org/kdenlive/7f93818e94131b5554556dcee410a6689fc39927
 [315]: http://commits.kde.org/kdenlive/7c3d36ad8db5d801d4e34a7bc405307da84ad0de
 [316]: http://commits.kde.org/kdenlive/e7d46636589fffbaa4a495167e3e4998d7ae41ad
 [317]: http://commits.kde.org/kdenlive/f5c6a65750962e15172044924199e182659e6ed0
 [318]: http://commits.kde.org/kdenlive/a2ecf104b0b44462dbd5f3c60707f963ca8c019a
 [319]: http://commits.kde.org/kdenlive/842958f21cfb69645ac738a7fa1b40f23b1cc94a
 [320]: http://commits.kde.org/kdenlive/5fd38e5e9088c76611b1982c22efdc15051912bd
 [321]: http://commits.kde.org/kdenlive/2248027f3c74bcf0893baf89a0ee5b526d7b05b7
 [322]: http://commits.kde.org/kdenlive/cfb0455f44e4fa6111ec0947eb2e907399871ea0
 [323]: http://commits.kde.org/kdenlive/d31f22cd9f56610ec03f147a40c995839a958567
 [324]: http://commits.kde.org/kdenlive/8a902fd49e69fea05d94f9bc52c74c6a1d5d8caa
 [325]: http://commits.kde.org/kdenlive/12d78e99ff7bd4cc6ec295b191f4318ef9ebeab0
 [326]: https://invent.kde.org/multimedia/kdenlive/-/issues/582
 [327]: http://commits.kde.org/kdenlive/61c735cd77b191440533a967511bb14749e16017
 [328]: https://bugs.kde.org/457878
 [329]: http://commits.kde.org/kdenlive/3d7c851e323f086700df2767e97cf165271b33b5
 [330]: http://commits.kde.org/kdenlive/394f4aa249625101b2783711756cbb8217a8ef96
 [331]: http://commits.kde.org/kdenlive/2754efad94724c580485596f4b64ccddc403464c
 [332]: http://commits.kde.org/kdenlive/3afeffb08f4335fe22a415170d83e8cf2225dac0
 [333]: http://commits.kde.org/kdenlive/40ef0f8a518f1bd5f5ec7e9032cb22e9a7f0939f
 [334]: http://commits.kde.org/kdenlive/afd262c29d1b59e0da37bc66fcb43cbced14da74
 [335]: http://commits.kde.org/kdenlive/6200464051ee84230b37eaf77daa39450d144a6d
 [336]: http://commits.kde.org/kdenlive/1489d1154159f8e8bf3f3cdc10cea167eb694e31
 [337]: http://commits.kde.org/kdenlive/74ef83e070c1c75c8a13c065046a6c78def562c7
 [338]: http://commits.kde.org/kdenlive/aaa8c773786a0ed46fd8cf8c731eac1bd66fa1b2
 [339]: http://commits.kde.org/kdenlive/881f0f2a69c8a6cec8f8bafe5f10d519105a7dcf
 [340]: http://commits.kde.org/kdenlive/5c8ec91fee84cbb6e1e2a703bc9e6294fa83eede
 [341]: http://commits.kde.org/kdenlive/81ee8a0e144fc264d538f38ffdc1711d5f9feb38
 [342]: http://commits.kde.org/kdenlive/3b2bd76b4b21c28dd78f340465ee2d5308d6e757
 [343]: http://commits.kde.org/kdenlive/10b7b54d7c3d7133d5906680b83d5caffb7e0679
 [344]: https://invent.kde.org/multimedia/kdenlive/-/issues/1499
 [345]: http://commits.kde.org/kdenlive/b75772bb7918e76804658a2fe05bb8762d590272
 [346]: http://commits.kde.org/kdenlive/0bae63225df6ef682e6eba5dae42a91d819965e5
 [347]: http://commits.kde.org/kdenlive/a673c5485676463976ea4dcff8862ee2c4a04fe2
 [348]: http://commits.kde.org/kdenlive/ea5f546507362fd05ff7ae712fe5732ec6c8551d
 [349]: http://commits.kde.org/kdenlive/b018382fe017899c210d28329a7552080028c2a9
 [350]: http://commits.kde.org/kdenlive/0891d2c4f0ad844a0de773f792adec4cc44e34b9
 [351]: http://commits.kde.org/kdenlive/5f958b3555dd455232a4a6226c3a347c502c24fd
 [352]: http://commits.kde.org/kdenlive/fff370e0bf526e03428325b71686b394edddda4f
 [353]: http://commits.kde.org/kdenlive/8ed8ae5b627254c94675eae730ce01ac7d24d78b
 [354]: http://commits.kde.org/kdenlive/51a809210f105076311efc759eb39d14cc25b287
 [355]: http://commits.kde.org/kdenlive/eba4508ea66f6b1231a338c5b47c09e800aeba74
 [356]: http://commits.kde.org/kdenlive/3a33414080451940827152604df5725f34c9cbe5
 [357]: http://commits.kde.org/kdenlive/fe2b1986843ae2e7a57966446dc14d77f8ac9277
 [358]: http://commits.kde.org/kdenlive/bcc479fb4822db89f5b4eda18e65a9943ea70613
 [359]: http://commits.kde.org/kdenlive/d58867992b4397f0a9c4164556409f79fd25e051
 [360]: http://commits.kde.org/kdenlive/c28f7a53e0331eafea4e3cb1b07de5307d0c2a0d
 [361]: http://commits.kde.org/kdenlive/bc1f6359d37ba5cbb214795579984d6a8e44310e
 [362]: http://commits.kde.org/kdenlive/4d13df6fb06a6f1c06cbd0d999b4d765136d3a71
 [363]: http://commits.kde.org/kdenlive/d6dc6c60427bbb64f319b98ad6e1f3ec982595dc
 [364]: http://commits.kde.org/kdenlive/a89312ab7d91d3bf5a30f48aee59326411d2a027
 [365]: http://commits.kde.org/kdenlive/3b4fb1a58e4d9de24b2b6632249ee3b67e94581c
 [366]: http://commits.kde.org/kdenlive/fd660aa22560d420239d17db583626e989cc2cde
 [367]: http://commits.kde.org/kdenlive/e3d7397b04b58198501c15df2d5279a592b889b1
 [368]: http://commits.kde.org/kdenlive/168662fd6503121062dcda0c3cbcb7302a7576c7
 [369]: http://commits.kde.org/kdenlive/b010f5b811b53f4c26cb6315aa14d5e45bb125a6
 [370]: http://commits.kde.org/kdenlive/18294c0a5cf8fd6eb0a07376edb0be804cc693f2
 [371]: http://commits.kde.org/kdenlive/a6a6d52ddaa03300669a722eb283cc3e0473a7a4
 [372]: http://commits.kde.org/kdenlive/584dfa25fa76fa5ec7858b3a4713efa389e136f4
 [373]: http://commits.kde.org/kdenlive/06a4f94214aa18a5e854aa496d1e7c489b22c338
 [374]: http://commits.kde.org/kdenlive/40ab0fc48b4dccff92dbadece457670ab891c2a5
 [375]: http://commits.kde.org/kdenlive/50d55c2dead216a066cd80e2189735ed6a675b54
 [376]: http://commits.kde.org/kdenlive/244e8b4a738ed68cde0f126304716d6912337931
 [377]: https://invent.kde.org/multimedia/kdenlive/-/issues/1441
 [378]: http://commits.kde.org/kdenlive/0146b39daa4e246d55b9c67708f648ee0f6a0708
 [379]: http://commits.kde.org/kdenlive/8ced8f54ae79e17ab89f6c384dbcbdbcd8be5996
 [380]: http://commits.kde.org/kdenlive/382ec1a0c14fc031b063836345999fc51945771a
 [381]: http://commits.kde.org/kdenlive/6146c1b856e279e009f047b5a5c37c52a285dedf
 [382]: http://commits.kde.org/kdenlive/f4aeb5efdc1b63ec4813642dcb72497cda6320aa
 [383]: http://commits.kde.org/kdenlive/7df11708281c1c6a3d745112042a7a18939f3302
 [384]: http://commits.kde.org/kdenlive/b80f7a0d3585b0d482571d426bbfba8ded9f478e
 [385]: http://commits.kde.org/kdenlive/e5309de68d9e7a539db6d3b0b034c8ef9b9385cb
 [386]: http://commits.kde.org/kdenlive/4db96355939e5c8bdd267fab2951f98b73d8571d
 [387]: http://commits.kde.org/kdenlive/0172979c6fc6d9c19ef28c558fbefb20f4dfb04c
 [388]: https://invent.kde.org/multimedia/kdenlive/-/issues/1494
 [389]: http://commits.kde.org/kdenlive/46ccf3052c0424b419ee601ee5004b5c90a6d945
 [390]: http://commits.kde.org/kdenlive/ef1dc06f927dc8d80126ec5bdd018ad6f3e0992d
 [391]: http://commits.kde.org/kdenlive/d7a8c463f60e9b5fe757c9af4acf9ebbe672c530
 [392]: https://bugs.kde.org/432699
 [393]: http://commits.kde.org/kdenlive/ebc087ade315ebf52a37767712a6aaf6681a7a79
 [394]: http://commits.kde.org/kdenlive/3a146b81382c54f50f62d0098b0037aae3ced656
 [395]: http://commits.kde.org/kdenlive/4157005eb0daa1e04f3d1f6fbb60122f12f2e7ed
 [396]: http://commits.kde.org/kdenlive/286e5897a3f61460570ed66d0d42fdad725719ae
 [397]: http://commits.kde.org/kdenlive/60a2bc2a95c4774a3f09895781a0ad6a052aa172
 [398]: http://commits.kde.org/kdenlive/3f8105070aeedcf09adba716f79639fc63f72f96
 [399]: https://bugs.kde.org/453149
 [400]: http://commits.kde.org/kdenlive/51fbacb25481759ff01be4c634ddedf4892cbf29
 [401]: http://commits.kde.org/kdenlive/4265a4416953f3158e52dcb9ab2c76c6bdb27c7d
 [402]: http://commits.kde.org/kdenlive/02086acf31aecdeb22c3e71731140227b61d480b
 [403]: https://invent.kde.org/multimedia/kdenlive/-/issues/1492
 [404]: http://commits.kde.org/kdenlive/a538fcd9ad8ab5726f55076f18719e00cc3ead44
 [405]: https://invent.kde.org/multimedia/kdenlive/-/issues/1491
 [406]: http://commits.kde.org/kdenlive/c779cbe8694837194d04030b09ccf8269b373181
 [407]: http://commits.kde.org/kdenlive/d5ed0a48ed639b04203520e832b134c536edd86f
 [408]: http://commits.kde.org/kdenlive/593b864ae7f780a6db4a39f13151b1303e7c1b66
 [409]: http://commits.kde.org/kdenlive/6880a0d21222706fb0b942d3a36e4ebb56696673
 [410]: https://bugs.kde.org/456871
 [411]: http://commits.kde.org/kdenlive/d622fd72d7b8ec1603db7b00de609d5737a9d98c
 [412]: http://commits.kde.org/kdenlive/28a919ac7fcabf0d831614f7aae00d3321316bf1
 [413]: https://bugs.kde.org/455883
 [414]: http://commits.kde.org/kdenlive/7c92ad4d77334ddcfaa883452ad7587287125d86
 [415]: http://commits.kde.org/kdenlive/f50249c9e299ca5c69b9ff9226cfe122a2ea5d34
 [416]: http://commits.kde.org/kdenlive/505983e529c80ba5c8a13dd0a02ff949f7530873
 [417]: https://bugs.kde.org/456619
