---
author: Julius Künzel
date: 2022-12-12T17:06:59+00:00
aliases:
- /en/2022/12/kdenlive-22-12-released/
- /fr/2022/12/version-22-12/
- /it/2022/12/kdenlive-22-12-2/
- /de/2022/12/kdenlive-22-12/
featured_image_bg: images/features/2212.png
---
The Kdenlive team is happy to announce the release of version 22.12. This development cycle comes with more than 350 commits, bringing new features, bug fixes and preparing the code base for exciting changes that we expect in the near future.

## Guides and Markers

Kdenlive always has had support for markers and guides to help you organize your work for a long time. With this new release, the whole guide/marker system has received a major overhaul to help you better organize your project.

All marker (clips) and guide (timeline) features can now be found in the new "Guides" dock. Its behavior is similar to the one of the Effect Stack, as the content displayed depends on the selection: if you select a bin clip, the clip's markers will be shown, if you click in the timeline, guides will be shown. Hence we removed the "Markers" tab in the clip properties, as it is not necessary anymore.

The main advantage of the new "Guides" dock is the ability to easily  seek, search, sort and filter eg. by category or text. It also nicely integrates with keyboard navigation, so you can easily seek and find a timeline guide without moving your mouse.  

{{< video src-webm="guides1.webm" muted="true" autoplay="true" loop="true" controls="false">}}

Another major change in this areas ia that now it is possible to manage categories ie. to have more (or less) than the formerly available 9 categories by using  custom colors and names.

Further more it is now possible to edit, add or remove multiple markers at once and the import/export of markers has been improved.

![](guides-export.png)

![](Guide-Categories.png)

![](guides-add-multiple.png)

## Effects

In the latest version of Kdenlive the following audio graph filters are keyframeable:

  * audio level visualization filter
  * audio spectrum filter
  * audio wave form filter

Several other effects that were broken due to syntax errors in xml code have been fixed, and we added automated tests to the build system to avoid regressions caused by such syntax errors in the future.

Copy/paste of Keyframes was a bit unclear until now, so we have now adopted a common copy/past behavior you already now from other software, including CTRL+C, CTRL+V shortcuts. To make it even more clear, additional buttons have been added to the effect keyframe bar.

![](keyframe_toolbar_copy-paste.png)

## Glaxnimate Integration

While the integration of Glaxnimate with Kdenlive was already a thing in the last release, we have now pushed things to the next level!

If you use a compatible version of Glaxnimate (version >= 0.5.1), Kdenlive now sends the content of the timeline to Glaxnimate which then shows it as background. This is similar to the "Show Background option in the title tool and makes it much easier to create animations that play together with your videos.

You can learn how this works from the [documentation][1].

![](glaxnimate_with_background-e1670707255426.png)

## User Interface

Two small but useful and requested features were added to quickly remove space or clips in your timeline: remove spaces after playhead and remove all clips after playhead

{{< video src-webm="remove-space.webm" muted="true" autoplay="true" loop="true" controls="false">}}

### Cache Limit

You can now define a maximum size for the cached data stored by Kdenlive in the environment settings. Cached data consists of your projects audio and video thumbnails, but also backup copies of your projects, proxy clips, etc. Kdenlive will now check every 2 weeks if the total cached data exceeds this limit and if so warns you and proposes to delete older cache data.

### Hamburger Menu

If you do not use the menu bar much and would like to save some space, there is good news:  you can hide the menu bar, the menu will now be available through a hamburger menu in the toolbar.

![](hamburger-menu.png)

### "What's this?"

In several places we have added "What's this?" text. If you hover the cursor over an element like a button, you will often see a tool tip with a short text about that element. This is not new, but in this version the tool tip, in some cases, now also says "Press Shift for more". This displays a longer text with a more detailed explanation. We are going to increase the number of places where such a "What's this?" text is available during the next releases.

![](whatsthis-1.png)

![](whatsthis-2.png)

### Settings Cleanup

The settings pages got a visual cleanup. Unused and useless options have been removed, others have been reordered to be easier to find and clearer in their aim. We also made similar options in the project and application settings more consistent and added a hint to make the difference between those settings more clear.

{{< img-comparison-slider before="settings-before.png" after="settings-after.png" >}}

## Under the Hood

Beside the visible features already mentioned, the team worked a lot on cleaning up the code base to improve maintainability as well as preparing it for changes we expect in the near future such as nested timelines.

### Qt6 and KDE Frameworks 6

Also technically Kdenlive can now be built against Qt6 and a CI has been added to ensure this does not regress. This is the first step to ensure the transition from version 5 to version 6 of Qt and KDE Frameworks will be smooth. However, this does not mean the version built against Qt6 will work as expected at this moment. There is still some work that needs to get done. We do not have a definite ETA for the switch to Qt6 yet, but we currently expect it happen in the second half of 2023.

### Improved Track composition

Some long due fixes were also made to MLT's qtblend transition. This used to be the default transition for track compositing in Kdenlive. However, due to some bugs causing unwanted scaling in some situations, we defaulted to another transition for Kdenlive since 21.12.0. This caused some performance regressions in timeline playback and rendering. This is now all fixed and some additional optimizations also improve performance. All of this is available in the recent MLT 7.12.0. You can ensure qtblend is used from Kdenlive's Misc Settings (under preferred track compositing).

## Other Noteworthy Changes

  * Online Resources: New Pixabay Video provider and improved performance for search.
  * Audio capturing: Add setting to disable countdown.
  * Audio: Add Pipewire as SDL output, fix several issues with audio level on the mixer widget.
  * Fixed color picker on Wayland.
  * Fix several situations where clips from bin or monitor could not be dropped to timeline.
  * Improved logic for finding relocated files.
  * [UnB contribution] Add description to save stack effect dialog.
  * Lots of fixes for subtitle import

[Got to download page][5]

As usual, if you encounter any issues please [let us know][2]. You may also contact us on our [Telegram][3] and [Matrix][4] channels.  

You make Kdenlive possible!

[Donate now!][6]

 [1]: https://docs.kdenlive.org/en/cutting_and_assembling/editing.html#edit-an-animation
 [2]: /bug-reports/
 [3]: https://t.me/kdenlive
 [4]: https://matrix.to/#/#kdenlive:kde.org
 [5]: /download
 [6]: /fund
 
