---
title: Kdenlive 19.12.2
author: Farid Abdelnour
date: 2020-02-07T00:16:18+00:00
aliases:
- /en/2020/02/kdenlive-19-12-2/
- /it/2020/02/kdenlive-19-12-2-2/
- /de/2020/02/kdenlive-19-12-2-3/
---

The second minor release of the 19.12 series is out with Qt 5.14 compatibility, Project Bin ability to sort subclips in chronological order, crash fixes and interface enhancements.
