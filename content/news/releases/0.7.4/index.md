---
author: Jean-Baptiste Mardelle
date: 2009-05-30T12:07:00+00:00
aliases:
- /users/j-b-m/kdenlive-074-released
- /discover/0.7.4
# Ported from https://web.archive.org/web/20160319045101/https://kdenlive.org/users/j-b-m/kdenlive-074-released
---

Kdenlive 0.7.4 is now released.

## Main changes

* Compatibility with the new 0.4.x versions of MLT
* Rendering jobs now start one after another instead of all together
* Rewritten DVD wizard, allowing for chapters, several buttons in menu and easy preview
* Start of a transcoding feature, allowing to easily convert a clip in another format
* New icon for Kdenlive by Alberto Villa
* Lots of bug fixes ([see full list][1]) and small other improvements

## Participate

With this new release, we encourage all users to participate to the project by using our different services: bug tracker, forum, content sharing, etc.

We also want to launch a new icon contest. Several users expressed the fact that a film strip as icon is a bit outdated for a digital video application, so we would like to open a contest, more on that soon.

We also would like to ask for help in the translation department, so if you feel like giving some help to translate Kdenlive, please contact us.

## Get Kdenlive

You can download Kdenlive source code from our [Sourceforge page][2], and see [the compilation instructions][3]. As usual, binary packages should be available soon from your distribution, check our [binary packages][4] for infos.

Kdenlive uses the [MLT framework][5] for all video processing. **You need MLT >= 0.4.0**.

Thanks to everyone who contributed to that release by testing, giving feedback and providing patches.

Jean-Baptiste Mardelle


  [1]: https://web.archive.org/web/20160319045101/http://www.kdenlive.org/mantis/search.php?project_id=1&sticky_issues=on&fixed_in_version=0.7.4&sortby=last_updated&dir=DESC&hide_status_id=-2
  [2]: http://sourceforge.net/project/platformdownload.php?group_id=46786
  [3]: https://web.archive.org/web/20160319045101/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive/installing-source
  [4]: https://web.archive.org/web/20160319045101/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive/pre-compiled-packages
  [5]: https://www.mltframework.org/
