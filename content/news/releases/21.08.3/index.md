---
author: Farid Abdelnour
date: 2021-11-08T17:50:38+00:00
aliases:
- /en/2021/11/kdenlive-21-08-3-released/
- /fr/2021/11/kdenlive-21-08-03/
- /it/2021/11/kdenlive-21-08-3/
---
The last maintenance release of the 21.08 series is out fixing many same track transition issues. Other noteworthy improvements include loop zones don't stop playback when adding effects, added ability to set clip thumbnails when hover seeking clips in the Project Bin and proxies can now be automatically generated for `.mlt` files.



