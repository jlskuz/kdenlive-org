  * Update catch.hpp. [Commit.][1] See bug [#440867][2]
  * Timeline clip drop: add id to each drag operation to avoid incorrectly interpreting a new drag operation as the continuation of a previous one. [Commit.][3]
  * Fix muting audio master broken. [Commit.][4]
  * Fix various mix resize/align issues. [Commit.][5]
  * Fix proxy clips not archived when requested. [Commit.][6]
  * Fix wipe and slide transition incorrect behavior on resize, and incorrectly detecting “reverse” state. [Commit.][7]
  * Fix same track transition if one clip has no frame at its end. [Commit.][8]
  * Fix crash and incorrect resize with same track transitions. [Commit.][9]
  * Fix mix cut position lost on paste. [Commit.][10]
  * Fix one cause of crash related to multiple keyframes move. [Commit.][11]
  * Fix proxying of playlist clips. [Commit.][12]
  * When a clip job creates an mlt playlist, check if the file is already in project to avoid double insertion. [Commit.][13]
  * Fix clip with mix cannot be moved back in place. [Commit.][14]
  * Fix loop mode broken on add effect. [Commit.][15]
  * Fix replacing AV clip with playlist clip broken. [Commit.][16]
  * Fix export frame broken for title clips. [Commit.][17]
  * Fix bin thumbnail hover seek not reset when leaving thumb area. [Commit.][18]
  * Project bin:when hover seek is enabled, restore thumb after seeking, set thumb with shift+seek. [Commit.][19]
  * Fix “adjustcenter” asset param in case where the frame size is empty. [Commit.][20]
  * Fix crash loading project with incorrectly detected same track transition. [Commit.][21]
  * Fix install path of frei0r effect UI’s. [Commit.][22]

 [1]: http://commits.kde.org/kdenlive/adc86cfe0f2df232106530e5ceef576a93a856b4
 [2]: https://bugs.kde.org/440867
 [3]: http://commits.kde.org/kdenlive/a70cccc9508b996c5122ccd15db2ef5a49c25700
 [4]: http://commits.kde.org/kdenlive/abfea9bbd158675dd89c553561b34c989d5e917f
 [5]: http://commits.kde.org/kdenlive/d9234c1cc931c27a2c7e99793293a99b6cb96a15
 [6]: http://commits.kde.org/kdenlive/783204cc14e0887afae90f3a623e88cf92fd59d2
 [7]: http://commits.kde.org/kdenlive/231b29cd20a83bf8df182fa09ddd19023aea6643
 [8]: http://commits.kde.org/kdenlive/b3552c983b3d190104ee71992535402bc54d745a
 [9]: http://commits.kde.org/kdenlive/761b20a919150be751b610a9f8a21cf94b9356f2
 [10]: http://commits.kde.org/kdenlive/15074cb7dc133f48082053799e604fa570d1985b
 [11]: http://commits.kde.org/kdenlive/3fdcfe16c76216fd16c77c6604d7ec753b13b0a6
 [12]: http://commits.kde.org/kdenlive/c22dbb8ab8e51d30b17303dc80f23c470cfa736f
 [13]: http://commits.kde.org/kdenlive/307f55aad74780072adb65402400238bdc5bdd52
 [14]: http://commits.kde.org/kdenlive/d906e739b5c05b42d50063e5259a56933223b09e
 [15]: http://commits.kde.org/kdenlive/f1132ba3f5a4ea38b007668d0995a9676b2ddd13
 [16]: http://commits.kde.org/kdenlive/7f5645d1255f8b50959e44f88504ba30c212cd6e
 [17]: http://commits.kde.org/kdenlive/d2937a31692501cb96390b725515e0877c731462
 [18]: http://commits.kde.org/kdenlive/45da0f7588d33a3cb10bb6a6a5abec27b8b7ac04
 [19]: http://commits.kde.org/kdenlive/7bffc9fab3d8d0fa49b6cd2023e75f1d4ff96c40
 [20]: http://commits.kde.org/kdenlive/273f1238b06761c0e31edf27c93cab780b25dad3
 [21]: http://commits.kde.org/kdenlive/00cad95df068669b7bdbb13ff9544f96b946de7c
 [22]: http://commits.kde.org/kdenlive/853762e6a9c664e0d457139d3156ea40061b2df7
