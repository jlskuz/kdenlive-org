* Fix error message (cannot create directory) when opening archived project. [Commit.](http://commits.kde.org/kdenlive/084294034dd909caaffa87893c36b3fe37e5bae4) 
* Fix incorrect Url handling in archive feature. [Commit.](http://commits.kde.org/kdenlive/2871dee6c51c827f41ad074c5a5d6b7435c69e08) See bug [#367705](https://bugs.kde.org/367705)
* Switch from kdelibs4 sound to Plasma 5 sound files. [Commit.](http://commits.kde.org/kdenlive/c0b0591e3e21144ec5eae7639aa9d921f059a50b) 

