  * Fix subtitle scrolling. [Commit.][1]
  * Fix language model combobox too small. [Commit.][2]Fixes bug [#465787][3]
  * Scroll timeline when moving a subtitle. Related to #1634. [Commit.][4]
  * Fix subtitles overlap on import. [Commit.][5]
  * Fix subtitle move regression. [Commit.][6]
  * Fix subtitle offset on group move. [Commit.][7]
  * Fix subtitles snapping. [Commit.][8]
  * Fix compilation. [Commit.][9]
  * Fix crash and offset when moving a group with subtitle. [Commit.][10]

 [1]: http://commits.kde.org/kdenlive/bdbf0bf5122ae5fce7334203a7d990dd7032a129
 [2]: http://commits.kde.org/kdenlive/7432631c675d034bfbf266443cc2f7ab6774128b
 [3]: https://bugs.kde.org/465787
 [4]: http://commits.kde.org/kdenlive/b53e78e697793e8fff51dc5ef7cc8eb3584147df
 [5]: http://commits.kde.org/kdenlive/ffda87a8de2bed6812edc23cc9536a01d3d58f0c
 [6]: http://commits.kde.org/kdenlive/cbc0201a7179834dc63a517919238400bb233d5b
 [7]: http://commits.kde.org/kdenlive/56d4646ec9ac56b09c64a7cae39fda98848e927e
 [8]: http://commits.kde.org/kdenlive/c3cfcd9e0254b2b74a5b58d49591bbc08d4c719c
 [9]: http://commits.kde.org/kdenlive/7e2421b2aa82e2658a31291f0a769543e3cddada
 [10]: http://commits.kde.org/kdenlive/998ddfa5e0471b7dba34ebb16909931cae3d7922
