---
author: Farid Abdelnour
date: 2023-03-06T19:24:01+00:00
aliases:
- /en/2023/03/kdenlive-22-12-3-released/
- /it/2023/03/kdenlive-22-12-3-2/
- /de/2023/03/kdenlive-22-12-3/
---
The last maintenance release of the 22.12 series is out with many fixes to subtitles:
