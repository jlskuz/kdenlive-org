---
date: 2003-01-13
title: Kdenlive/PIAVE 0.2.0 beta released
author: Jason Wood
---

We are very happy to announce the first beta release of Kdenlive and PIAVE. Together, Kdenlive and PIAVE form a video editing system, capable of editing, previewing and exporting DV video files.

Kdenlive provides a user-friendly KDE-based GUI environment for managing and editing your project, whilst PIAVE is seamlessly integrated to provide the low level support for video editing and playback.

This beta has been given a new version number (0.2.0) to bring the version numbers of PIAVE and kdenlive into sync.

A full press release can be found below.

# Kdenlive/PIAVE V0.2.0 (beta) released

We have the pleasure to announce the first beta release of Kdenlive and PIAVE.

Kdenlive is a generic video editor GUI. It provides a monitor window with controls, an overview of clips used in the current project, and most important, a timeline where you can arange clips to build a project. Clips can be trimmed, cut, and moved in the timeline. KdenLive can communicate via a XML based protocol to a render engine which handles the actual video data.

PIAVE is a render engine and effect library. It can read DV video files, render effects and transitions between input streams. Streams can be displayed or piped to a file. Any project can link agains libpiave and use the provided features. PIAVE also provides a server application which can receive commands from KdenLive.

- Release version: 0.2.0
- Release focus: Initial beta release, i.e. not for core developers only.

## Project status / features:

Many important features are still missing. But as the project matures and gets to a state where you can actually do something usefull, we want to get people testing what is already there and suggest features, we want to find out what is most important to people, and last not least we want to attract developers. There are many small and not so small things where people could help. Be it GUI development, XML/network development, or development of input, output, or effect plugins.

What you can already do:

- Open files in raw DV format (e.g. from dvgrab)
- trim / cut / arange the clips in the timeline
- scroll / step through the project
- play the project
- load / save projects
- export the project to a raw DV file

What you cannot yet do:

- control your DV cam and grab clips
- import or export other fileformats than raw DV
- handle sound and video separately
- effects and transitions are not yet configurable via the GUI which means they are practically not available.

## Dependencies

### KdenLive:

- build: KDE 3.x
- runtime: piave

### piave:

- libdv >= 0.98
- libxml2 > 2.0.0
- libxml++ = 0.16.0
- freetype2
- gdk-pixbuf >= 0.14.0
- SDL >= 1.1.8
- pkgconfig

## Additional Information / screenshots:

### KdenLive

- Home : http://www.uchian.pwp.blueyonder.co.uk/kdenlive.html
- Sourceforge : http://sourceforge.net/projects/kdenlive/

### PIAVE

- Home : http://modesto.sourceforge.net/piave/index.html
- Sourceforge : http://sourceforge.net/projects/modesto/

Any suggestions, help, bug reports, flames, etc. are welcome. Feel free to use the sourceforge trackers for bugs and feature request. The main mailing list is here: http://sourceforge.net/mail/?group_id=46786

We read kde-multimedia as well.
