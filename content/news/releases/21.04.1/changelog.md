  * Invalidate preview render on subtitle actions. [Commit.][1] Fixes bug [#435692][2]
  * Fix timecode validation on settings dialog. [Commit.][3]
  * Fix proxied clip cannot be dragged from monitor to timeline. [Commit.][4]
  * Fix incorrect speed cycling with j/l keys. [Commit.][5]
  * Ensure render widget is displayed again after being minimized. [Commit.][6]
  * Fix playback speed not reset on pause. [Commit.][7]
  * Update effect zones on effect deletion. [Commit.][8]
  * Render presets: load default values properly to ui. [Commit.][9] See bug [#421174][10]
  * Fix spacer tool not workin on single clips (without groups). [Commit.][11]
  * Improve naming of newely created profile. [Commit.][12] Fixes bug [#385981][13]
  * Archiver: Fix more bugs and crashes. [Commit.][14] See bug [#432206][15]
  * Archiver: Block UI while job is running. [Commit.][16]
  * Archiver: Don’t miss lumas,… on “timline only” mode, prettify code. [Commit.][17]
  * Fix several archiving issues with mlt files. [Commit.][18] Fixes bug [#435882][19]
  * Archive LUT files too. [Commit.][20]
  * Appimage: use mlt v6 branch. [Commit.][21]

 [1]: http://commits.kde.org/kdenlive/3ed0dbf11a251e19a74d717ae1161594e395d61d
 [2]: https://bugs.kde.org/435692
 [3]: http://commits.kde.org/kdenlive/5511f780a3cc525a811ca52b07464a1dfa85a422
 [4]: http://commits.kde.org/kdenlive/75c3f0044b01e41d3adadbc75686cf57eba73d89
 [5]: http://commits.kde.org/kdenlive/efab200479a9126efa2f0291f07e58a59b959fdb
 [6]: http://commits.kde.org/kdenlive/27ef1e4d973ee1ca5c2948742a34f8e641c2cffe
 [7]: http://commits.kde.org/kdenlive/1e2494bb7b0160acc89fb31c74e6b48099b0d8f6
 [8]: http://commits.kde.org/kdenlive/43e78433d23a61f3d38f93d53052e894d91bf111
 [9]: http://commits.kde.org/kdenlive/4c668d8ccc0f407485deff813b63caefb68f33ff
 [10]: https://bugs.kde.org/421174
 [11]: http://commits.kde.org/kdenlive/f559aa5d3351e59ddbd1ddf7eb8a32929d0179ff
 [12]: http://commits.kde.org/kdenlive/232c1753d570f1f3f4b6f082e0297bd33c13031b
 [13]: https://bugs.kde.org/385981
 [14]: http://commits.kde.org/kdenlive/05f3314d59f9f94e947a9c0778da55c518a00fad
 [15]: https://bugs.kde.org/432206
 [16]: http://commits.kde.org/kdenlive/62808fae1fe9ab294930909eead704a905e31e53
 [17]: http://commits.kde.org/kdenlive/3addc5fbf4f5f79bb17d6a8852c084efc96c30a8
 [18]: http://commits.kde.org/kdenlive/80dc8d4e7afeaa24fac4cfc45cc219919ecd3eca
 [19]: https://bugs.kde.org/435882
 [20]: http://commits.kde.org/kdenlive/03c0112172cbfa7fa43345e912de833f565decdd
 [21]: http://commits.kde.org/kdenlive/e485c3ce74af3f14aba2d0368038324fd8335c59
