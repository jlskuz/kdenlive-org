---
title: Kdenlive 21.04.1 is out
author: Farid Abdelnour
date: 2021-05-17T04:00:32+00:00
aliases:
- /en/2021/05/kdenlive-21-04-1-is-out/
- /fr/2021/05/kdenlive-21-04-1-2/
- /it/2021/05/kdenlive-21-04-1-pubblicato/
- /de/2021/05/kdenlive-21-04-1/
---
The first maintenance release of the 21.04 series is out with many bug fixes and improvements.

The video stabilization function (Vidstab) for clips in the project bin will be working again with upcoming version 21.04.2.
