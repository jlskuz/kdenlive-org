---
author: Farid Abdelnour
date: 2024-05-30T18:31:07+00:00
aliases:
- /en/2024/05/kdenlive-24-05-0-released/
- /fr/2024/05/kdenlive-24-05-0-2/
- /it/2024/05/kdenlive-24-05-0-disponibile/
- /de/2024/05/kdenlive-24-05-0/
featured_image_bg: images/features/2405.png
discourse_topic_id: 16384
---
The team is happy to announce Kdenlive 24.05, this update reimplements the Audio Capture feature and focuses on enhancing stability while introducing a few exciting new features like Group Effects and Automatic Subtitle Translations. This version comes with a huge performance boost and the usual batch of quality of life, user interface and usability improvements.
          
This release comes with several performance enhancements, significantly boosting efficiency and responsiveness. Highlights include a massive speed improvement when moving clips with the spacer tool, faster sequence switching, improved AV1 NVENC support, and quicker timeline operations. These optimizations are part of the ongoing performance improvement efforts funded by our recent fundraiser.

## Group Effects

In the last release, we introduced the ability to add an effect to a group of clips. This release now lets you control the parameters affecting all effects within the group.

![](group-effects.gif) 

## Multi Format Rendering

Video editors for social media can now rejoice: Kdenlive offers the ability to render videos in multiple aspect ratios, including horizontal, vertical, and square, all from a single project.

![](formats.png)

Simply set the desired format in the render widget. This feature was developed by Ajay Chauhan as part of the Season of KDE (SoK) and was mentored by the Kdenlive team. The mentoring process was funded by our recent fundraiser.

![](formats-settings.png) 

## Automatic Subtitle Translations

Continuing the subtitle improvements, we have added the ability to automatically translate subtitles using SeamlessM4T. This process happens locally without requiring an internet connection.

![](SeamlessM4T.gif) 

Please note that you need to download the models from the settings first.

![](seamless-config.png)

## Proxy

In this release, we’ve introduced a user-friendly interface for creating and editing external camera proxy profiles. Additionally, we’ve added a new proxy profile for the Insta 360 AcePro (thanks to Silvan Streuli).

![](proxy.gif)

## Improvements

This release brings several improvements to Kdenlive. Track selection is now more intuitive, with double-clicking allowing you to select a track in the timeline. FFmpeg TIMEBASE chapter export has been fixed (thanks to Jonathan Grotelüschen). Nested sequences are now more stable than ever. We've implemented a more robust copy-and-paste and sequence clip duplication system, fixed numerous crashes, and improved sequence compositing. Project archiving has been improved. More filtering options have been added to the file picker when importing clips, including categories like _Video files_, _Audio files_, _Image files_, _Other files_ and _User files _rather than the current _All supported files_ and _All files_ (thanks to Pedro Rodrigues). A new search field has been added to the Settings window. Additionally, integration with OpenTimelineIO has been enhanced.

Other highlights include:

### Multiple Bins
Implemented several fixes for handling multiple bins, ensuring stability and usability.

![](multibin.gif)

### Audio Capture  
The audio capture feature has been reimplemented in Qt6 (thanks to Lev Maslov). There is also now the ability to set the Default capture folder in the project bin as well as setting to allow captures to the stored in a subdirectory of the project folder on disk, rather than only in the root (Thanks to Christopher Vollick).

![](captures.png)

### Monitors  
You may now configure play/pause on monitor click, added the option to _Play Zone From Cursor_ and improved panning and zooming with the middle mouse button.

![](monitor.png) 

### Subtitles  
We’ve enhanced subtitle font styles by adding bold and italic attributes. Whisper now offers an option to set a maximum character count per subtitle and provides better user feedback by showing the output in the speech recognition dialog. In the Speech-to-Text settings, we’ve included links to the model folders and display their sizes.

![](subs.png)
