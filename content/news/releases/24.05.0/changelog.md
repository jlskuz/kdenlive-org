  * Double click to select a track in timeline. [Commit][1]. See bug [#486208][2].
  * Fix sequence clip inserted in another one is not updated if track is locked. [Commit][3]. Fixes bug [#487065][4].
  * Fix duplicating sequence clips. [Commit][5]. Fixes bug [#486855][6].
  * Fix autosave on Windows (and maybe other platforms). [Commit][7].
  * Fix crash on undo sequence close. [Commit][8].
  * Fix wrong FFmpeg chapter export TIMEBASE. [Commit][9]. Fixes bug [#487019][10].
  * Don&#8217;t invalidate sequence clip thumbnail on save, fix manually setting thumb on sequence clip. [Commit][11].
  * Fixes for OpenTimelineIO integration. [Commit][12].
  * Don&#8217;t add normalizers to timeline sequence thumb producer. [Commit][13].
  * Fix crash undoing an effect change in another timeline sequence. [Commit][14].
  * WHen dragging a new clip in timeline, don&#8217;t move existing selection. [Commit][15].
  * Faster sequence switching. [Commit][16].
  * Create sequence thumbs directly from bin clip producer. [Commit][17].
  * Better icon for proxy settings page. [Commit][18].
  * Fix mouse wheel does not scroll effect stack. [Commit][19].
  * Open new bin: only allow opening a folder. [Commit][20].
  * Fix monitor play/pause on click. [Commit][21].
  * Ensure Qtblend is the prefered track compositing option. [Commit][22].
  * Fix thumnbails and task manager crashes. [Commit][23].
  * Various fixes for multiple bin projects. [Commit][24].
  * Fix monitor pan with middle mouse button, allow zoomin until we have 60 pixels in the monitor view. [Commit][25]. See bug [#486211][26].
  * Fix monitor middle mouse pan. [Commit][27].
  * Track compositing is a per sequence setting, correctly handle it. [Commit][28].
  * Fix archive widget showing incorrect required size for project archival. [Commit][29].
  * FIx crash dragging from effect stack to another sequence. [Commit][30]. See bug [#467219][31].
  * Fix typo. [Commit][32].
  * Fix consumer crash on project opening. [Commit][33].
  * Fix copying effect by dragging in project monitor. [Commit][34].
  * Fix crash dropping effect on a track. [Commit][35].
  * Fix duplicating Bin clip does not suplicate effects. [Commit][36]. Fixes bug [#463399][37].
  * Workaround KIO Flatpak crash. [Commit][38]. See bug [#486494][39].
  * Fix effect index broken in effectstack. [Commit][40].
  * Fix double click in timeline clip to add a rotoscoping keyframe breaks effect. [Commit][41].
  * Fix copy/paste rotoscoping effect. [Commit][42].
  * Allow enforcing the Breeze icon theme (disabled by default on all platforms). [Commit][43].
  * Fix effect param flicker on drag. [Commit][44].
  * Fix tests warnings. [Commit][45].
  * Test if we can remove our dark breeze icon theme hack on all platforms with the latest KF changes. [Commit][46].
  * Dont lose image duration when changing project&#8217;s framerate. [Commit][47]. See bug [#486394][48].
  * Fix composition move broken in overwrite mode. [Commit][49].
  * Fix opening Windows project files on Linux creates unwanted folders. [Commit][50]. See bug [#486270][51].
  * Audio record: allow playing timeline when monitoring, clicking track rec&#8230; [Commit][52]. See bug [#486198][53]. See bug [#485660][54].
  * Fix compile warnings. [Commit][55].
  * Fix Ctrl+Wheel not working on some effect parameters. [Commit][56]. Fixes bug [#486233][57].
  * On sequence change: correctly stop audio monitoring, fix crash when recording. [Commit][58].
  * Fix Esc key not correctly stopping audio record. [Commit][59].
  * Fix audio rec device selection on Qt5. [Commit][60].
  * Fix Qt5 compilation. [Commit][61].
  * Fix audio capture source not correctly saved / used when changed. [Commit][62].
  * Fix audio mixer initialization. [Commit][63].
  * Fix crash disabling sequence clip in timeline. [Commit][64]. Fixes bug [#486117][65].
  * Minor fixes and rephrasing for render widget duration info. [Commit][66].
  * Adjust timeline clip offset label position and tooltip. [Commit][67].
  * Feat: Implement effect groups. [Commit][68].
  * Windows: disable force breeze icon and enforce breeze theme by default. [Commit][69].
  * Edit clip duration: process in ripple mode if ripple tool is active. [Commit][70].
  * Delay document notes widget initialisation. [Commit][71].
  * Limit the threads to a maximum of 16 for libx265 encoding. [Commit][72].
  * Another round of warning fixes. [Commit][73].
  * Fix Qt6 deprecation warning. [Commit][74].
  * Restore audio monitor state when connecting a timeline. [Commit][75].
  * Work/audio rec fixes. [Commit][76].
  * Cleanup and fix crash dragging a bin clip effect to a timeline clip. [Commit][77].
  * Add close bin icon in toolbar, reword open new bin. [Commit][78].
  * Correctly ensure all Bin Docks have a unique name, add menu entry in Bin to create new bin. [Commit][79].
  * Fix a few Project Bin regressions. [Commit][80].
  * Remove unused parameter. [Commit][81].
  * Add multi-format rendering. [Commit][82].
  * Fix crash opening a file on startup. [Commit][83].
  * New camera proxy profile for Insta 360 AcePro. [Commit][84].
  * Fix slip tool. [Commit][85].
  * Qt6 Audio recording fixes. [Commit][86].
  * MLT XML concurrency issue: use ReadWriteLock instead of Mutex for smoother operation. [Commit][87].
  * Rename View menu &#8220;Bins&#8221; to &#8220;Project Bins&#8221; to avoid confusion, don&#8217;t set same name for multiple bins. [Commit][88].
  * Add tooltip to channelcopy effect. [Commit][89].
  * Fix crash after save in sequence thumbnails. [Commit][90]. See bug [#485452][91].
  * Remove last use of dropped icon. [Commit][92].
  * Use default breeze icon for audio (fixes mixer widget using all space). [Commit][93].
  * Additional filters for file pickers / better way of handling file filters. [Commit][94].
  * [nightly flatpak] Fix build. [Commit][95].
  * Use default breeze icon for audio. [Commit][96].
  * Fix possible crash on closing app just after opening. [Commit][97].
  * Fix startup crash when pressing Esc. [Commit][98].
  * Fix effects cannot be enabled after saving with disable bin/timeline effects. [Commit][99]. Fixes bug [#438970][100].
  * Audio recording implementation for Qt6. [Commit][101].
  * Fix tests. [Commit][102].
  * Fix guides list widget not properly initialized on startup. [Commit][103].
  * Fix Bin initialized twice on project opening causing various crashes. [Commit][104]. See bug [#485452][91].
  * Fix crashes on insert/overwrite clips move. [Commit][105].
  * Fix clips and compositions not aligned to track after spacer operation. [Commit][106].
  * Fix spacer crash with compositions. [Commit][107].
  * Fix spacer crash with guides, small optimization for group move under timeline cursor. [Commit][108].
  * Correctly delete pluggable actions. [Commit][109].
  * Fix dock action duplication and small mem leak. [Commit][110].
  * View menu: move bins and scopes in submenus. [Commit][111].
  * Ensure autosave is not triggered while saving. [Commit][112].
  * Store multiple bins in Kdenlive Settings, remember each bin type (tree or icon view). [Commit][113].
  * Code cleanup: move subtitle related members from timelinemodel to subtitlemodel. [Commit][114].
  * Faster spacer tool. [Commit][115].
  * Fix tab order of edit profile dialog. [Commit][116].
  * Fix blurry folder icon with some project profiles. [Commit][117].
  * Fix spacer tool with compositions and subtitles (broken by last commit). [Commit][118].
  * Make spacer tool faster. [Commit][119].
  * Monitor: add play zone from cursor. [Commit][120]. Fixes bug [#484103][121].
  * Improve AV1 NVENC export profile. [Commit][122].
  * Translate shortcut too. [Commit][123].
  * Require at least MLT 7.22.0. [Commit][124].
  * Use proper method to remove ampersand accel. [Commit][125].
  * Drop code duplicating what KAboutData::setApplicationData() & KAboutData::setupCommandLine() do. [Commit][126].
  * Fix possible crash when quit just after starting. [Commit][127].
  * Fix crash in sequence clip thumbnails. [Commit][128]. See bug [#483836][129].
  * Fix recent commit not allowing to open project file. [Commit][130].
  * Go back to previous hack around ECM issue. [Commit][131].
  * Restore monitor in full screen if they were when closing Kdenlive. [Commit][132]. See bug [#484081][133].
  * When opening an unrecoverable file, don&#8217;t crash but propose to open a backup. [Commit][134].
  * Ensure we never reset the locale while an MLT XML Consumer is running (it caused data corruption). [Commit][135]. See bug [#483777][136].
  * Fix: favorite effects menu not refreshed when a new effect is set as favorite. [Commit][137].
  * Rotoscoping: add info about return key. [Commit][138].
  * Fix: Rotoscoping not allowing to add points close to bottom of the screen. [Commit][139].
  * Fix: Rotoscoping &#8211; allow closing shape with Return key, don&#8217;t discard initial shape when drawing it and seeking in timeline. [Commit][140]. See bug [#484009][141].
  * Srt_equalizer: drop method that is only available in most recent version. [Commit][142].
  * Fix: Speech to text, allow optional dependencies (srt_equalizer), fix venv not correctly enabled on first install and some packages not installing if optional dep is unavailable. [Commit][143].
  * Update and improve build documentation for Qt6. [Commit][144].
  * Add test for latest cut crash. [Commit][145].
  * Update Readme to GitLab CD destination. [Commit][146].
  * Check if KDE\_INSTALL\_DIRS\_NO\_CMAKE_VARIABLES can be disabled (we still have wrong paths in Windows install). [Commit][147].
  * Fix: cannot revert letter spacing to 0 in title clips. [Commit][148]. Fixes bug [#483710][149].
  * Audio Capture Subdir. [Commit][150].
  * Feat: filter avfilter.fillborders add new methods for filling border. [Commit][151].
  * [nightly flatpak] Use the offical Qt6 runtime. [Commit][152].
  * Update file org.kde.kdenlive.appdata.xml. [Commit][153].
  * Update file org.kde.kdenlive.appdata.xml. [Commit][154].
  * Add .desktop file. [Commit][155].
  * Updated icons and appdata info for Flathub. [Commit][156].
  * Fix whisper model size unit. [Commit][157].
  * Don&#8217;t seek timeline when hover timeline ruler and doing a spacer operation. [Commit][158].
  * Improve install steps for SeamlessM4t, warn user of huge downloads. [Commit][159].
  * Initial implementation of subtitles translation using SeamlessM4T engine. [Commit][160].
  * Make whisper to srt script more robust, use kwargs. [Commit][161].
  * Block Qt5 MLT plugins in thumbnailer when building with Qt6. [Commit][162]. Fixes bug [#482335][163].
  * [CD] Restore use of normal Appimage template after testing. [Commit][164].
  * Fix CI/CD. [Commit][165].
  * [CD] Disable Qt5 jobs. [Commit][166].
  * Speech to text: add a link to models folder and display their size in settings. [Commit][167].
  * Whisper: allow setting a maximum character count per subtitle (enabled by default). [Commit][168].
  * Enforce proper styling for Qml dialogs. [Commit][169].
  * Add missing license info. [Commit][170].
  * Allow customizing camcorder proxy profiles. [Commit][171]. Fixes bug [#481836][172].
  * Don&#8217;t move dropped files in the audio capture folder. [Commit][173].
  * Don&#8217;t Highlight Newly Recorded Audio in the Bin. [Commit][174].
  * Show whisper output in speech recognition dialog. [Commit][175].
  * Ensure translated keyframe names are initialized after qApp. [Commit][176].
  * Don&#8217;t call MinGW ExcHndlInit twice. [Commit][177].
  * Fix extern variable triggering translation before the QApplication was created, breaking translations. [Commit][178].
  * Fix bin thumbnails for missing clips have an incorrect aspect ratio. [Commit][179].
  * Add Bold and Italic attributes to subtitle fonts style. [Commit][180].
  * Warn on opening a project with a non standard fps. [Commit][181]. See bug [#476754][182].
  * Refactor keyframe type related code. [Commit][183].
  * Set Default Audio Capture Bin. [Commit][184].
  * Fix python package detection, install in venv. [Commit][185].
  * Try to fix Mac app not finding its resources. [Commit][186].
  * Another attempt to fix appimage venv. [Commit][187].
  * Add test for nested sequences corruption. [Commit][188]. See bug [#480776][189].
  * Show blue audio/video usage icons in project Bin for all clip types. [Commit][190].
  * Org.kde.kdenlive.appdata: Add developer_name. [Commit][191].
  * Fix compilation warnings. [Commit][192].
  * Better feedback message on failed cut. [Commit][193].
  * Set default empty seek duration to 5 minutes instead of 16 minutes on startup to have a more usable scroll bar. [Commit][194].
  * [Craft macOS] Try to fix signing. [Commit][195].
  * [Craft macOS] Remove config for signing test. [Commit][196].
  * Add some debug output for Mac effect drag crash. [Commit][197].
  * Effect stack: don&#8217;t show drop marker if drop doesn&#8217;t change effect order. [Commit][198].
  * Try to fix crash dragging effect on Mac. [Commit][199].
  * Another try to fix monitor offset on Mac. [Commit][200].
  * Don&#8217;t display useless link when effect category is selected. [Commit][201].
  * Add comment on MLT&#8217;s manual build. [Commit][202].
  * Add basic steps to compile MLT. [Commit][203].
  * Blacklist MLT Qt5 module when building against Qt6. [Commit][204].
  * Org.kde.kdenlive.appdata.xml use <https://bugs.kde.org/enter_bug.cgi?product=kdenlive>. [Commit][205].
  * Fix Qt5 startup crash. [Commit][206].
  * Refactor project loading message. [Commit][207].
  * More rebust fix for copy&paste between sequences. [Commit][208].

\[/et\_pb\_accordion\_item\]\[/et\_pb\_accordion\]\[/et\_pb\_column\]\[/et\_pb\_row\][/et\_pb_section]

 [1]: http://commits.kde.org/kdenlive/7d6bcedcb9fe918e56333ec6e4933328fcf66eb6
 [2]: https://bugs.kde.org/486208
 [3]: http://commits.kde.org/kdenlive/b86f3740eed1d34e75e9bd52709c4ad9650d94c2
 [4]: https://bugs.kde.org/487065
 [5]: http://commits.kde.org/kdenlive/0bb28c3dd32b16292dee6688b1c2e54f21505a0b
 [6]: https://bugs.kde.org/486855
 [7]: http://commits.kde.org/kdenlive/90467e36af378d908fbb2ee381989f21375d1153
 [8]: http://commits.kde.org/kdenlive/502643f0c9df82931aeb17b48debbcd57684512a
 [9]: http://commits.kde.org/kdenlive/df6c63018c091f0a332645012aae06c64829490b
 [10]: https://bugs.kde.org/487019
 [11]: http://commits.kde.org/kdenlive/e4da908d07d156d06bf6d4b97015d8335ed060f0
 [12]: http://commits.kde.org/kdenlive/23371001735a9bb68e8b5a69bd25554972028b03
 [13]: http://commits.kde.org/kdenlive/91cc5aae0fee96708bb86e0dfd1e76d6e2887452
 [14]: http://commits.kde.org/kdenlive/11278cbe943d6fc81bf445ad84c4533e24a08386
 [15]: http://commits.kde.org/kdenlive/e4b1d4f85f69bb8f38cf431ce88a55fd37f60230
 [16]: http://commits.kde.org/kdenlive/06219d4dcb6087246bface53110ac68c8517d12c
 [17]: http://commits.kde.org/kdenlive/b9137109273af1d72f8c5b1fc3b8067c42152d20
 [18]: http://commits.kde.org/kdenlive/4e491b7e33c73ba4074d78c4d045f345d2600c51
 [19]: http://commits.kde.org/kdenlive/9c835baaf2e6037ab879be11ef776650097dd3e9
 [20]: http://commits.kde.org/kdenlive/278077c2d404943fea5dcc5e87859246cb8faf3e
 [21]: http://commits.kde.org/kdenlive/2d38282912cf2d6f4043e39c1cf9a1466f176b19
 [22]: http://commits.kde.org/kdenlive/0d12412a0ebbf5d8737bcc70a632456c9c42f716
 [23]: http://commits.kde.org/kdenlive/192b30f87ee0eef545f28a3c22f2617c73e40159
 [24]: http://commits.kde.org/kdenlive/a42b49e13a6104b62085966d7814082292e5afb7
 [25]: http://commits.kde.org/kdenlive/92b39639b4b6aad4791703542d673f1dfae8c669
 [26]: https://bugs.kde.org/486211
 [27]: http://commits.kde.org/kdenlive/816a9e7ef72af003168f27800781e89ce43441f4
 [28]: http://commits.kde.org/kdenlive/37b279bb242e7353bdfdc6bd4fa1caba34febfce
 [29]: http://commits.kde.org/kdenlive/2696a6ff15bdbf244448757db35520ece7043144
 [30]: http://commits.kde.org/kdenlive/f5ab6310f24537ab5f9f826f7bba629e35881920
 [31]: https://bugs.kde.org/467219
 [32]: http://commits.kde.org/kdenlive/2eb752cecf99a1a0f87b3e672e064a3721abe20c
 [33]: http://commits.kde.org/kdenlive/70d6946adc8b2f4a36eeb3d318cc3efa5eceae81
 [34]: http://commits.kde.org/kdenlive/835408615a1eee62f3e4223f44339aee620b83c9
 [35]: http://commits.kde.org/kdenlive/24aeb4ece7f5bca813399252902dd8028a2038ba
 [36]: http://commits.kde.org/kdenlive/1e604c66aacc10e3690284405958155290535d10
 [37]: https://bugs.kde.org/463399
 [38]: http://commits.kde.org/kdenlive/cd6e37272a030d823dcad705cd0011e3b57745e3
 [39]: https://bugs.kde.org/486494
 [40]: http://commits.kde.org/kdenlive/27cf037445d6004ad70f61689a88f513e6b0b886
 [41]: http://commits.kde.org/kdenlive/778a8ccf664b542ab131b52ba793a5f9f783ddc9
 [42]: http://commits.kde.org/kdenlive/fe964070e709bd6226043a24be2eff2ba632064b
 [43]: http://commits.kde.org/kdenlive/d6b19d414a55758ed25a94d3bc237483fb02d903
 [44]: http://commits.kde.org/kdenlive/d07e2e4857ecd59f7c4f648e3863c7701021f4f3
 [45]: http://commits.kde.org/kdenlive/a7b8f44c583e52fb324ce392cb186ba101fe3495
 [46]: http://commits.kde.org/kdenlive/0b9544749c7f476796144cf60bde8bf39fd60ee0
 [47]: http://commits.kde.org/kdenlive/d9a3acc8fb433c181521f6b31b5c911ca0dc3bfe
 [48]: https://bugs.kde.org/486394
 [49]: http://commits.kde.org/kdenlive/e29f3d99765fafa71dbded1406cb68165039253b
 [50]: http://commits.kde.org/kdenlive/34a5deac4025394635ebd71a87f14dee982f976c
 [51]: https://bugs.kde.org/486270
 [52]: http://commits.kde.org/kdenlive/9840c3e8000b3b32427f66794569ada92248d5a2
 [53]: https://bugs.kde.org/486198
 [54]: https://bugs.kde.org/485660
 [55]: http://commits.kde.org/kdenlive/6c8ebc760ac39ca709418b771ac3320f456afc8f
 [56]: http://commits.kde.org/kdenlive/4d2be0a930f57d621a75303647cd822f84b8bc1c
 [57]: https://bugs.kde.org/486233
 [58]: http://commits.kde.org/kdenlive/22d6974a75298a8e7fbc388bc088186d2b48859e
 [59]: http://commits.kde.org/kdenlive/195f069bc773a45acabfc358a234d0ac4a472eb2
 [60]: http://commits.kde.org/kdenlive/41dfd63f7955cf65a9def88aa59847467157beb4
 [61]: http://commits.kde.org/kdenlive/54e5ef712ad623658940ce43a837e595ae907e1f
 [62]: http://commits.kde.org/kdenlive/f247262d8db72da1ae7b50d671c3f12185849836
 [63]: http://commits.kde.org/kdenlive/f4a16526a7f9207327d984b924d7c182e4689c6c
 [64]: http://commits.kde.org/kdenlive/c00957fdca65fbfa37efdd5e3e8fefec769dfd1e
 [65]: https://bugs.kde.org/486117
 [66]: http://commits.kde.org/kdenlive/73c82a4b62ebb5e555a07f18484dfcd9c75684b0
 [67]: http://commits.kde.org/kdenlive/287d271d1570889e99fa09934bb6239c33b84143
 [68]: http://commits.kde.org/kdenlive/4436021a20eddf9a907e0020a80035d44d61ddc8
 [69]: http://commits.kde.org/kdenlive/2122fe68262b635ce3ed068885fc81c87ade1ae9
 [70]: http://commits.kde.org/kdenlive/07d1b087854bcfe3ab12e555a778e5c0c7a37de3
 [71]: http://commits.kde.org/kdenlive/3f38a05067837de0a4899c4d80538254b58fe476
 [72]: http://commits.kde.org/kdenlive/48773eb64b4d4d903921a06946a26a7f00da5b3c
 [73]: http://commits.kde.org/kdenlive/3e580230e76ceded9175e48048dd518ffbdbee6a
 [74]: http://commits.kde.org/kdenlive/0d339cb30e6885ca76c8076557bbfb8a802bf507
 [75]: http://commits.kde.org/kdenlive/5c35040ef32b4c8b1e3528f1ea96798fe9303509
 [76]: http://commits.kde.org/kdenlive/ad9abafb668e7d3bd0d6058d7b4fa28c2f05723d
 [77]: http://commits.kde.org/kdenlive/109a997b14f8c428f710d95bff48afdd11b029f4
 [78]: http://commits.kde.org/kdenlive/eb803880a2bcccbd4a251ae3b7c27fba8aa5b84b
 [79]: http://commits.kde.org/kdenlive/61e1d8a5ac77689760d4410dedeaf2be96730fd0
 [80]: http://commits.kde.org/kdenlive/beb6ed85bc90d8bbc42b26cd497ba96e48112973
 [81]: http://commits.kde.org/kdenlive/eaa476c17d0b84b2fcf958794078a0c0b68d8cc2
 [82]: http://commits.kde.org/kdenlive/1eae60c08c6f1432e605e2fa9437028e60244f1b
 [83]: http://commits.kde.org/kdenlive/5c7ced0ac4c60d69bda4f7fb60144819245b1afb
 [84]: http://commits.kde.org/kdenlive/27f230bb90b4b66a3020023db75b3207978ae791
 [85]: http://commits.kde.org/kdenlive/b18a630fbbe75dbf7ab6fa512edd2ac446dee336
 [86]: http://commits.kde.org/kdenlive/f9aa5be58f50cfcbcd0d6825f4ac01dda7da77b3
 [87]: http://commits.kde.org/kdenlive/44e03fbf47aa3799861bf433284fcfba1d13494d
 [88]: http://commits.kde.org/kdenlive/34bb483705cc57cde11b0e9fb08335cf19a65845
 [89]: http://commits.kde.org/kdenlive/78ab39b998f660d232fbfbef0fdbfc0d145ca9f2
 [90]: http://commits.kde.org/kdenlive/595065b36d9334956e0fcf314309e0e1ba415b90
 [91]: https://bugs.kde.org/485452
 [92]: http://commits.kde.org/kdenlive/d5f8e57fedcf1f5e3ed612e9516ac084f375ea09
 [93]: http://commits.kde.org/kdenlive/43b085a5d4aa955dbea5cff35408899a8002e506
 [94]: http://commits.kde.org/kdenlive/19ef5271a3c9a8b7369e33944fa39135adb1ec4e
 [95]: http://commits.kde.org/kdenlive/b581ed8aef27738651adc5e8eb327befde3e8dd6
 [96]: http://commits.kde.org/kdenlive/77ce032a383c2fba6c629f59bdc52eac49cd6072
 [97]: http://commits.kde.org/kdenlive/66f914a257649db94648312a7bd2f23015ee6906
 [98]: http://commits.kde.org/kdenlive/43037990c3df713ecab04c8c4c1db36b0d50cb5d
 [99]: http://commits.kde.org/kdenlive/984dec0103b12782dae83e0e292a988db6c23c88
 [100]: https://bugs.kde.org/438970
 [101]: http://commits.kde.org/kdenlive/bfb1ef4c10962dc5b1e285b86b47dc3e1c4d25fa
 [102]: http://commits.kde.org/kdenlive/2114aeed9f2b156cf7d860a1b84f538169b5e0a8
 [103]: http://commits.kde.org/kdenlive/caacdcf244ae1181d2a924c084e694849e5a0c84
 [104]: http://commits.kde.org/kdenlive/b2f03c8063f6579949763fdf2ca34519486b9017
 [105]: http://commits.kde.org/kdenlive/d671ae81af5152418d7876ecaa617a486c620e58
 [106]: http://commits.kde.org/kdenlive/91cba0c769be641534aca71dcd3a14b4f193c86b
 [107]: http://commits.kde.org/kdenlive/cf3c145f0f866e7defd44e0ef24ef4dca0759696
 [108]: http://commits.kde.org/kdenlive/beb7c53c92161e797a793b815b053c47407fb40f
 [109]: http://commits.kde.org/kdenlive/5ac7bc560245dae073f7c5010d07fa057eeda165
 [110]: http://commits.kde.org/kdenlive/04582177d2b3d703b953ca80e6e5f61f65ae04a4
 [111]: http://commits.kde.org/kdenlive/b1f9007632e0705fb77c3054e2ffa4a65cc2143f
 [112]: http://commits.kde.org/kdenlive/5089a93c9c76e008806103b42cbefc6fa6fe78ab
 [113]: http://commits.kde.org/kdenlive/37d248119c16a281245a707f6460ec3e2909b0b7
 [114]: http://commits.kde.org/kdenlive/ea39529e0a56feeb2ad1a833d2a29838f2efe3fa
 [115]: http://commits.kde.org/kdenlive/68772e6a394720a831128e365f9ffd612a9da5ef
 [116]: http://commits.kde.org/kdenlive/4391cd7fa9bbfe89cb3a31af35c377d8ce88e943
 [117]: http://commits.kde.org/kdenlive/0068799e7803ea9fb5ae2b9801570ff74ca1d085
 [118]: http://commits.kde.org/kdenlive/f24a37ea3c4e29e28b010d495b3ed845ff85783c
 [119]: http://commits.kde.org/kdenlive/609278fdfdf3fcb942d2a0812354ffcbb4fce064
 [120]: http://commits.kde.org/kdenlive/664b7f81d2bdf1607e942aabf941ce393fa95798
 [121]: https://bugs.kde.org/484103
 [122]: http://commits.kde.org/kdenlive/117baaae99cb0359549f6f9cc3cd1bbf62c2ee26
 [123]: http://commits.kde.org/kdenlive/c17e7b3b0a16983c58bb128488c172e204224a36
 [124]: http://commits.kde.org/kdenlive/7431d8d2d61108ab2c535ed728dd2d5915390f3f
 [125]: http://commits.kde.org/kdenlive/1085c8393b0242d53ebc8c1b398a2fa43a922fcc
 [126]: http://commits.kde.org/kdenlive/2c14d139511eb16722841867596f10b079324f22
 [127]: http://commits.kde.org/kdenlive/7a09d96c50daf7c1061553da91134f4c6f20033f
 [128]: http://commits.kde.org/kdenlive/5814f7b9aa664e9c3e3e06c747cf94d63b073bc9
 [129]: https://bugs.kde.org/483836
 [130]: http://commits.kde.org/kdenlive/e7cf1299ac4b695bd3fa00aa9e8ec2c4063ed73e
 [131]: http://commits.kde.org/kdenlive/4bbfef63939a67b9dcb7c87bf85f93f33fc7db5b
 [132]: http://commits.kde.org/kdenlive/a28aae2b9a832d1872cd89fc60f36ed2a17cf971
 [133]: https://bugs.kde.org/484081
 [134]: http://commits.kde.org/kdenlive/d296a33fc4cf54aa068d52d2741c4c8ea5af3627
 [135]: http://commits.kde.org/kdenlive/75be42a77c94b84ce0621122c5ee0923b0729b54
 [136]: https://bugs.kde.org/483777
 [137]: http://commits.kde.org/kdenlive/a6be0bb04f44d22992b8d9020937c0f555b7d14d
 [138]: http://commits.kde.org/kdenlive/9d298460a08e62877c4aac19a02a2b0690bb00c5
 [139]: http://commits.kde.org/kdenlive/17f297d3b104be37e8d21622dd2d0411bb8183d0
 [140]: http://commits.kde.org/kdenlive/c8ac96845d315d8bd851abe2c6ae50c09ce3c7b1
 [141]: https://bugs.kde.org/484009
 [142]: http://commits.kde.org/kdenlive/9547fab009008bd258fd7db12e6a7cafe7644965
 [143]: http://commits.kde.org/kdenlive/d36a9521cbbf3032a294ff910259a59eb5ee4a2a
 [144]: http://commits.kde.org/kdenlive/ea26ee639dce142d2e91003c5de32f60b6527435
 [145]: http://commits.kde.org/kdenlive/3dcac3a672e0a29515993e3d4e877c8c3fa94bcc
 [146]: http://commits.kde.org/kdenlive/4bdb8cab65899e6c1db6d96fcee19e351f09d699
 [147]: http://commits.kde.org/kdenlive/17a5a459b3421834d9fcbd7d7202b12cad6c4a7b
 [148]: http://commits.kde.org/kdenlive/a2b7c225f79ac3381aa42d15b4f499b8a88d4de4
 [149]: https://bugs.kde.org/483710
 [150]: http://commits.kde.org/kdenlive/4fd772833ba2a3c38c284d653ffe5fee2e085351
 [151]: http://commits.kde.org/kdenlive/8e6ffa7ee3a368fa21e9730d08e22d23acff4cc8
 [152]: http://commits.kde.org/kdenlive/7fdc7e208464a7822d1d5b5edb0be53afb82aaac
 [153]: http://commits.kde.org/kdenlive/23f07a140f9a63230f2d656f4c506ffefc597974
 [154]: http://commits.kde.org/kdenlive/4c24677833f00308ccbaec3e5489f8f4c18502ad
 [155]: http://commits.kde.org/kdenlive/0838324eb9141524958f896c8fdf0719cdcb3741
 [156]: http://commits.kde.org/kdenlive/a1d559ae3e74c995831751aa14ebc88d0c9d5adf
 [157]: http://commits.kde.org/kdenlive/50de32d2ce6a3bc414f893430c61893ccd982d9c
 [158]: http://commits.kde.org/kdenlive/9fe37266ae37332ee4f52758c736004e222e141c
 [159]: http://commits.kde.org/kdenlive/8b5b2a59d4526b63ea83786c170c23a2bbc1c3b7
 [160]: http://commits.kde.org/kdenlive/46a668e1db7693dd9e21747b9cbc163eb76cea9b
 [161]: http://commits.kde.org/kdenlive/67a0014b2eb5dd17fc278aa210467622862c9322
 [162]: http://commits.kde.org/kdenlive/56cb0ef567cacf4a87964cd51a53371fbc109470
 [163]: https://bugs.kde.org/482335
 [164]: http://commits.kde.org/kdenlive/fb9a27dd79f7553ee8203c737df8c6777720d7ff
 [165]: http://commits.kde.org/kdenlive/34c9b7da3aa40f19375fc215becb64a97b6e4516
 [166]: http://commits.kde.org/kdenlive/0262b8fadbe194f3930f1809d50713a211636d30
 [167]: http://commits.kde.org/kdenlive/f9e2b7397a026b53d168e00edfc7ac82425fe365
 [168]: http://commits.kde.org/kdenlive/9df63007a86e11f10bb3f00b131dd5eb83a2c16d
 [169]: http://commits.kde.org/kdenlive/0791d4462b947859a6325644027beeb76ab0186f
 [170]: http://commits.kde.org/kdenlive/4330a38503a03d2a58cb09c46e3f011d48f72996
 [171]: http://commits.kde.org/kdenlive/0d1393c607c29de66d61089549bca14936c8e382
 [172]: https://bugs.kde.org/481836
 [173]: http://commits.kde.org/kdenlive/d86a7080aa5d933b2790c7df2f202ff2bfdcec57
 [174]: http://commits.kde.org/kdenlive/bd5646c8d86824a9f452bdbc9d2da5468c6d8c2b
 [175]: http://commits.kde.org/kdenlive/164aa27ea980e7b67787c9f032959c1a5e303827
 [176]: http://commits.kde.org/kdenlive/57b52eef0311344c4c9b8fa71505640be1bf36ff
 [177]: http://commits.kde.org/kdenlive/80d543f5604efcff6d733da94843d49302a27579
 [178]: http://commits.kde.org/kdenlive/7c6a7361ec8395becc8b00e9a530461b7427c0fb
 [179]: http://commits.kde.org/kdenlive/159a1aa82da735c839f9580ef32a79a6bd52ee98
 [180]: http://commits.kde.org/kdenlive/3959be837d944cf8fdc21af19f7f22b2fdf3db94
 [181]: http://commits.kde.org/kdenlive/d422c00d13c5253faa463213e3b63c7398999eca
 [182]: https://bugs.kde.org/476754
 [183]: http://commits.kde.org/kdenlive/746eb010d26365b1f73ac82ad829b47c1e281f8b
 [184]: http://commits.kde.org/kdenlive/59e95471d68b149893fda480c41da0deb83b5ce4
 [185]: http://commits.kde.org/kdenlive/cd7c8b05515db975e66fca36e5d38cdf5e8bcb3f
 [186]: http://commits.kde.org/kdenlive/292759a9223bf1c62ef2c77c811b6529f0b49b4a
 [187]: http://commits.kde.org/kdenlive/1a601b86b9fa502a36c36b5ce41dc3b23f3b03dd
 [188]: http://commits.kde.org/kdenlive/f582f63b71e3af36d18d90d91af7e8fd154ce234
 [189]: https://bugs.kde.org/480776
 [190]: http://commits.kde.org/kdenlive/2500afba187ef55e30d1d9c03d966acf9d2a6acc
 [191]: http://commits.kde.org/kdenlive/1887c0cf146de39d6bd237d6a430344326a232b4
 [192]: http://commits.kde.org/kdenlive/fd99df8f84d46f4b010b45f66a6f4dad32d7c549
 [193]: http://commits.kde.org/kdenlive/250dc5cbbeebb74f645d75b3b119d909af5daa4e
 [194]: http://commits.kde.org/kdenlive/e1d6f031b8cbebd05dee71a6f89dac5f2253786b
 [195]: http://commits.kde.org/kdenlive/6f5e1363f6f37b63964a4f4fda31b000eff70718
 [196]: http://commits.kde.org/kdenlive/9d42cc908ba3fab4f00adfdcf45d9bfaca0fe9d2
 [197]: http://commits.kde.org/kdenlive/5643667c70bdbbace72dad4242dd7bbf55d6a167
 [198]: http://commits.kde.org/kdenlive/f821406768e1a6d81e516f2521766156e316f2b1
 [199]: http://commits.kde.org/kdenlive/8cfd211ba21477a7d9d47fe489a9f3c4f279706f
 [200]: http://commits.kde.org/kdenlive/39eb7f9675a70d07210e79a2d133f309f51cc145
 [201]: http://commits.kde.org/kdenlive/2afb4a0ae705c66fc6357cc96faf9ae471b07a87
 [202]: http://commits.kde.org/kdenlive/4b9e6c4fa556eff5210e9369d9f243322781268c
 [203]: http://commits.kde.org/kdenlive/3a4088481ff9851455d253ddb7492664d78b5b8b
 [204]: http://commits.kde.org/kdenlive/b4ea9b9b5e7e8c2596afa50da00ec06856bd649d
 [205]: http://commits.kde.org/kdenlive/301118c72ced211b03d23b1c0fd11f4a507acdfe
 [206]: http://commits.kde.org/kdenlive/9a1f1d5c3ffa7ac809c97a261be40f5ac91df29c
 [207]: http://commits.kde.org/kdenlive/8af4d50f3a5e7cbcb7d4a85a70cb25c681642ea9
 [208]: http://commits.kde.org/kdenlive/5e931db21cefc9828c522eb94d110cc5af4a190d
