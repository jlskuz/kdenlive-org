---
author: Farid Abdelnour
date: 2024-06-20T12:50:16+00:00
aliases:
- /en/2024/06/kdenlive-24-05-1-released/
- /it/2024/06/kdenlive-24-05-1-2/
- /de/2024/06/kdenlive-24-05-1/
discourse_topic_id: 17400
---

The first maintenance release of the 24.05 series is out fixing issues in the spacer tool, effects and compositions, subtitle management and project settings to name a few. We addressed recently introduced crashes and freezes, including fixing the undo/redo track insertion and multiple track insertion issues. This version also improves AppImage packaging and enables notarization for macOS.
