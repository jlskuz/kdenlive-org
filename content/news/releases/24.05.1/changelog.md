  * Don&#8217;t try renaming sequence on double click in empty area of timeline tab bar. [Commit][1].
  * Fix deletion of wrong effect wihh multiple instances of an effect and group effects enabled. [Commit][2].
  * Fix single selected clip disappearing from timeline when dragging a new clip in timeline. [Commit][3].
  * [cmd rendering] Ensure proper kdenlive_render path for AppImage. [Commit][4].
  * Fix freeze/crash on undo/redo track insertion. [Commit][5].
  * Fix crash on undo/redo multiple track insertion. [Commit][6].
  * Project settings: don&#8217;t list embedded title clips as empty files in the project files tab. [Commit][7].
  * Fix undo move effect up/down. On effect move, also move the active index, increase margins between effects. [Commit][8].
  * Fix removing a composition from favorites. [Commit][9].
  * Properly activate effect when added to a timeline clip. [Commit][10].
  * Fix spacer tool can move backwards and overlap existing clips. [Commit][11].
  * Fix crash deleting subtitle when the file url was selected. [Commit][12]. Fixes bug [#487872][13].
  * Fix build when using openGLES. [Commit][14]. Fixes bug [#483425][15].
  * Fix possible crash on project opening. [Commit][16].
  * Fix extra dash added to custom clip job output. [Commit][17]. See bug [#487115][18].
  * Fix usage of QUrl for LUT lists. [Commit][19]. See bug [#487375][20].
  * Fix default keyframe type referencing the old deprecated smooth type. [Commit][21].
  * Be more clever splitting custom ffmpeg commands around quotes. [Commit][22]. See bug [#487115][18].
  * Fix effect name focus in save effect. [Commit][23]. See bug [#486310][24].
  * Fix tests. [Commit][25].
  * Fix selection when cutting an unselected clip under mouse. [Commit][26].
  * Fix loading timeline clip with disabled stack should be disabled. [Commit][27].
  * Fix crash trying to save effect with slash in name. [Commit][28]. Fixes bug [#487224][29].
  * Remove quotes in custom clip jobe, fix progress display. [Commit][30]. See bug [#487115][18].
  * Fix setting sequence thumbnail from clip monitor. [Commit][31].
  * Fix locked track items don&#8217;t have red background on project open. [Commit][32].
  * Fix spacer tool doing fake moves with clips in locked tracks. [Commit][33].
  * Hide timeline clip status tooltip when mouse leaves. [Commit][34]

 [1]: http://commits.kde.org/kdenlive/3ff481deebad133a2eb1affafc00eb22612fb1a6
 [2]: http://commits.kde.org/kdenlive/03c3e2f2aef6d22bfcf872326aabf2213c5d879f
 [3]: http://commits.kde.org/kdenlive/2ca54a3904383089be7a41603623866aceb275a9
 [4]: http://commits.kde.org/kdenlive/70c4b029f501da660b8ae7bf96069bb704634c97
 [5]: http://commits.kde.org/kdenlive/ccb0e2bec67699837555826da1f3edd39df08c95
 [6]: http://commits.kde.org/kdenlive/7190ef37dfaa445855ea65509c00426b114cb081
 [7]: http://commits.kde.org/kdenlive/63df37b4d69b6170eced5a7183401c333324dc1a
 [8]: http://commits.kde.org/kdenlive/b89a60c45bd4337cfd3f8aa20ce81798ad5b4133
 [9]: http://commits.kde.org/kdenlive/8a13b96f643b06bd3062fb4da170a6a97285ef10
 [10]: http://commits.kde.org/kdenlive/ca7b9058b8f17e1791d4c42dbbc7d56f5ef53ed4
 [11]: http://commits.kde.org/kdenlive/fd7a5b5d46067e632aaedc4ffb5efb7546694a3a
 [12]: http://commits.kde.org/kdenlive/1b48bb0919472bd29aee7acea7b37e546ff645ff
 [13]: https://bugs.kde.org/487872
 [14]: http://commits.kde.org/kdenlive/c8efd29521ce1a945e29e134364c2aeb0e1b76d3
 [15]: https://bugs.kde.org/483425
 [16]: http://commits.kde.org/kdenlive/2519cfcaadd9fa4cf14f21729a4c162e0f242c82
 [17]: http://commits.kde.org/kdenlive/51d4aab3ba1fcbeefa55ff9e635b469758b7dfce
 [18]: https://bugs.kde.org/487115
 [19]: http://commits.kde.org/kdenlive/75f150856323c0fff215155d15575e1715e5dc1e
 [20]: https://bugs.kde.org/487375
 [21]: http://commits.kde.org/kdenlive/75d07595c5da760b116a3d72919a334336902b8c
 [22]: http://commits.kde.org/kdenlive/f9b61a0adc6d89a4248a858b44b7a7c24537c803
 [23]: http://commits.kde.org/kdenlive/7a29ef962e4917b001bc1989183860e180763e2e
 [24]: https://bugs.kde.org/486310
 [25]: http://commits.kde.org/kdenlive/80f5213bbd1d0800f96c3414217278d76868239d
 [26]: http://commits.kde.org/kdenlive/c7f6cb483d79e179fd25e2339ba3cbcaea4aef69
 [27]: http://commits.kde.org/kdenlive/50df3927a741a8d1619a3535b48fb751cc22e3ff
 [28]: http://commits.kde.org/kdenlive/5f20e207267244c702c0980d8b86b291da8a3473
 [29]: https://bugs.kde.org/487224
 [30]: http://commits.kde.org/kdenlive/152f36d89141813423f82b8ba57e0f9d42d57c4e
 [31]: http://commits.kde.org/kdenlive/68d27e7683e65fcae17e35bc0bb3fbf3a1e3534f
 [32]: http://commits.kde.org/kdenlive/30e7af218691936041bb1c3dc00964222230376d
 [33]: http://commits.kde.org/kdenlive/dc954530a2450a9a8e88bfa0a162dd0ca16c611a
 [34]: http://commits.kde.org/kdenlive/96b4a57f94c536e7b5a8a27b87a070edf6ff087c
