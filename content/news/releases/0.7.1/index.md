---
date: 2008-12-29
author: Jean-Baptiste Mardelle
---

We are glad to announce the release of Kdenlive 0.7.1. Lots of improvements were made since the 0.7 release. The main changes include:

- New spacer tool
- Possibility to select on which track a transition should apply
- Cache audio and video thumbnails
- Fixes to clip move and effects
- Fix audio effects
- Several new translations
- And a lot of stability and other small bug fixes

A more complete changelog can be seen on our bug tracking page. You can download the source package from our sourceforge page, binary packages should follow soon. A live demonstration for MacOsX intel and Windows users is available. We invite you to test this new release and hope you enjoy it.

**Update:** MLT 0.3.4 was released at the same time and is recommended version to use with Kdenlive 0.7.1.
