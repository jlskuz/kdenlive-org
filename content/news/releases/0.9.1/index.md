---
title: 'Kdenlive 0.9.1: Fixing bugs in Kdenlive 0.9'
author: Jean-Baptiste Mardelle
date: 2012-05-28T09:59:00+00:00
aliases:
- /users/j-b-m/fixing-bugs-kdenlive-09
# Ported from https://web.archive.org/web/20150905112908/https://kdenlive.org/users/j-b-m/fixing-bugs-kdenlive-09
---


Ten days ago, I released the new 0.9 version of the Kdenlive video editor. Several annoying bugs were quickly detected and most of them are now fixed in git. so I am going to release Kdenlive 0.9.1 soon (hopefully tonight).

The fixes include:

* Fix freeze when reloading previously missing clip
* Fade effects lost when moving / resizing clip
* Undoing change in clip crop start breaking clip
* Make disabling of track effects possible
* Fix slideshow clips not working
* Fix crash on composite transition
* Fix crash when opening stop motion widget
* Fix rendering of projects created in another locale

For ubuntu users, these fixes are already available in the sunab's [kdenlive-svn PPA][1], so if you want to give it a try before the official release and give some feedback, that would be great.

Thanks to all the users who took time to help me fix these bugs. And for the next major release, we will probably try to make a beta release cycle so that we can improve the quality of the release.


  [1]: https://launchpad.net/~sunab/+archive/kdenlive-svn
