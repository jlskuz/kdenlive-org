---
title: Kdenlive 24.02.0 RC ready for testing
author: Jean-Baptiste Mardelle
date: 2024-02-02T16:36:59+00:00
aliases:
- /en/2024/02/kdenlive-24-02-0-rc-ready-for-testing/
- /it/2024/02/kdenlive-24-02-0-rc-pronto-per-i-test/
- /de/2024/02/kdenlive-24-02-0-rc-bereit-zum-testen/
discourse_topic_id: 10207
pre_downloads:
- platform: windows
  sources:
  - name: Installable
    url: https://files.kde.org/kdenlive/unstable/kdenlive-24.02.0-RC_A_-x86_64.exe
  - name: Standalone
    url: https://files.kde.org/kdenlive/unstable/kdenlive-24.02.0-RC_A_-x86_64_Standalone.7z
- platform: linux
  sources:
  - name: AppImage
    url: https://files.kde.org/kdenlive/unstable/kdenlive-24.02.0-RC_A_-x86_64.AppImage
- platform: macos
  text: |
    You will need to allow the app to start from **Preferences** -> **Security** on the first run
    
    Requires macOS 11 or later
  sources:
  - name: Silicon
    url: https://files.kde.org/kdenlive/unstable/kdenlive-24.02.0-RC_A_-macos-arm64.dmg
  - name: Intel
    url: https://files.kde.org/kdenlive/unstable/kdenlive-24.02.0-RC_A_-macos-x86_64.dmg
---

![](2402RC.png)

Kdenlive development has been very active since our [November sprint][1] focusing on the 24.02 [KDE Mega Release][2], that will be based on Qt6 (but is still compatible with Qt5) and KF6. In addition to the essential porting tasks, we dedicated efforts to fixing numerous bugs to enhance stability and introduced some exciting new features, which will be detailed in a forthcoming post. Noteworthy progress has also been made on the packaging front. Apart from Linux and Windows versions, we are thrilled to announce the first native version for Mac Arm processors.

The final release is scheduled for the end of the month so now is the time for the community to help us test the release candidate version and report any regressions or bugs found. Leave your feedback in the comments below, report them on our [bugtracker][3] or tell us about it in our next Kdenlive [online Café][4] on the 9th of February at 9PM UTC.

Below, you will find links for the various binaries. Remember that this is a pre-release software recommended for **testing purpose only**.


## Known issues

- **Audio recording** is not working on any platform
- **MacOS version** crashes on effect stack drag and drop, color theme for the application is not working yet and translations are missing.

 [1]: /en/2023/11/kdenlive-sprint-recap-november-2023/
 [2]: https://bugs.kde.org/buglist.cgi?quicksearch=product%3Akdenlive
 [3]: https://kde.org/announcements/megarelease/6/alpha/
 [4]: https://meet.kde.org/b/far-twm-ebr
