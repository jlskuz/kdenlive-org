  * Fix Whisper / numpy version on Mac. [Commit][1].
  * Try to fix venv on Mac. [Commit][2].
  * Fix monitor data lost on switch from/to fullscreen on Windows. [Commit][3]. See bug [#490708][4].
  * Improve default track height calculation. [Commit][5]. See bug [#490869][6].
  * Ensure qtblend composition is always preferred to cairoblend unless specifically requested by user. [Commit][7]. See bug [#491628][8].
  * Better syntax for package check. Patch by Philippe Fremy. [Commit][9].
  * Fix effectstack view keeps scrolling when mouse leaves. [Commit][10].
  * Improve drag and drop of effects, don&#8217;t create 2 separate entries on dropping effect from list. [Commit][11].
  * Fix effect stack scroll on drag. [Commit][12].
  * Stop dragging effect stack when mouse is outside of the widget. [Commit][13].
  * Fix reuse. [Commit][14].
  * Fix some effect names not translated. [Commit][15]. Fixes bug [#491438][16].
  * Fix python venv on Windows. [Commit][17].
  * Fix possible crash on python install and enforce correct packages for Windows Whisper. [Commit][18]. See bug [#490661][19].
  * Switch openai installer to python requirements files, allowing to fix numpy error on Windows. [Commit][20]. See bug [#491350][21].
  * Fix rendering progress sometimes incorrect. [Commit][22]. See bug [#490146][23].
  * Fix NVidia transcoding of 10bit videos (convert to 8bit). [Commit][24].
  * Default to GPU transcoding if available. [Commit][25].
  * Fix incorrect param in audio thumb introduced in recent commit. [Commit][26].
  * Minor optimization for preview render. [Commit][27].
  * Fix titler incorrect background scale. [Commit][28].
  * Fix subtitle widget size on AppImage. [Commit][29].
  * Fix detection of build in MLT lumas. [Commit][30].
  * Fix subtitle widget using too much space. [Commit][31].
  * When doing an extract operation on all tracks and guides are not locked, move / delete the guides accordingly. [Commit][32]. See bug [#490020][33].
  * Fix titler background is pixelated. [Commit][34]. Fixes bug [#431606][35].
  * Fix deleting several single selection items sometimes deletes an unselected clip. [Commit][36].
  * Make audio only render faster. [Commit][37]. See bug [#491109][38].
  * Add video only vaapi transcode profile. [Commit][39].
  * Don&#8217;t start proxy/audio tasks if a clip requires transcoding. [Commit][40].
  * First round of fixes for task manager. [Commit][41].
  * Add vaapi transcoding profile. [Commit][42].
  * Fix crash in task manager. [Commit][43].
  * Separate file for audio tracks fixes: Fix muted tracks exported, don&#8217;t export audio for video render. [Commit][44]. See bug [#491108][45].
  * Fix timeline scrolling down by a few pixels when already at the bottom when using rubberband or spacer. [Commit][46]. Fixes bug [#490956][47].
  * Ensure missing timeline clips all have the &#8220;Invalid&#8221; markup text. [Commit][48].
  * Fix crash cutting missing timeline clip. [Commit][49].
  * Fix possible crash in keyframe editor. [Commit][50].
  * Scroll effect stack view when dragging an effect. [Commit][51].
  * Fix crash adding/editing keyframes on tracks/master when using group effects feature. [Commit][52].
  * Fix possible QT debug crash on paste. [Commit][53].
  * Add Actions to quickly add Marker/Guides in a specific category. [Commit][54].
  * Fix loading sequence clip broken by last commit. [Commit][55].
  * Fix single selection resize affecting other grouped items. [Commit][56].
  * Add a control uuid to each bin clip to ensure clips can not get mixed on project opening. [Commit][57].
  * Fix crash after undo spacer move with guides. [Commit][58].
  * Fix transform keyframe center seeking outside clip. [Commit][59].
  * Fix crash pasting single selected clip. [Commit][60]. Fixes bug [#490370][61].
  * Fix track effects cannot be deleted if &#8220;group effect&#8221; feature is enabled. [Commit][62].
  * Updating a bin clip (name, tag) should be applied to all timelines, not only the current one. [Commit][63].
  * Slightly better adjustment of tracks in multicam view for more than 4 tracks. [Commit][64].
  * Make multitrack tool operate on inactive tracks as well. [Commit][65]. See bug [#489981][66].
  * Fix color theme menu not showing active theme. [Commit][67]. Fixes bug [#489958][68].
  * Fix cannot restore audio after saving project with master muted. [Commit][69]. Fixes bug [#489985][70].
  * Fix crash transcoding an audio only clip. [Commit][71].
  * Generating comobox box from mlt enumeration parameters in generic effect UI. [Commit][72].
  * Add createdate to dynamic text keywords and some tooltip info. [Commit][73]. See bug [#489119][74].
  * Update SoK file copyright. [Commit][75].
  * Implement keyframe curve editor. [Commit][76].
  * Fix transcoding sometimes results in vfr clips. [Commit][77].
  * Fix avgblur minimum value for x offset. [Commit][78].
  * Add sliders to keyframe import offset (Fixes #1884). [Commit][79].
  * Update include/exclude lists with latest status. [Commit][80].
  * Allow Effects/Compositions widgets to be smaller. [Commit][81].
  * Consistently use USE_DBUS in CMake. [Commit][82].
  * Monitor qml overlay: don&#8217;t allow editing effect if timeline cursor is outside effect. [Commit][83].
  * Get rid of ugly public/private hack in tests. [Commit][84].
  * In comments, replace en\_GB words &#8220;colour&#8221; and &#8220;analyse&#8221; with en\_US versions. [Commit][85].
  * Rename CMake option for DBus from NODBUS to USE_DBUS. [Commit][86].
  * Nicer monitor handles for transform effect, don&#8217;t allow invalid rect on resize. [Commit][87].
  * [cmd rendering] Ensure proper kdenlive_render path for AppImage. [Commit][88].
  * [nightly flatpak] update otio. [Commit][89].
  * Explicitly mention that QT&#8217;s major version in MLT must match the one used for Kdenlive. [Commit][90].
  * Fix undo move effect up/down. On effect move, also move the active index, increase margins between effects. [Commit][91].
  * Fix removing a composition from favorites. [Commit][92].
  * Properly activate effect when added to a timeline clip. [Commit][93].
  * Transform effect: allow moving frame from anywhere inside it, hide handles on move. [Commit][94].
  * Fix spacer tool can move backwards and overlap existing clips. [Commit][95].
  * Transform effect: add top, bottom, left, right handles. [Commit][96].
  * Add easing method to video fades. [Commit][97].
  * Transform effect: fix handles move on click. [Commit][98].
  * Transform effect: when working in timeline with several clips using a transform effect, Alt+Click allows cycling through the clips. [Commit][99].
  * [nightly flatpak] Switch to llvm18. [Commit][100].
  * Add grid to monitor effects scene (it snaps). [Commit][101].
  * Run callback server for OAuth2 only if necessary. [Commit][102].
  * Fix extra dash added to custom clip job output. [Commit][103]. See bug [#487115][104].
  * Fix include list license. [Commit][105].
  * Fix usage of QUrl for LUT lists. [Commit][106]. See bug [#487375][107].
  * Rename include/exclude lists. [Commit][108].
  * For Qt5 require at least KF 5.115.0, for Qt6 KF 6.0.0. [Commit][109].
  * Allow custom whitelist files for effects / compositions (no ui yet). [Commit][110].
  * Fix startup crash on KF 6.3. [Commit][111].
  * Fix missing whitelist license. [Commit][112].
  * Implement effects whitelist (WIP). [Commit][113].
  * Fix default keyframe type referencing the old deprecated smooth type. [Commit][114].
  * Warn if custom clip job contains uneven number of quotes. [Commit][115].
  * Be more clever splitting custom ffmpeg commands around quotes. [Commit][116]. See bug [#487115][104].
  * Fix effect name focus in save effect. [Commit][117]. See bug [#486310][118].
  * Fix tests. [Commit][119].
  * Fix selection when cutting an unselected clip under mouse. [Commit][120].
  * Add timestamp to undo commands. [Commit][121].
  * Fix loading timeline clip with disabled stack should be disabled. [Commit][122].
  * Fix crash trying to save effect with slash in name. [Commit][123]. Fixes bug [#487224][124].
  * Remove quotes in custom clip jobe, fix progress display. [Commit][125]. See bug [#487115][104].
  * Replace custom style menu by new KStyleManager menu. [Commit][126].
  * Use KIconTheme::initTheme & KStyleManager::initStyle for proper styling. [Commit][127].
  * Fix setting sequence thumbnail from clip monitor. [Commit][128].
  * ClipJob: fix typo. [Commit][129].
  * ClipJob: correct and update info and whatsthis. [Commit][130]. See bug [#487115][104].
  * Fix locked track items don&#8217;t have red background on project open. [Commit][131].
  * Fix spacer tool doing fake moves with clips in locked tracks. [Commit][132].
  * Hide timeline clip status tooltip when mouse leaves. [Commit][133].
  * UI files: use notr=&#8221;true&#8221; instead of old comment=&#8221;KDE::DoNotExtract&#8221;. [Commit][134].
  * Fix wrong FFmpeg chapter export TIMEBASE. [Commit][135]. Fixes bug [#487019][136].
  * Fix tests build. [Commit][137].
  * With Qt6 the breeze-icons *.rcc files are no longer delivered. [Commit][138].
  * Code Gardening: split main() into logical pieces. [Commit][139].
  * Create sequence thumbs directly from bin clip producer. [Commit][140].
  * Play monitor on click &#8211; make configurable. [Commit][141].
  * Fix opening documentation link for shuttle on Windows. [Commit][142].
  * Fix merge error. [Commit][143].
  * Typo for build command: ninja is use in the cmake command, so `sudo ninja install` should be used. [Commit][144].
  * JogShuttle, add note and link for installation on Windows. [Commit][145]. Fixes bug [#485602][146].
  * Optimize playback of sequence clips (don&#8217;t default to rgba format which triggers expansive compositing checks). [Commit][147].
  * Correct typo “file exist” → “file exists”. [Commit][148].
  * Draft: Clarify that the remaining time calculation is an estimation. [Commit][149].
  * Fix tests warnings. [Commit][150].
  * Improve user feedback. [Commit][151].
  * Audio record: allow playing timeline when monitoring, clicking track rec&#8230; [Commit][152]. See bug [#486198][153]. See bug [#485660][154].
  * Feat: Implement effect groups. [Commit][155].
  * Work/audio rec fixes. [Commit][156].
  * Add multi-format rendering. [Commit][157].
  * Ensure Docks are correctly sorted in view menu. [Commit][158].

 [1]: http://commits.kde.org/kdenlive/ae51d248b6903d2d97bb09d156a1d277f7dbbc20
 [2]: http://commits.kde.org/kdenlive/82de00f46568906948b4c94ce8f634aa0b34f178
 [3]: http://commits.kde.org/kdenlive/c05d6ef5c5fa181e3b7bbe671f45dad97ad31a99
 [4]: https://bugs.kde.org/490708
 [5]: http://commits.kde.org/kdenlive/c56183b2470959c7da52c8704a3b001ca637b7ab
 [6]: https://bugs.kde.org/490869
 [7]: http://commits.kde.org/kdenlive/02823371753be2c5a8885328204ba99bb5e7ab13
 [8]: https://bugs.kde.org/491628
 [9]: http://commits.kde.org/kdenlive/7c11bb848066c7f3387dae35a2e102a41fbad2bd
 [10]: http://commits.kde.org/kdenlive/a8779a2e1fb7415122e751f0906852372dfc1b28
 [11]: http://commits.kde.org/kdenlive/067506595ad99b0f811ec299a268cdb973439748
 [12]: http://commits.kde.org/kdenlive/a56d6b1a226b35d14307bacce0bdbee317ac8135
 [13]: http://commits.kde.org/kdenlive/5fdb9eeb8462dc2f22bc6bc396c17dd8a9d0f887
 [14]: http://commits.kde.org/kdenlive/1783b1a53535386bdef7ad4a418c78ede84fde54
 [15]: http://commits.kde.org/kdenlive/3b7b827c9a3411281ab3628a232ce91df625d8ea
 [16]: https://bugs.kde.org/491438
 [17]: http://commits.kde.org/kdenlive/a1892ee780f494c8d56542c6fc81f822d87c60cf
 [18]: http://commits.kde.org/kdenlive/98d90275df62dcae1e5cb4718482151ca8bcbab3
 [19]: https://bugs.kde.org/490661
 [20]: http://commits.kde.org/kdenlive/79045d36f909cbbda9ea5ad1f402f5eb0441d814
 [21]: https://bugs.kde.org/491350
 [22]: http://commits.kde.org/kdenlive/3b7d3effdc553f0452fa0470d27246daa83cfcb7
 [23]: https://bugs.kde.org/490146
 [24]: http://commits.kde.org/kdenlive/96b8c112805c19f9e896e3221bf9e81ce31f36de
 [25]: http://commits.kde.org/kdenlive/0e1103980fa774284205cba3231c29b973eb6e9f
 [26]: http://commits.kde.org/kdenlive/50279aaa0eef05343bf97f5d10c2a856754e1cf2
 [27]: http://commits.kde.org/kdenlive/73aa71929e1201919ae1f79e77f5f12f5b22d6af
 [28]: http://commits.kde.org/kdenlive/57705fca0e08e06fbb250c27baaaa3bfde389c52
 [29]: http://commits.kde.org/kdenlive/a7e9651d5f562060f08d711563ddc569c3094639
 [30]: http://commits.kde.org/kdenlive/a8539877f8c36f4808b58e7a69499d57cf161be8
 [31]: http://commits.kde.org/kdenlive/9d2ab6da99a5c5d9db8b910d40005c166ae2d139
 [32]: http://commits.kde.org/kdenlive/a71297e64a608b9fcfe433953ddf4b7c7d38b46b
 [33]: https://bugs.kde.org/490020
 [34]: http://commits.kde.org/kdenlive/c5bba2840f577f98b519768eb744cee3b46b329f
 [35]: https://bugs.kde.org/431606
 [36]: http://commits.kde.org/kdenlive/73e6dc2d89c4749c42e981c8f5c31e71be16d2e2
 [37]: http://commits.kde.org/kdenlive/4345933ca67ee473f22d11f90b47032c50f2895b
 [38]: https://bugs.kde.org/491109
 [39]: http://commits.kde.org/kdenlive/be2211a39c8c27f1e1af64626654badd5436a5bc
 [40]: http://commits.kde.org/kdenlive/6c853687123c3234fc64bc4fc86b2fc42c85d82d
 [41]: http://commits.kde.org/kdenlive/7b19a5127a2d6c8bbc90cd71961b86d1fee37f74
 [42]: http://commits.kde.org/kdenlive/446395e115a76fcb83dfa212d2d09be9c14ebeae
 [43]: http://commits.kde.org/kdenlive/b30be6e7cc742249d8125353266394ac8ac86329
 [44]: http://commits.kde.org/kdenlive/9983f74625abbac7288898db2f79db679befbca4
 [45]: https://bugs.kde.org/491108
 [46]: http://commits.kde.org/kdenlive/495f8b40208fa97776b5c75d2a4ae3e30fa22e9d
 [47]: https://bugs.kde.org/490956
 [48]: http://commits.kde.org/kdenlive/074598bbe3f8d8df144e2c8f38f26047a77c5506
 [49]: http://commits.kde.org/kdenlive/f5e6dbe88ca671fab304d19ea65871cb46bb99e1
 [50]: http://commits.kde.org/kdenlive/f26a1ad9a9c043973dcc2da826475ed2b7adb1aa
 [51]: http://commits.kde.org/kdenlive/bcb6a5626b7e5bbf17803520ea4da4c6b08c2911
 [52]: http://commits.kde.org/kdenlive/1e24fdb98483a0cea138affa728066ec54568c84
 [53]: http://commits.kde.org/kdenlive/684271282544763877f0d557be6661586d859bd4
 [54]: http://commits.kde.org/kdenlive/aa0e2da67e3c9d57a637f2f7813301b2bd9c575c
 [55]: http://commits.kde.org/kdenlive/dd4ce57933b1cf5246c35ecbbcaff81028d3ad5d
 [56]: http://commits.kde.org/kdenlive/ff94ce3c7188d00002c20cd6b2bf874017724a32
 [57]: http://commits.kde.org/kdenlive/28f632085a3f80c7bb177e3e496c8411fc0cf902
 [58]: http://commits.kde.org/kdenlive/384bf318b4bccde4677e7dbdd3325d3dff636958
 [59]: http://commits.kde.org/kdenlive/93c2ecb127e4f8181972187c676e9dce48ccf35f
 [60]: http://commits.kde.org/kdenlive/d3d6c259fd44d01caa385ab23ba7d0755c9f763f
 [61]: https://bugs.kde.org/490370
 [62]: http://commits.kde.org/kdenlive/961a94ed5e372d9120bd3a28650377ebde03d3d0
 [63]: http://commits.kde.org/kdenlive/74b4f508b9558a055248e5b3879a909f743e4ba8
 [64]: http://commits.kde.org/kdenlive/5a98255414faf5e0e8129848fde0da1c7ea101ce
 [65]: http://commits.kde.org/kdenlive/c75f11ad51deb642d46625d6f21d79f54ed0220d
 [66]: https://bugs.kde.org/489981
 [67]: http://commits.kde.org/kdenlive/1908c87c81430524a0becdf00a93a92b4bcbf9e6
 [68]: https://bugs.kde.org/489958
 [69]: http://commits.kde.org/kdenlive/63ca3f5010fa2bb8e005bd77166df49f5f18fd2e
 [70]: https://bugs.kde.org/489985
 [71]: http://commits.kde.org/kdenlive/e01e23c7e464d9c8d9170f06c2ba5d19fac3c0aa
 [72]: http://commits.kde.org/kdenlive/36f835a17f0551c6f977085fb612f904ee2d0582
 [73]: http://commits.kde.org/kdenlive/06d4dd408fef5e57492a6c26b4070de5cef4c3a5
 [74]: https://bugs.kde.org/489119
 [75]: http://commits.kde.org/kdenlive/f053c57974f4f3443df96ed3b20377d4db396653
 [76]: http://commits.kde.org/kdenlive/e5ba1698e1fe1bd6858a7ac1c3669e62e6adfff5
 [77]: http://commits.kde.org/kdenlive/342feb8660bc0a8ba27569dfbf6550e00161e341
 [78]: http://commits.kde.org/kdenlive/18c2fc3d4f87f2a38c586ea7a4837e0d376bfca1
 [79]: http://commits.kde.org/kdenlive/659c7f710fa6ef92a35c084d8277d71409cd33b4
 [80]: http://commits.kde.org/kdenlive/384e73e8a10f9c7e733eb4668ece4d8af50ae453
 [81]: http://commits.kde.org/kdenlive/a6c8e7c1faf8c2f28f7bbb0086c1d251599e7534
 [82]: http://commits.kde.org/kdenlive/34f00c8b408a01d3781b90a2954345df52b0093b
 [83]: http://commits.kde.org/kdenlive/cfef6e0b320102dd8ddf9cf857ace6505fec6f4b
 [84]: http://commits.kde.org/kdenlive/5b83daefcc06b41ea2ed6b5da3b2f72cda11375a
 [85]: http://commits.kde.org/kdenlive/4598cf1ecbfd76e7f9a5eb36f253e3ea44752601
 [86]: http://commits.kde.org/kdenlive/241e28a725ae6e6a1765f56685d9cff8b9c5d6ef
 [87]: http://commits.kde.org/kdenlive/90c5f3c6e4a3a1add60610c4ef2ac637d7c05271
 [88]: http://commits.kde.org/kdenlive/3957b8770720d7c45ba36da03578aba3fe16ba33
 [89]: http://commits.kde.org/kdenlive/6760ad62b657094b109406b3ad6ab14484914b00
 [90]: http://commits.kde.org/kdenlive/bed7949c471169b3cb7cc6584be484d0cacf0850
 [91]: http://commits.kde.org/kdenlive/e2589d6e175f94602e286fbd102ab38777069fa3
 [92]: http://commits.kde.org/kdenlive/56b3ee895dede53a34ff8a5197ab5f5af3265c25
 [93]: http://commits.kde.org/kdenlive/e9b609f977cabad47df4f5fd80af6477036e4d72
 [94]: http://commits.kde.org/kdenlive/81c4a19b7c5fda2b88c052ba9ad51e9d8360b8f7
 [95]: http://commits.kde.org/kdenlive/e9b4162040506de67515463581e8ac644b07995d
 [96]: http://commits.kde.org/kdenlive/f2f694d6e59c22e68ffe3ecb887f5bbb0c8f349d
 [97]: http://commits.kde.org/kdenlive/ff4ce58b5da933d5002ceeaf147b1458b73aa8e5
 [98]: http://commits.kde.org/kdenlive/7148ea78e9417d98e9658b5a6f90fb664c4e3fbd
 [99]: http://commits.kde.org/kdenlive/3b285ecc325f1af2f888f2e7cc073eacd4674fd9
 [100]: http://commits.kde.org/kdenlive/bc8752fa105de5f13f7146ca7e4434499d14b6af
 [101]: http://commits.kde.org/kdenlive/61ee0bd1a8084a8daffd26d30649d9b81044aa3f
 [102]: http://commits.kde.org/kdenlive/796293c9ebc81b9264607459df0aba5cd71fb352
 [103]: http://commits.kde.org/kdenlive/572a5bc9cbe4b658317b9f96341961bb0d2c3693
 [104]: https://bugs.kde.org/487115
 [105]: http://commits.kde.org/kdenlive/32b4e922301c8d4b02754f2cca2ba3e83d4fd044
 [106]: http://commits.kde.org/kdenlive/2082d0d04789aec46176f5c7dd1caabd13986a05
 [107]: https://bugs.kde.org/487375
 [108]: http://commits.kde.org/kdenlive/0b64648062be39cc844e353784c3ed17b8ccdfb4
 [109]: http://commits.kde.org/kdenlive/4ae5676407c36329e4fe2e58a23e65c1dac24fdf
 [110]: http://commits.kde.org/kdenlive/a26d3d881e031f7baf1dd325c405b56563ad5a9d
 [111]: http://commits.kde.org/kdenlive/11b80b3ab6a2119dedc35aeaea32d6f61ca571a1
 [112]: http://commits.kde.org/kdenlive/0a536aee9af674714216604ba5036448225fc770
 [113]: http://commits.kde.org/kdenlive/e32a8e4adbb3cd6de045d45e6875c6d377262cd5
 [114]: http://commits.kde.org/kdenlive/929b7726fb07dfc8f072a71bccfe9253d8f34524
 [115]: http://commits.kde.org/kdenlive/96bd844864041cba23fa00bec5cc87c310935d12
 [116]: http://commits.kde.org/kdenlive/128d0eb018d78ca51bc03470a76f13288362c8f2
 [117]: http://commits.kde.org/kdenlive/6c65851d52202189f210e43f52b1c3390d935721
 [118]: https://bugs.kde.org/486310
 [119]: http://commits.kde.org/kdenlive/2dcff57848dcba9b235e861f69cb7a9bf71886ae
 [120]: http://commits.kde.org/kdenlive/9d0236b970ff32521c71237cbaf6a4db41a1692e
 [121]: http://commits.kde.org/kdenlive/7b64a1ab9bdb450a7c129fd56703a3b2998ea1ca
 [122]: http://commits.kde.org/kdenlive/a096dd2dd688bc522f54040b2a0e04295667b8d2
 [123]: http://commits.kde.org/kdenlive/28ebbae62955220632d9dfa9804fdadcd2564a18
 [124]: https://bugs.kde.org/487224
 [125]: http://commits.kde.org/kdenlive/8dad01d8f1e44f807195c439d33d8f9fd0a69d9c
 [126]: http://commits.kde.org/kdenlive/15b7a71a003a85d9185bb542b90d1709647a783a
 [127]: http://commits.kde.org/kdenlive/da972ec4c69d7f9eefd7f325de782aac248373ac
 [128]: http://commits.kde.org/kdenlive/89d17d0f46f8efe0e699f6457f6abdd2a9ba744d
 [129]: http://commits.kde.org/kdenlive/93a3217490485cd5e99fc0bd7eb676e091403fce
 [130]: http://commits.kde.org/kdenlive/1b5e19edd534071bee69c1e1c29272b9fd9d666b
 [131]: http://commits.kde.org/kdenlive/a751cc3b8fbec26382bbf1a61179e7a0d9445570
 [132]: http://commits.kde.org/kdenlive/d8dbf36f5f9909abae5e1f07b7834eb857bb1039
 [133]: http://commits.kde.org/kdenlive/91579c75e4c1717ee67bad4a54ac89d1722732a9
 [134]: http://commits.kde.org/kdenlive/76b70de490c03f0e3afc11e9f2417de944132308
 [135]: http://commits.kde.org/kdenlive/a08ab55b59ec735f7ded0607287a17de47bc667f
 [136]: https://bugs.kde.org/487019
 [137]: http://commits.kde.org/kdenlive/bccb91080971759077ae26a06ae7a6fff95bb66c
 [138]: http://commits.kde.org/kdenlive/f08dfd1be5923934e5cde1f5633c1002be340b19
 [139]: http://commits.kde.org/kdenlive/e614c307a7bf9f655032ce6c62b8b9341d30f08f
 [140]: http://commits.kde.org/kdenlive/13352797bb6faf27ec249ceee114dda8c2f9f002
 [141]: http://commits.kde.org/kdenlive/44bcbab3858679806bf2c8c1e5d47353da0101cf
 [142]: http://commits.kde.org/kdenlive/f7488c8ecdd0e80670d68bdf8f309fdb4f274fd6
 [143]: http://commits.kde.org/kdenlive/374c59b1d58d543f0dae2806a4bb79ef8d697c03
 [144]: http://commits.kde.org/kdenlive/288c218b51da9802c4ff8a1115d5cefaf6d54484
 [145]: http://commits.kde.org/kdenlive/37744c04a686e23f94dac08730976913d8344b42
 [146]: https://bugs.kde.org/485602
 [147]: http://commits.kde.org/kdenlive/1509590ac542ee2f0526e538623ad29c1f106046
 [148]: http://commits.kde.org/kdenlive/c2f61d7fb94147462aab194b4293494a89601a5f
 [149]: http://commits.kde.org/kdenlive/e29c1609f39e9506f43e7d13445938a596a1f1dd
 [150]: http://commits.kde.org/kdenlive/c42a9da793ace6175357c6cce5798bcaf16921f9
 [151]: http://commits.kde.org/kdenlive/1da76c5d4e497969d8da54250af60063982dd67c
 [152]: http://commits.kde.org/kdenlive/776a2890a87bdc687d4772b4b2382c4cd2b09e2d
 [153]: https://bugs.kde.org/486198
 [154]: https://bugs.kde.org/485660
 [155]: http://commits.kde.org/kdenlive/98d7d40e390ebf4b379a4f2686c6195bb2ee9068
 [156]: http://commits.kde.org/kdenlive/5b15c6c36def87eceadcbb0ed2a8ccfd57c83423
 [157]: http://commits.kde.org/kdenlive/3c0cf874b5e66b3499cf2e349c30c04dc1cbc4c4
 [158]: http://commits.kde.org/kdenlive/1360fdd536582ada92031d1c5c71a35ec9522416
