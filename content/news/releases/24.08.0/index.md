---
author: Farid Abdelnour
date: 2024-09-02T00:52:17+00:00
aliases:
- /en/2024/09/kdenlive-24-08-0-released/
- /fr/2024/09/kdenlive-24-08-0/
- /it/2024/09/kdenlive-24-08-0-3/
- /de/2024/09/kdenlive-24-08-0-2/
featured_image_bg: images/features/2405.png
discourse_topic_id: 20782
---

Kdenlive 24.08 is out, and while summer is usually a quieter time for the team, this update comes packed with nifty new features, enhancements, and quality-of-life improvements, specially for Effects and Transitions. This version boosts performance in sequence playback and timeline preview rendering, improves handling of transcoding issues when importing large amounts of clips, adds VAAPI transcode profiles, and fixes GPU transcoding for Nvidia. On the packaging front, Whisper support has been improved for both Windows and macOS while also improving timing issues on all platforms.

## Effects and Transitions

This release comes with many improvements to Effects and Transitions that were made possible thanks to your contributions to our fundraiser.

### Easing Modes

In addition to the existing easing modes (linear, smooth, and constant), we've added several new options like: Cubic In/Out, Exponential In/Out, Circular In/Out, Elastic In/Out, and Bounce In/Out for transitions and effects.

![](easing1.gif) 

![](easing2.gif) 

### Effect Groups

The new Effect Groups feature enables you to control and apply changes to all clips within a group or selection, making the effect editing process more flexible.

![](effect-groups.gif) 

### Transform Effect UX

This release brings  several enhancements to the Transform effect, improving both usability and functionality. The handles’ visuals have been refined by adding more control points for easier adjustments. Now, you can move the frame from anywhere inside it, with the handles automatically disappearing during the move for a cleaner view. Also holding Alt+Click lets you cycle through clips without needing to select them in the timeline. We’ve also added a grid to the monitor, allowing clips to snap into place while moving.

![](transform-1.gif)

### Curve Editor

This version introduces an initial prototype of the highly anticipated curve editor for keyframes. Please note that it is in a **very alpha** state and is **not ready** for production.

![](curve-editor.gif)

## Other Highlights

This release includes over 130 commits. Here are a few highlights:

  * Fixed many Python related issues
  * Fixed Titles background issues
  * Added timestamps to undo commands
  * Fixed many crashes in various operations
  * Add Actions to quickly add Marker/Guides in a specific category
  * Run callback server for OAuth2 only if necessary.
  * Fixed issues with custom Clip Jobs
  * Fixed audio recording regressions
