---
date: 2003-10-20
author: Jason Wood
---

As of last Monday, Kdenlive and Piave version 0.2.3 have been released to the public. This release has a great deal of new features and improvements over the last released version, 0.2.0, including multiple video monitors, improved snap-to-clip support, AVI DV import, and numerous other improvements.

A full press release can be found below.

# Kdenlive/PIAVE V0.2.3 released

We have the pleasure to announce the 0.2.3 release of Kdenlive and PIAVE. This release feature major improvements over the previous 0.2.0 release.

Kdenlive is a video editing GUI. It provides dual video monitors, a multi-track timeline and clip list. Other features include customisable layout support, preliminary support for effects and translation into French.

Editing is possible with various tools via the timeline and video monitors, allowing you to move and resize one or more clips, and razor clips into two. All timeline tools can snap to clip start/ends and the current seek position. Kdenlive communicates via an XML based protocol to a render engine which handles the actual video data.

PIAVE is a render engine and effect library. It can read DV video files, render effects and transitions between input streams. Streams can be displayed or piped to a file. Any project can link agains libpiave and use the provided features. PIAVE also provides a server application which can receive commands from Kdenlive.

## Project status / features:

Visible new features since Kdenlive/Piave 0.2.0 :

- Dual video monitors
- Customisable layout support
- Edit using in/out points
- Splash screen
- Kdenlive Handbook (translated into French by Gilles Caullier <caulier.gilles@free.fr >)
- Suport for reading AVI DV files
- Translated into French by Gilles Caullier

Developer related new features since Kdenlive/Piave 0.2.0 :

- Preliminary support for effects in Kdenlive.
- Preliminary support for video format parameters in Kdenlive.
- Plugin architecture for Piave.
- Release version: 0.2.3
- Release focus: test version. This release is considered stable enough for testing by the general public.

Many important features are still missing, but Kdenlive is now useable for small projects. Even so, we would like people to test what we have and suggest features. We want to find out what is most important to people, and last not least we want to attract developers. There are many small and not so small things where people could help. Be it GUI development, XML/network development, or development of input, output, or effect plugins.

What you can already do:

- Open files in raw DV and AVI DV format (e.g. from dvgrab)
- trim / cut / arange the clips in the timeline
- scroll / step through the project
- play the project
- load / save projects
- export the project to a raw DV file

What you cannot yet do:

- control your DV cam and grab clips (supported by piave but not available via kdenlive)
- import or export other fileformats
- handle sound and video separately
- effects and transitions are not yet configurable via the GUI which means they are practically not available.

## Dependencies

### KdenLive:

- build: KDE 3.x
- runtime: piave

### piave:

- libdv >= 0.98
- libxml2 > 2.0.0
- freetype2
- gdk-pixbuf >= 0.14.0
- SDL >= 1.1.8
- pkgconfig

## Additional Information / screenshots:

### KdenLive

- Home : http://www.uchian.pwp.blueyonder.co.uk/kdenlive.html
- Sourceforge : http://sourceforge.net/projects/kdenlive/

### PIAVE

- Home : http://modesto.sourceforge.net/piave/index.html
- Sourceforge : http://sourceforge.net/projects/modesto/

Any suggestions, help, bug reports, flames, etc. are welcome. Feel free to use the sourceforge trackers for bugs and feature request. The main mailing list is here: http://sourceforge.net/mail/?group_id=46786

We read kde-multimedia as well.
