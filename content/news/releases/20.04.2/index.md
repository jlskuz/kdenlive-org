---
title: Kdenlive 20.04.2
date: 2020-06-08
---

The second maintenance release of Kdenlive 20.04 is out with many bug fixes and usability enhancements.
