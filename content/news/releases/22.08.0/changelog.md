  * Fix string typos. [Commit.][8]
  * Fix tests. [Commit.][9]
  * Ensure mix can easily be resized until clip end. [Commit.][10]
  * Fix project corruption on resize mix start. [Commit.][11]
  * Fix error and corruption loading reverted mixes. [Commit.][12]
  * Revert “Fix possible crash on profile switch, based on a contribution from Ivan Sudakov.”. [Commit.][13]
  * Revert “Try to fix project profile corruption.”. [Commit.][14]
  * Fix ambiguous widget name. [Commit.][15]
  * Fix compilation with KF5 5.86. [Commit.][16]
  * Update kdenliveeffectscategory.rc adding typewriter to Stylize. [Commit.][17]
  * Clearify UI representation of mixes. [Commit.][18]
  * Try to fix project profile corruption. [Commit.][19]
  * Fix possible crash on profile switch, based on a contribution from Ivan Sudakov. [Commit.][20]
  * Added xml UI for the avfilter CMakeLists.txt. [Commit.][21]
  * Added xml UI for the avfilter.shear. [Commit.][22]
  * Added xml UI for the avfilter.scroll. [Commit.][23]
  * Added xml UI for the avfilter.photosensitivity. [Commit.][24]
  * Added xml UI for the avfilter.monochrome. [Commit.][25]
  * Added xml UI for the avfilter.median. [Commit.][26]
  * Added xml UI for the avfilter.kirsch. [Commit.][27]
  * Added xml UI for the avfilter.exposure. [Commit.][28]
  * Added xml UI for the avfilter.epx. [Commit.][29]
  * Added xml UI for the avfilter.colortemperature. [Commit.][30]
  * Added xml UI for the avfilter.colorize. [Commit.][31]
  * Added xml UI for the avfilter.colorcorrect. [Commit.][32]
  * Added xml UI for the avfilter.colorcontrast. [Commit.][33]
  * Added xml UI for the avfilter.chromanr. [Commit.][34]
  * Added xml UI for the avfilter.cas. [Commit.][35]
  * Added xml UI for the avfilter.bilateral. [Commit.][36]
  * Update kdenliveeffectscategory.rc. [Commit.][37]
  * Updated blacklisted_effects.txt. [Commit.][38]
  * Updated CMakeLists.txt for frei0r effects. [Commit.][39]
  * Added xml interface for the frei0r\_bigsh0t\_eq\_to\_stereo. [Commit.][40]
  * Fix crash in proxy test dialog. [Commit.][41]
  * Fix proxy extension not changed when setting changed, fix proxies not rebuilt on param change. [Commit.][42]
  * Fix project cache folder not created on new document, causing thumbs to be recreated on opening. [Commit.][43]
  * Fix proxy aborting on unknown stream type. [Commit.][44]
  * Fix proxy resize with nvenc. [Commit.][45]
  * Fix cast to double moved outside division. [Commit.][46]
  * Fix wrong use of useSourceProfile. [Commit.][47]
  * Extract frame: fix incorrect handling of sar!= 1 profiles and incorrect use of useSourceProfile. [Commit.][48]
  * Render preset edit: allow specifying a file extension (for example mkv for matroska format). [Commit.][49]
  * Edit render profile: make most parameters optionnal, allow editing parameters text. [Commit.][50]
  * Only allow one selected render profile. [Commit.][51]
  * Fix incorrect shortcut sequence, spotted by Eugen. [Commit.][52]
  * Don’t show monitor ruler duration tooltip if no zone is set. [Commit.][53]
  * Fix compile warning. [Commit.][54]
  * Fix filtering TreeItem lists by non-ASCII strings. [Commit.][55] Fixes bug [#432699][56]
  * Add test for non-ascii list filtering (bug 432699). [Commit.][57]
  * Test histogram handling RGB/BGR. [Commit.][58]
  * Use QImage::pixel() in rgbparadegenerator.cpp. [Commit.][59]
  * Use QImage::pixel() in waveform. [Commit.][60]
  * Test waveform RGB/BGR handling. [Commit.][61]
  * Change vectorscope to use QImage::pixel(). [Commit.][62] Fixes bug [#453149][63]
  * Test vectorscope switching red and blue. [Commit.][64]
  * Fix proxy incorrectly scaled to 200px width when creating new project. [Commit.][65]
  * When proxy clip is deleted, ensure the proxy context menu action is unchecked. [Commit.][66]
  * Fix vaapi proxy encoding profile, switch prores to use proxy quality. [Commit.][67] See bug [#436358][68]
  * Ensure monitor is paused when extracting a frame. [Commit.][69]
  * Ensure dropped frames timer stops when playing stops. [Commit.][70]
  * Fix extract frame for playlist clips. [Commit.][71]
  * Extract frame: process in another frame so we don’t block the UI, make sure effects are applied. [Commit.][72]
  * Fix document folder incorrectly set on loading project with “Use parent folder as project folder”. [Commit.][73]
  * Render last frame. [Commit.][74]
  * Don’t crash loading project with incorrect subtrack count. [Commit.][75]
  * Export guides: remember last used format, add reset button to restore default settings. [Commit.][76]
  * Export guides: allow using HH:MM:SS:FF timecode for export. [Commit.][77]
  * Fix timeline audio record broken after pause/play. [Commit.][78]
  * Fix timeline duration offset of -1 frame. [Commit.][79]
  * Fix possible crashes on invalid track position. [Commit.][80]
  * Fix clip selected when not ready on duplicate, leading to incorrect display in clip monitor. [Commit.][81]
  * Don’t add a keyframe on double click unselected clip. [Commit.][82]
  * When copying effect with keyframes, don’t copy keyframes that are past the clip end. [Commit.][83]
  * Fix possible crash on opening shortcuts dialog. [Commit.][84]
  * Fix crash in debug mode when dragging a composition into timeline. [Commit.][85]
  * Manually register newer mime types for older OSes. [Commit.][86]
  * Always inform user if a file write fails. [Commit.][87]
  * Fix changing cursor position when trying to resize effect zoombar. [Commit.][88]
  * Fix save effect stack broken if there is only 1 effect in the stack. [Commit.][89]
  * Fix Insert Zone to Bin out point off by 1. [Commit.][90] Fixes bug [#455883][91]
  * Fix editing clips in external apps on Mac. [Commit.][92]
  * When aborting document load, open a new blank document. [Commit.][93]
  * Fix compilation. [Commit.][94]
  * Show proxy and metadata tabs on new project creation and correctly set their values to the document. [Commit.][95]
  * When source clip and proxy are both missing, propose to recreate proxy (or reuse existing on in case of LRV). [Commit.][96] See bug [#456185][97]
  * Fix external app path not correctly stored when editing for first time. [Commit.][98]
  * Fix editing external app path from Kdenlive settings, try to fix launching app on MacOS. [Commit.][99]
  * Ensure we never add a clip with id = 0. [Commit.][100]
  * Drop unused params. [Commit.][101]
  * Fix button text and glaxnimate path not working on edit clip. [Commit.][102]
  * Get rid of KOpenWithDialog to select default external applications (doesn not work on Windows/Mac), make path to glaxnimate configurable. [Commit.][103]
  * Don’t wait for a clip to be ready to get its type. [Commit.][104] Fixes bug [#456619][105]
  * Reintroduce open multiple video stream clips. [Commit.][106]
  * Remove Old Qt <5.15.2 checks. [Commit.][107]
  * Fix clip monitor sometimes incorrectly raised when editing subtitle style. [Commit.][108]
  * Fix compilation with KF < 5.95. [Commit.][109]
  * Remove mistakenly pushed code. [Commit.][110]
  * DEPENDENCY! Require at least MLT 7.8.0. [Commit.][111]
  * Refactor color scheme handeling to fix bugs. [Commit.][112] Fixes bug [#430580][113]
  * [Ripple Edit] implement snapping. [Commit.][114]
  * Revert changes from another branch that slipped into this one. Whoops. [Commit.][115]
  * Added the ability to change the subtitle framerate. [Commit.][116]
  * Trying to check for more subtitle files on startup. [Commit.][117]
  * Remove unused TimelineController:darkBackground(). [Commit.][118]
  * Fix `if (ok)` in ThumbnailCache::getAudioKey. [Commit.][119]
  * Revert changes from another branch that slipped into this one. Whoops. [Commit.][120]
  * Jobs C++ new keyword does not return zero. [Commit.][121]
  * Select a valid activeTrack for new documents. [Commit.][122]
  * Fix build on windows failing due to deprecated call. [Commit.][123]
  * Resourcewidget QFileDialog prefers “*” as filter. [Commit.][124]
  * Supplement to “Disable pip on Flatpak (we bundle the deps there)”. [Commit.][125]
  * [nightly flatpak] Enable mlt glaxnimate module. [Commit.][126]
  * Fix syntax error. [Commit.][127]
  * [Python Interface] Disable pip on Flatpak (we bundle the deps there). [Commit.][128]
  * DocOpenResult should hold unique_ptr to doc. [Commit.][129]
  * Clean up some comments and dead test code. [Commit.][130]
  * Apply 1 suggestion(s) to 1 file(s). [Commit.][131]
  * Merging with upstream changes in the master. [Commit.][132]
  * Merge to an updated upstream/master. [Commit.][133]
  * Added the ability to change the subtitle framerate. [Commit.][134]
  * Code gardening: Improvements for disabled deprecation versions. [Commit.][135]
  * Fix type (spotted by erjiang). [Commit.][136]
  * Fix render job sometimes not terminating correctly, fix play after render. [Commit.][137]
  * Add missing override keyword. [Commit.][138]
  * Add option to embed subtitles instead of burning them (mkv only). [Commit.][139]
  * Code Gardening: use ecm\_set\_disabled\_deprecation\_versions (only KF for. [Commit.][140]
  * Fix error in project duration. [Commit.][141]
  * Track compositing is now a simple checkbox instead of the deprecated none/high res choice. [Commit.][142]
  * Fix effect parameter spin box incrementing twice on mouse wheel. [Commit.][143]
  * Fix some spelling and grammar in stereotools effect. [Commit.][144] Fixes bug [#455229][145]
  * Fix compilation. [Commit.][146]
  * Improve timeline audio record preview (position and duration of the recording now adjusts when scaling timeline). [Commit.][147]
  * [dev-docs] Make clear how to install without root. [Commit.][148]
  * Test_utils: pass by const reference (cppcheck). [Commit.][149]
  * Next try: use DejaVu Sans instead of Noto Sans. [Commit.][150]
  * [Test] Use Note Sans instead of Liberation Sans (not available on CI). [Commit.][151]
  * Round-trip test for non-BMP unicode characters. [Commit.][152]
  * DropInvalidChars only if trying to recover document. [Commit.][153]
  * Re-organize KdenliveDoc constructor. [Commit.][154]
  * Hidden tracks should not be considered when calculating project duration. [Commit.][155]
  * DEPENDENCY CHANGE! Require at least KDE Frameworks 5.86. [Commit.][156]
  * DEPENDENCY CHANGE! Require at least Qt 5.15.2. [Commit.][157]
  * Ensure we have a 25 fps profile when testing mixes. [Commit.][158]
  * Remove duplicate headers between cpp/h. [Commit.][159]
  * Fix mouse wheel changing render edit preset while scrolling. [Commit.][160]
  * Minor cleanup, fix compilation on some systems. [Commit.][161]
  * Fix compilation &#8211; wrong change committed. [Commit.][162]
  * Fix bug and warning calculating available mix duration when no frame is available. [Commit.][163]
  * Fix timeline scrolling broken after opening a widget from timeline menu, like edit clip duration. [Commit.][164]
  * Fix oversized UI on Windows. [Commit.][165]
  * Fix incorrect encoding in rendered clip name on Windows. [Commit.][166] Fixes bug [#455286][167]
  * Subtitles styling: add shadow and opaque background box options. [Commit.][168]
  * Add support for rawr glaxnimate anims. [Commit.][169]
  * Implement unfinished auto subtitles mode (selected track / clip only). [Commit.][170]
  * Fix regression: audio / video only clips broken and removed from timeline after reopening project. [Commit.][171]
  * Add test for recent audio breaking regression. [Commit.][172]
  * Fix recent regression breaking audio. [Commit.][173] See bug [#455140][174]
  * Use | as + is deprecated. [Commit.][175]
  * Add JSon mimetype for Lottie animations, allow editing and check if module is there at first start. [Commit.][176]
  * Fix incorrect ungroup when dragging selection. [Commit.][177]
  * Add support for Lottie animations using the glaxnimate producer (Add Clip > Add Animation). [Commit.][178]
  * Fix incorrect behavior of external proxies, allow multiple patterns by profile. [Commit.][179] See bug [#455140][174]
  * Fix render url sometimes pointing to incorrect location. [Commit.][180]
  * Trying to check for more subtitle files on startup. [Commit.][181]
  * Colorize track head when armed for sound recording. [Commit.][182]
  * Fix startup warning cannot destroy paint device. [Commit.][183]
  * Fixes for external proxies. [Commit.][184] See bug [#455140][174]
  * Correctly enable current bin item proxy action after proxy is enabled/disabled in project settings. [Commit.][185]
  * Fix timeline cursor sometimes losing sync with wuler playhead. [Commit.][186]
  * Fix freeze copying proxy clips. [Commit.][187]
  * Update plist file. [Commit.][188]
  * Fix timeline cursor small repaint glitch. Maybe related to #1431. [Commit.][189]
  * Fix subtitle save, in cases where the file extension was missing. [Commit.][190]
  * Guides Exporter: also apply offset to {{frame}}. [Commit.][191]
  * Fix incorrect track tag displayed on errors in project opening. [Commit.][192]
  * Add a first basic subtitle test. [Commit.][193]
  * [Rendering] Allow for custom processing thread count. [Commit.][194] Fixes bug [#430193][195]
  * Fix icon color change in some situations (eg. Appimage). [Commit.][196] Fixes bug [#450556][197]
  * Fix line number in subtitle. [Commit.][198]
  * Display all effects in main effects tab. [Commit.][199]
  * Fix mistake in signal blocker. [Commit.][200]
  * Fix AppImage icons. [Commit.][201] See bug [#451406][202]
  * Update audio recording. Now triggered by the play button when in monitor mode. [Commit.][203]
  * Allow video files in luma transition. [Commit.][204]
  * Subtitles style: use undo/redo, enable styling widget when no subtitle is selected. [Commit.][205]
  * Basic support for subtitle styling (only one style for all subtitles). [Commit.][206] See bug [#437159][207]
  * Port deprecated enum. [Commit.][208]
  * Online resources: only show warning about loading time once. [Commit.][209] See bug [#454470][210]
  * Fix startup crash. [Commit.][211]
  * Fix more qt6 compile error. [Commit.][212]
  * Fix crash clicking ok in empty transcoding dialog. [Commit.][213]
  * Fix possible crash when load task is running on exit. [Commit.][214]
  * Add audio-input key to Mac plist hoping it can help fixing the microphone access issue. [Commit.][215]
  * Use | vs + for key. [Commit.][216]
  * Port QtConcurrent::run to new api in qt6. [Commit.][217]
  * Port QStringRef to QStringView in qt6. [Commit.][218]
  * It’s utf-8 by default in qt6. [Commit.][219]
  * Add missing includes (building against qt6). [Commit.][220]
  * Fix signature method for building against qt6. [Commit.][221]
  * Add Qt6::SvgWidgets (it’s a separate module now). [Commit.][222]
  * By default in qt6 QTextStream uses UTF-8. [Commit.][223]
  * Constructor never implemented. [Commit.][224]
  * Fix method signature when we build against qt6. [Commit.][225]
  * Use ${QT\_MAJOR\_VERSION} here too. [Commit.][226]
  * It’s enable by default in qt6. [Commit.][227]
  * Fix selection of default applications to edit audio/image files. [Commit.][228]
  * Fix file watcher broken, changed clips were not detected anymore. [Commit.][229]
  * Fix timeremap clip always using proxies on rendering. [Commit.][230] Fixes bug [#454089][231]
  * Ensure internal effects like subtitles stay on top so that they are not affected by color or transform effects. [Commit.][232]
  * Fix crash on undo center keyframe. [Commit.][233]
  * Fix crash changing clip monitor bg color when no clip is selected. [Commit.][234]
  * Fix crash on undo selected clip insert. [Commit.][235]
  * Add a clang-format pre-commit hook. [Commit.][236]
  * Fix nvenc codec. [Commit.][237] See bug [#454469][238]
  * Fix clip thumbs not discarded on property change. [Commit.][239]
  * On document loading, also check images for changes. [Commit.][240]
  * Fix missing audio with “WebM-VP9/Opus (libre)” preset. [Commit.][241] See bug [#452950][242]
  * Misc fixes in audio recording &#8211; show rec button in mixers when entering monitor mode. [Commit.][243]
  * Stop using avformat clip for testing since it fails… [Commit.][244]
  * Fix tests on system without avformat module. [Commit.][245]
  * Another attempt to correctly find tests dataset on CI. [Commit.][246]
  * Revert move test to use blipflash instead of avformat producer, trying to fix tests. [Commit.][247]
  * Fix tests and mix direction regression. [Commit.][248]
  * Fix compile warning/possible crash. [Commit.][249]
  * Fix major corruption on undo/redo clip cut, with tests. [Commit.][250]
  * Project loading: detect and fix corruption if audio or video clips on the same track use a different producer. [Commit.][251]
  * Fix crash dropping an effect on the clip monitor. [Commit.][252]
  * Revert audio mixer change that broke layout. [Commit.][253]
  * Fix problem detected on project opening show timecode in frames (use proper timeecode info now). [Commit.][254] See bug [#454237][255]
  * Guides Exporter: Replace QTimeEdit with TimecodeDisplay. [Commit.][256]
  * Guides Exporter: allow set a time offset for markers. [Commit.][257]
  * Fix proxying clips broken. [Commit.][258]
  * Speedup maker search. [Commit.][259]
  * Sorry, just seeing this now. Applying change. [Commit.][260]
  * Fix cannot put monitor in fullscreen with mirrored screens. [Commit.][261]
  * Audio record: remove rec button from audio mixers. Recording is now enabled by enabling monitoring (mic icon), then space bar/Esc to record/stop. [Commit.][262]
  * Audio record: show track head control when monitoring is activated, and countdown when recording is started. [Commit.][263]
  * Fix compilation with Qt < 5.14. [Commit.][264]
  * Fix mix on very short AV clips broken, with test. [Commit.][265]
  * Fix Slide mix not correctly updated when creating a new mix on the previous clip, add tests. [Commit.][266] See bug [#453770][267]
  * Fix mix mix not correctly reversed in some cases and on undo. [Commit.][268]
  * Add tooltips to timeline markers and guides. [Commit.][269]
  * Fix slide composition going in wrong direction (mix is still todo). [Commit.][270] See bug [#453770][267]
  * Fix several small glitches in bin selection. [Commit.][271]
  * Fix clip height not aligned to its track. [Commit.][272]
  * Fix speech to text on Mac. [Commit.][273]
  * Mixers: re-add show effect stack to master, improve layout with collapsed track mixers. [Commit.][274]
  * Fix crash/corruption in overwrite mode when moving grouped clips above or below existing tracks. [Commit.][275]
  * Remove deprecated method. [Commit.][276]
  * Refactor audio recording: allow pause/resume with space bar, display live waveform on record. [Commit.][277]
  * [“Export Guide” action] Minor i18n and icon fixes, remove unused code. [Commit.][278]
  * Add content width and height keywords for assets (effects, transitions,…). [Commit.][279]
  * Drop old Appimage build system from the docs too. [Commit.][280]
  * After audio recording, correctly seek to last frame of record in timeline. [Commit.][281]
  * Ensure record control is visible in track head when enabled. [Commit.][282]
  * Various small fixes for proxy status and job (Fixes #1426). [Commit.][283]
  * Fix ordering of new inserted tracks. [Commit.][284]
  * Only warn once per missing font on document opening. [Commit.][285]
  * Add test method to create title clip. [Commit.][286]
  * Fix possible test crash. [Commit.][287]
  * Add ‘reverse’ parameter to transition ‘mix’. [Commit.][288]
  * Update audio mic monitor when record settings changed. [Commit.][289]
  * Add the ability to export guides as chspters text. [Commit.][290] Fixes bug [#449887][291]
  * Lower proxy rendering priority. [Commit.][292]
  * Cleaning up the debug/output. [Commit.][293]
  * Working on the captions. [Commit.][294]
  * Fix custom effect type sometimes incorrect. [Commit.][295]
  * Add AVIF, HEIF and JPEG XL MIME types. [Commit.][296]
  * Fix memleak in test. [Commit.][297]
  * Don’t depend on monitor in load task (better for tests). [Commit.][298]
  * Timeline track head rec control now shows mic level event when not recording. [Commit.][299]
  * Fix audio rec level in mixer (track head still todo). [Commit.][300]
  * [CI] We don’t need KInit. [Commit.][301]
  * Remove old fallback icons (already disabled by default since a while). [Commit.][302]
  * Remove deprecated Appimage tools (we use KDE Craft now). [Commit.][303]
  * Add error message when input and output files are the same &#8211; transcoding. [Commit.][304]
  * Add tests for title text alignment calculation. [Commit.][305] See bug [#407849][306]
  * Fix tests. [Commit.][307]
  * Fix drag incorrectly terminating in icon view. [Commit.][308]
  * Add test for recent thumbnail cache freeze issue. [Commit.][309]
  * Don’t create task until we are sure we want it. [Commit.][310]
  * [Build System] Port away from deprecated ECM path variables. [Commit.][311]
  * [Build System] Replace 5 by ${QT\_MAJOR\_VERSION} in more places (Pt. 2). [Commit.][312]
  * [Build System] Replace 5 by ${QT\_MAJOR\_VERSION} in more places. [Commit.][313]
  * Resolved conflict in /src/bin/model/subtitlemodel.cpp. [Commit.][314]
  * Added support for the sbv format captions. [Commit.][315]
  * Fix crash trying to drag in empty space in Bin icon view. [Commit.][316]
  * Update kdenliveeffectscategory.rc new mlt’s box_blur added to the ‘Blur and Sharpen’ category. [Commit.][317]
  * Update CMakeLists.txt adding the new mlt’s Box_Blur. [Commit.][318]
  * Add new mlt’s Box_Blur ui. It was not working with the automatic one. [Commit.][319]
  * Update secondary\_color\_correction.xml fixing Transparency default value error. [Commit.][320]
  * Fix titler text alignment. [Commit.][321]
  * Fix potential deadlock, maybe related to #1380. [Commit.][322]
  * Small refactoring of cache get thumbnail. [Commit.][323]
  * Fix timeline preview failing when creating a new project. [Commit.][324]
  * Improve tags: allow adding, editing and reordering of tags. [Commit.][325]
  * Timeline preview profiles &#8211; remove unused audio parameters, fix interlaced nvenc. [Commit.][326]
  * Another set of minor improvements for monitor audio level. [Commit.][327]
  * Minor fix in audio levels look. [Commit.][328]
  * Ensure all color clips use the RGBA format. [Commit.][329]
  * Show dB in mixer tooltip. [Commit.][330]
  * Fix audio levels showing incorrect values, and not impacted by master effects. [Commit.][331]
  * Try to fix build against KF 5.93.0. [Commit.][332]
  * Make test failures CI failures (only Linux yet). [Commit.][333]
  * Remove text label. [Commit.][334]
  * Make monitor detection more robust for fullscreen mode. [Commit.][335]
  * Fix app focus lost on Windows when exiting monitor fullscreen. [Commit.][336]
  * Make monitor overlay guides color configurable. [Commit.][337]
  * Add build workaround for Mac M1. [Commit.][338]
  * Code quality fixes. [Commit.][339]
  * Switch from QQuickView to QQuickWidget &#8211; fixes broken playback on Mac OS. [Commit.][340]
  * Update kdenliveeffectscategory.rc moving frei0r.premultiply from ‘More checks’ to ‘Alpha, Mask and Keying’. [Commit.][341]
  * Update blacklisted_effects.txt enabling frei0r.premultiply. [Commit.][342]
  * Use pragma once for header guards. [Commit.][343]
  * Fix minor typos. [Commit.][344]
  * Require at least MLT 7.4.0. [Commit.][345]
  * Added the ability to import webVTT captions. [Commit.][346]
  * Removed duplicate code. [Commit.][347]
  * Increase the flexablilty of importing captions. See issue #1361. [Commit.][348]
  
 [8]: http://commits.kde.org/kdenlive/dbbc30cc5b6763e85ec6f0261c6ed5a5ed0df497
 [9]: http://commits.kde.org/kdenlive/17d752425f6fd22264a73e1ea5de11a8e47c3c88
 [10]: http://commits.kde.org/kdenlive/f90a20e88fd9dd7aa43b33c95db4bc7e89fe3525
 [11]: http://commits.kde.org/kdenlive/7278173248a28e278cc181f4b930cb2c38a00c05
 [12]: http://commits.kde.org/kdenlive/d1647276adb1c5b1a4ed0a13e76f6fc4fa86dbc3
 [13]: http://commits.kde.org/kdenlive/b9c9e4b423b3ef9c4b10de7935abfc558994acc9
 [14]: http://commits.kde.org/kdenlive/1fe9b7de4c92fbfe76be7dbb5328e7fee5c55e4a
 [15]: http://commits.kde.org/kdenlive/19073c3fb1528e14fb8815e93fb6407f2cb618e3
 [16]: http://commits.kde.org/kdenlive/9018c1f8147b7983e22d587be02bf46eecba8cb1
 [17]: http://commits.kde.org/kdenlive/dc05232b93fc72f24c1c18bf060829f6b5836cea
 [18]: http://commits.kde.org/kdenlive/10416866100707c17bbc8688e39a3167f3228a71
 [19]: http://commits.kde.org/kdenlive/f8f8000f2bf3b1d0aef60a6b82bcf24ec6f2d8bf
 [20]: http://commits.kde.org/kdenlive/db4ac94bc4880205230dff0de37ba61ad0c1f4f2
 [21]: http://commits.kde.org/kdenlive/9e4283b048818caf9cc4c059fbbcd7eabd62dee6
 [22]: http://commits.kde.org/kdenlive/5802b7032fede504f086812b0176fe52b4bd8071
 [23]: http://commits.kde.org/kdenlive/32887b8ca893628b7c52a0c37fcbfb85f571bb3a
 [24]: http://commits.kde.org/kdenlive/320f98c1a6c7213ad1807cf290cc487d50f7d006
 [25]: http://commits.kde.org/kdenlive/a8cc17becd68d4fd4b67106eba0e63502a5ed62c
 [26]: http://commits.kde.org/kdenlive/9d8ae43ea3116cb3ebf31ce32bd230092d27944c
 [27]: http://commits.kde.org/kdenlive/755a27d6edbb42068a48b376e9aae0b64c204505
 [28]: http://commits.kde.org/kdenlive/c793da5e69d8ad931060f9c270e917213579f923
 [29]: http://commits.kde.org/kdenlive/62d3c0ea68724ccddc7e9fcb5d947e5be954ce5f
 [30]: http://commits.kde.org/kdenlive/e853f18b4ae475c91729cc46a3fa84be9e1f0874
 [31]: http://commits.kde.org/kdenlive/e8b40071ba3e8bf8191237cc92899eefc976fc0a
 [32]: http://commits.kde.org/kdenlive/b199dd7be6bb8b7b0cdae7cf3da2645741c0e32a
 [33]: http://commits.kde.org/kdenlive/216976eb78577fad648bcb4bee57088582b1193d
 [34]: http://commits.kde.org/kdenlive/87477110816c8b92b4660dd02f23162a47176685
 [35]: http://commits.kde.org/kdenlive/38908560ed065dec2cd6869f2da17c55b0eea39e
 [36]: http://commits.kde.org/kdenlive/f6b4c6fdab65e70da570c07ee997c85470f4aa7c
 [37]: http://commits.kde.org/kdenlive/5203c16e0d41653777f0aedc2c5625c847081377
 [38]: http://commits.kde.org/kdenlive/3080019c86f825e9e26f95e12ba9149fffb750e6
 [39]: http://commits.kde.org/kdenlive/5cfb69b2f6926afc7d5fb2ede88a75089de70893
 [40]: http://commits.kde.org/kdenlive/25adfd245614a08e7cb4e3fd3eb32110b3110f49
 [41]: http://commits.kde.org/kdenlive/6a0ff188e5f1c421963ee198f52c2d21b0162acf
 [42]: http://commits.kde.org/kdenlive/db9c2de73c6589ff16ba6d47c2a1b542416cf3b1
 [43]: http://commits.kde.org/kdenlive/38f68854393ec21656acc10cf557c8f3a7ab2dad
 [44]: http://commits.kde.org/kdenlive/58c4b3f4a0bcafd5b5c960cefa965b1468d1b9f8
 [45]: http://commits.kde.org/kdenlive/976089217a97e0164b1fdd1566b53f82109dc59f
 [46]: http://commits.kde.org/kdenlive/b3e783329477205cfbcd5d570bd31497e2dd6933
 [47]: http://commits.kde.org/kdenlive/3e34893605df4a4a787e471a7e04d6bced201e88
 [48]: http://commits.kde.org/kdenlive/0b4af095c1bf115a124505c4b4261e56d97599f1
 [49]: http://commits.kde.org/kdenlive/a51b140a16fde34118d8ae5969a7a64e14d4d007
 [50]: http://commits.kde.org/kdenlive/364d2f2f3ceefd27d82dac7e3196be3cd564e644
 [51]: http://commits.kde.org/kdenlive/90aaecd6e9d083615966e3efe816b714a018757e
 [52]: http://commits.kde.org/kdenlive/19d6503f23b63b5ec173be91d0028279bd1ad4fe
 [53]: http://commits.kde.org/kdenlive/a3afe0edba0e9a07ea2f78a2fc65d8abc22d7f2a
 [54]: http://commits.kde.org/kdenlive/3fee49e33d38d7853c93f76794accedf95e6d881
 [55]: http://commits.kde.org/kdenlive/9648cb12294da4295654d4e7abc03e1d7b0b2903
 [56]: https://bugs.kde.org/432699
 [57]: http://commits.kde.org/kdenlive/07ad0cf79e33ea41ac60bae1abb2f16d4eda7245
 [58]: http://commits.kde.org/kdenlive/1b0a6e3e45955b2798164c99d48edd064b439d88
 [59]: http://commits.kde.org/kdenlive/23d98aa7e66aa1628ff92c31c19018c9413dbe7d
 [60]: http://commits.kde.org/kdenlive/faf73f361eab4086086996db1e00b89ac7803b7f
 [61]: http://commits.kde.org/kdenlive/25b33e15aa085f7f76e4dd3b3ae8f925f34378f2
 [62]: http://commits.kde.org/kdenlive/e0f68346d015180a0ccce2fd740bb3598f359a4a
 [63]: https://bugs.kde.org/453149
 [64]: http://commits.kde.org/kdenlive/9eca9f074c28203a8ebdbf8ea928af1adcc96f24
 [65]: http://commits.kde.org/kdenlive/2c1f5ee7b2cee806d481bd422a47aee6de8abcc0
 [66]: http://commits.kde.org/kdenlive/fe68811fb40ff9d512dfecfa60910a5180fecb04
 [67]: http://commits.kde.org/kdenlive/050dd0971c42c1124f4245dcd458309d73de4f5f
 [68]: https://bugs.kde.org/436358
 [69]: http://commits.kde.org/kdenlive/7c5cf283b7c0cdfea14fdfb0598237e88aca2002
 [70]: http://commits.kde.org/kdenlive/e9fc92ecf85e443664e9eba9c0d7f10b8003e2c3
 [71]: http://commits.kde.org/kdenlive/96ecfe35ee7b9196c8c9a3dbdbaea73a3e6d10ff
 [72]: http://commits.kde.org/kdenlive/26bcbb6fb920b86ea92970da87d0502816d84ca8
 [73]: http://commits.kde.org/kdenlive/4ad3de1bc2dfac3194878362fdf6ff4887c7f4a9
 [74]: http://commits.kde.org/kdenlive/e525ae5e93061f4a5c21a25d10482e5272249f28
 [75]: http://commits.kde.org/kdenlive/46dcfd32b090d22926ace916e426cf298861fa40
 [76]: http://commits.kde.org/kdenlive/13ae5abeba02eea6c15905adcff2aa188cd8071f
 [77]: http://commits.kde.org/kdenlive/bfbbaec2b161f93e4da417c440ccad0b15fcd17f
 [78]: http://commits.kde.org/kdenlive/c2db6fe127adc34f1075660cf3de8a3e34f6bc03
 [79]: http://commits.kde.org/kdenlive/bac847b699b6fb5705c8a3eb92af08a1bb65ac07
 [80]: http://commits.kde.org/kdenlive/df1b839548806f2c95edea3d5e7e8a479fce3a1b
 [81]: http://commits.kde.org/kdenlive/63548341b42ba433c54d42006e6386af30d29ef1
 [82]: http://commits.kde.org/kdenlive/1908bbea5f848dd6110c128fe9fd37d1ff8a32ff
 [83]: http://commits.kde.org/kdenlive/ee14310617d255beaa0eae7b3a43ae17c9bbcf3f
 [84]: http://commits.kde.org/kdenlive/9e11b4d636cb6f902f399c6627bc1a0d963299bc
 [85]: http://commits.kde.org/kdenlive/0ff5f95e08d212ad3af37c0256ffd9c3d9479212
 [86]: http://commits.kde.org/kdenlive/64428376edc96cde1fc7ec2564635569bd403d3c
 [87]: http://commits.kde.org/kdenlive/44935f201bc46d07bc32cb79b5db92b4847541f4
 [88]: http://commits.kde.org/kdenlive/e2046ae1fc43ec058b08b577433432acd5e74d27
 [89]: http://commits.kde.org/kdenlive/c4c5cbd626242ce4a8dfc9df7d094e3e097d3a7a
 [90]: http://commits.kde.org/kdenlive/d38f885b9115ad53e9ebf61def968480109f604a
 [91]: https://bugs.kde.org/455883
 [92]: http://commits.kde.org/kdenlive/3ae19d7a8d8ab83bc6cbe51892303ce418109335
 [93]: http://commits.kde.org/kdenlive/86980c457d4d022f7566df375b1b54349d743a3e
 [94]: http://commits.kde.org/kdenlive/0b8ac5a34b6b6cec3504e2d9c536d7e5c5d486a9
 [95]: http://commits.kde.org/kdenlive/9b882121e3d3e4a20d1eba8676556111ee44c591
 [96]: http://commits.kde.org/kdenlive/7730d39bd924bdbe57def3ffd7fbf32cf4330c5f
 [97]: https://bugs.kde.org/456185
 [98]: http://commits.kde.org/kdenlive/87f661852b1121fe90d4782cbb24e7c67115856f
 [99]: http://commits.kde.org/kdenlive/9b6370501b36f6f448b997ea09c00a3ee012f6ef
 [100]: http://commits.kde.org/kdenlive/dbde3c7a102a19203de803cf07333fe0b9037b8a
 [101]: http://commits.kde.org/kdenlive/00d8e75041b7bae81fcaf092c9afa24ad892ecf7
 [102]: http://commits.kde.org/kdenlive/9c712b893d8aeac67b97507c7a380a11cc1ac886
 [103]: http://commits.kde.org/kdenlive/ad9573b687039d12fa256c032c5a67afd989f4df
 [104]: http://commits.kde.org/kdenlive/d9d2d81efd98061873cbfbba75e6ee727b8de7f4
 [105]: https://bugs.kde.org/456619
 [106]: http://commits.kde.org/kdenlive/b4b4b727c6c4a650f6b36c566ee1ad4f5b3d20cb
 [107]: http://commits.kde.org/kdenlive/4f6b7b7dbd9d348173a727fc8aa80bfac4513180
 [108]: http://commits.kde.org/kdenlive/b7b8511e2ea54eb840a84219f16c8d5bd2c65e55
 [109]: http://commits.kde.org/kdenlive/b2082142fb721dca8eb4bc5581c143e21a88f212
 [110]: http://commits.kde.org/kdenlive/52ec67d9fbbab88d18c052855d8197791b13d414
 [111]: http://commits.kde.org/kdenlive/9e71fd1aa9331b609d3a53d0c7e2fe41f1e5d628
 [112]: http://commits.kde.org/kdenlive/27e89b7940b3fd9f9175907bbb9ea316f137a1e2
 [113]: https://bugs.kde.org/430580
 [114]: http://commits.kde.org/kdenlive/3a87f6fff787c7207863d83672411fa99faf431f
 [115]: http://commits.kde.org/kdenlive/f20136862971d24f4a4546275fb64733f8dfa37e
 [116]: http://commits.kde.org/kdenlive/56a7ee2252b1be25cef05cbe49ad2679b936bcf5
 [117]: http://commits.kde.org/kdenlive/47c55409dafd843de0605017ba16267a5b50ebd0
 [118]: http://commits.kde.org/kdenlive/2ffa31b80999c5808d0ae8bec0eab67a01b7d722
 [119]: http://commits.kde.org/kdenlive/9f548b8419e77a65e34563b5b47f22973932b570
 [120]: http://commits.kde.org/kdenlive/bc7ec0d4c6052f5df88b23a328499d8081440031
 [121]: http://commits.kde.org/kdenlive/cf47be8af55094d2b58f880117b377d0627976ae
 [122]: http://commits.kde.org/kdenlive/6d70e3042636453d1bc3bd9be0b06b7ca119438c
 [123]: http://commits.kde.org/kdenlive/aaff3cad8a734dc49afdca456c43f9cdd3c951b0
 [124]: http://commits.kde.org/kdenlive/af42148f5c3d280150dde274311d06edf83b98c3
 [125]: http://commits.kde.org/kdenlive/d34c03e636753d2e0b58fd997e712751e3124a58
 [126]: http://commits.kde.org/kdenlive/cb9d880e36a04ddd6836d932a7453329b30ec853
 [127]: http://commits.kde.org/kdenlive/6ce98a151999031bffc2d04cad36aa8b148345bd
 [128]: http://commits.kde.org/kdenlive/f9ef095b9791fb683f391f118c98b0537b254590
 [129]: http://commits.kde.org/kdenlive/d198d4ef30124cd8ef70695df6dead8441fd5231
 [130]: http://commits.kde.org/kdenlive/ea7395ae2273522823f677e07c1df6a19017fe86
 [131]: http://commits.kde.org/kdenlive/864338793ca33e665dfc54fb9f771fc66c6db69b
 [132]: http://commits.kde.org/kdenlive/99331cfffee0aa25c834374660089e0ce50c67d7
 [133]: http://commits.kde.org/kdenlive/ed49a1502f074e4f6ff89abd463a6af1c0588d98
 [134]: http://commits.kde.org/kdenlive/64148a0f0e00af51da461d0fb0e0b27640bca965
 [135]: http://commits.kde.org/kdenlive/b89e89ceb409b85c6fec72dba2ff844f6efa4ab7
 [136]: http://commits.kde.org/kdenlive/dad45354e3a8e3152dc2b75966f30bd139a105e7
 [137]: http://commits.kde.org/kdenlive/2137e158197d29c6c254df5415df11a68fa41259
 [138]: http://commits.kde.org/kdenlive/2fad9869a193f61c9dc936588cf630ac8d10eec6
 [139]: http://commits.kde.org/kdenlive/34b1d26d5385a6262d6397d161f28f5982dc9ddc
 [140]: http://commits.kde.org/kdenlive/25828defd5e96387e968547d14eb7acbafeb7dfa
 [141]: http://commits.kde.org/kdenlive/d6de3ea960f4322f362ea1ecfac50adc15fc18f8
 [142]: http://commits.kde.org/kdenlive/1d451a315a48f46afe5678b611074dcc4e23fb2c
 [143]: http://commits.kde.org/kdenlive/945199a1260cd7aed21afc811af2b8b7702b1d61
 [144]: http://commits.kde.org/kdenlive/beae24e150b0f3316dd0670ec5a18ad70aa0f228
 [145]: https://bugs.kde.org/455229
 [146]: http://commits.kde.org/kdenlive/8a0f7e2626b2f8ecd61f233cdb5faa2192c0b5f9
 [147]: http://commits.kde.org/kdenlive/1ee9dc36af9964a97e3fdaa5701d1ae14e5d31f2
 [148]: http://commits.kde.org/kdenlive/1ca773846cb72547ccae7a66ba9ae27019d75366
 [149]: http://commits.kde.org/kdenlive/a59e74a7219a3d10f0fac3c41c951d525e239e9e
 [150]: http://commits.kde.org/kdenlive/fc8be2bfc8f7e559f0df14886ff84b7069cbeff7
 [151]: http://commits.kde.org/kdenlive/0fc1422affb2983d762134af1d6adeccf0b0ff59
 [152]: http://commits.kde.org/kdenlive/57b1c77aa29b59b3474967f7388eb54e0dc73653
 [153]: http://commits.kde.org/kdenlive/5f0f5883083f5173172d273472f0ae595f1bed31
 [154]: http://commits.kde.org/kdenlive/c1ca48e39f857c5b4f31697a100c707a9e6cde84
 [155]: http://commits.kde.org/kdenlive/9b0aa19834eef4d4875e76ffc698c6ac3e9e0d6d
 [156]: http://commits.kde.org/kdenlive/a3ed8ebe741eb38ae72936c64cb5eb75118e4da6
 [157]: http://commits.kde.org/kdenlive/7dab0bbdf060f9f50ed877e185acfe50a59fa3b0
 [158]: http://commits.kde.org/kdenlive/a33735eaefc502884744942f410ab1cd93531559
 [159]: http://commits.kde.org/kdenlive/6c294a0e1838698da9c03bdeeb34afc921eb0641
 [160]: http://commits.kde.org/kdenlive/f1b07a7dc0b7f607baefb8af01c7a407ba223e7b
 [161]: http://commits.kde.org/kdenlive/5aa2c2b37b6afb1bab1c15200421ba0c1aaf3a59
 [162]: http://commits.kde.org/kdenlive/352f532d24475584511054d49f12845644413429
 [163]: http://commits.kde.org/kdenlive/770227ce7cada1ee47dbcba01e6af8874b60e636
 [164]: http://commits.kde.org/kdenlive/926b426a4b91066593bfcbf96ee33779be9a0e12
 [165]: http://commits.kde.org/kdenlive/404e79def4b30345388201d55176154b1df13117
 [166]: http://commits.kde.org/kdenlive/8705611437a5bc108f3c90c243020ae403a3b644
 [167]: https://bugs.kde.org/455286
 [168]: http://commits.kde.org/kdenlive/7b064297d0b69c6f45b3df0e695bbcc402f11b8c
 [169]: http://commits.kde.org/kdenlive/0e2a23f79f0240806a6c2d72ab297ed29657a68a
 [170]: http://commits.kde.org/kdenlive/dce76ace96511f2dafe5c17fef867339cc09f6e9
 [171]: http://commits.kde.org/kdenlive/89f2e438e0f9fbadbb5bc08cfdd8ef66d08f59c4
 [172]: http://commits.kde.org/kdenlive/c053397880bc93ac4f5e8f3b251718b6b6fc6694
 [173]: http://commits.kde.org/kdenlive/76e4b8398574280acc2ec58b04e4ce3d14d264a1
 [174]: https://bugs.kde.org/455140
 [175]: http://commits.kde.org/kdenlive/7ca6dc0105070199b3194342db003494a435ea35
 [176]: http://commits.kde.org/kdenlive/c17d744fda5d906b8893e5aef5cbe1ceb9749445
 [177]: http://commits.kde.org/kdenlive/ad7739df3b11f092fc069f659ba556e1a0e90c9d
 [178]: http://commits.kde.org/kdenlive/99bea51a2dbe14d7f1816284845972306dff5cfa
 [179]: http://commits.kde.org/kdenlive/ba5e921916992cccd1d868f1e696a077d9f3f179
 [180]: http://commits.kde.org/kdenlive/7478f6c2a870a55c4dbb8b847f18dd64b27c5be2
 [181]: http://commits.kde.org/kdenlive/051469aece26f21cc46043a60d3c2ae1f95020cb
 [182]: http://commits.kde.org/kdenlive/6bcca99289029ad97ec256ffdcdf2604677d6d11
 [183]: http://commits.kde.org/kdenlive/936db127fed4af8497ba0028baa702ed6cb91e05
 [184]: http://commits.kde.org/kdenlive/9564b4093d0ddef1cf905a60db617d46d1175231
 [185]: http://commits.kde.org/kdenlive/4567619797387b357199981d50ba6426b809eb05
 [186]: http://commits.kde.org/kdenlive/469dd8512ddb5ee0afd6d83eab6a7267ab0e1d84
 [187]: http://commits.kde.org/kdenlive/cd368d67f07a74c1191c6363d7fccf3289da4ad6
 [188]: http://commits.kde.org/kdenlive/8a260956a6abd69cedbae3432e99b3e493e5a278
 [189]: http://commits.kde.org/kdenlive/2def7dccda6ed455f0028f6da85d247c11d2a4e2
 [190]: http://commits.kde.org/kdenlive/66cb75acb7191d21a8b5009bc11cc5f8c79f953a
 [191]: http://commits.kde.org/kdenlive/47b4ace1cc712432a87049a268debd835e7654f9
 [192]: http://commits.kde.org/kdenlive/c73b2c71d02d4e2cbec62f35f6b993e156a14d8a
 [193]: http://commits.kde.org/kdenlive/98f085ea942899178614d34138050dfe1b6315c5
 [194]: http://commits.kde.org/kdenlive/75d3a9e2f7703b0aa4ae3343edcec2963e9ffc55
 [195]: https://bugs.kde.org/430193
 [196]: http://commits.kde.org/kdenlive/4ca161d52ba43b894c70d216ee458e43331c0d70
 [197]: https://bugs.kde.org/450556
 [198]: http://commits.kde.org/kdenlive/0b816e142ac8691670582f0700b0c87f9ed3b8f9
 [199]: http://commits.kde.org/kdenlive/b0f6e0b2dd42038d785e6b8bb50386991167976e
 [200]: http://commits.kde.org/kdenlive/4ddb1886f8c22d8f92d4f1c8530622ed38889c9d
 [201]: http://commits.kde.org/kdenlive/06314a77a8f95ad13d2b4f5b72bf423e8a6ecdc3
 [202]: https://bugs.kde.org/451406
 [203]: http://commits.kde.org/kdenlive/6e90d733e862c2618d8f7717a0985ca84661d33a
 [204]: http://commits.kde.org/kdenlive/6658a97d1f49ad6d3cf8f736350a80e9abc073f1
 [205]: http://commits.kde.org/kdenlive/f1b062b9ff54a4c0ab1acebbaec302eee2f10982
 [206]: http://commits.kde.org/kdenlive/04ebbe086748dc6f9d7744fbebd5785bdbff64bb
 [207]: https://bugs.kde.org/437159
 [208]: http://commits.kde.org/kdenlive/62cb16b73271c7c297498b2a95151afda08ab8b8
 [209]: http://commits.kde.org/kdenlive/fe2f5c0ad695865425822616f5bf4fbbb6a4be02
 [210]: https://bugs.kde.org/454470
 [211]: http://commits.kde.org/kdenlive/914d5f2353082643c148a1da85b762248b639bdf
 [212]: http://commits.kde.org/kdenlive/1cf93d78c9e4d0b9ab0bd458a6763b66a6b055a1
 [213]: http://commits.kde.org/kdenlive/cdbb14e0a7d49a24d99756d1fe49fd59f1d46890
 [214]: http://commits.kde.org/kdenlive/124d7175f675e3adeefc3fe92de8be87836dd826
 [215]: http://commits.kde.org/kdenlive/bb1ac44e1431a9db19c995e160832fd16e0bf21d
 [216]: http://commits.kde.org/kdenlive/82b50b0a1a9ce572e6830e8cbd9fbc48c7aca7ce
 [217]: http://commits.kde.org/kdenlive/d1b60bcee87000c5eb9d9c8237edc16b5d9228db
 [218]: http://commits.kde.org/kdenlive/147abbfb7c1bffe3874c1e9f6706f9a94fbafa52
 [219]: http://commits.kde.org/kdenlive/dbd172bc2ad3c9a49e8f158c262d8b4465e48e4b
 [220]: http://commits.kde.org/kdenlive/f2cc59c8e8e0a494e24b3dabd22e0736ff9e7968
 [221]: http://commits.kde.org/kdenlive/1a74ec7987a448cc3a2f3a42afcd9a7a31a62713
 [222]: http://commits.kde.org/kdenlive/a77f92d301d68bb9b8c8fef73a8665de9ec3d66b
 [223]: http://commits.kde.org/kdenlive/979c0a74bcb8774f3950cfefe2c339749252a4e7
 [224]: http://commits.kde.org/kdenlive/84b340b434a9a04af989377a748163c153ff7baa
 [225]: http://commits.kde.org/kdenlive/09a7b3bd99537e007e6190444f7f8632ee8a2446
 [226]: http://commits.kde.org/kdenlive/e59d550c9a53455f4612d1f9d572f148a9304045
 [227]: http://commits.kde.org/kdenlive/5cfe5ef08ed19ee5f000ee40548fd14750c6fab5
 [228]: http://commits.kde.org/kdenlive/e87fb6dab27480faf2fc820a32e5367e70f8616c
 [229]: http://commits.kde.org/kdenlive/dbd284175dc7dd8f394eb140afb4494450be3022
 [230]: http://commits.kde.org/kdenlive/72f5af592ec20b5d30feb270257595a9e13e7b87
 [231]: https://bugs.kde.org/454089
 [232]: http://commits.kde.org/kdenlive/2119d2570af07aeadad37050031fab901843ead2
 [233]: http://commits.kde.org/kdenlive/0565e46cb238a8f899bae3d6becea784c65ba4d6
 [234]: http://commits.kde.org/kdenlive/ba44225094afd82d56d9fc34ec61fde40bdc21d4
 [235]: http://commits.kde.org/kdenlive/057b23cdcb8667370315d1a812d6e3097665acc1
 [236]: http://commits.kde.org/kdenlive/e66d86af84ab83d991d90ddd260986bf7375abce
 [237]: http://commits.kde.org/kdenlive/bbd3f61fadb0d77b48483387cca34012c3c5ad1c
 [238]: https://bugs.kde.org/454469
 [239]: http://commits.kde.org/kdenlive/fe63e559ed74f4a5e3fc38845964216334a3ce5f
 [240]: http://commits.kde.org/kdenlive/6725936091efd320e41a6f3fd73f91a0a183fa44
 [241]: http://commits.kde.org/kdenlive/e8a7a30bd0a5d030de4647ec30382e1d79468ee3
 [242]: https://bugs.kde.org/452950
 [243]: http://commits.kde.org/kdenlive/6e3a0c212e3555ab2f61f7f19df8cdc7bb72f667
 [244]: http://commits.kde.org/kdenlive/c88c0b5b338849feabf532b143caa2dc35bccb9e
 [245]: http://commits.kde.org/kdenlive/81e07119ea035c1704e0a7ddaa6a248ef43935c6
 [246]: http://commits.kde.org/kdenlive/4e79e64877983e4edd9cad2766a2c8d3c9581b7a
 [247]: http://commits.kde.org/kdenlive/1ee07f60edad8959f36bfbfb0442b3427d3c0d33
 [248]: http://commits.kde.org/kdenlive/90bdf55338d7d02f0bd35de92db1e69467ad0416
 [249]: http://commits.kde.org/kdenlive/82dc7c716efda2776b580c1aa081f2e690d14ac0
 [250]: http://commits.kde.org/kdenlive/4e1db7e652083d98007ff0c9bc015e48b7a5cab0
 [251]: http://commits.kde.org/kdenlive/424ab7bb5bcbaeb52d85ce2b4d94a8fdc159419e
 [252]: http://commits.kde.org/kdenlive/de4c5c515b7845f4059c6348ccaf14e9bd0835f7
 [253]: http://commits.kde.org/kdenlive/aa45a7fbcee294aefea06acc209d9a843808d3e7
 [254]: http://commits.kde.org/kdenlive/0eb812a291e4cec7418c8e7515e0f3562b247f83
 [255]: https://bugs.kde.org/454237
 [256]: http://commits.kde.org/kdenlive/7177f9a8370117be140c31fbacee66464b944f02
 [257]: http://commits.kde.org/kdenlive/85435a3b068b6645428345106c99fc5a300934dc
 [258]: http://commits.kde.org/kdenlive/424f6aca05826cd81ba11c4639699a3293e4fad2
 [259]: http://commits.kde.org/kdenlive/1b937acebff1c564b30006b500026f5b79e9c54a
 [260]: http://commits.kde.org/kdenlive/e901a88c6f6e7e1f585654994a5dfb4567a43e20
 [261]: http://commits.kde.org/kdenlive/1ce6428a55e68bcfaa609faf8b75141bda53261c
 [262]: http://commits.kde.org/kdenlive/4c99375c476c8c71e4aa0be5c00657f64f6f769f
 [263]: http://commits.kde.org/kdenlive/9d4c6830618253f7a0cc3ebf9a6d9fb88777d462
 [264]: http://commits.kde.org/kdenlive/3b5ecdd89d26e8745fb270acf453e54325615c7f
 [265]: http://commits.kde.org/kdenlive/5a5fa9e28a7b951b19086da83f2b290dcdd201f5
 [266]: http://commits.kde.org/kdenlive/b0be110a6bbf1a8731c4f01ffda6e370aa2a1ccc
 [267]: https://bugs.kde.org/453770
 [268]: http://commits.kde.org/kdenlive/814d5f5d6cde406e96d112c287de6d1b25855cb1
 [269]: http://commits.kde.org/kdenlive/2260ebf941169971982941d1ca9bdac7049d1a84
 [270]: http://commits.kde.org/kdenlive/b7437141227e703981355139b0d7b8bd6d9a3667
 [271]: http://commits.kde.org/kdenlive/5acc68d68d4db30774df861c671bf99e3ff5ae3f
 [272]: http://commits.kde.org/kdenlive/ddc65879860ede76c1a3b3dab3e5c97e4fdda385
 [273]: http://commits.kde.org/kdenlive/1daedc13d52202fcca914fc4e614a30e415e925e
 [274]: http://commits.kde.org/kdenlive/341524d5db645decab5aee5c28d1bc1b9677b0c9
 [275]: http://commits.kde.org/kdenlive/e8d0c9588fd0f2ee0ce3af906943f912c1c4c7d5
 [276]: http://commits.kde.org/kdenlive/2c8a1ad9cc72f81af3a09f073717c50cc6017f61
 [277]: http://commits.kde.org/kdenlive/ef8a9ac5e766a5a696db6a6a7530422897eb2690
 [278]: http://commits.kde.org/kdenlive/39953946a227a5c2c8ccacef45e449d6c93f7790
 [279]: http://commits.kde.org/kdenlive/bef6aab3efca64323f39768fe23acbda8e476395
 [280]: http://commits.kde.org/kdenlive/a6ea49f8f4688171057a4cf932097fca96e34456
 [281]: http://commits.kde.org/kdenlive/d84fa82f76e9742eeb772c1e489bb84b02d7a4f3
 [282]: http://commits.kde.org/kdenlive/a5fae71baaf0c90a0a4106dca6da29939bf651fa
 [283]: http://commits.kde.org/kdenlive/3dcf36a62fcc29c5206d9e9f1c357b96bf5145c2
 [284]: http://commits.kde.org/kdenlive/63a11481034449fd5b1e6a474994a6a86bbc3fe3
 [285]: http://commits.kde.org/kdenlive/93118c5dc574a0c3c5ef10861b9d47a7421038d5
 [286]: http://commits.kde.org/kdenlive/a79e12403a0a8ab5df9011f36084b8f3ca02b9bf
 [287]: http://commits.kde.org/kdenlive/fa7d1da6dd14e9667982c6f17beaaaa6f826f9b5
 [288]: http://commits.kde.org/kdenlive/7a11f526ac0fc94e6d1c23b6be2eca96a4071698
 [289]: http://commits.kde.org/kdenlive/4513f0a6beb34ec8b15c97a3062e573ec82e0dd1
 [290]: http://commits.kde.org/kdenlive/695306aec3d5f6f22285c29868c20acc4fa10ab4
 [291]: https://bugs.kde.org/449887
 [292]: http://commits.kde.org/kdenlive/421b1137bb5eae9a9c5a69f0bee835620e533343
 [293]: http://commits.kde.org/kdenlive/f7ba0e8476c32d36961958efe43658c9a37e10e5
 [294]: http://commits.kde.org/kdenlive/56102edd8ba1b9a5a2f40f2fb8bdc537b4e6bba7
 [295]: http://commits.kde.org/kdenlive/7258d7603f28f1722a5c796b7e9a0582579c8039
 [296]: http://commits.kde.org/kdenlive/620edf0881d110ce42f9bec1d2a9c5fbc8a82c8e
 [297]: http://commits.kde.org/kdenlive/538e461735ae5a430536b71aaf9aeb7ab4da166e
 [298]: http://commits.kde.org/kdenlive/a35fec5a9721cb486cfd346ae1d27f7978e53db8
 [299]: http://commits.kde.org/kdenlive/56b297a72de8f53e2c1fd2900b8b3fdc1a77ae7d
 [300]: http://commits.kde.org/kdenlive/12203f46634142ec8d2d5ff390603928c401c691
 [301]: http://commits.kde.org/kdenlive/633969f9d695607a7f436fe1d644ecb6ec0ce0e8
 [302]: http://commits.kde.org/kdenlive/75715f5f65b8431d557e446d3cca7e5f9c94f97c
 [303]: http://commits.kde.org/kdenlive/fcc075cb18fbd354685359acc4808ee718d1aabc
 [304]: http://commits.kde.org/kdenlive/1abf2cb67ac69371d15ad65acb12bfd52d860124
 [305]: http://commits.kde.org/kdenlive/d8fd6be84853915d0e2df778ceda0a9d73836e38
 [306]: https://bugs.kde.org/407849
 [307]: http://commits.kde.org/kdenlive/6147876552b55943196b704f845afe66df718570
 [308]: http://commits.kde.org/kdenlive/1b145e9931ea4facacc48f56d965fd0aa13be9d4
 [309]: http://commits.kde.org/kdenlive/3632bf05785cfa76cdd08eb660c27a93aa5c17d0
 [310]: http://commits.kde.org/kdenlive/5bbd0e6eef2e99d46b53e178663d69fe4d3613a2
 [311]: http://commits.kde.org/kdenlive/f66d9b7c215c4acfb1971087d26c44a3925a8f96
 [312]: http://commits.kde.org/kdenlive/4c274dbc8757f8a44da58546d040b45fef73730d
 [313]: http://commits.kde.org/kdenlive/40387125f0fca8278a363cc493874225b7a962e1
 [314]: http://commits.kde.org/kdenlive/20e6ae7c4936c979e598f7c1a8cda359529f2d1d
 [315]: http://commits.kde.org/kdenlive/e2414819cbaa1a655223ddc0b65bc02b31e4febb
 [316]: http://commits.kde.org/kdenlive/05d51c2a8a56786eccee363489ab61bcdc1d44b6
 [317]: http://commits.kde.org/kdenlive/518f367ac392a2adbaf4808cbfe7d24910c95f9c
 [318]: http://commits.kde.org/kdenlive/963b19936399bbd76683855c0ade09d4b1bbf9c3
 [319]: http://commits.kde.org/kdenlive/8dd25be83ba1032c3b4eaf9350e455f2938332e8
 [320]: http://commits.kde.org/kdenlive/48fc74275ea28496bb45460cdae5f60f290e7d38
 [321]: http://commits.kde.org/kdenlive/370bb750e0519e32716742366186e9b6a6fce5b0
 [322]: http://commits.kde.org/kdenlive/44e24dcdccd6bdcf4d6e7412e9c792c95a8f9b20
 [323]: http://commits.kde.org/kdenlive/1b3c3f7bf640ca28abc8f0a732ea87170ac91d37
 [324]: http://commits.kde.org/kdenlive/3d27961c4246bcb4d23764a72f64c6b666184616
 [325]: http://commits.kde.org/kdenlive/871d539af786ed944662e02487616886828d6df8
 [326]: http://commits.kde.org/kdenlive/c88dd88d74458f802504e0ad333636986eaa7d26
 [327]: http://commits.kde.org/kdenlive/764f76261c420698d0a7fdf9fe081f4f6c85aafd
 [328]: http://commits.kde.org/kdenlive/6fc77a1d61b9c8bf51177db4a620b186433cdab7
 [329]: http://commits.kde.org/kdenlive/ff3e6f987c102996a38f360ab00af0840f702faf
 [330]: http://commits.kde.org/kdenlive/83449faae82e54fa14bec3f49b07bbeb9d6a2bbc
 [331]: http://commits.kde.org/kdenlive/3a3c8aba6d1ece4edc09cf4ee853c97c95f72a1b
 [332]: http://commits.kde.org/kdenlive/5d81e669582eecd10cae4093cf8532eb62c72a26
 [333]: http://commits.kde.org/kdenlive/46f8b132183a61b7fa34dc6469d065db3ba5a80a
 [334]: http://commits.kde.org/kdenlive/a3cbd6ead2fedfce83ac1418e3dbe2965a167e8c
 [335]: http://commits.kde.org/kdenlive/48d60bba1a45137dc0cdc0f03db2e0850c51c2cd
 [336]: http://commits.kde.org/kdenlive/3594e8a2aefc0501adc49058c87a2a40edb8f9f0
 [337]: http://commits.kde.org/kdenlive/8469dfac736c79766312ad4415a1ac28d037760a
 [338]: http://commits.kde.org/kdenlive/24b913e8708c15091044bc56203bc2d25b696444
 [339]: http://commits.kde.org/kdenlive/f9da836d1a82bfa74b9d70cea4c16e9fc4fee6b4
 [340]: http://commits.kde.org/kdenlive/c1ff434e93bce7837aa96757f51b47d912c2f05a
 [341]: http://commits.kde.org/kdenlive/e7271d9187db004729e1c717ecb75827e5cb78ad
 [342]: http://commits.kde.org/kdenlive/5ebe7a1f4dbcadf413456394d0f0c57cbd4c948f
 [343]: http://commits.kde.org/kdenlive/4e5c59057377a1c0437e826ec9d336540eef275b
 [344]: http://commits.kde.org/kdenlive/22c13d4e1700bdfc85004239d9fcb4fd131fa7d4
 [345]: http://commits.kde.org/kdenlive/061fa53a4c9c512508ad793ed07bd1e841fc1bad
 [346]: http://commits.kde.org/kdenlive/08ad5a0c47aca01019ff3c02f8a035712fa404ea
 [347]: http://commits.kde.org/kdenlive/c0d234edb758cf992f49695715cb8293185e29cb
 [348]: http://commits.kde.org/kdenlive/a78f8f783b490f3f1920b32282274246ce3a54b7

