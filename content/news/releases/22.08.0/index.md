---
author: Farid Abdelnour
date: 2022-08-22T05:00:02+00:00
aliases:
- /en/2022/08/kdenlive-22-08-released/
- /fr/2022/08/__trashed/
- /de/2022/08/kdenlive-22-08/
featured_image_bg: images/features/2208.png
---
The 22.08 release brings many user interface improvements and bug fixes for a smooth editing experience. Speaking of smooth, this version comes with many enhancements to the proxy clip generation system, resulting in a smoother interface and support for NVENC and VAAPI proxy clip encoding has been fixed.

Rendering now supports an experimental Parallel Processing feature for faster render speeds. This version adds support for importing AVIF, HEIF, HEIC and JPEG XL image formats, [LOTTIE][1] and RAWR animations, as well as VTT (Web Video Text Tracks) and SBV (YouTube) subtitle files. New features include an improved audio recording experience, global subtitle styling, exporting of guides as chapters for YouTube/PeerTube/Vimeo, and integration with [Glaxnimate][2] vector graphics animation program. Colorscopes (Waveform, Vectorscope, and RGB parade) are finally working on Windows.

The team would like to thank the code contributions from Eric Jiang, Nathan Hinton, Gary Wang, Marius Pa, Daniel Novomeský, Martin Owens, Brendan Davidson and Ivan Sudakov; and extend our gratitude to all the community members for reporting issues, creating tutorials and offering support.

## Subtitle Styling

An initial implementation of styling subtitles allows to modify the font, font size, font and outline colors, add shadows, set position and background color. More options are expected in the next releases.

![](subtitle-style.gif)

## Effects and Compositions

The main effects tab now displays all audio and video effects improving the search experience. This version also comes with new effects: Shear, Scroll, Photosensitivity, Monochrome, Median, Kirsch, Exposure, EPX Scaler, Color Temperature, Color Overlay, Color Correct, Color Contrast, Chroma Noise Reduction, Contrast Adaptive Sharpen, Bilateral and VR360 Equirectangular to Stereo.

The Luma composition now supports video matte files.

### Color Temperature effect
{{< video src="color-temp.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

### Luma matte video transitions
{{< video src="stinger-compositions.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

## Glaxnimate Integration and Lottie Support

This version comes with integration with the Glaxnimate vector graphics animation program and support for rawr (Glaxnimate) and lottie animation file formats.

{{< video src="glax-tut.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

## Export Guides as Chapters

This new feature allows you to use Guides as chapter description markers for YouTube, PeerTube or Vimeo. Simply generate the text in the desired format from the export guides interface, press the copy to clipboard button and paste it to the video description of your video. You may find all available export variables in the [documentation][3].

![](guides-export.png)

## Audio Recording

The audio recording interface and experience received an overhaul. The newly added microphone button in the mixer automatically activates the audio monitoring mode and sets up the selected track for recording. To start recording use the record button in the track head or use the spacebar on your keyboard. The spacebar also pauses and resumes recording, while the Esc key exits recording mode. Notice also the track head changes color for a better visual feedback.

![](rec.gif) 

Before recording the user will get a 3 second countdown-timer cue.

![](countdown.gif)

## User Interface

Every release comes with user interface improvements, some more visible than others. This version comes with an Improved clip-tagging system which allows you to easily add, edit and reorder tags in the project bin. The color of the monitor guide overlays can also be configured. A bug causing an oversized interface in the Windows version and missing or incorrectly colored icons in the AppImage has been fixed.

{{< video src="tags-ui.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

{{< video src="color-overlays.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

## Other fixes

  * Added the ability to change the subtitle framerate.
  * Make monitor overlay guides color configurable.
  * Implemented snapping in Ripple edit mode.
  * Track compositing is now a simple checkbox instead of the deprecated none/high res choice.
  * Implement unfinished auto subtitles mode (selected track / clip only).
  * Many same track transition fixes.
  * Speedup maker search.
  * Fixes to many platform specific issues in Windows, Mac, AppImage and Flatpak packages.

Go grab the latest version from our [download page][4] and, as usual, if you encounter any issues please [let us know][5]. You may also contact us in our [Telegram][6] and [Matrix][7] channels.

 [1]: https://lottiefiles.com/
 [2]: https://glaxnimate.mattbas.org/
 [3]: https://docs.kdenlive.org/en/cutting_and_assembling/guides.html?highlight=export+guides#export-guides-as-chapter
 [4]: https://kdenlive.org/en/download/
 [5]: https://kdenlive.org/en/bug-reports/
 [6]: https://t.me/kdenlive
 [7]: https://matrix.to/#/#kdenlive:kde.org
