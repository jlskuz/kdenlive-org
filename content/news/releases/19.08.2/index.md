---
title: Kdenlive 19.08.2 is out
author: Farid Abdelnour
date: 2019-10-11T08:00:34+00:00
aliases:
- /en/2019/10/kdenlive-19-08-2-is-out/
- /fr/2019/10/kdenlive-19-08-2-est-pret/
- /it/2019/10/kdenlive-19-08-2-disponibile/
- /de/2019/10/kdenlive-19-08-2-ist-freigegeben/
---

Kdenlive 19.08.2 is out with many goodies ranging from usability and user interface improvements all the way to fixes to speed effect bugs and even a couple of crashes. Automask effect gets back the possibility to store the tracking data to clipboard which then can be used for motion following by importing the tracking data into i.e transform effect.
