  * Fix crash on composition resize. [Commit.][1]
  * Update MSYS2 build script. [Commit.][2]
  * Fix Windows audio screen grab (#344). [Commit.][3]
  * Remove local reference to current project. [Commit.][4]
  * Disable multitrack view on render. [Commit.][5]
  * Fix clip duration incorrectly reset on profile change. Fixes #360. [Commit.][6]
  * Fix compile warnings. [Commit.][7]
  * Make affine filter bg color configurable. Fixes #343. [Commit.][8]
  * Fix speed job in some locales. Fixes #346. [Commit.][9]
  * Fix some remaining effectstack layout issues. [Commit.][10]
  * Fix keyframes not deleted when clip start is resized/cut. [Commit.][11]
  * Fix track effects not working when a clip is added at end of track or if last clip is resized. [Commit.][12]
  * Add clickable field to copy automask keyframes. Fixes #23. [Commit.][13]
  * Show track effect stack when clicking on it&#8217;s name. [Commit.][14]
  * Fix crash trying to access clip properties when unavailable. [Commit.][15]
  * Fix effectstack layout spacing issue introduced in recent commit. [Commit.][16]
  * Fix proxy clips lost on opening project file with relative path. [Commit.][17]
  * Update AppData version. [Commit.][18]
  * Cleanup effectstack layout. Fixes !58 #294. [Commit.][19]
  * Fix mixed audio track sorting. [Commit.][20] See bug [#411256][21]
  * Another fix for speed effect. [Commit.][22]
  * Speed effect: fix negative speed incorrectly moving in/out and wrong thumbnails. [Commit.][23]
  * Fix incorrect stabilize description. [Commit.][24]
  * Cleanup stabilize presets and job cancelation. [Commit.][25]
  * Deprecate videostab and videostab2, only keep vidstab filter. [Commit.][26]
  * Fix cancel jobs not working. [Commit.][27]
  * Fix some incorrect i18n calls. [Commit.][28]
  * Don&#8217;t hardcode vidstab effect settings. [Commit.][29]

 [1]: http://commits.kde.org/kdenlive/6d8c7cfbedcd9feae08ad2575da42157a618560f
 [2]: http://commits.kde.org/kdenlive/728844d8da885208ea2ff14051b6f0fe05865529
 [3]: http://commits.kde.org/kdenlive/01322357e608e984b9e51986be5926704f21bcdd
 [4]: http://commits.kde.org/kdenlive/c1c39a936f3abcb336f707b0b04f9ebae6fd1d7e
 [5]: http://commits.kde.org/kdenlive/1c804123d60948bf5e65272453467d16fd8dd620
 [6]: http://commits.kde.org/kdenlive/d02863d2b4b647dd3485f39dcca3ffe0c2718307
 [7]: http://commits.kde.org/kdenlive/56536237259f0ef154430db8a46b4e82a5a6d24a
 [8]: http://commits.kde.org/kdenlive/1553e308c793fbd3de1456dd1ee8e11bb27d060c
 [9]: http://commits.kde.org/kdenlive/dd928e87f6e77946609cf3097dd7ee53c4bfd255
 [10]: http://commits.kde.org/kdenlive/4fe87ef07851e3c86493ed65f9b11c8ebf0b5964
 [11]: http://commits.kde.org/kdenlive/83a4ab9a760ee492dcfa392120274fa111babfa8
 [12]: http://commits.kde.org/kdenlive/4cd6df703cd6258636030b251e91ebb16223a7aa
 [13]: http://commits.kde.org/kdenlive/21382f010f2f57eadb6a1caf3b7550c9017645ef
 [14]: http://commits.kde.org/kdenlive/1bfca966cb6096ee5977fdb58297559c84ef3b3d
 [15]: http://commits.kde.org/kdenlive/61d2b8508d849ff91ff952a5ca768dceaf6c6764
 [16]: http://commits.kde.org/kdenlive/dfe4b56c3c372603f63131bfb799f917aea2c1f9
 [17]: http://commits.kde.org/kdenlive/dd9cfe332784fac10e826f07bb4f8b58eedcd6c2
 [18]: http://commits.kde.org/kdenlive/c57414c40a6378625ae870343540ffaff6bc7238
 [19]: http://commits.kde.org/kdenlive/9cc83e804bac6ee803dddb45361224a2a9e96151
 [20]: http://commits.kde.org/kdenlive/15ddd416647f4727ce6bb3bad39cc6500c78b272
 [21]: https://bugs.kde.org/411256
 [22]: http://commits.kde.org/kdenlive/0e5b1d7e2f969d880e4618ca1af60823895db6b6
 [23]: http://commits.kde.org/kdenlive/c03ccad4960bd0ea6d3c03f1ad41f1ff76c21838
 [24]: http://commits.kde.org/kdenlive/e716e00e40ca941123324056a034c7398e083dcc
 [25]: http://commits.kde.org/kdenlive/2e5007fd6546b17fc547b61e787f71c951e547c8
 [26]: http://commits.kde.org/kdenlive/3ed27aacfc1e97ff5df8fe1214880681e8401fe0
 [27]: http://commits.kde.org/kdenlive/17a7e6f4088d71c3d7989210175aa8c440bbf5d3
 [28]: http://commits.kde.org/kdenlive/8aa5a69b2188cf991102dd427bfeed0e66d19656
 [29]: http://commits.kde.org/kdenlive/5c22efbbbebd686ba194d503100f4e629c202c89
