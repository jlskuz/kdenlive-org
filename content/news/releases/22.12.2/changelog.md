  * Fix crash deleting a subtitle clip. [Commit.][1]
  * Fix scene split job does not save sub clips. [Commit.][2]
  * Fix monitor qml overlay painting corruption with Qt 5.15.8. [Commit.][3]See bug [#464027][4]
  * Don&#8217;t unnecessarily double check track duration on clip move. [Commit.][5]

 [1]: http://commits.kde.org/kdenlive/a9ff26d06812e574d7e98040e4bb8be2cb53726c
 [2]: http://commits.kde.org/kdenlive/80f5c87c9e4ecc4d80003e2f254dd3f2d614a17a
 [3]: http://commits.kde.org/kdenlive/2c6eeac036f8243363df6da9314f3fa0b8ee0834
 [4]: https://bugs.kde.org/464027
 [5]: http://commits.kde.org/kdenlive/2f8715c7668be63ccd2f7eef4621d345661a4b97
