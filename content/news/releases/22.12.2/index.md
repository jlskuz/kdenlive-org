---
author: Farid Abdelnour
date: 2023-02-09T19:02:06+00:00
aliases:
- /en/2023/02/kdenlive-22-12-2-released/
- /it/2023/02/kdenlive-22-12-2-3/
- /de/2023/02/kdenlive-22-12-2-2/
---
Kdenlive 22.12.2 is out with the following fixes:

