---
date: 2009-04-15
author: Jean-Baptiste Mardelle
---

![](0.7.3.png)

We are glad to announce the release of Kdenlive 0.7.3

Lots of bugs were fixed in this release, including some crashes triggered in Qt 4.5 and a lot of cleanup was done in the code.

## The new features are:

- Clip grouping
- Creation of rendering scripts for delayed rendering
- Double pass encoding for rendering (just add "pass=2" to your profile)
- Track locking
- Configurable monitor background color for better previewing
- Web updates: you can now download new rendering profiles from Kdenlive's web site
- Split audio, allows you to separate a clip in it's audio and video parts
- Improved compatibility with Kdenlive 0.5 project files

## Get Kdenlive

You can download Kdenlive source code from our [Sourceforge page](https://sourceforge.net/project/platformdownload.php?group_id=46786), and see the [compilation instructions](https://web.archive.org/web/20160318183953/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive/installing-source). As usual, [binary packages](https://web.archive.org/web/20160318183953/http://kdenlive.org/user-manual/downloading-and-installing-kdenlive/pre-compiled-packages) will be announced in our pages when available.

Kdenlive uses the [MLT framework](https://mltframework.org/) for all video processing. **We recommend that you use the latest MLT version ( 0.3.8) for a better compatibility.**

Thanks to everyone who contributed to that release by testing, giving feedback and providing patches.

Jean-Baptiste Mardelle
