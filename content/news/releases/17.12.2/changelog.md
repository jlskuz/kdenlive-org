* Fix compatibility with MLT >= 6.6.0. [Commit.](http://commits.kde.org/kdenlive/92b88c61ac552b74fa9e8a8fcf2ed1bf100cdf4a) 
* Fix build with Qt 5.6. [Commit.](http://commits.kde.org/kdenlive/94c420c85b55d5fabeebc38984f0afcbe3cc9e93) 
* Fix backwards seeking not stoping when reaching clip start. [Commit.](http://commits.kde.org/kdenlive/ea185dfce692382abe3cb4ab3450db7774237553)
* Add release version number to AppData. [Commit.](http://commits.kde.org/kdenlive/3bc52cf369958258b5d3483c52b13a3bfac54f3f) 
* Don't query available widget styles on each startup. [Commit.](http://commits.kde.org/kdenlive/41a9199800c370b8077083ab04d39d567e660da7)
* Fix webM encoding (auto replace vorbis by libvorbis if installed). [Commit.](http://commits.kde.org/kdenlive/5de2633cc59424eb64968534d01ebe4e9186830f) Fixes bug [#388503](https://bugs.kde.org/388503)

