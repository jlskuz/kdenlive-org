* Fix typo x246 > x264, patch by Nicolas F. [Commit.](http://commits.kde.org/kdenlive/ea07ce9d67d7049b2b4aeb4e75b84fe953008243) 
* Fix inverted colors in scopes. [#359541](https://bugs.kde.org/359541)
