---
author: Jean-Baptiste Mardelle
date: 2013-04-07T08:49:00+00:00
aliases:
- /users/j-b-m/kdenlive-096-released
- /discover/0.9.6
# Ported from https://web.archive.org/web/20160323112418/https://kdenlive.org/users/j-b-m/kdenlive-096-released
# Ported from https://web.archive.org/web/20130409152955/http://www.kdenlive.org/discover/0.9.6
---


![](kdenlive096d.png)

We are happy to announce the immediate release of Kdenlive 0.9.6.

This version fixes several bugs and crashes, including a very annoying bug that caused project files to seem corrupted. All users are strongly encouraged to upgrade.

You will find detailled infos about the changes in this release below.

The source code can be downloaded from the [KDE][2] servers, there are 2 available tarballs, since we decided to also distribute Kdenlive with its [user manual][3]:

* [0.9.6 source code only (5M)][4]
* [0.9.6 source code including documentation (19M)][5]

Packages for your distributions should hopefully be available soon.  
It is recommended to use the latest version of the [MLT framework][6] (0.8.8) to get the recent bugfixes and improvements.

As usual, a big thank you to all those who helped us make this release better by giving us feedback and investigating the bugs they found.  
You can check the Kdenlive [online manual][3] and [forums][7] for documentation and help.

For the Kdenlive team,

Jean-Baptiste Mardelle

## New Features

* Added a Reverse clip option to Clip Jobs that creates a backwards clip
* The list of audio / video bitrates can now be customized in custom rendering profiles

## The solved issues in this version are:

* Fix keyframes when cutting a clip / undoing a clip cut
* Warn before overwriting .mlt stabilized file
* Fix monitor confusion (clip monitor sometimes playing timeline,...)
* Fix the Mono to Stereo effect UI
* Fix proxy of playlist having wrong aspect ratio in some locales
* Fix transition widget not correctly updated when resizing a transition
* Fix DVD chapters broken when using an intro movie
* Fix error message (No matching profile) poping up in render widget when everything was ok
* Fix clip keyframes not showing on project load
* Fix bug when moving guide (was not moving to the correct place)
* Fix project corruption (wrong character) caused by some clip's metadata
* Fix possible crash on track deletion
* Fix timeline corruption when using spacer tool or overwrite edit mode
* Fix possible crash when editing speed effect
* Fix transition losing all its properties when moved
* Fix crash when pressing del when editing animation in title widget
* Fix crash when doing quick clip resize
* Fix corruption when groups where overlapping
* Fix corruption when adding a title clip where a transition already existed
* Fix timeline preview corruption with some transitions

  [2]: https://www.kde.org/
  [3]: https://docs.kdenlive.org
  [4]: https://download.kde.org/stable/kdenlive/0.9.6/src/kdenlive-0.9.6.tar.bz2.mirrorlist
  [5]: https://download.kde.org/stable/kdenlive/0.9.6/src/kdenlive-0.9.6-with-doc.tar.bz2.mirrorlist
  [6]: https://www.mltframework.org/
  [7]: https://discuss.kde.org/tag/kdenlive
