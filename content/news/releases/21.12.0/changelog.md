  * Move default lut value to proper function. [Commit.][3]
  * Fix last commit always resetting lut file to first installed one on project load. [Commit.][4]
  * Ensure lut effect is initialized with a file when added. [Commit.][5]
  * Fix inconsistencies in subtitle model leading to broken (uneditable) items. [Commit.][6]
  * Don’t allow typewriter effect on clip other than text (caused a freeze). [Commit.][7]
  * After extract operation, seek cursor to in point. [Commit.][8]
  * Fix possible crash on rotoscoping edit. [Commit.][9]
  * Fix crash on misconfigured wipe composition. [Commit.][10] Fixes bug [#446203][11]
  * Keyframe import fixes: crash importing to effect rect without opacity, fix motion tracker exporting with opacity=0, fix limiting keyframes messing import. [Commit.][12]
  * Fix crash importing empty keyframes. [Commit.][13]
  * Update 21.12 splashscreen. [Commit.][14]
  * Fix keyframe selection on add. [Commit.][15]
  * Fix add keyframe does not selected. [Commit.][16]
  * Fix extract frame using monitor preview resolution. [Commit.][17]
  * Fix pasting items in timeline to use mouse / cursor position depending on how it is called menu / shortcut. [Commit.][18]
  * Spacer tool: Don’t allow independant move of grouped items. [Commit.][19] See bug [#443324][20]
  * Fix tool label width in statusbar. [Commit.][21]
  * Fix crash moving clip with mixes in insert/overwrite mode. [Commit.][22]
  * Fix group move with mix sometimes broken. [Commit.][23]
  * Fix errors/crash in insert mode (lift/extract) with mixes. [Commit.][24]
  * Fix crash using spacer tool on grouped clips with a clip in the group positioned before spacer start operation. [Commit.][25] Fixes bug [#443324][20]
  * Fix mix cut pos lost when switching mix composition. [Commit.][26]
  * Luma transition: add option to control alpha channel (fixes transition on clips with different aspect ratios). [Commit.][27]
  * Insert/overwrite mode: delete mixes on clip move. [Commit.][28]
  * Fix end resize bug. [Commit.][29]
  * Fix cannot move clip left when only 1 frame space. [Commit.][30]
  * Remove useless string duplication. [Commit.][31]
  * Fix blank length calculation allowing incorrect 1 frame overlap in some cases. [Commit.][32]
  * Fix crash on undo mix cut. [Commit.][33]
  * Fix left resize regression. [Commit.][34]
  * Fix right mouse click behavior on monitor when effect scene is displayed. [Commit.][35]
  * Another round of mix resize issues, with added tests. [Commit.][36]
  * Another fix for mix resize corruption. [Commit.][37]
  * Fix another clip marker issue introduced with old code for Ripple. [Commit.][38]
  * Fix some mix resize issues allowing to create invalid mixes. [Commit.][39]
  * Fix broken find/replace breaking timeline clip markers display. [Commit.][40]
  * Fix some 1 frame clip mix incorrectly detected as invalid. [Commit.][41]
  * Appstream Data: the manual is now at <https://docs.kdenlive.org>. [Commit.][42]
  * Extract frame from timeline monitor now correctly disables proxy to create a full res image. [Commit.][43]
  * Fix MLT api change causing startup crash on movit (Movit still not usable). [Commit.][44] See bug [##442880][45]
  * Track name edit: fix focus issue, enable F2 shortcut. [Commit.][46] Fixes bug [#440185][47]
  * “Go to clip start/end” should use clip under cursor if none is selected. [Commit.][48] Fixes bug [#440024][49]
  * Fix transcoding of title clips. [Commit.][50]
  * Typewriter effect should not be blacklisted!. [Commit.][51] Fixes bug [#445232][52]. See bug [#436113][53]
  * Fix “Select Transition” should select mixes too. [Commit.][54] Fixes bug [#440023][55]
  * Fix display of timeline usage in clip monitor. [Commit.][56]
  * Show timeline usage in clip monitor. [Commit.][57]
  * Fix default project path ignored on creating new project. [Commit.][58] Fixes bug [#444595][59]
  * Fix warning. [Commit.][60]
  * Fix audio/video only drag from bin. [Commit.][61]
  * Hide audio mix from transitions list in same track and composition stack. [Commit.][62]
  * Fix possible crash in url effect parameter. [Commit.][63]
  * Fix crash on close. [Commit.][64]
  * Fix video only clips displaying audio icon. [Commit.][65]
  * Allow closing secondary bin. [Commit.][66]
  * Also display usage icon on non AV clips. [Commit.][67]
  * Improve visibilty of bin clip usage (colored icons). [Commit.][68]
  * Bin icon view: clips used in timeline use bold font for name. [Commit.][69]
  * Bin icon view: make audio /video drag icons more visible on hover. [Commit.][70]
  * Switch multicam view to qtblend composition to avoid monitor preview scaling issues. [Commit.][71]
  * Multiple bins: put folder name as widget title, enable up button and double click to enter folder in secondary bins. [Commit.][72]
  * Added UI for the frei0r_transparency effect. [Commit.][73]
  * Hide secondary bin title bar. [Commit.][74]
  * Fix incorrect QList insert. [Commit.][75]
  * Fix render time overlapping text button. [Commit.][76]
  * Attempt to fix slideshow detection on Windows. [Commit.][77]
  * Remove old custom function to append shortcut to action tooltip, causing shortcuts to appear twice. [Commit.][78]
  * Keyframe import: display user friendly param name in combobox, fix import of opacity. [Commit.][79]
  * Fix affine (Transform) opacity is now in the 0-1 range with MLT7, fix cairo affine blend composition default params. [Commit.][80]
  * Disable duplicate keyframe when cursor is over a keyframe. [Commit.][81]
  * Ctrl+A in bin will select all items in current folder. Switching from tree view to icon view keeps selection. [Commit.][82]
  * Allow seeking by clicking on zoombar when not zoomed. [Commit.][83]
  * Fix keyframe incorrectly moved when attempting to seek in keyframe view. [Commit.][84]
  * Titler: prevent selecting inexisting font. [Commit.][85]
  * Fix some issues in multiple bin. [Commit.][86]
  * Multi bin: activate through folder context menu event, dock new bins with the main bin. [Commit.][87]
  * Ripple: several fixes. [Commit.][88]
  * Add ripple to tool and menu bar. [Commit.][89]
  * Fixes for Slip and add Ripple (not feature complete yet). [Commit.][90]
  * Allow creating multiple Project bin widgets. [Commit.][91]
  * Mixes: switch to slider to adjust mix position in settings. [Commit.][92]
  * Fix mix duration indicated 1 frame less than real. [Commit.][93]
  * Fix same track indicator on composition drop not showing correctly. [Commit.][94]
  * Remove debug. [Commit.][95]
  * Mixes: add button in composition list widget to display only transitions. Dragging a transition over a clip cut between 2 clips will add a same track mix. [Commit.][96]
  * When resizing an unaligned mix, increase/decrease left/right side alternatively for better result. [Commit.][97]
  * Fix audio mix duration / position incorrect in mix widget. [Commit.][98]
  * Add a position widget to mix parameters to allow sliding the mix. [Commit.][99]
  * Fix some mix resize issues. [Commit.][100]
  * Mix resizing: allow to go down to 1 frame. [Commit.][101]
  * Fix moving clip with mixes on same track. [Commit.][102]
  * Fix mix tests. [Commit.][103]
  * Fix crash introduced with a29dcd7f. [Commit.][104]
  * Resize Mix left side: limit to previous clip length, fix undo corruption. [Commit.][105]
  * Fix recent mix regression. [Commit.][106]
  * Fix Bin hover thumbnails in icon view mode. [Commit.][107]
  * Fix incorrect mix deletion when moving a mixed clip on same track. [Commit.][108]
  * Fix crash on mix undo. [Commit.][109]
  * Fix clazy warnings. [Commit.][110]
  * Fix monitor play zone. [Commit.][111]
  * Fix timeline corruption on title clips resize. [Commit.][112]
  * Fix crash undoing clip move with mix. [Commit.][113]
  * Fix corruption introduced in recent mix change. [Commit.][114]
  * Prevent possible crash closing project with a track effect. [Commit.][115]
  * Fix crash on mix undo. [Commit.][116]
  * Keyframes: moving a keyframe in timeline will also move other selected keyframes. [Commit.][117]
  * Fix wipe and slide compositions broken on resize. [Commit.][118]
  * Fix minor typo. [Commit.][119]
  * Add tooltips on keyframe hover. [Commit.][120]
  * Fix incorrect collision detection with clip having mixes. [Commit.][121]
  * Add setting to choose preferred track compositing composition. [Commit.][122]
  * Timeline clip drop: add id to each drag operation to avoid incorrectly interpreting a new drag operation as the continuation of a previous one. [Commit.][123]
  * Fix muting audio master broken. [Commit.][124]
  * Fix keyframes test. [Commit.][125]
  * Refactor keyframe selection, now in sync between timeline and effect stack. [Commit.][126]
  * Fix various mix resize/align issues. [Commit.][127]
  * Fix proxy clips not archived when requested. [Commit.][128]
  * Fix wipe and slide transition incorrect behavior on resize, and incorrectly detecting “reverse” state. [Commit.][129]
  * Fix composition startup check. [Commit.][130]
  * Updated kdenlivedefaultlayouts.rc. [Commit.][131]
  * Fix same track transition if one clip has no frame at its end. [Commit.][132]
  * Fix crash and incorrect resize with same track transitions. [Commit.][133]
  * Fix mix cut position lost on paste. [Commit.][134]
  * Fix one cause of crash related to multiple keyframes move. [Commit.][135]
  * Fix minor typo. [Commit.][136]
  * Fix proxying of playlist clips. [Commit.][137]
  * Add optionto put job clips in the same folder as original instead of a subfolder like “Stabilized”. [Commit.][138]
  * Switch track compositing to frei0r.cairoblend by default instead of qtblend. [Commit.][139]
  * Fix replacing proxied clip. Related to #1216. [Commit.][140]
  * When a clip job creates an mlt playlist, check if the file is already in project to avoid double insertion. [Commit.][141]
  * On project load, detect possible corruptions related to same track transitions, fix them if possible and log changed / problems in project notes. [Commit.][142]
  * Project notes: when adding a timeline reference timecode, also include current track info so we can make notes on specific tracks. [Commit.][143]
  * Added the CAPS plugins category. [Commit.][144]
  * Correctly rearranged all the LSP plugins in the proper folder. [Commit.][145]
  * Blacklisted the ladspa which come with MLT that are totally borken. [Commit.][146]
  * Updated frei0r_cartoon.xml fixes issue 1221. [Commit.][147]
  * Updated avfilter_highpass.xml. [Commit.][148]
  * Add some debug info for clip move errors. [Commit.][149]
  * Fix clip with mix cannot be moved back in place. [Commit.][150]
  * Fix loop mode broken on add effect. [Commit.][151]
  * Fix replacing AV clip with playlist clip broken. [Commit.][152]
  * Another attempt to fix fullscreen monitor going to wrong display. [Commit.][153]
  * Fix export frame broken for title clips. [Commit.][154]
  * Fix bin thumbnail hover seek not reset when leaving thumb area. [Commit.][155]
  * Add prefix support for external proxies required for GoPro footage. [Commit.][156]
  * Kdenlive display settings: allow to define on which physical monitor the fullscreen monitor view will show up. [Commit.][157]
  * Project bin:when hover seek is enabled, restore thumb after seeking, set thumb with shift+seek. [Commit.][158]
  * Updated frei0r_softglow.xml. [Commit.][159]
  * Fix crash loading project with incorrectly detected same track transition. [Commit.][160]
  * Add GPL3 license. [Commit.][161]
  * Silence up scripty warnings. [Commit.][162]
  * Added to kdenliveeffectscategory.rc other ladspa collections available in software repositories. [Commit.][163]
  * Nightly Flatpak: add ladspa noise supressor effect and some updates. [Commit.][164]
  * Update kdenliveeffectscategory.rc with new categories for audio effects. [Commit.][165]
  * Added new 4 xml UI for hiding some audio effects. [Commit.][166]
  * Updated versions of the blacklisted_effects.txt and kdenliveeffectscategory.rc. [Commit.][167]
  * Slightly clean startup debug log. [Commit.][168]
  * Update CMakeLists.txt. [Commit.][169]
  * Update CMakeLists.txt. [Commit.][170]
  * Update CMakeLists.txt. [Commit.][171]
  * Delete librnnoise\_ladspa.xml because it’s duplicated as ladspa\_librnnoise in the ladspa folder. [Commit.][172]
  * Uploaded new xml UI for audio effects. [Commit.][173]
  * Motion tracker: don’t switch to real time analysis on reset, add None type to blur and make it default. [Commit.][174]
  * Nightly Appimage: add wayland socket. [Commit.][175]
  * Update CMakeLists.txt. [Commit.][176]
  * Update CMakeLists.txt. [Commit.][177]
  * Update CMakeLists.txt. [Commit.][178]
  * Update CMakeLists.txt. [Commit.][179]
  * Update CMakeLists.txt. [Commit.][180]
  * Update CMakeLists.txt. [Commit.][181]
  * Update CMakeLists.txt. [Commit.][182]
  * Upload New File frei0r_R.xml. [Commit.][183]
  * Upload New File frei0r_G.xml. [Commit.][184]
  * Upload New File frei0r_B.xml. [Commit.][185]
  * Update avfilter_vectorscope.xml by adding (Advanced) to the name. [Commit.][186]
  * Update frei0r_pr0file.xml by adding (Advanced) to the name. [Commit.][187]
  * Update frei0r_lenscorrection.xml adding (keyframable) to the effect name. [Commit.][188]
  * Update avfilter_negate.xml change name to Negate and add info about the option of inverting the alpha channel in the description. [Commit.][189]
  * Update gamma.xml change the name to Gamma (keyframable). [Commit.][190]
  * Update avfilter_selectivecolor.xml. [Commit.][191]
  * Update chroma_hold.xml change name to the effect to Color Keep. [Commit.][192]
  * Ensure we don’t end up with a dead connection activating a monitor. [Commit.][193]
  * Update avfilter_boxblur.xml changed effect name and parameters description. [Commit.][194]
  * Fix motion tracker broken on reset or subsequent analyse requests, remove Boosting algorithm (not ported to MLT’s filter). [Commit.][195]
  * Make color wheel (“lift/gamma/gain” effect) keyframable. [Commit.][196] See bug [#393668][197]
  * Update CMakeLists.txt. [Commit.][198]
  * Upload New File avfilter_deesser.xml. [Commit.][199]
  * Upload New File avfilter_dcshift.xml. [Commit.][200]
  * Update CMakeLists.txt. [Commit.][201]
  * Upload New File avfilter_crystalizer.xml. [Commit.][202]
  * Upload New File avfilter_crossfeed.xml. [Commit.][203]
  * Upload New File avfilter_compensationdelay.xml. [Commit.][204]
  * Upload New File avfilter_compand.xml. [Commit.][205]
  * Update CMakeLists.txt. [Commit.][206]
  * Upload New File avfilter_bs2b.xml. [Commit.][207]
  * Upload New File avfilter_bass.xml. [Commit.][208]
  * Update avfilter_bandreject.xml. [Commit.][209]
  * Upload New File avfilter_bandpass.xml. [Commit.][210]
  * Upload New File avfilter_bandpass.xml. [Commit.][211]
  * Upload New File avfilter_apulsator.xml. [Commit.][212]
  * Update CMakeLists.txt added 5 new xml. [Commit.][213]
  * Upload New File avfilter_aphaser.xml. [Commit.][214]
  * Upload New File avfilter_allpass.xml. [Commit.][215]
  * Upload New File avfilter_alimiter.xml. [Commit.][216]
  * Upload New File avfilter_acrusher.xml. [Commit.][217]
  * Upload New File avfilter_acontrast.xml. [Commit.][218]
  * Fix stabilize and speed jobs not starting. [Commit.][219]
  * Update channelcopy.xml fix the ID which creates 2 copy of the same effect. [Commit.][220]
  * Fix remove space in all tracks with locked tracks. [Commit.][221]
  * Reorder and clean up data dir. [Commit.][222]
  * Titler: deselect start viewport when “Edit end viewport” gets triggered. [Commit.][223]
  * Titler: add keyboard modifiers to limit itme movement on one axis. [Commit.][224]
  * Clip import: fix different framerate warning was never shown. [Commit.][225]
  * Fix spacer track sometimes not allowing to reduce space. [Commit.][226]
  * Improvements on titler animation. [Commit.][227]
  * Update motion tracker to display keyframes directly in the effect and make them editable. Requires latest MLT git. [Commit.][228]
  * Update frei0r_softglow.xml by fixing parms values and adding the missing Blur parameter. [Commit.][229]
  * Improve cursor shape for keyframe view in timeline. [Commit.][230]
  * Keyframe import dialog: disable UI elements if it doesn’t make sense. [Commit.][231]
  * Make it possible to copy and import rotoscoping keyframes. [Commit.][232] Fixes bug [#442372][233]
  * Fix install after 7befb02673f46de246ef18aad25ecc37b8969f4c. [Commit.][234]
  * Remove unused prehistoric banner. [Commit.][235]
  * Drop MLT’s composite transition for Slide transition. [Commit.][236]
  * Updating license version in about box. [Commit.][237]
  * Fix crash on keyframe import of 1 dimensional params. [Commit.][238]
  * Remove broken “duplicate bin clip with timeremap” stuff that caused crash dropping playlists in timeline. [Commit.][239] Fixes bug [#441777][240]
  * Fix color picker in multiscreen config. [Commit.][241]
  * Fix monitor zoom affecting titler background frame. [Commit.][242]
  * Monitor config: add option to disable progressive playback. [Commit.][243]
  * Ensure we always use UTF-8 enconding when writing files. [Commit.][244]
  * Previous commit: Load current color properly to UI. [Commit.][245]
  * Add option to select clip monitor background color. [Commit.][246]
  * Startup crash detection: make the check later so that we can also detect movit crash and propose to reset the config file. [Commit.][247]
  * When editing / creating a render profile, focus the edited profile on save. [Commit.][248]
  * Filter tasks: fix encoding issue breaking job (stabilize, motion tracker). [Commit.][249]
  * Improve color accuracy of preview (backported from Shotcut). [Commit.][250]
  * Fix fake rect parameter not updating monitor overlay (alhpashape, corners) when changing value in effect stack. [Commit.][251]
  * Move avfilter_loudnorm.xml to the correct place (avfilter dir). [Commit.][252]
  * Clear effect xmls by moving frei0r into a seperate folder. [Commit.][253]
  * Fix adjust to frame size option in transform/position & zoom compositions. [Commit.][254]
  * Motion tracker: make “pixelate” and “opaque fill” options in the blur type list instead of having extra parameters for them. [Commit.][255]
  * Header SPDX harmonisation. [Commit.][256]
  * Motion tracker: add pixelate option (requires latest MLT git). [Commit.][257]
  * Tiny details in build instructions. [Commit.][258]
  * KDE standard for headers. [Commit.][259]
  * Fix color picker incorrectly selecting a rect zone after first use. [Commit.][260]
  * Fix compilation (strange incorrect char encoding). [Commit.][261]
  * Add contexts to avoid disambiguation with the form without plural. [Commit.][262]
  * Improve version check of previous commit. [Commit.][263]
  * First implementation of KAboutComponents. [Commit.][264]
  * Add missing file and update licences. [Commit.][265]
  * Fix install on macOS. [Commit.][266]
  * Temporarily revert use of QML KNewStuff dialog on windows. [Commit.][267]
  * Various fixes for motion tracker effect, add the new DaSIAM model, requires to download some extra model files. [Commit.][268]
  * Update .gitignore. [Commit.][269]
  * Fix compositions hidden when top clip had a same track transition. [Commit.][270]
  * Update kdenliveeffectscategory.rc Moved audiowaveform on the ON MASTER group. [Commit.][271]
  * Update audiowaveform.xml. [Commit.][272]
  * Fix same track transition erratic resize. [Commit.][273]
  * Undo the previous commit because the right effect was already available. [Commit.][274]
  * Update blacklisted_effects.txt Fieldorder was blacklisted for a mistake and it’s useful to manage interlaced fields. [Commit.][275]
  * Don’t allow to toggle multitrack view if multicam tool is active. [Commit.][276]
  * Multicam tool: perform operation on keyboard track selection too. [Commit.][277]
  * More hamonisation of headers. [Commit.][278]
  * Fix typo. [Commit.][279]
  * Fix possible crash on incorrect active effect. [Commit.][280]
  * Update avfilter_xbr.xml. [Commit.][281]
  * Update avfilter_hqx.xml. [Commit.][282]
  * Ensure bin audio thumbnails are loaded on project open. [Commit.][283]
  * Import keyframes: live preview of the positioning in the monitor. [Commit.][284]
  * Keyframe import: add extra align options and manual offset. [Commit.][285]
  * Fix title widget background frame not showing up. [Commit.][286]
  * Some additional header harmonisation &#8211; more to come. [Commit.][287]
  * Update CMakeLists.txt. [Commit.][288]
  * Upload Cairogradient was missing several useful parameters. [Commit.][289]
  * Applying licensedigger –prettyheader. [Commit.][290]
  * Adjust monitor toolbar icon size. [Commit.][291]
  * Try using correctly themed icons for monitor toolbars. [Commit.][292]
  * Ensure monitor toolbar buttons don’t steal focus. [Commit.][293]
  * Fix spacer tool corruption when applied on clips with same track transition. [Commit.][294]
  * Fix copy/paste of clips with same track transitions. [Commit.][295]
  * Find MLT on Mac, add debug. [Commit.][296]
  * Apply 23 suggestion(s) to 23 file(s). [Commit.][297]
  * Fix possible crash on document open. [Commit.][298]
  * Instructions to build not in wiki anymore. [Commit.][299]
  * Crop effect: use project resolution by default(solves proxy issue). [Commit.][300] Fixes bug [#408235][301]
  * Clarifying licensing &#8211; part 1. [Commit.][302]
  * Apply 11 suggestion(s) to 11 file(s). [Commit.][303]
  * Merging post rebase. [Commit.][304]
  * Restraining licences to compatible subsets for harmonizing. [Commit.][305]
  * Applying replace_hearder script. [Commit.][306]
  * Applying licensedigger. [Commit.][307]
  * Adding missing copyright and licence headers. [Commit.][308]
  * Unification of LGPL* files. [Commit.][309]
  * Fix bug and crash in keyframe apply value to selected keyframes. [Commit.][310]
  * Restraining licences to compatible subsets for harmonizing. [Commit.][311]
  * Pressing Escape will revert to the select tool, and clear current selection if select tool is active. [Commit.][312]
  * Fix fade to alpha broken with MLT-7. [Commit.][313]
  * Update mask\_start\_frei0r_select0r.xml. [Commit.][314]
  * Update CMakeLists.txt. [Commit.][315]
  * Uploaded gpstext.xml. [Commit.][316]
  * Update kdenliveeffectscategory.rc. [Commit.][317]
  * Update kdenliveeffectscategory.rc. [Commit.][318]
  * Update mask\_start\_frei0r_select0r.xml. [Commit.][319]
  * Update blacklisted_effects.txt. [Commit.][320]
  * Add a mask_start version of frei0r.select0r for secondar color. [Commit.][321]
  * Update CMakeLists.txt. [Commit.][322]
  * Upload New File avfilter_loudnorm.xml. [Commit.][323]
  * Make it possible to change selection in slip mode. [Commit.][324]
  * Add (“View”) menu entry for “Open Command Bar”. [Commit.][325]
  * Add option to ignore subfolder structure on import. [Commit.][326]
  * Applying replace_hearder script. [Commit.][327]
  * Applying licensedigger. [Commit.][328]
  * Adding missing copyright and licence headers. [Commit.][329]
  * Fix ‘abort/remove job’ button toggling. [Commit.][330]
  * Licence texts are now in LICENSES directory not COPYING file. [Commit.][331]
  * Clarifying license in README and metadata. [Commit.][332]
  * Removing GPL-2.0 license text. [Commit.][333]
  * Adding licence texts following REUSE specification. [Commit.][334]
  * Remove unused “Trim mode” action. [Commit.][335]
  * Stop trimming mode (internal) only if it is running. [Commit.][336]
  * Invert option switch, fix Dbus ON mode, abort job in OFF. [Commit.][337]
  * Licence texts are now in LICENSES directory not COPYING file. [Commit.][338]
  * Fix seek to guide on click. [Commit.][339]
  * Fix undo effect change was restoring incorrect parameter. [Commit.][340]
  * Use localserver/localsocket rather than DBus (Mac, Windows). [Commit.][341]
  * Clip properties audio gain: don’t add several audio gain effects. [Commit.][342]
  * Multitrack tool: don’t stop playing on lift operation, don’t touch audio tracks. [Commit.][343]
  * Add missing file from previous commit. [Commit.][344]
  * Add multicam tool allowing to lift tracks by clicking in the project monitor’s track view. [Commit.][345]
  * Improve slip behavior for clips on locked tracks. [Commit.][346]
  * Add display names for mlt’s dynamical generated luma files. [Commit.][347]
  * Fix render name incorrectly kept in some cases after save as. [Commit.][348]
  * Allow to slip only non-endless clips (no compositions etc.). [Commit.][349]
  * Fix signal connection broke by 81ddab103f09a5ef827191d841d571ddcbe26e62. [Commit.][350]
  * Make sure slip indicator is always visible in slip mode. [Commit.][351]
  * Fix some clang & clazy warnings. [Commit.][352]
  * UI Config: Increase version to apply recent change. [Commit.][353]
  * Add advanced trimming tool: Slip. [Commit.][354]
  * Fix scene detection job (should now work on Windows). [Commit.][355]
  * Don’t allow importing a project cache folder (audio/video thumbs, proxy,…). [Commit.][356]
  * Appimage: abort if missing frei0r, avformat, vidstab or rubberband modules, warn on missing openCV and translations. [Commit.][357]
  * Compile MLT with GCC 9 &#8211; fixes brightness effect corruption. [Commit.][358]
  * Clarifying license in README and metadata. [Commit.][359]
  * Removing GPL-2.0 license text. [Commit.][360]
  * Adding licence texts following REUSE specification. [Commit.][361]
  * Fix mix crossfade sometimes using wrong order (starting at 100% and ending at 0%) instead of reverse. [Commit.][362]
  * Fix cache param mistake. [Commit.][363]
  * Fix tests randomly failing. [Commit.][364]
  * Fix string of previous commit. [Commit.][365] See bug [#440218][366]
  * Add a tooltip to the track effects toggle button in the track header. [Commit.][367] Fixes bug [#440218][366]
  * Fix build with -DCRASH\_AUTO\_TEST=ON. [Commit.][368] Fixes bug [#440414][369]
  * Update catch.hpp. [Commit.][370] See bug [#440867][371]
  * Clip properties: add b frame info (yes or no). [Commit.][372]
  * Fix timeline preview broken in recent change. [Commit.][373]
  * Fix QWheelEvent::position() not available in Qt < 5.14. [Commit.][374]
  * Fix more clang-tidy/clazy warnings. [Commit.][375]
  * Fix a possible build error and some clazy warnings. [Commit.][376]
  * Fix minor typo. [Commit.][377]
  * Cleanup code after previous commit. [Commit.][378]
  * Replace QRegExp (removed in Qt6) by QRegularExression. [Commit.][379]
  * Remove unecessary code introduced with 42b10c96. [Commit.][380]
  * Fix some deprecation warnings. [Commit.][381]
  * KNewStuff: centralize code, use KNS3::QtQuickDialogWrapper if possible. [Commit.][382]
  * Fix detection of MLT on Windows. [Commit.][383]
  * Fix timeremap crash on undo/redo. [Commit.][384]
  * Save Effect Stack in stack header. [Commit.][385]
  * Build instructions: remove kdoctools-dev. [Commit.][386]
  * [flatpak] Update mfx-dispatch. [Commit.][387]
  * [Issue-291] Create Folder is available in bin context menu. [Commit.][388]
  * Remap: fix editing “speed before” broke, correctly clear remap widget on clip deletion. [Commit.][389]
  * Some progress in remap keyframe widget (WIP). [Commit.][390]
  * Flatpak: Update some dependencies (2). [Commit.][391]
  * Flatpak: Update some dependencies and restructure manifest (1). [Commit.][392]
  * [flatpak] Add libva and libva-utils. [Commit.][393]
  * Update mediasdk package. [Commit.][394]
  
 [3]: http://commits.kde.org/kdenlive/537fe89d1d503056fb1fcf12745aad5c934c28d3
 [4]: http://commits.kde.org/kdenlive/77437e787db1b4d9667565864e942797efdef752
 [5]: http://commits.kde.org/kdenlive/59bdb1b12754d48133840ac6c6fed2d4f622adc0
 [6]: http://commits.kde.org/kdenlive/07230a205b8dae742bcae8ec47e4cfd580eae69a
 [7]: http://commits.kde.org/kdenlive/cfdcf0d2ce054af27c4d8a6a104f6f1cfd288cf0
 [8]: http://commits.kde.org/kdenlive/ba91915ed8a744e685d4bad668b1ce12659dc04b
 [9]: http://commits.kde.org/kdenlive/74b825dcf5917bbefa1275401135d4acbbc7842a
 [10]: http://commits.kde.org/kdenlive/66e96bc4a9e5cb54bbda3294277c45cd372ee011
 [11]: https://bugs.kde.org/446203
 [12]: http://commits.kde.org/kdenlive/88146032ae3e5b8bf2286930a3e3390ffcc2c8e4
 [13]: http://commits.kde.org/kdenlive/449af1b97b04a3fd113b0fa3de297f19d73b2cae
 [14]: http://commits.kde.org/kdenlive/02c8c04c9b162054c71100a3be24cda7ecf2efa7
 [15]: http://commits.kde.org/kdenlive/509af7f96f256755f21f52231715cbaf3052fb30
 [16]: http://commits.kde.org/kdenlive/a43533d3941c9978bf420e9cad099e49e7cfe406
 [17]: http://commits.kde.org/kdenlive/dc45090736fc5f8c10be1ece70d98a298682b2df
 [18]: http://commits.kde.org/kdenlive/0618b5de2bc490d8a59f4a33b1a994b7b5e860c6
 [19]: http://commits.kde.org/kdenlive/c1b0f27582bbb97932d5a400aa189042739582db
 [20]: https://bugs.kde.org/443324
 [21]: http://commits.kde.org/kdenlive/369f7baef306b8bbfd7dc0a0e50565487e1776d8
 [22]: http://commits.kde.org/kdenlive/5a2c8849a809bf4c91e7a864b21f20e38438f85a
 [23]: http://commits.kde.org/kdenlive/0ed64d951e46fcdca7f18fdf3fde78bd9e72a69b
 [24]: http://commits.kde.org/kdenlive/676b79c16f99bd623951728aee1b67c1ae7e1edf
 [25]: http://commits.kde.org/kdenlive/504154d1ecd773b3f4854436138cc21082265d0c
 [26]: http://commits.kde.org/kdenlive/e0899fa5c5f6f51a24063395e9d9fb75b1a66bf9
 [27]: http://commits.kde.org/kdenlive/ae24f6d6ae66f608f8a1eb9862bd062e11fa66e5
 [28]: http://commits.kde.org/kdenlive/72f0ce9980b0f4dff89c65b5709a82fc3e7fa96e
 [29]: http://commits.kde.org/kdenlive/4c470753c85deefd62f3bdaf5a35f32865500296
 [30]: http://commits.kde.org/kdenlive/149cdf5e1da8effa4db9709e264d6f1804de138a
 [31]: http://commits.kde.org/kdenlive/3e6d06eac3069491dc21a676115c76df94973396
 [32]: http://commits.kde.org/kdenlive/6c2a5c15dbaeb18b80082d2a5f8a46ac4f606d9f
 [33]: http://commits.kde.org/kdenlive/59cc1202d21c75ec5033bd677157c7dd5a071203
 [34]: http://commits.kde.org/kdenlive/1374bd44753def20dc8c3dc437fc731c5125b412
 [35]: http://commits.kde.org/kdenlive/462b430d72b47ec8e30cc0b5c81ebc3dde6d6098
 [36]: http://commits.kde.org/kdenlive/026f210934395db000194df9b4a841bcc9197d36
 [37]: http://commits.kde.org/kdenlive/cbf2af356c42b5337938e9a99e649f12f0057495
 [38]: http://commits.kde.org/kdenlive/48c605b64a07a33675dd2d389ecebe7cce716e68
 [39]: http://commits.kde.org/kdenlive/46440d9eabf420dab5689561b46eeb6dd7231c83
 [40]: http://commits.kde.org/kdenlive/956009fc69c3225053b496c024415274e036f5eb
 [41]: http://commits.kde.org/kdenlive/86b662f7eab999efdf077f3fc8dc3573d4a8b7f1
 [42]: http://commits.kde.org/kdenlive/045c214f5a9e36eeb19578dcb460e71a2cf29315
 [43]: http://commits.kde.org/kdenlive/2cf317dbed8308f46857f749c96fc8e1ba53657a
 [44]: http://commits.kde.org/kdenlive/b24148d53ec26651f48f4bf6fe368d9861eea155
 [45]: https://bugs.kde.org/#442880
 [46]: http://commits.kde.org/kdenlive/816c93790d434909f31f615937f9ffaa0b7278e6
 [47]: https://bugs.kde.org/440185
 [48]: http://commits.kde.org/kdenlive/21cd94995de06a05b223909688aaf623107ab42f
 [49]: https://bugs.kde.org/440024
 [50]: http://commits.kde.org/kdenlive/7b7d27196313609ea530bb53d98c2b22bb313832
 [51]: http://commits.kde.org/kdenlive/7e1a2251e42f59ccdd29f45a3b28a96e81360697
 [52]: https://bugs.kde.org/445232
 [53]: https://bugs.kde.org/436113
 [54]: http://commits.kde.org/kdenlive/e1cd395adb4741a8b9610e6bf91bf523bb418325
 [55]: https://bugs.kde.org/440023
 [56]: http://commits.kde.org/kdenlive/a7c06a570bf0d5bd60fc0620efccec34174ee8b2
 [57]: http://commits.kde.org/kdenlive/d4575061d29c9161f6de511fdbe18bb4d17aa675
 [58]: http://commits.kde.org/kdenlive/49699b4bb9f888d879d3d2263c3ba4110195db3f
 [59]: https://bugs.kde.org/444595
 [60]: http://commits.kde.org/kdenlive/f70f952e50fee3907d5230ac2d1b9a704fa0568a
 [61]: http://commits.kde.org/kdenlive/10ea22996eeededa87fa551231c059cbd2b33764
 [62]: http://commits.kde.org/kdenlive/1790aaf3a1e8e220f84f36a0d96a180a7eba3d58
 [63]: http://commits.kde.org/kdenlive/a3a5582bf255d3b9969b61443c295e48bb9cf9b4
 [64]: http://commits.kde.org/kdenlive/5a09ccff643acb0775434f2d8bd9c622999c28a1
 [65]: http://commits.kde.org/kdenlive/9ffe861498afe5b7e8219cdfb18db3834dd72485
 [66]: http://commits.kde.org/kdenlive/0ab118277f486f5f18a3b0d7cd70867c8afb7488
 [67]: http://commits.kde.org/kdenlive/1e2d827bc33515187aaaed8cf30ab677bb969f4a
 [68]: http://commits.kde.org/kdenlive/bcf6bdca4281d0a8d5c6dd5027ab7cbfd7a29683
 [69]: http://commits.kde.org/kdenlive/5563df92c94b66de61bb83050336ea8cd21c7b20
 [70]: http://commits.kde.org/kdenlive/6741f01b76741acd02ccfd6d0773e6a6f0fb0fbf
 [71]: http://commits.kde.org/kdenlive/e7915cb35723db2cd18175051b09656c8f7a8e03
 [72]: http://commits.kde.org/kdenlive/b04dedff04424b1075bff2f80c959d90a6e67f5a
 [73]: http://commits.kde.org/kdenlive/d311eda6ec5a3d96d1e277f6b25ae6d1f532db16
 [74]: http://commits.kde.org/kdenlive/0c3df81fcf9ace3e3cf70464eafdb602dec4c9e5
 [75]: http://commits.kde.org/kdenlive/c89ff3ef824c4bd0afa8e287509e432bc4b8e5e2
 [76]: http://commits.kde.org/kdenlive/ae64fa29cbe0c313bad3d0fe8647fad6e4557cf7
 [77]: http://commits.kde.org/kdenlive/32cc9bace95b7962f95b830339488ae8fe19966a
 [78]: http://commits.kde.org/kdenlive/7962e1670fe7e1868470456c6b1d6ae998a5fce5
 [79]: http://commits.kde.org/kdenlive/bbac0b2203ee5bb64beb3ce040abcdbfb084cfe1
 [80]: http://commits.kde.org/kdenlive/957e7597907e83d2884c350ee989ee073827c3da
 [81]: http://commits.kde.org/kdenlive/03b4df1f7b614d99c47c02edb0a8dbda8b41f7e2
 [82]: http://commits.kde.org/kdenlive/de9bf0f014833266c4ed52bd044d055a70fe1ab8
 [83]: http://commits.kde.org/kdenlive/4f9b6c6bf05d1be1e2e06a9e7d7c9bd415178959
 [84]: http://commits.kde.org/kdenlive/deb51c9fa5a02d4b5d8bd957dba58e4613decd15
 [85]: http://commits.kde.org/kdenlive/d7c528b26f13df73378761323291fb6abdba0fd8
 [86]: http://commits.kde.org/kdenlive/dafd50fd412e46128e3a669e39d474ffb6b4147f
 [87]: http://commits.kde.org/kdenlive/58ddd078344f95efd1e889b97ffff51ff20464ca
 [88]: http://commits.kde.org/kdenlive/61bf3c8bfda906205b25ccb49aba9620fee76673
 [89]: http://commits.kde.org/kdenlive/c8806156f50c5fc650f96a9089559f88d7816667
 [90]: http://commits.kde.org/kdenlive/8215916122f801bda841ea8b8eb173745534ed09
 [91]: http://commits.kde.org/kdenlive/2f7f7c5b3326d99caddf734ce11614b93072b7c9
 [92]: http://commits.kde.org/kdenlive/3aa01a85e17cd9795e19fdb5b55efcba9ee1cd36
 [93]: http://commits.kde.org/kdenlive/c0bd2cc49ead1cd71df669bf99be34ced464614b
 [94]: http://commits.kde.org/kdenlive/f404a4e09c555b4cc746d722e392dfa21987ddaa
 [95]: http://commits.kde.org/kdenlive/bb81a5e8910bb7572b5b255fa06b5e16ecfa4513
 [96]: http://commits.kde.org/kdenlive/97b0ca554d2b60861518b9ce1f9dfef5a9cec90b
 [97]: http://commits.kde.org/kdenlive/177250f5c230bf07dc31ba08d24103812581686a
 [98]: http://commits.kde.org/kdenlive/a570f04c097a67fc3b108bb9c5abf8e24f391b16
 [99]: http://commits.kde.org/kdenlive/dac7f2781213d91c4633c9c1f99d342d8d6e3a3e
 [100]: http://commits.kde.org/kdenlive/cbf49b2ce46c01c366700bfdc7896644ed6c40e2
 [101]: http://commits.kde.org/kdenlive/26321bd6b9eb143cd553881668c98b746fc0dbf4
 [102]: http://commits.kde.org/kdenlive/385e885aa1166a3f5d8b6c0e07d0f611789f6476
 [103]: http://commits.kde.org/kdenlive/eab78ff713fe555321513805efc7e99ddd52a225
 [104]: http://commits.kde.org/kdenlive/89a4bdf211c99536f8f456aed9379778377b3224
 [105]: http://commits.kde.org/kdenlive/a29dcd7feeabcdbe9d4cf022f5b2a96b75855893
 [106]: http://commits.kde.org/kdenlive/e2804b6fe78de7d7890cd2aa03c134af17c6f8d0
 [107]: http://commits.kde.org/kdenlive/414b48698b62071f0e743cc5b78a09a08bbefa3b
 [108]: http://commits.kde.org/kdenlive/712a74e1a51f82f04174eb77deb2b7ab4eff1ec5
 [109]: http://commits.kde.org/kdenlive/5272548155d7321b0bdc97854c7bcde48d4d1539
 [110]: http://commits.kde.org/kdenlive/82eea08001f55e9a3555999cf023f7a71b9126a3
 [111]: http://commits.kde.org/kdenlive/4642f7ea463e96bcf55c1cb0830abe473d35322a
 [112]: http://commits.kde.org/kdenlive/ecc524fd4f946a184e87864d2191a5cafd3079dd
 [113]: http://commits.kde.org/kdenlive/e858aed69a6a0cc8fb66f21ea449a2a8ef0c798a
 [114]: http://commits.kde.org/kdenlive/0c275f08ca9d232650a34eada6b20032dc171d3a
 [115]: http://commits.kde.org/kdenlive/422e526a0bbc2059a41b5fad7a3bcd0d5a1c1c60
 [116]: http://commits.kde.org/kdenlive/f19c2faa8fa8797c4de486d4492488e1593b9439
 [117]: http://commits.kde.org/kdenlive/7c4a8b0d6592e73ef04980dc88f2e1e86a5fd923
 [118]: http://commits.kde.org/kdenlive/dbd6d54f2b555c72435c9572a20b169800dd5c7f
 [119]: http://commits.kde.org/kdenlive/b476b4dc98bf64fb437c162d2e3e8f74b2144fcf
 [120]: http://commits.kde.org/kdenlive/f41c9463a5a1f1de9c3c030d94f12e53a568454f
 [121]: http://commits.kde.org/kdenlive/26c28145015d9ed8ace920a8c5b328f07dac2539
 [122]: http://commits.kde.org/kdenlive/0514c181f318706c10e2503d062f602e952a614a
 [123]: http://commits.kde.org/kdenlive/9d8ec7e7b5d06263ffbef134cebb5be74b853379
 [124]: http://commits.kde.org/kdenlive/949729bc07787a7961c044cccf62318045d454dc
 [125]: http://commits.kde.org/kdenlive/a9920a36063d61090a21ae08e6a4af68292a5ee1
 [126]: http://commits.kde.org/kdenlive/1de9399b4e73a9db00b19a8e71bb5505b06f8b21
 [127]: http://commits.kde.org/kdenlive/eeaec72218627de1ae2b2be0681d40c17f439bea
 [128]: http://commits.kde.org/kdenlive/7cca61898ec646a52a1a5976440652e61eb72903
 [129]: http://commits.kde.org/kdenlive/f824ed70ceb1f6a394404bacd903ec191acb91df
 [130]: http://commits.kde.org/kdenlive/996dad59b1b531f1d5cfa346079bf66dbdbdc6f0
 [131]: http://commits.kde.org/kdenlive/ff89bde23f29176c4538b7e121fd90dfb200e4bd
 [132]: http://commits.kde.org/kdenlive/3f6b18b38257f8ae4572d975d67b7d418c8a8175
 [133]: http://commits.kde.org/kdenlive/3edca1b2bb55349543cf7bd5df7c88647942f381
 [134]: http://commits.kde.org/kdenlive/03c50600a7b25ed17b16be1c0f163bdc2ab3c010
 [135]: http://commits.kde.org/kdenlive/9080a829116e316052e0e55fce34f7637dc35048
 [136]: http://commits.kde.org/kdenlive/66b9172868ff764be37934ad8c55d52f6ccde23c
 [137]: http://commits.kde.org/kdenlive/6ee0c1fd59947071cf82e66a5755885a9d06c90b
 [138]: http://commits.kde.org/kdenlive/6101709197f2689eb43c4566674a399b9fdc6a58
 [139]: http://commits.kde.org/kdenlive/fef176c7fb69cc0980422ee9cd1df52367adcb6d
 [140]: http://commits.kde.org/kdenlive/499f5e0e86b499ee4ba5217dcea3679f22ac77bd
 [141]: http://commits.kde.org/kdenlive/54d859d0cf791bc77b7e5346c041d14ba3e75e4d
 [142]: http://commits.kde.org/kdenlive/97065379a0f995557aea0c6c01db495f4a41ef3d
 [143]: http://commits.kde.org/kdenlive/2acab4fb5d317c065bb157f8a74eecab7fcebb0e
 [144]: http://commits.kde.org/kdenlive/0f009bc326da8ee594f5634384a5715dcdd4ee44
 [145]: http://commits.kde.org/kdenlive/ed41d1fcf2d0dd93aa9f05c87758830ff23b27c4
 [146]: http://commits.kde.org/kdenlive/04b77b9841a4bb72b9a46de2ddc2b1f70d90599a
 [147]: http://commits.kde.org/kdenlive/3ad298ce2dec188981e1c15fa47084fb3f142c5c
 [148]: http://commits.kde.org/kdenlive/9066fcf32a9209a8eb326fcbde4dad41d19e0247
 [149]: http://commits.kde.org/kdenlive/2bc61984ccfb1a4c492f3c17d5c1df89b8af62fd
 [150]: http://commits.kde.org/kdenlive/279037645a28848e185a4b2a3714b27bb6a70e85
 [151]: http://commits.kde.org/kdenlive/d8d8d95ebcb04835258d034c7e514f382504d797
 [152]: http://commits.kde.org/kdenlive/2981c5583f20a997daae8347dca5486ae5dccbbd
 [153]: http://commits.kde.org/kdenlive/0884e41a8c921a3c6150a88fdd7e96369e34a5f9
 [154]: http://commits.kde.org/kdenlive/5c4daf5961037819fb94f6ef95aac435b406b173
 [155]: http://commits.kde.org/kdenlive/c2a1aa09b5ae905ce97ade5056b14b0e959f43af
 [156]: http://commits.kde.org/kdenlive/a644d42ced3dc34e95589077efc5192787e6a27b
 [157]: http://commits.kde.org/kdenlive/8874bde919d0487543cb66e6e861822ea2192fec
 [158]: http://commits.kde.org/kdenlive/0684aba218df4b56a6ad41711b6e516b6e6c148d
 [159]: http://commits.kde.org/kdenlive/f58cca40524e7b45b2e1f019da938bc131cfc478
 [160]: http://commits.kde.org/kdenlive/18ecd72a09ea2368de795a75325cdd89f3afedd4
 [161]: http://commits.kde.org/kdenlive/24eb0223463c1f154d51a9b72b64b30583cb01df
 [162]: http://commits.kde.org/kdenlive/8016e635eb3c442cf925ee80e8962d4a503f2169
 [163]: http://commits.kde.org/kdenlive/1ad1169b47daa30022061bc83b7b2db53d3f06c8
 [164]: http://commits.kde.org/kdenlive/31ece266cbfa8dcb1fd4d6d1d59468c27b9063b3
 [165]: http://commits.kde.org/kdenlive/f6ddbe5adea89a0fa5ea77a1cc56daa525b7e5cb
 [166]: http://commits.kde.org/kdenlive/297d198246bf480ded09aa2e52233800a161344f
 [167]: http://commits.kde.org/kdenlive/8953bd5699930fff59cda074bf4f2f8df09f2286
 [168]: http://commits.kde.org/kdenlive/193c505687dcd38a1291d6b1517157a96dc7c4ee
 [169]: http://commits.kde.org/kdenlive/6f9ae7b824461d5c30560ee2a22777cdf2e7b4ae
 [170]: http://commits.kde.org/kdenlive/8fdb7df48b97e16a4cd889fc47b54b7e7b7f5763
 [171]: http://commits.kde.org/kdenlive/1710aa256540bb193ab180c45048c6b81c96442c
 [172]: http://commits.kde.org/kdenlive/f50444a590d787da06f9021ab48f7c6807087588
 [173]: http://commits.kde.org/kdenlive/7fc9f95c495e34f2a5dc1e370e079ec5bf2fc3d8
 [174]: http://commits.kde.org/kdenlive/803ad61ac47c6661aa2df410874204dcceb7eb64
 [175]: http://commits.kde.org/kdenlive/b743f35fe48fb4731baaf46d510b4f7b8bd31a6a
 [176]: http://commits.kde.org/kdenlive/33370d04b83c8c87a5aa512d4e04be66a6609574
 [177]: http://commits.kde.org/kdenlive/8cf0a52d7ae56ed3185f49f878fe9d1041d1811f
 [178]: http://commits.kde.org/kdenlive/8cc0f851d5c994714b00b9202112d574b90d57fc
 [179]: http://commits.kde.org/kdenlive/c9ff7dd93775d6f19424dd52afe52bb9ffc04108
 [180]: http://commits.kde.org/kdenlive/abda9007f26e0e461ff0eb6a7523fbbcfc2ff705
 [181]: http://commits.kde.org/kdenlive/ba9f8304291f65ecdf29a9dea76e2cfaed7cc600
 [182]: http://commits.kde.org/kdenlive/e95716611c7e73062751c8b75c7c1b5b90b6d2e9
 [183]: http://commits.kde.org/kdenlive/62c9c0f8edc16fbde7887469145f8443c137e16d
 [184]: http://commits.kde.org/kdenlive/0c87551da1b57c033777498181e7bab647fc8959
 [185]: http://commits.kde.org/kdenlive/5c11779ffffe384c54afde66d48aa156400b73cb
 [186]: http://commits.kde.org/kdenlive/cae8df53a20b16a6b2efdb101ce72d397a2d296f
 [187]: http://commits.kde.org/kdenlive/1a0ae0667cbd00d9662e692ace78fdc24c12b77d
 [188]: http://commits.kde.org/kdenlive/610380696257cb12a9b4d98d174f6b1a1b15d9bc
 [189]: http://commits.kde.org/kdenlive/72fac4635ea8d69d9209ab5f18589b5a23bf6d29
 [190]: http://commits.kde.org/kdenlive/20f65d895395cc4fbd1a65e997aa444c3c7c73b9
 [191]: http://commits.kde.org/kdenlive/d0bdc33d720e776ed312333c47d289368947c361
 [192]: http://commits.kde.org/kdenlive/2ab7d94d7bd8272cbd7aec0736a7883e1e05add3
 [193]: http://commits.kde.org/kdenlive/60678be059c42b4151fefc3384f6729bac027596
 [194]: http://commits.kde.org/kdenlive/bbf7e0086bba4d9e3a12b5d5da430ee3b78dfee7
 [195]: http://commits.kde.org/kdenlive/5195da0452e315e9a59a900699c23864fade89b8
 [196]: http://commits.kde.org/kdenlive/285c1cdf9130fa089d72104d07f702304d6ba4c0
 [197]: https://bugs.kde.org/393668
 [198]: http://commits.kde.org/kdenlive/98787276a492e363a50bd1f6711546b880582843
 [199]: http://commits.kde.org/kdenlive/559884ed3072430ae8108a15a3d0ac04e43d555d
 [200]: http://commits.kde.org/kdenlive/08b9c875d98eaf18371179e008575be2bb6b9a50
 [201]: http://commits.kde.org/kdenlive/47991cea59b6e1d4f8b7052e2effd06d0fe8408e
 [202]: http://commits.kde.org/kdenlive/9e762b1742a92c7f496dd910d8241a901328d3d2
 [203]: http://commits.kde.org/kdenlive/0fdcafe0bae5c00697df13c673a40905decd56f9
 [204]: http://commits.kde.org/kdenlive/eafb60e34e3d7bb94025c0dadbbc8c6a65456f06
 [205]: http://commits.kde.org/kdenlive/e7ba82c61b0584159b6ce9232148328e77596818
 [206]: http://commits.kde.org/kdenlive/77847bdeb8af96f026c6f93f67dabb6163ca1eb1
 [207]: http://commits.kde.org/kdenlive/56e7be926ef7983659cfd760243c9fd57607583a
 [208]: http://commits.kde.org/kdenlive/ececa4ff5ceb0d91a9019eac76036a3e832c9d62
 [209]: http://commits.kde.org/kdenlive/5d510c8a867cc1b53d435fa7be6ff8ccb3f2656d
 [210]: http://commits.kde.org/kdenlive/b09b280b49ba80b9e2aea3f7339cd6fac4963b0f
 [211]: http://commits.kde.org/kdenlive/11032632e14ebc0920b9288ad52c12ceaaf1b0c4
 [212]: http://commits.kde.org/kdenlive/facbab87a806ea2b2b430b3785093dbccebdeb41
 [213]: http://commits.kde.org/kdenlive/e5ede26735a5ff1bd6ba6a98b5e0f04d60059df4
 [214]: http://commits.kde.org/kdenlive/239561c29ab24a9c8c555ccef762c5d7eeb05818
 [215]: http://commits.kde.org/kdenlive/834d70d0def127fc0db3c379957080e6a644fde8
 [216]: http://commits.kde.org/kdenlive/342aa7bcef1e6e006e4c801507ae0b3bdd74a488
 [217]: http://commits.kde.org/kdenlive/a70d31912570680c043cffc8d7852b01c3781594
 [218]: http://commits.kde.org/kdenlive/b6a4febe309265f323ea212fe90081049bc328c7
 [219]: http://commits.kde.org/kdenlive/c7506ad58bfd8e2ea7ab3036c648d5aec412e98a
 [220]: http://commits.kde.org/kdenlive/a1b0f13be88e7f6df71529a3d98fb5500a5ff56c
 [221]: http://commits.kde.org/kdenlive/238f688ead2d4354e907b3311625debc554a1b88
 [222]: http://commits.kde.org/kdenlive/5f5630431a5870d2877f86a894822535ae872ad3
 [223]: http://commits.kde.org/kdenlive/59e53b3c5f7cd9ed8cd6d97dd3b7dca53ed4c47e
 [224]: http://commits.kde.org/kdenlive/c8ecde2491eea5e5ecb20cbeb17d6123454f7ab3
 [225]: http://commits.kde.org/kdenlive/d391f6d343367571fc4c7c44904bc84dd24e552e
 [226]: http://commits.kde.org/kdenlive/700c6eece7611d98e252bf68485fbfbd5d9bfdc9
 [227]: http://commits.kde.org/kdenlive/503883664aa38ba7e0394b8346f8a6579a29eb07
 [228]: http://commits.kde.org/kdenlive/c3e34f8a6d3a8af1de2e1e668edc4c267251e232
 [229]: http://commits.kde.org/kdenlive/293f0edd607fddd4d85cda72c7310c7266d5721f
 [230]: http://commits.kde.org/kdenlive/f4cb04368dab539408a981e6f583b9dd7ebe4b59
 [231]: http://commits.kde.org/kdenlive/8d14944b31761ce90d6a7f0296b343c9641fa86b
 [232]: http://commits.kde.org/kdenlive/ad2c3f22404efa997641d8c64454663d0e1da508
 [233]: https://bugs.kde.org/442372
 [234]: http://commits.kde.org/kdenlive/a17e839c8fb5a243467978d23a7cddfe3e7b1582
 [235]: http://commits.kde.org/kdenlive/7befb02673f46de246ef18aad25ecc37b8969f4c
 [236]: http://commits.kde.org/kdenlive/6731a1a80317b76364f41c6de3cd3eb6ca723f26
 [237]: http://commits.kde.org/kdenlive/342fdcc470aeaa1394692192a3adf075c7710c26
 [238]: http://commits.kde.org/kdenlive/4c0163c5b93e6f648e90f1cd9c2df8f06f2c7d9c
 [239]: http://commits.kde.org/kdenlive/36283985381c6fbd15004138cb68297d05b2d8e8
 [240]: https://bugs.kde.org/441777
 [241]: http://commits.kde.org/kdenlive/1a41597b08986a6e886962873fa3296a8ce21833
 [242]: http://commits.kde.org/kdenlive/5d2b100fa9706f99f41915fb53372de4a061dded
 [243]: http://commits.kde.org/kdenlive/0ead076723bea876a6adbf6affafee3efcbd940b
 [244]: http://commits.kde.org/kdenlive/340407078f5e699d780475ab2f04940912d38aed
 [245]: http://commits.kde.org/kdenlive/4905a9016a4c911a9681b435323eca04bc000b7c
 [246]: http://commits.kde.org/kdenlive/2a81464eda25f0743533c1b2c632c4111bf8148b
 [247]: http://commits.kde.org/kdenlive/85dfe0a0d7bb5044e3cec55a1a83c8431d900fdf
 [248]: http://commits.kde.org/kdenlive/3d2e023629eccbc62031778e615f78778a9ce748
 [249]: http://commits.kde.org/kdenlive/cc2ece304d10d2dfa1c0c1a34dbcb031f6ee76c7
 [250]: http://commits.kde.org/kdenlive/4d18d2791f22f5b389ecbc78cfca2e071c4a5499
 [251]: http://commits.kde.org/kdenlive/eefb79f901956a921622b8d807db0844271afba5
 [252]: http://commits.kde.org/kdenlive/d64fe2d97b86fcce9433f59a0959c6a2cb9ff234
 [253]: http://commits.kde.org/kdenlive/a3e21feb9ba3a9405f66efbb260058dd982f1bce
 [254]: http://commits.kde.org/kdenlive/2ee142a68b9fbe5e41a702b9cc8f2ca42e4646f1
 [255]: http://commits.kde.org/kdenlive/cacb5dd261632271c6a8a43ef611aaafc1346fa9
 [256]: http://commits.kde.org/kdenlive/b147288703039a53608d9204ffba1637fd88af3e
 [257]: http://commits.kde.org/kdenlive/66f2229cd0a32d41bd7f2bec6261c720b4292afa
 [258]: http://commits.kde.org/kdenlive/e591242ecfc98b5e6e529bdbc0e233cfddc95c1d
 [259]: http://commits.kde.org/kdenlive/a4fc056742a38dd356509865abc5735e15489c34
 [260]: http://commits.kde.org/kdenlive/2f8f8f5fac11ebf1feaae03c3d080ccec7f16645
 [261]: http://commits.kde.org/kdenlive/65c96a7ad9c5af8a52e76c03f53d4ae0d91bec7a
 [262]: http://commits.kde.org/kdenlive/83c0051f58a9eee61f6c05ea3bd738146fca2292
 [263]: http://commits.kde.org/kdenlive/3c53baa6e97d7c359a45d2fbba6a7c1ab950ee7b
 [264]: http://commits.kde.org/kdenlive/f44fa281f020c93985d91963e4b20428fcc2f771
 [265]: http://commits.kde.org/kdenlive/313565af261247e2663fec801ab2575002fc8eb7
 [266]: http://commits.kde.org/kdenlive/99781d53b8e93491ee6c92996cc61705f6945f8f
 [267]: http://commits.kde.org/kdenlive/e4670c659fe25374eee90f8287c57ec368544f22
 [268]: http://commits.kde.org/kdenlive/8df33bb0843bad0484f866e8f7bfb298247da2fe
 [269]: http://commits.kde.org/kdenlive/8d8c4557c6d5cf8c6c88f271308950c42a0989e6
 [270]: http://commits.kde.org/kdenlive/a8a25bc8b438df267a4e41e2b527ca4b6c2644c5
 [271]: http://commits.kde.org/kdenlive/d5c2d0faf0112c735f538b1fb5d7c9ca8f56358b
 [272]: http://commits.kde.org/kdenlive/c4169c91ac52d3c0e7d9cd5d2527c9611b7bc773
 [273]: http://commits.kde.org/kdenlive/32717056488b8d5aed7ac4af7668434191765bef
 [274]: http://commits.kde.org/kdenlive/81d83a72b4c8a2da764017fe3f16d89eeb151b7e
 [275]: http://commits.kde.org/kdenlive/6d089271c89a0ec89489bec9e6059a2659806c38
 [276]: http://commits.kde.org/kdenlive/708a2fc76498448dbcc21ca6e40215a999537574
 [277]: http://commits.kde.org/kdenlive/c41e8b488fffe4df20f9f3e2c425809c70e43b05
 [278]: http://commits.kde.org/kdenlive/706f3fe036faa106cb38b9f412a39147734cc4a5
 [279]: http://commits.kde.org/kdenlive/acbdafae77c44485854f424972a31bb197f4b59f
 [280]: http://commits.kde.org/kdenlive/ff0ec149095cf8ba100717abf994fef73d64f242
 [281]: http://commits.kde.org/kdenlive/b8eb7a0a9c7cc3362f95802b12f45d7b217f6aa0
 [282]: http://commits.kde.org/kdenlive/64a6db8c57fd7e8f10d844a7483f5287c956c0ec
 [283]: http://commits.kde.org/kdenlive/c82ec8ae4c193b961492f378faf0149243d39683
 [284]: http://commits.kde.org/kdenlive/8efd44d72970c595d7a147e5394993215e052dc4
 [285]: http://commits.kde.org/kdenlive/ecd6382ac627e79d9d053d36946f3187ea47179e
 [286]: http://commits.kde.org/kdenlive/91b962e7e0beba94a3ff47c2732df986dcd0175b
 [287]: http://commits.kde.org/kdenlive/8416aeaa2b2d38c94c5f077fcf00bc140cbfc7ca
 [288]: http://commits.kde.org/kdenlive/0b34631b1a8a3571cf345869b1bcfdd788d6613d
 [289]: http://commits.kde.org/kdenlive/0c3f91e2b5fe5d2b76abf0808d1da8f3b51ae86d
 [290]: http://commits.kde.org/kdenlive/29ab140f710adc740a810448613e8243e7271d80
 [291]: http://commits.kde.org/kdenlive/ebd9af81bc9b6b73ef7aa2debbb3b317b02ac9b0
 [292]: http://commits.kde.org/kdenlive/e59e084c0946f1a4189ddad2188c4271bde55bd9
 [293]: http://commits.kde.org/kdenlive/2312bdeb08f115d15bb886f6fc6262e094912b20
 [294]: http://commits.kde.org/kdenlive/a89d914360a4b238cb380e28c1ad048616053745
 [295]: http://commits.kde.org/kdenlive/fd4de56131fba0bbb88d4baf03e5b57d09e29bc9
 [296]: http://commits.kde.org/kdenlive/afff6143184e4b8105451cb2bf196b8761b16715
 [297]: http://commits.kde.org/kdenlive/29773d3398cdce1857d71dfdcade68836b3740fc
 [298]: http://commits.kde.org/kdenlive/e68468ef82d1fda1786ba22e395594945d9a7c72
 [299]: http://commits.kde.org/kdenlive/2dd13b59d5ea51df4be1b96c4980cb4254ef8f4a
 [300]: http://commits.kde.org/kdenlive/dcb71b2dd8d54bf82e883640c4c7c846f9cc4cab
 [301]: https://bugs.kde.org/408235
 [302]: http://commits.kde.org/kdenlive/030e79f0baed15956a7dc2164854e4a31212dc23
 [303]: http://commits.kde.org/kdenlive/462a335a1fb3139c3c793c1e5b161d09f793c5f1
 [304]: http://commits.kde.org/kdenlive/6f94cb4b7dc3b8d2709b3e950a6b2efb50073f62
 [305]: http://commits.kde.org/kdenlive/b80a909e48c3b36ea3b58426fe42efb4253e47e7
 [306]: http://commits.kde.org/kdenlive/8d354f07f5b81ad0a1c7ceeb68d26dec2efa81d7
 [307]: http://commits.kde.org/kdenlive/9f24807b10b353b02c05429806e0b81d7d29c0f7
 [308]: http://commits.kde.org/kdenlive/857c43fbf2b4d136b12e998ac7f139c827d4c3cc
 [309]: http://commits.kde.org/kdenlive/14c57d6a18704777823a6151c25389785f692a69
 [310]: http://commits.kde.org/kdenlive/1052478b157726f8476ce47f20b7fb5c4919db63
 [311]: http://commits.kde.org/kdenlive/2a1f26855eb9c51d09929620307dc40bfe29055d
 [312]: http://commits.kde.org/kdenlive/8730a8c0d9c880ee4bc49b9155d75a90dd4b0672
 [313]: http://commits.kde.org/kdenlive/c1e744bec3e6c209566b6d9d689d58b73dad4789
 [314]: http://commits.kde.org/kdenlive/3c52502442c3d0ef5bacfcbeed6e886479400fb8
 [315]: http://commits.kde.org/kdenlive/6b5335bdf6ce3bcf4a598275398db8352b1785d0
 [316]: http://commits.kde.org/kdenlive/cb451a8105a7eba136f8bc554b0568fbfc5a76b5
 [317]: http://commits.kde.org/kdenlive/d104f12e94047995d303fcb87d4a33f60fd7012e
 [318]: http://commits.kde.org/kdenlive/a048336cc463f50cf23395ab74394bcfb4dadd64
 [319]: http://commits.kde.org/kdenlive/56fce4ee48cd27df852c69f1ae0226a1c0e4e257
 [320]: http://commits.kde.org/kdenlive/9acc40b1a3ce1806283d15430be87f6b73ec15b0
 [321]: http://commits.kde.org/kdenlive/aa75ebd454e39a39b082d304ae0806c163922f5d
 [322]: http://commits.kde.org/kdenlive/7e8f1899aa1b3a5986ac961fc865279d1b1d9881
 [323]: http://commits.kde.org/kdenlive/056ca31a6da49b20f1f7ad8c44b55ac891188a73
 [324]: http://commits.kde.org/kdenlive/3f9fb162b633e81561101978e2b0647fee499cfb
 [325]: http://commits.kde.org/kdenlive/da7214edb479e516d6d2389b08d7b8742b8b8a22
 [326]: http://commits.kde.org/kdenlive/0ccb60cafc052840a5d7fc0073fc95a29cb4a167
 [327]: http://commits.kde.org/kdenlive/0eea5c61369f8eb37732439b993cd503b3ca1a90
 [328]: http://commits.kde.org/kdenlive/8d7e7eca56e2ca73b060ca5764ac6f8104ef2a38
 [329]: http://commits.kde.org/kdenlive/e6069b7641e77589381c019aae6e9e47e89b5a9c
 [330]: http://commits.kde.org/kdenlive/61de26d5e27b1ad6d1706797136f6b4c978d5e7e
 [331]: http://commits.kde.org/kdenlive/4bc2315adaddc0c56765ee1215e0583799eecfd1
 [332]: http://commits.kde.org/kdenlive/84f225580361d3572f38a584b3f7ac8c11b00be4
 [333]: http://commits.kde.org/kdenlive/429faa71960571996a7834b60d28536039f30067
 [334]: http://commits.kde.org/kdenlive/78201795b565595a44c50b1285583ebaa8816497
 [335]: http://commits.kde.org/kdenlive/7a05e261716fa08d634b051fe679b50697eca19b
 [336]: http://commits.kde.org/kdenlive/70bd964f39df172fbe2933994f68eb724c67de69
 [337]: http://commits.kde.org/kdenlive/b520e187cd6e80cd924230edec688e9ca341a490
 [338]: http://commits.kde.org/kdenlive/38b918d8bb235190f8f929d3e164a2eded5d052c
 [339]: http://commits.kde.org/kdenlive/a06cefed768d0f9233eda6845b2670dc165b93fc
 [340]: http://commits.kde.org/kdenlive/c1ed0be9c2e29a13bc46d62a64e1043e33312745
 [341]: http://commits.kde.org/kdenlive/72777c5d5ebbef9e8eb557485edaab8b24de396a
 [342]: http://commits.kde.org/kdenlive/3a24da5dbceafe67000dec9ff5606c6e3190b5a0
 [343]: http://commits.kde.org/kdenlive/519b4fc0e346367f7e50d7a5a6ed07039edf86d0
 [344]: http://commits.kde.org/kdenlive/25412ec4116b8c0cb7909eedbca8b2feb00042b8
 [345]: http://commits.kde.org/kdenlive/e0f58e6a7224ac639c504354bef258d33be67a2d
 [346]: http://commits.kde.org/kdenlive/ae9d66bd8a5c7c2cc1be24d3eea2e30234c4b504
 [347]: http://commits.kde.org/kdenlive/d22080ee2a7e65accc7e6578de2df06876b77bd5
 [348]: http://commits.kde.org/kdenlive/16dd7b41c0a832b653585dd5e9d295d54368c0af
 [349]: http://commits.kde.org/kdenlive/3ed0a9a95da437ef95fa5622dd30b51a5533f313
 [350]: http://commits.kde.org/kdenlive/4d08de25cdc2969cf7a1856946887e3a83a4a503
 [351]: http://commits.kde.org/kdenlive/b73e10b8ec1ac524255f2f0db1addea0058c17e5
 [352]: http://commits.kde.org/kdenlive/81ddab103f09a5ef827191d841d571ddcbe26e62
 [353]: http://commits.kde.org/kdenlive/941a1b8e45c583905bb005ab65baca57d0f6f552
 [354]: http://commits.kde.org/kdenlive/a69963ab998c07cb9b713913c413477c464a3028
 [355]: http://commits.kde.org/kdenlive/4372507228942849cbf6c3ad24f48c4ffb51f635
 [356]: http://commits.kde.org/kdenlive/18c4426bd9994672042187b44ffd6f9ffb508a87
 [357]: http://commits.kde.org/kdenlive/edaf18e29cdb325cd7cdc914e1f6d7a242a7da09
 [358]: http://commits.kde.org/kdenlive/a8f6552cea1a94ba85488d59bc752c8337c3856c
 [359]: http://commits.kde.org/kdenlive/eb766ee40a1a7e28d325983d06ae76ffd852e5f6
 [360]: http://commits.kde.org/kdenlive/8167365dca95d84d67ffa7dea05132e73893ae8d
 [361]: http://commits.kde.org/kdenlive/01da1cf296b7b903223b74fdff80245b0dbf303c
 [362]: http://commits.kde.org/kdenlive/98f8faca7a2a701dd6964797aab191d63a421282
 [363]: http://commits.kde.org/kdenlive/38b26afc19b8b4d8bc9e8d1fbe0ec42f8aa725fc
 [364]: http://commits.kde.org/kdenlive/ca371a8346a0b75dcacbfa4919eeb9cbe5cff28f
 [365]: http://commits.kde.org/kdenlive/834c791eef12f6ddf0cdcce7eb472f39051cbbcc
 [366]: https://bugs.kde.org/440218
 [367]: http://commits.kde.org/kdenlive/92956539822f28aa221f11b1409b50a4beb283e6
 [368]: http://commits.kde.org/kdenlive/cdbfc26c7b99f73ddbf2b39926bdcf97afb1f5a4
 [369]: https://bugs.kde.org/440414
 [370]: http://commits.kde.org/kdenlive/d6462a990fe42148079a570b3be4bc960b3548a5
 [371]: https://bugs.kde.org/440867
 [372]: http://commits.kde.org/kdenlive/e9a8fbb404df97ea1f1f9a39b75dcfa6006024fc
 [373]: http://commits.kde.org/kdenlive/5adad7a7c89af4de0bad8885e23530c055765865
 [374]: http://commits.kde.org/kdenlive/47d2146cb45114e3e113a04714da814d1ef0e41b
 [375]: http://commits.kde.org/kdenlive/a4a3e1a2b47b25d8af1b8486c8d590287333f854
 [376]: http://commits.kde.org/kdenlive/2e040676c156d9c3577fedc62362eddcb5560e57
 [377]: http://commits.kde.org/kdenlive/615be201cc649dbf4154fa3eaf5bfc5888e28610
 [378]: http://commits.kde.org/kdenlive/15ce0c316ce949f9788570ce39c06477c93c39cb
 [379]: http://commits.kde.org/kdenlive/c5b43352534cd7121fac3b8ad576ce7499ff8df1
 [380]: http://commits.kde.org/kdenlive/e22eccc203f099de8426f58adc5237298a088fe0
 [381]: http://commits.kde.org/kdenlive/3b4b43b4296565c7acac1900cc5b2182f2278ef9
 [382]: http://commits.kde.org/kdenlive/42b10c961e1205f21bfd69de872c3a7bf340dee8
 [383]: http://commits.kde.org/kdenlive/6b563c980b9f2f782a89468508042f5f9e7c7933
 [384]: http://commits.kde.org/kdenlive/c9942f8a6146e09cf0b0db439361f27436501bc7
 [385]: http://commits.kde.org/kdenlive/4d266c6f38339da8e95e066d2c3cdad6971fd1d4
 [386]: http://commits.kde.org/kdenlive/f88bfbbba8efb7a3b81b2add13287e8c609751fc
 [387]: http://commits.kde.org/kdenlive/a84e55567ea2dc6e564179a77685f664c20b315e
 [388]: http://commits.kde.org/kdenlive/cdf7a5b8c3f2411d7d607ec1fac0d26fe9f8631f
 [389]: http://commits.kde.org/kdenlive/41fdf7b149b1c43723fc04006ccd3532487f5b9a
 [390]: http://commits.kde.org/kdenlive/c5782078a1304d1e5b03908bbb892188e6fd1bd6
 [391]: http://commits.kde.org/kdenlive/ed54bafbb7921c6692f31b15ba78d63a47610dc5
 [392]: http://commits.kde.org/kdenlive/1e0f3f08b7c2345a8f9926bc25c970b71487ce4f
 [393]: http://commits.kde.org/kdenlive/6c4ba1b77350e0398ed10d3ce3d2689a1ee053f3
 [394]: http://commits.kde.org/kdenlive/60946a9a13f61e921494a88e6937945bb1f10824
