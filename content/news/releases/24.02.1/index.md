---
date: 2024-03-28
title: Kdenlive 24.02.1
author: Farid Abdelnour
discourse_topic_id: 13130
aliases:
- /it/2024/03/kdenlive-24-02-1-2/
- /de/2024/03/kdenlive-24-02-1/
---

We’re delighted to announce the first maintenance release of the 24.02 series, tackling regressions, bugs, and crashes. A big thank you to everyone who reported issues during this transition – keep up the great work!

