---
author: Farid Abdelnour
date: 2017-04-21T03:31:02+00:00
aliases:
- /en/2017/04/kdenlive-17-04-released/
---

![Screenshot of Kdenlive version 17.04.0](screenshot.png)

We release 17.04 with a redesigned profile selection dialog to make it easier to set screen size, framerate, and other parameters of your film. Now you can also play your video directly from the notification when rendering is finished. Some crashes that happened when moving clips around on the timeline have been corrected, and the DVD Wizard has been improved.

Please note that while this major release may seem to have few features development is at full throttle in the refactoring branch. You can monitor the progress [here][1].

 [1]: https://cgit.kde.org/kdenlive.git/log/?h=refactoring_timeline
