---
title: Kdenlive 19.04.2 is out
author: Farid Abdelnour
date: 2019-06-07T08:00:26+00:00
aliases:
- /en/2019/06/kdenlive-19-04-2-is-out/
- /it/2019/06/kdenlive-19-04-2-scaricabile/
- /de/2019/06/kdenlive-19-04-2-ist-freigegeben/
---

The second minor release of the 19.04 series is out with 77 bug fixes and minor usability improvements. Among the highlights for this release are fixes for compositing issues, misbehaving guides/markers and grouping inconsistencies. The Windows version also comes with improvements such as slideshow import and dark themes have now white icons (enable "Force Breeze Icon Theme" under settings). See the full list of commits down below.

As previously stated this cycle is focused on polishing the rough edges post the code refactoring and a whopping 118 fixes have been submitted in the last two months alone. We ask the community to keep testing and reporting issues on our [gitlab][1] instance or join us on IRC (#kdenlive) or [Telegram][2].

**Update**: the 19.04.2 AppImage is available on [KDE servers][3], with an added fix for group operation slowdown.

## In other news

After this months Café we started to define the [upcoming milestones][4] for 19.08 which include Nested timelines and improvements in Speed Effect controls and advanced editing tools (insert/lift/overwrite). Also [work started][5] on the Titler revamping as part of the GSOC program. You can share your thoughts and ideas with Akhil over at the [phabricator task][6].


 [1]: https://invent.kde.org/kde/kdenlive/issues
 [2]: https://t.me/kdenlive
 [3]: https://download.kde.org/Attic/kdenlive/19.04/linux/kdenlive-19.04.2f-x86_64.appimage
 [4]: https://invent.kde.org/kde/kdenlive/issues?milestone_title=Kdenlive+19.08
 [5]: /en/2019/06/revamping-the-titler-tool-gsoc-19/
 [6]: https://phabricator.kde.org/T10747
