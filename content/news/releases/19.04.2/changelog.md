  * Fix copy effect or split does not keep disabled state. [Commit.](http://commits.kde.org/kdenlive/a8874a4d1324d9965fde811443e429cb0fa65925) Fixes bug [#408242](https://bugs.kde.org/408242)
  * Fix various keyframe related issues. [Commit.](http://commits.kde.org/kdenlive/b10d53bf09df465cf401a05c1e7b83351d660da2)
  * Fix error in composition index for 1st track. [Commit.](http://commits.kde.org/kdenlive/e71e7609b5b47a461f9863307f1a4a0df30d1411) Fixes bug [#408081](https://bugs.kde.org/408081)
  * Fix audio recording not added to timeline. [Commit.](http://commits.kde.org/kdenlive/504181807b2663a3ca561d8af044061076cb9b3d)
  * Fix guides in render widget. [Commit.](http://commits.kde.org/kdenlive/99cf7c0ab6057681229395e001f2258473038074)
  * Fix timeline selection/focus broken by recent commit. [Commit.](http://commits.kde.org/kdenlive/613235326d2168350378e8968f6677ddbdb8ff78)
  * Fix fade in broken on cut clips. [Commit.](http://commits.kde.org/kdenlive/4cff4653a82860c99321d45eaf083e14136e346f)
  * Revert audio capture to wav (should fix Windows issue #214). [Commit.](http://commits.kde.org/kdenlive/7c03f9008ad46731d4c18bcce5c9cddef551b890)
  * Fix automask bugs (initial zone incorrect and not displayed on monitor). [Commit.](http://commits.kde.org/kdenlive/ecf1a8da8429663124e21d36cc8ded8d975efe45)
  * Fix timeline unresponsive after deleting all clips. [Commit.](http://commits.kde.org/kdenlive/7e5dabd42a2dc0bbf686d50ddf72ac3d9518524d)
  * Properly load colors & icons (Fix #112). [Commit.](http://commits.kde.org/kdenlive/43a5b6df1206d4e891d065bad6b5689135d92b9d)
  * Fix clip grab state not updated on deselection. [Commit.](http://commits.kde.org/kdenlive/3ae09e5707273e1d21cac69f6fdd11c9a62300d7)
  * Add speed info to clip tooltip. [Commit.](http://commits.kde.org/kdenlive/7099b8acf41315de9c27067a6fbdfdfef99fc4f5)
  * Allow shortcut for change speed action. [Commit.](http://commits.kde.org/kdenlive/d364bac90ec6ad3cf72b72c628bd647abf2d1361)
  * Fix copy / paste track issue. [Commit.](http://commits.kde.org/kdenlive/28633d7a75c95865ce070e7914119320085868fa)
  * Fix slideshow clips on Windows. [Commit.](http://commits.kde.org/kdenlive/7db21278c5a38e4187abf48a9901d624cfb0466f)
  * Fix windows icons. [Commit.](http://commits.kde.org/kdenlive/d14952356202c707ffc5c3327a8a8bcc18320438)
  * Add properly scaled Windows icon. [Commit.](http://commits.kde.org/kdenlive/17e6272aac892b6f3324110c00eed7d6e94bc8ba)
  * Fix crash opening old project file. [Commit.](http://commits.kde.org/kdenlive/54e3030bab91b4dbfa962d43a22f1e6a8eae62a9)
  * Remove old speed effect from categorization. [Commit.](http://commits.kde.org/kdenlive/5f30505886409894f70fefa4ef92389310c8cb77)
  * Automatically convert old custom effects to new type (and make a backup copy in the legacy folder). [Commit.](http://commits.kde.org/kdenlive/88c56c1147af97becf430e3e50d88056af08e829)
  * Fix clip transcode incorrect label. [Commit.](http://commits.kde.org/kdenlive/f960577dfed12a668562374497852ee6d136228b) Fixes bug [#407808](https://bugs.kde.org/407808)
  * Fix various transcoding issues. [Commit.](http://commits.kde.org/kdenlive/257806a30b29a30398ba4ac2814298ae68f66d1e) See bug [#407808](https://bugs.kde.org/407808)
  * Prevent saving corrupted file (with no tracks). [Commit.](http://commits.kde.org/kdenlive/1ca29700b03ab68ed9c7194dc6428295bd045abd) See bug [#407798](https://bugs.kde.org/407798)
  * Detect corrupted project files on opening, propose to open backup. [Commit.](http://commits.kde.org/kdenlive/6f1e61c6930cb5342b091c0b3c62a3e697b7d932) See bug [#407798](https://bugs.kde.org/407798)
  * Fix timewarp test after rounding change in timewarp clip duration. [Commit.](http://commits.kde.org/kdenlive/85df567bd0c4e7655aa5270d881f40543d13e773)
  * Use default composition duration instead of full clip length on composition creation. [Commit.](http://commits.kde.org/kdenlive/960c51c2b0eba229543ef16785dfbca948ac42b4)
  * Fix invalid clip on project opening. [Commit.](http://commits.kde.org/kdenlive/efbf6bd1002174b893befd1f216f594ba0db1839) See bug [#407778](https://bugs.kde.org/407778)
  * Fix 1 frame offset in clip duration after speed change. [Commit.](http://commits.kde.org/kdenlive/7c9787b8ed76e728af882711b9358f3bc0798f3e)
  * Fix incorrect minimum speed. [Commit.](http://commits.kde.org/kdenlive/38f48b653469950676e0847a70baa617b5729683)
  * Fix remaining marker issues. [Commit.](http://commits.kde.org/kdenlive/51e92d90019634edf8976f1eb84be96b971372a3)
  * Don&#8217;t create producers with non integer length (fixes invalid clip issue). [Commit.](http://commits.kde.org/kdenlive/407e2e0990d778c16cd3f40fd03c7b3aa6a2db19)
  * Do not use MLT producer&#8217;s get_length_time methd as it changes the way the length property is stored, causing inconsistencies (clock vs smpte_df). [Commit.](http://commits.kde.org/kdenlive/bc32c54a408d283de1a0568301dcd3c626967632) See bug [#407778](https://bugs.kde.org/407778)
  * Fix crash when marker was at clip start. [Commit.](http://commits.kde.org/kdenlive/1088dfba64db8ff7e7e59e5350f90ec9ab06d7db)
  * Fix marker position on clip cuts with speed effect. [Commit.](http://commits.kde.org/kdenlive/d5cb03d4261364b289bc6f52c531e7769e243479)
  * Fix custom effect appearing with wrong name after save. [Commit.](http://commits.kde.org/kdenlive/f37c1d8e023e1befd41268af6752333cac151327)
  * Use rounder rect icon instead of placeholder folder icon for custom effects. [Commit.](http://commits.kde.org/kdenlive/77ac9a0011d06d8e608a5bb831dbbdd6acd26b46)
  * Correctly hide/show asset settings when deselected/reselected. [Commit.](http://commits.kde.org/kdenlive/746678983ccc0c475f7f4599c318dbbfe69066d9)
  * Fix markers and snapping for clips with speed effect. [Commit.](http://commits.kde.org/kdenlive/f6118f65380a110c366fe55eea83795ffe79fb67)
  * Disable filter clip job on tracks. [Commit.](http://commits.kde.org/kdenlive/50e1cb6304294eaa871a53acc776d1392bc5771c)
  * Fix crash in audio thumbs with reverse speed clip. [Commit.](http://commits.kde.org/kdenlive/ea356ac7a354311eae302ffa67ec30391af7675e)
  * Fix mistake in previous commit. [Commit.](http://commits.kde.org/kdenlive/bc85fb4d01840358f2502404402ee2f7b42c02e6)
  * Fix removeAllKeyframes. [Commit.](http://commits.kde.org/kdenlive/1f06a9861f1fb35efb7df9ca5c61f8e720ced04a)
  * Make lock track undoable and other fixes for locking + tests. [Commit.](http://commits.kde.org/kdenlive/4fb549a7b6af23974e164640aa4a07fb72d5acc9)
  * Re-add &#8220;go to guide&#8221; menu in timeline. [Commit.](http://commits.kde.org/kdenlive/046df4ca1d8b637bd3fe5eaa5c9428c7645a4cbc) Fixes bug [#407528](https://bugs.kde.org/407528)
  * Fix timeline doesn&#8217;t scroll with cursor. [Commit.](http://commits.kde.org/kdenlive/60128fff1596501a3f786937fb6a46f4f585fd0b) Fixes bug [#407433](https://bugs.kde.org/407433)
  * When importing a project file as clip, deduce the empty seek space. [Commit.](http://commits.kde.org/kdenlive/74072058e88860e28eb2f2315447b7679800aca2)
  * Fix opening project containing invalid clips (when a source file somehow went missing). [Commit.](http://commits.kde.org/kdenlive/43ae922aebd7721ff6be1f15df718d1e86df5e82)
  * Fix ungrouping when we have a selection of groups and single clips. [Commit.](http://commits.kde.org/kdenlive/828f24344a942a220c643780d349f755378b16af)
  * Don&#8217;t invalidate timeline/refresh monitor on audio effects. [Commit.](http://commits.kde.org/kdenlive/f9d31a61721cb335a3c518def9d65d47e0ba7e57)
  * Fix wrong stream imported by default on multistream clips. [Commit.](http://commits.kde.org/kdenlive/321f45f60019df268f4e6e2d42f26d728e7ac132)
  * Improve snap behavior on group resizing. [Commit.](http://commits.kde.org/kdenlive/bcbbdbba25bbd2caef3a9d91b58d01f8e287dcf0)
  * Fix dynamic text broken because of missing font & keyword params. [Commit.](http://commits.kde.org/kdenlive/fe566c0799f82f69c1583e5ef90b3d0a4a3cbf52)
  * Fix snapping issues (disable snapping on high zoom levels). [Commit.](http://commits.kde.org/kdenlive/aeb55fceb61b2137230b1e4a101d4637af3b47a2)
  * Better abstraction for locking mechanism. [Commit.](http://commits.kde.org/kdenlive/9f129e22045bb62b624e597f8c2fbe60aeb89d89)
  * Fix endless clip test. [Commit.](http://commits.kde.org/kdenlive/ef8942a685c2ec8acd3ab74060b99888cad82dc5)
  * Fix resetView test. [Commit.](http://commits.kde.org/kdenlive/8371b055b0d344ea5cf4dd90ea0a390885d563a5)
  * Fix edit duration from timeline menu not connected. [Commit.](http://commits.kde.org/kdenlive/787436dd767b5205d1703d3f092f22a99d5d06eb)
  * Fix crash on resize after recent group resize fix. [Commit.](http://commits.kde.org/kdenlive/884ad0a2cd417f7665ff18cb87ade63257c05f0f)
  * Restore go to marker/guide context menu in monitor. [Commit.](http://commits.kde.org/kdenlive/62b260d7c5a598edda5625449fc344b8fb6e76b3)
  * Fix regrouping items loses AVSplit property. [Commit.](http://commits.kde.org/kdenlive/baee7bf682c2c9926ea52a1f3b1c8124da8f0317)
  * Fix: interpolation in rotoscoping filter. [Commit.](http://commits.kde.org/kdenlive/71457a78317ca99f8348efe33725608943120ade) Fixes bug [#407418](https://bugs.kde.org/407418)
  * Fix list parameter default value broken (rotoscoping), ensure we always have a keyframe at in point. [Commit.](http://commits.kde.org/kdenlive/67bab49b2a99ae9220906be15dee6ee43f3bf373)
  * Allow building on Ubuntu LTS & derivatives. [Commit.](http://commits.kde.org/kdenlive/72792d492f57482b39a002e8d0b8d3e8f01ae487)
  * Fix context menu &#8220;edit guide&#8221; leaving empty space in menu. [Commit.](http://commits.kde.org/kdenlive/09877c3d722b240008b74a1961fa92bc16325fe7)
  * Fix fuzzer compilation. [Commit.](http://commits.kde.org/kdenlive/b1911119969dceca47698952c4b058a8f2ec247b)
  * Fix timeline preview crash. Since a QCoreApp was created by kdenlive_render, MLT did not create its own QApplication, leading to linking crashes. [Commit.](http://commits.kde.org/kdenlive/de3e5ee16fa8ac1d6b02451d5c9dc9143f24e74e)
  * Enforce progressive and fps on dnxhd timeline preview profiles. [Commit.](http://commits.kde.org/kdenlive/ceb8d38d68fab548dd71c54b0c848875cca3370f)
  * Add AppImage specific code to ensure we always set the correct path for MLT, FFmpeg, etc. [Commit.](http://commits.kde.org/kdenlive/e2f8630071818c7a6fc01364b446ac636154337c)
  * Don&#8217;t delete timeline preview files on project close. [Commit.](http://commits.kde.org/kdenlive/5600246b4a47c695a525d036b18144b068368a9a)
  * Fix crash trying to delete first keyframe. Fixes #180. [Commit.](http://commits.kde.org/kdenlive/1900cc331c24c849c339039abea5dd96059945e1)
  * Revert composition sorting to match previous stable behavior. [Commit.](http://commits.kde.org/kdenlive/5e34170845391011e582760898d61bb057d03b81)
  * Fix title clip length 1 frame shorter than expected on creation. [Commit.](http://commits.kde.org/kdenlive/8a1fc7772df834759a1b6bb6ac9845a6508feac3)
  * Fix grouping after copy / paster. [Commit.](http://commits.kde.org/kdenlive/5c9d4ead11930f30bdb7c9089268a8509d9e4cde)
  * Fix gap on clip move when trying to move clips. [Commit.](http://commits.kde.org/kdenlive/a72fff76fd6f0ca00e17e7166620c1adb76c4f16)
  * Fix composition tracks listed in reverse order. [Commit.](http://commits.kde.org/kdenlive/b5b08387c23755e29ebcb6f0e376e6811088f632)
  * Fix copy/paste composition is one frame shorter. [Commit.](http://commits.kde.org/kdenlive/c9f551ba8921cbc8cd4160c3805d622259f2c10f)
