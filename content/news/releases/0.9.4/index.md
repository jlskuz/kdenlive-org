---
author: Jean-Baptiste Mardelle
date: 2013-01-28T20:03:00+00:00
aliases:
- /users/j-b-m/kdenlive-094-released
- /discover/0.9.4
# Ported from https://web.archive.org/web/20160319181830/https://kdenlive.org/users/j-b-m/kdenlive-094-released
# Ported from https://web.archive.org/web/20160319020930/https://kdenlive.org/discover/0.9.4
---


[![](kdenlive-094-small.png)][1]We are finally releasing version 0.9.4 of the Kdenlive video editor after several delays.

This version fixes several bugs and crashes, all users are encouraged to upgrade. The source code can be downloaded from the [KDE servers][2], packages for your distributions should hopefully be available soon. It is also recommended to use the latest version of the MLT framework (0.8.8) to get the recent bugfixes and improvements.

You will find detailled infos about the new features / fixes below.

Our efforts will now concentrate on merging the refactoring work started by Till Theato (as a result of our fundraising campain) , and cleaning the codebase to make it easier to evolve and integrate the future developments in the MLT video framework.

As usual, a big thank you to all those who helped us make this release better by giving us feedback and investigating the bugs they found.  
You can check the Kdenlive [online manual][4] and [forums][5] for documentation and help.

For the Kdenlive team,

Jean-Baptiste Mardelle

## Changes

### Rewritten DVD Wizard
The DVD Wizard was mostly rewritten, now allowing 16:9 menus. It now also autodetects the format of your videos and proposes a trandcoding if it is necessary. In fact, you can now drop any video in the Wizard and just click transcode to get it in the correct DVD format.

DVD menus were also broken in the past versions, this should be fixed. While the menu creation remains very basic, you can now add a shadow to the menu's texts to make them look nicer.

![](dvdwizard_1.png)

### Improved clip markers

The clip markers (comments that can be added and used for seeking in the clip) have been improved. You can now have several categories that show up in different colors. Markers can also now be imported and exported, and are compatible with Audacity's format.

![](markers.png)


### Rewritten Screen Capture

In previous versions, screen capture was performed through the RecordMyDesktop utility. This caused several troubles since it created Ogg Theory files that were not correctly supported by MLT (seeking issues). So in this release, the Screen Capture feature now relies on FFmpeg / Libav which means one less dependency and a more reliable capture. Encoding parameters can be adjusted by the user.

### Support for multiple streams clips

Some apps, like video chat application can produce video clips that have several streams embedded, for example where you can see and hear each participant to a conversation. Kdenlive now detects this kind of clips and allows you to import them as separate clips so that you can use the different streams.

### Clip analysis feature

An exciting new feature is also introduced in this version: clip analysis. In short, we can have some MLT effects that analyse the clip and store the result for further use. The first usable cases are:
* **automatic scene detection**: Kdenlive can now parse your clips to find the different scenes and add markers or cut the clip accordingly. The process is currently very slow but it's a start.
* **Improved motion tracking**: Kdenlive can also now analyse an object's motion, and the result of this can be used as keyframes for a transition or an effect. For example, you can now have a title clip that follows an object.

![](scenecut.png)

{{< video src="me2.mp4" muted="true">}}

### Stability and Performance improvements
Lots of efforts were put into stability and performance, seeking in timeline shoud be smoother, you now have audio scrubbing when using OpenGL display, audio can be recorded while playing your project for voiceover effects, and much much more. 


  [1]: https://web.archive.org/web/20160319181830/http://www.kdenlive.org/sites/default/files/kdenlive-094.png
  [2]: https://download.kde.org/Attic/kdenlive/0.9.4/src/kdenlive-0.9.4.tar.bz2?mirrorlist
  [4]: https://docs.kdenlive.org
  [5]: https://discuss.kde.org/tag/kdenlive
