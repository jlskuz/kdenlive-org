* Compile MLT with GCC 9 - fixes brightness effect corruption. [Commit.](http://commits.kde.org/kdenlive/46dc123e2e820d8441d7d73caaac766b5f084fe8) 
* Fix mix crossfade sometimes using wrong order (starting at 100% and ending at 0%) instead of reverse. [Commit.](http://commits.kde.org/kdenlive/7f5e10cc0215e3b46d101a21c96d2a68ef343afd) 
* Upgrade document version and fix custom affine effects for MLT 7 when. [Commit.](http://commits.kde.org/kdenlive/d8d79dd59aa4e45e884f6a36c1594aa0d199f2fc) 
* Make it possible to import mlt rect keyframes to frei0r.alphaspot. [Commit.](http://commits.kde.org/kdenlive/8b5f75d2d5e26df1a7428e1a6bdd347408771873) 
