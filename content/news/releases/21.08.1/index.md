---
title: Kdenlive 21.08.1 is out
author: Farid Abdelnour
date: 2021-09-04T10:55:56+00:00
aliases:
- /en/2021/09/kdenlive-21-08-1-is-out/
- /fr/2021/09/kdenlive-21-08-1-2/
- /it/2021/09/kdenlive-21-08-1-e-qui/
- /de/2021/09/kdenlive-21-08-1/
---

The first maintenance release of the 21.08 series is out fixing regressions to Fade transition and Position and Zoom effect (when applied to bin clips) also, on Windows, Text to Speech is working again. This version adds the ability to import motion tracked data to the _Alpha Shapes_ and _Alpha Shapes (mask)_ effects.

### Outlook for the next major release

In other news work on the 21.12 release has started with some very exciting features coming! Below is the initial implementation of the new multicam editing tool.

![](multicam.gif)

The _Slip_ mode of the upcoming Advanced Trimming feature is done, up next is _Ripple_. ?

{{< video src=slip-tool.mp4 autoplay="true" muted="true" loop="true" controls="false">}}
