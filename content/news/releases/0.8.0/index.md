---
author: Jean-Baptiste Mardelle
date: 2011-04-26T22:37:00+00:00
aliases:
- /users/j-b-m/kdenlive-08-released
- /discover/0.8
# Ported from https://web.archive.org/web/20160322193501/https://kdenlive.org/users/j-b-m/kdenlive-08-released
---


![](080.png)

We are proud to announce the release of Kdenlive 0.8. This new version brings several new features, including improved UI, proxy clips, audio monitoring, HDMI output, new effects and more.

As usual, Kdenlive benefits from the recent improvements in the [MLT framework][1](version 0.7 is required) and [Frei0r effects][2](version 1.3 is recommended) projects.

You can find a detailed list of changes below.

Update (2nd of May 2011):  
MLT 0.7.2 was just released, all users are encouraged to upgrade.

Source code can be downloaded from our [sourceforge page][4], and packages will be announced on our [download page][5] when available.

Support can be found in our [forums][6] and issues reported on our [bugtracker][7].

We hope you will enjoy this release!

For the Kdenlive team:  
Jean-Baptiste Mardelle

## Illustrated new features

### Rotoscoping

![](roto-intro.png)

New effect for creating masks using cubic Bézier curves and keyframes. [Read more][8]

### Corners GUI

![](corners-intro.png)

Easy perspective image alignment. Also explains our new effects GUI! [Read more][9]

### Widget Layouts

![](kdenlive-layout-audio.preview.png)

Layouts can be saved and restored, and optimized for specific tasks. No flooding with widgets anymore! [Read more][10]

### Light Graffiti

![](lightgraffiti-book.preview.jpg)

Long-time exposure … but for video! The first Light Graffiti effect that is not kept secret and is available for everybody … [Read more][11]

### Audio Spectrum and Spectrogram

![](kdenlive-spectrogram-zeroproject.preview.png)

Audio Monitoring has arrived in kdenlive! Including detection of overmodulation. [Read more][12]

### I and Q lines for the Vectorscope

![](vectorscope-iq-lines.png)

Flesh tone lines for the Vectorscope.<br>Help you with color grading. [Read more][13]

### Stop Motion capture

![](stopmotion.jpeg)

A new widget allows you to easily do Stop Motion capture, capturing frames from your webcam or HDMI Decklink card. [Read more][14]


  [1]: https://www.mltframework.org/
  [2]: https://frei0r.dyne.org/
  [4]: https://sourceforge.net/projects/kdenlive/
  [5]: https://web.archive.org/web/20160322193501/https://kdenlive.org/user-manual/downloading-and-installing-kdenlive
  [6]: https://discuss.kde.org/tag/kdenlive
  [7]: https://web.archive.org/web/20160322193501/https://kdenlive.org/bug-reports
  [8]: /users/ttill/rotoscoping
  [9]: /users/ttill/perspective-image-placement
  [10]: /users/granjow/widget-layouts-kdenlive
  [11]: /users/granjow/writing-light-graffiti-effect
  [12]: /users/granjow/introducing-scopes-audio-spectrum-and-spectrogram
  [13]: /users/granjow/vectorscope-what-i-and-q-lines-are-good
  [14]: /users/j-b-m/coming-soon-your-desktop
