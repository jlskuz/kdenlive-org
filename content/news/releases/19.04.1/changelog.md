  * Search effects from all tabs instead of only the selected tab
  * Add missing lock in model cleanup. [Commit.](http://commits.kde.org/kdenlive/d6ee2612cc4a44b97166410b18feab07b346c12a)
  * Move levels effect back to main effects. [Commit.](http://commits.kde.org/kdenlive/54210884a2f8bfe8654e3d5ee54286f8c92bfeb8)
  * Fix crash closing project with locked tracks. Fixes #177. [Commit.](http://commits.kde.org/kdenlive/9353f7fa82d49b2b873a9c60e8c82b921b8a1b1f)
  * Speedup selecting bin clip when using proxies (cache original properties). [Commit.](http://commits.kde.org/kdenlive/fc81e1653ea7e007bbc17a0269b5f20b5c1e4f80)
  * Disable threaded rendering with movit. [Commit.](http://commits.kde.org/kdenlive/a048e250b86cc9e2de745df55f8bd77be9a1cfb7)
  * Fix wrong thumbnails sometimes displayed. [Commit.](http://commits.kde.org/kdenlive/7249f2fa72bc49274a31708136487ecb29ee5b07)
  * Ensure fades always start or end at clip border. [Commit.](http://commits.kde.org/kdenlive/34bed63e95747bc0314f38eff28864221b7cb716)
  * Fix loading of clip zones. [Commit.](http://commits.kde.org/kdenlive/86588bc24e7a686fae4239372f6fc32d1a522814)
  * Fix transcoding crashes caused by old code. [Commit.](http://commits.kde.org/kdenlive/395445bea728e6e7386f71c8c1cb6e674fcdcbe0)
  * Fix fades copy/paste. [Commit.](http://commits.kde.org/kdenlive/f3ac41ab8db8fd55b855f7482870d876fac23af1)
  * Fix broken fadeout. [Commit.](http://commits.kde.org/kdenlive/48dd96e08cd314441da482b67b23c7f9f2f26193)
  * Fix track red background on undo track deletion. [Commit.](http://commits.kde.org/kdenlive/ce23fec8122fdbf9a29e4e8228c9161138c84c5b)
  * Update appdata version. [Commit.](http://commits.kde.org/kdenlive/052e7b00a303d8a1ea00d33880813aef88952870)
  * Zooming in these widgets using CTRL+two-finger scrolling was almost. [Commit.](http://commits.kde.org/kdenlive/b7f5e7ce5bf5b19f32b49f34a9a04b43d6a5ec44) Fixes bug [#406985](https://bugs.kde.org/406985)
  * Fix crash on newly created profile change. [Commit.](http://commits.kde.org/kdenlive/1da40dbc4320c50ed09a611c6103aa6c3b5e6ae0)
  * Always create audio thumbs from original source file, not proxy because proxy clip can have a different audio layout. [Commit.](http://commits.kde.org/kdenlive/ff3097ac6c69c24f0879cd1eac114a8e51ae18b7)
  * Mark document modified when track compositing is changed. [Commit.](http://commits.kde.org/kdenlive/fcbfdb00b03eac5d7869b62c2c7f51d3ae3875d6)
  * Fix compositing sort error. [Commit.](http://commits.kde.org/kdenlive/5d6376f833d6dc6157d9132c0c96c0c77ce5d54b)
  * Fix crash opening old project, fix disabled clips not saved. [Commit.](http://commits.kde.org/kdenlive/0b58e790d1d792b4ae55c5ed36ecb782eeae724b)
  * Fix crash and broken undo/redo with lift/gamma/gain effect. Fixes #172. [Commit.](http://commits.kde.org/kdenlive/c900e5b3b7d5dab72299f5cbbff61920fc193b95)
  * Fix clip marker menu. Fixes #168. [Commit.](http://commits.kde.org/kdenlive/1a176696f9cfccb16b4e7f3560e4b9482c053eac)
  * Fix composition forced track lost on project load. Fixes #169. [Commit.](http://commits.kde.org/kdenlive/876d46d8b11f5fb2ed9c3919b22e1dad1e995187)
  * Fix spacer / remove space with only 1 clip. Fixes #162. [Commit.](http://commits.kde.org/kdenlive/ad27e4b9844eadbf4a8c1ed4d6312df4d4015a45)
  * Fix timeline corruption (some operations used a copy of master prod instead of track producer). [Commit.](http://commits.kde.org/kdenlive/baf386fb4f980e7eeabaadcac9240640d9d80f9b)
  * Check whether first project clip matches selected profile by default
  * Renderwidget: Use max number of threads in render. [Commit.](http://commits.kde.org/kdenlive/4f774201ddc8e3a88c01f7c0758dfe1568441bd9)
  * Fix razor tool not working in some cases. Fixes #160. [Commit.](http://commits.kde.org/kdenlive/8580bf212ade31f65b1c7745b628f370c0049ad0)
  * Better os detection macro. [Commit.](http://commits.kde.org/kdenlive/7b2afc2618df79922a5bc4c1c31fcbb1676f22a9)
  * Remove crash, not solving 1st startup not accepting media (see #117). [Commit.](http://commits.kde.org/kdenlive/48a5cdb420e22dd0938f9159ddb49f0f93147d00)
  * Remove unneeded unlock crashing on Windows. [Commit.](http://commits.kde.org/kdenlive/0ed3dc18863d956e03b2eee2a19b496be7194d0c)
  * Some fixes in tests. [Commit.](http://commits.kde.org/kdenlive/c4f94791315303f7a798ece65be5b873eeaa8d96)
  * Forgotten file. [Commit.](http://commits.kde.org/kdenlive/bd22dce8d09fda05a11d3702dd785ebbfaba6c8d)
  * Improve marker tests, add abort testing feature. [Commit.](http://commits.kde.org/kdenlive/a6e78fe033a8be8f99643f79290efdc5e8ed6fd9)
  * Add tests for unlimited clips resize. [Commit.](http://commits.kde.org/kdenlive/b1b811676bb8f4d6b330c57f1d31e3e2bcb7a395)
  * Small fix in tests. [Commit.](http://commits.kde.org/kdenlive/d6cf6ca01296dc77b6112c4087961d92bfe9c576)
  * Fix AppImage audio recording (switch from wav to flac). [Commit.](http://commits.kde.org/kdenlive/ac43da1c34eb03a22be31a0085103bea3ffe6864)
  * Dont remember clip duration in case of profile change. Fixes #145. [Commit.](http://commits.kde.org/kdenlive/6dd5e215ce804a50ae4cf66b82f77fbb5a4407ab)
  * Fix spacer broken when activated over a timeline item. [Commit.](http://commits.kde.org/kdenlive/a1b45e5cc2389d316d369622031359d804d4d8bf)
  * Improve detection of composition direction. [Commit.](http://commits.kde.org/kdenlive/1331f66fc62d6695a840021ae031105ea1eb60d2)
  * Unconditionnaly reload producers on profile change. Related to #145. [Commit.](http://commits.kde.org/kdenlive/ed107a46f0547c34f4173bd46fff024c09204671)
