---
author: Farid Abdelnour
date: 2019-05-11T15:08:38+00:00
aliases:
- /en/2019/05/kdenlive-19-04-1-released/
- /it/2019/05/kdenlive-versione-19-04-1/
- /de/2019/05/kdenlive-19-04-1-freigegeben/
---

The Kdenlive team is happy to announce the first minor release of the 19.04 series fixing 39 bugs. The feedback by the community as well as the effort put in reporting issues has been very helpful and we encourage to keep it up. We expect to finish polishing in the coming months in order to focus on our planned pro features.

Kdenlive 19.04.1 fixes some important issues, so all 19.x users are encouraged to upgrade. Easiest way to test it is through the AppImage, available from the KDE servers as usual: [19.04.1 AppImage][2]

The Appimage also contains some last minute fixes that will be in 19.04.2 since we are still busy fixing some remaining issues after our big refactoring. This Appimage should fix the rendering and timeline preview issues recently reported, and the 19.04.1 fixes are listed below.

Other news: work continues to improve OpenGL support, fixes by the team have been merged into MLT improving speed and the Titler will be rewritten as a [GSOC project][1].


 [1]: https://summerofcode.withgoogle.com/projects/#5398631413186560
 [2]: https://download.kde.org/Attic/kdenlive/19.04/linux/kdenlive-19.04.1c-x86_64.appimage
