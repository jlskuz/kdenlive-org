---
title: Kdenlive 15.04.2 information page
date: 2015-06-03
---
 
Kdenlive 15.04.2 was released with KDE Applications 15.04.2 on the 3rd of June 2015.

The source code can be downloaded from KDE's servers.

This is a bugfix release, with the following changes:

* Fix timeline corruption when zooming with mouse wheel over a selected item
* Fix timeline corruption when scrolling while resizing an item

Below is a list of reported issues that were fixed:

* [Bug 346491][1] 15.04.0 Transition (yellow) bar appears behind clip when created - can't view or edit it
* [Bug 348151][2] Adding a transcode profile doesn't get saved

  [1]: https://bugs.kde.org/show_bug.cgi?id=346491
  [2]: https://bugs.kde.org/show_bug.cgi?id=348151
