---
title: Kdenlive 19.12.1 is out
author: Farid Abdelnour
date: 2020-01-10T09:00:35+00:00
aliases:
- /en/2020/01/kdenlive-19-12-1-is-out/
- /de/2020/01/kdenlive-19-12-1-ist-freigegeben/
---

Kdenlive 19.12.1 is out with many bug fixes and usability improvements. For the whole 19.12 release cycle we will continue focusing on polishing the rough edges in preparation the next major release in April.
