* Fix autosave not working when opening project file from command line or click in file browser. [#348674](https://bugs.kde.org/348674)
* Re-open development for 15.04.3 release. [Commit.](http://commits.kde.org/kdenlive/a3e6a47fc78ff1276e31313a246025c0756c8ed0) 
* Fix zoom broken after context menu shown in timeline. [#348671](https://bugs.kde.org/348671)
