---
title: Kdenlive 19.12 beta
author: Farid Abdelnour
date: 2019-11-25T16:34:11+00:00
aliases:
- /en/2019/11/kdenlive-19-12-beta/
- /it/2019/11/kdenlive-19-12-beta-2/
- /de/2019/11/kdenlive-19-12-beta-3/
---

![](Kdenlive1912BETA.png)

kdenlive 19.12 beta is out with many bug fixes and improvements. The highlights include:

  * New audio mixer
  * Bin monitor redesign
  * Performance and usability improvements
  * Many Windows fixes
  * Master effects
  * Re-implement scrolling trough compositions.

Check it out and report any issues!
