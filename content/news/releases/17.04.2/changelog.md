* Fix crash changing title font for 2nd time. <a href="http://commits.kde.org/kdenlive/94b1b432ecd06d6ec71ebf3ee69942b13815b8d3">Commit.</a> Fixes bug <a href="https://bugs.kde.org/379606">#379606</a>
* More Windows rendering fixes. <a href="http://commits.kde.org/kdenlive/8f788c1d13e08fe8bfed8858ee70933f8d42d1fe">Commit.</a>
* Fix init of geometryWidget. <a href="http://commits.kde.org/kdenlive/3ab863f0d58594802b53a36d0002d9415b9622ba">Commit.</a> See bug <a href="https://bugs.kde.org/379274">#379274</a>
