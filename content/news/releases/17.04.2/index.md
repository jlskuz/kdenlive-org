---
author: Farid Abdelnour
date: 2017-06-09T14:51:52+00:00
aliases:
- /en/2017/06/kdenlive-17-04-2-released/
---
A minor release is out fixing a couple crashes in the titler and Affine transition as well as improving a Windows rendering issue. Note that all focus and energy are still on the timeline refactoring due for 17.08. Soon we will provide an AppImage version for testing, stay tuned.
