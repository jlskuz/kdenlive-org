---
title: 'Kdenlive 0.9.10… Yet Another Bugfix Release'
author: Vincent Pinon
date: 2014-10-01T07:53:00+00:00
aliases:
- /node/9184
- /discover/0.9.10
# Ported from https://web.archive.org/web/20160322050221/https://kdenlive.org/node/9184
# Ported from https://web.archive.org/web/20141014140139/http://www.kdenlive.org/discover/0.9.10
---

4 month after 0.9.8, here comes a new release of your favorite video editor.

Don't look for shiny new features: development has been focused at removing unreliable behaviours.  
You will only find new VBR export settings, video stabilizer, better look on high DPI screens, but mainly no more crashes?

You can download the source tarball from [KDE servers][1], and have a look at the changelog below.

This being done, we will be able to focus on things planned in Randa: transfer to KDE infrastructure, code cleaning, port to KF5… Big changes hidden behind small words!

## Changes

* handle VBR encoding profiles, and use it!
* video stabilizer: added new "vid.stab", removed older ones
* environment variable MLT_PREFIX overrides profiles & melt path
* several fixes for high dpi screens
* change proxy profile, obsolete with libav
* add SVG to image formats for slideshows
* restore project render bitrate (#3326)
* show proxy name in clipproperties
* add link to MLT doc in render edit dialog (#3355)

## Bug fix

* update maximum track height (#3241)
* resize render path box (#3300)
* change transition settings layout (#3336)
* heat colormap in spectrogram (#3142)
* fix clip monitor switch on proxy creation end (#3308)
* fix clip jobs (stabilize) operating on proxies (#3337)
* fix timeline corruption by clip resize end + zoom change
* fix title clip duration not taken into account (#3309)
* fix timeline thumbs flicker when fully zoomed (#3247)
* fix thumbnailer aspect ratio
* fix fades for split audio (#3323)
* fix transcoding (#3334)
* fix 'Clip in Timeline' when project monitor is inactive (#3261)
* fix audio analysis (use dynamic memory instead of stack)
* fix render end notification
* fix render timecode overlay (#3260)
* set tooltip for monitor pause button (#3256)
* fix slideshow clips adding 1 frame (#3289)
* fix archive project icon (#3325)
* fix compilation on armhf (#3345)
* don't allow creating keyframes for "fixed" filters (#2956)
* re-enable compilation on KDE < 4.5 (squeeze) (#3319)
* re-enable Clip menu (fix bug #3347)
* fix screen recording failure (#3358)
* fix crash detected with undo/redo (bug #3240)
* fix crash cutting group on a clip edge (#3312, #3350)
* fix crash on image sequence (#3331)
* fix crash in tracks config editor (#3367)
* fix crash on audiofiles
* fix crash on opening project file with images
* fix warnings from static analyzers (cppcheck, krazy)
* sort source files, easier to understand (for new developpers)


  [1]: http://download.kde.org/stable/kdenlive/0.9.10/src/kdenlive-0.9.10.tar.bz2.mirrorlist
