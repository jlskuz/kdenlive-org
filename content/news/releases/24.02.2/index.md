---
date: 2024-04-15T13:53:10+00:00
title: Kdenlive 24.02.2
discourse_topic_id: 14097
author: Farid Abdelnour
aliases:
- /en/2024/04/kdenlive-24-02-2-released/
- /it/2024/04/kdenlive-versione-24-02-2/
- /de/2024/04/kdenlive-24-02-2/
---

The second maintenance release of the 24.02 series is out with performance optimizations when moving clips in the timeline and across multiple project bins, packaging improvements to macOS and Windows versions and fixes to copy/paste of effects, rotoscoping, Nvidia encoding among others.
