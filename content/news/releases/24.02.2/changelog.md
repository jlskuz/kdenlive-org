* Fix blurry folder icon with some project profiles. [Commit.](https://commits.kde.org/kdenlive/e11b50b3fecb96939b6323b5df4d8fe6817ec6eb)
* Fix timeline not following playhead.  [Commit](https://commits.kde.org/kdenlive/7e372234d9cf0835bef3997187dcc44013e13696)
* When copy/paste effects from a group, only paste effects for the active clip. [Commit](https://commits.kde.org/kdenlive/5e76cb365941a10e5d6e96e1abbf2b07375e848c). Fixes bug [#421667](https://bugs.kde.org/421667)
* Optimize group move (don&#8217;t update clip position twice). [Commit](https://commits.kde.org/kdenlive/19600e4c0e8e34f00f67eaf3e5834a38eb685887)
* Fix nvidia encoding. [Commit](https://commits.kde.org/kdenlive/3a3e3894fb8e82a1f152de8e9ec9c0ed7d13ff29).
* Multiple improvements for timeline keyboard grab (don&#8217;t test each frame on a move, scoll timeline accordingly, don&#8217;t lose focus on app switch). [Commit](https://commits.kde.org/kdenlive/b07046873f5e84dbaee9b92b86044e9521e2812c).
* Update to last commit: only sync shortcuts if there was a change. [Commit](https://commits.kde.org/kdenlive/baa93e0b24909addddf2374d2569d8c7a52ee6e3).
* Fix: editing toolbar config discards newly set keyboard shortcuts. [Commit](https://commits.kde.org/kdenlive/6a5aa6f92450bb69a8e849eb2b4947539c0961e3).
* Increase Qt6 limit for max image size. [Commit](https://commits.kde.org/kdenlive/36d9d2d76f329185d4daab1f4e7c2e4c6990ddb7). See bug [#484752](https://bugs.kde.org/484752).
* Fix: Ensure secondary bins have a title bar when needed and that the dock widgets list is always correctly sorted. [Commit](https://commits.kde.org/kdenlive/f820296578ce66f6fcec53aa49c17f08107745bf).
* Don&#8217;t perform bin block twice on main bin. [Commit](https://commits.kde.org/kdenlive/befe0858dbae8f91e8db864116abfdefd1da953b).
* Fix: lag moving clips from one bin to another and unneeded monitor clip reload. [Commit](https://commits.kde.org/kdenlive/fc2c026d9c38b044c44f54878531539673ca61cc).
* Fix crash and color theme broken on Windows when opening a project by double click. [Commit](https://commits.kde.org/kdenlive/9041fef0429708ec9f064125c8a63125971b5ccc).
* Try to fix empty monitor when switching to/from fullscreen on Mac. [Commit](https://commits.kde.org/kdenlive/e246dac2975ded0c5c3be0fefee9e063ad3c2fbf).
* Fix mem leak on save. [Commit](https://commits.kde.org/kdenlive/707bc0f78e22f8edf83b508b57f403e2e8b9f994).
* Add more locks around xml producer, fix autosave triggered on project open. [Commit](https://commits.kde.org/kdenlive/37adf37a97fc129e99ffd598b8791d0063b72de4).
* Mediabrowser: ensure thumbnails are generated after changing the view. [Commit](https://commits.kde.org/kdenlive/5c77705ffa401ae034ed9dd9515d47c3fef57585).
* Enable video thumbnails in media browser for Win/Mac. [Commit](https://commits.kde.org/kdenlive/8cfa2ea978fb1c1e2d1910adebbe89c08a6978ab).
* Fix: don&#8217;t propose existing name for new sequence. [Commit](https://commits.kde.org/kdenlive/185cce37a522d1e2abceab47fe02d2b115c94715). Fixes bug [#472753](https://bugs.kde.org/472753).
* Fix crash in sequence clip thumbnails. [Commit](https://commits.kde.org/kdenlive/1bef4dad70ce4fe7689e8faa9931537b1ff6ac99). See bug [#483836](https://bugs.kde.org/483836).
* Ensure we never reset the locale while an MLT XML Consumer is running (it caused data corruption). [Commit](https://commits.kde.org/kdenlive/ac078f004298f42807e2a8beed476486ab96067a). See bug [#483777](https://bugs.kde.org/483777).
* Add icon data to shared-mime-info. [Commit](https://commits.kde.org/kdenlive/a1cabf8ef7b8abf80c48c76c60feab69b7cba498).
* Fix: favorite effects menu not refreshed when a new effect is set as favorite. [Commit](https://commits.kde.org/kdenlive/941104faf417a36f39755226cd469d55929cc78f).
* Fix: Rotoscoping not allowing to add points close to bottom of the screen. [Commit](https://commits.kde.org/kdenlive/0d806055868f976f8a21e24d66f79603011f344d).
* Fix: Rotoscoping &#8211; allow closing shape with Return key, don&#8217;t discard initial shape when drawing it and seeking in timeline. [Commit](https://commits.kde.org/kdenlive/8ce440f539db7a19f3ec9a45641a48bfe7ab59ba). See bug [#484009](https://bugs.kde.org/484009).
* Fix: cannot translate the &#8220;P&#8221; for Proxy in timeline. [Commit](https://commits.kde.org/kdenlive/5ab2de05066f31cdf7934a713679dccbb2b176af). Fixes bug [#471850](https://bugs.kde.org/471850).
* Fix white background and blank monitor on Windows after going back from fullscreen. [Commit](https://commits.kde.org/kdenlive/efbfbe63a81ee633597310bb32ab4593cdb55c82). Fixes bug [#484081](https://bugs.kde.org/484081).
* Fix wrong KDEInstallDirs on Windows. [Commit](https://commits.kde.org/kdenlive/dd6e81cf47418ac4df6c0f4612ccde631bccac6d).
* Fix recent commit not allowing to open files. [Commit](https://commits.kde.org/kdenlive/c03365762bad3e20f7b6511b3dc5b99cf900c459).
* Don&#8217;t crash opening aa corrupted project file with no tracks. [Commit](https://commits.kde.org/kdenlive/41a5320e89fc101a763ef7495bfa816eb97b7867).
* Fix: cannot move compositions properly in timeline with Qt6. [Commit](https://commits.kde.org/kdenlive/c83404329ac6c0d3ddf9da658542b7c4417020ca). Fixes bug [#484062](https://bugs.kde.org/484062).
* Proxy clip: highlight proxy in file manager when opening the folder. [Commit](https://commits.kde.org/kdenlive/4298a42125ccb43c1879a6da91d00e39407e6147).
