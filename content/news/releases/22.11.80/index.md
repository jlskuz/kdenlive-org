---
title: Kdenlive 22.12 beta available
author: Farid Abdelnour
type: post
date: 2022-11-15T18:28:12+00:00
pre_downloads:
- platform: windows
  sources:
  - name: Installable
    url: https://download.kde.org/unstable/kdenlive/22.12/kdenlive-22.12_Beta1.exe
  - name: Standalone
    url: https://download.kde.org/unstable/kdenlive/22.12/kdenlive-22.12_standalone_Beta1.exe
- platform: linux
  sources:
  - name: AppImage
    url: https://download.kde.org/unstable/kdenlive/22.12/kdenlive-22.12-beta1.AppImage
- platform: macos
  sources:
  - name: Intel
    url: https://download.kde.org/unstable/kdenlive/22.12/kdenlive-22.12-beta1.dmg
---

Today we also release the 22.12 BETA, please give it a spin and let us know if you encounter any issues.
