* Fix autosave: work around KAutoSaveFile bug with non-ASCII chars. [Commit.](http://commits.kde.org/kdenlive/630f7bbfcf7fed53de85b3bccc2051da13391639) 
* Get ready for transform centered rotation. [Commit.](http://commits.kde.org/kdenlive/529e7a235b271430ddb3d3a7c594fe5b15d2495c) 
* Fix keyframes unseekable on bin effects. [Commit.](http://commits.kde.org/kdenlive/f4c9a045f281d11faa5c797263241c3343ce20d8) 
* Fix lift to handle negative values (requires latest MLT version). [Commit.](http://commits.kde.org/kdenlive/4a7088e8b052b3818b5be7e45a0b779e38a62bfb) 
* Prefer SDL2 to SDL1 (dropped by FFmpeg and so MLT). [Commit.](http://commits.kde.org/kdenlive/04541ba0dd0d11a510e0c252292c77df3a794109) 

