---
author: Farid Abdelnour
date: 2017-10-14T02:11:59+00:00
aliases:
- /en/2017/10/kdenlive-17-08-2-released/
---
The second minor release of the 17.08 series is out adding a _rotate from image center_ option in the Transform effect among other usability improvements. In other news the dev team continues making progress for the much anticipated 17.12 release. Start the countdown!
