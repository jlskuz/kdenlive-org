  * Clear undo history after deleting a sequence since undoing more would crash. [Commit.][3]
  * Delay settings dialog script checks to make Settings dialog appear faster. [Commit.][4]
  * Fix incorrect argument type. [Commit.][5]
  * Fix Whisper combobox device too narrow to read text. [Commit.][6]
  * Fix startup crash caused by last commit. [Commit.][7]
  * Fix tests. [Commit.][8]
  * Fix broken click in timeline after dropping effect on a clip. [Commit.][9]
  * When finding a timeline clip from bin context menu, also activate its track. [Commit.][10]
  * Fix possible crash on add clip. [Commit.][11]
  * Rebuild timewarp and timeremap sequence producers if missing on load. Display sequence tmp files in the cache data dialog. [Commit.][12]
  * Add missing create clip entries in project menu. [Commit.][13]
  * Fix crash on app close. [Commit.][14]
  * Fix compile warning. [Commit.][15]
  * Cleanup creation of media browser, store last location in project settings. [Commit.][16]
  * Add config buttons to Stabilize and Speed clip jobs. [Commit.][17]
  * Make Bin filter use an OR when filtering in the same category. [Commit.][18]
  * Cleanup unused code. [Commit.][19]
  * Add proper UTF8 suffix to locale. [Commit.][20]
  * Fix warning. [Commit.][21]
  * Fix encoding error on keyframes paste. [Commit.][22]
  * Copying keyframes with no selection should copy all keyframes, not none, fix crash on paste. [Commit.][23]
  * Fix crash when a missing clip is on an audio track (caused by link_swresample). [Commit.][24]
  * Don&#8217;t freeze ui when fixing missing clips on project open. [Commit.][25]
  * Bin: allow filtering by sequence type. [Commit.][26]
  * Correctly recover projects with timewarp producers having incorrect Kdenlive:id. [Commit.][27]
  * Fix recursive search on missing project files. [Commit.][28]
  * Fix proxied clips using producer instead of chain. [Commit.][29]
  * Fix missing clips incorrectly detected on load. [Commit.][30]
  * Fix titler outline covering text. Requires latest MLT git. [Commit.][31]
  * Fix changing custom project folder does not enable Apply Settings. [Commit.][32]Fixes bug [#468156][33]
  * Reuse thumbs producer for thumbnail creation. [Commit.][34]
  * Fix project profile not checked after first clip transcoding. [Commit.][35]
  * Fix fps not appended to transcoded clip name. [Commit.][36]
  * Don&#8217;t incorrectly show warning message when disabling project proxies. [Commit.][37]
  * Fix thumbnail not updated on rotate. [Commit.][38]
  * Refactor save title to use QSaveFile instead of QTemporaryFile + KIO job. [Commit.][39]See bug [#467917][40]
  * Fix bin filter button not working with unused filter. [Commit.][41]Fixes bug [#468038][42]
  * Fix incorrect tooltip in Clip Monitor toolbar. [Commit.][43]
  * Subtitle style, use QStringList to avoid comma separator mistakes. [Commit.][44]
  * Fix subtitle position breaking opaque background. [Commit.][45]Fixes bug [#467745][46]
  * Fix sequence clips description. [Commit.][47]
  * Made descriptions of projectclips editable again. [Commit.][48]
  * Nicer look for clip monitor jobs overlay. [Commit.][49]
  * Hide on monitor clip jobs when no clip is selected. [Commit.][50]
  * Improve timeline scrolling when dragging from project bin, fix vertical scroll on drag. [Commit.][51]
  * Don&#8217;t draw a frame around timeline when tabs shown. [Commit.][52]
  * Fix another crash closing sequence clip. Related to #1401. [Commit.][53]
  * Fix various crashes on quit or sequence clip deletion. [Commit.][54]
  * Ensure audio is correctly added / removed to timeline sequence when audio tracks are added/removed. [Commit.][55]
  * Workaround MLT freeze on mp3 files with png cover art. [Commit.][56]
  * Ensure the &#8220;Monitor Config&#8221; menu does not replace Settings dialog on Mac, update syntax for KStandardAction. [Commit.][57]
  * Fix audio detection on playlist clips, fix creating library item from selection messes transition tracks. [Commit.][58]
  * Select new Library item when added. [Commit.][59]
  * Attempt to fix missing icons on hover. [Commit.][60]
  * Fix monitor background color on Wayland. [Commit.][61]
  * Add a thumbnailer-deprecated folder for KF < 5.100. [Commit.][62]
  * Fix tests. [Commit.][63]
  * Fix composition paste broken. [Commit.][64]
  * When saving project with cache data in project folder, abort proxy operation. [Commit.][65]
  * Fix crash reordering track effects. [Commit.][66]
  * Resize composition on expand playlist zone. [Commit.][67]
  * Expand : correctly crop in/out of the sequence on expand. [Commit.][68]
  * Fix compositions broken in library clips. [Commit.][69]
  * Ensure new clips are inserted in correct library folder when another clip is selected. [Commit.][70]
  * Ensure clips added to library go inn the active folder. [Commit.][71]
  * Refactor python install script (used for opentimeline and speech recognition engine) to run in non blocking mode and display output to user. [Commit.][72]
  * Add enum to avoid confusion in Settings Pages. [Commit.][73]
  * Fix crash on sequence close/reopen and project close. [Commit.][74]
  * [Clip Jobs] Multiple fixes and improvements. [Commit.][75]
  * Ensure images are using an MLT::Producer, not a Chain. [Commit.][76]
  * Fix razor line incorrect offset on subtitle track. [Commit.][77]
  * Fix crashes with duplicated sequences, incorrect warning message about missing audio tracks when opening new sequence. [Commit.][78]
  * Create sequence from selection: add context menu entry, propose the correct number of tracks, fix bug canceling creation. [Commit.][79]
  * Various fixes for the clip job manager. [Commit.][80]
  * Allow nesting a timeline sequence (from the timeline menu > Create sequence with selection). [Commit.][81]
  * Better hig conformance for speech config. [Commit.][82]
  * Add check config button to Whisper settings page. [Commit.][83]
  * Fix Qt6 compilation. [Commit.][84]
  * Don&#8217;t show log button if log is empty. [Commit.][85]
  * Fix compilation with KF < 5.100. [Commit.][86]
  * Several fixes and improved feedback for whisper speech recognition. [Commit.][87]
  * Add &#8220;Cut subtitle after first line&#8221;. [Commit.][88]
  * Ensure sequence clips are not counted as unused and not deleted by project clean. [Commit.][89]
  * Next try to fix build for KF < 5.100. [Commit.][90]
  * Try to fix build for KF < 5.100. [Commit.][91]
  * Fix crash trying to duplicate first unsused sequence. [Commit.][92]
  * Add support for whisper speech recogition engine for automated subtitling. [Commit.][93]Fixes bug [#467172][94]
  * [Code Gardening] Another round to fix compiler warning. [Commit.][95]
  * [Code Gardening] more fixes. [Commit.][96]
  * [Code Gardening] Fix compiler warnings. [Commit.][97]
  * Don&#8217;t advertise GUI as built before all main widgets are really built &#8211; as advised by Ondrej Popp. [Commit.][98]
  * Fix audio and target tracks in sequence clips. [Commit.][99]
  * Remove unused param. [Commit.][100]
  * Move Full Color Range rendering to a checkbox in render dialog. [Commit.][101]
  * Fix drag and drop erratic behavior. [Commit.][102]
  * Fix timecode display with fps > 100. [Commit.][103]Fixes bug [#466486][104]
  * Disable multicam mode when switching timeline tab or closing the project. [Commit.][105]
  * Fix crash dragging grouped clips. [Commit.][106]
  * Fix possible crash on new project. [Commit.][107]
  * Fix color properties name. [Commit.][108]
  * Add some debug if file loading fails. [Commit.][109]
  * What&#8217;s This, second round, fix typos. [Commit.][110]
  * [Effects] Color params are keyframable since MLT 7.12. [Commit.][111]
  * Bump minimum MLT requirement to 7.14.0. [Commit.][112]
  * Attempt to fix full luma range render, add a &#8220;color range&#8221; parameter to render profiles. [Commit.][113]
  * Allow adding a playlist with a different fps to a project (allowed since MLT 7.14). [Commit.][114]
  * When subtitle encoding cannot be confidently detected, default to UTF-8. Blindly trust results for tests. [Commit.][115]
  * Fix subtitle preview not displayed when opening the import dialog. [Commit.][116]
  * Subtitles import: allow overriding detected codec through a list of available codecs, show preview to make choice easier. [Commit.][117]
  * Add timer effect. [Commit.][118]
  * Fix detection of audio in timeline/playlist clips. [Commit.][119]
  * Add button to create new timeline sequence in timeline&#8217;s tab bar. [Commit.][120]
  * Fix projects aways opening on first sequence. [Commit.][121]
  * Fix speed effect on sequence clips. Still seems a bit crashy though&#8230; [Commit.][122]
  * Fix timeline sequence with speed effect set as invalid on project close. [Commit.][123]
  * Fix regression in last commit (sequence bin clip not synced with its timeline). [Commit.][124]
  * Refresh sequence clip thumbnail when switching tab, only refresh sequence clip if the sequence changed. [Commit.][125]
  * Allow setting a bin sequence clip thumbnail from the project monitor context menu (set current image as thumbnail). [Commit.][126]
  * Add context menu option to define default sequences folder. [Commit.][127]
  * Store Bin zoom level in project settings. Related to #1401. [Commit.][128]
  * Fix bin clip renaming inconsistencies. Now you cannot set an empty name on a sequence clip. [Commit.][129]
  * Double click a sequence now opens its tab and seeks to the correct position. [Commit.][130]
  * Fix timeline tab settings not correctly stored (zoom, scroll position, zone). [Commit.][131]
  * Fix crash dragging a clip effect onto another timeline tab, related to #1401. [Commit.][132]
  * Use default number of A/V tracks when creating a new sequence. [Commit.][133]
  * Fix duplicate sequence clip and cleanup. [Commit.][134]
  * Revert prefill change until properly fixed in MLT. [Commit.][135]
  * Fix clip in timeline menu when switching tab. [Commit.][136]
  * Open/close a timeline tab is now integrated with undo/redo. [Commit.][137]
  * When a sequence clip had no audio tracks, don&#8217;t insert an audio part when dragging in another timeline. [Commit.][138]
  * Fix script rendering subtitles broken. [Commit.][139]Fixes bug [#466232][140]
  * Fix timeline menu contains duplicate/broken entries. [Commit.][141]
  * Display sequence tracks count in clip properties widget. [Commit.][142]
  * Fix confusing use of clip_type property and small mem leak in sequence creation. [Commit.][143]
  * Fix sequence clips not deleted when closing project. [Commit.][144]
  * Fix tests. [Commit.][145]
  * After a crash, if movit is enabled propose to disable it instead of deleting the config file. [Commit.][146]
  * Consistent use of Mlt::Profile to build MLT objects, fixes a few memleaks. [Commit.][147]
  * Fix tests. [Commit.][148]
  * Fix crash on new project, timewarp and thumbnails for sequence clips. [Commit.][149]
  * Fix white thumb on newly created sequence clips. [Commit.][150]
  * Convert the last emit to Q_EMIT. [Commit.][151]
  * Fix build with KF < 5.98.0. [Commit.][152]
  * Fix test. [Commit.][153]
  * Unfortunately I missed a bunch of stuff in the last commits. [Commit.][154]
  * Enable latest version of KDECompilerSettings. [Commit.][155]
  * [Code Gardening] Port emit, signals, slots to Q_*. [Commit.][156]
  * [Code Gardening] Port away from Qt foreach. [Commit.][157]
  * Port away from deprecated KDeclarative. [Commit.][158]
  * [CD] Add GitLab jobs for AppImage and Windows binary. [Commit.][159]
  * What&#8217;s This, second round. [Commit.][160]
  * [Flatpak] Increase timeout for flatpak. [Commit.][161]
  * [Archive Project] Don&#8217;t crash on error, show more useful messages. [Commit.][162]
  * Docs: specify version 7 of mlt packages in build instructions. [Commit.][163]
  * Allow copy/paste of a sequence clip between projects. [Commit.][164]
  * Fix transcoding for sequence clips. [Commit.][165]
  * Fix copy/paste of sequence clips in same project. [Commit.][166]
  * Fix Bin clip deletion broken regression. [Commit.][167]
  * Double click a sequence clip in timeline will open its timeline tab. [Commit.][168]
  * Fix deleting of sequence clips. [Commit.][169]
  * Correctly store newly created sequences and update window title on sequence rename. [Commit.][170]
  * Display timeline sequence name in window title if there is only 1 sequence opened. [Commit.][171]
  * Fix crash opening project with a bin clip missing a kdenlive:id reference or missing folder. [Commit.][172]
  * Fix project archiving with nested timelines. [Commit.][173]
  * Fix check profile on first clip broken and various regressions linked to switch from MLT producer to chain. [Commit.][174]
  * Fix closing/reopening of timeline tabs. [Commit.][175]
  * Timelinepreview test: print out available formats on failure. [Commit.][176]
  * Fix use-after-free in ThumbnailCache::remove(). [Commit.][177]Fixes bug [#463764][178]
  * If timelinepreview test fails, list available ffmpeg formats. [Commit.][179]
  * [Tests] Fix rendermodeltest. [Commit.][180]
  * Ensure there are always guide categories available. [Commit.][181]
  * Do not allow to accept MarkerDialog if no category available. [Commit.][182]
  * Fix disappearing timeline cursor. [Commit.][183]
  * Fix missing line causing subtitle crash. [Commit.][184]
  * [nightly flatpak] Update more dependencies. [Commit.][185]
  * [nightly flatpak] Split out dependencies. [Commit.][186]
  * [nightly flatpak] update dependencies. [Commit.][187]
  * Enforce timeline preview parameters for tests. [Commit.][188]
  * Fix composition text. [Commit.][189]
  * More tests cleanup. [Commit.][190]
  * Ensure timeline preview test uses a progressive profile for mpeg rendering. [Commit.][191]
  * Indentation fixes. [Commit.][192]
  * Cleanup timeline preview test. [Commit.][193]
  * Fix tests. [Commit.][194]
  * Refactoring JobSent method. [Commit.][195]
  * Add a basic subtitles overlap test. [Commit.][196]
  * Switch from Producer to Chain to support improved audio normalizer. [Commit.][197]
  * Fix subtitle track not loading correctly on project open. [Commit.][198]
  * Fix track compositing with black background. [Commit.][199]
  * Fix subtitle track not showing on first click of &#8220;Show subtitle track&#8221;. [Commit.][200]
  * Fix timeline duration when opening older project file. [Commit.][201]
  * Ensure project notes works with multiple timelines. [Commit.][202]
  * [Flatpak nightly] Add x-checker-data for auto update detection. [Commit.][203]
  * Fix timeline properties lost on older KdenliveDoc opening (zone, guides, groups,&#8230;). [Commit.][204]
  * Fix initial timeline settings (zone, etc). [Commit.][205]
  * Bump Kdenlive document version. [Commit.][206]
  * Ensure nested sequences have a transparent background. [Commit.][207]
  * Fix loading of timeline preview. [Commit.][208]
  * Fix rendering of nested sequences. [Commit.][209]
  * Ensure a timeline sequence never accepts a drop of another sequence embeding it. [Commit.][210]
  * Refactoring of the Kdenlive project file format to correctly handle nested timelines. [Commit.][211]
  * Fix typo in Contrast Adaptive Sharpen Filter. [Commit.][212]Fixes bug [#465068][213]
  * Ensure subtitle models are correctly shown/hidden for each timeline tab. [Commit.][214]
  * Improve audio on playback start (backported from Shotcut). [Commit.][215]
  * Disable some clip actions for sequences that do not make sense. [Commit.][216]
  * Fix crash opening project. [Commit.][217]
  * Use edit-select-\* instead of kdenlive-\*select-all. [Commit.][218]
  * Use snap instead of kdenlive-snap. [Commit.][219]
  * Use application-menu instead of kdenlive-menu. [Commit.][220]
  * Use edit-delete instead of kdenlvie-deleffect. [Commit.][221]
  * Use selection-raise/lower instead of kdenlive-up/down. [Commit.][222]
  * Use lock and unlock instead of kdenlive-*lock. [Commit.][223]
  * Replace kdenlive-align-\* icons with align-\*. [Commit.][224]
  * Replace kdenlive-zindex-\* icons by object-order-\*. [Commit.][225]
  * Fix handling of group data for secondary timelines. [Commit.][226]
  * Fix changing profile crash. [Commit.][227]
  * Save sequence specific properties. [Commit.][228]
  * Fix crash adding a subtitle after changing timeline tab. [Commit.][229]
  * Fix various load/save issues. [Commit.][230]
  * Ask for sequence name on add sequence. [Commit.][231]
  * Ensure we render the active timeline tab. [Commit.][232]
  * Show render duration in render dialog, correctly update on timeline tab switch. [Commit.][233]
  * Fix monitor zone out and duration indicator. [Commit.][234]
  * Fix Qt6 compilation. [Commit.][235]
  * Fix reopening timeline tabs on project open. [Commit.][236]
  * Fix track not active and targets not set when switching to new timeline tab. [Commit.][237]
  * Fix reopening project with nested timeline. [Commit.][238]
  * Fix tests. [Commit.][239]
  * Ensure initial playlist duration is correctly set. [Commit.][240]
  * Create initial sequence on startup, store onened timeline tabs in project file. [Commit.][241]
  * Fix crash on open new project. [Commit.][242]
  * Add more &#8220;What&#8217;s this?&#8221; texts. [Commit.][243]
  * Don&#8217;t use deprecated KMessageBox::sorry. [Commit.][244]
  * Don&#8217;t try setting marker model on empty timeline. [Commit.][245]
  * Fix tests. [Commit.][246]
  * Store marker model in monitor instead of fetching it for each frame. [Commit.][247]
  * Try to fix timewarp test. [Commit.][248]
  * Fix tests. [Commit.][249]
  * Start fixing tests. [Commit.][250]
  * Make subtitleModel depend on TimelineModel instead of KdenliveDoc and TimelineController. [Commit.][251]
  * Move timeline preview to timelinemodel to prepare multiple timeline. [Commit.][252]
  * Rename Timeline clips to &#8220;Sequences&#8221;. [Commit.][253]
  * Don&#8217;t allow inserting a timeline sequence onto itself. [Commit.][254]
  * Use Timeline 1 for first timeline tab. [Commit.][255]
  * Fix editing extra timeline broken after reopen. [Commit.][256]
  * Fix saving project with a secondary timeline opened. [Commit.][257]
  * Implement nesting v2. [Commit.][258]
  * Don&#8217;t try setting marker model on empty timeline. [Commit.][259]
  * Fix tests. [Commit.][260]
  * Store marker model in monitor instead of fetching it for each frame. [Commit.][261]
  * Try to fix timewarp test. [Commit.][262]
  * Fix tests. [Commit.][263]
  * Start fixing tests. [Commit.][264]
  * Make subtitleModel depend on TimelineModel instead of KdenliveDoc and TimelineController. [Commit.][265]
  * Move timeline preview to timelinemodel to prepare multiple timeline previews (for nesting), add basic timeline preview test. [Commit.][266]
  * Rename Timeline clips to &#8220;Sequences&#8221;. [Commit.][267]
  * Make description of cmd options better understandable. [Commit.][268]
  * Reorder main function. [Commit.][269]
  * Add missing license header. [Commit.][270]
  * Warn user when low on memory. [Commit.][271]
  * Port away from deprecated ThumbCreator. [Commit.][272]
  * Use Timeline 1 for first timeline tab. [Commit.][273]
  * Don&#8217;t allow inserting a timeline sequence onto itself. [Commit.][274]
  * Fix editing extra timeline broken after reopen. [Commit.][275]
  * Don&#8217;t show rescale filter in UI, it is internal to MLT. [Commit.][276]
  * Fix saving project with a secondary timeline opened. [Commit.][277]
  * Add xml files for new frei0r transitions and updated defish0r parameters. [Commit.][278]
  * Implement nesting v2. [Commit.][279]
  * Directly use timeline model to get guides. [Commit.][280]
  * New camera proxy for Akaso. [Commit.][281]
  * Merge 22.12. [Commit.][282]
  * Update copyright year to 2023. [Commit.][283]
  * Add optional Clip Job overlay in clip monitor allowing to view and cancel the current clip jobs. [Commit.][284]
  * Add new dialog to configure clip jobs. [Commit.][285]
  * Fix possible freeze on aborting edit-friendly transcoding request. [Commit.][286]
  * [Rendering] Fix &#8220;Render at preview resolution&#8221; sometimes not considered. [Commit.][287]
  * [Rendering] Move some code to allow testing, add tests. [Commit.][288]
  * [Render Settings] Reduce conversion between String and Map. [Commit.][289]See bug [#462650][290]. See bug [#458718][291]
  * [Render Presets] Use map for params in model. [Commit.][292]
  * [Render Presets] First step to use a map for parameters. [Commit.][293]
  * [Render Model] more tests and fixes. [Commit.][294]
  * [Export Guides] Better window title. [Commit.][295]
  * Fix(reuse): Use generic phrase for copyrights for .po files. [Commit.][296]
  * No need to mention Kdenliven authors for this file as it is just re-uploaded here. [Commit.][297]
  * Fix(reuse):use proper comment syntax for cmake file. [Commit.][298]
  * Chore(reuse): add IP info in some header. [Commit.][299]
  * Fix(icons): update mime-type icons with current logo. [Commit.][300]
  * Chore(reuse): add IP info in dep5 for po/ folder. [Commit.][301]
  * Chore(reuse): Add BSL license and fix header. [Commit.][302]
  * Chore(reuse): Add IP info for /dev-docs. [Commit.][303]
  * [Tag Widget] Fix crash if no category exists. [Commit.][304]
  * Fix and improve render preset parameter splitting with whitespaces. [Commit.][305]
  * [Qt6] Fix build. [Commit.][306]
  * Add initial tests for render model. [Commit.][307]
  * Fix compilation and use new timelinemodel related method to get guidesModel. [Commit.][308]
  * [Render Presets] Fix UI options for ratecontrol. [Commit.][309]
  * Cleanup: remove unused arg. [Commit.][310]
  * Preview chunks should be sorted by integer. [Commit.][311]
  * Fix timeline preview incorrectly stopping when moving a clip outside preview zone. [Commit.][312]
  * QMetaObject::invokeMethod should be used with Qt::DirectConnection when expecting a return argument. [Commit.][313]
  * Fix designer plugin crash. [Commit.][314]
  * Disable parallel processing on 32bit systems. [Commit.][315]
  * Fix pressing Esc during timeline drag corrupts timeline. [Commit.][316]
  * Fix guides incorrectly moved when unlocked. [Commit.][317]
  * Update mouse position in timeline toolbar on zoom and scroll timeline. [Commit.][318]
  * Fix crash dropping an effect with a scene (rotoscoping, transform,&#8230;) on the project monitor. [Commit.][319]
  * Fix zoom sometimes behaving incorrectly on very low zoom levels. [Commit.][320]
  * Fix zoom on mouse not working as expected when zooming after last clip. [Commit.][321]
  * Restrict guides to integer position on paint to avoid drawing artifacts. [Commit.][322]
  * Fix resize zone conflicting with move on low zoom levels. [Commit.][323]
  * Harmonize effect sliders suffix (space is now automatically inserted between value and suffix), add rotation button to rotation sliders. [Commit.][324]
  * Fix title clip line break sometimes pushing text outside item rect. [Commit.][325]
  * Fix rendering when using an MLT properties file with a space in it. [Commit.][326]Fixes bug [#462650][290]
  * Effect slider: press shift while dragging will adjust values one by one. [Commit.][327]
  * Fix monitor overlay sometimes incorrectly placed. [Commit.][328]
  * Ensure on monitor marker color is updated even if 2 markers have the same text. [Commit.][329]
  * Add status bar tooltip for zone resize. [Commit.][330]
  * Cleanup monitor zone resize. [Commit.][331]
  * [kdenlive_render] Remove unused code. [Commit.][332]
  * [kdenlive_render] Use qDebug() also if DBus JobViewServer not available. [Commit.][333]
  * [kdenlive_render] Make destination argument optional. [Commit.][334]
  * [kdenlive_renderer] Ensure xml file is open for reading. [Commit.][335]
  * [kdenlive_render] Use qDebug() if socket not connected (NODBUS only yet). [Commit.][336]
  * [kdenlive_render] Simplifiy code by using QStringList::takeFirst(). [Commit.][337]
  * [kdenlive_render] Drop &#8211;in and &#8211;out parameter. [Commit.][338]
  * [kdenlive_render] Remove workaround for MLT bug that is fixed since long. [Commit.][339]
  * Adjust Kdenlive rendering to recent changes in kdenlive_render. [Commit.][340]
  * [kdenlive\_render] Port kdenlive\_render to QCommandLineParser. [Commit.][341]
  * [kdenlive_render] Remove leftover of unused player parameter. [Commit.][342]
  * Fix guides missing on document open. [Commit.][343]
  * Fix guides color not updated in timeline when the category color was updated. [Commit.][344]
  * Refactoring: move guidesmodel in timelinemodel (required for nesting). [Commit.][345]
  * Better debug info on failed proxy for small images. [Commit.][346]
  * Update README.md. [Commit.][347]
  * Fix MaxOS compilation. [Commit.][348]
  * Fix minor typo. [Commit.][349]
  * Be more clever finding resources paths when the project was relocated (for example opened from an external drive). [Commit.][350]
  * Fix crash on document open. [Commit.][351]
  * Fix possible crash on document open. [Commit.][352]
  * Store mark in/out as private members instead of recreating them. [Commit.][353]
  * Add zone-in/zone-out to contextual mouse menu for clip monitor(issue 1508). [Commit.][354]
  * Audio graph filters are keyframable now. [Commit.][355]Fixes bug [#459554][356]
  * Auto-call taskManager.taskDone() when run() ends. [Commit.][357]
  
[3]: http://commits.kde.org/kdenlive/0e7eeb9f90feaa56b9a1949be53aceaff8d1d6a6
 [4]: http://commits.kde.org/kdenlive/24e01a45197e7a057566b6edfd52db25fb99e63a
 [5]: http://commits.kde.org/kdenlive/738ac45c135176832def188d488e1bf05cda7da8
 [6]: http://commits.kde.org/kdenlive/cd0f5ca4d1ceaef215d3dfd5c7876fa7e3d1641e
 [7]: http://commits.kde.org/kdenlive/1e90fbb0ce481ec4d377d988e523411024e8be2d
 [8]: http://commits.kde.org/kdenlive/b0cda6f309c7f811c22531eb0887d1924d7c9c4b
 [9]: http://commits.kde.org/kdenlive/a133c6e3b5b2c4c02088e15cc8d8cfee2b873825
 [10]: http://commits.kde.org/kdenlive/764ab11300febc8098db5aca5cc2444a463892c7
 [11]: http://commits.kde.org/kdenlive/cac7c925934cc4124c112261217bb482367ec09a
 [12]: http://commits.kde.org/kdenlive/00f56b1028c565fc4443f4101580df6f6448b9d5
 [13]: http://commits.kde.org/kdenlive/e7f542dd36d070d007e42336ab8030db6d96a38c
 [14]: http://commits.kde.org/kdenlive/39a39ee9fe2a2fe8c34333250e4b1ecf8ec59425
 [15]: http://commits.kde.org/kdenlive/2b7200e32d0b22c62cddafc1cfe669cee5d650cd
 [16]: http://commits.kde.org/kdenlive/b24cb32f0b80a5de844bec68241bd1fe436d8aa1
 [17]: http://commits.kde.org/kdenlive/85d4e15a77c01c87b210e9e2f7b9b663395c9dad
 [18]: http://commits.kde.org/kdenlive/063205f0b4d6a8c285743b1c7acf8a3e0b05eead
 [19]: http://commits.kde.org/kdenlive/7337b1750d2c91cdfd1a70840d377fee123c5855
 [20]: http://commits.kde.org/kdenlive/f9402fc50e4689125418208f44e4cc2cac5ed9ff
 [21]: http://commits.kde.org/kdenlive/da589c4077dece76329ef6c5e8b40f935efa70ad
 [22]: http://commits.kde.org/kdenlive/b6b3bbedf7b3acb071b9c62ade43a3760107c1ec
 [23]: http://commits.kde.org/kdenlive/555521a387cd20aab36baf4f717161f72eac3572
 [24]: http://commits.kde.org/kdenlive/81984fe78ad7aba7b6427b861560ef6a0513ab9f
 [25]: http://commits.kde.org/kdenlive/bcd799ffcaeb1af65b8768c8865d7a1dfa10e031
 [26]: http://commits.kde.org/kdenlive/8b413866f3894cf3631135a9cd43c6acae10a8d6
 [27]: http://commits.kde.org/kdenlive/428adceba18f905c741e7ad67a1846e5a10e7cf7
 [28]: http://commits.kde.org/kdenlive/b0b2403dd6ca31db932201d7b3016df173542602
 [29]: http://commits.kde.org/kdenlive/3cee243fe258c1b3a50311ffcabf13d987f7f52b
 [30]: http://commits.kde.org/kdenlive/4b169638c7e27b40bbe012c55781c2e1b31b7d2d
 [31]: http://commits.kde.org/kdenlive/ca083d6f966c2e40e90f101312887a4fdbad6c4e
 [32]: http://commits.kde.org/kdenlive/d89d077e9b68352d2a4158694f05c1561547caeb
 [33]: https://bugs.kde.org/468156
 [34]: http://commits.kde.org/kdenlive/76d735b231b5015d4b2207a6b37949ebcc3aaf93
 [35]: http://commits.kde.org/kdenlive/936ac1d1f6d1941426b850b1b17c20b01e6d8486
 [36]: http://commits.kde.org/kdenlive/580590a8a5d35fce47bf4a25ef3ef6aaa8b33785
 [37]: http://commits.kde.org/kdenlive/bb480ef299c452295c482c7e2a56a04a6ea74e46
 [38]: http://commits.kde.org/kdenlive/038f7ef3fb25c215df0243e19bad9cc4425d43d6
 [39]: http://commits.kde.org/kdenlive/506e7cbb50961246eb4f9585541ad1f641568639
 [40]: https://bugs.kde.org/467917
 [41]: http://commits.kde.org/kdenlive/9ef4f38c9f2897b0200bb7d58b662896a138ab9f
 [42]: https://bugs.kde.org/468038
 [43]: http://commits.kde.org/kdenlive/6a0bcec6d03892c92532a10e230f74b978eb9833
 [44]: http://commits.kde.org/kdenlive/a66d3e0dba7f8561dfa4a96ed85b08b86e03c774
 [45]: http://commits.kde.org/kdenlive/d4ad6d34dd69644a4ccbda28d0572781fbd23662
 [46]: https://bugs.kde.org/467745
 [47]: http://commits.kde.org/kdenlive/096acf523b4b7e7585d6deac04fdc1b73f248c57
 [48]: http://commits.kde.org/kdenlive/2aec6e18d534f723c781dcabf6016144febb7846
 [49]: http://commits.kde.org/kdenlive/1229b10e35556e2b251e3829ea922e44344ef972
 [50]: http://commits.kde.org/kdenlive/f6e9eb2abab6a03464d2be2bde0102d392de0e92
 [51]: http://commits.kde.org/kdenlive/002320af6a51ffc0cb756a761c82f166399f4c9b
 [52]: http://commits.kde.org/kdenlive/8bb71f86f2a170b118d9c126fbab07e0775670f2
 [53]: http://commits.kde.org/kdenlive/6a5637f34b5352d64b631b0c8fde7ecac3be930c
 [54]: http://commits.kde.org/kdenlive/7940f247ae83ce7464b07495c470d9a616bf48ad
 [55]: http://commits.kde.org/kdenlive/6c65448341fe29d94f991ff8f83ce3da90b2d705
 [56]: http://commits.kde.org/kdenlive/b85a14af4aac0e509f4146e250089794f48737b2
 [57]: http://commits.kde.org/kdenlive/600496507745e4f85743c5f9a708e7bfbda6aa9a
 [58]: http://commits.kde.org/kdenlive/5aef99554178f93cb64e011fbebd2142a9d61947
 [59]: http://commits.kde.org/kdenlive/4fd47d8f4c2e70904539d390ea92afa0ddd83601
 [60]: http://commits.kde.org/kdenlive/c2803fdbeb56ff8cb14ca9e88992bf8b45b6c481
 [61]: http://commits.kde.org/kdenlive/38c63d49595a1266ecc8e0830dbc01f711a2d24b
 [62]: http://commits.kde.org/kdenlive/21494e25dbaa0380e09c01884c89b3695075ff52
 [63]: http://commits.kde.org/kdenlive/b1c8ad1e3ba7f5ce0cff588492bb4e432413f370
 [64]: http://commits.kde.org/kdenlive/b1753c1ce0afb8320b05699a0beda32f52e6bb1f
 [65]: http://commits.kde.org/kdenlive/358cf0723b316e0a6818df0a1f789fcd7f710bca
 [66]: http://commits.kde.org/kdenlive/383f96287908e725aa6cf5725adf62665519bd71
 [67]: http://commits.kde.org/kdenlive/8af2ddcc91c90aea8f6bd6e44a3cfef52390e336
 [68]: http://commits.kde.org/kdenlive/90b15fffaf23d7df8fd0b1fd7b6de0e4f6c5e7da
 [69]: http://commits.kde.org/kdenlive/ac392e9b889a2e0b4c43689beab8ae78d3fab8bb
 [70]: http://commits.kde.org/kdenlive/489a9d706f07754c401b68320426b0f6c7e68273
 [71]: http://commits.kde.org/kdenlive/86ec01197965fa86429e3ad24de7b185e4c5ea81
 [72]: http://commits.kde.org/kdenlive/5a0b36ee5b424d7266408670f79a018cff5ce1b8
 [73]: http://commits.kde.org/kdenlive/7900fd7e87185bd80abdc613b0e3442807c243ad
 [74]: http://commits.kde.org/kdenlive/a98656f9169292553f24e8a9a7ca6086dd73dd90
 [75]: http://commits.kde.org/kdenlive/3c15dd8684d8c3596a8c8c6bcd5822f70252d358
 [76]: http://commits.kde.org/kdenlive/63dae0b614ccb7949e33c0538f0586c1f94bfc4c
 [77]: http://commits.kde.org/kdenlive/83bfc4672f2b180c645467ade25cc6b58cc22e5b
 [78]: http://commits.kde.org/kdenlive/585498899b4c13343cdcc25fbb501d31d3358726
 [79]: http://commits.kde.org/kdenlive/faa511863e233c655fd7d721b7ac986cbfcec385
 [80]: http://commits.kde.org/kdenlive/e37d2f4f4593b1eb09ac12e5f624b5e0e3f7b9c0
 [81]: http://commits.kde.org/kdenlive/5119a755b83babc4b7df8863038182e75a4b3cc9
 [82]: http://commits.kde.org/kdenlive/3f9db14517a5254036a25cc31ce6aba5d976bf38
 [83]: http://commits.kde.org/kdenlive/217e56513a2b729c9064846b041631388d8072d0
 [84]: http://commits.kde.org/kdenlive/05b2ee884c36495ffd17a222f4f8b2c0f6e44601
 [85]: http://commits.kde.org/kdenlive/1a22607978741371459c5531a3cc90b93d30f089
 [86]: http://commits.kde.org/kdenlive/07ee8de467fb7a673bdea954e6fd5e023c965721
 [87]: http://commits.kde.org/kdenlive/54462138b45f42a97717215d9b4d152d2d8f25e7
 [88]: http://commits.kde.org/kdenlive/24c5d60ed86bbd64fdeea4d56e8a1cbbe9ddf382
 [89]: http://commits.kde.org/kdenlive/38f73f40f2ddf5c30e2500bcd122ad35a43bb923
 [90]: http://commits.kde.org/kdenlive/949b2b9db33932dabc93c06e58aa82b810fdb90f
 [91]: http://commits.kde.org/kdenlive/399160cdc3ac501a9ee36ea2054bcc77499defa0
 [92]: http://commits.kde.org/kdenlive/9282cdf668c5d2dbf185d13c03b6c21f62850c62
 [93]: http://commits.kde.org/kdenlive/7c1936bb44b592eaf3174f559fbb97cf61cc2bcc
 [94]: https://bugs.kde.org/467172
 [95]: http://commits.kde.org/kdenlive/e1730dfa68da7cff87049bad674d1b76e4f975fe
 [96]: http://commits.kde.org/kdenlive/edfa1fd72f2a8d3d1b48f679969e94c50c54e73e
 [97]: http://commits.kde.org/kdenlive/cf8a23189a007e1fad0ed7171edc8f920e2448db
 [98]: http://commits.kde.org/kdenlive/cba3b5c101964723626e5229e91caee892079a9b
 [99]: http://commits.kde.org/kdenlive/b8913484786a40fbce9e2b83c7d827c2ec086fb7
 [100]: http://commits.kde.org/kdenlive/6666d85ca078f93980204ac21c747f1beb2d3657
 [101]: http://commits.kde.org/kdenlive/85ceafde74250b9f51da0a3db024cfd02fd44dd0
 [102]: http://commits.kde.org/kdenlive/2d863c4dc15e63cc696b27c3535b55214b8205eb
 [103]: http://commits.kde.org/kdenlive/16aaf392888853122140a218d5ff417cb6cc6373
 [104]: https://bugs.kde.org/466486
 [105]: http://commits.kde.org/kdenlive/0d206024c98357d9a62c9a3d789f3f9597cb4972
 [106]: http://commits.kde.org/kdenlive/1c08c8deb32eb1812fcd10555592eb8f0e278e1c
 [107]: http://commits.kde.org/kdenlive/e4d0c76b587a5d9527bdec17a702f5772220e259
 [108]: http://commits.kde.org/kdenlive/4c552897bd86a7fbc8b57c12c3cd57d4e5125c77
 [109]: http://commits.kde.org/kdenlive/6d1d474ea237d1abfa263155c7ad5b9227270e3f
 [110]: http://commits.kde.org/kdenlive/ec70a05fc23114160ef183aa6481c4c8bff13441
 [111]: http://commits.kde.org/kdenlive/9c3c1703209a6008c1c12c61842249460661c668
 [112]: http://commits.kde.org/kdenlive/31e3ae21339c356aa00f19a02e28036848effc58
 [113]: http://commits.kde.org/kdenlive/0bd5627f95d164553dd24e1a7cda4f5151d289a1
 [114]: http://commits.kde.org/kdenlive/f8718e181db82870edfe8b33d423d2e3699aa62f
 [115]: http://commits.kde.org/kdenlive/530d41cad5d5c09fb5eea240f6b27534dd6ed5e4
 [116]: http://commits.kde.org/kdenlive/027019cf9b3c7162168414699fa11edfd6803dbf
 [117]: http://commits.kde.org/kdenlive/68667634b1506ed85da59b388e52b84090e2f541
 [118]: http://commits.kde.org/kdenlive/ea8a6092e756430f0e53b2f9250891ccfc0c362d
 [119]: http://commits.kde.org/kdenlive/82ad5af6b3f01ddc87fa1f4b9e1729a1d5b3a0bf
 [120]: http://commits.kde.org/kdenlive/2412a20d91758cd183814377fbd3a9a8e9900d43
 [121]: http://commits.kde.org/kdenlive/83bb8d604dafa9ba5d4ead7c1eb0982bd9837092
 [122]: http://commits.kde.org/kdenlive/aefc745adacf78bf1bdda59690796c0d5ede9300
 [123]: http://commits.kde.org/kdenlive/dd8c40a242316b881a4ed96d5ceda0725d6e8fdc
 [124]: http://commits.kde.org/kdenlive/09171a5c2c3fc2f3466eba40196985d0a6a9b2c0
 [125]: http://commits.kde.org/kdenlive/2d92749acaa6f0c5dd4da0e61729eadb411e8701
 [126]: http://commits.kde.org/kdenlive/0ca2fcaf0ccdad8df124082bcb029e997e941f0e
 [127]: http://commits.kde.org/kdenlive/52c3a223395adf646f0c52d315ae12af0361799d
 [128]: http://commits.kde.org/kdenlive/1f87fb3ff8da9057a8f0fe1d891799979a2593b9
 [129]: http://commits.kde.org/kdenlive/cc21106d256a21d91869b3764fc789c3aa659d8b
 [130]: http://commits.kde.org/kdenlive/250284b8af59aacb3f6f60661f0e142671715a4c
 [131]: http://commits.kde.org/kdenlive/43361f9ee218fb7fe21323463211b835b7d683d3
 [132]: http://commits.kde.org/kdenlive/8ba7f0cac03044d1bad0e7b6301ee3f270aacc00
 [133]: http://commits.kde.org/kdenlive/8b2e76c884da3780a6775180eb4ea9a83feaf955
 [134]: http://commits.kde.org/kdenlive/82c75c950f0013ff757b0b062010ca9276b5b18f
 [135]: http://commits.kde.org/kdenlive/01ef50e0dbf8cea1bb40ad5acc6a7ed558faa90d
 [136]: http://commits.kde.org/kdenlive/3e6f460454e2a33e69996c835de7eb2975037058
 [137]: http://commits.kde.org/kdenlive/c11fd58e9ffee532970fde5d34083db8907897ab
 [138]: http://commits.kde.org/kdenlive/bff047c0ca41684f1f9f6e9e86db1c8d68cec0a8
 [139]: http://commits.kde.org/kdenlive/449508c5eb9fcc3b9821bf18dd8b4277afc1d508
 [140]: https://bugs.kde.org/466232
 [141]: http://commits.kde.org/kdenlive/9a1f7f92653a57e11f59398fa35a35e9bbaa8a75
 [142]: http://commits.kde.org/kdenlive/cbf1af13607914149056b0fcd5f75944051c0112
 [143]: http://commits.kde.org/kdenlive/bf51d3120b09051539001a4cd3833e30a0483394
 [144]: http://commits.kde.org/kdenlive/7aed58c6396388c6d9bb7d0d9658d54bc8bed452
 [145]: http://commits.kde.org/kdenlive/0db99c2fd84737758f742d55fce6b6449f111fa6
 [146]: http://commits.kde.org/kdenlive/7afc68c3055d4d4ad3bf4463e507edea36652ce1
 [147]: http://commits.kde.org/kdenlive/8cd53dadef1cc03dbd403d0229bec502d8724940
 [148]: http://commits.kde.org/kdenlive/f9acbbf069ba7e283828a09984736454b72ebe0c
 [149]: http://commits.kde.org/kdenlive/3157a0d25f679491dd0939b96bde7ee739399f4e
 [150]: http://commits.kde.org/kdenlive/a5214f214b41fa653b6b6c0d587dc02c785a22e4
 [151]: http://commits.kde.org/kdenlive/97de1c2defc78a8b7b9423ea1830dbfc50aa59a7
 [152]: http://commits.kde.org/kdenlive/a446ca8210f3a93e815d944d37633ec1b3583251
 [153]: http://commits.kde.org/kdenlive/53c47dca81ed7d368399b17a859f6f2db8926ec4
 [154]: http://commits.kde.org/kdenlive/2f93eb2a1ef494218da895f3c908c3fc2bcc0503
 [155]: http://commits.kde.org/kdenlive/9a10947821b1ef673b8f4409450a84c76e0f2855
 [156]: http://commits.kde.org/kdenlive/8641281afedcc2abff18cbbc8aafdbe8b0287792
 [157]: http://commits.kde.org/kdenlive/b8accd95ded5e4d379072014611d52b6d795a577
 [158]: http://commits.kde.org/kdenlive/ec2d929649918e40489a20cd89ec7aee3429ea76
 [159]: http://commits.kde.org/kdenlive/cc50144194cca11b8218d37ad40563b0babc6f66
 [160]: http://commits.kde.org/kdenlive/6a12a309323d23ddaeaae6e1b4f9e16685d63d1e
 [161]: http://commits.kde.org/kdenlive/9c1a2846d7a8ad07fb682a5823485bc0ab969545
 [162]: http://commits.kde.org/kdenlive/3360de35567c707f4ff88eb6fb310a4b26166119
 [163]: http://commits.kde.org/kdenlive/1c0d0d959448bf43a4a642967a16d1af790b9385
 [164]: http://commits.kde.org/kdenlive/1c4a77b6b9da6f0b52c6c581b1786176ba408431
 [165]: http://commits.kde.org/kdenlive/3e0ee932bb3cfe9167e05c47409a24668f1bee9e
 [166]: http://commits.kde.org/kdenlive/2139c7fbea8e0270bae35cdcc2f1b7016fd81efc
 [167]: http://commits.kde.org/kdenlive/baee1389783fbc2ac072458a5d424c865a1d7159
 [168]: http://commits.kde.org/kdenlive/906346239f49a669d2d5ef7034ae984056dab85c
 [169]: http://commits.kde.org/kdenlive/68676e80a0c902dbfb43bda10f52deba3031a511
 [170]: http://commits.kde.org/kdenlive/890af04e7fadbf31adc6b97fcc2f105a6bec97cc
 [171]: http://commits.kde.org/kdenlive/a31f05527bceeb5a1da88602566727d904577aa3
 [172]: http://commits.kde.org/kdenlive/ddf941925d3ead19d657e3a751658a3d9dc5af5b
 [173]: http://commits.kde.org/kdenlive/c2bf8652bd99f1c49b6f16fafa8ff4d92a3bcdb4
 [174]: http://commits.kde.org/kdenlive/3e33a633b88be12200057ef18e3845f75743a36c
 [175]: http://commits.kde.org/kdenlive/ad9de6d06b23d9a33ff0592d8bd35976dbf35f14
 [176]: http://commits.kde.org/kdenlive/d4151d383d63e597ebf8f4be5350042c1abe219d
 [177]: http://commits.kde.org/kdenlive/d42e318347ad4ec71e1df7fdcc3b3459e10f90bc
 [178]: https://bugs.kde.org/463764
 [179]: http://commits.kde.org/kdenlive/5eb26b76d5e6ee0ddef0ee4a459d5d0e5062195d
 [180]: http://commits.kde.org/kdenlive/54060193c566759189464897f10c0db498821878
 [181]: http://commits.kde.org/kdenlive/a85415dfcb04f2fd15004638db249056e0c3a668
 [182]: http://commits.kde.org/kdenlive/b1e4b48156bf273fea198f04402da1f39c1ca60c
 [183]: http://commits.kde.org/kdenlive/00a486e59f17cbb28ec2fc4e61ff381e2a262b9b
 [184]: http://commits.kde.org/kdenlive/88ec2db1dfd936115b2e68ab2a4586bbed3cea10
 [185]: http://commits.kde.org/kdenlive/8348e5bd181a8af7a1f0ae3ba370cee6d3f595a3
 [186]: http://commits.kde.org/kdenlive/81653496bc26da9f53ce6c6ae1996e30d31c3d69
 [187]: http://commits.kde.org/kdenlive/76a7c3a2a7ec67b755cece21e4043bbb8743bdf3
 [188]: http://commits.kde.org/kdenlive/9a4404ff1064cd74b3459aeaccd6de39fac0659e
 [189]: http://commits.kde.org/kdenlive/16c8ccf3f16c11b24c7cae903b392dee03329da2
 [190]: http://commits.kde.org/kdenlive/3a4cd90bc843fe758242bd6427fcabb5cbe52e8a
 [191]: http://commits.kde.org/kdenlive/e16331976ab059f820bf52b5d7f89bf9c95bc7cb
 [192]: http://commits.kde.org/kdenlive/c56f3cec77b5eaa297e129b785cca98603746263
 [193]: http://commits.kde.org/kdenlive/16e592fa42765d2d8a5d4b4dec98f2aecb2509ce
 [194]: http://commits.kde.org/kdenlive/4591937fad59f58d9cc5ba30fb2f906364cd888c
 [195]: http://commits.kde.org/kdenlive/10fa1376593b193743b7524f333213b01c07e8f3
 [196]: http://commits.kde.org/kdenlive/86839a4c759e4867c6a81160f6e38129a1a511c3
 [197]: http://commits.kde.org/kdenlive/404ab9b917c02f9b92d7c0d28c13afaf69185f37
 [198]: http://commits.kde.org/kdenlive/2eaf9c74dce3df8690d17b3b6eddc40b13c8e265
 [199]: http://commits.kde.org/kdenlive/c822ac711e4de49821b62e09781041a8c751b79d
 [200]: http://commits.kde.org/kdenlive/d7747c1827eadb9d5737ed8ab9d8184a652b6444
 [201]: http://commits.kde.org/kdenlive/93c24f8f5469aa905bae418e878978b69598e785
 [202]: http://commits.kde.org/kdenlive/92189f6978e4ddd6ce004505401a73d3b7f7b717
 [203]: http://commits.kde.org/kdenlive/4e332d32bdef20ee0b781d983d0e9ca31429ee47
 [204]: http://commits.kde.org/kdenlive/ccbdcd4530e406c21fc67dc97dfb80e091b7bc64
 [205]: http://commits.kde.org/kdenlive/653465d9a24034e4e0b63c1cb3a29a48e8693b6f
 [206]: http://commits.kde.org/kdenlive/62b764a4fee8061e30653dbc69867023293cc31d
 [207]: http://commits.kde.org/kdenlive/760bae44045f18dcfd4844e43527c48bea381773
 [208]: http://commits.kde.org/kdenlive/9119b7985aabc0cf4a2210e20ff94f41e97086d8
 [209]: http://commits.kde.org/kdenlive/f6d047299e83404b3c8f97a7732db30177921765
 [210]: http://commits.kde.org/kdenlive/826b6b995ee5758c23e4aeb1083d1fe2c9273743
 [211]: http://commits.kde.org/kdenlive/633e436cce7e449dd83a549f3ae97b748573f37a
 [212]: http://commits.kde.org/kdenlive/73453f3903492b9e10dc785f86a141c1539e8ea4
 [213]: https://bugs.kde.org/465068
 [214]: http://commits.kde.org/kdenlive/1193d5641201301e0e52b996ff705255c5368b61
 [215]: http://commits.kde.org/kdenlive/55263efb38861c1494d28c83a12fb8e544fe5162
 [216]: http://commits.kde.org/kdenlive/089825e20c6990dbc187fd15954926242287243a
 [217]: http://commits.kde.org/kdenlive/0d0953003264fc8f919bb532e336c89ef951f9c9
 [218]: http://commits.kde.org/kdenlive/0045b974aaeec19ff57cedc32b9ab6e26b29fc31
 [219]: http://commits.kde.org/kdenlive/614f83a2f714cd7abdf629bd13adb7812622d31e
 [220]: http://commits.kde.org/kdenlive/0aa17e6e0db455f40c4446f03dbe406921ad50d8
 [221]: http://commits.kde.org/kdenlive/05a3d4767206bf259812722b3d95106da925b1b6
 [222]: http://commits.kde.org/kdenlive/272f318f1d08b239548a894e8c08cb1e6bb9e66c
 [223]: http://commits.kde.org/kdenlive/886d846917fade2de7e239391782731b7028c5ac
 [224]: http://commits.kde.org/kdenlive/25ce1fdf4abc5b9d56e00278ee38da978fd5ce55
 [225]: http://commits.kde.org/kdenlive/e4ea6bd329f358dee393f8e8fe09fd6bc8408e2c
 [226]: http://commits.kde.org/kdenlive/e2fe636ada7d883b962b7d6980e18d4038108d70
 [227]: http://commits.kde.org/kdenlive/37f2be63f82367df20ee94e701da4df7535d456c
 [228]: http://commits.kde.org/kdenlive/99348cfcdabceb8bcf759d84bcad71ae6b20654d
 [229]: http://commits.kde.org/kdenlive/84e101ecd6b886b860bb1e2c44e0a05acc660968
 [230]: http://commits.kde.org/kdenlive/5ca6a6d2e674453a57ea3c74758493f596f622d8
 [231]: http://commits.kde.org/kdenlive/4413987fbfa2a4b9b9bcb322ed79d1084f74eb68
 [232]: http://commits.kde.org/kdenlive/9bb3ea00099e5fc04c07025d3ab764df9e527d84
 [233]: http://commits.kde.org/kdenlive/681ef26798ffb19e7dc9bd5fe4657c173c0d0742
 [234]: http://commits.kde.org/kdenlive/be1cc83b14dfdeb31db2e03d462e3f7bf20c6843
 [235]: http://commits.kde.org/kdenlive/76abd4eb0feb59ac8fcea6b636b444ffd393f668
 [236]: http://commits.kde.org/kdenlive/09408e312df587a071326dfde385023012775745
 [237]: http://commits.kde.org/kdenlive/9559ecd2876b734aab12d6b0821ef58d6f778822
 [238]: http://commits.kde.org/kdenlive/0c1b5ccaa083ce18c743857172d8cb831ebc5d5b
 [239]: http://commits.kde.org/kdenlive/c166b3852999acfd04933e1746f652d2435c5d82
 [240]: http://commits.kde.org/kdenlive/784d06deaea6b9fa8e62af31d60c1b0f33100f2c
 [241]: http://commits.kde.org/kdenlive/409919b823c95a7aff096fa02cf1611a2e97271a
 [242]: http://commits.kde.org/kdenlive/41388de63ae72dfc37733bf1c6d2f4b0f27283de
 [243]: http://commits.kde.org/kdenlive/9dd86279369527c76a7fd93aa1bcec979fe20706
 [244]: http://commits.kde.org/kdenlive/923d9be1ee6ff601ae03ac0652452a9d43fdb407
 [245]: http://commits.kde.org/kdenlive/1fb80c61d0c5bc53d5e366a74345ffa088e92e27
 [246]: http://commits.kde.org/kdenlive/ac2e728cd9137e867257574f16600eeb4c8d8b71
 [247]: http://commits.kde.org/kdenlive/692330e34f33bb75c0d155be57bdfbad6fd278f4
 [248]: http://commits.kde.org/kdenlive/15d9f11c3ea954cb0a985525d8e7d7c5d7ec613b
 [249]: http://commits.kde.org/kdenlive/469a2e7f179fea04f9545e1ded56b939b59e5555
 [250]: http://commits.kde.org/kdenlive/f7bd171a57eb6b4f941f4fc5272b5a2deead0082
 [251]: http://commits.kde.org/kdenlive/13da8c1effc1117ac2cfd2c5c8a345516e848144
 [252]: http://commits.kde.org/kdenlive/93009549f07ce0dd892cd3012636b27b875f343e
 [253]: http://commits.kde.org/kdenlive/ff9af13695f5824d380196e00d6714be4cd3d38f
 [254]: http://commits.kde.org/kdenlive/6ff84a943919beeed8304dbf97a5f8464adb5b73
 [255]: http://commits.kde.org/kdenlive/dd85af5fcc7c330244d2e4aa847d8643822b82a7
 [256]: http://commits.kde.org/kdenlive/e2205da63b16b08b09873d293ae435bcec8187e1
 [257]: http://commits.kde.org/kdenlive/943b5b428f70edf2e28d3a9408ce7a32fe3d0ef8
 [258]: http://commits.kde.org/kdenlive/de8d7f370de9f65fac0e82005e8f754d61d3210f
 [259]: http://commits.kde.org/kdenlive/6913cc04fc3476adc45fcbcff2fb26eaa464816a
 [260]: http://commits.kde.org/kdenlive/48d8d52030582258208d64b3952249f7e9c43614
 [261]: http://commits.kde.org/kdenlive/d176e80a7bde3d21dc03103e9e26d07e7a4d09b7
 [262]: http://commits.kde.org/kdenlive/afc884fcfdc9ef90b1f1c9bf77b2fab9c5755aab
 [263]: http://commits.kde.org/kdenlive/b96c06fc137cf56e7ce957cc642201a96e7c294a
 [264]: http://commits.kde.org/kdenlive/a8f85beff12af26d2d2f6fddb18566668bcab363
 [265]: http://commits.kde.org/kdenlive/2f5d355a93fdd623281d8afa61fb54d3b233a28f
 [266]: http://commits.kde.org/kdenlive/0f69a3d50db3e39b7d1bf49c4f027be80b520663
 [267]: http://commits.kde.org/kdenlive/f120d6b742550fbf2e4390911a2deed66983557b
 [268]: http://commits.kde.org/kdenlive/2ccff2122628abb205c8e600bcda8a7be5dbf40f
 [269]: http://commits.kde.org/kdenlive/86738cb7bb4748107c0e3e4161a30ad694172ec6
 [270]: http://commits.kde.org/kdenlive/6fcf3acfac6270a2bb1fe7250335efb49837a6f4
 [271]: http://commits.kde.org/kdenlive/b281ef142e7e3227b5533da6b01b68f7693454b7
 [272]: http://commits.kde.org/kdenlive/bac4a1edd50581510f76211607a24f9d4ac0961a
 [273]: http://commits.kde.org/kdenlive/135a7b6df10bc487dd108ae05607d88a64e1b028
 [274]: http://commits.kde.org/kdenlive/3532f5a4b0af766a4a092999a7271a8982dee6e7
 [275]: http://commits.kde.org/kdenlive/56de1399b5f9d96ac121c381c01aa87a4e705a2b
 [276]: http://commits.kde.org/kdenlive/039cb003b6311cf5f06a14b3b671bc09195ce47b
 [277]: http://commits.kde.org/kdenlive/d70c5c03f227b16ede712e0233d54dbe5379d5d1
 [278]: http://commits.kde.org/kdenlive/fc54a36ca29471b57cc18f4530415f9b2ef33b8e
 [279]: http://commits.kde.org/kdenlive/955b02462a4b06396b6660bfdb87cfa5112c99d0
 [280]: http://commits.kde.org/kdenlive/0d71490d7aa71e7d571ba2b9b2668a3b11d545bf
 [281]: http://commits.kde.org/kdenlive/d63cdc10db2f56c34af0c09f977ce685fe26fa47
 [282]: http://commits.kde.org/kdenlive/9a7903dd4940d9dc46694f75c0c705c3f7250201
 [283]: http://commits.kde.org/kdenlive/90ec0762cb4297d90c809e4d482074a7200066f2
 [284]: http://commits.kde.org/kdenlive/6401eae3640aff9698c194b3227b5bb5bb6112ef
 [285]: http://commits.kde.org/kdenlive/e1778a450c0e34b4e9141b8cff132e42e6633fb5
 [286]: http://commits.kde.org/kdenlive/d4818f5edc74b2162d84925f0fad43ef339ef081
 [287]: http://commits.kde.org/kdenlive/2a7f2b5ea05df31412ac6003b167890ba426417b
 [288]: http://commits.kde.org/kdenlive/c27550ed7e1bb0ae0f48c199f5d304597c40d9e7
 [289]: http://commits.kde.org/kdenlive/3a5c22910267c490960c36179f2933b3c119c2eb
 [290]: https://bugs.kde.org/462650
 [291]: https://bugs.kde.org/458718
 [292]: http://commits.kde.org/kdenlive/9ef0c3b0b4bec3895b13e0283120f140f862f65f
 [293]: http://commits.kde.org/kdenlive/a98f3abebdbe88aaee3fe9140a7f6b20ce49cdff
 [294]: http://commits.kde.org/kdenlive/9062da54b8cfd8562bba717985c3a03243f4c1b4
 [295]: http://commits.kde.org/kdenlive/676932129b0f36fbc2f7f5899896349ace676ea6
 [296]: http://commits.kde.org/kdenlive/3275f5b3baa7a2818dd26c6722c22fff90d4e5ec
 [297]: http://commits.kde.org/kdenlive/bd11dc8443c4bc9c07dce3d069cf7b4cc59bb7bf
 [298]: http://commits.kde.org/kdenlive/8f058b9818c26623e07d7f4baf76df53143d104b
 [299]: http://commits.kde.org/kdenlive/47ed8a7bc1b962cd45e99583a3fb8c469d676423
 [300]: http://commits.kde.org/kdenlive/6f66019efe2447cf0e7b5e047e8f6eaa091270f8
 [301]: http://commits.kde.org/kdenlive/0bfa7f8756b4365539123c8920aa1f3497c2d017
 [302]: http://commits.kde.org/kdenlive/f26c297f0672956d327594d7308f15c22f531ee9
 [303]: http://commits.kde.org/kdenlive/2a9a27e103288a55130b0b4da7fcf67a14d81b5d
 [304]: http://commits.kde.org/kdenlive/c5d3fe45b810fad26fb8d8a859bcb8af11cdc464
 [305]: http://commits.kde.org/kdenlive/48fefe862aa00555a38172b30000abc2266aa1ea
 [306]: http://commits.kde.org/kdenlive/1a9c5a227409f3ba7200fd7d30bd6a406467c3bf
 [307]: http://commits.kde.org/kdenlive/2944bed52c6db712ed9ad94f6395165cd93f5ba5
 [308]: http://commits.kde.org/kdenlive/107199c41a0f70df914194935722858621faf20c
 [309]: http://commits.kde.org/kdenlive/b09b23b68d6ecb824b2e5d9198e579a3e9435e56
 [310]: http://commits.kde.org/kdenlive/5b4a67fa5d9429879732c43da1ef2c37a4fadc59
 [311]: http://commits.kde.org/kdenlive/ed40b958f72b16fd143792e598ee51c57345830c
 [312]: http://commits.kde.org/kdenlive/0882de6013d77e3c535ebf6461792e2fa01fbc4c
 [313]: http://commits.kde.org/kdenlive/1ad7f502d40e8472e77fe1c723642700bc3c1208
 [314]: http://commits.kde.org/kdenlive/87754b4df865ce39f4ca0393a8935f67e099c404
 [315]: http://commits.kde.org/kdenlive/a78b5dd22e194c48e8fc49d5fc5f732fb94e314a
 [316]: http://commits.kde.org/kdenlive/be2a3cb58437aa0bb736f3dfb26d97619df66449
 [317]: http://commits.kde.org/kdenlive/f0eea441f30f4c524bbe326cbda965641109819b
 [318]: http://commits.kde.org/kdenlive/3efe5a57353c9769356e62f2622e34ae2cdb9747
 [319]: http://commits.kde.org/kdenlive/b34347f7457306e51e775fa83c9f0c6366ca0e45
 [320]: http://commits.kde.org/kdenlive/ad3c1a52685eef066c60d4369e89a7002bf8ddb7
 [321]: http://commits.kde.org/kdenlive/79ed150a945543c48d30820f9111fa2766acb485
 [322]: http://commits.kde.org/kdenlive/c294289010bc69edd269e6f8374954c5e0ea6899
 [323]: http://commits.kde.org/kdenlive/a939c51bc9036e65091d7bbb8dbfd97bea03f666
 [324]: http://commits.kde.org/kdenlive/d370f436eb35844cc2d66f5b724db7189f28769f
 [325]: http://commits.kde.org/kdenlive/07fd4a0af3991a4a00bb0f0e004e1c84f8a2362d
 [326]: http://commits.kde.org/kdenlive/1c50d7d88de316da6a82841498006046b61fd73d
 [327]: http://commits.kde.org/kdenlive/3ac1d20e74bc5b896f5ac084b89b8ab9ab659e0a
 [328]: http://commits.kde.org/kdenlive/aa8276fbc8c1c45f20d7cc26f8151bf5fd3c8e4f
 [329]: http://commits.kde.org/kdenlive/9145ea2c2c08108716e71cf9af7ba00a19b23be3
 [330]: http://commits.kde.org/kdenlive/56b4002bc04d22f5503f985e56c97596571027ac
 [331]: http://commits.kde.org/kdenlive/be6f48bd304a8b634fade8453e00c9cda3b2d56c
 [332]: http://commits.kde.org/kdenlive/424225c6a914f3e2e51edab51b24945414d4c578
 [333]: http://commits.kde.org/kdenlive/99cbac8e6f50e8ca7fad354e83333db8f5b6ac08
 [334]: http://commits.kde.org/kdenlive/e7cf2effb4532874533a25c38e48bab6b3e9c266
 [335]: http://commits.kde.org/kdenlive/56f37e448d89143a161791ba57ee8fe6c8529245
 [336]: http://commits.kde.org/kdenlive/63c7928b1d8af89b45e6a368ad79941a34aba3cf
 [337]: http://commits.kde.org/kdenlive/7a8d9e4b9b8b0e7ac9223569db3c4c0aaed8df54
 [338]: http://commits.kde.org/kdenlive/926d39d21e2fe8a10df93efc9987f42e34e5ac87
 [339]: http://commits.kde.org/kdenlive/aa298b79b4cf793acd1b4dbdaac434b29ed804da
 [340]: http://commits.kde.org/kdenlive/1f67f8cd56ed987bca4b864a9290fc2542b1eb64
 [341]: http://commits.kde.org/kdenlive/daa0c02cd1a503f2b5722d785e5bffeeb1953923
 [342]: http://commits.kde.org/kdenlive/505d2eddab61f9072389e8ebec856fa96c1bf287
 [343]: http://commits.kde.org/kdenlive/fb741dea34d9fe59070d8ae32f82945d0cd8e692
 [344]: http://commits.kde.org/kdenlive/984297f238c12b6ab1219efad51a1a6751544bc6
 [345]: http://commits.kde.org/kdenlive/14ab2a0fdf28156eb151d86655c65d580c94deb1
 [346]: http://commits.kde.org/kdenlive/2c3b121081b184975702211ea8947b981652fa16
 [347]: http://commits.kde.org/kdenlive/b53d63377b1eaa0db3739d4a3c7926b3377b6ad5
 [348]: http://commits.kde.org/kdenlive/051899673818b5c100e5f194645520b7500fbbf4
 [349]: http://commits.kde.org/kdenlive/e274eacc8233e86521036e804f241696940ac7c3
 [350]: http://commits.kde.org/kdenlive/eb19b56fd3b790517ab7429c64300d496c7f09a6
 [351]: http://commits.kde.org/kdenlive/ee2bc61a8cd3654617b2e083ffcf09d4ca7d1606
 [352]: http://commits.kde.org/kdenlive/eeb27bf5375954fe062f755fe2df768d62821cc3
 [353]: http://commits.kde.org/kdenlive/0e5dcbbdec45b8388ce32a1faa5e52f401ca37a8
 [354]: http://commits.kde.org/kdenlive/2cb9faa5c0ee3dbb86e359adbd6225251bafb8b3
 [355]: http://commits.kde.org/kdenlive/7a31421fe73e90a10ec956f043159482ba63a9e0
 [356]: https://bugs.kde.org/459554
 [357]: http://commits.kde.org/kdenlive/eddb7c6e2634378e4029effaa0968459464cf761
