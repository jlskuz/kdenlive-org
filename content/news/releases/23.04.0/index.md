---
author: Farid Abdelnour
date: 2023-04-24T08:00:46+00:00
aliases:
- /en/2023/04/kdenlive-23-04-0-released/
- /fr/2023/04/kdenlive-23-04/
- /it/2023/04/kdenlive-23-04-0/
- /de/2023/04/kdenlive-23-04-0-2/
featured_image_bg: images/features/2212.png
---

<div class="alert alert-info" role="alert">
    <strong>Attention:</strong> Kdenlive version 23.04.0 comes with a new project file version 1.1 which is not backward compatible. Once opened in 23.04.0 you cannot open the project file in older versions.
</div>

We are excited to announce the availability of the latest Kdenlive 23.04.0 version. This is a very special milestone for us as this marks the first release since the start of our successful fundraising. Kdenlive is an open source project, and as such we try to be as transparent as possible on how the development is going and where the money from the campaign is going. A detailed transparency report will be published in the next days.

The big highlight in today's release is the support for nested timelines. This allows you to open several timeline tabs to work on different sequences of your project. These sequences can then be assembled to create a final project. Another addition is the support of the Whisper speech to text engine, in addition of the already supported VOSK back-end. This brings an improved accuracy, support for many languages, and the possibility to translate to English on the fly. And we also finally fixed a longstanding issue that caused the Kdenlive UI to show texts in English instead of the requested language.

A lot of polishing and bug fixing work has been done in this release, some highlights include: improved handling of imported clips with huge sizes, snappier playback of clips in timeline, added option to render in full color range, added warning when cached data exceeds 1GB, improved filtering of the Project Bin to mention a few. Some great work was also done by the MLT team on our video back-end, meaning that the audio cracks that could happen on some projects should be fixed in the latest MLT development version used in our binary packages (AppImage, Mac and Windows).

Many exciting changes are also on our roadmap for this year! For the next release, we will improve the effects workflow, and for the end of the year, we should hopefully have the Qt6 version with improved GPU support.

## Nested Timelines

With nested timelines, you can work on individual parts of your project separately, and then combine them into a larger project once you're done. This feature can help you to be more efficient and productive when working on complex video projects. Overall, nested timelines are a valuable addition to Kdenlive that has been planned ever since the timeline refactoring milestone (version 19.04) and we hope that they will help you to create more sophisticated video projects with ease.

![](nested-timelines.gif)

## Whisper

This release comes with **[OpenAI's Whisper][1]** speech recognition system which handle punctuations flawlessly, supports many languages and has the option to automatically translate generated texts to English. Do note that for a better experience it is recommended to have a GPU for processing. Whisper can be used in the Text to Speech widget or for generating subtitles.

Whisper can be easily setup by a config wizard in the settings panel.

![](whisper-settings.png)

![](whisper-subtitles.png "Subtitle generation widget")

![](speech-editor.png "Text Editor widgetText Editor widget") 

## Effects and Transitions

This release comes with a variety of improvements and fixes to the effect and transition stack. New parameters (crop, adjust scaling and aspect ratio) have been added to the defish effect, the color value in many effects is now keyframeable and an improved audio normalizer. For those who require finer adjustments, pressing shift while dragging sliders now allows for adjustments on a one-by-one step basis, providing greater control and accuracy.

### New Timer Effect

The new timer filter allows to add a counter overlay on your videos with the possibility to style the font, position, format and more. It can count up or down according to the duration of your clip. You may add it to a track or on master as well.

{{< video src="timer-effect.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

### New Transitions

This version comes with new keyframeable directional (up, down, left and right) transitions such as _slide_, _wipe_, _push_, _barn door_, _circle_ and _rect_.

![](slide-left.gif "Slide left")

![](push-right.gif "Push right")

![](wipe-up.gif "Wipe up")

![](barn-wipe-hori.gif "Barn wipe horizontal")

![](circle-wipe.gif "Circle wipe")

![](rect-wipe.gif "Rect wipe")

##### KDE Store

Wait there's more! The [**KDE Store**][2] has a new category for _Kdenlive project files_ allowing to download nifty animated templates made by the community.

![](msf-3-4-push-up.gif "Push up")

![](msf-4-4-reveal-slide-rotor.gif "Reveal slide rotor") 

![](msf-4-7-push-left.gif "Push left") 

![](msf-4-2-slide-in.gif "Slide in")

## Subtitles

Kdenlive 23.04 brings several significant improvements to subtitle handling. When importing subtitles, if encoding cannot be confidently detected it defaults to UTF-8. A drop-down list has been added with available encoding formats in case it needs to be overridden.

![](subtitle-encoding.png) 

When cutting subtitles with the razor tool _(x)_ or cut clip function _(shift + r)_ there is now an option in the settings to either duplicate the subtitle text or split them after the first line.

![](subtitle-cut-option.png) 

### Duplicate text

{{< video src="duplicate.mp4" autoplay="true" muted="true" loop="true" controls="false">}}

### Split after first line

{{< video src="split.mp4" autoplay="true" muted="true" loop="true" controls="false">}}


 [1]: https://github.com/openai/whisper
 [2]: https://www.pling.com/s/App-Addons/browse?cat=702&page=1&ord=latest
 
