---
author: Farid Abdelnour
date: 2017-08-17T16:49:06+00:00
aliases:
- /en/2017/08/kdenlive-17-08-released/
---

![Screenshot of Kdenlive version 17.08.0](screenshot.png)

Kdenlive 17.08 is released bringing minor fixes and improvements. Some of the highlights include fixing the Freeze effect and resolving inconsistent checkbox displays in the effects pannel. Downloaded transition Lumas now appear in the interface. Now it is possible to assign a keyboard shortcut for the Extract Frame feature also a name is now suggested based on the frame number. Navigation of clip markers in the timeline behave as expected upon opening the project. Audio clicks issues are resolved although this requires building MLT from git or wait for a release. In this cycle we've also bumped the Windows version from Alpha to Beta.

We continue steadfastly making progress in the refactoring branch due for the 17.12 release. We will soon make available a package for testing purposes. Stay tuned for the many exciting features coming soon.
