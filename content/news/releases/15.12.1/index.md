---
title:  Kdenlive 15.12.1 information page 
date: 2016-01-12
---

Kdenlive 15.12.1 was released in January 2016 as part of the KDE Applications release. This version fixes many bugs reported in the 15.12.0 version, all users are encouraged to upgrade to this new version. Among the changes, Kdenlive will now automatically use the Breeze widget style if available when used on a non KDE Desktop. Several timeline corruption issues were also fixed. Here is a non-exhaustive list of fixes:
