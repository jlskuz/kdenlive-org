---
date: 2006-05-30
author: Jean-Baptiste Mardelle
---

We are proud to announce the **immediate release of Kdenlive 0.3**, a non linear video editor for KDE. This is the first release using the great [MLT video framework](https://www.mltframework.org/), combined with the power of [FFMPEG](https://ffmpeg.org). This brings real time effects and transitions to Kdenlive, with the support of many video formats.

While Kdenlive cannot yet be compared to a full featured professional video editor, it already has all basic features to create your movie:

- Video, audio, color, image and text clips
- Transitions (crossfade, picture in picture, ..)
- Effects (grayscale, sepia, luma,...)
- Multi track editing and mixing
- Export to several popular formats
- Video thumbnails
- Dual monitor display
- Customizable layout

You will find more information on our website and our Wiki. And for the curious, here is a link to a [screenshot](/historic-screenshots).
Please note that firewire import/export is not implemented yet, but is on top of the planned features. We are encouraging everyone to try this release and give some feedback so that we can bring easy and powerful video editing to the KDE Desktop.

This release is also a call for everyone interested to join the project. We need developpers, graphic artists, testers and volunteers for the packaging!

A big thanks to all the people that contributed to this release, and we hope that you enjoy Kdenlive.

_Jean-Baptiste Mardelle_
