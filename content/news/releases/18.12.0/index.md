---
title: 18.12 release and some news
author: Farid Abdelnour
date: 2018-12-14T16:12:11+00:00
aliases:
- /en/2018/12/18-12-release-and-some-news/
- /fr/2018/12/version-18-12-et-quelques-news/
- /it/2018/12/versione-18-12-e-altre-novita/
---

## 18.12 release

Kdenlive 18.12 is out. In this version we have fixed some crashes and made some other improvements.

See below for the full changelog.

## Refactoring

If you were waiting for the refactoring version, we're afraid you'll have to wait a bit longer. We decided to postpone it for the 19.04 release cycle which will give us more time to polish all the edges and add some new nifty features.

* We now have a nightly build system so you can try all the latest features;
* Among the highlights since the last release;
* Added parallel processing feature for render speed improvements;
* Added hardware acceleration for proxy clip creation;
* Blackmagic Design decklink output is back;
* The Speed effect has been reintroduced;
* Made keyframe improvements and timeline clip keyframeable GUI.

Please help us test and report your feedback in the comments.

## In other news

After the bug squashing day an interested developer joined the the team and is fixing MOVIT (GPU effects) support. We are very happy to see more people interested in contributing code to the project. Check out our [Junior Jobs list][1] and send your patches.

On the Windows front, we have implemented many improvements, among them the hanging process on exit is fixed but more on that next week. 😉

The team has also started brainstorming the interface redesign. You can follow the progress (and contribute) in [this refactoring task][2].

![](wip-timeline.png)

  [1]: https://phabricator.kde.org/project/view/40/
  [2]: https://phabricator.kde.org/T10085
