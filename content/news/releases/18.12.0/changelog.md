* Backport crash on image sequence import. [Commit.](http://commits.kde.org/kdenlive/2191e921fd5f32fb90b17eb0266a1bd3338ad711) 
* Backport fix for titler text gradient. [Commit.](http://commits.kde.org/kdenlive/e27d5801b555775b308c8931e04f5cef032f1b90) 
* Add donation url to appdata. [Commit.](http://commits.kde.org/kdenlive/f5565c3177e8a75bdafdf50e0592d85ff9b32c1e) 
* Fix minor EBN issues and typos. [Commit.](http://commits.kde.org/kdenlive/161d2ac3efb6d503ebf0b1527cec15d00b72535e) 
* Fix play/pause on Windows. [Commit.](http://commits.kde.org/kdenlive/cd50b3e744fdd4e4030e3ff18d522573f19eb47d) 
* Sync quickstart manual with UserBase. [Commit.](http://commits.kde.org/kdenlive/3837bd71c6a4ad89e49a8a1f3161839bd534d9d6) 
* Install doc files. [Commit.](http://commits.kde.org/kdenlive/169648d1e98cc0c30c1f8c7b6cdc0ab68e6bd877) 
* Make it compiles when we use QT_NO_NARROWING_CONVERSIONS_IN_CONNECT. [Commit.](http://commits.kde.org/kdenlive/079b88eb12ea28e88f99c3b0564c46dfe9a003b9) 
* Fix minor EBN issues. [Commit.](http://commits.kde.org/kdenlive/79c3d3a3967155be6c2f4c22fbfb15e174c2d0de) 
