---
title: Kdenlive packages available for testing
author: Farid Abdelnour
date: 2016-10-24T08:35:49+00:00
aliases:
- /en/2016/10/kdenlive-packages-available-for-testing/
pre_downloads:
- platform: linux
  sources:
  - name: AppImage
    url: https://download.kde.org/unstable/kdenlive/16.12/linux/
---

Getting the correct dependencies and up to date version of required libraries has always been a challenge for Kdenlive users. So we are pleased to announce that we now provide binary packages for download. These packages contain the latest Kdenlive development version (git master), as well as current development versions of MLT, Frei0r, OpenCV. The GPU movit library is not included at this stage. There might be some performance hit due to the nature of the formats, but these packages will be most helpful to debug and test the alpha/beta versions so that we can provide better releases. It will also help to identify issues linked to missing dependencies or outdated libraries on your system.

So if you are ready, you can download **Kdenlive's first AppImage** below!

Then, simply make the file executable and run it! In a terminal:

```bash
chmod a+x kdenlive-16.12-alpha1-x86_64.AppImage
./kdenlive-16.12-alpha1-x86_64.AppImage
```

We also provide a **Snap package** available for testing on Ubuntu distros:  

```bash
sudo snap install --edge --force-dangerous --devmode kdenlive-devel
kdenlive-devel
```

The snap package is in the edge channel and requires "devmode" because of current limitations in the snap format.

These packages will not touch your system's libraries, and can easily be removed. Please leave your feedback in the comments or on our [mailing list][1](registration required) / [forums][2].

We will provide several updates to this packages before the Kdenlive 16.12 release so that you can follow the development / bug fixes.

 [1]: https://mail.kde.org/mailman/listinfo/kdenlive
 [2]: https://forum.kde.org/viewforum.php?f=262
