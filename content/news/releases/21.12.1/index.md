---
author: Farid Abdelnour
date: 2022-01-07T19:56:33+00:00
aliases:
- /en/2022/01/kdenlive-21-12-1-released/
- /fr/2022/01/version-de-maintenance-21-12-1/
- /it/2022/01/kdenlive-21-12-1-2/
- /de/2022/01/kdenlive-21-12-1/
---

The first maintenance release of the 21.12 series is out with fixes to ripple mode, project archiving and multiple bins. This version also enforces to transcode footage with variable framerates to a standard framerate value.
