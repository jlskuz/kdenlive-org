  * Fix multiple bins should always stay tabbed together. [Commit.][1]
  * Fix shortcuts sometimes broken on fullscreen monitor. [Commit.][2]
  * Enforce 29.97 fps when using a clip with 29.94 or 29.96 fps. [Commit.][3]
  * Fix audio thumbs not created after profile change. [Commit.][4]
  * Fix compilation warnings (function type compatibility). [Commit.][5]
  * Ripple: fix strange behaviour on Windows and macOS. [Commit.][6]
  * Add xml ui for audiolevelgraph effect and other xml format fixes. [Commit.][7]
  * Improvements and fixes for the status bar message field. [Commit.][8]
  * Add ripple test for single track groups. [Commit.][9]
  * Fix ripple in several scenarios with groups. [Commit.][10]
  * Improve Keybind Info with compositions. [Commit.][11]
  * Fix crash on clip insert in ripple mode. [Commit.][12]
  * Fix archiving. [Commit.][13]
  * Fix keyframe disappearing in timeline after moving the previous one in effect stack. [Commit.][14]
  * Don’t allow undo when resizing clip/composition (fixes crash). [Commit.][15]
  * Fix freeze on multiple title clip duplication. [Commit.][16] Fixes bug [#443507][17]
  * Fix mistake in last commit. [Commit.][18]
  * Various fixes on project opening with missing proxies (playlist and timeremap broken). [Commit.][19]
  * Add more ripple tests. [Commit.][20]
  * Fix ripple of groups after commit c1b0f275. [Commit.][21]
  * Restructure ripple code to make it possible to run more tests. [Commit.][22]
  * Fix mix corruption when moving a clip with start and end mixes to another track, add test. [Commit.][23]
  * Fix concurrency crash with autosave and multicam mode. [Commit.][24]
  * Fix crash on extract frame if image was already part of the project. [Commit.][25]

 [1]: http://commits.kde.org/kdenlive/f71fde9f00e7b9a4fe897ca1bbc4fad37421131e
 [2]: http://commits.kde.org/kdenlive/3a5964ca4868962938d2dfbd49fcbf2b8016818b
 [3]: http://commits.kde.org/kdenlive/f59033daba9060f384e69e57af3a6cf245b8830a
 [4]: http://commits.kde.org/kdenlive/96c4dc5f3f11f3ba102a48f256c0b6437be903bd
 [5]: http://commits.kde.org/kdenlive/5b8ff6de6527963a4a2f8575f94b5e518e1f88ad
 [6]: http://commits.kde.org/kdenlive/569374950a17eaa03a7e69e25d5c3fad7b6617de
 [7]: http://commits.kde.org/kdenlive/08b1465b72f82fa6d3e4096efac8547ee2737b8f
 [8]: http://commits.kde.org/kdenlive/0b0face087c7d2cfc5395aa7de3f94e3ac871302
 [9]: http://commits.kde.org/kdenlive/672ef532630eae5dc98cb44277f3334fcf8c00b1
 [10]: http://commits.kde.org/kdenlive/75cbe22a43fd2d334483b8faaedcf2cd284b60d6
 [11]: http://commits.kde.org/kdenlive/d0a4f2d9dc636c394959ca2f6975798ea6173291
 [12]: http://commits.kde.org/kdenlive/8a2ed43d5a0b2735476929308ca96e57d580b3a4
 [13]: http://commits.kde.org/kdenlive/17d0ce55dc0f88f4f4a2dda7ce084c407b79e6a6
 [14]: http://commits.kde.org/kdenlive/1f0520404258c104f9de669e999e7400151061c3
 [15]: http://commits.kde.org/kdenlive/9e4d9f5ee4c98eb8817f5f36d5f9a4a6f678baea
 [16]: http://commits.kde.org/kdenlive/6d2d09cc16bd7d44d2b590fcecd6e34aba6f9e49
 [17]: https://bugs.kde.org/443507
 [18]: http://commits.kde.org/kdenlive/41b5ebc0b860f002ed72f9b87eaa07dcd7e9a943
 [19]: http://commits.kde.org/kdenlive/96ab36c4d2470eb19927d4e82b7e40242a925ba8
 [20]: http://commits.kde.org/kdenlive/9101fa7aff246b8a654e0b21ec3f23106f1a6691
 [21]: http://commits.kde.org/kdenlive/cdc5d15fc358a021312dd9fc466415d81578b474
 [22]: http://commits.kde.org/kdenlive/d38d6c6a8fe6f3e6dc84215acebaedc1dace1ec7
 [23]: http://commits.kde.org/kdenlive/eff592e4178f579b76e607eec08d03599bfd1827
 [24]: http://commits.kde.org/kdenlive/b1ab19792c086aa32a2661e660ff704db2c3c0f5
 [25]: http://commits.kde.org/kdenlive/7c96bd22239bdcd2d9554f3cf8d6324fc51fc8e0
