---
author: Farid Abdelnour
date: 2017-02-11T13:45:36+00:00
aliases:
- /en/2017/02/kdenlive-16-12-2-released/
---

![Screenshot of the Kdenlive clip monitor with a clip of a horse and the about dialog showing version 16.12.2](screenshot.png)

The second maintenance release of the 16.12 series is out, part of [KDE Applications 16.12.2][1].

This release fixes startup crashes with some graphic cards, as well as some fixes to MOVIT (GPU effect processing) and minor stability issues. The Appimage version as well as our PPA's were updated, check our [download section][2] for instructions. An updated Windows version will be released in the next days. This is a relatively small update since all our efforts are currently focused on the timeline refactoring branch which will bring professional grade new features and more stability. Stay tuned for more news!

On the community side of news our official [G+ channel][3] just reached 500 members and we have seen an increase in our Microsoft Windows userbase after our alpha release.


 [1]: https://kde.org/announcements/applications/16.12.2/
 [2]: /download
 [3]: https://plus.google.com/u/0/communities/114460978880033344765
