---
title: 23.04.0a hotfix release
author: Farid Abdelnour
date: 2023-04-30T12:30:04+00:00
aliases:
- /en/2023/04/23-04-0a-hotfix-release/
# featured_image: /images/features/hotfix.png
---

We would like to announce the immediate availability of Kdenlive 23.04.0a fixing an issue where projects would fail to load under certain circumstances. We recommend all users to update.
