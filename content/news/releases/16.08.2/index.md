---
author: Farid Abdelnour
date: 2016-10-13T00:00:25+00:00
aliases:
- /en/2016/10/kdenlive-16-08-2-released/
---

This is the second service release of the 16.08 cycle with a total of 36 commits fixing many keyframe related problems as well as improvements to proxy clip rendering, user interface, workflow and compilation issues.

This cycle saw the launch of the [Toolbox][1] section of the website, a collection of posts covering in depth reviews of new and existing tools and features.

 [1]: /toolbox
