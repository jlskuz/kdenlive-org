---
author: Farid Abdelnour
date: 2019-04-22T08:00:19+00:00
aliases:
- /en/2019/04/kdenlive-19-04-released/
- /it/2019/04/kdenlive-versione-19-04/
- /de/2019/04/kdenlive-version-19-04/
---
We are happy and proud to announce the release of Kdenlive 19.04 refactored edition concluding a 3 year cycle in which more than 60% of the code base was changed with +144,000 lines of code added and +74,000 lines of code removed. This is our biggest release ever bringing new features, improved stability, greater speed and last but not least maintainability (making it easier to fix bugs and add new features).

## New refactored timeline

### Split Audio/Video

We have changed the way timeline tracks work. Each track is now either audio or video, and will only accept audio and video clips respectively. When dragging an AV clip from the project bin in timeline, the clip will be automatically split, the video part going on a video track, and the audio part on an audio track.

![](splitAV.gif)

### Configurable layout

Tracks can be individually resized. (Holding down shift makes all video or audio tracks change in height simultaneously.) 

![](resize-tracks.gif)

Switch live between two different layout modes (Mixed or Split).

![](layout-modes.gif) 

### Keyboard Navigation

You now have the possibility to move clips and compositions with your keyboard. To do it, select a clip in timeline and use the &#8220;Grab Current Item&#8221; (Shift+G) function from the Timeline menu.

![](shift-g.gif) 


You can then move the item with your arrow keys. Keyframes can also be moved individually. Just click on a keyframe in timeline, then move it left/right with arrows, change its value with + and -. Alt+arrow to go to another keyframe.

![](moving-keyframes.gif) 

### Improved keyframe handling

  * Add a new keyframe by double clicking in timeline.
  * You can move a keyframe without altering it&#8217;s value by using the vertical line that appears when you are above or below a keyframe.
  * Remove a keyframe by dragging it far above or below the clip limits.

![](keyframe-improvements.gif) 

### Audio Recording

An option in track headers allows you to have audio record controls. This enables you to play your project while recording a voice over comment. Very handy for making tutorials. (Not available in the AppImage version yet.)

![](audio-recording.gif) 

### Other improvements

  * Configurable thumbnails for each track. You can choose to display between In frame, In/Out frames, All frames or No thumbnails.
  * Improved workflow due to keyboard shortcuts and track targets.
  * Added group hierarchy (group of groups).
  * You can now copy/paste timeline clips between different projects (if you have 2 running instances of Kdenlive or after opening a different project) using the standard copy/paste.
  * Clip name always displayed when scrolling.
  * Individual clips can now be disabled while still in the timeline but with no audio and no video &#8211; (works for all clip types)
  * Faster timeline preview with support for hardware accelerated encoding.
  * Improved feedback on move/resize.
  * Automatic timeline color switch when changing the color theme.
  * When using the Razor tool (x), you can now press shift while moving the mouse cursor over a clip to preview the cut frame.

## Project Bin

  * Improve management of proxy clips (can now be deleted for each clip).
  * Shortcut icon to drag only the audio or only the video of a clip in timeline.
  * Improved management of audio/video streams, also allowing to enable/disable audio or video for a clip.

![](bin.gif) 

## Monitors

  * Monitor toolbar: move with the mouse to the upper right corner of the monitor to access the toolbar.
  * Support multiple guide overlays.
  * Shortcut icon in clip monitor to drag only the audio or only the video of a clip in timeline.
  * Support for external monitor display using Blackmagic Design decklink cards.

![](monitors.gif) 

## Effects and Compositions

  * Major improvements to the Speed effect.
  * Added back the Motion Tracker.  (For compiled MLT versions with OpenCV support only.)
  * You can now define preferred effects and compositions. These will appear in the timeline context menus and in the effect/composition baskets for easier access.
  * Organized effects under 3 categories, “Main” (which contains the most important audio/video effects), “Video effects” and “Audio effects”.
  * [Tested and removed][1] all effects which are not working.
  * When dragging a composition from the list into timeline, it will automatically adjust its duration (to fit the entire clip or the duration of a clip overlap depending on the context).
  * Move the whole Rotoscoping mask at once. Moving all points in the mask: A cross is displayed at the center of all points that allows moving the whole mask.
  * 1 click transitions

![](speed.gif) 

![](effects-compositions.gif) 

## Titler

  * Added configurable visual guides.
  * Visualize missing elements, allowing to move or delete them.
  * A combobox allows you to choose the default background, between checkered, black and white.
  * Align buttons now cycle to allow you to align to safe margins and frame border instead of just frame border.

## Rendering

  * Added support for HW accelerated render profiles. (Experimental)
  * Added support to render with a transparent background for export formats that support alpha channel. (Experimental)

## Resources

Since a long time, Kdenlive allowed users to download extra user contributed resources from the [KDE Store][2]. These download options have now been moved to more logical places instead of the &#8220;Settings&#8221; menu. Title templates can be downloaded from the Titler dialog, Render profiles from the Render dialog, Luma compositions from the Composition list and Shortcut layouts from the Shortcuts dialog.


## But…

There is still work to be done! We concentrated on stability (finding and fixing as many crashes as possible) and that the most important functionality for a slim editing workflow works. There are [still known issues][3] in which we will dedicate the next 3 monthly releases of the [19.04 cycle][4] to finish polishing the rough edges.

So why release? Well, this version is much better than the old one. Having it out in the wild will help us test it under various situations. We now have a stable platform allowing us to fix issues quicker in order to move on to adding new features.

## What's next?

For 19.08 we hope to merge the refactoring of the Titler, a GSOC project, and begin implementing the professional features in our roadmap starting with advanced trimming tools. Work also has started to improve OpenGL support (better GPU usage) so this year is promising to be the foundation for entering into professional video editing waters. Stay tuned!

 [1]: https://community.kde.org/Kdenlive/Development/CleaningEffects#Table_of_effects
 [2]: http://store.kde.org
 [3]: https://invent.kde.org/kde/kdenlive/issues
 [4]: https://community.kde.org/Schedules/Applications/19.04_Release_Schedule
