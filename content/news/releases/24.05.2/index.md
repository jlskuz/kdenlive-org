---
author: Farid Abdelnour
date: 2024-07-08T19:41:24+00:00
aliases:
- /en/2024/07/kdenlive-24-05-2-released/
- /it/2024/07/kdenlive-versione-24-05-2/
- /de/2024/07/kdenlive-24-05-2/
discourse_topic_id: 18422
---

The second maintenance release of the 24.05 series is out.
