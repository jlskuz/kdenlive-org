  * Fix guides categories not correctly saved in document. [Commit][1]. Fixes bug [#489079][2].
  * Fix adding record track adds a normal audio track. [Commit][3]. Fixes bug [#489080][4].
  * Fix rendering with aspect ratio change always renders with proxies. [Commit][5].
  * Fix compilation on Windows with KF 6.3.0. [Commit][6].
  * Fix timeline duration not correctly updated, resulting in audio/video freeze in timeline after 5 min. [Commit][7].
  * Fix Windows build without DBUS. [Commit][8].
  * Fix crash on spacer tool with subtitles. [Commit][9].

 [1]: http://commits.kde.org/kdenlive/9e14a96dbe3720eac0683f894053d7a0cce48631
 [2]: https://bugs.kde.org/489079
 [3]: http://commits.kde.org/kdenlive/ae67b4bed4dac64a86e454f291992c89379084a3
 [4]: https://bugs.kde.org/489080
 [5]: http://commits.kde.org/kdenlive/752767c9f2566626a9c2ea55887a4dda9e663c30
 [6]: http://commits.kde.org/kdenlive/de682e01e4706c1ef799fc938f74edd3591f9dfd
 [7]: http://commits.kde.org/kdenlive/8be0e826471332bb739344ebe1859298c46e9e0f
 [8]: http://commits.kde.org/kdenlive/8e20374f0f803371ef8c22f522a25377398424db
 [9]: http://commits.kde.org/kdenlive/9cb547030b0b7be155c07e9b17d72161a66d6494
