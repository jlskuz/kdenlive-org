* Fix crash & corruption on dragging multiple clips in timeline, fix thread warning on monitor refresh. [Commit.](http://commits.kde.org/kdenlive/c17302f7eacbf08bb6a2c77c5a5e90d657d1b926) 
* Avoid possible profile corruption with xml producer. [Commit.](http://commits.kde.org/kdenlive/463e9f845318c6ffa83be97ed81da18af84f75aa) See bug [#371189](https://bugs.kde.org/371189)
* Avoid relying on xml to clone a clip. [Commit.](http://commits.kde.org/kdenlive/dc80cd7352e01fd7e4b29aa3953f8f577b702a86) See bug [#377255](https://bugs.kde.org/377255)
* Src/dvdwizard/dvdwizardmenu.cpp: do not show "grid" in output. [Commit.](http://commits.kde.org/kdenlive/dbc6793d3fe04089fbebfba5f8d2ecf8de614783) Fixes bug [#377256](https://bugs.kde.org/377256)
* Src/dvdwizard/dvdwizard.cpp: fix file loading in slotLoad. [Commit.](http://commits.kde.org/kdenlive/76ceba16400109e991a43684f75ab91c76106749) Fixes bug [#377254](https://bugs.kde.org/377254)
* Fix Render Widget's file dialog not working correctly. [Commit.](http://commits.kde.org/kdenlive/0f1f490cfc66566fa12421b5103f8f29d62c86e6) Fixes bug [#371685](https://bugs.kde.org/371685)
* Fix render job duration when past midnight. [Commit.](http://commits.kde.org/kdenlive/28816933a37a4799d8cade1b04c80bdde417911a) Fixes bug [#372370](https://bugs.kde.org/372370)
* Fix Bin Effect reset. [Commit.](http://commits.kde.org/kdenlive/d85742fcb1b62dcf1b0f4e03532072615499fca8) Fixes bug [#376494](https://bugs.kde.org/376494)
* Fix unnecessary refresh of tools when selecting titler item. [Commit.](http://commits.kde.org/kdenlive/a6ac89e1ad406c74d5169080cf9277ef685fb832) 
* Fix fadeouts re-appearing on clip cut+resize. [Commit.](http://commits.kde.org/kdenlive/a5490e5609d3676ec6cf12d9013d4e6854347171) 

