---
title: Kdenlive 16.12.3 is out
author: Farid Abdelnour
date: 2017-03-09T16:38:07+00:00
aliases:
- /en/2017/03/kdenlive-16-12-3-is-out/
---

![Screenshot of Kdenlive with the version number 16.12.3 shown on the project monitor](screenshot.png)

The last release of the 16.12 branch brings a few, but important improvements, like fixing a couple of crashes and avoiding a possible corruption as well as a overnight render bug along with other minor stability improvements. All in all 16.12 was a great release and the best is still to come.

We continue our focused effort in the timeline refactoring which will bring professional grade tools, stay tuned for more info on that soon!
