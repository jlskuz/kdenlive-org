---
author: Farid Abdelnour
date: 2022-10-17T19:23:51+00:00
aliases:
- /en/2022/10/kdenlive-22-08-2-released/
- /it/2022/10/kdenlive-22-08-2-2/
- /de/2022/10/kdenlive-22-08-2/
---

The second maintenance release of the 22.08 series is out with many usability and bug fixes. Some highlights include fixed pasted "ghost" keyframes, effects overlay now properly scale on monitor zoom, loopable image sequence clips are working again and VP8 alpha renders don't crash anymore. This version also comes with some quality of life improvements like when pasting a clip the cursor moves to the last frame and remembering the effect's keyframe status. We've also added Pixabay videos to the online resources module.
