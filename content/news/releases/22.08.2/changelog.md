  * Fix config and render ui tab order. [Commit.][1]
  * Fix pasting ungrouped audio clip sometimes landing on unexpected track or complaining there is not enough tracks. [Commit.][2]
  * Fix tests. [Commit.][3]
  * Ensure resource providers are not listed twice. [Commit.][4]Fixes bug [#460060][5]
  * Make timecode display listen to profile change and automatically adjust fps. [Commit.][6]
  * Remember effect keyframe status (show/hide). [Commit.][7]
  * Seek to item last frame on paste. [Commit.][8]
  * Fix effect overlay not properly scaling on monitor zoom. [Commit.][9]
  * Timeline preview: ensure we don’t insert chunks of the wrong size (would cause 1 on 2 chunks to fail insert), ensure the orange “working” chunks disappears on stop. [Commit.][10]
  * Fix possible profile corruption when switching to a never used profile. [Commit.][11]
  * Fix crash closing proxy test dialog. [Commit.][12]
  * [Image Sequences] Fix loop option. [Commit.][13]Fixes bug [#382432][14]
  * [Image Sequence] Fix wrong thumbnails. [Commit.][15]
  * Fix thumbnails for loopable clips. [Commit.][16]
  * [Online Resources] Add provider for Pixabay Videos. [Commit.][17]Fixes bug [#435569][18]
  * [Online Resource Providers] Support object downloadUrls arrays. [Commit.][19]
  * [Resource Widget] Fix open license and provider website. [Commit.][20]
  * Fix track audio level empty on pause. [Commit.][21]
  * Align master audio level with MLT’s audiolevel filter (use only the first 200 samples). [Commit.][22]
  * Don’t add unnecessary audio level filter on master. [Commit.][23]
  * Minor fix for updated MLT audiolevel filter (will fix track levels). [Commit.][24]
  * Deprecate MLT’s old boxblur filter (replaced with new box_blur effect). [Commit.][25]
  * Fix resetting keyframe selection after deleting a keyframe from timeline. [Commit.][26]
  * Fix pasting effect with keyframes partially broken. [Commit.][27]
  * Correctly preselect timeline toolbar when editing it from context menu. [Commit.][28]
  * Fix effect stack view incorrect on hide keyframes (was still showing the timecode). [Commit.][29]
  * Fix ghost keyframes created when pasting an effect to a clip that has a crop start smaller than source clip and on clip speed resize. [Commit.][30]
  * Fix wrong timecode offset in keyframewidget of transitions. [Commit.][31]Fixes bug [#439748][32]
  * Fix crash on bin clip deletion with instance on locked track. [Commit.][33]Fixes bug [#459260][34]
  * Add test for bin clip deletion with instance on locked track. [Commit.][35]See bug [#459260][34]
  * Fixed thumbnail cache not being rebuilt anymore in “Show video preview in thumbnails” mode. [Commit.][36]
  * Don’t update keyframe parameters when changing a keyframe selection state. [Commit.][37]
  * Fix tests crash. [Commit.][38]
  * Fix vp8 with alpha render crash. [Commit.][39]
  * Don’t delete audio tasks when switching profile. [Commit.][40]
  * Fix usage count column visible in bin. [Commit.][41]
  * Fix uninitialized var messing audio record and possible crash. [Commit.][42]
  * Fix sorting by date not working for newly inserted clips, other sorting issues. [Commit.][43]See bug [#458784][44]
  * Don’t mess rotation data on proxy transcoding. [Commit.][45]

 [1]: http://commits.kde.org/kdenlive/b96f3f051cdc0af88ac64c04b9c18e8b49c79445
 [2]: http://commits.kde.org/kdenlive/0f31ad668d51f178d1e416f2943909ac6cccf824
 [3]: http://commits.kde.org/kdenlive/834732dc785c1e5da64e7b3d3c18f201d56b5370
 [4]: http://commits.kde.org/kdenlive/c60b4f05c363293a542b38426f0fd3335f495ce1
 [5]: https://bugs.kde.org/460060
 [6]: http://commits.kde.org/kdenlive/98910a9651f44c453bc500aa022ee8331d0f5dad
 [7]: http://commits.kde.org/kdenlive/528f5b8f6f2bc42ad0907e89e1676f585e9fd03a
 [8]: http://commits.kde.org/kdenlive/da41e086b742edf1b2ea39a52aebf9973f36f68a
 [9]: http://commits.kde.org/kdenlive/0d159652b306d3d7ce3607ff8778390b2276cb0b
 [10]: http://commits.kde.org/kdenlive/bb8e30082543a4e47ecf00afdae15881cbc9b359
 [11]: http://commits.kde.org/kdenlive/8040e2afd37420ba962c42b6f2e2aa9d7b0c7093
 [12]: http://commits.kde.org/kdenlive/0add3c68425fb96301b57cd195cb9d3d68837341
 [13]: http://commits.kde.org/kdenlive/5010483eb9e07be81fea8a3e9e25e100ceab4bc0
 [14]: https://bugs.kde.org/382432
 [15]: http://commits.kde.org/kdenlive/dc88cc22222293ef6e40483d00a9f714eb896505
 [16]: http://commits.kde.org/kdenlive/a2c9a6efe32f03778647cea7d0ce723ee073d848
 [17]: http://commits.kde.org/kdenlive/f65a48a07182c5a6aa0322caa96680c99ef4462f
 [18]: https://bugs.kde.org/435569
 [19]: http://commits.kde.org/kdenlive/05dda422e625daf3c14a70c72d4fbd562e618640
 [20]: http://commits.kde.org/kdenlive/b2145f1ea45a4632f12e6611d86d23f9ae70aea9
 [21]: http://commits.kde.org/kdenlive/e5e2c1131e61c1388f0832f2b67678322a759eba
 [22]: http://commits.kde.org/kdenlive/248217d32b495d4b7d9772701675d0ceeaea567f
 [23]: http://commits.kde.org/kdenlive/287442ab0112b01a07873d5cd187a70486b8928c
 [24]: http://commits.kde.org/kdenlive/25957fa0f25047a939539a135df4614aa980e752
 [25]: http://commits.kde.org/kdenlive/1ff1c163028c19938a78f8650ec119e07fecd72e
 [26]: http://commits.kde.org/kdenlive/058c66c4c5060bda7a6838a8115e5cd720b1b0bf
 [27]: http://commits.kde.org/kdenlive/9ef2ee5ea3f9e9450858d430792db30493fb00b5
 [28]: http://commits.kde.org/kdenlive/755def5b963f6ec8787c93cbd24a9a01e45a60d7
 [29]: http://commits.kde.org/kdenlive/7fb1bea555a667474b4c42881e7a8a8eb18da6f8
 [30]: http://commits.kde.org/kdenlive/d9d663ed70c0d9b7217d157187d699b4f6f3f9b9
 [31]: http://commits.kde.org/kdenlive/246682a095700df3081b258bb0623659209336b8
 [32]: https://bugs.kde.org/439748
 [33]: http://commits.kde.org/kdenlive/505a09f8bad262540aefd51d691a1641f91cadc2
 [34]: https://bugs.kde.org/459260
 [35]: http://commits.kde.org/kdenlive/96a87abe0148104e985b09585aec8cd76356c250
 [36]: http://commits.kde.org/kdenlive/854d3ffa346cc3f4026390a2057b8aa1eb48317d
 [37]: http://commits.kde.org/kdenlive/825ad3473e790ee25690a30fab67606d70deff14
 [38]: http://commits.kde.org/kdenlive/c682bae55e2e56f630d3ecf0bc1bd68f57908819
 [39]: http://commits.kde.org/kdenlive/855ca5f4294251892c72aaf2399df0543c8c018d
 [40]: http://commits.kde.org/kdenlive/cb9784cdb0ba693af941fc45c8fe6a106980ab5e
 [41]: http://commits.kde.org/kdenlive/ed83b193e2794b3a9669c99aa6cb038416efd656
 [42]: http://commits.kde.org/kdenlive/a66448d362702828a9652610ef11bf312383230e
 [43]: http://commits.kde.org/kdenlive/daabb0ab708a27caf6da5a2b6ec2d91d08881f44
 [44]: https://bugs.kde.org/458784
 [45]: http://commits.kde.org/kdenlive/34e66aaafdd07ec925ea06d16f3f33d0c0182fde
