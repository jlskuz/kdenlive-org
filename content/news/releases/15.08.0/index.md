---
title: 'Kdenlive 15.08: 4 years of maturation, and how to help Free Software to live'
author: Vincent Pinon
date: 2015-08-18T08:09:00+00:00
aliases:
- /node/9442
- /discover/15.08.0
# Ported from https://web.archive.org/web/20160406103202/https://kdenlive.org/node/9442
---


Finally it is ready: Kdenlive 15.08 is an important accomplishment!

As usual, you can read a short changelog (below), [download the code][2], [try it][3] and [report issues and submit patches][4]...

If you follow our news, you might know that this release integrates work started for several years... that just reached a satisfactory state (hopefully :\)

* great parts of code have been refactored, an effort [discussed in 2011][5], with a short [funded job in 2012][6], then [re-targeted in 2014][7] (Randa Meetings twice!)
* this now enabled to brings back GPU effects from Movit, [demoed in 2014][8] (FOSDEM)
* and also allowed to experiment many new features, in project bin (like master clip effects) and monitor (split view effect tuning), [finalized weeks back][9] (for Akademy)

From this short history you can note that Free Software events mark important milestones in our project evolutions.  
Lonely development is unsustainable for a big projects; gathering developers, but also people with broader vision, every time renews motivation, gives new directions, focuses energy.

A cycle is ending, let's boost next one: [Randa Meetings 2015][10] are coming!  
We want to be there; we already offer much of our time, please don't let us pay for the travel, and support us through [KDE sprints fundraising][11]! That may also help wonderful people on wonderful projects, that would be your good deed for the day/week/... :-D

## Kdenlive 15.08.0 information page

Kdenlive 15.08.0 was released with KDE Applications 15.08.0 on 19th of August 2015.

This is a major release containing many changes to the core source, in an effort to cleanup the code.

### Major changes

* New file format. Kdenlive can open older project files, but older Kdenlive versions cannot open the new file format.
* Drop SDL support for monitor display, we now use OpenGL - should be supported on all fairly recent computers.
* Improved UI for color correction effects (Sop/Sat)
* It is now possible to add an effect to a Bin clip by dropping the effect on the clip. The effect will then be applied to all instances of this clip in timeline, mostly useful for color correction
* Effect Preview: Clip monitor now has an option to "Compare effect" displaying a customizable split screen to see the clip with and without effect

### Missing features

Due to the large refactoring, several features have been disabled, but we hope to re-introduce them as soon as possible. These are:

* Clip generators disabled (countdown, noise, online content search)
* Blackmagic Decklink playback / capture disabled
* Stopmotion Widget disabled
* Check if clips are on a removable device
* Rotoscoping effect broken

### Fixed issues
    
* [Bug 349907](https://bugs.kde.org/349907) "Add image" in title clip editor doesn't show any files
* [Bug 350250](https://bugs.kde.org/350250) (Solved) Title clip: Created Title-Text-Image not visible in Clip- nor Project-Monitor (black Field, no Text)
* [Bug 350275](https://bugs.kde.org/350250) About window in 15.04.3 still refers to the old bugtracker
* [Bug 348928](https://bugs.kde.org/350250) Effects dir wrong
* [Bug 348989](https://bugs.kde.org/350250) Volume effect resets to 0
* [Bug 350017](https://bugs.kde.org/350250) Curves handles are lost when clip is deselected

![Help bring touch interfaces to KDE, Support Randa 2015](Fundraiser-Banner-2015.png)


  [2]: https://download.kde.org/Attic/applications/15.08.0/src/kdenlive-15.08.0.tar.xz
  [3]: https://community.kde.org/Kdenlive/Development/KF5
  [4]: https://bugs.kde.org/describecomponents.cgi?product=kdenlive
  [5]: /users/granjow/we-re-randa
  [6]: https://web.archive.org/web/20160406103202/https://kdenlive.org/fundraising-campaign-2012
  [7]: /node/9182
  [8]: https://archive.fosdem.org/2014/schedule/event/movit/
  [9]: https://conf.kde.org/en/akademy2015/public/events/293
  [10]: http://www.randa-meetings.ch/
  [11]: https://www.kde.org/fundraisers/kdesprints2015
