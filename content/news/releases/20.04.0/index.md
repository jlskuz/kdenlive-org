---
title: Kdenlive 20.04 is out
author: Farid Abdelnour
date: 2020-04-24T06:00:56+00:00
aliases:
- /en/2020/04/kdenlive-20-04-is-out/
- /fr/2020/04/kdenlive-20-04-est-la/
- /it/2020/04/kdenlive-20-04-disponibile/
- /de/2020/04/kdenlive-20-04-ist-freigegeben/
---
![The new splash screen of Kdenlive version 20.04.0](splash-background.png)

Jean-Baptiste Mardelle and team are happy to announce the release of Kdenlive 20.04, this version marks the one year anniversary release of the code refactoring. The highlights include major speed improvements due to the Preview Scaling feature, New rating, tagging sorting and filtering of clips in the Project Bin for a great logging experience, Pitch shifting is now possible when using the speed effect, Multicam editing improvements and OpenTimelineIO support. Besides all the shiny new features, this version comes with fixes for 40 critical stability issues as well as a major revamp of the user experience. Kdenlive is now more reliable than ever before.

## Preview resolution

The new preview resolution speeds up the editing experience by scaling the video resolution of the monitors.

![](preview.gif)

## Project Bin

The logging experience just got better with the introduction of filters, you can now rate and color tag your clips.

### Clip rating

![](stars.gif)

### Color tagging

![](tags.gif)

### Filtering by clip type

![](filter-by-type.png)

### New sorting modes

![](sorting.png)

### Replace clips in the bin

![](replace-clip.png)

## Multicam Editing

New multicam editing interface allows you to select a track in the timeline by clicking on the project monitor.

![](multicam.gif)

Batch alignment of multiple clips to reference.

![](batch-align.gif)

## Pitch Shift

Pitch compensation feature when changing a clip's speed.
![](pitch-shift.png) 

## OpentimelineIO import/export

![](/images/OpenTimelineIO_Logo.svg) 

Added import and export support to Pixar's OTIO interchange format allowing interoperability with Final Cut 7 XML, Final Cut Pro X XML and Adobe Premiere to name a few ([Full support list][1]). **Kdenlive Windows:** coming soon.

## Motion Tracking

The Motion Tracking received a batch of bug fixes and new tracking algorithms (CSRT and MOSSE). **Kdenlive Windows:** see version 20.04.1.

![](motion-tracker.png)

## Zoom bar

New zoom bar for keyframes.

![](zoombar.gif)

## Interface and Usability

  * **Effect groups** are back!
  * **Rotoscoping:** Allow editing rotoscoping points before closing the shape, Shift + double click to add a new point, add/remove points on double click, double click center cross to resize, add horizontal/vertical only resize handles.
  * **Colored clips** according to type in timeline.
  * Direct drop clips to timeline.
  * Facelift to monitor, project bin, timeline and audio mixer interfaces.
  * **Snapping:** Disable snapping when pressing shift while dragging, press shift when using Spacer tool to disable snapping.
  * Add menu in track header to switch between single and separate channel audio thumbnails.
  * New **Splash Screen.**
  * **Render Profiles:** added new audio profiles **FLAC** and **ALAC**, new **alpha video** profiles VP8, VP9 and MOV and **GIF** image export profile. 
  * **Shortcuts**: New shift + a shortcut  to activate/deactivate target tracks, assign "g" shortcut to add/remove guide, added standard F2 shortcut in Project Bin for renaming,
  * Fixed ability to use fullscreen monitors.
  * Fixed **DVD wizard**.
  * Added audio backends options (DirectSound, WinMM and Wasapi) to the Windows version to prevent crackling in some cases.

## Legacy features

Some features were not backported after the refactoring are back.

### Audio Waveform Filter

The Audio Waveform Filter is back, just add it to the Master effects.

![](audio-analysis.gif)

### Effect Groups

Effect Groups are finally back, you can now create your effect combinations and use them across projects again.

![](effectgroup.gif)

## Features video

Watch the video for a detailed overview of some of the features:

<iframe title="Kdenlive 20.04" width="560" height="315" src="https://tube.kockatoo.org/videos/embed/5a667edc-afb2-4cac-b6d9-e287486e74b4" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

 [1]: https://opentimelineio.readthedocs.io/en/latest/tutorials/adapters.html
