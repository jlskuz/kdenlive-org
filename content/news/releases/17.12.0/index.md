---
author: Jean-Baptiste Mardelle
date: 2017-12-15T08:45:12+00:00
aliases:
- /en/2017/12/kdenlive-17-12-0-released/
---

![Screenshot of Kdenlive version 17.12.0](screenshot.png)

We are happy to announce the latest Kdenlive version, part of the [KDE Applications 17.12 release][1], making it the last major release using the current code base. This is a maintenance release focused on stability, while feature development is going in next year's 18.04 version. Proxy clips were given some attention and should give you better seeking experience as well as reduced memory usage for images. Other fixes include fixes in timeline preview, a crash when using a Library clip and smoother seeking on rewind playback.

## Packaging 
We have been pushing the AppImage packages lately because it allow us to put all required dependencies inside one file that can easily be downloaded and run on all linux distros. Today, we can also announce the immediate availability of the Kdenlive 17.12 AppImage, downloadable [here][4].

AppImage related fixes:

  * Fix audio distortion affecting the 17.08.3 AppImage
  * Include Breeze style

Vincent Pinon is also continuing the support for the Windows version, and you can get Kdenlive 17.12 for Windows [here][5].

## Next
We are also making available the first usable "preview" AppImage of the refactoring branch which will receive all development focus from now and will be released as 18.04. It is not ready for production but allows you to have a look at Kdenlive's future. You may follow the development progress [here][2].

Download the Kdenlive 18.04 alpha 2 AppImage [here][6].

## Meet us:
Next [Kdenlive Café][3] is tonight on #kdenlive at 21PM (CET), so feel free to join us for some feedback!

<div class="alert alert-info" role="alert">
Important
  <ul>
    <li>
      Packagers must take note that <em>libsamplerate</em> is now a dependency due to recent changes in FFMPEG.
    </li>
    <li>
      It is recommended for Ubuntu (and derivatives) users to use the AppImage version until further notice.
    </li>
  </ul>
</div>

 [1]: https://www.kde.org/announcements/announce-applications-17.12.0.php
 [2]: https://phabricator.kde.org/T5638
 [3]: https://kdenlive.org/category/cafe/
 [4]: https://files.kde.org/kdenlive/release/kdenlive-17.12-x86_64.AppImage.mirrorlist
 [5]: https://files.kde.org/kdenlive/release/Kdenlive-17.12.0-w64.7z.mirrorlist
 [6]: https://files.kde.org/kdenlive/unstable/kdenlive-18.04-alpha2.AppImage.mirrorlist
