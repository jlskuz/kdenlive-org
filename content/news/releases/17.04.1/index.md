---
author: Farid Abdelnour
date: 2017-05-12T02:38:11+00:00
aliases:
- /en/2017/05/kdenlive-17-04-1-released/
---
With the ongoing refactoring at full throttle a minor bug fix released with the ability to use VAAPI in transcoding and rendering by inserting a pre-parameter in you encoding profile (refer to commit and bug report for more info), a performance improvement and some Windows version fixes.
