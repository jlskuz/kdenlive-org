* Fix title clip duration cannot be changed. [Commit.](http://commits.kde.org/kdenlive/b9652701524dc594ed24699136fe97e9032691e9) Fixes bug [#379582](https://bugs.kde.org/379582)
* Allow passing pre-parameters using "-i" to specify where the input file name should go in ffmpeg parameters. [Commit.](http://commits.kde.org/kdenlive/456635ac2c5e15d3445d9889cdb0e42b5d984811) See bug [#378832](https://bugs.kde.org/378832)
* Fixed off-by-one errors in fft-based cross-correlation computations. [Commit.](http://commits.kde.org/kdenlive/3404da90de3ce10377cbebaaf5abc9229f1ce43f) 
* Don't show color theme configuration option unavailable on Windows. [Commit.](http://commits.kde.org/kdenlive/414d6579a16d2ce8d2ea7a2574fc33c0e90f837c) Fixes bug [#375723](https://bugs.kde.org/375723)
* Fix temp path on Windows. [Commit.](http://commits.kde.org/kdenlive/673d0f79ed2a083dafeb74a2aefd582b31d1f3a8) Fixes bug [#375717](https://bugs.kde.org/375717)
* Fix MLT doc link. [Commit.](http://commits.kde.org/kdenlive/7cf9980c1fe905f1d3657e5faf00df1a2795b807) Fixes bug [#375316](https://bugs.kde.org/375316)
* Fix script export & QScript header left. [Commit.](http://commits.kde.org/kdenlive/984df727b9b08db3353f71f8c5f30d6b8ec56a9b) 

