---
title:  Kdenlive 16.04.0
date: 2016-04-20
aliases:
- /discover/16.04.0
- /node/9452
# Ported from https://web.archive.org/web/20160617082328/https://kdenlive.org/node/9452
# Ported from https://web.archive.org/web/20160602023518/https://kdenlive.org/discover/16.04.0
---

Kdenlive 16.04 is a major release, bringing you the result of several months of work with the feedback of several professional video editors. Many usability and stability issues were addressed as well as dozens of new features.

Here is a quick list of some of the features that were added:

## Monitor

* Configurable monitor overlay: you can now display timecode and safe zones in monitors
* Audio waveform overlay: Clip monitor now displays a preview of the audio, making it easier to seek to the wanted place
* Markers can now be edited directly on the monitor display
* Effects can be dropped directly on the monitor
* Live display of playback fps
* New audio level meter in monitor toolbar

## Timeline

* Faster audio thumbnails: rewritten, they are now created 7x faster, drawing 5x faster
* Editing: added "Match Frame" feature
* Transitions: implement transition parameters - Cairo Blend transitions now supports opacity and alpha operation and is keyframable

## Effects

* "Split view " to display a split view of timeline's current clip with and without effect to compare
* Speed effect now uses MLT's new TimeWarp producer, so sound is now working with speed effect
* Support for curves in some effect parameters (volume, etc)

## Project Bin

* Metadata: clip metadata is back, giving you details about your audio / picture files
* Improved automatic profile: if enabled, Kdenlive automatically proposes to switch profile to match that of the first added clip
* Generators: you can easily create noise, color bar and counter clips

## Library

* New Library widget allowing to save parts of the current timeline to share them between several projects

## Titler

* Fix longstanding (since the 12.x series) crash affecting titles with transitions
* Allow adjusting letter and line spacing
* Text shadow
* Gradient fill for text and rectangles

## DVD Wizard

* Alignment grid for menus

## Export

* Rendering widget rewritten to be less frightening
* New parameter to decide if you prefer faster encoding or smaller file size

## General

* New "Favorite Effects" widget, allowing quick access to your preferred effects
* Many small UI and workflow improvments and bug fixes
