  * New camera proxy for Akaso. [Commit.][1]
  * Fix replacing image clip discards its duration. [Commit.][2] Fixes bug [#463382][3]
  * Subtitles: when using a background with alpha, draw one rect around all lines to avoid overlay issues. [Commit.][4]
  * Fix multiple issues with copy/paste/move rotoscoping keyframes. [Commit.][5]
  * Don&#8217;t build designer plugins by default &#8211; only useful for developers. [Commit.][6]
  * Fix color balance filter not disabled when picking a new color. [Commit.][7]
  * Fix possible freeze on aborting edit-friendly transcoding request. [Commit.][8]
  * Fix remove space on tracks with a mix. [Commit.][9]
  * Fix editing multiple markers sets all comments to comment of first. [Commit.][10]
  * Fix designer plugin crash. [Commit.][11]
  * Fix guides move in spacer/trimming operations and tests. [Commit.][12]
  * Small improvement to audio on pause/play. [Commit.][13]
  * Fix typo. [Commit.][14]
  * Don&#8217;t interrupt timeline playback when refreshing the clip monitor or changing preview resolution. [Commit.][15]
  * Don&#8217;t show timeline preview crash message if job was stopped by a timeline operation. [Commit.][16]
  * Fix close button in status messages not working. [Commit.][17]
  * Preview chunks should be sorted by integer. [Commit.][18]
  * Fix timeline preview incorrectly stopping when moving a clip outside preview zone. [Commit.][19]
  * QMetaObject::invokeMethod should be used with Qt::DirectConnection when expecting a return argument. [Commit.][20]
  * Disable parallel processing on 32bit systems. [Commit.][21]
  * Fix pressing Esc during timeline drag corrupts timeline. [Commit.][22]
  * Fix guides incorrectly moved when unlocked. [Commit.][23]
  * Update mouse position in timeline toolbar on zoom and scroll timeline. [Commit.][24]
  * Fix crash dropping an effect with a scene (rotoscoping, transform,&#8230;) on the project monitor. [Commit.][25]
  * Fix zoom sometimes behaving incorrectly on very low zoom levels. [Commit.][26]
  * Fix zoom on mouse not working as expected when zooming after last clip. [Commit.][27]
  * Restrict guides to integer position on paint to avoid drawing artifacts. [Commit.][28]
  * Fix resize zone conflicting with move on low zoom levels. [Commit.][29]
  * Fix title clip line break sometimes pushing text outside item rect. [Commit.][30]
  * Fix rendering when using an MLT properties file with a space in it. [Commit.][31] Fixes bug [#462650][32]
  * Fix monitor overlay sometimes incorrectly placed. [Commit.][33]
  * Ensure on monitor marker color is updated even if 2 markers have the same text. [Commit.][34]
  * Cleanup monitor zone resize. [Commit.][35]

 [1]: http://commits.kde.org/kdenlive/95a5eb04060b1716fd82c33854d9df58ef7eeafe
 [2]: http://commits.kde.org/kdenlive/a58689dbb4b58906525ae5d06451db714f08d1f5
 [3]: https://bugs.kde.org/463382
 [4]: http://commits.kde.org/kdenlive/3f38004e7d88eb4bc0b9dba8ed3998bf2b3d730a
 [5]: http://commits.kde.org/kdenlive/4cc1af75d77d12b21b9bdb43baba8ddb0e9fdfdd
 [6]: http://commits.kde.org/kdenlive/978ebcf51c0a779f3711d7c6b732b866211d2b6c
 [7]: http://commits.kde.org/kdenlive/8fd8ec751387765c862fab36adeb600c05b896f4
 [8]: http://commits.kde.org/kdenlive/d0b871f0fe16f2f66fb5c0a9d2fd30ed1c92de9b
 [9]: http://commits.kde.org/kdenlive/08fc0897823f95a7597c1afa91febbff2da17cce
 [10]: http://commits.kde.org/kdenlive/8b77b7da5284b29f30c4646ec49d650e19b8def4
 [11]: http://commits.kde.org/kdenlive/78c216161f01fae3ebda62bcb27446ba386d1bbe
 [12]: http://commits.kde.org/kdenlive/75d1bb8062901c4ad25523c227325674636b5e59
 [13]: http://commits.kde.org/kdenlive/cfa13f830c787946ed9c936eb22615ebf5d1a94b
 [14]: http://commits.kde.org/kdenlive/73e63f60a4b0cd52c11b2363c04d95ee8124467c
 [15]: http://commits.kde.org/kdenlive/256e4d6fbe76d20a1749287ab3846a11f2f4fea6
 [16]: http://commits.kde.org/kdenlive/f6b699194bd9c627b4b328f3563a6afd52e3c792
 [17]: http://commits.kde.org/kdenlive/ca2393aae5b5e99c77d71015e6ff8d67c700b62b
 [18]: http://commits.kde.org/kdenlive/66db84ba5e97a71236bc1cb2747206e14a3ce0a6
 [19]: http://commits.kde.org/kdenlive/789239336823c39846f4f7f5453d5f0d6a02806f
 [20]: http://commits.kde.org/kdenlive/468aa1213c3db3724a20b32817966d0b969664e2
 [21]: http://commits.kde.org/kdenlive/e4291ba5f0b21d606706eab17c1692f0708ffbbe
 [22]: http://commits.kde.org/kdenlive/ad81f39d99d7d8ed1bd27a80e8693eb69f6e53aa
 [23]: http://commits.kde.org/kdenlive/125d1b3e4950878d60010b7e9b4902e7a91509b6
 [24]: http://commits.kde.org/kdenlive/51ea8c037ab7d99cc45b128c263d3614a72e1d42
 [25]: http://commits.kde.org/kdenlive/8baeda138be2622ef6edba6fe743580367af0943
 [26]: http://commits.kde.org/kdenlive/b74dc5e9b2fa3fb91980b264d2a230d8232e19c2
 [27]: http://commits.kde.org/kdenlive/549da07cd701e4a32df90f1e16546e66bc22cd59
 [28]: http://commits.kde.org/kdenlive/4b36bc293c903a44468a94dea6951a2b63da6782
 [29]: http://commits.kde.org/kdenlive/e35b9017826551057fe7041e36e3652e1dd17447
 [30]: http://commits.kde.org/kdenlive/a6be02a90e8764431d5aedfa28547e6d464d4ae7
 [31]: http://commits.kde.org/kdenlive/05fa0568d1511ab65dae958a20c342204937baca
 [32]: https://bugs.kde.org/462650
 [33]: http://commits.kde.org/kdenlive/1cceea6f274ea007dfb21cc8cd23293a6543595c
 [34]: http://commits.kde.org/kdenlive/431b643275afc50a845ce0206b907c56479f32b2
 [35]: http://commits.kde.org/kdenlive/cf9b53629815f99ecfca2020ff20703a91dc7cf7
