---
date: 2023-01-09T12:55:04+00:00
aliases:
- /en/2023/01/kdenlive-22-12-1-released/
- /it/2023/01/kdenlive-22-12-1-2/
- /de/2023/01/kdenlive-22-12-1/
---
The first maintenance release of the 22.12 series is out with support for Akaso proxy files and a small improvement to audio pause/play. Some highlights include fixes to timeline preview rendering, copy/paste/move of keyframes in the rotoscoping effect, moving of clips on low zoom levels and avoid overlay issues with subtitles backgrounds with alpha.


