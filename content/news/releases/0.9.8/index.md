---
title: 'Kdenlive 0.9.8, the stable track'
author: Vincent Pinon
date: 2014-05-14T15:20:00+00:00
aliases:
- /node/9179
- /discover/0.9.8
# Ported from https://web.archive.org/web/20160316174831/https://kdenlive.org/node/9179
# Ported from https://web.archive.org/web/20140519102005/http://kdenlive.org/discover/0.9.8
---


Dear kdenlive fans,

If you edit movies "for real", you probably don't use the development snapshots (do you?) and stick to the latest release, dating back from one year. Yet, our bugtracker didn't remain empty even with v0.9.6, so you may have been waiting for an upgrade... here it comes!

v0.9.8 doesn't include big moves like architecture refactoring or GLSL (be patient!), it is mainly fixing bugs and bringing minor changes. Check below for more details.  
As previously, you can [get the source code from KDE servers][2]. Hope that your distro won't be the last to package it ;-) MLT upgrade to 0.9 is not mandatory, but keeping up to date is always good to avoid falling on old bugs.

Of course, we thank everyone who contributed or helped us in other ways during this one-year period.

Don't hesitate to relay this info, and to come help us (code, manual, translation, etc)!

## Changes

* Fades: timeline shortcut now applies to video on clips containing video and audio
* Copy proxies with 'move project'
* Remove the MLT processing threads option that never really worked
* Clean encoding profiles using MLT presets and profiles
* Support a custom suffix for FFmpeg binaries (mostly for packagers)
* Make audio align work asynchronously
* Add support for JogShuttle on newer systems, make shuttle device selection more straightforward, add ProV2 keys

## Bugs fixed

* Fix warnings from gcc, cppcheck, clang, scan-build: fixes many crashs & leaks
* Code cleanup (use const ref, fix includes, mem leaks, optimize...)
* Fix keyframing messed up for some effects
* Fix timeline corruption when trying to move clip before 0
* Fix crash when closing title widget
* Fix thumbnails for image clips in timeline
* Fix crash on quick undo/redo (#3240)
* Fix multithreading (#3186)
* Fix some problems with transitions keyframes
* Fix scopes (#3052)

  [2]: https://download.kde.org/stable/kdenlive/0.9.8/src/kdenlive-0.9.8.tar.bz2.mirrorlist
