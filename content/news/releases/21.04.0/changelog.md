  * Fix various spacer crashes. [Commit.][9]
  * Fix crash on remove space. [Commit.][10]
  * Fix crash on fade deletion from timeline. [Commit.][11]
  * Fix undo fade deletion when removed by dragging outside clip in timeline. [Commit.][12]
  * Add 21.04 splash-screen. [Commit.][13]
  * Fix nvidia’s proxy profile. [Commit.][14]
  * Workaround crash playing remote files in MLT 6.26.0. [Commit.][15]
  * Fix various focus issues (related to #859). [Commit.][16]
  * Fix in point calculation on speed revert. [Commit.][17] Fixes bug [#424582][18]
  * Alpha shapes: allow going outside screen. [Commit.][19]
  * Try to fix value change on hover issue (maybe Qt regression). [Commit.][20] See bug [#435531][21]
  * Monitor effect toolbar: center to prevent covering top/bottom handles. [Commit.][22]
  * When double clicking a title clip, seek to click pos before editing title to show correct background. [Commit.][23]
  * Fix effect parameter sliders with some ranges like bezier handles. [Commit.][24]
  * Fix possible crash on quit. [Commit.][25]
  * Fix bezier curves messy layout. [Commit.][26]
  * Bezier curve widget: adjust height to monitor resolution. [Commit.][27]
  * Allow drag & drop of vosk dictionaries urls. [Commit.][28]
  * Fix timeline preview parameters, add comments to make things cleaner. [Commit.][29]
  * FIx possible startup crash. [Commit.][30]
  * Fix bug in subtitle deletion undo. [Commit.][31]
  * Ensure subtitle track is displayed when dropping a subtitle file in timeline. [Commit.][32]
  * Backport nvenc codec name fix. [Commit.][33]
  * Fix proxy clips vaapi profiles. [Commit.][34]
  * Fix keyframe limit on import from clipboard. [Commit.][35] Fixes bug [#433618][36]
  * Show keyframe value in tooltip when editing in timeline. [Commit.][37]
  * Fix clip monitor refresh on title clip change. [Commit.][38]
  * Ensure subtitle track is always shown when adding a subtitle. [Commit.][39]
  * When selecting a bin clip from timeline, take care of speed in zone selection. [Commit.][40] Fixes bug [#425417][41]
  * Fix rotation behaving in unexpected way (automatically repeating). [Commit.][42] Fixes bug [#425682][43]
  * Fix spacer/insert/remove space with grouped clips on both sides of the move. [Commit.][44] Fixes bug [#390668][45]
  * Automatically update subtitle text when changing focus. [Commit.][46]
  * *Fix recent regression in timeline selection. [Commit.][47]
  * Ensure subtitle track is shown whenever a subtitle item is added. [Commit.][48]
  * Fix crash on project close. [Commit.][49]
  * Fix inconsistent opengl mode on Windows,. [Commit.][50]
  * Fix broken cache deletion. [Commit.][51] Fixes bug [#434429][52]
  * Minor fix for visual state for missing files. [Commit.][53]
  * Fix replacement of missing images in title clips. [Commit.][54] Fixes bug [#411324][55]
  * Backup subtitles too. [Commit.][56]
  * Archive import: Fix dialog, allow *.zip. [Commit.][57] See bug [#386398][58]
  * Appimage dependencies: Freeze vidstab version. [Commit.][59] Fixes bug [#435147][60]
  * Archive Project: fix subtile files are not added to archive. [Commit.][61] Fixes bug [#434401][62]
  * Archive Project: fix some lumas are not found. [Commit.][63]
  * Automatic Scene Split: apply zones correctly. [Commit.][64] Fixes bug [#435263][65]
  * Fix change speed for slideshow clips. [Commit.][66] See bug [#429795][67]. See bug [#428263][68]. See bug [#392670][69]
  * Fix archiving for generator mlt clips. [Commit.][70] Fixes bug [#420623][71]
  * Fix timeline keyframe position for opacity for affine and other non qtblend effects. [Commit.][72]
  * Fix composition keyframe view broken. [Commit.][73]
  * Fix crash on document close. [Commit.][74]
  * Unbreak audio spectrum. [Commit.][75]
  * Properly release timelinemodel on document close. [Commit.][76] See bug [#409667][77]
  * Fix crash when closing project while an effect monitor overlay was active. [Commit.][78]
  * Ensure new transcoding profiles appear even if user already has some custom ones. [Commit.][79]
  * Fix unconfigured consumer causing various crashes. [Commit.][80] See bug [#409667][77]
  * Revert commit 93dbb1f0995163d96a63c5e7a2a0c812542a681b. [Commit.][81]
  * Titler: update background alpha slider properly. [Commit.][82] Fixes bug [#433889][83]
  * Don’t call reconfigure from render thread. [Commit.][84] See bug [#428632][85]
  * Black font for project monitor markers. [Commit.][86]
  * Fix compile warnings. [Commit.][87]
  * Fix setting frame background color. [Commit.][88]
  * Start playback from beginning if on timeline ende at action start. [Commit.][89] Fixes bug [#353051][90]
  * Fix wipes for slideshow clips. [Commit.][91] Fixes bug [#434360][92]
  * Add/Edit marker/guide dialog: always show category color in combobox, always use black font for guides/markers. [Commit.][93]
  * Remove unused files. [Commit.][94]
  * Fix warnings. [Commit.][95]
  * Add some padding to guide labels, use black font. [Commit.][96]
  * Ensure guides are also drawn over subtitle track. [Commit.][97]
  * Fix incorrect cast. [Commit.][98]
  * Remove some debug output. [Commit.][99]
  * Move speed indicator in qml overlay. [Commit.][100]
  * Fix project duration label displaying one frame too much. [Commit.][101] Fixes bug [#425639][102]
  * Fix keyframes with master effects having a zone. [Commit.][103]
  * Fix Qt 5.15 / KF 5.78 warnings. [Commit.][104]
  * Fix clang-tidy / clazy warnings. [Commit.][105]
  * Clazy fixit (default checks, from qtcreator). [Commit.][106]
  * Fix timeline operation broken after aborted right resize operation. [Commit.][107]
  * Move clip markers below clip name to avoid overlap, fix markers position when track is collapsed. [Commit.][108]
  * Fix regression crash editing marker comment. [Commit.][109]
  * Ruler zones and guides now also snap to timeline cursor, drag+ shift disables snapping. [Commit.][110]
  * Improve timeline zone snapping when moving with mouse. [Commit.][111]
  * Rendering fixes: fix black frame at end of rendered project, enforce out point to avoid rendering trailing black frames, stop rendering before end guide. [Commit.][112] Fixes bug [#425960][113]
  * Don’t show zone duration while dragging it. [Commit.][114]
  * Now that we have effect zone, allow multiple unique effects (like fades) on tracks / master. [Commit.][115]
  * Make master effect zones use the same height as timeline zone for a cleaner look. [Commit.][116]
  * Fix master effect zones hidden. [Commit.][117]
  * Fix titler zoom incorrectly passed as integer. [Commit.][118]
  * Fix master effect zones hidden in recent change, ensure moving a zone with mouse always sticks to frames. [Commit.][119]
  * Fix timeline crashes. Qml === operator doesn’t work on clip producer type. [Commit.][120]
  * Comment out useless debug stuff. [Commit.][121]
  * Fix broken compositions (incorrect qml comparison change in recent commit). [Commit.][122]
  * Clip monitor: use marker category’s color as background. [Commit.][123]
  * Monitor overlay: use guide category as background color, ensure guide at timeline position appears above others. [Commit.][124]
  * Fix startup crash on empty config file. [Commit.][125]
  * Fix tests after last commit. [Commit.][126]
  * Workaround app translation mess, small fixes for locale matching. [Commit.][127] See bug [#434179][128]
  * Ensure timeline zoombar left handle is always visible. [Commit.][129]
  * Fix mono clip audio thumbnails broken. [Commit.][130]
  * Refactor guide model to fix several bugs (moving a guide over another on replaced it). [Commit.][131]
  * Fix some more warnings. [Commit.][132]
  * Fix some more warnings. [Commit.][133]
  * Fix some more warnings. [Commit.][134]
  * Fix some more warnings. [Commit.][135]
  * Fix some more warnings. [Commit.][136]
  * Fix some more warnings. [Commit.][137]
  * Fix QML comparisons warnings. [Commit.][138]
  * Fix various timeline ruler repaint issues. [Commit.][139]
  * Add pulse capture profile (needed for flatpak). [Commit.][140]
  * Timeline ruler: small UI improvements (add left/right handles to zones, move preview markers to bottom). [Commit.][141]
  * Cleanup timeline guides: increase number of guides categories(9), move guide label above timeline ruler. [Commit.][142]
  * Appimage: openssl for ffmpeg (#918), fix rubberband build. [Commit.][143]
  * Fix effect zone for bin clips. [Commit.][144]
  * Refresh monitor refresh latency on effect change. [Commit.][145]
  * Fix startup crash with preview scaling enabled. [Commit.][146]
  * Fix bunch of cast warnings (manual). [Commit.][147]
  * Auto fix warnings. [Commit.][148]
  * Cleanup for shortcut list. [Commit.][149]
  * Monitor: add possiblity to use shortcut for show/hide edit mode. [Commit.][150] Fixes bug [#434405][151]
  * Fix various monitor refresh issues. [Commit.][152]
  * Update requirements listed in speech to text config message. [Commit.][153]
  * Don’t show opacity when not relevant in the list of params in keyframe paste value. [Commit.][154]
  * Clang-tidy -fix: modernize-use-equals-default. [Commit.][155]
  * Clang-tidy -fix: modernize-use-emplace. [Commit.][156]
  * Clang-tidy -fix: modernize-use-default-member-init. [Commit.][157]
  * Clang-tidy -fix: modernize-use-bool-literals. [Commit.][158]
  * Clang-tidy -fix: modernize-use-auto. [Commit.][159]
  * Clang-tidy -fix: modernize-raw-string-literal. [Commit.][160]
  * Clang-tidy -fix: modernize-pass-by-value. [Commit.][161]
  * Fix warnings. [Commit.][162]
  * Clang-tidy -fix: modernize-make-unique. [Commit.][163]
  * Clang-tidy -fix: modernize-loop-convert. [Commit.][164]
  * Clang-tidy -fix: modernize-deprecated-headers. [Commit.][165]
  * Clang-tidy -fix: modernize-use-nullptr. [Commit.][166]
  * Clang-tidy -fix: modernize-use-override. [Commit.][167]
  * Improve ui for copy keyframe parameter. [Commit.][168]
  * Restore softness param in composite transition. [Commit.][169]
  * Zoom bar: always show handles. [Commit.][170]
  * Timeline zoombar, related to #651 !184. [Commit.][171]
  * Do not show timecode in shortcut list. [Commit.][172] Fixes bug [#433679][173]
  * Validate timecode in settings. [Commit.][174] Fixes bug [#432580][175]
  * Titler: use TimecodeDisplay for duration input. [Commit.][176]
  * Cleanup and improvements for titlewidget code. [Commit.][177]
  * Titler: Add ellipse item. [Commit.][178]
  * Fix timeline operation cannot be performed after group resize with no move. [Commit.][179]
  * Ensure ruler ticks don’t get over zones. [Commit.][180]
  * Include pango library in Appimage. [Commit.][181]
  * Improve audio thumbnail offset on clip cut or longer clips. [Commit.][182] See bug [#423337][183]
  * Include fribidi to ensure we have a working libass and avformat module in Appimage. [Commit.][184]
  * Fix active effect mess, resulting in incorrect monitor connection and crash. [Commit.][185] See bug [#424809][186]
  * README: update instruction for nightly flatpak. [Commit.][187]
  * Update nightly flatpak based on flathub script. [Commit.][188]
  * README: add build status for nightly builds, add flatpak nightly instruction. [Commit.][189]
  * Remove unnecessary debug message introduced with bf8dac93. [Commit.][190]
  * Rotoscoping: add center-based resize (by shift); improve key bind info. [Commit.][191]
  * Fix scaling of rotoscope mask. [Commit.][192]
  * Various fixes for effect zones, disable for clips. [Commit.][193]
  * QtWebEngine no more needed. [Commit.][194]
  * Allow building with Qt5.11 (debian buster). [Commit.][195]
  * Project Bin: do not allow zoom 0 for icon view (nothing is visible). [Commit.][196]
  * Further fixes on icon install. [Commit.][197]
  * Comment breeze-dark icon install leading to a file conflict. [Commit.][198]
  * Enable speech to text on playlist files. [Commit.][199]
  * Correctly update effect stack on effect zone undo. [Commit.][200]
  * Fix undo set effect zone. [Commit.][201]
  * Always show master effect zones. [Commit.][202]
  * Samll improvement in timeline ui scaling on low res monitors. [Commit.][203]
  * Hide frame on Master button. [Commit.][204]
  * Titler: Fix crash on load title during animation edit. [Commit.][205] Fixes bug [#433010][206]
  * Some fixed for timeline ruler (fix timecode sometimes stopping in the middle of timeline). [Commit.][207]
  * Fixes wrong version checking for typewriter and allows to work with development version of mlt-6.25. [Commit.][208]
  * Add missing icon file. [Commit.][209]
  * Comment out missing icon. [Commit.][210]
  * Allow setting effect zone for master / track effects, initial implementation. [Commit.][211]
  * Change subtitle icons. [Commit.][212]
  * Improve and fix “add-subtitle” icon. [Commit.][213]
  * Add icons for keyframe actions. [Commit.][214]
  * Update credits. [Commit.][215]
  * Use two digits for hours in SRT timecodes. [Commit.][216] Fixes bug [#433193][217]
  * Fix incorrect handling of mix transition when moving one of the clips to another track. [Commit.][218] See bug [#433527][219]
  * Refresh monitor after title edit from timeline. [Commit.][220]
  * Allow to edit title clip with double click on timeline. [Commit.][221]
  * Speech to text: always select full sequence when clicking on a no speech section. [Commit.][222]
  * Fix windows text edit speech recognition. [Commit.][223]
  * On windows, the official Python3 package installs the executable as “python”, not “python3”. [Commit.][224]
  * Fix upgrading python speech to text modules. [Commit.][225]
  * Fix focus issue after editing timeline item duration on double click. [Commit.][226]
  * Fix focus on item under mouse after switching back from another app. [Commit.][227]
  * Show error if speech modules were removed since last run. [Commit.][228]
  * Fix disambiguation found by gettext 0.21. [Commit.][229]
  * Display speech to text python modules version, improve config feedback. [Commit.][230]
  * Improve speech to text config. [Commit.][231]
  * Fix disambiguations found by gettext 0.21. [Commit.][232]
  * Fix recursive search broken on cancel. [Commit.][233] Fixes bug [#433773][234]
  * Grapped clips: jump to next free space within a track (left/right). [Commit.][235]
  * Change shortcut for Loop Zone to avoid conflict with windows system. [Commit.][236]
  * Fix various selection issues. [Commit.][237]
  * Give KeyframeView focus back to make shortcuts working. [Commit.][238]
  * Fix my typo: i18n->i18nc. [Commit.][239]
  * Fix disambiguations found by gettext 0.21. [Commit.][240]
  * Grapped clips: jump to next free track if neighbour is occupied. [Commit.][241]
  * Comment out recent lost timeline focus that made things worse. [Commit.][242]
  * Improve focus handling when switching to fullscreen monitor. [Commit.][243]
  * Fix disambiguations found by gettext 0.21. [Commit.][244]
  * Do not allow keyframe edit if keyframes are hidden. [Commit.][245]
  * Open duration window on double click on timeline item. [Commit.][246] Fixes bug [#407574][247]
  * Builtin typewriter for kdenlive titler. [Commit.][248]
  * Ensure we use an UTF-8 encoding for markers. [Commit.][249] See bug [#433615][250]
  * Don’t mark document modified when opening a project file with guides. [Commit.][251] See bug [#433615][250]
  * Fix animated param view when keyframes are hidden. [Commit.][252]
  * Make timeline tracks separator slightly more visible. [Commit.][253]
  * Comment out attempt to fix windows python’s env vars. [Commit.][254]
  * When focusing the app, ensure we have the correct timeline item under mouse referenced. [Commit.][255]
  * Render Dialog: remember state of “more options”. [Commit.][256] Fixes bug [#433600][257]
  * Attempt to fix python scripts exec on Windows. [Commit.][258]
  * Keyframes: Shift+drag now allows selecting last keyframe, fix corruption / crash on group keyframe move. [Commit.][259]
  * Transcode job: don’t silently overwrite exported files. [Commit.][260] Fixes bug [#433623][261]
  * Don’t enforce profile width multiple of 8. [Commit.][262]
  * Text edit: add bookmarks, save analysed speech in bin clip, various fixes. [Commit.][263]
  * Appimage: don’t fail on missing bigsh0t. [Commit.][264]
  * Titler: update tab order. [Commit.][265] Fixes bug [#433590][266]
  * Do not allow zero for values of a project profile (framrate, framesize,…). [Commit.][267] Fixes bug [#432016][268]
  * Profile width in MLT can be a multiple of 2, not 8. [Commit.][269]
  * Text based edit: save button now adds the edited clip to bin playlist, subsequent changes automatically update the playlist. [Commit.][270]
  * Update text based edit, allow preview. [Commit.][271]
  * Fix appimage creation on missing libva driver. [Commit.][272]
  * AnimatedRect: add “adjustcenter” default (Pillar Echo effect). [Commit.][273]
  * Adjust appimage final script with recent changes. [Commit.][274]
  * Don’t rebuild existing audio thumbs for playlist on project opening, display playlist audio thumbs in clip monitor. [Commit.][275]
  * Master disappeared… [Commit.][276]
  * Add missing patch files. [Commit.][277]
  * Appimage dependency: path frameworks to build without phonon, get rid of libcanberra. [Commit.][278]
  * Fix wrong duration for non-animated GIFs. [Commit.][279]
  * Fixuifiles. [Commit.][280]
  * Remove “Create Region” menu item (not re-implemented yet) #82. [Commit.][281]
  * Treat GIFs as video, not as image. [Commit.][282] Fixes bug [#410908][283]. See bug [#411180][284]
  * Build scripts: remove unused kdoctools dependency. [Commit.][285]
  * Another round of appimage dependency fixes. [Commit.][286]
  * Add more missing dependencies for Kdenlive Appimage dependency build. [Commit.][287]
  * Add missing KNewStuff file. [Commit.][288]
  * Add KNewStuff for “Apply LUT” effect. [Commit.][289]
  * Use urllist for lut effect. [Commit.][290]
  * Fix color picker corruption. [Commit.][291]
  * Titler: show correct outline color on first use. [Commit.][292]
  * Titler: minor ui improvements. [Commit.][293]
  * Effect and transition list: make it possible to search by id. [Commit.][294]
  * Effects: “save” mode for url (frei0r.bigsh0t\_stabilize\_360) #350. [Commit.][295]
  * Add appimage missing libva. [Commit.][296]
  * Add appimage missing fribidi dependency. [Commit.][297]
  * Use urllist for wipe, region, luma, dissolve and composite. [Commit.][298] Implements feature [#356034][299]
  * Add new assetparam ui type “urllist”. [Commit.][300]
  * Fix subtitle selection by keyboard shortcut. [Commit.][301]
  * Subtitles: fix crash on “select clip” shortcut. [Commit.][302]
  * Hide keyframe mode for rotoscoping (only linear supported). [Commit.][303]
  * Online Resources: fix crash and polishing #918. [Commit.][304]
  * Add missing appimage dependency file. [Commit.][305]
  * Online widget: minor ui cleanup. [Commit.][306]
  * Some fixes for text analysis on clip zones. [Commit.][307]
  * Fix reset config on windows #931. [Commit.][308]
  * Fix text editing selection/deletion. [Commit.][309]
  * Update Appimage dependencies. [Commit.][310]
  * Expanded track tag width only if >10 audio OR video tracks, not sum of. [Commit.][311]
  * Audiomixer: show track name. [Commit.][312]
  * Fix downloaded template titles and lumas not found on Windows. [Commit.][313]
  * Keep title text item editable if even if it is empty. [Commit.][314]
  * Apply !180 fixing #165. [Commit.][315]
  * Fix crash if no provider configs are found. [Commit.][316]
  * Fix invisible text cursor in title editor #165 and other minor tweaks. [Commit.][317] Fixes bug [#403941][318]. Fixes bug [#407497][319]
  * Apply !178 (drop effects on master button). [Commit.][320]
  * Apply !159. [Commit.][321]
  * Appimage: qt now needs networkauth. [Commit.][322]
  * Apply !171 (typewriter effect in latest MLT, only for title clips…). [Commit.][323]
  * Duplicated file with name clash on windows. [Commit.][324]
  * Apply !176. [Commit.][325]
  * Apply !177: dependency change! Qt NetworkAuth instead of Qt WebEngine. [Commit.][326]
  * Lighter export profiles names, allow tuning alpha & GPU profiles. [Commit.][327]
  * Apply !153. [Commit.][328]
  * Text edit: Fix search, remove deleted words from sequence on insert to timeline. [Commit.][329]
  * Text editing: switch to custom text editor, allow inserting selection to timeline. [Commit.][330]
  * Remove online resources from project menu. [Commit.][331]
  * Remove QtWebEngine from build. [Commit.][332]
  * Fix freesound config. [Commit.][333]
  * Add providers. [Commit.][334]
  * Cleanup and add documentation. [Commit.][335]
  * Remove qt-oauth-lib. [Commit.][336]
  * Fixed Colorize typo. [Commit.][337]
  * Speech to text: attempt to fix Windows UTF-8 encoding, fix crash when no clip selected and incorrect subtitle tooltip. [Commit.][338]
  * Disambiguated the string “Slide” for Slide Transition Name. [Commit.][339]
  * Disambiguated the string “Wipe” for Wipe Transition Name. [Commit.][340]
  * Disambiguated the string “Luma” for Luma Transition Name. [Commit.][341]
  * Disambiguated the string “Dissolve” for Dissolve Transition Name. [Commit.][342]
  * Disambiguated the string “Composite” for Composite Transition Name. [Commit.][343]
  * Fix context name from Effect to Transition. [Commit.][344]
  * Disambiguated the string “Stabilize” for Stabilize Effect Name. [Commit.][345]
  * Disambiguated the string “Regionalize” for Regionalize Transition Name. [Commit.][346]
  * Disambiguated the string “Regionalize” for Regionalize Effect Name. [Commit.][347]
  * Disambiguated the strings “Vinyl” for Vinyl Effect Name and “Year” for Vinyl Effect Year. [Commit.][348]
  * Disambiguated the string “Reverb” for Reverb Effect Name. [Commit.][349]
  * Disambiguated the string “Vectorscope” for Vectorscope Effect Name. [Commit.][350]
  * Disambiguated the string “Primaries” for Primaries Effect Name. [Commit.][351]
  * Disambiguated the string “Glow” for Glow Effect Name. [Commit.][352]
  * Disambiguated the string “Wave” for Wave Effect Name. [Commit.][353]
  * Disambiguated the string “Tint” for Tint Effect Name. [Commit.][354]
  * Disambiguated the string “Sepia” for Sepia Effect Name. [Commit.][355]
  * Disambiguated the string “Luminance” for Luminance Effect Name. [Commit.][356]
  * Disambiguated the string “Limiter” for Limiter Effect Name. [Commit.][357]
  * Disambiguated the string “Greyscale” for Greyscale Effect Name. [Commit.][358]
  * Disambiguated the string “Curves” for Curves Effect Name. [Commit.][359]
  * Disambiguated the string “Brightness” for Brightness Effect Name. [Commit.][360]
  * Disambiguated the string “Obscure” for Obscure Effect Name. [Commit.][361]
  * Disambiguated the string “Freeze” for Freeze Effect Name. [Commit.][362]
  * Disambiguated the string “Transform” for Affine Transform Effect Name. [Commit.][363]
  * Disambiguated the string “Transform” for Qtblend Transform Effect Name. [Commit.][364]
  * Disambiguated the string “Mirror” for Mirror Effect Name. [Commit.][365]
  * Disambiguated the string “Distort” for Distort Effect Name. [Commit.][366]
  * Disambiguated the string “Defish” for Defish Effect Name. [Commit.][367]
  * Disambiguated the string “Corners” for Corners Effect Name. [Commit.][368]
  * Disambiguated the string “Soft Glow” for Soft Glow Effect Name. [Commit.][369]
  * Disambiguated the string “Emboss” for Emboss Effect Name. [Commit.][370]
  * Disambiguated the string “Fade out” for Audio Fade Out Effect Name. [Commit.][371]
  * Disambiguated the string “Fade in” for Audio Fade In Effect Name. [Commit.][372]
  * Disambiguated the string “Fade out” for Image Fade Out Effect Name. [Commit.][373]
  * Disambiguated the string “Fade in” for Image Fade In Effect Name. [Commit.][374]
  * Disambiguated the string “Sharpen” for Sharpen Effect Name. [Commit.][375]
  * Disambiguated the string “Grain” for Grain Effect Name. [Commit.][376]
  * Disambiguated the string “Dither” for Dither Effect Name. [Commit.][377]
  * Disambiguated the string “Blur” for Blur Effect Name. [Commit.][378]
  * Disambiguated the string “White Balance” for White Balance Effect. [Commit.][379]
  * Disambiguated the string “Saturation” for Saturation Effect Name. [Commit.][380]
  * Disambiguated the string “Levels” for Levels Effect Name. [Commit.][381]
  * Disambiguated the string “Invert” for Invert Effect Name. [Commit.][382]
  * Disambiguated the string “Gamma” for Gamma Effect Name. [Commit.][383]
  * Fix windows speech to text. [Commit.][384]
  * Disambiguated the string “Contrast” for Contrast Effect Name. [Commit.][385]
  * Disambiguated the string “Colorize” for Colorize Effect Name. [Commit.][386]
  * Disambiguated the string “Normalize” for Normalize Effect Name. [Commit.][387]
  * Disambiguated the string “Mute” for Mute Effect Name. [Commit.][388]
  * Disambiguated the string “Gain” for Gain Effect Name. [Commit.][389]
  * Disambiguated the string of “Pan” for Image Pan. [Commit.][390]
  * Disambiguated the strings of “Pan” for Effect name and Audio Pan. [Commit.][391]
  * Disambiguated the string “Size” for Filesize. [Commit.][392]
  * Disambiguated the string “Size” for Filesize. [Commit.][393]
  * Subtitle edit: switch to KTextEdit so we get spell check. [Commit.][394]
  * Text based speech recognition: propose to show log on failure. [Commit.][395]
  * Fix text edit search, highlight search line background to reflect status (found/not found). [Commit.][396]
  * Various improvments for speech text analysis (display silences, fix zone analysis). [Commit.][397]
  * Introduce analysis of clip zone or subclip. [Commit.][398]
  * Various fixes for speech recognition subtitles (show progress, don’t terminate before process finished). [Commit.][399]
  * Initial version of online resource rewrite. [Commit.][400]
  * Move Qt-OAuth-Lib to a lib. [Commit.][401]
  * Text based editing: show speech recognition progress, initial search. [Commit.][402]
  * Enforce utf8 in auto generated subtitles. [Commit.][403]
  * Fix crash after closing auto subtitles dialog, add config button to speech widgets opening the config page. [Commit.][404]
  * Remember last speech model used. [Commit.][405]
  * Only list speech model folder if it contains an expected config file, and disable/enable speech features when system config is updated. [Commit.][406]
  * Make speech recognition work with custom model folder. [Commit.][407]
  * Fix custom speech model folder broken. [Commit.][408]
  * Implement speech model deletion. [Commit.][409]
  * Move speech recognition settings to Kdenlive’s main settings dialog. [Commit.][410]
  * Speech to text: set clip monitor zone when a sentence is selected in text editor. [Commit.][411]
  * Speech to text widget: put text in a list view, clicking on a sentence seeks the clip monitor. [Commit.][412]
  * Wip: first version of working text clip analysis. [Commit.][413]
  * Fix speech script install, add preview for text based edit widget. [Commit.][414]
  * Fix compilation with KF5 < 5.71. [Commit.][415]
  * Speechdialog: Fix url for archive extract job. [Commit.][416]
  * Update copyright year for speechdialog. [Commit.][417]
  * First draft of speech to text (requires the python vosk and srt modules). [Commit.][418]
  * Automatically highlight text for editing when adding a subtitle. [Commit.][419]
  * FIx possible crash on subtitle resize, and allow cutting unselected subtitle. [Commit.][420]
  * Fix subtitle text not updated on go to next/prev and related crash. [Commit.][421]
  * Allow keyboard grab of subtitles. [Commit.][422]
  * Allow resizing unselected subtitles. [Commit.][423]
  * Remove env variable breaking UI translation. [Commit.][424]
  * WHen dragging, hover on tab will focus the tab. Make it work from start. [Commit.][425]
  * Focus dockwidget on tab hover. [Commit.][426]
  * Switch to updated subtitle icon &#8211; should be working now. [Commit.][427]
  * Fix clip with mix transition cannot be cut in some circumstances. [Commit.][428]
  * Ensure all track tags have the same width if more than 10 tracks. [Commit.][429]
  * Smaller icons in library widget. [Commit.][430]
  * Fix rendering uses wrong locale, resulting in broken slowmotion in render and possibly other issues on some locales. [Commit.][431]
  * Fix building tests with odd MLT install. [Commit.][432]
  * Fix build. [Commit.][433]
  * Make RTTR optional. [Commit.][434]
  * Allow building with Qt 5.11 (on Debian stable). [Commit.][435]
  * Expose proxy info in playlist clip properties (to allow delete, etc). [Commit.][436]
  * Fix proxied playlists rendering blank and missing sound. [Commit.][437]
  * Fix playlist proxies broken. [Commit.][438]
  * Fixed issue where changing speed resets audio channel of clip to channel 1. [Commit.][439]
  * Ensure color/image/title clips parent producer always has out set as the longest duration of its timeline clips. [Commit.][440]
  * Ensure clips have an “unnamed” label if name is empty. [Commit.][441]
  * Fix parameter type. [Commit.][442]
  * Typewriter effect. [Commit.][443]
  * Rename “record timecode” to “source timecode”. [Commit.][444]
  * Effect keyframe minor fixes (improve hover color and allow pasting param to keyframe 0). [Commit.][445]
  * Fix frame timecode not updated in monitor overlay. [Commit.][446]
  * Re-enable audio playback on reverse speed. [Commit.][447]
  * Fix changing speed breaks timeline focus. [Commit.][448]
  * Ensure a group/ungroup operation cannot be performed while dragging / resizing a group. [Commit.][449]
  * Cleanup monitor overlay toolbars and switch to QtQuick2 only. [Commit.][450]
  * Improve show/hide monitor toolbar (ensure it doesn’t stay visible when mouse exits monitor). [Commit.][451]
  * Check if QPainters were initialised. [Commit.][452]
  * Correctly disable subtitle widget buttons when no subtitle is selected, add button tooltips. [Commit.][453]
  * Various typo fixes, patch by Kunda Ki. [Commit.][454]
  * Fix lift value incorrect on click. [Commit.][455] Fixes bug [#431676][456]
  * Switch failed operation messages to ErrorMessage for better visibility. [Commit.][457]
  * Update render target when saving project under a new name. [Commit.][458]
  * Some polishing for effect and rotoscoping qml overlays. [Commit.][459]
  * Add monitor and ruler key binding info. [Commit.][460]
  * Add DropArea to “Master” button to drop effects to it. [Commit.][461]
  * Move key binding info on the left, context item info on the right, show key info for project bin. [Commit.][462]
  * Add double click info for subtitle track. [Commit.][463]
  * Improve and fix ressource manager, add option to add license attribution. [Commit.][464]
  * Fix some crashes on subtitle track action. [Commit.][465]
  * Inform user on failed paste. [Commit.][466]
  * Improve subtitle track integration: add context menu, highlight on active. [Commit.][467]
  * Set range for zoome of avfilter.zoompan to 1-10 (effect doesn’t support. [Commit.][468]
  * Fix incorrect arguments parsing on app restart. [Commit.][469]
  * Fix build. [Commit.][470]
  * Fix compilation. [Commit.][471]
  * Fix several key binding message issues (missing/incorrect messages, incorrect background highlight). Related to #916. [Commit.][472]
  * Status bar: add key binding info zone to display possible key combinations in timeline. [Commit.][473]
  * Fix recent regression (crash moving clip in timeline). [Commit.][474]
  * Fix subtitles not displayed on project opening. [Commit.][475]
  * Attempt to fix subtitle encoding issue. [Commit.][476]
  * Fix broken Freesound login and import. [Commit.][477]
  * Fix regression in subtitle resize. [Commit.][478]
  * Fix clips incorrectly resized on move with mix. [Commit.][479]
  * Fix grouped clips independently resized when resizing the group. [Commit.][480]
  * Add Shift modifier to spacer tool to move guides too. [Commit.][481]
  * Fix double insertion of image sequences. [Commit.][482]
  * Search recursive for luma files to find lumas installed with KNewStuff. [Commit.][483]
  * Default filter for only supported files. [Commit.][484]
  * Update kdenliveeffectscategory.rc. [Commit.][485]
  * Disable crashing context menu in title widget. [Commit.][486]
  * Refactor: DRY up some code. [Commit.][487]
  * Add scrolling orientation setting. [Commit.][488]
  * Fix spelling and remove unnessecary comments. [Commit.][489]
  * Update Copyright. [Commit.][490]
  * Add import and export for layouts, introduce new file type. [Commit.][491]
  * Don’t store current variables (doc, itemmodel) in media browser, add clip to project on double click. [Commit.][492]
  * Fix disabled clip regression (color and opacity changes were not applied anymore). [Commit.][493]
  * Fix compilation. [Commit.][494]
  * Delete equalizer.xml. [Commit.][495]
  * Delete eq.xml. [Commit.][496]
  * Delete selectivecolor.xml. [Commit.][497]
  * Delete unsharp.xml. [Commit.][498]
  * Dragging an effect from a track to another should properly activate icon and create an undo entry. [Commit.][499]
  * Always keep timeline cursor visible when seeking with keyboard, not only when “follow playhead when playing is enabled”. [Commit.][500]
  * Implement missing subtitle copy/paste. [Commit.][501] Fixes bug [#430843][502]
  * Fix crash on copy subtitle (not implemented yet). [Commit.][503]
  * Ensure jobs for timeline clips/tracks are properly canceled when the clip/track is deleted, fix crash on audio align deleted clip. [Commit.][504]
  * Fix crash check lockfile. [Commit.][505]
  * Add a lock file to check for startup crash and propose to reset config. [Commit.][506]
  * Fix crash if the clip of an audio align job is deleted during calculations. [Commit.][507]
  * Fix possible crash dragging clip in timeline from a file manager. [Commit.][508]
  * Added fix for ffmpeg 4.2. [Commit.][509]
  * Add AV1 profile. [Commit.][510]
  * Various display adjustments for compositions and clips. [Commit.][511]
  * Reset config should also delete xmlui config file. [Commit.][512]
  * Fix disabling proxy loses some clip properties. [Commit.][513]
  * Improve MLT build by enabling more options. [Commit.][514]
  * Add patterns to the titler widget. [Commit.][515]
  * Fix tests. [Commit.][516]
  * Fix some regressions in keyframe move. [Commit.][517]
  * Update copyright year to 2021. [Commit.][518]
  * Read mediainfo’s TimeCode_FirstFrame tag. [Commit.][519]
  * Changed Widget name. [Commit.][520]
  * Import img seq added. [Commit.][521]
  * Add mediainfo based recording timecode option in clip monitor. [Commit.][522]
  * Import window as widget. [Commit.][523]
  * Fortesting. [Commit.][524]
  * Merge. [Commit.][525]
  * Undo/redo on clip monitor set in/out point. [Commit.][526]
  * Don’t snap on subtitles when track is hidden. [Commit.][527]
  * Add option to delete all effects in selected clip/s. [Commit.][528]
  * Fix some more xml parameters by Eugen Mohr. [Commit.][529]
  * Fix crash when all audio streams of a clip were disabled. [Commit.][530] Fixes bug [#429997][531]
  * Fix some broken effects descriptions, spotted by Eugen Mohr. [Commit.][532]
  * Reduce latency on forwards/backwards play. [Commit.][533]
  * Add ITU 2020 colorspace in clip properties. [Commit.][534]
  * Fix the integer value of effect parameter’s checkbox. Fixes #880. [Commit.][535]
  * Fix various typos spotted by Kunda Ki. [Commit.][536]
  * Update binplaylist.cpp. [Commit.][537]
  * Update binplaylist.cpp. [Commit.][538]
  * Update binplaylist.cpp. [Commit.][539]
  * Update Composition.qml. [Commit.][540]
  * Update projectmanager.h. [Commit.][541]
  * Update Clip.qml. [Commit.][542]
  * Update kdenlive_render.cpp. [Commit.][543]
  * Update avfilter_colorlevels.xml. [Commit.][544]
  * Update Clip.qml(unnecessary change mistake from my end). [Commit.][545]
  * Fix user-facing and non-user-facing typos. [Commit.][546]
  * Automatically update title clip name when we edit a duplicate title. [Commit.][547]
  * Add option to not pause the playback while seeking. [Commit.][548]
  * Fix some crashes with locked subtitle track. [Commit.][549]
  * Fix qml deprecation warning. [Commit.][550]
  * Fix track effects applying only on first playlist. [Commit.][551]
  * Fix timeline vertical scrolling too fast. [Commit.][552]
  * Fix crash on locked subtitle select/move. [Commit.][553]
  * Fix clip move incorrectly rejected. [Commit.][554]
  * Propose to transcode clips that don’t have a valid duration instead of failing. [Commit.][555] Fixes bug [#430262][556]
  * Fix regression with crash in effect stack. [Commit.][557]
  * Add preliminary support to copy a keyframe param value to other selected keyframes. [Commit.][558]
  * Move timeline tooltips in statusbar. [Commit.][559]
  * Update README’s build status. [Commit.][560]
  * Add normalizers to MLT thumbcreator, fixing Kdeinit crash. [Commit.][561] See bug [#430122][562]
  * Effectstack: Add duplicate keyframe(s) button. [Commit.][563]
  * Effectstack: select multiple keyframes by shift-click + drag (like in timeline). [Commit.][564]
  * Improve grabbing of keyframes in effect stack. [Commit.][565]
  * Initial implementation of grouped keyframe operation (move/delete). Select multiple keyframes with CTRL+click. [Commit.][566]
  * When calculating a folder hash (to find a missing slideshow), take into accound the file hash of 2 files inside the folder. [Commit.][567]
  * Ensure subtitle track buttons are hidden when the track is hidden. [Commit.][568]
  * Fix project profile creation dialog not updating properties on profile selection. [Commit.][569]
  * Don’t change Bin horizontal scrolling when focusing an item. [Commit.][570]
  * Fix composition unselected on move. [Commit.][571]
  * Add channel selection to audiowaveform filter. [Commit.][572]
  * Fix unwanted keyframe move on keyframe widget seek. [Commit.][573]
  * Don’t snap on subtitles when locked. [Commit.][574]
  * Show/lock subtitle track now correctly uses undo/redo. [Commit.][575]
  * Restor subtitle track state (hidden/locked) on project opening. [Commit.][576]
  * Fix qmlt typo. [Commit.][577]
  * Fix color picker offset, live preview of picked color in the button. [Commit.][578]
  * Implement subtitle track lock. [Commit.][579]
  * Add hide and lock (in progress) of subtitle track. [Commit.][580]
  * Zoom effect keyframe on CTRL + wheel, add option to move selected keyframe to current cursor position. [Commit.][581]
  * Add “unused clip” filter in Project Bin. [Commit.][582] Fixes bug [#430035][583]
  * Add deprecated label to deprecated effects. [Commit.][584]
  * Removed last commit. [Commit.][585]
  * Import img sequence added. [Commit.][586]
  * Import Window as Widget Feature. [Commit.][587]
  * Add libass target for AppImage. [Commit.][588]
  * Fix minor typos. [Commit.][589]
  * Add menu for subtitle clips. [Commit.][590]
  * Various subtitle fixes (moving, allow selecting). [Commit.][591]
  * Fix subtitle resize undo. [Commit.][592]
  
 [9]: http://commits.kde.org/kdenlive/ccc7884ba56e6781d0bfc4020b3a000e9729068e
 [10]: http://commits.kde.org/kdenlive/b74e28aabb3e994c7304c560addaaa44392fecac
 [11]: http://commits.kde.org/kdenlive/769b551b27cff73c2aba9511e278dab4717283d4
 [12]: http://commits.kde.org/kdenlive/06879371231ace061565deb6aa85cc6e7a93c080
 [13]: http://commits.kde.org/kdenlive/bb4b151a85b3fb8da33b82caadd74a0fe8d858c5
 [14]: http://commits.kde.org/kdenlive/2c7084d9bd2117b4750d66d9ccb9a3a59908afac
 [15]: http://commits.kde.org/kdenlive/7d053acc8f6a8be1d70f30fd8781358dfe1f22fa
 [16]: http://commits.kde.org/kdenlive/3a36c4ac8c4c695734c78f021b56df8b2f6183fd
 [17]: http://commits.kde.org/kdenlive/78f4cfea4db89c72082c26226ad4407927788c1a
 [18]: https://bugs.kde.org/424582
 [19]: http://commits.kde.org/kdenlive/e85005bab3f59bdf003aef160ca4d79075b93976
 [20]: http://commits.kde.org/kdenlive/7d854707582246cab8f08409c8727be5520b86a5
 [21]: https://bugs.kde.org/435531
 [22]: http://commits.kde.org/kdenlive/084d021f5a470ef74ef2c288223cee3bcf2c5c18
 [23]: http://commits.kde.org/kdenlive/4865c5121a39784e2d8bc5ceaa1019206c364439
 [24]: http://commits.kde.org/kdenlive/8ce010d4410dd23144aa47af9703b3b90783bf3a
 [25]: http://commits.kde.org/kdenlive/87637ac4f0fa85c3cf2bb77a19a13b43fd26319b
 [26]: http://commits.kde.org/kdenlive/47a0961d3faf60dba538969301a03abf856a5501
 [27]: http://commits.kde.org/kdenlive/e2db3ecf80ccaff8033b47b5c4700fc516d4c2a2
 [28]: http://commits.kde.org/kdenlive/a82db684156c35022c258df47fef86a2cc92b6ae
 [29]: http://commits.kde.org/kdenlive/55b6bf25bf454fa5329606a4a6b4c2ea50b620b6
 [30]: http://commits.kde.org/kdenlive/f717ff8d00aec2f15958c4c527d63795a6b41773
 [31]: http://commits.kde.org/kdenlive/d765c0b9268714e4b2a5d40c1cf362f58740d574
 [32]: http://commits.kde.org/kdenlive/49db3a2fce43d9c14652730efe8696ae1290023e
 [33]: http://commits.kde.org/kdenlive/d343386ab8e2d9350b5a1fcad3e31dfd50b75786
 [34]: http://commits.kde.org/kdenlive/8d390084baf12aa132e0d0bcbdeff897a7ed7eb7
 [35]: http://commits.kde.org/kdenlive/24278b9a83f5bd5279faa202d81a6e318066f566
 [36]: https://bugs.kde.org/433618
 [37]: http://commits.kde.org/kdenlive/73d9982d2c207c0cc1f2ac01b005024cd38fbba7
 [38]: http://commits.kde.org/kdenlive/263d26575da394b7ccbc598609320f750f30c4aa
 [39]: http://commits.kde.org/kdenlive/7333e5d34f13ff131ce6e4f26c044bd007abed2d
 [40]: http://commits.kde.org/kdenlive/e4796e36cbe2bead87a0291ce1e05949ad210f4a
 [41]: https://bugs.kde.org/425417
 [42]: http://commits.kde.org/kdenlive/7a9a9a03bacd53f843a43d9a8485c4a2bc46ab6b
 [43]: https://bugs.kde.org/425682
 [44]: http://commits.kde.org/kdenlive/caaa044f92b744e9914ba5410f780b4ce1c07931
 [45]: https://bugs.kde.org/390668
 [46]: http://commits.kde.org/kdenlive/b46a47392abd74b1f11b0c88d78c1533ffa44a21
 [47]: http://commits.kde.org/kdenlive/ddb0311d98255e043ad93c6306a8c3f323622dd4
 [48]: http://commits.kde.org/kdenlive/f6fdb1add8025e2afe2af6ef9c65dba0dee1f145
 [49]: http://commits.kde.org/kdenlive/0e833e9c3fec2ba6bf0830d656b8f5d6b23f8566
 [50]: http://commits.kde.org/kdenlive/f5f1c670d6d88377fca1f860cf21b510abed3370
 [51]: http://commits.kde.org/kdenlive/a2004f7b9001e6622d7ed4268aef45721d424e6a
 [52]: https://bugs.kde.org/434429
 [53]: http://commits.kde.org/kdenlive/d562d3cee8d61333e990b65fa9a2c349f288f062
 [54]: http://commits.kde.org/kdenlive/de55a57e7395738028478748b1ffec662baffed6
 [55]: https://bugs.kde.org/411324
 [56]: http://commits.kde.org/kdenlive/ea3031cdc28a12a5ebc9b58bac59922c529153ba
 [57]: http://commits.kde.org/kdenlive/301cfeb8978c69a17289923409d10c59527da292
 [58]: https://bugs.kde.org/386398
 [59]: http://commits.kde.org/kdenlive/68e559ec556f0498a906445eca91485375f28de0
 [60]: https://bugs.kde.org/435147
 [61]: http://commits.kde.org/kdenlive/5e5d8ffa0722d758e9dd0029dd7335f3764ed463
 [62]: https://bugs.kde.org/434401
 [63]: http://commits.kde.org/kdenlive/89a31330c5e58606d80c1d36859607109a1d6c1a
 [64]: http://commits.kde.org/kdenlive/0a2910ef4351c3038ba229982f6a6b9e13a8b0ec
 [65]: https://bugs.kde.org/435263
 [66]: http://commits.kde.org/kdenlive/5f65e06b45d802e92c0a26828f82745eb4c9c910
 [67]: https://bugs.kde.org/429795
 [68]: https://bugs.kde.org/428263
 [69]: https://bugs.kde.org/392670
 [70]: http://commits.kde.org/kdenlive/0329427786a3e804c60bc23b2b2099506eb4a9fc
 [71]: https://bugs.kde.org/420623
 [72]: http://commits.kde.org/kdenlive/aca447195470a5d300808451a96d0053dad1ff31
 [73]: http://commits.kde.org/kdenlive/734575e7112b25f822afe60d31e3c64c342b8909
 [74]: http://commits.kde.org/kdenlive/0074ecb09020f2b7206ca0a0600660b4936bae70
 [75]: http://commits.kde.org/kdenlive/a94846c4658f7af59f3f472581ff0e6bdef75a6b
 [76]: http://commits.kde.org/kdenlive/fcc17c068c2077c507f38578e007a97ef697336b
 [77]: https://bugs.kde.org/409667
 [78]: http://commits.kde.org/kdenlive/6bc54d5d8e6fda876a06ed30e544bb193ae199d4
 [79]: http://commits.kde.org/kdenlive/758009ce67a4b2e7832b81679959389971f8131e
 [80]: http://commits.kde.org/kdenlive/f32f9d9f1709e9aeb7c68afea28b95895cf10c76
 [81]: http://commits.kde.org/kdenlive/20d6725789aea63107ff9c7f9ff244a07a26a97f
 [82]: http://commits.kde.org/kdenlive/24532f22171222cdffb2058011664fd6c4ef34b6
 [83]: https://bugs.kde.org/433889
 [84]: http://commits.kde.org/kdenlive/5f712c9178170af239ad1db0b21caf130e350e3d
 [85]: https://bugs.kde.org/428632
 [86]: http://commits.kde.org/kdenlive/a05d6e27500b3cedfb4ce038cc60a96192f7e371
 [87]: http://commits.kde.org/kdenlive/f773fe911d38b999678a03d6bebc394de6afae76
 [88]: http://commits.kde.org/kdenlive/9ac90dcf3da6f49404acf19bfcf0db002328a012
 [89]: http://commits.kde.org/kdenlive/93dbb1f0995163d96a63c5e7a2a0c812542a681b
 [90]: https://bugs.kde.org/353051
 [91]: http://commits.kde.org/kdenlive/a996c2c98d47a39fdd5a84b8da161ed8de98d4b6
 [92]: https://bugs.kde.org/434360
 [93]: http://commits.kde.org/kdenlive/9b80720e3bec0fd671c435fe53ec186aa4d9561b
 [94]: http://commits.kde.org/kdenlive/16928a5ce4b25eea93ada0bb135263cc6ded4f07
 [95]: http://commits.kde.org/kdenlive/04527b701ccd76d9febc11fec6ba8179fa588c20
 [96]: http://commits.kde.org/kdenlive/b632603562b2c697fd9f575de50142d033018a62
 [97]: http://commits.kde.org/kdenlive/1e5f2ea53e67f4719ac974efb43682d42ef5ab5f
 [98]: http://commits.kde.org/kdenlive/d034d62b410f556626670d49306b14ebe4c4e942
 [99]: http://commits.kde.org/kdenlive/540b919f4e1cb23278630e81d5d460b376698a34
 [100]: http://commits.kde.org/kdenlive/1f4bbabfd55970369f3ed3a1533773e251e4a5e9
 [101]: http://commits.kde.org/kdenlive/482eb8a52b0b7067e0f51f49412940838465a61b
 [102]: https://bugs.kde.org/425639
 [103]: http://commits.kde.org/kdenlive/d71d91a534fec6db5dc0808c571f5926897b02e7
 [104]: http://commits.kde.org/kdenlive/985844f66805c9eb848188de41d10d9e64986df9
 [105]: http://commits.kde.org/kdenlive/7ba3ff0baea78abfb60e5ad53fa8ccfa2e23dab0
 [106]: http://commits.kde.org/kdenlive/bff5ba5be3bc885a2bf790b25f64ea9259f89355
 [107]: http://commits.kde.org/kdenlive/1360538b034407ea37ae2fb3146066ae5c5453f5
 [108]: http://commits.kde.org/kdenlive/b50f8eec4161eb8de842282ff2dcdc2811d814c2
 [109]: http://commits.kde.org/kdenlive/14377458ea9264eac55873ef9504d3e1bf57e6bf
 [110]: http://commits.kde.org/kdenlive/4ef24fb77ae2c2a3b65ab389594adae82b84b916
 [111]: http://commits.kde.org/kdenlive/c63280d4bf6c02d47b16a791028980fbf68ce32b
 [112]: http://commits.kde.org/kdenlive/b74e65dc299cb38b4498076bb2ac0e735a3f080e
 [113]: https://bugs.kde.org/425960
 [114]: http://commits.kde.org/kdenlive/d247f4798364c5f2c540522d3b1661cf67640a43
 [115]: http://commits.kde.org/kdenlive/386983a8c35fedaf9e4709bae62f6262bbb2a82a
 [116]: http://commits.kde.org/kdenlive/5c3fad4a9caf8ad60b70bfc9c97b7d05d5bdfb4e
 [117]: http://commits.kde.org/kdenlive/3bcc359f16ac795dc45a3419e30c794bf2ad6860
 [118]: http://commits.kde.org/kdenlive/77830ba057c1afb7b55050710401116171672c91
 [119]: http://commits.kde.org/kdenlive/a5472db7ccfa45d0a3ff82c82d70e7981f8b2af5
 [120]: http://commits.kde.org/kdenlive/da2ec73d3b543f142acce40e804c0726fef789f5
 [121]: http://commits.kde.org/kdenlive/21ab179d16ee807e78025d2c07dacd9400b61bf5
 [122]: http://commits.kde.org/kdenlive/ec712303791cb2c54eccffe148e0b0574ea3a4c3
 [123]: http://commits.kde.org/kdenlive/09c6656a6882fd17401968fa1157dc4224d681bd
 [124]: http://commits.kde.org/kdenlive/b2635a9b8585b953f17a801804c8b163be4293bb
 [125]: http://commits.kde.org/kdenlive/d21f2e46fd5d5db87f76b0974623131fe7627d5c
 [126]: http://commits.kde.org/kdenlive/54ca0baea1acb3eab5a08b8ef8a2e98668e74978
 [127]: http://commits.kde.org/kdenlive/e20a4a4a07abe628b504e96e9b3aba9b1aa5e5ec
 [128]: https://bugs.kde.org/434179
 [129]: http://commits.kde.org/kdenlive/46a5ae7d50d59669a5505fd1616e9373c61b8885
 [130]: http://commits.kde.org/kdenlive/e8f1480f1c532924b45eef59d446d3c2ffe4e55f
 [131]: http://commits.kde.org/kdenlive/4f3545cd0bbf52ca3f68816f0d6dc94f4b373a23
 [132]: http://commits.kde.org/kdenlive/4eee97f22a5de40733065b6b590c456b86e0c437
 [133]: http://commits.kde.org/kdenlive/440d9b51553781d62333e650a866dc9f40d2b01f
 [134]: http://commits.kde.org/kdenlive/467dcdc25ddbeb6236a7ee3c14f19a5d4b6822b4
 [135]: http://commits.kde.org/kdenlive/635f91590f4bb5bcefce3499bd952e381f9bafdc
 [136]: http://commits.kde.org/kdenlive/c0243124d185befde53ef8fe2f6bdc51a0bde6f0
 [137]: http://commits.kde.org/kdenlive/b2ae458f926890eeda764e3f54bfce965de36098
 [138]: http://commits.kde.org/kdenlive/eb7f502613aaadf2b5464b37386c4ff82cc34477
 [139]: http://commits.kde.org/kdenlive/7a00c5718822fd7b929778bce5b3d7cda08e1e57
 [140]: http://commits.kde.org/kdenlive/fe6d5e0b98ea688830d7fa5f78a07643a943125e
 [141]: http://commits.kde.org/kdenlive/8caf4274190b9059baa04c500d02c2a23953e48b
 [142]: http://commits.kde.org/kdenlive/7cf862ea65f3a4f8464fe88f4430628150ad2b5a
 [143]: http://commits.kde.org/kdenlive/cfc0bcedf0a1561aa3f7ac882caefa769103e929
 [144]: http://commits.kde.org/kdenlive/fea56f7b100c4597fce0596baf72f60fd31c8144
 [145]: http://commits.kde.org/kdenlive/f015fd43c162d3840f572f464f96c67b02c2f783
 [146]: http://commits.kde.org/kdenlive/20783fde63a248ae7cdcf5f0adb14ff4f9acb7b1
 [147]: http://commits.kde.org/kdenlive/749d5e59ccb89623f14ff5f7460574fccaabc997
 [148]: http://commits.kde.org/kdenlive/71ef7b641bfee917afe061bb602835103ac59d6c
 [149]: http://commits.kde.org/kdenlive/2508239537801d36664ebd5967900e558eb52c0f
 [150]: http://commits.kde.org/kdenlive/615887974288c3ce94bb874a88dfef99d8de60ea
 [151]: https://bugs.kde.org/434405
 [152]: http://commits.kde.org/kdenlive/df06ca6cc84773f431dcbc5201f4ea04b661fd3d
 [153]: http://commits.kde.org/kdenlive/d548ace29f557079d0abd717685b3900fd78c60f
 [154]: http://commits.kde.org/kdenlive/09b664e0a949c30cbeece14d83b84add818439bd
 [155]: http://commits.kde.org/kdenlive/cfd169320cd84a2235f8c094b8376bc2f5427e61
 [156]: http://commits.kde.org/kdenlive/83c0ded8c065a7d9e48ac4a6e6aa6d3ffb86077b
 [157]: http://commits.kde.org/kdenlive/40a4166508663d9bc38b191c3053154ea0edeb1d
 [158]: http://commits.kde.org/kdenlive/78b6cf6c98ca848fa858458bd34c0391b495466a
 [159]: http://commits.kde.org/kdenlive/a6a125fd75a4c834faa5387de9f91d840003da6c
 [160]: http://commits.kde.org/kdenlive/27db4607d6d0e6a81d8664e917e6907088e356d2
 [161]: http://commits.kde.org/kdenlive/9ee93449c73239637b2e19e14b188f06b7525bc8
 [162]: http://commits.kde.org/kdenlive/7ca22b50a5323ead9ea267606558eb4e6dce3b0f
 [163]: http://commits.kde.org/kdenlive/9b945e756146eb961857bebddba84903451435ff
 [164]: http://commits.kde.org/kdenlive/4279e53aac9b8632aaf5217f59e3632efee1d04c
 [165]: http://commits.kde.org/kdenlive/11559bb392ab13f520243d43fbbc3f4dee057897
 [166]: http://commits.kde.org/kdenlive/5b1fd3948254421a105ff4a99df39d292e10f823
 [167]: http://commits.kde.org/kdenlive/30eb85f3ceb5eeaf1fac5217a6d5e5af8aa12c5f
 [168]: http://commits.kde.org/kdenlive/b29ce73ff7f4d01cf38cd2a4637fd4f956d7facc
 [169]: http://commits.kde.org/kdenlive/b28377b35835dd024e22df2f109cff4dd46d2d5d
 [170]: http://commits.kde.org/kdenlive/296976935875fba3b2f70a66cf1033fe199ab1a1
 [171]: http://commits.kde.org/kdenlive/df92fa264686fca9783b7ce81b00073c2e6308e2
 [172]: http://commits.kde.org/kdenlive/199d7d3a010a194f0e949a8407504ae04400166d
 [173]: https://bugs.kde.org/433679
 [174]: http://commits.kde.org/kdenlive/0a7b17ab8bb6deaee3e32a7026a1669bda8a3a9e
 [175]: https://bugs.kde.org/432580
 [176]: http://commits.kde.org/kdenlive/e152455ef83089e718de59d0366c1500b683ce42
 [177]: http://commits.kde.org/kdenlive/777e038932c1c9897e4e955e7a14e4427f74b2f1
 [178]: http://commits.kde.org/kdenlive/ca39e773536ac2b4ced0ad10202e0ef63745a65e
 [179]: http://commits.kde.org/kdenlive/e76c17c9a7a75d347a67c3d9a50fb27004b66736
 [180]: http://commits.kde.org/kdenlive/5680f375e7aa8ccd6ca56e0ce303157a223d5a12
 [181]: http://commits.kde.org/kdenlive/3b63c0647bd77a0a82e4700a798de7a02109ff18
 [182]: http://commits.kde.org/kdenlive/e0c0a3df27969261ec4bb211e662e61070291d4e
 [183]: https://bugs.kde.org/423337
 [184]: http://commits.kde.org/kdenlive/abc1984d3ef80716201a540b487302ab0c03c5f9
 [185]: http://commits.kde.org/kdenlive/7ad1d88606286bee0488b07849af48b2e5ea4727
 [186]: https://bugs.kde.org/424809
 [187]: http://commits.kde.org/kdenlive/e4caa646c17a98e326b69fa71f4b106b308376f4
 [188]: http://commits.kde.org/kdenlive/b4d4405788a107580f6c753847677250c6ca9699
 [189]: http://commits.kde.org/kdenlive/c47b674e1985abf95df0ded3184518a16e232485
 [190]: http://commits.kde.org/kdenlive/cae2863526383021743323f1e15c2367f9a8be2c
 [191]: http://commits.kde.org/kdenlive/bf8dac93494276241ba9ac8f95c6a97d1b680636
 [192]: http://commits.kde.org/kdenlive/7c2b4adac1aee276f0ffe2742ed8e51b986b35c9
 [193]: http://commits.kde.org/kdenlive/fcc7016cb1275e3fce685f109bf46b9597077055
 [194]: http://commits.kde.org/kdenlive/16b7e53d8da8edc9e9b366192ba3ecf98729359a
 [195]: http://commits.kde.org/kdenlive/6a68722afb643e6933ad89011e1ed919e7603e18
 [196]: http://commits.kde.org/kdenlive/214cd6d5cd5426aac35dae820ac410e674f14513
 [197]: http://commits.kde.org/kdenlive/e771cc04daa449fe9081c71b9c3bb0839a78bc4e
 [198]: http://commits.kde.org/kdenlive/2bf8f8cee5f8bec4929b87183bee7d0a21125a0a
 [199]: http://commits.kde.org/kdenlive/ad7f47854afec07607ec1f36566aae9c7f6defa7
 [200]: http://commits.kde.org/kdenlive/91ec5baa769cf79ccdd1a971512d700531396d5c
 [201]: http://commits.kde.org/kdenlive/0eb69ac1dc72e77a8a38cf0e23aa4f36719cad89
 [202]: http://commits.kde.org/kdenlive/7816192f58ec2c92e452373aa37406f84557e6e0
 [203]: http://commits.kde.org/kdenlive/d5df2439482aea1a8122aea937d8f2d9d7fac05b
 [204]: http://commits.kde.org/kdenlive/9175175176b9eb69c03b0b4ccbb8170ae5722f39
 [205]: http://commits.kde.org/kdenlive/773f41b175ce7439beda66de3e0627193d855352
 [206]: https://bugs.kde.org/433010
 [207]: http://commits.kde.org/kdenlive/65c666948cd93ed8b28be9d2163475206da16713
 [208]: http://commits.kde.org/kdenlive/a22c7e40015072ccee588e984fa9fbaec11b1f1e
 [209]: http://commits.kde.org/kdenlive/a3b273833d2b4a4a1eedaf3448306c1c6a5d7bab
 [210]: http://commits.kde.org/kdenlive/8b7bd71eaea1c0e55633dfd448add8373e9ff827
 [211]: http://commits.kde.org/kdenlive/d130414a794b559a6e7186752b7e3a7a1dab74a3
 [212]: http://commits.kde.org/kdenlive/fb4dec0e0dcf28881f9e079ce9b658d171e2943a
 [213]: http://commits.kde.org/kdenlive/6be62de80e384383b641ef94b72d48a7d877ee3d
 [214]: http://commits.kde.org/kdenlive/557632dfebb731182193b40bcb62c5802d3a794d
 [215]: http://commits.kde.org/kdenlive/c0b1a4ba7af60d77ec7bae78b56adbfc539d8906
 [216]: http://commits.kde.org/kdenlive/b9c8ce82373af1e625941d4598eb1dab76e39747
 [217]: https://bugs.kde.org/433193
 [218]: http://commits.kde.org/kdenlive/071d831f4f722f0051784841c7466967b308a8fb
 [219]: https://bugs.kde.org/433527
 [220]: http://commits.kde.org/kdenlive/413b6fa36d8ef5a19e83a746df2349d2c026bd76
 [221]: http://commits.kde.org/kdenlive/851a68cb7e2e21fe5ac0e78ab85181f1cf5a97f8
 [222]: http://commits.kde.org/kdenlive/5b5a368e030e6b4bfcf38d7ad0f939e5eb05a1e1
 [223]: http://commits.kde.org/kdenlive/aeb264d3065739f0bb5f9b037fc03e33ff55faae
 [224]: http://commits.kde.org/kdenlive/f2e9760c834859312a60d06158a9bc495c3819b8
 [225]: http://commits.kde.org/kdenlive/5d445be8ae02dd8e99721ac0b74279580996be93
 [226]: http://commits.kde.org/kdenlive/6fe2391fe2e8f8fd9f9f056913ef54854ae6eac6
 [227]: http://commits.kde.org/kdenlive/71b670dfcebb250b694d03ecf0af9807baba3988
 [228]: http://commits.kde.org/kdenlive/e5621010aab02305fef4bb512877d6f24b993461
 [229]: http://commits.kde.org/kdenlive/dac5fb711e9960d0ab6677794123f3e895a241fb
 [230]: http://commits.kde.org/kdenlive/12be3276e58bd2f773961013c577ffd924f68639
 [231]: http://commits.kde.org/kdenlive/ddeedfa6f3cd1fd9e6e574d187eda49a64d203d9
 [232]: http://commits.kde.org/kdenlive/e1ef25fb64804dc53e7c096c958b780028f92b7c
 [233]: http://commits.kde.org/kdenlive/e26563bb4dbbc45f68ad24ba867fe838aa8138c8
 [234]: https://bugs.kde.org/433773
 [235]: http://commits.kde.org/kdenlive/fe7bb6ec275a7320da4580dc5c5a06f0d4b826ae
 [236]: http://commits.kde.org/kdenlive/221a1dd129223c6549add5ed77486b00baff5b9a
 [237]: http://commits.kde.org/kdenlive/b7f1705cc2fb586647cb662b6b31d132d0404442
 [238]: http://commits.kde.org/kdenlive/b3dd8a7cab5eadf08509a4ae9f2bf7a9099841ba
 [239]: http://commits.kde.org/kdenlive/dc6a21501c49b449a5d8f9355fa83d810bcfa768
 [240]: http://commits.kde.org/kdenlive/8acc424593fb13aef258f079d73a103553c3339f
 [241]: http://commits.kde.org/kdenlive/62e74b174a9efd812bf52e72ab7815e1818c2f2b
 [242]: http://commits.kde.org/kdenlive/00959165b309f493458464396f0158a2879aa56f
 [243]: http://commits.kde.org/kdenlive/e52546e8cae5ae64a01f763a4c96bb779dbe1ffc
 [244]: http://commits.kde.org/kdenlive/7d3b3756d1224403a5473d8380e20553105437af
 [245]: http://commits.kde.org/kdenlive/b88ef859194b030cbb3e77c3960f7e52e5877454
 [246]: http://commits.kde.org/kdenlive/52cfb205fb5cceade999824fb94cc172ac578f2d
 [247]: https://bugs.kde.org/407574
 [248]: http://commits.kde.org/kdenlive/885a968b851e4662c98730eaea22efd90cad6497
 [249]: http://commits.kde.org/kdenlive/89c5c95f28f1b515e054bc280f61e5d7c2cbc8f4
 [250]: https://bugs.kde.org/433615
 [251]: http://commits.kde.org/kdenlive/c63acf8556915d5d58c233d06aa9b9e62d58aabd
 [252]: http://commits.kde.org/kdenlive/d1e4f1e4cb73f2bea390d56b4eb72106ba9fe62f
 [253]: http://commits.kde.org/kdenlive/12dcf8060cfb2913955cd6c0855bc5d62a070b83
 [254]: http://commits.kde.org/kdenlive/03680feec9ac256503df390c3fdeadd9a20f96ab
 [255]: http://commits.kde.org/kdenlive/dff32d8e98f2c4230b91c3cddd6d385fdb102e30
 [256]: http://commits.kde.org/kdenlive/fbeee949fc8cb8809ad0c29ebb1ee06fd6bb0efd
 [257]: https://bugs.kde.org/433600
 [258]: http://commits.kde.org/kdenlive/e5c2aa18afafbc920d2e454bf23d1b5b1fc590c4
 [259]: http://commits.kde.org/kdenlive/7beefdd44eca8e1eb82da61860ce21dfbc03c6fa
 [260]: http://commits.kde.org/kdenlive/a00d28ea502998dd707bbee996521de0743eed58
 [261]: https://bugs.kde.org/433623
 [262]: http://commits.kde.org/kdenlive/d67fcf28e2ab0ca5bec021f0dd675ae997278388
 [263]: http://commits.kde.org/kdenlive/92c63e83b3373ec07eb3e03a7bd8469527a6a05b
 [264]: http://commits.kde.org/kdenlive/e3d5993d05b4cf4337bd9759ad041a021d0de4ec
 [265]: http://commits.kde.org/kdenlive/2994c6f11a040f0d52d077ffe478583887b24705
 [266]: https://bugs.kde.org/433590
 [267]: http://commits.kde.org/kdenlive/6dc1c6f7a62025909051c588676d74c408d17518
 [268]: https://bugs.kde.org/432016
 [269]: http://commits.kde.org/kdenlive/9a20bbcc9e8a341dd93967c45d0ae3619bb7c04d
 [270]: http://commits.kde.org/kdenlive/dbf849225f7b2055b4a001bcedd220e1ce480621
 [271]: http://commits.kde.org/kdenlive/e50ce7fc70f9e665567c243ad6ba3ffde97cc11c
 [272]: http://commits.kde.org/kdenlive/fd09a99d7b23f09c0935647e8f882975b2e68dc9
 [273]: http://commits.kde.org/kdenlive/92eaf0f5a3c5ef910011a541531835b9048c0cc1
 [274]: http://commits.kde.org/kdenlive/fb3dd0f5b1dfdbcb86fe21caa29edef36e65f1f6
 [275]: http://commits.kde.org/kdenlive/f84d0f36ea4b6699fd5850b29c8a1ea9fd11c762
 [276]: http://commits.kde.org/kdenlive/281c6a3d3cc9078c636de5c17daeaa8ac6771da6
 [277]: http://commits.kde.org/kdenlive/d5072c3042cb53b17a36c59fe1b91c099844ae43
 [278]: http://commits.kde.org/kdenlive/6d776ac8f2a4430235ec2b03fc71940866dc86fb
 [279]: http://commits.kde.org/kdenlive/a8b09467977c0db5c538c9c8a7ca66273fa5cc8e
 [280]: http://commits.kde.org/kdenlive/36dc29de40fe2b4ab90ab73802adc595fa57b81b
 [281]: http://commits.kde.org/kdenlive/d9c3049074e51d7ee8f7494238a86ca929b99829
 [282]: http://commits.kde.org/kdenlive/67c97f74216c1ab3d9093c7138aede55a2492972
 [283]: https://bugs.kde.org/410908
 [284]: https://bugs.kde.org/411180
 [285]: http://commits.kde.org/kdenlive/1f8f11699373d7cf218cf0f9b96b7e94fad907da
 [286]: http://commits.kde.org/kdenlive/a29377b2d93103042dddbf89fadf8afec5bc05c9
 [287]: http://commits.kde.org/kdenlive/d13b7ca957d81b2d7244675f8fb151825c010b95
 [288]: http://commits.kde.org/kdenlive/0d1085997ba4e0c008492716af157705180c17d4
 [289]: http://commits.kde.org/kdenlive/d0bf2406141c85583a1e9b631a7aac05902cbe6d
 [290]: http://commits.kde.org/kdenlive/74901acaf3b4e8915026f6b4733826a4990528b7
 [291]: http://commits.kde.org/kdenlive/1fc507cb28c055cde66646a81ee2512e96d1cf92
 [292]: http://commits.kde.org/kdenlive/91912ff5c950954627ce422f7fddb16baf2f1017
 [293]: http://commits.kde.org/kdenlive/4035a22f6ea85824d5da5e7cac6a27c79dee7855
 [294]: http://commits.kde.org/kdenlive/2ccd4ea224f76c1cd6c37f7854ba6894969c7d7d
 [295]: http://commits.kde.org/kdenlive/3827e0d013e23b27afd779ecea9af4489530c39c
 [296]: http://commits.kde.org/kdenlive/137fb105da4b4670c0ff40ef22fce2673d3ee81f
 [297]: http://commits.kde.org/kdenlive/b027a779583ccfe484ff22d94a401f1efe4dcee7
 [298]: http://commits.kde.org/kdenlive/3c9fb1a0f4117472a31b270a83cff79c61dbb7c6
 [299]: https://bugs.kde.org/356034
 [300]: http://commits.kde.org/kdenlive/8821b5ead55e536ddb3a3c4aa1a32e04e376f689
 [301]: http://commits.kde.org/kdenlive/b771350ede0178bd4185cb1a9b3c1bffbc36f106
 [302]: http://commits.kde.org/kdenlive/5013499c97ced1326b48749abb65ab4103b16b23
 [303]: http://commits.kde.org/kdenlive/c5d6d41bb1028a0ee1776310090650f42743cd11
 [304]: http://commits.kde.org/kdenlive/9077f8ee6a2251f3ef6e2acf56260e75d30bfb73
 [305]: http://commits.kde.org/kdenlive/655164c6d943958ed7667cd0ab05056f762534bc
 [306]: http://commits.kde.org/kdenlive/db4ad36939fbf33b9a41e98e71366c21c9586463
 [307]: http://commits.kde.org/kdenlive/eb37dfa614612f93e5c8e8c0f7b697e020655e91
 [308]: http://commits.kde.org/kdenlive/f13d23a22ac3157e7b585eee45c71b1d7ecc79f2
 [309]: http://commits.kde.org/kdenlive/66409a70e0ba2479843a63727700f0f1cd891525
 [310]: http://commits.kde.org/kdenlive/addeaec8f82b94de7fad23e812660550af82e96c
 [311]: http://commits.kde.org/kdenlive/078070c06abf7223057db380cbc1fe74adc28c58
 [312]: http://commits.kde.org/kdenlive/ace08d6170f5a657ed5aa28d1162818b455cc259
 [313]: http://commits.kde.org/kdenlive/94770e55a813b4e4eb244a4a19378addcdda7789
 [314]: http://commits.kde.org/kdenlive/04c4a1ce8efa314a9067454bb40e4fe0e5bcc80c
 [315]: http://commits.kde.org/kdenlive/f6b3e27f9c0f73d9a5767e76900ff5949df25bfb
 [316]: http://commits.kde.org/kdenlive/ebb65fc975353ce73b01632acda557dcb755980d
 [317]: http://commits.kde.org/kdenlive/0b5046fff025e3c0833d5dc87e68b14cbb544fee
 [318]: https://bugs.kde.org/403941
 [319]: https://bugs.kde.org/407497
 [320]: http://commits.kde.org/kdenlive/923175f9979e00dbfffc5bb9b81bdadd9a11a18e
 [321]: http://commits.kde.org/kdenlive/8513a71ff32d2eaf9005485d31397d58ed9b600c
 [322]: http://commits.kde.org/kdenlive/a2ce40ec3606f7758b210ab382ad366d430c8f64
 [323]: http://commits.kde.org/kdenlive/f46570bdd2c7a32d7941afcb381a5669b5458fe5
 [324]: http://commits.kde.org/kdenlive/e02c773611140710fbe366e786688f786fb769cd
 [325]: http://commits.kde.org/kdenlive/a137426f2b4ca04c88eabd9e932f2288512df8ba
 [326]: http://commits.kde.org/kdenlive/9ee4a1b71c333bd6e47db03c9edc72e58a58e55f
 [327]: http://commits.kde.org/kdenlive/c6adee5e04297278be97e3547aef73b6d4361484
 [328]: http://commits.kde.org/kdenlive/d9a08cd93d13d58e2feead6331b42f2b62e395d7
 [329]: http://commits.kde.org/kdenlive/a8649411022d2fc34f2b01f4c94f0d01bd127790
 [330]: http://commits.kde.org/kdenlive/db2456e8ae2c72b37992a83cca6a6111078f0780
 [331]: http://commits.kde.org/kdenlive/c523ac4aa9e38d42c3fd33e09db8d21c0552bf1d
 [332]: http://commits.kde.org/kdenlive/e9395b63ca49fb74bcfb4d9f2c8f00207777f34b
 [333]: http://commits.kde.org/kdenlive/e11c9bab8c5f238ad79e37c274c7a326456911f9
 [334]: http://commits.kde.org/kdenlive/603afd6fc82772778017008edde6ecf90b0918a1
 [335]: http://commits.kde.org/kdenlive/bffa093df552bd27a10b78e5a50d3bf5000d82b0
 [336]: http://commits.kde.org/kdenlive/50f59a4ef4613b36216b3300cef2039da09068da
 [337]: http://commits.kde.org/kdenlive/d993f89fb2db033cf408d9c28dc9bd365089a814
 [338]: http://commits.kde.org/kdenlive/8672cec17c5ccef7614d81f2835fc968482bccbd
 [339]: http://commits.kde.org/kdenlive/9507cbf5809fe55e40d44712bd30dacd0aa30a84
 [340]: http://commits.kde.org/kdenlive/93a02fc4482c7cf1ad21790c5864fb797ebb22fe
 [341]: http://commits.kde.org/kdenlive/9dea1662b25e23eaff76a339aa6fa86fe993b345
 [342]: http://commits.kde.org/kdenlive/a6a3985491bae6bd99c02e1c5c87a1aef5c55617
 [343]: http://commits.kde.org/kdenlive/c7f4bc6834a82beee16a094beb774914de079ec2
 [344]: http://commits.kde.org/kdenlive/aed56da0451a98ff9fe1349d8d7cfec28b9492e6
 [345]: http://commits.kde.org/kdenlive/5af041df723b87af9c24a9df11d7715225c56cc6
 [346]: http://commits.kde.org/kdenlive/5943d885d0212ea8d455289b5182d56073974eaf
 [347]: http://commits.kde.org/kdenlive/e3d2081703ff05014530b23b70cfd7cf66ac44e3
 [348]: http://commits.kde.org/kdenlive/006b9b2c9f2d6c398de97d4ceec7bb950fa76a26
 [349]: http://commits.kde.org/kdenlive/401ad8e62dc7da76ea22ea20b30b8094c348a423
 [350]: http://commits.kde.org/kdenlive/c51c68b04b2219aec75efa8aaa72d2aacba8b1af
 [351]: http://commits.kde.org/kdenlive/7acafbeabdb3564f725a05e8c2d1c580fe3cb3a6
 [352]: http://commits.kde.org/kdenlive/0ec55bc9cc7cdb376360dd0cd682e2190e8c7a3a
 [353]: http://commits.kde.org/kdenlive/6b344c8816f0b611e572d932b3a784f6af9e763c
 [354]: http://commits.kde.org/kdenlive/69e4947de30588f996ed51f6263db0f8710af051
 [355]: http://commits.kde.org/kdenlive/4553ccf94ab92d202ef98623ef791555d56fcd51
 [356]: http://commits.kde.org/kdenlive/d1f36c7d8e35ac194d30e5ebd906ac51f3205687
 [357]: http://commits.kde.org/kdenlive/ff40397261de6f88e4cde4a6cf4dd3c03d89a631
 [358]: http://commits.kde.org/kdenlive/df3be5df0489a6209e6d107d751f743df6922fb4
 [359]: http://commits.kde.org/kdenlive/ff1996bce7e6c4c4174511994db660c7fa883a78
 [360]: http://commits.kde.org/kdenlive/b8ea1c61b1e58b5e814a9d12cc4419c87aaa2d49
 [361]: http://commits.kde.org/kdenlive/5b610918580dec128e4d6c07149df8c3102a6b73
 [362]: http://commits.kde.org/kdenlive/888d01fead6da20a8962d913de077a9f06b0d42b
 [363]: http://commits.kde.org/kdenlive/ba24632458bf8c6504437b17131a8c79d6d680a3
 [364]: http://commits.kde.org/kdenlive/06a72f1a9ef4cb51e432af7ad06c8f7d1c2f7815
 [365]: http://commits.kde.org/kdenlive/db2c3668f14d82182fec4ba8e75c47428858de77
 [366]: http://commits.kde.org/kdenlive/89a38c3f4f568ac6877292ec4f75fce1fbd8d30f
 [367]: http://commits.kde.org/kdenlive/04b0cc9dc6f1b75e4a277bc236ff70a791b59ff7
 [368]: http://commits.kde.org/kdenlive/ba84df2c236cca3fcac8db21b837ef6d55081e15
 [369]: http://commits.kde.org/kdenlive/e1a12965436fdb3413e1f02003132ca7f7e1ab85
 [370]: http://commits.kde.org/kdenlive/062061f85062b09e922c576c366ea8775d444fa8
 [371]: http://commits.kde.org/kdenlive/98e8aab569e06f6619779b3df2581497a2d640e3
 [372]: http://commits.kde.org/kdenlive/ad3461b63f15051b87123f01dd7ce754c5b88109
 [373]: http://commits.kde.org/kdenlive/764479dab6091a16426253a3488a72060f7e025d
 [374]: http://commits.kde.org/kdenlive/df21885b49e769ebef3132a6d4e7e78695399dc5
 [375]: http://commits.kde.org/kdenlive/9e74baec19f93f6a91b51239594b37b026857632
 [376]: http://commits.kde.org/kdenlive/6ee9c78b1cbe308de9e0ef420d6a0ecf2ff3c1b5
 [377]: http://commits.kde.org/kdenlive/eca620cc01cba26ff6643c15412d5771a023a3c6
 [378]: http://commits.kde.org/kdenlive/e6411f416bd236a27c8d7047e9728a90974a5b7d
 [379]: http://commits.kde.org/kdenlive/aa9d6204bb63530d5658950beb60cc7e63294a20
 [380]: http://commits.kde.org/kdenlive/4b3a1b0b2d9c3e3d638110ce6c9d3a2a397e3f42
 [381]: http://commits.kde.org/kdenlive/dc2db9b3b9058352841ad139487208644d032488
 [382]: http://commits.kde.org/kdenlive/3606ad7ca950d7edf85b4971385653d938c4810e
 [383]: http://commits.kde.org/kdenlive/1b451836356a4b459b4392920dad607bcb9be069
 [384]: http://commits.kde.org/kdenlive/edcf8d3e95ce298c68208f790b67fac018f7a9d8
 [385]: http://commits.kde.org/kdenlive/a209af2bc4d55d7c573ffcafed5b98895d432714
 [386]: http://commits.kde.org/kdenlive/5187c72c2215223ee5f7b2a9828b233676ca76f2
 [387]: http://commits.kde.org/kdenlive/65d0770f27e4814e926c4a620c05efea79d52777
 [388]: http://commits.kde.org/kdenlive/167167ad28166a90ba71d4d9f690922037334a66
 [389]: http://commits.kde.org/kdenlive/6515101e44c55ad3ff0ec344537c9975643cdab7
 [390]: http://commits.kde.org/kdenlive/13d82ed1293839d6b821fda4b41149db6aac7c82
 [391]: http://commits.kde.org/kdenlive/e6eaac577afc01f08ab23e93f0d340ec0f00aae7
 [392]: http://commits.kde.org/kdenlive/0e1448d47938ca6b0b560f785e298c8b1ded7d1f
 [393]: http://commits.kde.org/kdenlive/aef9f2de7d8dea1a05dbcaafec797c04cca04ca6
 [394]: http://commits.kde.org/kdenlive/fc1f06115cf3f579a83eec4e4280a3bf2687c358
 [395]: http://commits.kde.org/kdenlive/878860d7c532064fe71ac291849f414f7ba31991
 [396]: http://commits.kde.org/kdenlive/b9fa9d613a8e3ef2a758dd4d7e9df0cb1c18921e
 [397]: http://commits.kde.org/kdenlive/900516da43871d8e1d74e4b4df664cee3935be22
 [398]: http://commits.kde.org/kdenlive/b138e207a7577bb2dee7b425b4f4d9107c4a4851
 [399]: http://commits.kde.org/kdenlive/7dd84cfab107bdf33e9b2cb52fb7ffdf78be2a2c
 [400]: http://commits.kde.org/kdenlive/3a65398eb477837e256e0581fc4e9577b640c4e6
 [401]: http://commits.kde.org/kdenlive/97c66ce88fa7b69b664f55a1a3be2f8176c4c5eb
 [402]: http://commits.kde.org/kdenlive/3095789a4b133a63f387d864bfa57942edaaec45
 [403]: http://commits.kde.org/kdenlive/d6d104816c33a9f916d33da15b4470488f514446
 [404]: http://commits.kde.org/kdenlive/4314c870a4df09ea701a93a34bc944a7565b8771
 [405]: http://commits.kde.org/kdenlive/203824f4202639219ac0bba919b0c3f723f2e57f
 [406]: http://commits.kde.org/kdenlive/9e83975291cba0ac92d3627a0d7663f88b478c23
 [407]: http://commits.kde.org/kdenlive/f5fa4ef391abdcee5a5acb579c54e36803ec20d9
 [408]: http://commits.kde.org/kdenlive/2f605450a4cb3041d19b1fe74a6ec2ef079a4557
 [409]: http://commits.kde.org/kdenlive/2a4fa72b756468f991333595a863d3660f597193
 [410]: http://commits.kde.org/kdenlive/3225eaad9d2a7ef167fe61f46aa56ba46011c0dc
 [411]: http://commits.kde.org/kdenlive/585e3a55faf3d13bd291a9ab7249f8e7f2df5426
 [412]: http://commits.kde.org/kdenlive/f4ad299c00b011f4d41ca529cc461336569a1de2
 [413]: http://commits.kde.org/kdenlive/ae80335f8043e3f708316192f60a684c8538c68c
 [414]: http://commits.kde.org/kdenlive/c76ae6b1214e12663127655a380f621fa86c99c0
 [415]: http://commits.kde.org/kdenlive/ba9ea12d2c3eb1ad838296017a9803f5c2c797db
 [416]: http://commits.kde.org/kdenlive/a391cdff4c6a2fe9b88a106282b77473159085dd
 [417]: http://commits.kde.org/kdenlive/882bc06657e2fc220f274bfdbaa358410970d4d0
 [418]: http://commits.kde.org/kdenlive/a4e7b25c2305b44624537cf4ffc482618cba19a2
 [419]: http://commits.kde.org/kdenlive/ad96891adc67c3b25a2e239d2232b2a211a6136c
 [420]: http://commits.kde.org/kdenlive/6d8451b5db8541abdf2ffab8c306ff53d7d764b4
 [421]: http://commits.kde.org/kdenlive/46650e6ab474c53260035c86e21772515c78850d
 [422]: http://commits.kde.org/kdenlive/11451821725df18b4494d14e4c9b5aae9bb7e84c
 [423]: http://commits.kde.org/kdenlive/c4d0a0a8e00772d2b2c341765eabef5e7465b8fa
 [424]: http://commits.kde.org/kdenlive/da71637dc8d8075f8eaad1ac3f7d927b17c7e631
 [425]: http://commits.kde.org/kdenlive/5a8ac122e86d5223ce1fbf16e138fc4972e1ce3a
 [426]: http://commits.kde.org/kdenlive/30aa792d872afd7f160c9bf1feb0780d21c12f7c
 [427]: http://commits.kde.org/kdenlive/013649ec31eb93eaf9c0b91a9e345ef3f55f4dbf
 [428]: http://commits.kde.org/kdenlive/66b12e2ea3ebdf406c848a6085ccc1f971cd98cc
 [429]: http://commits.kde.org/kdenlive/cfa820fac73c8b05b315610185dfa150489cfe3d
 [430]: http://commits.kde.org/kdenlive/4c0f1d9a0e871b7b43a8b36ddd052043f11247e1
 [431]: http://commits.kde.org/kdenlive/bda9cb53248c838ce68ffcdd4c21f85b2246f454
 [432]: http://commits.kde.org/kdenlive/9221b1c1ec86173d86444396c3be60981c5c5b3d
 [433]: http://commits.kde.org/kdenlive/d11bd4d88d608a857cf5e77c30ed4175b19600d6
 [434]: http://commits.kde.org/kdenlive/f3f8cdeb6d857b568b9b5371d4b0e127656cafd0
 [435]: http://commits.kde.org/kdenlive/2bf8cb462781efe28e2b83b339901bda161d10d2
 [436]: http://commits.kde.org/kdenlive/8dc7b55513c3015788a4964580c3e7b7ee142a2a
 [437]: http://commits.kde.org/kdenlive/fc4481ec22e21f064a4121b53a94d96a5c19a652
 [438]: http://commits.kde.org/kdenlive/3a87c8b4f33a82b6f24af95524746746729f28a4
 [439]: http://commits.kde.org/kdenlive/b5f06b30b55545819c11eb2427ef70c153dfa807
 [440]: http://commits.kde.org/kdenlive/270d302294ff262a8a28d2e61b44450405bbead9
 [441]: http://commits.kde.org/kdenlive/d14d442a57e928b48af3fd496736620d1e01fa0e
 [442]: http://commits.kde.org/kdenlive/bc5b723736bef31440c0cba421d0f563502bdc8d
 [443]: http://commits.kde.org/kdenlive/410daee2c1ce17d8b1dd41061b021120936c6bf6
 [444]: http://commits.kde.org/kdenlive/2eee77632e8fa7265ca4b839589e339c5dbce195
 [445]: http://commits.kde.org/kdenlive/4134de77cea6d2537106fe008e5afde72aef89f2
 [446]: http://commits.kde.org/kdenlive/7c45467a2c9654b2a5f468296fbb43b7b80b5c2d
 [447]: http://commits.kde.org/kdenlive/c7bb3b414fb1a2cfc1bd9d7b3034b8cd81a80155
 [448]: http://commits.kde.org/kdenlive/396af5cca9da19c4977e09ce9e7863f7bc70b94e
 [449]: http://commits.kde.org/kdenlive/96a6f883c2d36d6a8141e7d43a0ebe71138ac7d7
 [450]: http://commits.kde.org/kdenlive/9e33552021c4dad7f0640339f18d64afb2470371
 [451]: http://commits.kde.org/kdenlive/bbff11511d9ab594801ba6be0bd1dec7882f7038
 [452]: http://commits.kde.org/kdenlive/1fa06d61f1f018ebb876b0267a6e1b76db663098
 [453]: http://commits.kde.org/kdenlive/994afbbdf3cc3f22c1c47bb84c31dc74508e1ea9
 [454]: http://commits.kde.org/kdenlive/be2e8a1a57c87b8be924645c62a0c0d9fbe21499
 [455]: http://commits.kde.org/kdenlive/086008f62daf9bb9405143fa443d3c94f5458b3b
 [456]: https://bugs.kde.org/431676
 [457]: http://commits.kde.org/kdenlive/d4558cad294129c0c7581b72d776d9daa0dbfd1c
 [458]: http://commits.kde.org/kdenlive/26d744674934b0840ecb0c041cc6310bd18c6f52
 [459]: http://commits.kde.org/kdenlive/ebd573aaeafd26aa6e9ae8334a3e228f2f2ab1a8
 [460]: http://commits.kde.org/kdenlive/54bbc6ae9a14495da346d28ad890f9f95d93a288
 [461]: http://commits.kde.org/kdenlive/30067ab9ec12cfb0267a02ad23def2901bb4fe9e
 [462]: http://commits.kde.org/kdenlive/a4b4c526b25ec0d43bb473b62ea15762e855f60b
 [463]: http://commits.kde.org/kdenlive/8c73dd92f71075aae7b8456d1865244aee0e8298
 [464]: http://commits.kde.org/kdenlive/34ddc5a5c237ec88336bd65c27582980576a8563
 [465]: http://commits.kde.org/kdenlive/b4e2e0d759a3801f93e96f9013f32fb72e9b516d
 [466]: http://commits.kde.org/kdenlive/08254e68fde679f8e2b66c33ea22bbb0587f6caa
 [467]: http://commits.kde.org/kdenlive/90571f0885facb679ea2b1cfb119c05408b83156
 [468]: http://commits.kde.org/kdenlive/541d70022d7bf28395d38deeb1a2af2af65a0010
 [469]: http://commits.kde.org/kdenlive/4c03c9723b98df829d468989f522ad1a2cc04649
 [470]: http://commits.kde.org/kdenlive/888ba7bc1c67e0b56c1af14a98fa92ae126ed720
 [471]: http://commits.kde.org/kdenlive/d174a99843547b8aa1f4287031e84231d1b958fd
 [472]: http://commits.kde.org/kdenlive/aad2deedde7581979d3797fa2dba3b9c469d323e
 [473]: http://commits.kde.org/kdenlive/fb8d5554cbe088c5ba4370705d9f63308e7cd2f6
 [474]: http://commits.kde.org/kdenlive/dc2c87bfc90b1d57b821a998771d04234a9effd8
 [475]: http://commits.kde.org/kdenlive/25953f092d34509904006f054334648cc06dacf1
 [476]: http://commits.kde.org/kdenlive/b961dbc7d982ee840687ec7ab688c981660667bf
 [477]: http://commits.kde.org/kdenlive/eefecf92350acfb3f3f12e20a131951478abbc74
 [478]: http://commits.kde.org/kdenlive/48de2321bbc7e314625c9cfec6faa636ea53bb55
 [479]: http://commits.kde.org/kdenlive/f72ab410d46edc142566fac7ef705cfc2ace3b2f
 [480]: http://commits.kde.org/kdenlive/154f6aca128b6340d3a87d761adc683490e4160c
 [481]: http://commits.kde.org/kdenlive/f02069b2ea4812b073ef2751e1c9de1737f619f3
 [482]: http://commits.kde.org/kdenlive/9c28c2932727e271fba0f8fbbe5e5936009368d5
 [483]: http://commits.kde.org/kdenlive/8df0d1473d3f609ea2f8d5e5afb49960681782d5
 [484]: http://commits.kde.org/kdenlive/667b321511a176e5fc4010e137768bd7483ae779
 [485]: http://commits.kde.org/kdenlive/f87692bc69c964a1c2137b9e4928a4c36881a3f6
 [486]: http://commits.kde.org/kdenlive/856e3f2d1a8bf4616a6a9fe5d60d939136576071
 [487]: http://commits.kde.org/kdenlive/714e90d0049be7eabb7c6aa285d29b1ff1110e84
 [488]: http://commits.kde.org/kdenlive/2c6b5b18ee75c6821213c4a82939c18a2a89b3a7
 [489]: http://commits.kde.org/kdenlive/7e64ee1acc3f0bd52e51aee9e70438fec871aec9
 [490]: http://commits.kde.org/kdenlive/627ed593d64a39d8c6c6b58954614903299e5050
 [491]: http://commits.kde.org/kdenlive/5034e4e8bb565ee9f97d6b313a47119216053858
 [492]: http://commits.kde.org/kdenlive/07b8f09f1ca750daac9ff798e13fa6b18a96fd63
 [493]: http://commits.kde.org/kdenlive/8d41f1527ea29f3a1903b5682ed934e1fa74256c
 [494]: http://commits.kde.org/kdenlive/8a4b2f90d7341eddc21df84aa01d109e506b1749
 [495]: http://commits.kde.org/kdenlive/683d069095f81a1cfb10cdbc94525ef0edc0a4c2
 [496]: http://commits.kde.org/kdenlive/006bf392344ab744e4a7551476be20afed49e989
 [497]: http://commits.kde.org/kdenlive/a2e863db5c8f594e28287ef19358b623c8d132a2
 [498]: http://commits.kde.org/kdenlive/9dd66244c0a56195074ec8fec7f5002cc47cdf0d
 [499]: http://commits.kde.org/kdenlive/5027735d492bc186853ff7743607f7ad40d28f15
 [500]: http://commits.kde.org/kdenlive/fae156a816f43f31b2067774005978762100de1f
 [501]: http://commits.kde.org/kdenlive/7654f19f0e24b9be745f01b12a2169c8ff04b088
 [502]: https://bugs.kde.org/430843
 [503]: http://commits.kde.org/kdenlive/a621560ea2cd16e69af3d5f8bc31065197cc0556
 [504]: http://commits.kde.org/kdenlive/e2aca2a36468578bf39fe82c308d9da033229dc4
 [505]: http://commits.kde.org/kdenlive/c83f3cdbfa2ef41eb68c4e19edfebcc5cde8c24d
 [506]: http://commits.kde.org/kdenlive/b0daadb1b25d25208cba884e393fecd431366a3d
 [507]: http://commits.kde.org/kdenlive/7a8439f011a80ecffa87d6d72efca26ae0224b6c
 [508]: http://commits.kde.org/kdenlive/ee18a7527b5644c6d235f6e314fe56a716e56458
 [509]: http://commits.kde.org/kdenlive/0cc50e9a02c78501bb86a0a56a63e9c34bd6df66
 [510]: http://commits.kde.org/kdenlive/5bb035cd6b9eb3fde3d880823371d304f8f57879
 [511]: http://commits.kde.org/kdenlive/c07a55f7c96cd0e566680ce58e992596c68f1c4c
 [512]: http://commits.kde.org/kdenlive/8222199edd5a20f6f490ca13872b33aabdb3958e
 [513]: http://commits.kde.org/kdenlive/68c3d6b7d5b36ec10ec7690448c5cb377aba84e3
 [514]: http://commits.kde.org/kdenlive/e56674565c7514ae2aa4c022f5db523d22417649
 [515]: http://commits.kde.org/kdenlive/765ee784fd4b42f2da195f5ce56dc6adf827f5c9
 [516]: http://commits.kde.org/kdenlive/9a89c55d8ebbbb056bab2ff4cc226ba7ee0a355e
 [517]: http://commits.kde.org/kdenlive/71e73a0b9234bc9957328654a1d4915c8da7a171
 [518]: http://commits.kde.org/kdenlive/394e66009f95ddfc54893c2957eb0379b835d767
 [519]: http://commits.kde.org/kdenlive/e5a43efc0ed062227dfc0950fe25998cb75325e8
 [520]: http://commits.kde.org/kdenlive/c877a1f3727c8a08d7d90dd2e99be0cee6ab7501
 [521]: http://commits.kde.org/kdenlive/b4dc602ba7ea1fa4f816fa2353d9a1c704b191ea
 [522]: http://commits.kde.org/kdenlive/f2525c7246d0f917bdbec2d9ee0fdf89873f5703
 [523]: http://commits.kde.org/kdenlive/e0a6495cf8c92c5da61f7f8c371910547dd5d6fd
 [524]: http://commits.kde.org/kdenlive/b0f11e0128906c4074dd4d7e303cd951f585fe5e
 [525]: http://commits.kde.org/kdenlive/948845ecceb971ce5c2a885609cb09a3490df856
 [526]: http://commits.kde.org/kdenlive/470c06fcfadf6883308073e54d40bc8bed4fac4b
 [527]: http://commits.kde.org/kdenlive/e69a368e82160e5669003c593f18ae1ea6572272
 [528]: http://commits.kde.org/kdenlive/f3ae462f51facecd819dffd8c92edcef860b4171
 [529]: http://commits.kde.org/kdenlive/617fc88326aca381f58f39d6f5a31045a793a5b9
 [530]: http://commits.kde.org/kdenlive/e05c154a3e061156ae331f592c1dce3199daaeb1
 [531]: https://bugs.kde.org/429997
 [532]: http://commits.kde.org/kdenlive/1df0959bbf741b3347dbf37bac5a3abfb224670f
 [533]: http://commits.kde.org/kdenlive/3b1e01abd80c13e62e530edea465765c9778bc97
 [534]: http://commits.kde.org/kdenlive/b0ab640b1718611b0fef22b813ccbc1682c89e98
 [535]: http://commits.kde.org/kdenlive/167eaacf0f88fec8b263fd96cda365848ddde6a2
 [536]: http://commits.kde.org/kdenlive/5eeb81c6003193ae3a004b2f8b1093106d9484c9
 [537]: http://commits.kde.org/kdenlive/6faf8be11f1e698254abc83edd3a558a861fd119
 [538]: http://commits.kde.org/kdenlive/0ed472684c8d5c4deca326be337b47e813920d09
 [539]: http://commits.kde.org/kdenlive/216bf09cc52e7dcc0fda61e3f1fa546b4a048c90
 [540]: http://commits.kde.org/kdenlive/efa2294baf8a65f69bc5b2d542e8334ce3cf1d6e
 [541]: http://commits.kde.org/kdenlive/0a343f316a6b36d8c099ff2579fdb223aa7d4631
 [542]: http://commits.kde.org/kdenlive/5e4ebdd87423741a9b7a48fe1b1199a4d1b39120
 [543]: http://commits.kde.org/kdenlive/4ad663b74b7ff12716f8a7b41fe6b01f3a9dde3f
 [544]: http://commits.kde.org/kdenlive/59b166de2bbfe32649656f3d7bcde6ac87a38799
 [545]: http://commits.kde.org/kdenlive/9408ba3c1992ec4529e4e8a0dbd80b4de0c34add
 [546]: http://commits.kde.org/kdenlive/02e3fa953a28786b00a19436dd233bb97a0ba738
 [547]: http://commits.kde.org/kdenlive/a0d231ece85216e8e72f3fbab11f7b04ea063a49
 [548]: http://commits.kde.org/kdenlive/e81bd82c925ac142a62ec5aef9c2f246cd010bd5
 [549]: http://commits.kde.org/kdenlive/a3e0b15f21932f43084d16536fc62e9b9d02761e
 [550]: http://commits.kde.org/kdenlive/2f9f6380b32188bbfb6da12b9c75a43913b4dc7e
 [551]: http://commits.kde.org/kdenlive/5a9426ae009ef2f046fbf7216e66009fa8c5a24e
 [552]: http://commits.kde.org/kdenlive/e422797a20296112dd2a656d2e1a46a301a5e69b
 [553]: http://commits.kde.org/kdenlive/a12b054f80bdaf019f2746da8b193655c6d48ae6
 [554]: http://commits.kde.org/kdenlive/94c148c6285323dde3c95f0730a92d37ea547ba6
 [555]: http://commits.kde.org/kdenlive/511b75ff76a9d99ffbbe411f80bfc6755965453c
 [556]: https://bugs.kde.org/430262
 [557]: http://commits.kde.org/kdenlive/bc1041950a15209dee0f5d94313fdd144023eef7
 [558]: http://commits.kde.org/kdenlive/b6f8ec162cfc78d781a1c224f2036945a08d826a
 [559]: http://commits.kde.org/kdenlive/3a5ebe7256e9b24ab040e0d850f92f595f123fd3
 [560]: http://commits.kde.org/kdenlive/d5367bd136fcbd20b0fcd503cf9fb6a87179718a
 [561]: http://commits.kde.org/kdenlive/a6e04898cf4dcfa563196da866199ae58eede471
 [562]: https://bugs.kde.org/430122
 [563]: http://commits.kde.org/kdenlive/9a1da839c566123ebd17cbb983ac5e72cef5ef05
 [564]: http://commits.kde.org/kdenlive/b01d60d410cd2d88a0206b91b1e69291bfca08cf
 [565]: http://commits.kde.org/kdenlive/391451136af677f9347f447420175ed9f1f765b7
 [566]: http://commits.kde.org/kdenlive/80ba7f32a34c73b3dbebc8f19dad5d681d3c4628
 [567]: http://commits.kde.org/kdenlive/2093159bb6f0b6cac0fcbe4b496448d672404981
 [568]: http://commits.kde.org/kdenlive/5238366f0bdc07bee10dc7dfba8a22655ce67304
 [569]: http://commits.kde.org/kdenlive/4b5a6031b653e86bf8792c242d344340f8171209
 [570]: http://commits.kde.org/kdenlive/4b35cfafad3713283102951bf551bc61c9bbe98e
 [571]: http://commits.kde.org/kdenlive/1f798a9017df6b86490b223fc3c8269286f1d3dc
 [572]: http://commits.kde.org/kdenlive/ea1b69885339f07abf0d5eda1f44201ad6b8da2f
 [573]: http://commits.kde.org/kdenlive/25810e4f87321ebeaa92310635b8889d29b75a4e
 [574]: http://commits.kde.org/kdenlive/6fb694007b7bf73e5516d115b8571f0ba29eb0b1
 [575]: http://commits.kde.org/kdenlive/a315ab895af68612453b1ee1cb1683a84a331509
 [576]: http://commits.kde.org/kdenlive/cbc663702aa0be7c0ddc22a0f39c38f4a4bf6cca
 [577]: http://commits.kde.org/kdenlive/5dd33af2de6f424c3efee157c318f4dd932f37f8
 [578]: http://commits.kde.org/kdenlive/a8c6635e4536b501be33473281fb2a645c411d9c
 [579]: http://commits.kde.org/kdenlive/460d58d2e59d05f9ff1e6fc8cd1a9207bce44852
 [580]: http://commits.kde.org/kdenlive/4b7dd17e158911c1d586ef4455ee57d1010d95a3
 [581]: http://commits.kde.org/kdenlive/54cf637dc35227803f6c839ba761ab90bce35c88
 [582]: http://commits.kde.org/kdenlive/8bc21ec4291d1ad9df295bfc3a01ba9a310348df
 [583]: https://bugs.kde.org/430035
 [584]: http://commits.kde.org/kdenlive/2f0bb85f1732d07f0ae7bc673f768a08235a8326
 [585]: http://commits.kde.org/kdenlive/81768b4d074168d6b8873b44d4e4c75e801640ce
 [586]: http://commits.kde.org/kdenlive/192b18460dd22153a65cae358a803beac667b671
 [587]: http://commits.kde.org/kdenlive/966d2cacd728007859606a624ca40ae95bb0cc39
 [588]: http://commits.kde.org/kdenlive/75930b3651ddd226e0b735a56a0d9a0d8064b5ff
 [589]: http://commits.kde.org/kdenlive/5cc266c30e1bf06bebc1163c25a8e30a73d576c3
 [590]: http://commits.kde.org/kdenlive/2c02637c4a33713bdd21d72653a96aad1262d362
 [591]: http://commits.kde.org/kdenlive/6efd95118d4a24767916a70aa1ff8aa5a5c0e7dd
 [592]: http://commits.kde.org/kdenlive/783f788e29235f1cd0cad2b72015a4c16887665c
