---
author: Farid Abdelnour
date: 2021-04-27T12:00:20+00:00
aliases:
- /en/2021/04/kdenlive-21-04-released/
- /fr/2021/04/kdenlive-21-04/
---

The Kdenlive team is happy to announce the release of version 21.04 with lots of interface and usability improvements and many bug fixes. This version also comes with new Typewriter and Strobing effects as well as Effect Zones, which give you the ability to apply effects to track and/or timeline regions. There is also new Speech-to-Text feature to automatically transcribe audio to text and a Media Browser widget to easily browse and add your source material to your project. The Online Resources has been converted into a widget and buffed with more media providers. Not to mention tons of under the hood polishing in preparation for Qt6 and MLT7.

We would like to thank the contributions of Rafal Lalik (Typewriter effect), Vivek Yadav (Media Browser), Martin Sandsmark (Alpha Strobe effect) and Julius Künzel (Online Resources), and also welcome Julius as a core team member. While the devs were busy hacking the bug squashing team managed to close these past months more than 500 bugs in the tracker.

![](bugs-stats.png)

## Speech to text

The new _Speech to Text_ feature allows to automatically transcribe any audio to text using the [Vosk][1] speech recognition toolkit. Currently there is support for 17 languages and dialects using the [official models][2]. Download the model and add it as a dictionary in the settings or simply drag and drop it.

![](speech2text.gif)

## Interface and Usability Improvements {.western}

This cycle received a good amount of interface and usability improvements.

### Zoombars
Besides the availability of zoombars in the monitor and keyframe scroll bars, zoombars are now available in the timeline as well. You can easily zoom in/out in the timeline by dragging the edges of the timeline scrollbar. (Vertical zoombars coming soon.)

![](zoom-bar.gif)

### Key binding information
Key binding info has been added on the left while context item information has been moved to the right of the status bar.

![](context-and-keybinds.gif)

### Improved timeline visuals

The timeline got a visual overhaul with more and better looking guides/marker colors, the guides have been moved above the timeline ruler while preview and zone bars have been moved below.

![](timeline-overhaul.png "Before (above) and after (below)")

### New Media Browser

The new Media Browser allows you to easily navigate through your file system and add clips directly to the Bin or Timeline. You can enable it from View menu.

![](media-browser.gif)

## Improved Keyframe panel

The effect's keyframe panel has new icons, improved keyframe grabbing and new functions like:

* Move a selected keyframe to cursor
  ![](move-kf-to-cursor.gif)
* Duplicate a selected keyframe
  ![](duplicate-keyframe.gif)
* Apply a value to selected keyframe(s)
  ![](apply-value-to-selected-kf.gif)
* Select keyframes with CTRL + click
  ![](kf-ctrl-select.gif)
* Rubber select select keyframes with SHIFT + click
  ![](kf-rubber-select.gif)
* Move multiple keyframes at once
  ![](multiple-kf-move.gif)


## Effects

Besides the new shiny features a lot of enhancements have been added as well like the ability to delete all effects in selected clips, ability to download LUTs directly from the interface, added Drop Area to “Master” button to drop effects to it and polishing the rotoscoping monitor overlay to name a few.

### Typewriter

The beloved typewriter effect is back with a vengeance with 3 animation modes.

* Animation by character
  ![](by-character.gif)
* Animation by word
  ![](by-word.gif)
* Animation by line
  ![](by-line.gif)


### Alpha Strobe

The Alpha Strobe effect can be applied to text, images or videos.

* Effect applied to a text clip
  ![](STROBE.gif)
* Effect applied to a video track
  ![](alpha-strobe-image-video3.gif)

## Effect Zones

The new Effect Zones allow you to apply effects to specific regions of tracks or the timeline. Zones can be set from the effect zone bar in the timeline or from the interface in the effect panel.

### Track Effect Zone

![](track-effect-zone.gif) 

### Master Effect Zone 

![](timeline-effect-zone.gif)

## Online Resources

The new online resources widget features more source footage providers such as [Pixabay][4] and [Pexels][5] besides the already available [Freesound][6] and [Internet Archive][7]. Other possible providers are being considered, see [here][8] for more details.

![](online-resources-widget.gif)

It is important to give credit of the downloaded sources so we&#8217;ve added an option to directly import the license attribution as a project note. 

![](copyright.png) 

![](license-text-notes.png)

## Other noteworthy fixes

  * Add AV1 render profile.
  * Add “unused clip” filter in Project Bin.
  * Add channel selection to audio waveform filter.
  * Add ITU 2020 colorspace in clip properties.
  * Re-enable audio playback on reverse speed.
  * Improved Flatpak support.
  * Allow keyboard grab of subtitles.
  * Treat GIFs as video, not as image.
  * Fix many compile warnings and prepare for Qt6.
  * Fix wipes for slideshow clips.
  * Alpha shapes: allow going outside screen.

 [1]: https://alphacephei.com/vosk/
 [2]: https://alphacephei.com/vosk/models
 [3]: https://userbase.kde.org/Kdenlive/Manual/Timeline/Editing#Speech_to_text
 [4]: https://pixabay.com/
 [5]: https://www.pexels.com/
 [6]: https://freesound.org/
 [7]: https://archive.org/
 [8]: https://invent.kde.org/multimedia/kdenlive/-/issues/918

