
  * Don&#8217;t store duplicates for modified timeline uuid. [Commit.][5]
  * Fix recent regression (sequence clip duration not updated). [Commit.][6]
  * Clear undo history on sequence close as undoing a sequence close leads to crashes. [Commit.][7]
  * Correctly remember sequence properties (like guides) after closing sequence. [Commit.][8]
  * Fix various sequence issues (incorrect length limit on load, possible corruption on close/reopen). [Commit.][9]
  * Do our best to recover 23.04.0 corrupted project files. [Commit.][10] See bug [#469217][11]
  * Try to fix AppImage launching external app. [Commit.][12] See bug [#468935][13]
  * Get rid of the space eating info message in Motion Tracker. [Commit.][14]
  * Fix Defish range for recently introduced parameters. [Commit.][15] Fixes bug [#469390][16]
  * Fix rotation on proxy formats that don&#8217;t support the rotate flag. [Commit.][17]
  * Fix animated color parameter alpha broken. [Commit.][18] Fixes bug [#469155][19]
  * Fix 23.04.0 corrupted files on opening. [Commit.][20] See bug [#469217][11]
  * Fix another major source of project corruption. [Commit.][21]
  * Don&#8217;t attempt to move external proxy clips. [Commit.][22] Fixes bug [#468998][23]
  * Fix crash on unconfigured speech engine. [Commit.][24] Fixes bug [#469201][25]
  * Fix VOSK model hidden from auto subtitle dialog. [Commit.][26] Fixes bug [#469230][27]
  * Fix vaapi timeline preview profile. [Commit.][28] See bug [#469251][29]
  * Fix effects with filter task (motion tracker, normalize), re-add a non animated color property. [Commit.][30]
  * Switch test videos to mpg for CI. [Commit.][31]
  * Fix project corruption on opening, add test to prevent from happening again. [Commit.][32] See bug [#468962][33]
  * Fix concurrency crash in thumbnails. [Commit.][34]
  * Color wheel: highlight active slider, fix mouse wheel conflicts. [Commit.][35]
  * More fixes for luma lift gain color wheel (fix dragging outside wheel) and improved cursor feedback. [Commit.][36]
  * Various fixes for luma lift gain color wheel and slider. [Commit.][37]
  * Ensure Shape alpha resource are included in archived project. [Commit.][38]
  * Check missing filter assets on document open (LUT and Shape). [Commit.][39]
  * Fix various bugs and crashes on sequence close and undo create sequence from selection. [Commit.][40]
  * Fix temporary data check showing confusing widget. [Commit.][41]
  * Fix render profiles with no arguments (like GIF Hq). [Commit.][42]
  * Fix images embeded in titles incorrect path on extract. [Commit.][43]
  * Wait before all data is copied before re-opening project when using project folder for cache data. [Commit.][44]
  * Minor ui improvement for clip monitor jobs overlay. [Commit.][45]
  * Try to fix tests. [Commit.][46]
  * Don&#8217;t show unnecessary warning. [Commit.][47]
  * Ensure the mute\_on\_pause property is removed from older project files. [Commit.][48]
  * Fix clip properties default rotation and aspect ratio detection, display the tracks count for sequence clips. [Commit.][49]

 [5]: http://commits.kde.org/kdenlive/22204ff34d43345d4adbabe7b9f3b6b8e4d42067
 [6]: http://commits.kde.org/kdenlive/ed72a7ffa89c7d2c5ced22032760e25784d11b4c
 [7]: http://commits.kde.org/kdenlive/b1b931f11d1808c95c2757ec7703dcaa1b9ad2e8
 [8]: http://commits.kde.org/kdenlive/f074e4a872785a0c9b5e2732d689cc97d202b64d
 [9]: http://commits.kde.org/kdenlive/121d5f3db6b9717608f723d5eef404f7b425f4c5
 [10]: http://commits.kde.org/kdenlive/6e4433cff01d775251794425d34b531ae3522039
 [11]: https://bugs.kde.org/469217
 [12]: http://commits.kde.org/kdenlive/a8f21ac77871f9ff7295dae650b118f5b4e0ef39
 [13]: https://bugs.kde.org/468935
 [14]: http://commits.kde.org/kdenlive/4e0da0d7d470598358d6c91e40ffcc8fca46ae9b
 [15]: http://commits.kde.org/kdenlive/b91cbdb048a0f8dc0e21874940f0d00e9110060c
 [16]: https://bugs.kde.org/469390
 [17]: http://commits.kde.org/kdenlive/6b7de6963f659b80c3fe03df3008e6145505ba05
 [18]: http://commits.kde.org/kdenlive/98173f3043dafbb75d5b3c0d3c1027027d5bd65e
 [19]: https://bugs.kde.org/469155
 [20]: http://commits.kde.org/kdenlive/2eecb82e109949dbeb8137b4b5f83ac30cd65438
 [21]: http://commits.kde.org/kdenlive/b382ad3a3b010564127ac7c33823fc98ff580e0c
 [22]: http://commits.kde.org/kdenlive/ff7f1dba304f8afe2316bdc953491188844ec2ca
 [23]: https://bugs.kde.org/468998
 [24]: http://commits.kde.org/kdenlive/2832748ea34e279fd1aca8e602e4ad80f850078c
 [25]: https://bugs.kde.org/469201
 [26]: http://commits.kde.org/kdenlive/779d886f88a47847f3ec8ea7a70d12efbd76d2bc
 [27]: https://bugs.kde.org/469230
 [28]: http://commits.kde.org/kdenlive/674b190db4a8f6446dfff8a554091ab98135bcab
 [29]: https://bugs.kde.org/469251
 [30]: http://commits.kde.org/kdenlive/c63c01c6d67e3ef8183e0c044696341c8bd1216d
 [31]: http://commits.kde.org/kdenlive/29e659624f3bdcbfa33f90f360d07a8559dd5b37
 [32]: http://commits.kde.org/kdenlive/364e23f6d1ba104e40623297b82a0ee873942925
 [33]: https://bugs.kde.org/468962
 [34]: http://commits.kde.org/kdenlive/a8cb8a1cd61e47482155fb780de4d5a50778f8b2
 [35]: http://commits.kde.org/kdenlive/6568db0f23dc2bea08c64c8fd95b66debd88bc30
 [36]: http://commits.kde.org/kdenlive/aa43937b827d9c7cec0c34ec6d928af073ec6351
 [37]: http://commits.kde.org/kdenlive/99e80f993f1aceb21228713399976d9079d174e5
 [38]: http://commits.kde.org/kdenlive/030c00886d76d2f9a46e3b74a3c8bbd83a8915e3
 [39]: http://commits.kde.org/kdenlive/251e5021a401e5ba57f9b88b2150406cd259a40b
 [40]: http://commits.kde.org/kdenlive/5d34dd12651ee63e3f38367a5b1621115755dfd8
 [41]: http://commits.kde.org/kdenlive/6f901908c415e7f9680f2383f30fc7cd21261e76
 [42]: http://commits.kde.org/kdenlive/342d7c59c60f8d0927db646147bd33e06325b956
 [43]: http://commits.kde.org/kdenlive/90859b340818d6497c11bc87cfe8b391e8d44483
 [44]: http://commits.kde.org/kdenlive/bdd25855734ded607d5b0bfe075519b3df45d4a0
 [45]: http://commits.kde.org/kdenlive/21baec8ad2d181ca6a105063e93d9d4fabe1edcd
 [46]: http://commits.kde.org/kdenlive/e43c23c6e17d3b27fe7ff7d8c3d62a41a0a758b9
 [47]: http://commits.kde.org/kdenlive/31d898453a861cecd80719e2a1dfd95c9dc7830b
 [48]: http://commits.kde.org/kdenlive/dcb381b701ed5eebeffbd38c357535ce59c2e3f1
 [49]: http://commits.kde.org/kdenlive/3fe7257ce02869963b1116787a8bbf804b29a965
