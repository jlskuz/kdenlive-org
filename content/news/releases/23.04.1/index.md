---
author: Jean-Baptiste Mardelle
date: 2023-05-13T11:56:59+00:00
aliases:
- /en/2023/05/kdenlive-23-04-1-released/
- /fr/2023/05/version-intermediaire-23-04-1/
- /it/2023/05/kdenlive-23-04-1-disponibile/
- /de/2023/05/kdenlive-23-04-1/
---

Kdenlive 23.04.1 has just been released, and all users of the 23.04.0 version are strongly encouraged to upgrade.

The 23.04.0 release of Kdenlive introduced major changes with the support of nested timeline. However, several issues leading to crashes and project corruption went unnoticed and affected this release.

This should now be fixed in Kdenlive 23.04.1. While we have some automated testing, and continue improving it, it is difficult to test all configurations and cases on such a large codebase with our small team. We are however planning to improve in this area!

It is also important to note that Kdenlive has several automatic backup mechanisms, so even in such cases, data loss should be minimal (see our [documentation for more details][1]).

If you want to help us, don't hesitate to [get in touch, report bugs, test the development version][2], contribute to [the documentation][3] or [donate][4] if you feel like it!

Version 23.04.1 also fixes many other bugs, see the full log below:


 [1]: https://docs.kdenlive.org/en/importing_and_assets_management/projects_and_files/project.html
 [2]: https://kdenlive.org/en/developers-welcome/
 [3]: https://docs.kdenlive.org/en/
 [4]: https://kdenlive.org/en/fund/
