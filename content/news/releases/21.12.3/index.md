---
author: Farid Abdelnour
date: 2022-03-07T13:40:48+00:00
aliases:
- /en/2022/03/kdenlive-21-12-3-released/
- /fr/2022/03/kdenlive-21-12-3/
- /it/2022/03/kdenlive-21-12-3-3/
- /de/2022/03/kdenlive-21-12-3-2/
---
The last maintenance release of the 21.12 cycle is out with lots of usability polishing of keyframes, subtitles and proxy clips. Audio thumbnails on high zoom levels got a major performance optimization. This version also fixes 5 crashes including Wayland layout switching, time remapping module among others.

With this release we switched our AppImage building to [KDE Craft][1] that is already in use for our Windows and macOS builds. With this step our packages become more consistent in regard of bundled dependency versions across all platforms. Also the maintenance becomes less time consuming giving developers more time to focus on coding rather than packaging. We encourage users to reset their configurations to avoid any possible issues by going to menu **Help** -> **Reset Configuration**.

If you encounter any issues please [report them][2] and don't forget to checkout the [trouble shooting][3] tips.

 [1]: https://community.kde.org/Craft
 [2]: https://kdenlive.org/bug-triaging/
 [3]: https://docs.kdenlive.org/troubleshooting.html
