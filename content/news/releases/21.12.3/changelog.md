  * Fix tests and resize issue. [Commit.][4]
  * [Effect Keyframes] Fix “apply current value” uses wrong position. [Commit.][5]
  * [Effect Keyframes] Fix “copy current value to selected” crash. [Commit.][6]
  * [Effect Keyframes] Fix wrong keyframes shown as selected. [Commit.][7]
  * Fix tags corrupting audio/video icons in bin. [Commit.][8]
  * Fix audio thumb speed not correctly initialized (broke monitor thumbs). [Commit.][9]
  * Fix audio thumbs for clips with speed effect. [Commit.][10]
  * Fix crash cutting a grouped subtitle. [Commit.][11]
  * Fix 1 pixel offset at some zoom factors. [Commit.][12]
  * Correctly update add/remove keyframe button on undo/redo and active keyframe on speed change. [Commit.][13]
  * Fix possible crash in extract zone. [Commit.][14]
  * Fix thumbnails for playlist clips having a different resolution than project profile. [Commit.][15]
  * Fix crash updating speed before/after in time remap. [Commit.][16]
  * Fix proxy clips not correctly disabled on rendering. [Commit.][17]
  * Fix sometimes cannot resize clip when there is a 1 frame gap. [Commit.][18]
  * Various fixes for remove space in subtitle track. [Commit.][19]
  * Fix same track transitions sometimes broken by clip resize. [Commit.][20]
  * Fix 1 frame offset in subtitles when removing space. [Commit.][21]
  * Show clip labels as soon as there is one letter width. [Commit.][22]
  * Fix marker thumbnail size. [Commit.][23]
  * Don’t show clip thumbs on when clip is too small (<16 pixels). [Commit.][24]
  * Missing change from last commit (fix remove space). [Commit.][25]
  * Fix “remove space” not working on 1 frame space. [Commit.][26]
  * Only create proxy clips automatically if requested. [Commit.][27]
  * Fix audio wave for non stereo clips. [Commit.][28]
  * Fix qml binding loop warning. [Commit.][29]
  * Fix clip thumbnails extending past clip length. [Commit.][30]
  * Fix adjust to original size using proxy resolution. [Commit.][31]
  * Try to fix ghost icons on Windows. [Commit.][32]
  * Major speedup in audio thumbs drawing on high zoom levels. [Commit.][33]
  * Fix clip name not scrolling anymore. [Commit.][34]
  * Fix unusable bin icon for audio/video drag. [Commit.][35]
  * Fix Wayland crash on layout switch. [Commit.][36]
  * Minor optimization for audio thumbs drawing. [Commit.][37]
  * Fix .ass subtitle files not correctly read. [Commit.][38]
  * Ensure processes are in the path before starting an executable. [Commit.][39]
  * Fix timeline keyframes sometimes disappearing from view. [Commit.][40]
  * Fix wrong comparison of current settings and settings stored in the project settings dialog. [Commit.][41]
  * Fix again VPx quality: use constrained quality (bitrate>0). [Commit.][42]
  * [Spot Remover effect] Add xml ui to fix initalization (and other minor. [Commit.][43]
  * [Extract Frame] Fix wrong frame exported when using source resolution. [Commit.][44]
  
 [4]: http://commits.kde.org/kdenlive/b5eee433173c9c9646a0a17ef48d48e797870442
 [5]: http://commits.kde.org/kdenlive/dbf9e4e6807d1afc9e690b30f9a33b0c461a2df3
 [6]: http://commits.kde.org/kdenlive/db588a07dc22f7de3386d5ee81c5e5c1fa0d77bd
 [7]: http://commits.kde.org/kdenlive/b041e67f41ea81975455fe1bfe8a5a6fa9fb445a
 [8]: http://commits.kde.org/kdenlive/44a0501814ffab8dbd70c5e20df63b8e47a81003
 [9]: http://commits.kde.org/kdenlive/aec144dedf885ba4d8a9b70dc248851223898c10
 [10]: http://commits.kde.org/kdenlive/ef8d2637f0f2c90b59d2c59e51a455fc86a1fbf9
 [11]: http://commits.kde.org/kdenlive/67eaa5816c42eb5dc9e5e84576e0ae92192de76d
 [12]: http://commits.kde.org/kdenlive/cef3b29a2974ffa912f96582027895bd8b7eda12
 [13]: http://commits.kde.org/kdenlive/664c4361365ccd669fdea0c59f7b3c85789ed5b3
 [14]: http://commits.kde.org/kdenlive/79f8e687449ca9518f2bd345afd766c28eee164a
 [15]: http://commits.kde.org/kdenlive/2a4ab13057f094017e32ca8bbec075ac4df451a4
 [16]: http://commits.kde.org/kdenlive/406a48b16cdb226473a90b973fa2b60d30e290d5
 [17]: http://commits.kde.org/kdenlive/45c25b882a678e284996a7251c26eb867de40635
 [18]: http://commits.kde.org/kdenlive/4032344f54c923afc7fdcea61aa44f5c3509c47c
 [19]: http://commits.kde.org/kdenlive/a75b30c06421b7c4f31428794f9dc1cb609cb6e5
 [20]: http://commits.kde.org/kdenlive/9b5dc7e8f83ece65e9db8a72483e9c39cc3e21a1
 [21]: http://commits.kde.org/kdenlive/36809e3bd57c18d3f2e3478bf153b2a219bb90f8
 [22]: http://commits.kde.org/kdenlive/1093294c953e6a9cf46e31f37550c3dc7d7bc47d
 [23]: http://commits.kde.org/kdenlive/6aa79cbc84a80b1f86bd85801bdc981a9f26d092
 [24]: http://commits.kde.org/kdenlive/c2262e4f0b9da6773bb8676b294d520344532032
 [25]: http://commits.kde.org/kdenlive/11507b589243aaf3799f2a00450cd97c95cc87f3
 [26]: http://commits.kde.org/kdenlive/6c34acb20be6481cbefad8f91c0401ca677f1426
 [27]: http://commits.kde.org/kdenlive/add919c7c33ceb6f1f2aed02d70389fe864c7fc6
 [28]: http://commits.kde.org/kdenlive/d8170f0031446a872b934a962b6e5dc16cd5ce09
 [29]: http://commits.kde.org/kdenlive/817e35be1b4f560a3e6867dcefa86281fb50d86f
 [30]: http://commits.kde.org/kdenlive/6ca9ae64b6715f811598f064c3eda76e331d5d5c
 [31]: http://commits.kde.org/kdenlive/9d19863e2fd7cc672f178b801125f2b7e26ac841
 [32]: http://commits.kde.org/kdenlive/68e0cb402d1c2802f1edb3e203d7d9e2fa338005
 [33]: http://commits.kde.org/kdenlive/f43d851218ca6e9d5eb0617d8162d81590326c89
 [34]: http://commits.kde.org/kdenlive/03e2ca4590e443e3d441e0dba81c8fc63e9eaab6
 [35]: http://commits.kde.org/kdenlive/b7dd4819012f37ce9e3f1728bb8439107a6bbeb0
 [36]: http://commits.kde.org/kdenlive/1d5847a4ffd6f330a65187723d0d826dacb71dce
 [37]: http://commits.kde.org/kdenlive/b4911b0d4f99789d97cd711c9623b14f3d23fa09
 [38]: http://commits.kde.org/kdenlive/24e12eba52cfe7050078dea60426f64001bc3cf5
 [39]: http://commits.kde.org/kdenlive/de580556454818846a064a8de8f2016d726cd636
 [40]: http://commits.kde.org/kdenlive/a582fd66d71e2a55351fab5eab26f8e9bae69d6e
 [41]: http://commits.kde.org/kdenlive/2c5a5e5812de566f3b39a0e935a7d3fad17b8b9e
 [42]: http://commits.kde.org/kdenlive/086ab6b24d10cbb16a7f268892c922cd1b4de886
 [43]: http://commits.kde.org/kdenlive/40e3de128bd6a3104cf93b996a91dd2af6578cca
 [44]: http://commits.kde.org/kdenlive/080da9380775178cc57efa6fd170992fb4fa1fa0
