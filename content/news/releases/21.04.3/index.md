---
title: Kdenlive 21.04.3
author: Farid Abdelnour
date: 2021-07-08T18:50:53+00:00
aliases:
- /en/2021/07/kdenlive-21-04-3/
- /fr/2021/07/kdenlive-21-04-3-4/
- /it/2021/07/kdenlive-21-04-3-3/
- /de/2021/07/kdenlive-21-04-3-2/
---

The last maintenance release of the 21.04 series is out with improvements to same track transitions, improved Wayland support, as well as  fixing issues with rotoscoping and the speech to text module. This version also adds support for the WebP image format. _Due to technical issues there won't be a Windows version for this release._
