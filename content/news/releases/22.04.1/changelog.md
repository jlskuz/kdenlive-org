  * Add ‘reverse’ parameter to transition ‘mix’. [Commit.][1]
  * Fix custom effect type sometimes incorrect. [Commit.][2]
  * Fix drag incorrectly terminating in icon view. [Commit.][3]
  * Fix freeze cause by incorrect duplicate entry in thumbnail cache. [Commit.][4]
  * Fix crash trying to drag in empty space in Bin icon view. [Commit.][5]
  * Update kdenliveeffectscategory.rc new mlt’s box_blur added to the ‘Blur and Sharpen’ category. [Commit.][6]
  * Update CMakeLists.txt adding the new mlt’s Box_Blur. [Commit.][7]
  * Add new mlt’s Box_Blur ui. It was not working with the automatic one. [Commit.][8]
  * Update secondary\_color\_correction.xml fixing Transparency default value error. [Commit.][9]
  * Fix titler text alignment. [Commit.][10]
  * Fix potential deadlock, maybe related to #1380. [Commit.][11]
  * Small refactoring of cache get thumbnail. [Commit.][12]
  * Fix timeline preview failing when creating a new project. [Commit.][13]
  * Timeline preview profiles &#8211; remove unused audio parameters, fix interlaced nvenc. [Commit.][14]
  * Another set of minor improvements for monitor audio level. [Commit.][15]
  * Minor fix in audio levels look. [Commit.][16]
  * Ensure all color clips use the RGBA format. [Commit.][17]
  * Show dB in mixer tooltip. [Commit.][18]
  * Fix audio levels showing incorrect values, and not impacted by master effects. [Commit.][19]

 [1]: http://commits.kde.org/kdenlive/f7bc263e620657957726f5887a07ab913525a31a
 [2]: http://commits.kde.org/kdenlive/89cdeb163db3392ce5cbc6094974e8f4b83c11b2
 [3]: http://commits.kde.org/kdenlive/c6f537bea43ec6fb88153752dd8c9ccf106f66e3
 [4]: http://commits.kde.org/kdenlive/077f2f290a4f42ca7bacae1cc05c87db16c3f35c
 [5]: http://commits.kde.org/kdenlive/ae1e79a176067ed3f78ff473fd5063972c26f1d2
 [6]: http://commits.kde.org/kdenlive/7431f170cefc5424a5bf21fc2188161d18a6e266
 [7]: http://commits.kde.org/kdenlive/02cd900aa5e72ed17f13585434aaa1576fa87dd2
 [8]: http://commits.kde.org/kdenlive/42c8bd8c555acd470563cfcab94197112857f1d7
 [9]: http://commits.kde.org/kdenlive/6ba4c16f48d9ef64c26ea2c2744c066fee52e1ef
 [10]: http://commits.kde.org/kdenlive/4685d1353cf0b098049cdf31828720241c9f026a
 [11]: http://commits.kde.org/kdenlive/3c59d78541e124c4a639537bbe1f696ebd563d40
 [12]: http://commits.kde.org/kdenlive/a61a8f94657883e781e405e6db30a9b2a25b7a6c
 [13]: http://commits.kde.org/kdenlive/9f51aeb7ca86b074284782accfb993c626aa1707
 [14]: http://commits.kde.org/kdenlive/e725bcfdcddda0753232fef78e87636c3865c881
 [15]: http://commits.kde.org/kdenlive/18f77f07afb37011473e3fef211d42ba6434d648
 [16]: http://commits.kde.org/kdenlive/1ba4dedcfe7d59504815d5c237c4605586d69ed4
 [17]: http://commits.kde.org/kdenlive/8ab8f8e53e1935190521470ba306bf0c6b664352
 [18]: http://commits.kde.org/kdenlive/a5690290305ac7c096b8b3a3bdd49745913d813a
 [19]: http://commits.kde.org/kdenlive/637661db295e18f623f439b090e384f34fb29100
