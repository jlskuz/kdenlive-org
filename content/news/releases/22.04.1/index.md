---
author: Farid Abdelnour
date: 2022-05-23T11:13:44+00:00
aliases:
- /en/2022/05/kdenlive-22-04-1-released/
- /fr/2022/05/kdenlive-22-04-1-3/
- /it/2022/05/kdenlive-22-04-1/
- /de/2022/05/kdenlive-22-04-1-2/
---
The first maintenance release of the 22.04 series is out with two out-of-the-box effect templates: _Secondary Color Correction_ and _Shut-off_ as well as a new _Box Blur_ filter. This version fixes incorrect levels displayed in the audio mixer, timeline preview rendering, thumbnail caching and text alignment in the Titler. There is also a _reverse_ option in same track transitions.
