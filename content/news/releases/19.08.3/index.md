---
title: Kdenlive 19.08.3 is out
author: Farid Abdelnour
date: 2019-11-12T12:17:06+00:00
aliases:
- /en/2019/11/19-08-3-is-out/
- /fr/2019/11/la-version-18-08-3-de-kdenlive-est-sortie/
- /it/2019/11/e-uscito-kdenlive-19-08-3/
- /de/2019/11/kdenlive-19-08-3-ist-freigegeben/
---

The last minor release of the 19.08 series is out with a fair amount of usability fixes while preparations are underway for the next major version. The highlights include an audio mixer, improved effects UI and some performance optimizations. Grab the nightly AppImage builds, give it a spin and report any issues.
