---
date: 2004-03-15
author: Jason Wood
---

The latest stable version of Kdenlive, 0.2.4, was released on the 15th of March, 2004. Get it here! We have the pleasure to announce the 0.2.4 releases of both Kdenlive and PIAVE. This primarily a bug-fix release over the previous 0.2.3 release, although several new features have been added.

New Features in 0.2.4:

* Italian Translation by Daniele Medri
* Initial support for importing Kino project files. (Seems complete, but not fully tested)
* Preliminary support for snap markers
* Play selected button on clip monitor.

Some important features are still missing, but Kdenlive is now useable for small projects. We would like people to test what we have and tell us which features they miss the most. We want to find out what is most important to people, and last not least we want to attract developers. There are many small and not so small things where people could help. Be it GUI development, XML/network development, development of plugins, or simply reporting bugs.
