* Revert OpenGL change that might cause startup crash on some config. [#364278](https://bugs.kde.org/364278)
* Fix shadow offset in title editor. [#364584](https://bugs.kde.org/364584)
* Fix crash on reverse clip job (backport from master). [Commit.](http://commits.kde.org/kdenlive/f96ed884857cd47f224c51e6373611dded21d074) 
