  * Fix audio channel setting breaking opening of existing project file. [Commit][1].
  * Fix possible crash in audiolevel widget. [Commit][2].
  * Fix default audio channels for project not correctly saved. [Commit][3].
  * Fix guide/marker categories all black on some non english locales. [Commit][4].
  * Ensure Media browser saves zoom level when using mouse wheel to zoom. [Commit][5].
  * Extract audio: export only active streams, merge all if requested. [Commit][6].
  * Fix crash on subclip transcoding. [Commit][7].
  * Fix audio extract for multi stream clips. [Commit][8].
  * When restoring audio or video component in timeline, first try target track, then mirror track. [Commit][9].
  * Fix multi guide export enabled by default. [Commit][10].
  * Fix guides categories all black when opening a project from a different locale. [Commit][11].
  * Fix archiving crash on Windows caused by filesystem case sensitivity. [Commit][12].
  * Project Bin: don&#8217;t draw icon frame if icon size is null. [Commit][13].
  * Fix zone rendering not remembered when reopening a project. [Commit][14].
  * Fix detection/fixing when several clips in the project use the same file. [Commit][15].
  * Correctly update guides list when switching timeline tab. [Commit][16].

 [1]: http://commits.kde.org/kdenlive/5cb9fbbdef71563da856614537cfc10fd333e9b9
 [2]: http://commits.kde.org/kdenlive/88ce7e227417d252d8988934f5316d6b1d2303e5
 [3]: http://commits.kde.org/kdenlive/deb922fba936b30aa97dccf934b3d5c507484dee
 [4]: http://commits.kde.org/kdenlive/59955a98d1a91c2963a7d276b101c31ad9c2922e
 [5]: http://commits.kde.org/kdenlive/51c925296706c27dde49edb2ae8ed7617db695cc
 [6]: http://commits.kde.org/kdenlive/1c3a4130d9eefe63708fac2506c07852920d9a38
 [7]: http://commits.kde.org/kdenlive/5d60cd1f0ec2cfd00d538465a1011ef9596f473e
 [8]: http://commits.kde.org/kdenlive/4dae6cb0aa2edd96bf0e3f216eabde03ce9cdc59
 [9]: http://commits.kde.org/kdenlive/9bb0a1fb8aafb07743bc4b62278bc9c9fd52dfd7
 [10]: http://commits.kde.org/kdenlive/7a65e5949dc4b562a199a945aa3e4607cb54a5be
 [11]: http://commits.kde.org/kdenlive/d54d2bb0106eb8413f969161fef536d8c563b4f8
 [12]: http://commits.kde.org/kdenlive/d331eb82e1c31e077a0050dd130defad950a4e44
 [13]: http://commits.kde.org/kdenlive/01be71dd7e3ffdfb43fed5f6aff3afee08c216ab
 [14]: http://commits.kde.org/kdenlive/14217917b0121bea0bcaa24d5e328976eebdebd6
 [15]: http://commits.kde.org/kdenlive/fc291091e96551a9235f987f05adf48c2ef0d6c2
 [16]: http://commits.kde.org/kdenlive/2e98e8b26005c15c43799ba6b199a5ee0af9befd
