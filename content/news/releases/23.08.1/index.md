---
author: Farid Abdelnour
date: 2023-09-18T17:38:23+00:00
aliases:
- /en/2023/09/kdenlive-23-08-1-released/
- /it/2023/09/kdenlive-23-08-1-2/
- /de/2023/09/kdenlive-23-08-1/
discourse_topic_id: 5218
---

The first maintenance release of the 23.08 series is out!


