---
date: 2024-03-11
title: Kdenlive 24.02.0
author: Farid Abdelnour
featured_image_bg: /images/features/2402.png
discourse_topic_id: 12084
aliases:
- /en/2024/03/kdenlive-24-02-0-released/
- /fr/2024/03/kdenlive-24-02-0-est-sorti/
- /it/2024/03/kdenlive-24-02-0-2/
- /de/2024/03/kdenlive-24-02-0/
---

The team is thrilled to introduce the much-anticipated release of Kdenlive 24.02, featuring a substantial upgrade to our frameworks with the adoption of Qt6 and KDE Frameworks 6. This significant under-the-hood transformation establishes a robust foundation, shaping the trajectory of Kdenlive for the next decade.

The benefits of this upgrade are particularly noteworthy for Linux users, as improved Wayland support enhances the overall experience. Additionally, users on Windows, MacOS, and Linux will experience a substantial performance boost since Kdenlive now runs natively on DirectX, Metal, and Vulkan respectively, replacing the previous abstraction layer reliance on OpenGL and Angle, resulting in a more efficient and responsive application. This upgrade brings significant changes to packaging, featuring the introduction of a dedicated package for Apple Silicon, the discontinuation of PPA support and an enhanced method for installing the Whisper and Vosk speech-to-text engines.

While a significant effort has been invested in providing a stable user experience in this transition, we want to acknowledge that, like any evolving software, there might be some rough edges. Some known issues include: themes and icons not properly applied in Windows and AppImage, text not properly displayed in clips in the timeline when using Wayland and a crash in the Subtitle Manager under MacOS. Worth noting also is the temporary removal of the audio recording feature pending its migration to Qt6. We appreciate your understanding and encourage you to provide feedback in this release cycle so that we can continue refining and improving Kdenlive. In the upcoming release cycles (24.05 and 24.08), our development efforts will concentrate on stabilizing any remaining issues stemming from this upgrade. We’ll also prioritize short-term tasks outlined in our [roadmap][1], with a specific emphasis on enhancing performance and streamlining the effects workflow.

In terms of performance enhancements, this release introduces optimized RAM usage during the import of clips into the Project Bin. Furthermore, it addresses Nvidia encoding and transcoding issues with recent ffmpeg versions.

To safeguard project integrity, measures have been implemented to prevent corruptions. Projects with non-standard and variable frame rates are not allowed to be created. When rendering a project containing variable frame rate clips, users will receive a warning with the option to transcode these clips, mitigating potential audio-video synchronization issues.

Users can now enjoy the convenience of an automatic update check without an active network connection. Glaxnimate animations now default to the rawr format, replacing Lottie. Furthermore, we’ve introduced an FFv1 render preset to replace the previously non-functional Ut Video. And multiple project archiving issues have been fixed.

Beyond performance and stability we’ve managed to sneak in several nifty quality-of-life and usability improvements, the highlights include:

## Subtitles

This release introduces multiple subtitle support, allowing users to conveniently choose the subtitle from a drop-down list in the track header.

![](subtitle-switch.gif)

A subtitle manager dialog has been implemented to facilitate the import and export of subtitles.

![](subtitle-manage.png)

Now, in the Import Subtitle dialog, you have the option to create a new subtitle instead of replacing the previous one.

![](subtitle-import.png)

## Speech-to-Text

The Speech Editor, our text-based editing tool that enables users to add clips to the timeline from selected texts, now includes the option to create new sequences directly from the selected text.

![](stt-editor.gif)

## Effects

The initial implementation of the long awaited easing interpolation modes for keyframes has landed. Expected soon are easing types (ease in, ease out and ease in and out) and a graph editor.

![](easing.png)

![](blur.png)

## Rendering

Added the option to set an interpolation method for scaling operations on rendering.

![](rendering.png)

## Quality-of-Life and Usability

Added the option to apply an effect to a group of clips by simply dragging the effect onto any clip within the group.

![](effect-to-group.gif)

Conveniently move or delete selected clips within a group using the Alt + Select option.

![](alt-operation.gif)

Added a toggle button to clips with effects to easily enable/disable them directly from the timeline.
![](effect-switch.gif)

Added list of last opened clips in Clip Monitor’s clip name.

![](clip-monitor-name.gif)

Added the ability to open the location of the rendered file in the file manager directly from the render queue dialog.

![](open-folder.png)

The Document Checker has been completely rewritten following the implementation of sequences. Now, when you open a project, Kdenlive checks if all the clips, proxies, sequences, and effects are loaded correctly. If any errors are spotted, Kdenlive seamlessly sorts them out in the project files, preventing any possible project corruptions

![](document-checker.png)

Added the ability to trigger a sound notification when rendering is complete.

![](notification-finish.png)

 [1]: /roadmap
