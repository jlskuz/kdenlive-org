  * Fix multitrack view not exiting for some reason on tool switch (Qt6). [Commit.][2]
  * Fix qml warnings. [Commit.][3]
  * Show blue audio/video usage icons in project Bin for all clip types. [Commit.][4] See issue [#1816][5]
  * Multiple fixes for downloaded effect templates: broken link in effect info, empty name, cannot edit/delete. [Commit.][6]
  * New splash for 24.02. [Commit.][7]
  * Subtitles: add session id to tmp files to ensure 2 concurrent versions of a project don&#8217;t share the same tmp files. [Commit.][8] Fixes bug [#481525][9]
  * Fix title clip font&#8217;s weight lost between Qt5 and Qt6 projects. [Commit.][10]
  * Fix audio thumbnail not updated on replace clip in timeline. [Commit.][11] Fixes issue [#1828][12]
  * Refactor mouse position in the timeline to fix multiple small bugs. [Commit.][13] Fixes bug [#480977][14]
  * Subtitle import: disable ok button when no file is selected, only preview the 30 first lines. [Commit.][15]
  * Fix wrong clip dropped on timeline when subtitle track is visible. [Commit.][16] See bug [#481325][17]
  * Fix track name text color on Qt6. [Commit.][18]
  * Ensure we don&#8217;t mix title clips thumbnails (eg. in duplicated clips). [Commit.][19]
  * Fix scopes and titler bg on Win/Mac. [Commit.][20]
  * Fix incorrect item text. [Commit.][21]
  * Fix extract frame from video (fixes titler background, scopes, etc). [Commit.][22]
  * Make AVFilter average and gaussian blur keyframable. [Commit.][23]
  * Ensure we always load the latest xml definitions for effects. [Commit.][24]
  * Fix composition paste not correctly keeping a_track. [Commit.][25]
  * Ensure custom keyboard shortcuts are not deleted on config reset. [Commit.][26]
  * Fix crash after changing toolbar config: ensure all factory()->container actions are rebuild. [Commit.][27]
  * Try to fix white monitor on undock/fullscreen on Windows / Mac. [Commit.][28]
  * Fix sequence copy. [Commit.][29] See bug [#481064][30]
  * Fix pasting of sequence clips to another document messing clip ids. [Commit.][31]
  * Fix python package detection, install in venv. [Commit.][32] See issue [#1819][33]
  * Another pip fix. [Commit.][34]
  * Fix typos in venv pip. [Commit.][35]
  * Venv: ensure the python process are correctly started. [Commit.][36]
  * Add avfilter dblur xml description to fix param range. [Commit.][37]
  * Fix typo. [Commit.][38]
  * Correctly ensure pip is installed in venv. [Commit.][39]
  * Fix undocked widgets don&#8217;t have a title bar to allow moving / re-docking. [Commit.][40]
  * Ensure pip is installed inside our venv. [Commit.][41]
  * Fix Qt6 dragging clips with subtitle track visible. [Commit.][42] Fixes bug [#480829][43]
  * Subtitle items don&#8217;t have a grouped property &#8211; fixes resize bug. [Commit.][44] See bug [#480383][45]
  * Fix Shift + resize subtitle affecting other clips. [Commit.][46]
  * Speech to text : switch to importlib instead of deprecated pkg_resources. [Commit.][47]
  * Multi guides export: replace slash and backslash in section names to fix rendering. [Commit.][48] Fixes bug [#480845][49]
  * Fix moving grouped subtitles can corrupt timeline if doing an invalid move. [Commit.][50]
  * Fix sequence corruption on project load. [Commit.][51] Fixes bug [#480776][52]
  * Fix sort order not correctly restored, store it in project file. [Commit.][53] Fixes issue [#1817][54]
  * Ensure closed timeline sequences have a transparent background on opening. [Commit.][55] Fixes bug [#480734][56]
  * Fix Arrow down cannot move to lower track if subtitles track is active. [Commit.][57]
  * Enforce refresh on monitor fullscreen switch (fixes incorrectly placed image). [Commit.][58]
  * Fix audio lost when replacing clip in timeline with speed change. [Commit.][59] Fixes issue [#1815][60]
  * Fix duplicated filenames or multiple uses not correctly handled in archiving. [Commit.][61] Fixes bug [#421567][62]. Fixes bug [#456346][63]
  * Fix multiple archiving issues. [Commit.][64] Fixes bug [#456346][63]
  * Do not hide info message on render start. [Commit.][65]
  * Fix Nvidia transcoding. [Commit.][66] See issue [#1814][67]
  * Fix possible sequence corruption. [Commit.][68] Fixes bug [#480398][69]
  * Fix sequences folder id not correctly restored on project opening. [Commit.][70]
  * Fix duplicate sequence not creating undo entry. [Commit.][71] See bug [#480398][69]
  * Fix drag clip at beginning of timeline sometimes loses focus. [Commit.][72]
  * Fix luma files not correctly checked on document open, resulting in change to luma transitions. [Commit.][73] Fixes bug [#480343][74]
  * [CD] Run macOS Qt5 only on manual trigger. [Commit.][75]
  * Fix group move corrupting undo. [Commit.][76] Fixes bug [#480348][77]
  * Add FFv1 render preset to replace non working utvideo. [Commit.][78]
  * Fix possible crash on layout switch (with Qt in debug mode), fix mixer label overlap. [Commit.][79]
  * Hide timeline clip effect button on low zoom. [Commit.][80] Fixes issue [#1802][81]
  * Fix subtitles not covering transparent zones. [Commit.][82] Fixes bug [#480350][83]
  * Group resize: don&#8217;t allow resizing a clip to length < 1. [Commit.][84] Fixes bug [#480348][77]
  * Luma fixes: silently autofix luma paths for AppImage projects. Try harder to find matching luma in list, create thumbs in another thread so we don&#8217;t block the ui. [Commit.][85]
  * Fix crash cutting grouped overlapping subtitles. Don&#8217;t allow the cut anymore, add test. [Commit.][86] Fixes bug [#480316][87]
  * Remove unused var. [Commit.][88]
  * Effect stack: don&#8217;t show drop marker if drop doesn&#8217;t change effect order. [Commit.][89]
  * Try to fix crash dragging effect on Mac. [Commit.][90]
  * Another try to fix monitor offset on Mac. [Commit.][91]
  * Optimize some of the timeline qml code. [Commit.][92]
  * Fix DocumentChecker model directly setting items and incorrect call to columnCount() in index causing freeze in Qt6. [Commit.][93]
  * Fix clip monitor not updating when clicking in a bin column like date or description. [Commit.][94] Fixes bug [#480148][95]
  * Ensure we also check &#8220;consumer&#8221; producers on doc opening (playlist with a different fps). [Commit.][96]
  * Fix glaxnimate animation not parsed by documentchecker, resulting in empty animations without warn if file is not found. [Commit.][97]
  * Fix NVidia encoding with recent FFmpeg. [Commit.][98] See issue [#1814][67]
  * Fix clip name offset in timeline for clips with mixes. [Commit.][99]
  * Better way to disable building lumas in tests. [Commit.][100]
  * Don&#8217;t build lumas for tests. [Commit.][101]
  * Fix Mac compilation. [Commit.][102]
  * Fix data install path on Windows with Qt6. [Commit.][103]
  * Fix ridiculously slow recursive search. [Commit.][104]
  * Fix start playing at end of timeline. [Commit.][105] Fixes bug [#479994][106]
  * Try to fix mac monitor vertical offset. [Commit.][107]
  * Don&#8217;t display useless link when effect category is selected. [Commit.][108]
  * Fix save clip zone from timeline adding an extra frame. [Commit.][109] Fixes bug [#480005][110]
  * Fix clips with mix cannot be cut, add test. [Commit.][111] Fixes issue [#1809][112]. See bug [#479875][113]
  * Fix cmd line rendering. [Commit.][114]
  * Windows: fix monitor image vertical offset. [Commit.][115]
  * Fix project monitor loop clip. [Commit.][116]
  * Add test for recent sequence effect bug. [Commit.][117] See bug [#479788][118]
  * Fix tests (ensure we don&#8217;t try to discard a task twice). [Commit.][119]
  * Blacklist MLT Qt5 module when building against Qt6. [Commit.][120]
  * Fix monitor offset when zooming back to 1:1. [Commit.][121]
  * Fix sequence effects lost. [Commit.][122] Fixes bug [#479788][118]
  * Avoid white bg label in status bar on startup. [Commit.][123]
  * Fix qml warnings. [Commit.][124]
  * Fix clicking on clip fade indicator sometimes creating a 2 frames fade instead of defined duration. [Commit.][125]
  * Improved fix for center crop issue. [Commit.][126]
  * Fix center crop adjust not covering full image. [Commit.][127] Fixes bug [#464974][128]
  * Fix various Qt6 mouse click issues in monitors. [Commit.][129]
  * Disable Movit until it&#8217;s stable (should have done that a long time ago). [Commit.][130]
  * Fix Qt5 startup crash. [Commit.][131]
  * Add time to undo action text. [Commit.][132]
  * Fix cannot save list of project files. [Commit.][133] Fixes bug [#479370][134]
  * Add missing license info. [Commit.][135]
  * [Nightly Flatpak] Replace Intel Media SDK by OneVPL Runtime. [Commit.][136]
  * [Nightly Flatpak] Fix and update python deps. [Commit.][137]
  * [Nightly Flatpak] Switch to Qt6. [Commit.][138]
  * Fix editing title clip with a mix can mess up the track. [Commit.][139] Fixes bug [#478686][140]
  * Use Qt6 by default, fallback to Qt5. [Commit.][141]
  * Fix audio mixer cannot enter precise values with keyboard. [Commit.][142]
  * [CI] Require tests with Qt6 too. [Commit.][143]
  * Add FreeBSD Qt6 CI. [Commit.][144]
  * Apply i18n to percent values. [Commit.][145]
  * Show GPU in debug info. [Commit.][146]
  * Prevent, detect and possibly fix corrupted project files, fix feedback not displayed in project notes. [Commit.][147] Fixes issue [#1804][148]. See bug [#472849][149]
  * [nightly Flatpak] Add patch to fix v4l-utils. [Commit.][150]
  * Update copyright to 2024. [Commit.][151]
  * [nightly flatpak] fix v4l-utils once more. [Commit.][152]
  * [nightly Flatpak] v4l-utils uses meson now. [Commit.][153]
  * Don&#8217;t crash on first run. [Commit.][154]
  * [nightly flatpak] Try to fix v4l-utils. [Commit.][155]
  * [nightly flatpak] Cleanup. [Commit.][156]
  * Get rid of dropped QtGraphicalEffects. [Commit.][157]
  * Fix qml warnings. [Commit.][158]
  * Qt6: fix subtitle editing in timeline. [Commit.][159]
  * Fix subtitles crashing on project load (incorrectly setting in/out snap points). [Commit.][160]
  * Test project&#8217;s active timeline is not always the first sequence. [Commit.][161]
  * Ensure secondary timelines are added to the project before being loaded. [Commit.][162]
  * Ensure autosave is not triggered when project is still loading. [Commit.][163]
  * Show GPU name in Wizard. [Commit.][164]
  * Avoid converting bin icons to/from QVariant. [Commit.][165]
  * [Nightly Flatpak] Update deps. [Commit.][166]
  * Fix Qt6 audio / video only clip drag broken from clip monitor. [Commit.][167]
  * Fix rubber select incorrectly moving selected items when scrolling the view. [Commit.][168]
  * Port away from jobclasses KIO header. [Commit.][169]
  * Fix variable name shadowing. [Commit.][170]
  * When switching timeline tab without timeline selection, don&#8217;t clear effect stack if it was showing a bin clip. [Commit.][171]
  * Fix crash pressing del in empty effect stack. [Commit.][172]
  * Ensure check for HW accel is also performed if some non essential MLT module is missing. [Commit.][173]
  * Fix closed sequences losing properties, add more tests. [Commit.][174]
  * Don&#8217;t attempt to load timeline sequences more than once. [Commit.][175]
  * Fix &#8220;Sequence from selection&#8221; with single track. [Commit.][176]
  * Refactor code for paste. [Commit.][177]
  * Fix timeline groups lost after recent commit on project save. [Commit.][178]
  * Ensure we always use the correct timeline uuid on some clip operations. [Commit.][179]
  * Qt6: fix monitor image vertical offset. [Commit.][180]
  * Always keep all timeline models opened. [Commit.][181] See bug [#478745][182]
  * Add animation: remember last used folder. [Commit.][183] See bug [#478688][184]
  * Fix KNS KF6 include. [Commit.][185]
  * Add missing include. [Commit.][186]
  * Refresh effects list after downloading an effect. [Commit.][187]
  * Fix crash searching for effect (recent regression). [Commit.][188]
  * Fix audio or video only drag of subclips. [Commit.][189] Fixes bug [#478660][190]
  * Fix editing title clip duration breaks title (recent regression). [Commit.][191]
  * Glaxnimate animations: use rawr format instead of Lottie by default. [Commit.][192] Fixes bug [#478685][193]
  * Effect Stack: remove color icons, fix mouse wheel seeking while scrolling. [Commit.][194] See issue [#1786][195]
  * Fix timeline focus lost when dropping an effect on a clip. [Commit.][196]
  * Disable check for removable devices on Mac. [Commit.][197]
  * [CD] Use Qt6 templates instead of custom magic. [Commit.][198]
  * Fix type in Purpose KF version check. [Commit.][199]
  * Fix dropping lots of clips in Bin can cause freeze on abort. [Commit.][200]
  * Right click on a mix now shows a mix menu (allowing deletion). [Commit.][201] Fixes bug [#442088][202]
  * Don&#8217;t add mixes to disabled tracks. [Commit.][203] See bug [#442088][202]
  * Allow adding a mix without selection. [Commit.][204] See bug [#442088][202]
  * Fix proxied playlist clips (like stabilized clips) rendered as interlaced. [Commit.][205] Fixes bug [#476716][206]
  * [CI] Try different approach for macOS signing. [Commit.][207]
  * [CI] Signing test, explicitly source env for now. [Commit.][208]
  * Camcorder proxies: ensure we have the same count of audio streams and if not, create a new proxy with audio from original clip (Fixes Sony FX6 proxies). [Commit.][209]
  * Fix typo. [Commit.][210] Fixes issue [#1800][211]
  * [CI] Re-enable Flatpak. [Commit.][212]
  * [CI] More fixes for the signing test. [Commit.][213]
  * [CI] Fixes for the signing test. [Commit.][214]
  * [CI] Add macOS signing test. [Commit.][215]
  * [CI] Fix pipeline after recent renaming upstream. [Commit.][216]
  * Qml warning fixes. [Commit.][217]
  * Add subtitle manager to project mneu. [Commit.][218]
  * Fix groups tests. [Commit.][219]
  * Fix transparency lost on rendering nested sequences. [Commit.][220] Fixes bug [#477771][221]
  * Fix guides categories not applied on new document. [Commit.][222] Fixes bug [#477617][223]
  * Fix selecting several individual items in a group. [Commit.][224]
  * Add import/export to subtitle track manager. [Commit.][225]
  * Drag & drop of effect now applies to all items in a group. [Commit.][226] See issue [#1327][227]
  * New: select an item in a group with Alt+click. You can then perform operations on that clip only: delete, move. [Commit.][228] See issue [#1327][227]
  * Consistency: activating an effect in the effects list now consistently applies to all selected items (Bin or Timeline). [Commit.][229]
  * Cleanup assets link to documentation. [Commit.][230]
  * Check MLT&#8217;s render profiles for missing codecs. [Commit.][231] See bug [#475029][232]
  * Various fixes for python setup. [Commit.][233]
  * Fix Qt6 compilation. [Commit.][234]
  * FIx incorreclty placed ifdef. [Commit.][235]
  * Start integrating some of the new MLT keyframe types. [Commit.][236]
  * Various fixes for python venv install. [Commit.][237]
  * Fix missing argument in constructor call. [Commit.][238]
  * Fix crash on auto subtitle with subtitle track selected. [Commit.][239]
  * Fix python install stuck. [Commit.][240]
  * Improve timeline clip effect indicator. [Commit.][241] See issue [#445][242]
  * Work/multisubtitles. [Commit.][243]
  * Fix some issues in clip monitor&#8217;s last clip menu. [Commit.][244]
  * Various fixes and improved feedback for Python venv, add option to run STT on full project. [Commit.][245]
  * Text corrections. [Commit.][246]
  * Fix typos. [Commit.][247]
  * If users try to render a project containing variable framerate clips, show a warning and propose to transcode these clips. [Commit.][248]
  * Fix qml warning (incorrect number of args). [Commit.][249]
  * Fix qt6 timeline drag. [Commit.][250]
  * Flatpak: Use id instead of app-id. [Commit.][251]
  * Fix audio stem export. [Commit.][252]
  * Add link to our documentation in the effects/composition info. [Commit.][253]
  * Qt6: fix monitor background and a few qml mouse issues. [Commit.][254]
  * Rename ObjectType to KdenliveObjectType. [Commit.][255]
  * We need to use Objective C++ for MetalVideoWidget. [Commit.][256]
  * When pasting clips to another project, disable proxies. [Commit.][257] Fixes issue [#1785][258]
  * Remove unneeded lambda capture. [Commit.][259]
  * Fix monitor display on Windows/Qt6. [Commit.][260]
  * Cleanup readme and flatpak nightly manifests. [Commit.][261]
  * [Nightly Flatpak] Do not build tests. [Commit.][262]
  * Fix tests broken by last commit. [Commit.][263]
  * Add list of last opened clips in Clip Monitor&#8217;s clip name. [Commit.][264]
  * Add Craft Jobs for Qt6. [Commit.][265]
  * [CI] Switch to new template include format. [Commit.][266]
  * [CI] Add reuse-lint job. [Commit.][267]
  * Chore: REUSE linting for compliance. [Commit.][268]
  * Don&#8217;t check for cache space on every startup. [Commit.][269]
  * Don&#8217;t allow creating profile with non standard and non integer fps from a clip. [Commit.][270] See issue [#476754][271]
  * Remove unmaintained changelog file. [Commit.][272]
  * Automatically check for updates based on the app version (no network connection at this point). [Commit.][273]
  * Fix project duration for cli rendering. [Commit.][274]
  * Fix clips with missing proxy incorrectly loaded on project opening. [Commit.][275]
  * Fix compilation with KF < 5.100. [Commit.][276]
  * Add undo redo to text based edit. [Commit.][277]
  * Check and remove circular dependencies in tractors. [Commit.][278] Fixes bug [#471359][279]
  * Hide resize handle on tiny clips with mix. [Commit.][280]
  * Fix minor typos. [Commit.][281]
  * Adapt to new KFileWidget API. [Commit.][282]
  * Fix mix not always deleted when moving grouped clips on same track. [Commit.][283]
  * Fix python venv for Windows. [Commit.][284]
  * Fix timeremap. [Commit.][285]
  * Fix replace clip keeping audio index from previous clip, sometimes breaking audio. [Commit.][286] See bug [#476612][287]
  * Create sequence from selection: ensure we have enough audio tracks for AV groups. [Commit.][288]
  * Fix timeline duration incorrect after create sequence from timeline selection. [Commit.][289]
  * Add a Saving Successful event, so people can easily play a sound or show a popup on save if wanted. [Commit.][290] See issue [#1767][291]
  * Fix project duration not updating when moving the last clip of a track to another non last position. [Commit.][292] See bug [#476493][293]
  * Update file kdenlive.notifyrc. [Commit.][294]
  * Duplicate .notifyrc file to have both KF5 and KF6 versions. [Commit.][295]
  * Don&#8217;t lose subtitle styling when switching to another sequence. [Commit.][296] Fixes bug [#476544][297]
  * Port from deprecated ksmserver calls. [Commit.][298]
  * Allow aborting clip import operation. [Commit.][299]
  * Ensure no urls are added to file watcher when interruping a load operation. [Commit.][300]
  * Fix crash dropping url to Library. [Commit.][301]
  * When dropping multiple files in project bin, improve import speed by not checking if every file is on a remote drive. [Commit.][302]
  * Fix titler shadow incorrectly pasted on selection. [Commit.][303] Fixes bug [#476393][304]
  * Sequences folder now has a colored icon and is always displayed on top. [Commit.][305]
  * Fix Qt5 compilation. [Commit.][306]
  * Fix Qt5 compilation take 3. [Commit.][307]
  * Fix Qt5 compilation take 2. [Commit.][308]
  * Fix Qt5 compilation. [Commit.][309]
  * Fix some Qt6 reported warnings. [Commit.][310]
  * Fix pasted effects not adjusted to track length. [Commit.][311]
  * Python virtual env: Add config tab in the Environement Settings page, minor fixes for the dependencies checks. [Commit.][312]
  * [Qt6] We need to link to d3d on Windows. [Commit.][313]
  * Convert license headers to SPDX. [Commit.][314]
  * Use pragma once for new monitor code. [Commit.][315]
  * Fix Qt6 build on Windows. [Commit.][316]
  * Text based edit: add font zooming and option to remove all silence. [Commit.][317]
  * Move venv to standard xdg location (.local/share/kdenlive). [Commit.][318]
  * Whisper now has word timings. [Commit.][319]
  * Use python venv to install modules. [Commit.][320]
  * Fix timeline preview ignored in temporary data dialog. [Commit.][321] Fixes bug [#475980][322]
  * Improve debug output for tests. [Commit.][323]
  * Correctly prefix python scripts, show warning on failure to find python. [Commit.][324]
  * Qt6 Monitor support. [Commit.][325]
  * Speech to text: fix whisper install aborting after 30secs. [Commit.][326]
  * Don&#8217;t try to generate proxy clips for audio with clipart. [Commit.][327]
  * Clip loading: switch to Mlt::Producer probe() instead of fetching frame. [Commit.][328]
  * Multiple fixes for time remap losing keyframes. [Commit.][329]
  * [CI] Increase per test timeout. [Commit.][330]
  * Add secondary color correction xml with renamed alphasp0t effect, fix effectgroup showing incorrect names. [Commit.][331]
  * Add png with alpha render profile. [Commit.][332] See issue [#1605][333]
  * Fix Mix not correctly deleted on group track move. [Commit.][334] See issue [#1726][335]
  * Cleanup commented code. [Commit.][336]
  * Fix setting default values is never executed. [Commit.][337]
  * Cleanup param insert and placeholder replacement. [Commit.][338]
  * Move render argument creation to a function. [Commit.][339]
  * Move project init logic out of renderrequest. [Commit.][340]
  * Use projectSceneList() for both cli and gui rendering. [Commit.][341]
  * Use active timeline for rendering. [Commit.][342]
  * Adapt to KBookmarkManager API change. [Commit.][343]
  * Small cleanup. [Commit.][344]
  * Properly initialize projectItemModel and bin playlist on render request. [Commit.][345]
  * Revert &#8220;Properly initialize projectItemModel and bin playlist on render request&#8221;. [Commit.][346]
  * Fix for renamed frei0r effects. [Commit.][347]
  * Fix rendering with alpha. [Commit.][348]
  * Rotoscoping: don&#8217;t auto add a second kfr at cursor pos when creating the initial shape, don&#8217;t auto add keyframes until there are 2 keyframes created. [Commit.][349]
  * Fix description &#8211;render-async flag. [Commit.][350]
  * Fix keyframe param not correctly enabled when selecting a clip. [Commit.][351]
  * Fix smooth keyframe path sometimes incorrectly drawn on monitor. [Commit.][352]
  * Allow setting the default interpolation method for scaling operations on rendering. [Commit.][353] Fixes issue [#1766][354]
  * Don&#8217;t attempt to replace clip resource if proxy job was not completely finished. [Commit.][355] Fixes issue [#1768][356]
  * Properly initialize projectItemModel and bin playlist on render request. [Commit.][357]
  * Rename render params, don&#8217;t load project twice. [Commit.][358]
  * Remove accelerator on timeline tab rename. [Commit.][359] Fixes issue [#1769][360]
  * Print render errors for cli rendering too. [Commit.][361]
  * Minor cleanup. [Commit.][362]
  * Improve exit code on failure. [Commit.][363]
  * [cli rendering] Fix condition for subtitle. [Commit.][364]
  * Show documentchecker warning only if relevant. [Commit.][365]
  * Fix printing of documentchecker results. [Commit.][366]
  * [cli renderer] Ensure x265 params are calculated. [Commit.][367]
  * Custom clip job: allow using current clip&#8217;s frame as parameter. [Commit.][368]
  * Properly adjust timeline clips on sequence resize. [Commit.][369]
  * Remove unused debug stuff. [Commit.][370]
  * Fix project duration not correctly updated on hide / show track. [Commit.][371]
  * Custom clip jobs: handle lut file as task output. [Commit.][372]
  * Allow renaming a timeline sequence by double clicking on its tab name. [Commit.][373]
  * Fix resize clip with mix test. [Commit.][374]
  * Fix resize clip start to frame 0 of timeline not correctly working in some zoom levels,. [Commit.][375]
  * Remember Clip Monitor audio thumbnail zoom & position for each clip. [Commit.][376]
  * Asset List: ensure favorite are shown using a bold font. [Commit.][377]
  * Fix asset list using too much height. [Commit.][378]
  * Switch Effects/Compositions list to QWidget. [Commit.][379]
  * Drop unused and deprecated qmlmodule QtGraphicalEffects. [Commit.][380]
  * Fix warning. [Commit.][381]
  * Fix multiple audio streams broken by MLT&#8217;s new astream property. [Commit.][382] Fixes bug [#474895][383]
  * Custom clip jobs: ensure we never use the same output name if several tasks are started on the same job. [Commit.][384]
  * Custom clip jobs: ensure script exists and is executable. [Commit.][385]
  * Fix dialogs not correctly deleted, e.g. add track dialog, causing crash on exit. [Commit.][386]
  * Ensure clips with audio (for exemple playlists) don&#8217;t block audio when inserted on video track. [Commit.][387]
  * Ensure translations cannot mess with file extensions. [Commit.][388]
  * Fix another case blocking separate track move. [Commit.][389]
  * Fix grabbed clips cannot be moved on upper track in some cases. [Commit.][390]
  * Final blocks for enabling render test suite: add synchronous option to exit only after rendering is finished, add option for render preset (use H264 as default). [Commit.][391]
  * Implement #1730 replace audio or video of a bin clip in timeline. [Commit.][392]
  * Fix cppwarning. [Commit.][393]
  * Fix move clip part of a group on another track not always working. [Commit.][394]
  * Fix playlist count not correctly updated, allowing to delete last sequence. [Commit.][395] Fixes bug [#474988][396]
  * Fix motion-tracker Nano file name and links to the documentation. [Commit.][397]
  * Stop installing kdenliveui.rc also as separate file, next to Qt resource. [Commit.][398]
  * Library: add action to open a library file in a File manager. [Commit.][399]
  * Fix tests and possible corruption in recent mix fix. [Commit.][400]
  * Correctly highlight newly dropped files in library. [Commit.][401]
  * Fix threading issue crashing in resource widget. [Commit.][402] Fixes issue [#1612][403]
  * Fix freeze on adding mix. [Commit.][404] See issue [#1751][405]
  * Make Lift work as expected by most users. [Commit.][406] Fixes bug [#447948][407]. Fixes bug [#436762][408]
  * Fix load task discarding kdenlive settings (caused timeline clips to miss the &#8220;proxy&#8221; icon. [Commit.][409]
  * Fix multiple issues with Lift/Gamma/Gain undo. [Commit.][410] Fixes bug [#472865][411]. Fixes bug [#462406][412]
  * Fix freeze / crash on project opening. [Commit.][413]
  * COrrectly update effect stack when switching timeline tab. [Commit.][414]
  * Drop timeline guides, in favor of sequence clip markers. [Commit.][415]
  * Optimize RAM usage by not storing producers on which we did a get_frame operation. [Commit.][416]
  * Fix guide multi-export adding an extra dot to the filename. [Commit.][417]
  * Open the recursive search from the project file location. [Commit.][418]
  * Inform user about time spent on recursive search. [Commit.][419]
  * Allow open contained folder in job queue dialog. [Commit.][420]
  * Read input and output from command line. [Commit.][421]
  * Correctly process configurable render params. [Commit.][422]
  * Fix crash on subclip transcoding. [Commit.][423] Fixes issue [#1753][424]
  * Fix audio extract for multi stream clips. [Commit.][425]
  * Correctly set render params for headless rendering. [Commit.][426]
  * Ensure some basic parts are built with headless rendering. [Commit.][427]
  * Remove unneeded setting of CMake policies, implied by requiring 3.16. [Commit.][428]
  * Fix detection/fixing when several clips in the project use the same file. [Commit.][429]
  * Render widget: show warning if there is a missing clip in the project. [Commit.][430]
  * DocumentChecker: Enable recursive search for clips with proxy but missing source. [Commit.][431]
  * Fix rnnoise effect parameters and category. [Commit.][432]
  * Fix minor typo. [Commit.][433]
  * Fix zone rendering not remembered when reopening a project. [Commit.][434]
  * Add missing test file. [Commit.][435]
  * Various document checker fixes: fix display update on status change, allow sorting in dialog, hide recreate proxies if source is not available, add test for missing proxy. [Commit.][436]
  * Project Bin: don&#8217;t draw icon frame if icon size is null. [Commit.][437]
  * Fix clips with empty resource not detected by our documentchecker code. [Commit.][438]
  * Fix document checker dialog not enabling ok after removing problematic clips. [Commit.][439]
  * Document checker dialog: fix selection, allow multiple selection, limit color background and striked out text to a specific column. [Commit.][440]
  * Show fade value on drag. [Commit.][441] Fixes issue [#1744][442]
  * If copying an archived file fails, show which file failed in user message. [Commit.][443]
  * Don&#8217;t incorrectly treat disabled proxy (-) as missing. [Commit.][444] Fixes issue [#1748][445]
  * Fix minor typo. [Commit.][446]
  * Fix box_blur xml. [Commit.][447]
  * Add new &#8220;preserve alpha&#8221; option to box blur. [Commit.][448]
  * Transcoding: add option to replace clip in project (disabled for timeline sequence clips). [Commit.][449] See issue [#1747][450]
  * Add notr=&#8221;true&#8221; for text that should not be translated. [Commit.][451]
  * When an MLT playlist proxy is missing, it should be reverted to a producer, not stay in a chain. [Commit.][452]
  * Adapt to kbookmarks API change. [Commit.][453]
  * Adapt to KNotifcations API change. [Commit.][454]
  * Try to auto fix path of LUT files on project opening. [Commit.][455]
  * Automatically fix missing fonts (like before). [Commit.][456]
  * Remove unused ManageCapturesDialog. [Commit.][457]
  * [DCResolverDialog] Improve UI. [Commit.][458]
  * Fix recursive search and &#8220;use placeholder&#8221;. [Commit.][459]
  * [REUSE] Remove duplicated entry in dep5. [Commit.][460]
  * Chore(REUSE): Further linting. [Commit.][461]
  * Chore(REUSE): Add headers in data/effects/update. [Commit.][462]
  * Chore(REUSE): Add headers in src/ui. [Commit.][463]
  * Chore(REUSE): Add missing licence texts. [Commit.][464]
  * Chore(reuse): Add missing IP info. [Commit.][465]
  * Chore(REUSE): Add SPDX info to CMakelists.txt files. [Commit.][466]
  * Add missing include (fix qt6 build). [Commit.][467]
  * Don&#8217;t duplicate KF\_DEP\_VERSION + remove unused REQUIRED\_QT\_VERSION. [Commit.][468]
  * Fix configure qt6. [Commit.][469]
  * [ColorWheel] Show real color in slider instead of black and white. [Commit.][470] See issue [#1405][471]
  * Add QColorUtils::complementary. [Commit.][472]
  * Add some accessibility names for testing. [Commit.][473]
  * Add option to export guides as FFmpeg chapter file. [Commit.][474] See bug [#451936][475]
  * [Rendering] Further restructuring. [Commit.][476]
  * [DocumentResource] Fix workflow with proxies. [Commit.][477]
  * Try to fix tests. [Commit.][478]
  * [DocumentChecker] Fix and polish after refactoring. [Commit.][479]
  * [DocumentChecker] Refactor code to split logic and UI. [Commit.][480]
  * [DocumentChecker] Start to split UI and backend code. [Commit.][481]
  * Add our mastodon on apps.kde.org. [Commit.][482]
  * Fix typo not installing renderer. [Commit.][483]
  * Fix tests. [Commit.][484]
  * Delete unused var. [Commit.][485]
  * Initial (yet hacky) cli rendering. [Commit.][486]
  
 [2]: http://commits.kde.org/kdenlive/c27597c96dd60c56fd4edc1d3f5298491abf44a4
 [3]: http://commits.kde.org/kdenlive/5113ba4ad3ed8cf6abf03c15ec2853dd1a170903
 [4]: http://commits.kde.org/kdenlive/f38897c6497dd13bd829352b48c02180cf8ed5f1
 [5]: https://invent.kde.org/multimedia/kdenlive/-/issues/1816
 [6]: http://commits.kde.org/kdenlive/2580f09e5448ef072c32592ba63e655a4b592519
 [7]: http://commits.kde.org/kdenlive/311a8d128830ac07fa12f18613a9a3aa7e7201c2
 [8]: http://commits.kde.org/kdenlive/0b67c373df9aa2f38a8a11ad491b08fd18563ff4
 [9]: https://bugs.kde.org/481525
 [10]: http://commits.kde.org/kdenlive/b65dec7c875d05fd8b662114ba0ec50ce2b62fa8
 [11]: http://commits.kde.org/kdenlive/85c3061d78e491beaa8cbc0d83e557c4aee3809b
 [12]: https://invent.kde.org/multimedia/kdenlive/-/issues/1828
 [13]: http://commits.kde.org/kdenlive/c1fc9512cefff05b6ea8f412cee9bb64a599bce5
 [14]: https://bugs.kde.org/480977
 [15]: http://commits.kde.org/kdenlive/995e2693fdca4e463a0496e6842ff2a734c561f0
 [16]: http://commits.kde.org/kdenlive/4c9eca8adca13394392328210f5737b9d409a631
 [17]: https://bugs.kde.org/481325
 [18]: http://commits.kde.org/kdenlive/5da1e8e838083293294515d489ec2735ead0bd0d
 [19]: http://commits.kde.org/kdenlive/1e26ce280ccec2ee9c06e363c3069de1b0d6cf46
 [20]: http://commits.kde.org/kdenlive/23685c755ec98b12bb9a04ecef638c8d1258a4b4
 [21]: http://commits.kde.org/kdenlive/1b275b58f02a5fda013c6ef484623cee7f786708
 [22]: http://commits.kde.org/kdenlive/85391400e43ee92ed442f339e9071309920dc534
 [23]: http://commits.kde.org/kdenlive/8cb9f8074068c5c2a79bf440d61dfbe35daba9e0
 [24]: http://commits.kde.org/kdenlive/e215d21e4ea60b97a2c79a39b1fc66f4d8da9c3b
 [25]: http://commits.kde.org/kdenlive/e257ea5f7f708601ff378402bd573f7f0ddfce63
 [26]: http://commits.kde.org/kdenlive/c5085679fdd625bd6aadaf36c9b13ca0d2253b7a
 [27]: http://commits.kde.org/kdenlive/5bdbfa5f87f75b79cabc03b23037387384393c0b
 [28]: http://commits.kde.org/kdenlive/1ced5a9f4cb1966970c92b53a9cded96d831452a
 [29]: http://commits.kde.org/kdenlive/8b5f5216f74d350907d6c611321899a610aca2e8
 [30]: https://bugs.kde.org/481064
 [31]: http://commits.kde.org/kdenlive/97621ff7ba21b79262b819b1c2f46a92bed4034b
 [32]: http://commits.kde.org/kdenlive/fd463d4b5946239224028e91de782d98acbd4773
 [33]: https://invent.kde.org/multimedia/kdenlive/-/issues/1819
 [34]: http://commits.kde.org/kdenlive/31cb408ac1f740c9305dce724730bf77baa8353b
 [35]: http://commits.kde.org/kdenlive/1b8f9ca8ccd5c409126c1f59688f11a5c5c3403b
 [36]: http://commits.kde.org/kdenlive/15b6457923f8dcf46ae7083e90d2529ba0474a4b
 [37]: http://commits.kde.org/kdenlive/4c46811c054a539c5f0ff888397e40867ccaaad4
 [38]: http://commits.kde.org/kdenlive/3f77d976f9c4e18be79857ebcad65b9d59ac3c21
 [39]: http://commits.kde.org/kdenlive/cfba3736aef47f26feb5301df1a48433081288e8
 [40]: http://commits.kde.org/kdenlive/f53a4835e56179ae5ed8ac4b0fb82adc6aa5df98
 [41]: http://commits.kde.org/kdenlive/f7d222e58d9a36a540c9c56b115bd1bfd3d3eb1b
 [42]: http://commits.kde.org/kdenlive/9b172c9ba14a5fce575132d94a91cee577134380
 [43]: https://bugs.kde.org/480829
 [44]: http://commits.kde.org/kdenlive/1a69139dfd62c5330e84d3deca0d17db733dd3a6
 [45]: https://bugs.kde.org/480383
 [46]: http://commits.kde.org/kdenlive/586ad357d2f90d5ab9dd2b6b9236a1e1f4f6e26c
 [47]: http://commits.kde.org/kdenlive/5cfb3a17a9fc879e2bf5aa5b297e459e6613075f
 [48]: http://commits.kde.org/kdenlive/c288dab07d836cada8d668944cde088fd4238b33
 [49]: https://bugs.kde.org/480845
 [50]: http://commits.kde.org/kdenlive/b60edcf124ca06f36dc92632955cc548021cb7a0
 [51]: http://commits.kde.org/kdenlive/eadf5d11134fed8a1230d7866c770a3a64eb162c
 [52]: https://bugs.kde.org/480776
 [53]: http://commits.kde.org/kdenlive/a49358ebf441e35192e52852866c242d0287250e
 [54]: https://invent.kde.org/multimedia/kdenlive/-/issues/1817
 [55]: http://commits.kde.org/kdenlive/f6a10cbd665588d491d7bd275bd2717b723d28ec
 [56]: https://bugs.kde.org/480734
 [57]: http://commits.kde.org/kdenlive/893a2f00e59a8512497673a8a9423d2aa9dcb7f6
 [58]: http://commits.kde.org/kdenlive/8ff2ccf97f2968cac07d7b569a99ac2093f3f845
 [59]: http://commits.kde.org/kdenlive/21718281abb1cdc0ce72c2b25adc09cb928536ac
 [60]: https://invent.kde.org/multimedia/kdenlive/-/issues/1815
 [61]: http://commits.kde.org/kdenlive/273b8d99070578eb4af93aeb88adbc51bcd9603a
 [62]: https://bugs.kde.org/421567
 [63]: https://bugs.kde.org/456346
 [64]: http://commits.kde.org/kdenlive/ec64132e1a8f9b1e79013b5ff90f5b72e9f2b7d4
 [65]: http://commits.kde.org/kdenlive/1acb8719eb5028d6c59be8f1f7598e675050cc8b
 [66]: http://commits.kde.org/kdenlive/331fefcf8df7b504d8d93439193c9751b52e0a51
 [67]: https://invent.kde.org/multimedia/kdenlive/-/issues/1814
 [68]: http://commits.kde.org/kdenlive/bad2ff7babc8e50d097c44b5f839b852f279a1a7
 [69]: https://bugs.kde.org/480398
 [70]: http://commits.kde.org/kdenlive/5e89a6dadddd8709031569cb56a777b0961d06ad
 [71]: http://commits.kde.org/kdenlive/4ae7c24d5936652b94de187edf28d21457b7f21a
 [72]: http://commits.kde.org/kdenlive/f2c1284e21f44a9a18cd6a8ed38b69545f6871c8
 [73]: http://commits.kde.org/kdenlive/c7b4620046c5de48a678d94133c936c5c5dfa342
 [74]: https://bugs.kde.org/480343
 [75]: http://commits.kde.org/kdenlive/39c1c59478540b03b0fff0a9e5e8d4230f895c61
 [76]: http://commits.kde.org/kdenlive/57721eb6ef4a5772f688757ff80cec1ca8f8bd2f
 [77]: https://bugs.kde.org/480348
 [78]: http://commits.kde.org/kdenlive/e2f5268d7b9c83a05f16fa48cb590de1a8dad892
 [79]: http://commits.kde.org/kdenlive/5fd2ea6f35ece30b94f45c97d1e07f7990a89b4f
 [80]: http://commits.kde.org/kdenlive/e5ec5e82d85d76ed994bb960a2c603ceb9936a8c
 [81]: https://invent.kde.org/multimedia/kdenlive/-/issues/1802
 [82]: http://commits.kde.org/kdenlive/42cf0cbeef4058d6f1a7753fcbeb93ed6cbac081
 [83]: https://bugs.kde.org/480350
 [84]: http://commits.kde.org/kdenlive/7a56275032b3c1de3d1845f5e9b8681dcc047f6b
 [85]: http://commits.kde.org/kdenlive/1363461fcfc2261fea64d840ebefe3f9f85acd93
 [86]: http://commits.kde.org/kdenlive/281972a5c12d808aa3664c5273b32ec3462c090e
 [87]: https://bugs.kde.org/480316
 [88]: http://commits.kde.org/kdenlive/4878e41f0b5037cdbe92f69b6ae443f655dcd5e5
 [89]: http://commits.kde.org/kdenlive/411dc01e75aea35fffa5499d8c4a688654305b67
 [90]: http://commits.kde.org/kdenlive/7df3e7f26a640143f407bd6bd58333f0026c2520
 [91]: http://commits.kde.org/kdenlive/f9666386cc1dc12fa825d3738f0acc2e96f99285
 [92]: http://commits.kde.org/kdenlive/f34b2779ea579eb3c7d8782d67d66960ca4d6fc4
 [93]: http://commits.kde.org/kdenlive/92b881dfa23c82363fd05a330ceba06bb1401119
 [94]: http://commits.kde.org/kdenlive/1e9f669c61c3d85ff2ad47084e0fe5decf20d664
 [95]: https://bugs.kde.org/480148
 [96]: http://commits.kde.org/kdenlive/cf05ec42c6b1de82821060879aac2d1e29d1a000
 [97]: http://commits.kde.org/kdenlive/1f356d6724c2cb87ce7539176002e284116045aa
 [98]: http://commits.kde.org/kdenlive/50ae0995ce72589495b92de68c036f5b1171b1fe
 [99]: http://commits.kde.org/kdenlive/d36e2e18bf7670f8a78c0d7803e50fa71ba1f8a0
 [100]: http://commits.kde.org/kdenlive/ede3adae1899e6a7a106803470c7b3e129952063
 [101]: http://commits.kde.org/kdenlive/ec4dc0c72fff4c0fec3a79158b9c69b0a7bc5f5b
 [102]: http://commits.kde.org/kdenlive/778e79505c72278abe583e3e46023ac8422a1a8b
 [103]: http://commits.kde.org/kdenlive/49ac57eb695f444e36cb0dcb69eba984b522508d
 [104]: http://commits.kde.org/kdenlive/7e40f7fcd0b93bfb8aed82c3da74383ef18cf51c
 [105]: http://commits.kde.org/kdenlive/c647e0861095bc893a53628da993668f07cf4201
 [106]: https://bugs.kde.org/479994
 [107]: http://commits.kde.org/kdenlive/47c251bbe46d79bd54e0ac356db7d93af559c51b
 [108]: http://commits.kde.org/kdenlive/252f93764e6f642f384804fd1ef8eb6faab7b4c2
 [109]: http://commits.kde.org/kdenlive/fba6a141d49b64d6bc463e2d7ea5478d82f33789
 [110]: https://bugs.kde.org/480005
 [111]: http://commits.kde.org/kdenlive/bfa290bf3ce6feae063fc8ceccde7a7b50036530
 [112]: https://invent.kde.org/multimedia/kdenlive/-/issues/1809
 [113]: https://bugs.kde.org/479875
 [114]: http://commits.kde.org/kdenlive/5534719af52fec578302140a80abba3ed8da11d3
 [115]: http://commits.kde.org/kdenlive/c6d991206cb3cb37b6d25c573d6b659922ff0a48
 [116]: http://commits.kde.org/kdenlive/342ce4d372f389140f8e21ff6df62b2c10de006f
 [117]: http://commits.kde.org/kdenlive/ee873cf885b618e67b56c71fc06295040a87e709
 [118]: https://bugs.kde.org/479788
 [119]: http://commits.kde.org/kdenlive/da2ad2dbe07d1cd99eeb842fc56b49b50fbcd836
 [120]: http://commits.kde.org/kdenlive/a3d52b2b3258a9c250dd3ad27d618d024beee4c6
 [121]: http://commits.kde.org/kdenlive/4b00e27e5b9a94cba3836875c4671bb8884c5880
 [122]: http://commits.kde.org/kdenlive/7c49766b45127af863112b8ff61e3a5be6668dbf
 [123]: http://commits.kde.org/kdenlive/caa91f9b0836c76cf29c12e07afe2a137c5632ae
 [124]: http://commits.kde.org/kdenlive/eb5026ce0a3b1b77886912333750747215387f85
 [125]: http://commits.kde.org/kdenlive/e09496c6842eb3f6aca39c2029cf604f34437882
 [126]: http://commits.kde.org/kdenlive/159c8e4907a2deb43b424c3dcb15e44fbdec8927
 [127]: http://commits.kde.org/kdenlive/d9683c991e1342891a3007e79c73d721d54eabeb
 [128]: https://bugs.kde.org/464974
 [129]: http://commits.kde.org/kdenlive/83042c2a0a696bf77c9d7b8c5bca6a37fb6db19a
 [130]: http://commits.kde.org/kdenlive/41b74bd282e8a96be3b75f423f8101fa506d858d
 [131]: http://commits.kde.org/kdenlive/efda0ab818ab32707501a992040dd91946030cd4
 [132]: http://commits.kde.org/kdenlive/bcffeef34eb6bcdd79fa9ce515e2490128c70cf4
 [133]: http://commits.kde.org/kdenlive/76f3122a26d462f57dbab654f0727d7d6054c494
 [134]: https://bugs.kde.org/479370
 [135]: http://commits.kde.org/kdenlive/e4c248d7954c85c1d3ef7a0d6bf9e289f3075f8c
 [136]: http://commits.kde.org/kdenlive/79a374ab29de8b7e535905b247e62fae4a9aa2c2
 [137]: http://commits.kde.org/kdenlive/d642020b370c464f983933d89438f1477b15e512
 [138]: http://commits.kde.org/kdenlive/1fad61eba13ab1000edcbf9b18c8715260a81b42
 [139]: http://commits.kde.org/kdenlive/16888cdf8cd62e68c4781f06d01d4de9f778f27c
 [140]: https://bugs.kde.org/478686
 [141]: http://commits.kde.org/kdenlive/3c104cdee6abde311140187ad3f07e31b555abb6
 [142]: http://commits.kde.org/kdenlive/3acfa977512af7f48d5172caff8afbc152b181ad
 [143]: http://commits.kde.org/kdenlive/5921e805b058c376f7758f04c786604c9d65287b
 [144]: http://commits.kde.org/kdenlive/865b39f87993a9b51b56a6bfb46c5f2beb7ac2d6
 [145]: http://commits.kde.org/kdenlive/c9e5385298ba6eaafd50066550a31ece0109c930
 [146]: http://commits.kde.org/kdenlive/7f619d140214f93cf412abecc15ef956a1226613
 [147]: http://commits.kde.org/kdenlive/7739b3a47296403bc08e07ea16c9e8edb4aa769c
 [148]: https://invent.kde.org/multimedia/kdenlive/-/issues/1804
 [149]: https://bugs.kde.org/472849
 [150]: http://commits.kde.org/kdenlive/03214480ab79b49e9f31889ad6ce81a2566fd375
 [151]: http://commits.kde.org/kdenlive/48c54746de3ac521e6db2b749b176c563558e31c
 [152]: http://commits.kde.org/kdenlive/63fbe2e6b60c9cde096e13b90a9adfc1265ba8ad
 [153]: http://commits.kde.org/kdenlive/b6931168c21584b24f955ca9b608c3495052f0ba
 [154]: http://commits.kde.org/kdenlive/f9c10c37f5de54187933b2bc78aa2641ea013c5c
 [155]: http://commits.kde.org/kdenlive/15fa9613e751f49d36237fd957e7f721ff254422
 [156]: http://commits.kde.org/kdenlive/88098a2558c764c17890e8b22b03464858211f22
 [157]: http://commits.kde.org/kdenlive/3905cbac5beb57f0e9dd7b55e81d967dd33bb5e8
 [158]: http://commits.kde.org/kdenlive/2a996f6fef6cfde6bf9cb73e8908c9a54ca6c217
 [159]: http://commits.kde.org/kdenlive/10c65e2d143edc0eba63ea71b91751bdb0769e94
 [160]: http://commits.kde.org/kdenlive/12903c66ff3aa8cbcb5783b3eb6cbf834da7d22e
 [161]: http://commits.kde.org/kdenlive/b245cb98d3cdd958b7752f9f885324928cfcacd2
 [162]: http://commits.kde.org/kdenlive/a8bc671690186cc924823aad687630154a18aefd
 [163]: http://commits.kde.org/kdenlive/2b4d441482d5b1e65b670578fa39c12bb9a0a3c1
 [164]: http://commits.kde.org/kdenlive/464966c5dccb13d557a5f0943383c65b401ced42
 [165]: http://commits.kde.org/kdenlive/0f5ea279e340f92efeb78013177979383049ba4d
 [166]: http://commits.kde.org/kdenlive/cd4b361e45733ea6b8634d9c1ece683f92ab96e9
 [167]: http://commits.kde.org/kdenlive/a1e0138754f04497d535df2aef3757321a9fd526
 [168]: http://commits.kde.org/kdenlive/905daa1bc0d98ed3d8edabd99e790a815d7feae2
 [169]: http://commits.kde.org/kdenlive/f1f16fe074338cf84442258daf770c7267ac55da
 [170]: http://commits.kde.org/kdenlive/bc585037a683be9338ca4307a33da289de83b339
 [171]: http://commits.kde.org/kdenlive/8cfea6558f0825d149883079bb352745cdfa6304
 [172]: http://commits.kde.org/kdenlive/2534af5cf7ff0613a3f9f9c3f5a1e0bc17aa108f
 [173]: http://commits.kde.org/kdenlive/991708c15a5baaac3648a852e0c0de553530971e
 [174]: http://commits.kde.org/kdenlive/cbfedf20301f7e2784f5a4202ae4063a9ccec3c6
 [175]: http://commits.kde.org/kdenlive/6c70720a02d2ea1cf07fd0a3159f227e4e421527
 [176]: http://commits.kde.org/kdenlive/592a766bb3d360a8296e63ab9d9657c908f40c35
 [177]: http://commits.kde.org/kdenlive/a000d124685aaedf58bd8af2b621270ff173b538
 [178]: http://commits.kde.org/kdenlive/1219d88301db3e623765620c1a3c52a19f3a722c
 [179]: http://commits.kde.org/kdenlive/15ff451bb3236d9ca9e92a7186df26599aa8bfc8
 [180]: http://commits.kde.org/kdenlive/8bd053fffd886088e46ae86e9aa0b880f2713e08
 [181]: http://commits.kde.org/kdenlive/339869651fa12c721d789a5a8dbd7dc734c62964
 [182]: https://bugs.kde.org/478745
 [183]: http://commits.kde.org/kdenlive/895dff094ed0053847e4cae742b9f5e7b65e0304
 [184]: https://bugs.kde.org/478688
 [185]: http://commits.kde.org/kdenlive/63d820a4aca55b3362efcbcd97d8d735821058df
 [186]: http://commits.kde.org/kdenlive/2d48bddd3f5a62ada1634402061c2ccd64af2f87
 [187]: http://commits.kde.org/kdenlive/9a864f8a1bbefb2d7f53122e2387163c844da5b4
 [188]: http://commits.kde.org/kdenlive/4c8d3403290b8fb6e9b332f94bbdd68a733d46e2
 [189]: http://commits.kde.org/kdenlive/5bc7dfa8030e9e305fab7d9af001ce71bdeb92ae
 [190]: https://bugs.kde.org/478660
 [191]: http://commits.kde.org/kdenlive/595df27561448484f11f9e961c25a0b1635c04af
 [192]: http://commits.kde.org/kdenlive/5665ab494b3f860df84e0810c26a2bf6376566f6
 [193]: https://bugs.kde.org/478685
 [194]: http://commits.kde.org/kdenlive/16fb257043db431862a8c1862f6fa6026a1a8a49
 [195]: https://invent.kde.org/multimedia/kdenlive/-/issues/1786
 [196]: http://commits.kde.org/kdenlive/54fa7802df4005bc8dd74a3f5f3dec0a9ae5363b
 [197]: http://commits.kde.org/kdenlive/e7804ec0a70d1a96c7a0ee624225d171cd8a4adc
 [198]: http://commits.kde.org/kdenlive/ed0cb840beaab19ae83d01637fd3362e91196826
 [199]: http://commits.kde.org/kdenlive/fdc375c92d40102eeb8253bac1ce1dd407a2d966
 [200]: http://commits.kde.org/kdenlive/fbe28b20b09a51f97f26671c90b4f605a7c3103a
 [201]: http://commits.kde.org/kdenlive/b1da177c002c5abfa3e096fef48b6306da996fa2
 [202]: https://bugs.kde.org/442088
 [203]: http://commits.kde.org/kdenlive/6f91539ab0e6c6699156dcd85e0349ea74d2bbe3
 [204]: http://commits.kde.org/kdenlive/ab2169122e150697bf07f9f41695310df685076b
 [205]: http://commits.kde.org/kdenlive/fc3eb0b43282f22d07919a200d2e609ad3645d30
 [206]: https://bugs.kde.org/476716
 [207]: http://commits.kde.org/kdenlive/7ebfade290a833891d46f93b336fc305014e8dab
 [208]: http://commits.kde.org/kdenlive/5a083f2c2db94d20aed2358d71a8d78d16127323
 [209]: http://commits.kde.org/kdenlive/b404f5838bb768cc56cf82302bde7e3b7ff1f532
 [210]: http://commits.kde.org/kdenlive/a83e15a04a64894e549e7dfe0dbf514e89c31326
 [211]: https://invent.kde.org/multimedia/kdenlive/-/issues/1800
 [212]: http://commits.kde.org/kdenlive/72deeb4b3aa173c5e86f731c3f450fd8dc3caf35
 [213]: http://commits.kde.org/kdenlive/4eb40a8421b78a82717d5fc1f1bd12be27b1f187
 [214]: http://commits.kde.org/kdenlive/2f452fc71904ddef3a6745d391148aed50a57ac6
 [215]: http://commits.kde.org/kdenlive/f9afcc9eb5b258a808ac2047f1280cb51de348ab
 [216]: http://commits.kde.org/kdenlive/442cfbf7df9e3cd1f03e88e05b9e2ee92ff0e613
 [217]: http://commits.kde.org/kdenlive/3b5a7c1ab7a2e5e7ad3bd32c8b64fb928580d123
 [218]: http://commits.kde.org/kdenlive/409401b4ca4e5f7ddabbea3c4faed157dfa1e760
 [219]: http://commits.kde.org/kdenlive/8b039ed318a3f6618b4b93e7f924591674c3d93f
 [220]: http://commits.kde.org/kdenlive/8a7ed69df3b6588a7b372ce704ede829a7a752f8
 [221]: https://bugs.kde.org/477771
 [222]: http://commits.kde.org/kdenlive/34e644dba8a2131aea890f7bac75776483e7fa28
 [223]: https://bugs.kde.org/477617
 [224]: http://commits.kde.org/kdenlive/9b0c80095fd3221c58eb6c9e0e918db07ea0caf0
 [225]: http://commits.kde.org/kdenlive/72d062ced7a3b565beae4e8c664daa1473a9bc3d
 [226]: http://commits.kde.org/kdenlive/1cf8af8690805774ee57e8d336ef8d00de0798f9
 [227]: https://invent.kde.org/multimedia/kdenlive/-/issues/1327
 [228]: http://commits.kde.org/kdenlive/f944f6108ba7ae84dbf088ac04be4e8b2cced928
 [229]: http://commits.kde.org/kdenlive/cef2037aa619d6aafec53eed66b5afe5a0cb6790
 [230]: http://commits.kde.org/kdenlive/c638a54404d45851e66d4dc6af0ba74711352406
 [231]: http://commits.kde.org/kdenlive/33a9731f86366b936ed5eddbbd77285751431bd5
 [232]: https://bugs.kde.org/475029
 [233]: http://commits.kde.org/kdenlive/6e58cba3f9aef7578df95dad24e3231e98fa2f2f
 [234]: http://commits.kde.org/kdenlive/43857f1508bdc6838780fbed3e7e1122e66ef0bb
 [235]: http://commits.kde.org/kdenlive/6484f91c69fa920920ea809c3efcb2156927bed3
 [236]: http://commits.kde.org/kdenlive/fbef83b0fb68407919e4fea9d9c384b827bef57a
 [237]: http://commits.kde.org/kdenlive/4a6ea1d314862db20cf5572153601ba881067faa
 [238]: http://commits.kde.org/kdenlive/f2300e7153d445845fa403e68c9373cf74494619
 [239]: http://commits.kde.org/kdenlive/8f78924854f2650a1490039d9ca2b73e166b8cdb
 [240]: http://commits.kde.org/kdenlive/64e346bcc77572f1f8d54f448b0492fbec9e712e
 [241]: http://commits.kde.org/kdenlive/07711507549c919497254028dd6a96d1887bfcd5
 [242]: https://invent.kde.org/multimedia/kdenlive/-/issues/445
 [243]: http://commits.kde.org/kdenlive/0a2411aaf0c6183bfdeaaf9d630cf922b57c8d4b
 [244]: http://commits.kde.org/kdenlive/9476536118c1f079450fa930a52ec5e69cd82789
 [245]: http://commits.kde.org/kdenlive/60a922adab246eb88b683c7e219a4a4dcb5a86a0
 [246]: http://commits.kde.org/kdenlive/299bfa750cd448ee2e255846ca39cccfed8ebfe2
 [247]: http://commits.kde.org/kdenlive/d7967bf501f1df9f0020401f749794888678f99b
 [248]: http://commits.kde.org/kdenlive/3aba11ca37aca20804d1eb9cc0c9b79e098783ab
 [249]: http://commits.kde.org/kdenlive/93a77653d71ba62a0bb4a914471999ae724e2752
 [250]: http://commits.kde.org/kdenlive/f76888f80e45654358f30bbf4ca04235e269cc49
 [251]: http://commits.kde.org/kdenlive/589e9a73994266531b25bb1f7de58149fa903fb1
 [252]: http://commits.kde.org/kdenlive/807d0ddd9a788371c7347341a0a315feb28cb0b3
 [253]: http://commits.kde.org/kdenlive/a2ac5956893899715ff520ecfe2193844b492de2
 [254]: http://commits.kde.org/kdenlive/41b21301005e14ea759842cd8edee5feaf441488
 [255]: http://commits.kde.org/kdenlive/7edaffc602503e61bef73327965786727bf782f7
 [256]: http://commits.kde.org/kdenlive/498c790fda6957c88c1086fb082f6ebc7ce252cb
 [257]: http://commits.kde.org/kdenlive/12c29996f60c81e2175c8812eb407fd5233e2b33
 [258]: https://invent.kde.org/multimedia/kdenlive/-/issues/1785
 [259]: http://commits.kde.org/kdenlive/51ca0609f81dec333214a62fd4af60caa359717f
 [260]: http://commits.kde.org/kdenlive/0ee4463b544719a3793e11a0bd529b15446419b5
 [261]: http://commits.kde.org/kdenlive/f8ec8a54c7bbe7a5eba5164061c95ba60d79dbcb
 [262]: http://commits.kde.org/kdenlive/202a4b268da3ed7a060ba2b18ff190ec6a07b221
 [263]: http://commits.kde.org/kdenlive/8c6980cff5bc61d59b042c611bf1066445a5d51d
 [264]: http://commits.kde.org/kdenlive/23c3ff1688f311dc0a0e24b2393e99eb5ef4100a
 [265]: http://commits.kde.org/kdenlive/397588d06b72d6e98264fae53862380516305c14
 [266]: http://commits.kde.org/kdenlive/9a6779e24257edab830147c97cefbd12169cf769
 [267]: http://commits.kde.org/kdenlive/17faca7c7962bb1819eb0b204348f96250c12265
 [268]: http://commits.kde.org/kdenlive/fd21ebec903116f7e4e4aa63b8af560ea5012d30
 [269]: http://commits.kde.org/kdenlive/ee3b9ea2c5dfc2a8a494ad72f2df61d9b812b362
 [270]: http://commits.kde.org/kdenlive/dfd6974cd29a011eec404bcb50846f98dcf7404e
 [271]: https://invent.kde.org/multimedia/kdenlive/-/issues/476754
 [272]: http://commits.kde.org/kdenlive/10ffbee0443496898c54d418c265b66a2188ca99
 [273]: http://commits.kde.org/kdenlive/7841d94c5d5da304a14afd52a046811b7c3a887d
 [274]: http://commits.kde.org/kdenlive/10b5764437b46e5d32a59347c910b0462fa38f76
 [275]: http://commits.kde.org/kdenlive/8669befcac8067e11c8b6c11fe62fdaa509c3fb7
 [276]: http://commits.kde.org/kdenlive/246a87a41223f324e8e25ac80a04a91f1a4e8344
 [277]: http://commits.kde.org/kdenlive/352e6f9111e64a21df67d878ac3b561324638dba
 [278]: http://commits.kde.org/kdenlive/fda32639b24c6e53250731ec65626f2f66500059
 [279]: https://bugs.kde.org/471359
 [280]: http://commits.kde.org/kdenlive/ec072f528ee62f7d2dec16ee6409208a6ab62e4a
 [281]: http://commits.kde.org/kdenlive/31809c20fe9fa939a8ccd5958515f3669e72d3e7
 [282]: http://commits.kde.org/kdenlive/ca78b3a85d6a4492edf94bc00848b127220d5cbd
 [283]: http://commits.kde.org/kdenlive/db956277068def5833c7939310f5d6d34538247b
 [284]: http://commits.kde.org/kdenlive/4c7bb0e4f11b6aa0c0b62d3dbd445e5676b44189
 [285]: http://commits.kde.org/kdenlive/b89b5586d1ddb89fd2b0d83045c87d97709db562
 [286]: http://commits.kde.org/kdenlive/58e9d76d375c59e6fd9c2bdc128edf9d660d950f
 [287]: https://bugs.kde.org/476612
 [288]: http://commits.kde.org/kdenlive/b2327cd8a92c2b0c4d9d5892ef5cb7cf270c708e
 [289]: http://commits.kde.org/kdenlive/c475271b0cd6a37f44327ddf1d4e9392e99136cb
 [290]: http://commits.kde.org/kdenlive/50d1faeb99b1b0b16ebdb73dc490de6a87f49a64
 [291]: https://invent.kde.org/multimedia/kdenlive/-/issues/1767
 [292]: http://commits.kde.org/kdenlive/20cd25d75ec46793324ee8ad16fa7db85af90a44
 [293]: https://bugs.kde.org/476493
 [294]: http://commits.kde.org/kdenlive/361397bc05d5eecec06de045c2a30f0c2f39848e
 [295]: http://commits.kde.org/kdenlive/8258190359f04286a83955c02e36e82b0c1f2a97
 [296]: http://commits.kde.org/kdenlive/73a1c265e84641f6e0b7962514a76c2b85c506fc
 [297]: https://bugs.kde.org/476544
 [298]: http://commits.kde.org/kdenlive/09bb185e21e695ed3fbd959cbe591622b5c8ded7
 [299]: http://commits.kde.org/kdenlive/c87979cea7ac417c23268a1842cfdfa3f8ecfb2a
 [300]: http://commits.kde.org/kdenlive/79b0c5291b46945dbacbb28774e468e409466047
 [301]: http://commits.kde.org/kdenlive/d6da8910a1fe2f122517a2747a4daa63e552a6cf
 [302]: http://commits.kde.org/kdenlive/869964e34ca9b8fef7d6bfbac7a191a805fe8274
 [303]: http://commits.kde.org/kdenlive/81d5f4e1aeb6611286d69b385320b38b772a6858
 [304]: https://bugs.kde.org/476393
 [305]: http://commits.kde.org/kdenlive/d4704a90ab7e599559275cb0247dbd416ebe27bd
 [306]: http://commits.kde.org/kdenlive/219972f045e864dba40ffd018eb58b0ebaa06974
 [307]: http://commits.kde.org/kdenlive/44df76d8b98037777a0824df77884396aa283948
 [308]: http://commits.kde.org/kdenlive/db63af6423020bd070a7dd6e097eafed8e32accc
 [309]: http://commits.kde.org/kdenlive/0f4f14a772cd68ffb6ac4369a6e0eaf4a21d4053
 [310]: http://commits.kde.org/kdenlive/938fa1ede9ffab4018acdb94dc53281eeead4a52
 [311]: http://commits.kde.org/kdenlive/b8253ccab466153be57477aafaa367db59224789
 [312]: http://commits.kde.org/kdenlive/1ffa013a7336530cb82ed88ce35811ab97b5f8d7
 [313]: http://commits.kde.org/kdenlive/42c5e4b0aabc8e60651b01b7787f0c9e0fe9a15f
 [314]: http://commits.kde.org/kdenlive/10f72c78705db00ed22ff8cb352b60cc0380e114
 [315]: http://commits.kde.org/kdenlive/d701e5680508fecd263d50d34400a3bed9910942
 [316]: http://commits.kde.org/kdenlive/72486f22b1b44dacf935948d1bc06b3ddb6ed0b4
 [317]: http://commits.kde.org/kdenlive/e614e9372667af765470d532c6db9e867bad2690
 [318]: http://commits.kde.org/kdenlive/c420ba796a6b7e25bad466863d9255952142cebb
 [319]: http://commits.kde.org/kdenlive/29e5e44f9eeaa78fb1e8bbe5280ad21462d0924a
 [320]: http://commits.kde.org/kdenlive/b4829698b93216749c5e3aa2b74d7380973c1264
 [321]: http://commits.kde.org/kdenlive/2f67613f07069970200264bfd168613afe410451
 [322]: https://bugs.kde.org/475980
 [323]: http://commits.kde.org/kdenlive/04884a43987604b54b0525c81269eca2dd5e010d
 [324]: http://commits.kde.org/kdenlive/9cdf7cffb91b4725c2b10c5310f41dd1732f44a3
 [325]: http://commits.kde.org/kdenlive/1deb87b1ec8ec68b895848862323d1ea7874282f
 [326]: http://commits.kde.org/kdenlive/880ece8582f820a9d9123809a91d7b5860307bd1
 [327]: http://commits.kde.org/kdenlive/fdd4bf7c90d2f59f2c39d3b32d429c2512e882eb
 [328]: http://commits.kde.org/kdenlive/4d6f30d75356d4574151c82e2383ab836f73ecab
 [329]: http://commits.kde.org/kdenlive/58774336ec38e62ff4324c7cae3a68c560d20032
 [330]: http://commits.kde.org/kdenlive/fd696ab9d48d23efcb5d15702f06cf9aca00a900
 [331]: http://commits.kde.org/kdenlive/8fb4adb50f078f3cc8a0e9c321a65c02bd79db92
 [332]: http://commits.kde.org/kdenlive/0c96c3c19ac4a666992dcace1ab8d08b8c27dd00
 [333]: https://invent.kde.org/multimedia/kdenlive/-/issues/1605
 [334]: http://commits.kde.org/kdenlive/efb26708b0d58a9c44e578abe71ca9d46352d4bb
 [335]: https://invent.kde.org/multimedia/kdenlive/-/issues/1726
 [336]: http://commits.kde.org/kdenlive/49ad51c3956b74ef6f0377c83a63ed43e20a1436
 [337]: http://commits.kde.org/kdenlive/4a50355ca6c1bf70e70d6240848f1ecedce7f722
 [338]: http://commits.kde.org/kdenlive/7e555a72ae400b462db89e4f94ee469e1b50c799
 [339]: http://commits.kde.org/kdenlive/26e5a1e089c73f4737b5f78c305df863fd92f7c2
 [340]: http://commits.kde.org/kdenlive/d42647aac2443becc126991a33b2a9b3f5005e4d
 [341]: http://commits.kde.org/kdenlive/9ce9bb20037c2e2da2845cef2d90bee2099454be
 [342]: http://commits.kde.org/kdenlive/6fcc2df23c005bc4bd55b3cc9ccbd9da315aa852
 [343]: http://commits.kde.org/kdenlive/7d0639629f63cf2ad678311fc91f1c539d807d72
 [344]: http://commits.kde.org/kdenlive/63332f680a9ffa5786bd1802dbfbfb5f67c9f7c4
 [345]: http://commits.kde.org/kdenlive/4992d801a8336f04b002c25fd1c2b1fe792168d0
 [346]: http://commits.kde.org/kdenlive/b652532cdfdbd7d391f1c1afbe7fc8eee4f50199
 [347]: http://commits.kde.org/kdenlive/eb7ef279aea3e204e4a8ca422fd56639965e5046
 [348]: http://commits.kde.org/kdenlive/4fc57544d44f22e30d2b9bbe4c918c895040ac74
 [349]: http://commits.kde.org/kdenlive/c996ae258fe8e5d1718e208144688376b9ec9e9e
 [350]: http://commits.kde.org/kdenlive/df372375671e31f9a13fb82009dbf7c5e1f7fee7
 [351]: http://commits.kde.org/kdenlive/8916d5e7190c98e3872610ca5f971dd84ed98ba8
 [352]: http://commits.kde.org/kdenlive/120af1da376572a665ff5c280ddd41386192f2fa
 [353]: http://commits.kde.org/kdenlive/18c7d093076d640571bd546edf5c130a889c96a5
 [354]: https://invent.kde.org/multimedia/kdenlive/-/issues/1766
 [355]: http://commits.kde.org/kdenlive/bc3cddc34f4f29a62f4c7185d560eaf5d1b1c14f
 [356]: https://invent.kde.org/multimedia/kdenlive/-/issues/1768
 [357]: http://commits.kde.org/kdenlive/ad7adf20cd46bb00dfc6d5f0ae0ad959d0f36227
 [358]: http://commits.kde.org/kdenlive/72ba33eda7cc208d52f89a9c49e99209a1823d94
 [359]: http://commits.kde.org/kdenlive/6e53898cf7dd6896fd3e6ac89546cedb0b9dbb75
 [360]: https://invent.kde.org/multimedia/kdenlive/-/issues/1769
 [361]: http://commits.kde.org/kdenlive/9a97bbf63072a0f8b6c2f80ece145b011e0a3a09
 [362]: http://commits.kde.org/kdenlive/21e262c52cd0e54e9cee9af4a673bd7dad63580e
 [363]: http://commits.kde.org/kdenlive/b6b6d95d6b103ffbc2ebb1386fb6502e128f235b
 [364]: http://commits.kde.org/kdenlive/78354810626a5aaf777e2907becee52bd2587658
 [365]: http://commits.kde.org/kdenlive/24117d081f8b7b676cc0cb1c3f3cd3708282bcf0
 [366]: http://commits.kde.org/kdenlive/0229d074c1ba59ec9232e4eb0d8fa63ab9b8f13d
 [367]: http://commits.kde.org/kdenlive/dbf6e6cb209a6298465c7e8d815f71ff75bb5a03
 [368]: http://commits.kde.org/kdenlive/f4b7d4007a336489accdfbaa57387f2db1726fb4
 [369]: http://commits.kde.org/kdenlive/ff09e054d4fa82207d1c0bd0226fc54adf8cbbcb
 [370]: http://commits.kde.org/kdenlive/f3e7bef9e1a0beed7af7c1a64d76418414c83784
 [371]: http://commits.kde.org/kdenlive/8de56f0cd508e73cc584e2296a39652b775f8dfe
 [372]: http://commits.kde.org/kdenlive/d28f735d8c60553d6085ca296ecf1b5d5c0bab24
 [373]: http://commits.kde.org/kdenlive/0c65b5ed51d82552fd6e27f03539d44d3eb8d45c
 [374]: http://commits.kde.org/kdenlive/202cfc5b4feb66370d2907191c04d7e9dcb32840
 [375]: http://commits.kde.org/kdenlive/22b93bbb67f68a9dc7609a3ea7a6783ad137da95
 [376]: http://commits.kde.org/kdenlive/65278b82ecf8fb46acbdb918c1b9bd309dec6976
 [377]: http://commits.kde.org/kdenlive/b93226c379e3fe999a045c95812176be2e77c1bc
 [378]: http://commits.kde.org/kdenlive/5161c4781735d39f6bd1efd3eb539dc4504f08ca
 [379]: http://commits.kde.org/kdenlive/93e309673f48a46faaec3b8329ccbf3a1d8ff2e9
 [380]: http://commits.kde.org/kdenlive/5f22ffea0c6a3a19dd957482f47dc378357bfd5a
 [381]: http://commits.kde.org/kdenlive/c15951080cb0ecc2676114d6156ccdb88a4f6f14
 [382]: http://commits.kde.org/kdenlive/f76e24a0b00b441a87466943ce6a3b318574510e
 [383]: https://bugs.kde.org/474895
 [384]: http://commits.kde.org/kdenlive/99d1d6d8bf28821336750a288b063ec77ea49064
 [385]: http://commits.kde.org/kdenlive/689528dc78b57d37452a6593306ea5e9d78fe70b
 [386]: http://commits.kde.org/kdenlive/b400387383c47c370a322d7c4215d3b90eb41515
 [387]: http://commits.kde.org/kdenlive/ccc30ebc3f9498d2fd88a6603684a79957371e11
 [388]: http://commits.kde.org/kdenlive/2b90c747ef47e3a97f0924ad9f1d59c00f0d5948
 [389]: http://commits.kde.org/kdenlive/54ed2e8acfe85d035efab1585afcce9f666f308a
 [390]: http://commits.kde.org/kdenlive/53d05995936053a9fd679b47d0cbe2e4063831f3
 [391]: http://commits.kde.org/kdenlive/559a2bfddaacc684592ff24e672dda8a2cb2f413
 [392]: http://commits.kde.org/kdenlive/1f288e3dfcc04aa194221261d658e2d54c6c953a
 [393]: http://commits.kde.org/kdenlive/c119db4499b10bfb1676a8dcd7a11dc5f6f74dd3
 [394]: http://commits.kde.org/kdenlive/4e417b843fc9338fdabd83cbc1edcfd76950f70d
 [395]: http://commits.kde.org/kdenlive/75ee756eaf9482b4d1b645d4f5eb1ccd77b7150e
 [396]: https://bugs.kde.org/474988
 [397]: http://commits.kde.org/kdenlive/55f418c136886f40e24a1545fa72a366715b3681
 [398]: http://commits.kde.org/kdenlive/32233b801475a3c311cba0c3242d9cc72c78e552
 [399]: http://commits.kde.org/kdenlive/087a6a5a5a1519eb50b7f69c263ba1e78f51c5c5
 [400]: http://commits.kde.org/kdenlive/fb29033a3d68eca5b1b7215197ce98b810d65715
 [401]: http://commits.kde.org/kdenlive/8a928e66485052ae15b1888840642fab84a840d1
 [402]: http://commits.kde.org/kdenlive/6bee09f9cad7c20881213d7990d9e0c7e001af2c
 [403]: https://invent.kde.org/multimedia/kdenlive/-/issues/1612
 [404]: http://commits.kde.org/kdenlive/eb56bb12cfc9c4f05c4fb8971b498456de8c851e
 [405]: https://invent.kde.org/multimedia/kdenlive/-/issues/1751
 [406]: http://commits.kde.org/kdenlive/cda16f42b0e6991e368cf39b144d10a6cc10432f
 [407]: https://bugs.kde.org/447948
 [408]: https://bugs.kde.org/436762
 [409]: http://commits.kde.org/kdenlive/5a7d0b330069e144ab3c238b3f562440d08f3103
 [410]: http://commits.kde.org/kdenlive/48515473d3d2d3b193dee7342585be9ae13fee66
 [411]: https://bugs.kde.org/472865
 [412]: https://bugs.kde.org/462406
 [413]: http://commits.kde.org/kdenlive/37dfd6f233ea409afd7bddb68e129824fd11f632
 [414]: http://commits.kde.org/kdenlive/778cb0a6d2b343a8c8c62f0dbb1f545c737cfda7
 [415]: http://commits.kde.org/kdenlive/276b75e320e965ceed128618d244bae328070c5d
 [416]: http://commits.kde.org/kdenlive/d479d8819f8ce97d437ee076d0abd951c3cd4180
 [417]: http://commits.kde.org/kdenlive/67768c4b58ff514af47d69b8150015b333e45c1d
 [418]: http://commits.kde.org/kdenlive/a7ec7e45b5dc8e73563c6776a2e200d6956f5c82
 [419]: http://commits.kde.org/kdenlive/59fdf99866b144b0680934fc965cfdc1d23c1f38
 [420]: http://commits.kde.org/kdenlive/9c0791f2c8428316890af73481e1ee82ac1577e4
 [421]: http://commits.kde.org/kdenlive/af7afdd3fff0cbfce4a3bf8b48e68dcab476058f
 [422]: http://commits.kde.org/kdenlive/71b907e8243d2638cbb2e958633c5c864e58fa99
 [423]: http://commits.kde.org/kdenlive/919417f882026f56ea409b3c1b84819383d4da97
 [424]: https://invent.kde.org/multimedia/kdenlive/-/issues/1753
 [425]: http://commits.kde.org/kdenlive/71bc55787f51df687042f4859fd58f1e37c6f568
 [426]: http://commits.kde.org/kdenlive/743aa18107386fdb4fa2feddcfddf6d4a42ea5f4
 [427]: http://commits.kde.org/kdenlive/1011df4ebb1ffe5cf51d25e396258ed8ff51bcf4
 [428]: http://commits.kde.org/kdenlive/90bbc12eddf0a32e8d68bc9b3ac212e7247db184
 [429]: http://commits.kde.org/kdenlive/7eacf89ae40b9ed96a91a8808e4f8e07570a37b7
 [430]: http://commits.kde.org/kdenlive/7d71b025a8ee6ea7e2b6509635f48a9c6fbc007f
 [431]: http://commits.kde.org/kdenlive/be69ed97e56678e72f5251d2fd99425422c9df0a
 [432]: http://commits.kde.org/kdenlive/0862c19021edfa0ce7dc5b43a3a27b02a9f4ccfe
 [433]: http://commits.kde.org/kdenlive/fe30d719fc570443cff3d1177d6a9050e2aa0e42
 [434]: http://commits.kde.org/kdenlive/3bb2f08efcbd16048133f6aa2686d9b13702e2be
 [435]: http://commits.kde.org/kdenlive/ae714fb9e5fd094d86f1d09150871545fd0fe929
 [436]: http://commits.kde.org/kdenlive/0568517de4b011cdde36ed173daef8191e89ec71
 [437]: http://commits.kde.org/kdenlive/d94257c8ac2a8752049acc88dd84097816aa1d95
 [438]: http://commits.kde.org/kdenlive/048053bbaac56ca2df92d476f73e49380d50b53b
 [439]: http://commits.kde.org/kdenlive/4c34abc4270692a5a5770d5ba3771e5ad3fa7e27
 [440]: http://commits.kde.org/kdenlive/f341e55c4a39599b8758d54f51dd1b73965bcd48
 [441]: http://commits.kde.org/kdenlive/d7d86c98b5579dca3ec7c16abf26dd2ee5fe4b53
 [442]: https://invent.kde.org/multimedia/kdenlive/-/issues/1744
 [443]: http://commits.kde.org/kdenlive/985b129c7beb3ffa123366b21a06e5079ca66101
 [444]: http://commits.kde.org/kdenlive/8af3465391e265125d9daa4382ce59a43ef9a601
 [445]: https://invent.kde.org/multimedia/kdenlive/-/issues/1748
 [446]: http://commits.kde.org/kdenlive/0d6ce68c888e134e5262a349092bf26f1e0767c6
 [447]: http://commits.kde.org/kdenlive/c91a8cd32ab5085cc86702139a2440cb5a3b98f9
 [448]: http://commits.kde.org/kdenlive/84ede0df7a2c0064423cd7b2ace8dc61d1eb3fc0
 [449]: http://commits.kde.org/kdenlive/cd699619fb329b27fe29b75aecdf19db8572cd60
 [450]: https://invent.kde.org/multimedia/kdenlive/-/issues/1747
 [451]: http://commits.kde.org/kdenlive/edb2c9a7334d5471f68114ec98241a4eb787642a
 [452]: http://commits.kde.org/kdenlive/0f973fdb6a37d8d3dbbf208e6995e86a84ff77be
 [453]: http://commits.kde.org/kdenlive/e7b9a18dc66db74c9c9f7397d742bbf11aaac43e
 [454]: http://commits.kde.org/kdenlive/3490ed6292778ef50360fb149c3e9bd39ab8b600
 [455]: http://commits.kde.org/kdenlive/fb6d2c52884356798b4739d0e4696a1bd0e0072c
 [456]: http://commits.kde.org/kdenlive/e7366c2959c47ba39115901b3fa0d9133bafa622
 [457]: http://commits.kde.org/kdenlive/a0714430caa4ee4460bf717662f9e2876c29fedb
 [458]: http://commits.kde.org/kdenlive/a8225ad9a773c3ee572b5305032cc2b8b93313c9
 [459]: http://commits.kde.org/kdenlive/61031df6c65cf2e22388596e973d7e8acd46d3be
 [460]: http://commits.kde.org/kdenlive/9e43257dd9cfb97f347760a8dd82f584fe469261
 [461]: http://commits.kde.org/kdenlive/78f287ec54a3042ec01ce12d87a423beb566db46
 [462]: http://commits.kde.org/kdenlive/0e5e5882fca242295d97d678747c216e12e368af
 [463]: http://commits.kde.org/kdenlive/82ac1a06ca3fe2663e6ee01e1d5c0b564836f700
 [464]: http://commits.kde.org/kdenlive/e97618d924fc69dccbc18ab74dd73953754307ef
 [465]: http://commits.kde.org/kdenlive/b67c83e476327b96a34889a1dfeb8935e33a4805
 [466]: http://commits.kde.org/kdenlive/445f56256c7452fa8c146a1e8cf5638dfcf1eba3
 [467]: http://commits.kde.org/kdenlive/ac2b96c0f43eeec0376f6ce8e251109701df0136
 [468]: http://commits.kde.org/kdenlive/735be9253ad25a5e916989c7f4c00f54aa465b52
 [469]: http://commits.kde.org/kdenlive/cdf161dd6b18cd9ca608d95ec371d5f58383e506
 [470]: http://commits.kde.org/kdenlive/66fa5c300e7f93922331ce863b110c069cb6faec
 [471]: https://invent.kde.org/multimedia/kdenlive/-/issues/1405
 [472]: http://commits.kde.org/kdenlive/6bdf0a628dbe8ee4ffcba25fe5588050f320372d
 [473]: http://commits.kde.org/kdenlive/33c9850b40a39c83ab2813e125a25925c75ab492
 [474]: http://commits.kde.org/kdenlive/20107347fcbb25ecfc3eeee8e372bc28b6d1220d
 [475]: https://bugs.kde.org/451936
 [476]: http://commits.kde.org/kdenlive/f3aee46d69177c45eb9fa6b8868e9eb3b728607a
 [477]: http://commits.kde.org/kdenlive/53d919dd40cb2966cd934baf57b1eb6f3fef9995
 [478]: http://commits.kde.org/kdenlive/30ee10edaed8f19a2672f5aad070a0d79492c5c1
 [479]: http://commits.kde.org/kdenlive/70eaed57cc55095aa82b89e27f33bb37b09db5aa
 [480]: http://commits.kde.org/kdenlive/17b5f18a02f4a15cdb24a498c0a1fb2c742ec7f2
 [481]: http://commits.kde.org/kdenlive/c80413a9e7058837b626772401ae5548ddea87f9
 [482]: http://commits.kde.org/kdenlive/e2d2c8e08d1d635756cfa03ea0de8b725726ee2f
 [483]: http://commits.kde.org/kdenlive/037f58c76fcada37b0242ebf05a425b08ce53e17
 [484]: http://commits.kde.org/kdenlive/8c4eea726654f0eb74b015e7c3f1f44115a3d99b
 [485]: http://commits.kde.org/kdenlive/a44ac3a048a162917c4c68777d30bdf41ad92dbc
 [486]: http://commits.kde.org/kdenlive/a23b46ee135c1786c26b49f0a737fd4e29302bed
