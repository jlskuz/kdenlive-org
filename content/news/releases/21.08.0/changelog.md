
  * Fix timeline preview broken in recent change. [Commit.][2]
  * Frei0r.alphaspot: Implement copy position to mlt_rect effects. [Commit.][3]
  * Add monitor scene for frei0r.alphaspot. [Commit.][4]
  * Make more effects work with mask_start: rotoscoping, shape. [Commit.][5]
  * Add dependencies and category for mask\_start and mask\_apply. [Commit.][6]
  * Add dependency system for assets to depend on other assets. [Commit.][7]
  * Don’t allow deleting items when a spacer move operation is running. [Commit.][8] Fixes bug [#408434][9]
  * Correctly select a clip in clip monitor after it is added to project. [Commit.][10]
  * Color picker: correctly disable clip effect when trying to pick a color from the screen. [Commit.][11]
  * Fix crash moving clip with speed effect and mix. [Commit.][12] Fixes bug [#433579][13]
  * Fix crash on project with channels != 2 and track muting. [Commit.][14] See bug [#439837][15]
  * Fix compilation warning. [Commit.][16]
  * Add xml for filter masks. [Commit.][17]
  * Remove track “preview” compositing that is deprecated. [Commit.][18]
  * Time remap: fix blend mode set to on by default, ensure the last keyframe is at length+1 to avoid preview scaling artifacts. [Commit.][19]
  * Fix preview scaling switches monitor focus to clip monitor. [Commit.][20]
  * Fix timeremap requesting invalid clip index (producer is not inserted in a track at that point). [Commit.][21]
  * Fix muting audio track broken with mixes. [Commit.][22] See bug [#440019][23]
  * Fix crash on render/save when subtitle track was last active track. [Commit.][24] See bug [#439355][25]
  * Fix detection of MLT on Windows. [Commit.][26]
  * Fix crash on closing project with subtitles. [Commit.][27]
  * Fix some warnings. [Commit.][28]
  * Fix tab order in Kdenlive Settings misc dialog. [Commit.][29]
  * Fix timeremap crash on undo/redo. [Commit.][30]
  * Fix geometry param (like tracker) not restoring value after save. [Commit.][31]
  * Restore timeremap keyframes on effect deletion undo. [Commit.][32]
  * Fix motion tracker effect broken for timeline cut clips. [Commit.][33]
  * Fix timeremap widget not enabled in some cases. [Commit.][34]
  * Ensure markers are properly sorted in Clip Properties dialog, enable F2 rename. [Commit.][35]
  * Fix position and zoom effect and affine composition broken with switch to MLT7. [Commit.][36]
  * Fix audio thumbs missing on clip reload (like when changing autorotate value). [Commit.][37]
  * Move remap marker before clip name, fix qml warning. [Commit.][38]
  * TImeline guides: add delimiter and highlight active one. [Commit.][39]
  * Disable time remap for color or image clips and clips with speed effect. [Commit.][40]
  * Various fixes for timeremap. [Commit.][41]
  * Timeremap: don’t seek on drag start (caused delay), add snap to start/end of clip. [Commit.][42]
  * Fix timeremap keyframe grab zone. [Commit.][43]
  * Removing a remap effect now restores input duration. [Commit.][44]
  * Fix mix direction lost on save / change track, fix clip offset on vertical move while deleting start mix. [Commit.][45]
  * Fix timeremap undo/redo to resize clip in one pass. [Commit.][46]
  * Small update for timeremap ui. [Commit.][47]
  * Use KDE\_INSTALL\_QTQCHDIR to install QCH documentation. [Commit.][48]
  * Fix various mix move issue and tests. [Commit.][49]
  * Fix various time remap display glitches, only resize timeline clip on mouse release. [Commit.][50]
  * Make time remap a checkable option in the context menu so it can easily be removed. [Commit.][51]
  * Fix crash on mix group move, fix mix sometimes refusing to be created. [Commit.][52]
  * Time remap: display negative speed where it makes sense. [Commit.][53]
  * Fix possible crashes in timeremap. [Commit.][54]
  * Time remap: fix expanding clip creating extra keyframes. [Commit.][55]
  * Fix another grouped mix move crash. [Commit.][56]
  * Fix crash on grouped mix deletion. [Commit.][57]
  * Properly set default label for guides. [Commit.][58]
  * Fix various timeremap inconsistencies. [Commit.][59]
  * Various timeremap UI and workflow fixes. [Commit.][60]
  * Fix various mix move issues. [Commit.][61]
  * Show edit dialog on guide creation via “Add/Remove Guide”. [Commit.][62]
  * Fix: always enable marker actions to make them usable in clip monitor. [Commit.][63]
  * Fix cannot move grouped clips with mix. [Commit.][64]
  * Drop custom combobox stylesheet causing unreadable text. Thanks to Martin Sandsmark for the hint. [Commit.][65] Fixes bug [#428755][66]
  * Revert “Fix clips with mix cannot be move onto another track.”. [Commit.][67]
  * Fix clips with mix cannot be move onto another track. [Commit.][68]
  * Remove seek checkboxes in timeremap, add button to center keyframe at cursor position. [Commit.][69]
  * Don’t stop preview render when editing outside of the preview zone. [Commit.][70]
  * Fix reverse wipes. [Commit.][71]
  * Mix alignment: remember and adjust resize accordingly. [Commit.][72]
  * Hide composition list on audio mix. [Commit.][73]
  * Fix crash dragging multiple audio streams clip in timeline with locked tracks. [Commit.][74] Fixes bug [#439849][75]
  * Expose mix duration and align buttons. [Commit.][76]
  * Fix crash on multiple items deletion. [Commit.][77]
  * Timeremap: expose frame blending param. [Commit.][78]
  * Time remap: fix keyframe timecode editing, add pitch correction. [Commit.][79]
  * Upgrade document version and fix wipe params for MLT 7 when opening older project file. [Commit.][80]
  * Fix crash on remap clip selection, add button to delete remap effect. [Commit.][81]
  * Timeremap: make it work with clip not starting at 0 and adjust view on clip resize. [Commit.][82]
  * Fix moving clip group before another clip broken. [Commit.][83]
  * Time remap: fix monitor focus and seeking issues. [Commit.][84]
  * Remap: fix editing “speed before” broke, correctly clear remap widget on clip deletion. [Commit.][85]
  * Get rid of translucent timeline cursor on high zoom levels, minor optimizations. [Commit.][86]
  * Check for duplicates on add clip. [Commit.][87]
  * MLT7 dropped luma files, they are now generated on the fly, so adjust to this new behavior (still have to find a way to generate thumbnails). [Commit.][88]
  * Fix crash dropping clip in timeline. [Commit.][89]
  * Time remap: allow making clip longer, various fixes. [Commit.][90]
  * Fix remap zoombar. [Commit.][91]
  * More fixes for time remap ui. [Commit.][92]
  * Cleaner remap ui. [Commit.][93]
  * Some progress in remap keyframe view. [Commit.][94]
  * Some progress in remap keyframe widget (WIP). [Commit.][95]
  * Add action to focus active monitor timecode (default shortcut is ‘=&#8217;). [Commit.][96]
  * Fix slide composition with MLT7 (geometry deprecated). [Commit.][97]
  * Adapt wipe to MLT 7 changes, might require code to make previous projects compatible. [Commit.][98]
  * Add 21.04.3 release notes to appdata. [Commit.][99]
  * Time remap: seek clip and project monitor for better adjustment, mark remaped timeline clips with a red square. [Commit.][100]
  * Fix typo. [Commit.][101]
  * First version of timeline clip time remapping. To test, select time remap from timeline context menu and “time remapping” widget to manage keyframes. [Commit.][102]
  * Dynamically update max number of concurrent proxy/transcode jobs, don’t show error message on job abort. [Commit.][103] Fixes bug [#432365][104]
  * Fix freeze on loading clip with unknown duration. [Commit.][105]
  * Correctly display info on error with jobs, fix stablize add clip checkbox not working. [Commit.][106] Fixes bug [#436805][107]
  * Fix various cut task issues. [Commit.][108]
  * Fix titler background not correctly updated in some cases. [Commit.][109]
  * Fix tests. [Commit.][110]
  * Update aom. [Commit.][111]
  * Reimplement delete unused clips (from hard disk). [Commit.][112]
  * Enable “save temporary files next to project file” for new projects too. [Commit.][113]
  * Fix crash closing project with a mix on a clip with keyframable effect. [Commit.][114]
  * Add option to save temporary data in same folder as the project file. [Commit.][115]
  * Fix speech to text after recent VOSK api change. [Commit.][116]
  * Keyframes widget: add option to copy values at cursor pos to clipboard. [Commit.][117] Fixes bug [#439284][118]
  * Import keyframes dialog: simplify UI on import of a single keyframe. [Commit.][119] See bug [#439284][118]
  * Import keyframes dialog: add two new modes offset and inverted position. [Commit.][120]
  * Import keyframes dialog: make position alignment more customizable. [Commit.][121] Fixes bug [#426316][122]
  * Improve default options of the keyframe import dialog. [Commit.][123]
  * Update build instructions for mlt with opencv. [Commit.][124]
  * Update mlt build instructions to cmake. [Commit.][125]
  * Start up check: mlt’s xml module is required. [Commit.][126]
  * Add missing space beetween type and var name. [Commit.][127]
  * Scene split job: Bring back to UI entry after mlt7 switch, cleanup files. [Commit.][128]
  * Add aac mimetype. [Commit.][129]
  * Switch to mlt7. [Commit.][130]
  * First draft saving timeremap to playlist. [Commit.][131]
  * Time remap: fix crash on go to previous keyframe, make use of undo/redo for keyframe changes. [Commit.][132]
  * Minor remap fixes. [Commit.][133]
  * First proof of concept ui for time remap. [Commit.][134]
  * Fix various typos. [Commit.][135]
  * Add webp mime type to clip creation dialog. [Commit.][136]
  * Fix install. [Commit.][137]
  * Initial implementation of a time remap widget (not working yet). [Commit.][138]
  * Fix startup crash on Wayland, thanks to <login.kde@unrelenting.technology>. [Commit.][139] Fixes bug [#431505][140]
  * Add release description for 21.04.2 to appdata. [Commit.][141]
  * Fix typo. [Commit.][142]
  * Cleanup font setting for qml timeline. [Commit.][143]
  * Appimage: add tap-plugins, fix ladspa. [Commit.][144]
  * Resolved Bug 436895 &#8211; “Rotoscoping not working right”. [Commit.][145]
  * Fix qml anchoring warning in mix. [Commit.][146]
  * Mix: Fix first clip out not correctly reset on second clip deletion. [Commit.][147]
  * Fix right clip of a mix cannot be moved backwards. [Commit.][148]
  * Fix crash on exit when a mix is selected. [Commit.][149]
  * Make previous/next/add keyframe shortcuts work on compositions and mixes. [Commit.][150]
  * Proper implementation of go to next/previous and add keyframe shortcuts. [Commit.][151]
  * Allow assigning shortcuts to go to next/previous keyframes and add/remove keyframes in effect stack. [Commit.][152]
  * Fix color clip thumbnail incorrect after changing a clip’s color. [Commit.][153]
  * Correctly update missing fonts to avoid repetitive warning on project opening. [Commit.][154]
  * Show speed in status bar on resize. [Commit.][155]
  * CLeanup and fix some composition resize & move issues. [Commit.][156]
  * Hopefully proper patch to solve “white” rendering issues. [Commit.][157]
  * Fix license abbreviation (patch by JungHee Lee). [Commit.][158]
  * Update appimage ogg. [Commit.][159]
  * Don’t reset locale for external call of melt renderer (fixes white image rendering). [Commit.][160]
  * Fix various spacer issues. [Commit.][161]
  * Update src/kdenlivesettings.kcfg. [Commit.][162]
  * Update src/ui/configsdl_ui.ui. [Commit.][163]
  * Update src/monitor/glwidget.cpp. [Commit.][164]
  * Improve proxy testing widget. [Commit.][165]
  * Add widget to compare proxy profiles (speed and file size). [Commit.][166]
  * Audio cut task: detect incompatible encoders to prevent losing audio or video on stream copy attempt. [Commit.][167]
  * No need to load bin thumbnails twice. [Commit.][168]
  * Add 21.04.1 release information to the appdata. [Commit.][169]
  * Fix clip thumbs disappearing on timeline resize. [Commit.][170]
  * Fix audio thumbs blurry on track height change. [Commit.][171]
  * Fix timeline thumbnails not saved with project. [Commit.][172]
  * Scene split: allow selecting markers category and create subclips. [Commit.][173]
  * Some progress on ui scene cut task. [Commit.][174]
  * Fix crash on add clip introduced in last commit. [Commit.][175]
  * Restore audio normalise feature in track thumbnail. [Commit.][176]
  * First version (not all features reimplemented) of the scene split task using ffmpeg’s filter. [Commit.][177]
  * Various fixes for spacer moving guides. [Commit.][178]
  * Restore profile check feature on add clip. [Commit.][179]
  * Show markers thumbnails on hover in clip monitor. [Commit.][180]
  * Fix project clips disappearing on proxy creation. [Commit.][181]
  * Spacer tool should not allow moving items before another clip. [Commit.][182]
  * Add option to save as existing title as new item from withing titler. [Commit.][183]
  * Add action to raise Dock Widgets (e.g. with a shortcut). [Commit.][184]
  * Add short instruction for how to build flatpak version. [Commit.][185]
  * Fix thumbnails for audio clips in bin. [Commit.][186]
  * Fix crash on closing project with keyframable track effect. [Commit.][187]
  * Fix clip speed job creating 1 frame clip. [Commit.][188]
  * Remove DVD Wizard. [Commit.][189] Fixes bug [#403128][190]. Fixes bug [#403405][191]. Fixes bug [#413567][192]. Fixes bug [#406042][193]. Fixes bug [#407242][194]. Fixes bug [#420319][195]. Fixes bug [#390431][196]. Fixes bug [#377829][197]. Fixes bug [#420428][198]
  * Delete remaining jobmanager stuff. [Commit.][199]
  * Port remaining cache and cut jobs to tasks, start removal of jobmanager. [Commit.][200]
  * Fix subclip thumbs, port stabilize and speed jobs to new task framework. [Commit.][201]
  * Further fixes for guide moving on spacer operations. [Commit.][202]
  * Fix guides are sometimes not moved even if they are unlocked. [Commit.][203]
  * Flatpak: fix ladspa envvar. [Commit.][204]
  * Appimage: Add FCITX support. [Commit.][205]
  * Appimage: update ext_boost url. [Commit.][206]
  * Fix audio thumbs for multistream clips. [Commit.][207]
  * Make number of concurrent proxy/transcode jobs configurable. [Commit.][208]
  * Fixes and improvements for clips load / audio thumb jobs. [Commit.][209]
  * Titler: increase max font size from 1000 to 10000. [Commit.][210]
  * Add option to lock/unlock guides. Unlocked guides will move along when using spacer tool or adding/removing space. [Commit.][211]
  * Fix tests. [Commit.][212]
  * Fix proxied clip cannot be dragged from monitor to timeline. [Commit.][213]
  * Fix incorrect speed cycling with j/l keys. [Commit.][214]
  * Ensure render widget is displayed again after being minimized. [Commit.][215]
  * Fix playback speed not reset on pause. [Commit.][216]
  * Fix spacer tool not workin on single clips (without groups). [Commit.][217]
  * Convert transcoding to new taskmanager. [Commit.][218]
  * Fix forgotten mutex unlock. [Commit.][219]
  * More progress on jobmanager rewrite, port proxy task. [Commit.][220]
  * Start proper replacement for JobManager. [Commit.][221]
  * Start deprecating loadjob in favor of QRunnable cliploadtask. [Commit.][222]
  * [flatpak] Add mfx-dispatch for improved hwaccel support. [Commit.][223]
  * Mlt7: Remove autotrack\_rectangle (motion\_est) further cleanup. [Commit.][224]
  * Mlt7: Remove autotrack\_rectangle (motion\_est). [Commit.][225]
  * Initial commit to transition from jobmanager’s QtConcurrent model to more flexible and simpler QRunnable. [Commit.][226]
  * Add patch for KF solid 5.81. [Commit.][227]
  * Appimage: update kf5 to 5.81. [Commit.][228]
  * Nightly flatpak: fix libdvdread url. [Commit.][229]
  * Appimage: mlt7 fix find melt. [Commit.][230]
  * Appimage: switch to mlt7. [Commit.][231]
  * Nightly flapak: fix typo. [Commit.][232]
  * Remove unnecessary exiv2 patch. [Commit.][233]
  * Mlt7: fix automated mlt detection. [Commit.][234]
  * Ensure grouped clip effect stack is displayed when selecting through keyboard. [Commit.][235]
  * When dropping an effect on an AV group, apply effect on correct part even if audio effect is dropped on video. [Commit.][236]
  * Update opencv. [Commit.][237]
  * More flatpak improvements. [Commit.][238]
  * Fix MLT7 % rect parsing. [Commit.][239]
  * Compile and run on MLT-7 (some TODO’s left). [Commit.][240]
  * Remove unnecessary version checks for mlt 6 versions (new min is ver 7). [Commit.][241]
  * Mlt7: update mlt min version. [Commit.][242]
  * Mlt7: replace deprecated mlt\_sample\_calculator(). [Commit.][243]
  * Mlt7: follow renaming. [Commit.][244]
  * Appimage: freeze bigsh0t to fix build error. [Commit.][245]
  * Another round of Appimage cleanup. [Commit.][246]
  * Remove unused AppImage packaging. [Commit.][247]
  * Nightly flatpak: fix srt and vosk build. [Commit.][248]
  * Nightly flatpak: add bigsh0t. [Commit.][249]
  * Update build.md &#8211; mixing in info from the wiki. [Commit.][250]
  * Nightly flatpak: add srt and vosk. [Commit.][251]
  * Nightly flatpak: update OpenTimleineIO to version 13.0.0. [Commit.][252]
  * Flatpak nightly: switch mlt to cmake, use v6 branch. [Commit.][253]
  * Remember last used parameter for edit friendly transcoding. [Commit.][254]
  * Implement check and transcoding for non seekable files. [Commit.][255] Fixes bug [#371062][256]
  * Freeze MLT version for packaging. [Commit.][257]
  * Add 21.04 splash-screen. [Commit.][258]
  * More Doxygen fixes. [Commit.][259]
  * Flatpak updates (nightly). [Commit.][260]
  * Try to fix value change on hover issue (maybe Qt regression). [Commit.][261] See bug [#435531][262]
  * NVENC H264 and H265 parameters updated for ffmpeg to understand properly. [Commit.][263]
  * Update build.md: Add instruction to build *.qch file. [Commit.][264]
  * Add option to build & install QCH file. [Commit.][265]
  * Refactor cache manager. [Commit.][266]
  * Add option to copy debug information to clipboard. [Commit.][267]
  * Improve key binding info for transform effects. [Commit.][268]
  * Correct several Doxygen comments. [Commit.][269]
  * Fix unconfigured consumer causing various crashes. [Commit.][270] See bug [#409667][271]
  * Add option to go to start if playback started on timeline end. [Commit.][272] Fixes bug [#353051][273]
  * Fix setting frame background color. [Commit.][274]
  * Load RCC icons on MacOS too. [Commit.][275]
  * Allow starting without DBus. [Commit.][276]
  * Fix project duration label displaying one frame too much. [Commit.][277] Fixes bug [#425639][278]
  * Fix keyframes with master effects having a zone. [Commit.][279]
  * Add qml deprecation info. [Commit.][280]
  * Fix startup crash on empty config file. [Commit.][281]
  * Add pulse capture profile (needed for flatpak). [Commit.][282]
  * Appimage: openssl for ffmpeg (#918), fix rubberband build. [Commit.][283]
  * Cleanup for shortcut list. [Commit.][284]
  * Monitor: add possiblity to use shortcut for show/hide edit mode. [Commit.][285] Fixes bug [#434405][286]
  * Some additional doxygen fixes. [Commit.][287]
  * Fix tests after last commit. [Commit.][288]
  * Workaround app translation mess, small fixes for locale matching. [Commit.][289] See bug [#434179][290]
  * Restore softness param in composite transition. [Commit.][291]
  * Zoom bar: always show handles. [Commit.][292]
  * Some more doxygen fixes. [Commit.][293]
  * Timeline zoombar, related to #651 !184. [Commit.][294]
  * Do not show timecode in shortcut list. [Commit.][295] Fixes bug [#433679][296]
  * Validate timecode in settings. [Commit.][297] Fixes bug [#432580][298]
  * Titler: use TimecodeDisplay for duration input. [Commit.][299]
  * Cleanup and improvements for titlewidget code. [Commit.][300]
  * Titler: Add ellipse item. [Commit.][301]

 [2]: http://commits.kde.org/kdenlive/d8e2d36c415ce91e644da0c63da5f86f4616c1ec
 [3]: http://commits.kde.org/kdenlive/a6e369db10c50370e1a521655b6c4f7c4cf90ce0
 [4]: http://commits.kde.org/kdenlive/293e9d4f1e6ee997caae0c9c5fa362f24b8170a4
 [5]: http://commits.kde.org/kdenlive/9b3792882f00d691ff422389c9f6c80e3b0cd45a
 [6]: http://commits.kde.org/kdenlive/9757abf20cf8c5a4823a9526be6fb5cd7794f2e5
 [7]: http://commits.kde.org/kdenlive/29df1929ff4a940b2cf74a2c5b47ec88586b1ef3
 [8]: http://commits.kde.org/kdenlive/ad7cbbb994df4669ce464803eb5931c38ab2a940
 [9]: https://bugs.kde.org/408434
 [10]: http://commits.kde.org/kdenlive/a747bf597e6e961d442075a454e7b9d31adc9532
 [11]: http://commits.kde.org/kdenlive/f6774b8ec6ef09ada50767bc705819847ae241b4
 [12]: http://commits.kde.org/kdenlive/aaf16ed138d66ee660fcefdd19053d7cbe17b9fc
 [13]: https://bugs.kde.org/433579
 [14]: http://commits.kde.org/kdenlive/69173eb533c9948e9cb3ca959a19f66d16aca7e0
 [15]: https://bugs.kde.org/439837
 [16]: http://commits.kde.org/kdenlive/f9e8ead7af4778fef3599778ab5b2c0b61d431e4
 [17]: http://commits.kde.org/kdenlive/23bcdd33cf0e14c30e86b026b788fcbeb04deb58
 [18]: http://commits.kde.org/kdenlive/7ea475b850ceb9fe38bf1aa0498cd82d04cff976
 [19]: http://commits.kde.org/kdenlive/498173e4032f8b238efd0a47c217cad8010f913a
 [20]: http://commits.kde.org/kdenlive/dc67a66b63f6a49dbcfaed0dcd16bf2e81080ddb
 [21]: http://commits.kde.org/kdenlive/fcf8ee8b2a0c047ccb08b949cdc5ad981207b1c0
 [22]: http://commits.kde.org/kdenlive/f1ebdba6a3b84b20ba8d2545c5148140c17feb74
 [23]: https://bugs.kde.org/440019
 [24]: http://commits.kde.org/kdenlive/e84437b039e474db637d6b9430a791bb2d5782c6
 [25]: https://bugs.kde.org/439355
 [26]: http://commits.kde.org/kdenlive/629012014067becc6e994a5a000ce6210db82c07
 [27]: http://commits.kde.org/kdenlive/73c88028b09f053c4815c1db8ffd762c28d7e731
 [28]: http://commits.kde.org/kdenlive/98bc1309b03678dee39c91c9a5860e1d7a5cb7de
 [29]: http://commits.kde.org/kdenlive/2a0c6c038482d2793001b73e00d35cf5f3a4dcab
 [30]: http://commits.kde.org/kdenlive/3683fb094211526dee0ef5b2c560c89bfc2e9fd7
 [31]: http://commits.kde.org/kdenlive/f84710537fd7196a506819ac80e2485ec63e5388
 [32]: http://commits.kde.org/kdenlive/92d6af694a32cbdd6f06d9fede18a7de3eac4a91
 [33]: http://commits.kde.org/kdenlive/97f71883033e64c7d5c832d0c1664075d295879e
 [34]: http://commits.kde.org/kdenlive/cdf7db3506f7b93ffe327c4ca56feb5a03a0c9b6
 [35]: http://commits.kde.org/kdenlive/dd3ca408963995279b4f8312f8a952a5337d0343
 [36]: http://commits.kde.org/kdenlive/69434fb89fb3efa151d0e28fb4f00726ddeed4ea
 [37]: http://commits.kde.org/kdenlive/9b828c4b57aa0154344fa410da8416ac19590448
 [38]: http://commits.kde.org/kdenlive/c055ffa769c217a6e87a90f3756a4a5258d5c4f1
 [39]: http://commits.kde.org/kdenlive/71ba58f48e1fe785d4f1bed858d55f78ce46ca5f
 [40]: http://commits.kde.org/kdenlive/39494a12556b2f11e3339ab6ed20b5fba48c1f74
 [41]: http://commits.kde.org/kdenlive/b3a4ae10a9f5bf7c1bfa32566a85ea1e0ecd8223
 [42]: http://commits.kde.org/kdenlive/e0765a832f2f64d0d78ece89801e47e011fd84cc
 [43]: http://commits.kde.org/kdenlive/470b7f345f9907867e145a46de95726c68d405b7
 [44]: http://commits.kde.org/kdenlive/a50a24f1dd224b16f3794510b1977d1d2cf85f9c
 [45]: http://commits.kde.org/kdenlive/b4c0505cdfe934cf9fb61069f49fc8db498b77fd
 [46]: http://commits.kde.org/kdenlive/66b95243bfb9866c20804b654dfd5e5998a609b8
 [47]: http://commits.kde.org/kdenlive/636cd23f544b14d0f2ee61320e0ea8a35f3d592b
 [48]: http://commits.kde.org/kdenlive/70b4260e497ff5c97cc45c37da8c1b77cf390442
 [49]: http://commits.kde.org/kdenlive/f7f3fc215e38f02bf533cf0e6326954d3d2c98d8
 [50]: http://commits.kde.org/kdenlive/058b8e882e958835bd0690b09321c11d08eb2d3a
 [51]: http://commits.kde.org/kdenlive/fbbac11593897dfeda8455ae12ce6c445675ea39
 [52]: http://commits.kde.org/kdenlive/f6048bd147b6fc86ab2a0eb7429ae6c2d1bad398
 [53]: http://commits.kde.org/kdenlive/1bb25c612c90aa0022472986260201a7c4d24246
 [54]: http://commits.kde.org/kdenlive/affc281fbe26ccec6debe8b070a00670f341e9a6
 [55]: http://commits.kde.org/kdenlive/511997cbfee41e475eab31b7fd61edcd2354bd36
 [56]: http://commits.kde.org/kdenlive/dfc4097e4eb163910dca026c3695d423bee88e8c
 [57]: http://commits.kde.org/kdenlive/84a008368b68a31169dda9043778e703fb6dc17d
 [58]: http://commits.kde.org/kdenlive/f65254d7f8e8c766bf5c18fb3f1bc411937a28da
 [59]: http://commits.kde.org/kdenlive/17b0bc047ff81522906a33a28df6652c0138ec2a
 [60]: http://commits.kde.org/kdenlive/14c98d92ba0c8f639fd3249f73f0dfe54a139155
 [61]: http://commits.kde.org/kdenlive/b2e2abb2fb0f4cbc8df90718984218a408c42a9d
 [62]: http://commits.kde.org/kdenlive/c435bd427c4fd0d5cf4f959939fcc2291f7cc6b5
 [63]: http://commits.kde.org/kdenlive/eb3dec0346995933d547129628ef94bddd797f51
 [64]: http://commits.kde.org/kdenlive/9c198b201f89f5ee6fdc87aad6f5dc68473ea154
 [65]: http://commits.kde.org/kdenlive/465e6dbb790baef47ecc5e8061546737422096fc
 [66]: https://bugs.kde.org/428755
 [67]: http://commits.kde.org/kdenlive/dd4d8d37bb308bcead917eb43deed4d705c1740a
 [68]: http://commits.kde.org/kdenlive/2b4fcacce33e69f1aa85c9b18d4beb5b8bbdd141
 [69]: http://commits.kde.org/kdenlive/af4a81f6f9e0df3fdc7641a28609435ca00a0065
 [70]: http://commits.kde.org/kdenlive/ec313d5dc4d6b58978aff7065ee97da3c1e6f850
 [71]: http://commits.kde.org/kdenlive/cec9664648f7abb82542dbe19d8d28e484be426d
 [72]: http://commits.kde.org/kdenlive/6f7df8c3753ac855293b8d875a5b6aa06dbe2d7d
 [73]: http://commits.kde.org/kdenlive/5869fd57c55b6e15736878cb26c242e07fd19e0e
 [74]: http://commits.kde.org/kdenlive/6c37857ead305a43f330517983c3c040dd67e696
 [75]: https://bugs.kde.org/439849
 [76]: http://commits.kde.org/kdenlive/d061e92d1e59b8159342a703c557d51c06f84d68
 [77]: http://commits.kde.org/kdenlive/06181f0e831b4816681b7d183c913e57f1f35e5e
 [78]: http://commits.kde.org/kdenlive/8421430b7d4d970bc9ae632d8d6e2c6b2719b5da
 [79]: http://commits.kde.org/kdenlive/202c2c1ccff26c70469c5124bdbf1118e76f0a73
 [80]: http://commits.kde.org/kdenlive/59a699783e2affd4357e82d57fb96dc86f9c2e0e
 [81]: http://commits.kde.org/kdenlive/0c87cda1281221e217b781e5d1e397a6261397da
 [82]: http://commits.kde.org/kdenlive/4df1f4242070dfdc9dd7b5e2af9b7e4225f2790a
 [83]: http://commits.kde.org/kdenlive/8ac4374b1a00b044320a294caeee38df9270b6d2
 [84]: http://commits.kde.org/kdenlive/5993f95ce882282b0789dbcc4c9c8d15d5be8f96
 [85]: http://commits.kde.org/kdenlive/134fdd58a1cae2d22870c8cb6bf89f37a6e8ce4b
 [86]: http://commits.kde.org/kdenlive/8f54fb2a21d27821aa26aa078832e4aa50931766
 [87]: http://commits.kde.org/kdenlive/8ff45d906e9ca9983539a00bbdc393349d700f34
 [88]: http://commits.kde.org/kdenlive/b3a9740055be4d191425665e2d59884e60a9cd92
 [89]: http://commits.kde.org/kdenlive/f6ae3b0d394ec6a88163738fbdb1794fc896bc49
 [90]: http://commits.kde.org/kdenlive/86aa0fd307830f10a695abdc824efcfcea9ef9df
 [91]: http://commits.kde.org/kdenlive/397be32a421edc409b45ffe8ebaf66e81202215e
 [92]: http://commits.kde.org/kdenlive/a4c94e1489c980e84fe201a6fc4ec2db1997c567
 [93]: http://commits.kde.org/kdenlive/25ef39173c867df4be1f4ce5ddc8e6642ee60881
 [94]: http://commits.kde.org/kdenlive/feffd144d4c37a74082fbc866febd221aa4bf56d
 [95]: http://commits.kde.org/kdenlive/84f61a5fb3d508baba7869c84dc7d9e6a8ff5a87
 [96]: http://commits.kde.org/kdenlive/9d414f57526408711841d46c6736385530d7947f
 [97]: http://commits.kde.org/kdenlive/71e96e109ba334da8300b2dee724805cd23837d4
 [98]: http://commits.kde.org/kdenlive/ff1609762a4e8f0a925aabbf3a1eb5370b3789d6
 [99]: http://commits.kde.org/kdenlive/cfc18fb0233a7493281613755e5b646cbe8c25ed
 [100]: http://commits.kde.org/kdenlive/d8c9742a8f643808796d60f60535cdf2e495bf20
 [101]: http://commits.kde.org/kdenlive/94e27faf2d70088214f94c548529e268858ccde8
 [102]: http://commits.kde.org/kdenlive/1350e856d167f871c91ad65216f731a9a887a881
 [103]: http://commits.kde.org/kdenlive/759ca9bbd2d1cad534ed0853703d6d2704830bab
 [104]: https://bugs.kde.org/432365
 [105]: http://commits.kde.org/kdenlive/212f3a58c3494723d025a5d7523c40da8ec1f238
 [106]: http://commits.kde.org/kdenlive/8ef991dd6152df3aadf96b2a49ba36fc51c5d6bc
 [107]: https://bugs.kde.org/436805
 [108]: http://commits.kde.org/kdenlive/08eecf0b8e7e85fc67b38b2ec3ef932dc5e8c4b9
 [109]: http://commits.kde.org/kdenlive/e2920ecbea91137ed6c454001abb383b43991fc5
 [110]: http://commits.kde.org/kdenlive/347bd2b4396388cd04b2af2013e0a8801e49d464
 [111]: http://commits.kde.org/kdenlive/61860694176dcdb30df2df89b1598e955fda8bcc
 [112]: http://commits.kde.org/kdenlive/88ae8e428132c67e9135402403a4fe20c6d348a0
 [113]: http://commits.kde.org/kdenlive/e628768bd46baea5a8211280665538c828bedf79
 [114]: http://commits.kde.org/kdenlive/66976dfd4b2b8740af3fff25fc815709b737aaf5
 [115]: http://commits.kde.org/kdenlive/8ca31313d8c23bd67df8c8381f35e5d21447fad5
 [116]: http://commits.kde.org/kdenlive/383732d3cfd18b1a9cc028e5792b1b0e9456ce47
 [117]: http://commits.kde.org/kdenlive/28dcebc4726e8af33b1b30c01e2623bea79c50a1
 [118]: https://bugs.kde.org/439284
 [119]: http://commits.kde.org/kdenlive/001b843cfb97898a003f5c5687712ee543a48453
 [120]: http://commits.kde.org/kdenlive/107ff77339a683fcd3df4acb186df02c0a7a12b7
 [121]: http://commits.kde.org/kdenlive/e2ada40b228470fd74f6d17a0e3728d1b8071b06
 [122]: https://bugs.kde.org/426316
 [123]: http://commits.kde.org/kdenlive/b5f171bd1e0ce20bcf311022ca09614fa2f07ec2
 [124]: http://commits.kde.org/kdenlive/7320c6127d9d61a75b60f092e15b8875cf7436e8
 [125]: http://commits.kde.org/kdenlive/f78dee9fb0d0200c6834ee9ebcfdfdf5d0e8f498
 [126]: http://commits.kde.org/kdenlive/397da608d0079ae291a0e652c961ec77a9b0c71a
 [127]: http://commits.kde.org/kdenlive/78c74476d569629cfec159868c557c341b568c45
 [128]: http://commits.kde.org/kdenlive/5cf61ff0801451a9572d50ff92d39cb286f8d2fc
 [129]: http://commits.kde.org/kdenlive/8ecff03b2d59f243502d6caefefbd3e1938460da
 [130]: http://commits.kde.org/kdenlive/634227b5418db3917f14f7ccc78bf5f4c4c5c2a0
 [131]: http://commits.kde.org/kdenlive/71c58a76aeb981fcd162ed8ced597a2934658ac1
 [132]: http://commits.kde.org/kdenlive/f6ae106325a7bbe7467dd369141e9c8f0d429dd1
 [133]: http://commits.kde.org/kdenlive/ad339ef59a28fe374503fdc1cac2f6a14193c65f
 [134]: http://commits.kde.org/kdenlive/c96f9f618f5a7467f2746b76b06b312c05c88ef6
 [135]: http://commits.kde.org/kdenlive/ea4527ae1bc9d58beebecbdf05ef2d384bf85aeb
 [136]: http://commits.kde.org/kdenlive/bff0482832f28a138f202fe46429ca79900aaf53
 [137]: http://commits.kde.org/kdenlive/9a65c2e857cfc097549ffbf86646071d3c49f18f
 [138]: http://commits.kde.org/kdenlive/db8e753c245d3f543088c569fefb42d669ff4213
 [139]: http://commits.kde.org/kdenlive/185b30362f1f2a3cf340e6fbcd92cd41157e2c9d
 [140]: https://bugs.kde.org/431505
 [141]: http://commits.kde.org/kdenlive/ee7a141b16502aa31c2b742880723c1a4fe89a16
 [142]: http://commits.kde.org/kdenlive/caf3c428812d53ff29365b275583def2aa7b0c67
 [143]: http://commits.kde.org/kdenlive/8fb6a122706d1aa723db8b7b69f12f437981181b
 [144]: http://commits.kde.org/kdenlive/46e979ea3e372bd1ac16475bf200c197bd6c6949
 [145]: http://commits.kde.org/kdenlive/0a80687f392d30a7dd5bb15cfad63fad5a7f0c12
 [146]: http://commits.kde.org/kdenlive/6bf17261db3ce5b28665337af5f421f0602bb627
 [147]: http://commits.kde.org/kdenlive/a8599d6e5c3045ab622a5efd35e28641efd9f9fc
 [148]: http://commits.kde.org/kdenlive/f305f37d1110a7711d4009732cccd3a04aa7a4eb
 [149]: http://commits.kde.org/kdenlive/64722e24b07d33ae6efc39e3e97122c637a60502
 [150]: http://commits.kde.org/kdenlive/91601f5acc3b11ea38b5e75ed221afaf05364938
 [151]: http://commits.kde.org/kdenlive/f8351c3d6328b7860cc45e37954e1a5b9d04feea
 [152]: http://commits.kde.org/kdenlive/b4768f86ac90fda7d81d97d6458b500042c5b83d
 [153]: http://commits.kde.org/kdenlive/433a54694239f297de4dff293218f34c3cf5b7d3
 [154]: http://commits.kde.org/kdenlive/9505699391da4242e22f1f2aa5458599bea07916
 [155]: http://commits.kde.org/kdenlive/8d31018beda410b8bba06e9a513064ac65ff3c15
 [156]: http://commits.kde.org/kdenlive/f64067ab4c3ed875bb4bb8001311a747b761a843
 [157]: http://commits.kde.org/kdenlive/dafda4969d28012a75f3c880260fc8067d2df60d
 [158]: http://commits.kde.org/kdenlive/82643ae40ff49c30ac8155249d197e489e7acd98
 [159]: http://commits.kde.org/kdenlive/e5776050820350013ee3a3d1c8352f89c6866adc
 [160]: http://commits.kde.org/kdenlive/fa04b7f904ccc183b2d4528f13e83a963e5d143f
 [161]: http://commits.kde.org/kdenlive/9db68961d0a61d34db6d5c06e6a05de4804a01f5
 [162]: http://commits.kde.org/kdenlive/eef41b4d82ee9a2a19afe9ae879964ba82c7cce9
 [163]: http://commits.kde.org/kdenlive/ae6bf98c9e9de71679db584d681693e538624f77
 [164]: http://commits.kde.org/kdenlive/3924567e4bc463f444753ffd99d5b98157d13acb
 [165]: http://commits.kde.org/kdenlive/2ba788254c31356b3cba29fe893d89f21bdaf172
 [166]: http://commits.kde.org/kdenlive/515cfa5ff0db5e7a7749ff80f203917bf6c7d813
 [167]: http://commits.kde.org/kdenlive/bfdcee543f2a09028b78b745e4c092eb4e47c47c
 [168]: http://commits.kde.org/kdenlive/9b89f4ee37632cbecf12c75224eaade3b644418d
 [169]: http://commits.kde.org/kdenlive/68df7dd948e33e29395f3e70a36266c1c82f2282
 [170]: http://commits.kde.org/kdenlive/8b41ca258ebedd0706021c199ab242f9377410a4
 [171]: http://commits.kde.org/kdenlive/ad23bd66e1bc9135cfb83376bdda09d37195f4cb
 [172]: http://commits.kde.org/kdenlive/ab106fda0f905e7b15416d3ca1c0407e769c4f4d
 [173]: http://commits.kde.org/kdenlive/75feb1d7bbf66978f26ed988272bd13bcafd843f
 [174]: http://commits.kde.org/kdenlive/383b454c1e4a1a71451b789e061039d7e92d659e
 [175]: http://commits.kde.org/kdenlive/427c713227baf057c55de2d21aa94496ca343b64
 [176]: http://commits.kde.org/kdenlive/f1a9ebdf427eb11d7c52e89d25e18066a2b61954
 [177]: http://commits.kde.org/kdenlive/ad13260179cb9f1b9d6d659eeebca8d419a1a427
 [178]: http://commits.kde.org/kdenlive/702a58cc9d698a1672dd74f87eda56d38af7e3f0
 [179]: http://commits.kde.org/kdenlive/e48041f48d4393d92a57f28cacca3b382781d769
 [180]: http://commits.kde.org/kdenlive/21ae98d138cc9ff51b646e1e6de58421091d6921
 [181]: http://commits.kde.org/kdenlive/891149f2e3d86fcdad6ebf68d75e16f1c44cffa4
 [182]: http://commits.kde.org/kdenlive/156ddfff413f6cb830c3c22f91ddbe85a6807539
 [183]: http://commits.kde.org/kdenlive/340c556d97f95e1ca74e15d00184693d04192e2f
 [184]: http://commits.kde.org/kdenlive/0a803427e3b7f9a2cca41579fdb0988932dddfcf
 [185]: http://commits.kde.org/kdenlive/460b92e7afbde0ddbd5b5fbaf4eba0ec98792846
 [186]: http://commits.kde.org/kdenlive/2c87162c1616beb4fd91fd7b83b8bc4095beaabd
 [187]: http://commits.kde.org/kdenlive/fd2371611ae8a2f4a3cb8613250ec829d3bc4384
 [188]: http://commits.kde.org/kdenlive/642333e2d265f9c1c47e9f9e9af6bdf3d6214cc4
 [189]: http://commits.kde.org/kdenlive/5aaa7466ea0f88642ac81bd31bd1223df17956c1
 [190]: https://bugs.kde.org/403128
 [191]: https://bugs.kde.org/403405
 [192]: https://bugs.kde.org/413567
 [193]: https://bugs.kde.org/406042
 [194]: https://bugs.kde.org/407242
 [195]: https://bugs.kde.org/420319
 [196]: https://bugs.kde.org/390431
 [197]: https://bugs.kde.org/377829
 [198]: https://bugs.kde.org/420428
 [199]: http://commits.kde.org/kdenlive/8ddfc9ef756031a4c4a35a752c474ae613a5ae08
 [200]: http://commits.kde.org/kdenlive/83003d029042d395e54264c80c97de539bbee3b4
 [201]: http://commits.kde.org/kdenlive/8db7e33ce11cb523ea5b862e12b155a2c6215ea0
 [202]: http://commits.kde.org/kdenlive/4f522688614439e080abb6554a1ed6ccce6330ef
 [203]: http://commits.kde.org/kdenlive/1bb40db393822e0d780d061a1640ff67c47bebc1
 [204]: http://commits.kde.org/kdenlive/4993dc1f9bd425a3f322c114f9325a02ba957345
 [205]: http://commits.kde.org/kdenlive/fcd4e61fe5b527eed2fb32a27e5c4b9a0b796422
 [206]: http://commits.kde.org/kdenlive/02aad603d2cd8d86ea99b6093c55efb74f6d0a28
 [207]: http://commits.kde.org/kdenlive/566d96bf287bd5254b36e1dba1d4b42de62aaae1
 [208]: http://commits.kde.org/kdenlive/d15951b18e330ae4fa6e9d7cc1a9b3a823c2c8d5
 [209]: http://commits.kde.org/kdenlive/daa2c64cb6c40f2aee1fd5d01442137c6f5c9a74
 [210]: http://commits.kde.org/kdenlive/7854f777bbb000cdaa4c14e08223796a414bde92
 [211]: http://commits.kde.org/kdenlive/9a08ea41e94c5e883f3bcc46b105cfe1df9c5212
 [212]: http://commits.kde.org/kdenlive/63bb28399df22748d9001a52e947dbed9bb0eaf7
 [213]: http://commits.kde.org/kdenlive/f5ebdb5406a0f98e6362627647d47ba0f7e91317
 [214]: http://commits.kde.org/kdenlive/a42613bd76f7b56958838aeae569aad72c5fd81c
 [215]: http://commits.kde.org/kdenlive/5174b1e37cc3bebc15f2fcb30365f9d0120783be
 [216]: http://commits.kde.org/kdenlive/3b89b7e494d8c5724473760e8f11a7b66016d33b
 [217]: http://commits.kde.org/kdenlive/bfb79099c7bcfd8d393514f2fef44032121f1d5a
 [218]: http://commits.kde.org/kdenlive/00f0c3a5a9cdece0753a3b4e95960c0612a86adc
 [219]: http://commits.kde.org/kdenlive/15228e34311093d7d9bf69ac659e77e36d852815
 [220]: http://commits.kde.org/kdenlive/a35a76d54e9e6804b0bf3c83aa49269c9be41f87
 [221]: http://commits.kde.org/kdenlive/d7e914ba98d9e98b58a5cdf610f798fb114974d6
 [222]: http://commits.kde.org/kdenlive/3c919550405f35207a91b94fdc8f67bebfbcee62
 [223]: http://commits.kde.org/kdenlive/3ea8bbc6ee01e6ce30627be867d8058d5b60b52b
 [224]: http://commits.kde.org/kdenlive/c9ca75367658259d663d501c67a60232f003b717
 [225]: http://commits.kde.org/kdenlive/5bc595c82d624d568c54cd012419d649f4f9865a
 [226]: http://commits.kde.org/kdenlive/b392b15f4fe76b30c62c90c376f3c53492ba7a05
 [227]: http://commits.kde.org/kdenlive/534cea43ec512c1a6c179393f9cd8e40d9c7c2f3
 [228]: http://commits.kde.org/kdenlive/1fb19b6b8c63463da390fa05f7ce7067f977bdec
 [229]: http://commits.kde.org/kdenlive/89e5fb8c054c51173229fc4eb8d3a0bd182b9185
 [230]: http://commits.kde.org/kdenlive/ba32259d268779f7feb7336b74316babe826cb71
 [231]: http://commits.kde.org/kdenlive/f6753f2698290ba23e24f5cdfb6f71581077c93f
 [232]: http://commits.kde.org/kdenlive/30e8dcd321450639a42d5b6ded94def1c6241633
 [233]: http://commits.kde.org/kdenlive/13467e6c6452566d30a1ed7925fbaeb7fc75da50
 [234]: http://commits.kde.org/kdenlive/e484320e94ae1e44daa745d004bd0183b4fc36df
 [235]: http://commits.kde.org/kdenlive/c9ff53e197646446eaad4be76322ea74a159cfb8
 [236]: http://commits.kde.org/kdenlive/ba07f618b6647cd003e531dfd748c7bf8830dd9d
 [237]: http://commits.kde.org/kdenlive/4f6687e4388deb23e51553fff64ebb10967e622d
 [238]: http://commits.kde.org/kdenlive/e938afc09a703515f51098f65500ace2edbbd5ac
 [239]: http://commits.kde.org/kdenlive/55c39948ae11342da42dd40dae11a34740037717
 [240]: http://commits.kde.org/kdenlive/3dcf866784c7454d5da42b8ae8094cf3aac8d42f
 [241]: http://commits.kde.org/kdenlive/9917df1a55e9f829c36601ff87d1e11ac93ce967
 [242]: http://commits.kde.org/kdenlive/ad41a1c91ee5f0d80313ad686bcdaba2606b1825
 [243]: http://commits.kde.org/kdenlive/4954716bc4d61643bc420e01ac5c63d7533c9849
 [244]: http://commits.kde.org/kdenlive/5bceffdc3b23ad2379cfcda1ab84b2e376ea4a3f
 [245]: http://commits.kde.org/kdenlive/72d3eef939a8347e4eecb8ac2d7764e068ad70b6
 [246]: http://commits.kde.org/kdenlive/5757743884ef4c23e4924bb10856110bd70fcd0b
 [247]: http://commits.kde.org/kdenlive/b90816ff2412597ed40e130953a7a1209dc9bd58
 [248]: http://commits.kde.org/kdenlive/2eb7b5b890aebe971b6f4a8e6af90986f20cb58e
 [249]: http://commits.kde.org/kdenlive/a4c2e7be7383306aa87595cfa241944f3a5012bd
 [250]: http://commits.kde.org/kdenlive/70490907f478619cc06575f6b548074fe681199c
 [251]: http://commits.kde.org/kdenlive/66dc831bbb9965a58352e3ea0028fec7df9389e2
 [252]: http://commits.kde.org/kdenlive/f0d7fb6542c2ccca233904d4128e946b692de249
 [253]: http://commits.kde.org/kdenlive/0e8b490d1de4bd43d1a3f45d65146bcf929304db
 [254]: http://commits.kde.org/kdenlive/8bee1d8691e1cfb046ec55a68f7b86bffe349bff
 [255]: http://commits.kde.org/kdenlive/faf778167d723de182bbbef79450d1f017820dc6
 [256]: https://bugs.kde.org/371062
 [257]: http://commits.kde.org/kdenlive/073e6678a0f5a39b42e279315744e55bab2b17fc
 [258]: http://commits.kde.org/kdenlive/30a98e0334e2bb74c20ae9e8fdd1e453c5894c93
 [259]: http://commits.kde.org/kdenlive/531a886ab46420872fa527a4572c181e092952f2
 [260]: http://commits.kde.org/kdenlive/f8a690864b53781f0de8216579e63226ecd8f15c
 [261]: http://commits.kde.org/kdenlive/f86dbde2fac1b1b0043deb7601ec90763400640c
 [262]: https://bugs.kde.org/435531
 [263]: http://commits.kde.org/kdenlive/e68bbd1dd6bef9a9cae239d61d76f320bc3d0d00
 [264]: http://commits.kde.org/kdenlive/dd46255a2cc12aa20c962818b1aa779eae20520e
 [265]: http://commits.kde.org/kdenlive/4ef3bca26f25e46f4dc666e3b8a8e0a1bf2d539f
 [266]: http://commits.kde.org/kdenlive/ea2fc56da0f2134c60e8fee6b3b1f20fe224aa20
 [267]: http://commits.kde.org/kdenlive/d1e034a8efb06a80b3cf77b9e062772d49a984c9
 [268]: http://commits.kde.org/kdenlive/f13da46a88518b8350719b0361af8a1615da2083
 [269]: http://commits.kde.org/kdenlive/b4d2c85e5bdbebd837305b48e79054d06db6a44d
 [270]: http://commits.kde.org/kdenlive/11e5be5ea2c98fda76780aec9a9f796fe6ef14e2
 [271]: https://bugs.kde.org/409667
 [272]: http://commits.kde.org/kdenlive/e0cd0a021d8c260a7b329acb4ba327f750875a30
 [273]: https://bugs.kde.org/353051
 [274]: http://commits.kde.org/kdenlive/8fc9b014bd1d3dc5da76af307451fe8ae2edf4a1
 [275]: http://commits.kde.org/kdenlive/289700b22f692da14010803d7ade48c5f6f8f25d
 [276]: http://commits.kde.org/kdenlive/f165a6b51d305b002e98c1335ca8f4bd275bdc05
 [277]: http://commits.kde.org/kdenlive/d8fe0cd57b9d2a9fde7affb6de89cdcca15e0ed1
 [278]: https://bugs.kde.org/425639
 [279]: http://commits.kde.org/kdenlive/634ab3be11531c63fff8937014560865d49624bf
 [280]: http://commits.kde.org/kdenlive/fe2fc194b5f789f3bbb3e8e1cb6cee984d89c869
 [281]: http://commits.kde.org/kdenlive/f4033fa209a95bb7a142130048b2b9089b2d9a7d
 [282]: http://commits.kde.org/kdenlive/f185b75b5c6ce94ae2636be4fd4e8564ae2af29e
 [283]: http://commits.kde.org/kdenlive/89429755af9a6d6b3629026d478439cb7b44a603
 [284]: http://commits.kde.org/kdenlive/05517b1cc4b66ab27abb72d127559aac11f01973
 [285]: http://commits.kde.org/kdenlive/717121c4e9ffe82d36c057f35798129f207071d4
 [286]: https://bugs.kde.org/434405
 [287]: http://commits.kde.org/kdenlive/bd94ca90522d67bbb7b5f238ea7d04b38c535226
 [288]: http://commits.kde.org/kdenlive/c8d05f8d4dd12027fddffa39b495c3f1b60693bb
 [289]: http://commits.kde.org/kdenlive/659f2d352965db81a63eac21db14165279d42e9d
 [290]: https://bugs.kde.org/434179
 [291]: http://commits.kde.org/kdenlive/4f2ea208a9d6ec5fbcc5bde39b7a08d112c41629
 [292]: http://commits.kde.org/kdenlive/dea7a8bbf4ff524288a1c32c8139931110d03ed0
 [293]: http://commits.kde.org/kdenlive/e6677862cc99bcc115c3f5999627c2f187fbf20c
 [294]: http://commits.kde.org/kdenlive/32d90385f437f5b4ce272ea4196e56f8ef291a60
 [295]: http://commits.kde.org/kdenlive/22a8b828d481f16e3411929a7f983aa2d32e3b07
 [296]: https://bugs.kde.org/433679
 [297]: http://commits.kde.org/kdenlive/431181a80c236d8a32204e965586b46ae0ed8600
 [298]: https://bugs.kde.org/432580
 [299]: http://commits.kde.org/kdenlive/e9ff0cc6c54e783b0e9232dd7c1b027e769fd473
 [300]: http://commits.kde.org/kdenlive/888177ad2a3013ba59904e7c5d3d92995322fc73
 [301]: http://commits.kde.org/kdenlive/e5a171062adae88a2a7280dbae66c5ccad9acc98
