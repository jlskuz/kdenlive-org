---
title: Kdenlive 21.08 is out
author: Farid Abdelnour
date: 2021-08-16T06:00:15+00:00
aliases:
- /en/2021/08/kdenlive-21-08-is-out/
- /fr/2021/08/kdenlive-21-08/
- /it/2021/08/kdenlive-21-08-disponibile/
- /de/2021/08/kdenlive-21-08-freigegen/
---

Kdenlive 21.08 is out with an upgraded engine, bug fixes and many interface and usability improvements. Although the highlights are mostly under the hood we managed to add some nifty new features as well. This version now requires MLT7, which comes with a new time remapping feature and a more stable and concise code base. (MLT7 sets the foundation for long awaited features like GPU processing, multicore support and improved color management to name a few, all expected in [MLT8][1].)

**IMPORTANT**
The _Automask_ effect has been removed in MLT7 (along with many other legacy and buggy modules) but it will eventually be replaced with more powerful OpenCV tools. The _Region_ module is also removed and it has been replaced with a new _Effect Masking_ feature. Taking the opportunity of these changes we have also cleaned up the code base and removed old and unmaintained tools like the DVD Wizard (yes, some people still used it) and the Preview Compositing mode.

We do not recommend to open old projects with Kdenlive 21.08 due to the aforementioned changes.

## Performance

We continue with every release to improve performance and with this version the Jobs Processing code has been completely refactored fixing an interface lag/freeze when importing hundreds of files and hundreds of gigabytes at once. Also panning through the timeline has a more fluid and smoother experience due to recent changes.

## Time Remapping (Speed Ramps)

The new Time Remap feature allows to keyframe the speed of a clip.

![](timeremap.gif)

## Masking Effects

You may now apply effects to only affect specific regions of a clip by using masks. Do note that this is only the initial implementation so expect an improved workflow in the coming releases. For now the process involves 3 steps:

  1. Add one of the 3 available masks: _Shape alpha (mask)_, _Rotoscoping (mask) or Alpha shapes (mask)._
  2. Add an effect (or effects) to be applied to the masked region.
  3. Add _Mask Apply_ to activate the mask to the effects in step 2.

![](mask.gif)

_You may apply more than one mask per clip by following the same 3 step process._

## Interface and Usability Improvements

### Move Guides with Spacer Tool  
Easily moves Guides along with clips using the Spacer Tool by using the new _Guides Locked_ option.

![](guidemove.gif)

### Raise widgets with shortcuts  
Added ability to assign shortcuts to raise dock widgets.

![](shortcuts.gif)

### Shortcuts for keyframe functions  
Added ability to assign shortcuts to 3 keyframe functions: _Add/Remove Keyframe_, _Go to next keyframe_ and _Go to previous keyframe._

![](shortcut-keyframe.gif)

### Same track transitions improvements
Added additional options to the same track transitions: _Duration_, _Reverse_ and _Alignment_.

![](sametrack-options.gif)

### Command Bar
The command bar allows to easily search for any action in Kdenlive like changing themes, adding effects, opening files and more. It can be accessed with the shortcut: **Ctrl + Alt + i**. _(This feature requires KDE Frameworks lib version 5.83)_ 

![](searchbar.gif) 

### Copy value at cursor position to clipboard
In addition to the _Copy Keyframes to clipboard,_ the new _Copy value at cursor position to clipboard_ option allows to copy only the current value to a single keyframe.

![](copyatcursor.png)

### New mapping modes and options when importing tracked data 
_Inverted Position_ and _Offset Position_ are two new mapping modes for importing data from the motion tracker. _Inverted position_ behaves like the current _Position_ function but inverts the imported x and y values. _Offset Position_ can be used for footage stabilization since it imports the difference between the first keyframe position (reference point) and the current keyframe position. Also new mapping locations were added: _Top left, Center, Bottom right_.

![](mapping-modes.gif)


### Rename guides from monitor
Clicking on a guide in the Project monitors allows to easily rename it.

![](edit-guides.gif)

### Other fixes

  * Titler: When editing a title clip there is a new  _Add as new title_ option.
  * Add option to go to start if playback started on timeline end
  * Fix audio thumbs for multistream clips
  * Show markers thumbnails on hover in clip monitor
  
 [1]: https://www.mltframework.org/changes/todo/
