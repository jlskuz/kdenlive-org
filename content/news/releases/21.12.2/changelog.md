  * Fix sometimes cannot move grouped clip right when only 1 empty frame. [Commit.][1]
  * When saving effect, show it under its name, not id in effect list. [Commit.][2]
  * Fix fade effects not correctly saved or pasted. [Commit.][3]
  * Fix clip monitor allowing seek past clip length with transparency background enabled. [Commit.][4]
  * Fix green tint on first image extract. [Commit.][5]
  * Minor cleanup of add marker ui. [Commit.][6]
  * Ensure thumbnail preview profile is not changed by clip resolution. [Commit.][7]
  * Fix alpha render and add utvideo. [Commit.][8] Fixes bug [#448010][9]. See bug [#436879][10]
  * Fix freeze trying to drag a clip that was just added to Bin. [Commit.][11]
  * Uptade frei0r.scale0tilt.xml with Scale X and Y parameters now animated. [Commit.][12]
  * Fix cherry-pick typo. [Commit.][13]
  * Fix timeline ruler not working after effect drop in some circumstances. [Commit.][14]
  * Fix various bugs in timeremap (keyframes random move, crashes). [Commit.][15]
  * Time Remap: don’t allow keyframe after last frame of source clip. [Commit.][16]
  * Protect timeline preview list with mutex. [Commit.][17]
  * Fix slideshow duration not updated on profile change. [Commit.][18]
  * Fix detection of missing timeline preview chunks on opening. [Commit.][19]
  * Don’t attempt to create audio thumbs if thumbs are disabled. [Commit.][20] Fixes bug [#448304][21]
  * Speedup loading of projects with timeline preview. [Commit.][22]
  * Add some default LUT files. [Commit.][23]
  * [Wizzard] Update link to troubleshooting docs. [Commit.][24]
  * Try to find mediainfo on windows automatically too. [Commit.][25]
  * [Setup Wizard] Show codes if there are only info messages, fix doc link. [Commit.][26]
  * Disable “Change Speed” and “Time Remap” actions if the other one is. [Commit.][27] Fixes bug [#443613][28]
  * Check for mediainfo in setup wizard. [Commit.][29]
  * Fix extract frame on Windows (also used for Titler and scopes). [Commit.][30]
  * Improve monitor zoom. [Commit.][31] Fixes bug [#434404][32]
  * Use a SPDX standard license identifier in Appstream data. [Commit.][33] Fixes bug [#448134][34]
  * Rename forgotten rgb24 and rgb24a after MLT 7 port. [Commit.][35]

 [1]: http://commits.kde.org/kdenlive/f1ca6d017b65f58ec17e12afc97cc27f0e0c4093
 [2]: http://commits.kde.org/kdenlive/ce05e2e448a0e4e3cb4a3a3e4b862df3d5681ac5
 [3]: http://commits.kde.org/kdenlive/0b7ab8dd6a7b3dafa8e635d837ee499d411b7150
 [4]: http://commits.kde.org/kdenlive/2883c62a2e3c98eddebe76ef5811519b0d0418b9
 [5]: http://commits.kde.org/kdenlive/c2558c7afc4ae73c4934e1c08044c1d3622991ce
 [6]: http://commits.kde.org/kdenlive/371e21deecbf1a28e21d00cb0cb3248125fbe3e4
 [7]: http://commits.kde.org/kdenlive/991989ab761023bbab8052deefd84b047f8bd654
 [8]: http://commits.kde.org/kdenlive/136a07aa2563b4e59d881bb74b431d992e947e38
 [9]: https://bugs.kde.org/448010
 [10]: https://bugs.kde.org/436879
 [11]: http://commits.kde.org/kdenlive/44a217e78ebf9ebf61eae8321f32935527a3379e
 [12]: http://commits.kde.org/kdenlive/e9e08f0e0442446cfa39e609066999efbeb4e938
 [13]: http://commits.kde.org/kdenlive/dd3aca47e7f72c1452f55359ba9eeec6280c83e4
 [14]: http://commits.kde.org/kdenlive/17eea317ed3d8fb2ea0bae415246e725797f99df
 [15]: http://commits.kde.org/kdenlive/d8b2a2569dbaf3d212c70c9f5f93bb2b395d869e
 [16]: http://commits.kde.org/kdenlive/c7d53f8a169772fdb191061be3ba9eb46755d728
 [17]: http://commits.kde.org/kdenlive/7937551fb91c9fa7664af5d1e5a7020fa8573a2d
 [18]: http://commits.kde.org/kdenlive/3739fe99f6f52146acddc62329ec1220426c7a79
 [19]: http://commits.kde.org/kdenlive/433572a69c25d640e9a70970ce5465429a7cb3fa
 [20]: http://commits.kde.org/kdenlive/c1e7f6a0917fbdd04cac51c09930c49fd523f913
 [21]: https://bugs.kde.org/448304
 [22]: http://commits.kde.org/kdenlive/d0a5fb9c983b5b77260e24482bef552aeef23c98
 [23]: http://commits.kde.org/kdenlive/e3b712379359923e8efce0bb2156da542231e6da
 [24]: http://commits.kde.org/kdenlive/934bcffcaf0e6b6782b60fa1c7efc82a93b7ff29
 [25]: http://commits.kde.org/kdenlive/77adf710fd5dd15908df724b549b37673eeb9784
 [26]: http://commits.kde.org/kdenlive/419e1b2fb86411efd2f90a8dfad46212f0131e2e
 [27]: http://commits.kde.org/kdenlive/9d3a7c6ecc726eae2242abeb477d3d8c3f908e68
 [28]: https://bugs.kde.org/443613
 [29]: http://commits.kde.org/kdenlive/ade1c515e697ad367e45b6298ee369d5f7087bef
 [30]: http://commits.kde.org/kdenlive/c04ff48b4bc416954d2733a8ed842a39f1feb40e
 [31]: http://commits.kde.org/kdenlive/e6694e6ba457aeecd5657f1ec3627bec7b6f6ac2
 [32]: https://bugs.kde.org/434404
 [33]: http://commits.kde.org/kdenlive/307bedfe593eb344e2db2604208e2c7147263d79
 [34]: https://bugs.kde.org/448134
 [35]: http://commits.kde.org/kdenlive/88b3d381864573a9db256e476157a866e60dfb90
