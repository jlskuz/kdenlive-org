---
author: Farid Abdelnour
date: 2022-02-08T12:43:06+00:00
aliases:
- /en/2022/02/kdenlive-21-12-2-released/
- /fr/2022/02/auto-draft-3/
- /it/2022/02/kdenlive-21-12-2-2/
- /de/2022/02/kdenlive-21-12-2-3/
---
Kdenlive 21.12.2 is out with faster performance when opening projects, added stock LUTs, improved monitor zoom (more zoom steps, higher zoom levels and usage of current monitor center as reference for zooming). This version also fixes time remapping issues and alpha rendering among others.
