  * Fix remove space in all tracks with locked tracks. [Commit.][1]
  * Fix spacer track sometimes not allowing to reduce space. [Commit.][2]
  * Drop MLT’s composite transition for Slide transition. [Commit.][3]
  * Remove broken “duplicate bin clip with timeremap” stuff that caused crash dropping playlists in timeline. [Commit.][4] Fixes bug [#441777][5]
  * Fix color picker in multiscreen config. [Commit.][6]
  * Fix monitor zoom affecting titler background frame. [Commit.][7]
  * Ensure we always use UTF-8 enconding when writing files. [Commit.][8]
  * Startup crash detection: make the check later so that we can also detect movit crash and propose to reset the config file. [Commit.][9]
  * When editing / creating a render profile, focus the edited profile on save. [Commit.][10]
  * Filter tasks: fix encoding issue breaking job (stabilize, motion tracker). [Commit.][11]
  * Improve color accuracy of preview (backported from Shotcut). [Commit.][12]
  * Fix fake rect parameter not updating monitor overlay (alhpashape, corners) when changing value in effect stack. [Commit.][13]
  * Fix adjust to frame size option in transform/position & zoom compositions. [Commit.][14]
  * Fix color picker incorrectly selecting a rect zone after first use. [Commit.][15]
  * Fix compositions hidden when top clip had a same track transition. [Commit.][16]
  * Fix same track transition erratic resize. [Commit.][17]
  * Fix possible crash on incorrect active effect. [Commit.][18]
  * Move avfilter_loudnorm.xml to the correct place (avfilter dir). [Commit.][19]
  * Clear effect xmls by moving frei0r into a seperate folder. [Commit.][20]
  * Multitrack view: Fix scaling in some cases eg. with rotoscoping. [Commit.][21]
  * “Composite” composition: add align parameters to UI. [Commit.][22]
  * Don’t show time remapping dock by default. [Commit.][23]
  * Ensure bin audio thumbnails are loaded on project open. [Commit.][24]
  * Fix title widget background frame not showing up. [Commit.][25]
  * Crop effect: use project resolution by default(solves proxy issue). [Commit.][26] Fixes bug [#408235][27]
  * Fix bug and crash in keyframe apply value to selected keyframes. [Commit.][28]
  * Fix fade to alpha broken with MLT-7. [Commit.][29]
  * Fix “gpstext” effect default value. [Commit.][30]
  * Update mask\_start\_frei0r_select0r.xml. [Commit.][31]
  * Update CMakeLists.txt. [Commit.][32]
  * Uploaded gpstext.xml. [Commit.][33]
  * Update kdenliveeffectscategory.rc. [Commit.][34]
  * Update kdenliveeffectscategory.rc. [Commit.][35]
  * Update mask\_start\_frei0r_select0r.xml. [Commit.][36]
  * Update blacklisted_effects.txt. [Commit.][37]
  * Hide mask_start (unusable as a standalone effect). [Commit.][38]
  * Add a mask_start version of frei0r.select0r for secondar color. [Commit.][39]
  * Fix crash when dropping audio/video only from monitor to bin. [Commit.][40]
  * Fix undo effect change was restoring incorrect parameter. [Commit.][41]
  * Fix scene detection job (should now work on Windows). [Commit.][42]
  * Don’t allow importing a project cache folder (audio/video thumbs, proxy,…). [Commit.][43]
  * Fix render name incorrectly kept in some cases after save as. [Commit.][44]
  * Fix paste position when mouse is over subtitle track. [Commit.][45]
  * Fix crash on pasting grouped subtitle. [Commit.][46] Fixes bug [#439524][47]
  * Fix noise when setting producer (e.g. when opening a project). [Commit.][48] Fixes bug [#433847][49]
  
 [1]: http://commits.kde.org/kdenlive/580efc69daa5938e7f00f1743bf6381f470e30fb
 [2]: http://commits.kde.org/kdenlive/0d7dd3b6e020584d39b13d7beaa0098354350d12
 [3]: http://commits.kde.org/kdenlive/e0edf3e8ff7205b0275373186e47d2400456cb31
 [4]: http://commits.kde.org/kdenlive/82df289b43163486ca0ceaa656726021ef9ed3ac
 [5]: https://bugs.kde.org/441777
 [6]: http://commits.kde.org/kdenlive/552d0a7039c4ce2f86f07270d3b1478d1d0b704b
 [7]: http://commits.kde.org/kdenlive/9288b5575112e0af37cf307f444f098e529c3fe0
 [8]: http://commits.kde.org/kdenlive/7cbb628ec23d7e985befeedf62d03658de4ad82e
 [9]: http://commits.kde.org/kdenlive/f0bba7031de4a058e05c88f7e5a8304bdf14f9a4
 [10]: http://commits.kde.org/kdenlive/ce643e3e282608ddf7e2e4cbd0b0c6aeb777d3df
 [11]: http://commits.kde.org/kdenlive/42fafb49e5b85fe79576b947bee13b787a3478dd
 [12]: http://commits.kde.org/kdenlive/95b648a8c4b01f53992e0d7c51fb99061986530a
 [13]: http://commits.kde.org/kdenlive/0cff60058dfb8ef2b9889cdda1ad04c67624e524
 [14]: http://commits.kde.org/kdenlive/7c483d315c8d9d0723210cc30e812d1f036c7477
 [15]: http://commits.kde.org/kdenlive/d9af07d0fa8d96b43da0972b3451533cb7294d83
 [16]: http://commits.kde.org/kdenlive/28314defa83d22d3807db4b2a7facb3be6e0f0a7
 [17]: http://commits.kde.org/kdenlive/c5f9379b633ec8defd7d94de05978206aa6a9590
 [18]: http://commits.kde.org/kdenlive/85201ee82cc45f8793203c4b7615208572aafd81
 [19]: http://commits.kde.org/kdenlive/008cbd493e32c6b8e920253985f37d1f47d6dda8
 [20]: http://commits.kde.org/kdenlive/fead2a0691d5f0d34791753e42145531ab7cec02
 [21]: http://commits.kde.org/kdenlive/807cf558b8a1af1a9b95357967644f300e911925
 [22]: http://commits.kde.org/kdenlive/70f5858492050a92ec4bec4f0c73f8e2652e1f1c
 [23]: http://commits.kde.org/kdenlive/53c7c85dbd7dd8651e343420d60d050185770ea4
 [24]: http://commits.kde.org/kdenlive/1168b7b0efba21480b3b1cf26fd12a10c67ddd0d
 [25]: http://commits.kde.org/kdenlive/a07509baaa91f6bbdaef631ff6fc4f529a9b5aef
 [26]: http://commits.kde.org/kdenlive/8aff4a1469c50ad1a127a15ead3b74494ae6d9a1
 [27]: https://bugs.kde.org/408235
 [28]: http://commits.kde.org/kdenlive/6a3fac989335b6e41e81ce89a89d9d3d90d3d3a0
 [29]: http://commits.kde.org/kdenlive/fc7bc4ce3713578fc175e64b674339a2bdeb87fd
 [30]: http://commits.kde.org/kdenlive/659a189ae7e42e5af54b53796e804c8e948ece13
 [31]: http://commits.kde.org/kdenlive/dcd8c9e954466e4349f3c6c63d5fac49ada37949
 [32]: http://commits.kde.org/kdenlive/b6faa441cfd61a2bff2341c72e8cb2607fed00f4
 [33]: http://commits.kde.org/kdenlive/d3b2b5c02862a41c7b9bd797b72cfdacd4d924ff
 [34]: http://commits.kde.org/kdenlive/4640293fa6a46263e391928b909e7973842d7fa5
 [35]: http://commits.kde.org/kdenlive/39ea8a6ebe78bb166faaf6249c47a52c8af8d6d0
 [36]: http://commits.kde.org/kdenlive/b49bd8ce8294901ccf713489d14af4fc400979ba
 [37]: http://commits.kde.org/kdenlive/d6e7bb982b8e5e6594aaf8b7066d049c22a2ce94
 [38]: http://commits.kde.org/kdenlive/095d654d32e3acb96ebcc294822f58f094277918
 [39]: http://commits.kde.org/kdenlive/801f91f66d49ee4d8508e31ad79ee6c29a835b7d
 [40]: http://commits.kde.org/kdenlive/19ac05a9d08116d9cc7891947253f10e721f6034
 [41]: http://commits.kde.org/kdenlive/f5b900f723b5b36f01d35b3a051aceac6d1521b4
 [42]: http://commits.kde.org/kdenlive/d88f7f0a5de95ce2ee63e7fad16fafffedad1463
 [43]: http://commits.kde.org/kdenlive/8ff92a646695fa906cc2346ffbb9495b2df11cf3
 [44]: http://commits.kde.org/kdenlive/33c812ace76ab95cb04b263cf330afb5aa0228e5
 [45]: http://commits.kde.org/kdenlive/4c1570cdfd7379c140487cda33fbc0eb6089c6dd
 [46]: http://commits.kde.org/kdenlive/1ba83ecfe724fd4e6fa576d498d40b3c3c94f615
 [47]: https://bugs.kde.org/439524
 [48]: http://commits.kde.org/kdenlive/86f1d76dcfc6e4bf777e9eecf188217ae3502f13
 [49]: https://bugs.kde.org/433847
