---
title: Kdenlive 21.08.2 is out
author: Farid Abdelnour
date: 2021-10-12T12:17:18+00:00
alaises:
- /en/2021/10/kdenlive-21-08-2-is-out/
- /fr/2021/10/kdenlive-21-08-2-2/
- /it/2021/10/kdenlive-21-08-2-pronto/
- /de/2021/10/kdenlive-21-08-2/
---

The second release of the 21.08 series is out with a polishing galore throughout all Kdenlive components. Compositing highlights include added align parameters to the _Composite_ interface, fixing line artifacts affecting the _Slide_ composition, compositions display correctly on clips with same track transitions, _Transform_ and _Composite & Transform_ compositions adjust properly to frame size. _Fade to Alpha_ effect is fixed. The _Color picker_ now works properly when using multiple screens and the color display in the monitors is now accurate. Under the hood improvements include the crash detection and recovery system has been improved, fix _Stabilize_ and _Scene Detection_ jobs, removed noise when opening a project, don't allow importing of project cache folders and always use UTF8 encoding when writing files.
