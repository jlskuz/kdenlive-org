---
title: Kdenlive 21.12 beta out for testing
author: Farid Abdelnour
date: 2021-11-15T14:18:36+00:00
aliases:
- /en/2021/11/kdenlive-21-12-beta-out-for-testing/
---

With version 21.12 just around the corner, we are releasing the first beta version (21.11.80) for testing. Try it out and help find breakages in your daily usage or bugs in these new features:

## Multiple Project Bins

You may now create multiple bins by right clicking on a folder and choosing the _open in new bin_ option.

## Multicam editing

Add your clips in different tracks, but at the same position in the timeline and activate the multicam tool by going to menu Tool -> Multicam tool. You may trim the clips in the desired track while the timeline is playing by pressing their corresponding numbers (for track V1, press key 1; for track V2 press key 2, etc…) or simply select the desired track in the monitor by clicking on it with the mouse.

## Same-track transitions

This release drastically Improved the same-track transition mode. Activate it by selecting a clip in the timeline and pressing the shortcut _U_ or going to the menu _Timeline_ -> _Current clip_ -> _Mix Clips._

## Slip trimming mode

Select a clip in the timeline and go to menu _Tool_ -> _Slip tool._ You may then slip the clip in the timeline to dragging it.

## Ripple trimming mode

Select a clip in the timeline and go to menu _Tool_ -> _Ripple tool._ Note that Ripple doesn't work with same-track transitions yet. You may then ripple the clip in the timeline to dragging the corners.

## Noise Suppressor for Voice

This new audio effect does miracles removing noises from your recording. You may find it under the audio effects tab.

## Importing folders ignoring sub-folder structures

You may now import your video footage or audio recording folder while automatically ignoring the folder structures created by some devices like Sony XDCam, Panasonic P2, Canon camcorders or Zoom audio recorders.

Please report any issues you are facing at <https://bugs.kde.org/enter_bug.cgi?product=kdenlive>. In the report don't forget to set 21.11.80 as your version!
