---
author: Jean-Baptiste Mardelle
date: 2017-01-13T17:31:31+00:00
aliases:
- /en/2017/01/kdenlive-16-12-1-released-with-windows-version/
---

![Screenshot of Kdenlive version 16.12.1 with the version number shown in the monitor](screenshot.png)

We are proud to announce the first maintenance release for the 16.12 cycle. Besides the usual bugfixes and usability improvements this release also marks the official release of the Windows port.

## Windows port

Last summer, thanks to a Google Summer of Code slot, Joseph Joshua started to work on a Windows port of Kdenlive. Vincent Pinon then continued to work on it and we are excited to offer today the much expected first version of Kdenlive for Windows. This is a first testing version, zipped in a folder that does not require an install. You must however separately <u>install FFmpeg</u>, following the simple instructions provided on the [download page][1].

## Packaging

Packaging effort is also going on the Linux side, with an up to date Appimage and *Ubuntu PPA containing the latest release, so you have no excuse not to try Kdenlive.

You can go straight to our [download page][1] to get the instructions to install Kdenlive 16.12.1

## About Kdenlive

Kdenlive is an open source video editing software, based on the MLT framework and FFmpeg libraries. We are a small team and are always welcoming new contributors.

We hold a monthly [IRC Kdenlive café][2] where users and developers meet, and you can also exchange on [our forum][3] or on our [mailing list][4]. Some great tutorials are also available from the [toolbox section][5] of our website.

We are part of the KDE community, which provides, among other, all the infrastructure for Kdenlive.
Donations are always welcome: <https://www.kde.org/community/donations/?app=kdenlive>

## Fixes and improvements in this version

More than 25 bugs were fixed in this release, as well as a few usability improvements.

 [1]: /download
 [2]: /categories/café/
 [3]: https://forum.kde.org/viewforum.php?f=265
 [4]: https://mail.kde.org/mailman/listinfo/kdenlive
 [5]: /toolbox/
