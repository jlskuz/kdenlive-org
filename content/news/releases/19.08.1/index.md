---
author: Farid Abdelnour
date: 2019-09-06T10:00:27+00:00
aliases:
- /en/2019/09/kdenlive-19-08-1-released/
- /it/2019/09/kdenlive-versione-19-08-1/
- /de/2019/09/kdenlive-19-08-1-ist-freigegeben/
---

![Screenshot of the project monitor with transform controls in Kdenlive version 19.09.1](screenshot.png)

The first minor release of the 19.08 series is out with usability fixes. The highlights include:

  * When using a resize effect on a video clip, Ctrl + resize allows you to keep the image centered.
  * Fixes for the custom audio effects that were broken.
  * The Encoder Speed in the render panel is working again allowing to set the encoder speed parameters to Slower, Medium, Faster and Ultrafast.

The stable AppImage is available from the [KDE servers][1].

 [1]: https://download.kde.org/Attic/kdenlive/19.08/linux/kdenlive-19.08.1b-x86_64.appimage
