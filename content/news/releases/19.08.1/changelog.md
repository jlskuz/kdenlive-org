<ul id="ulkdenlive">
  <li>
    Fix disabling clip only disable audio part of an AV clip. <a href="http://commits.kde.org/kdenlive/7c915ded1c85862293e7408d63f4b06895f98287">Commit.</a> Fixes bug <a href="https://bugs.kde.org/411466">#411466</a>
  </li>
  <li>
    Fix regression breaking timeline resize. <a href="http://commits.kde.org/kdenlive/9a51d703cd15a020aac35cbe6492f120305d65bc">Commit.</a>
  </li>
  <li>
    Fix timelinekeyboard focus on start and grab not correctly ended. <a href="http://commits.kde.org/kdenlive/853bbd53c8a6d72822526b4a757e667919df6b5f">Commit.</a>
  </li>
  <li>
    Default effects to video. <a href="http://commits.kde.org/kdenlive/9b1a25ec9905d3702c3ad5f20975eb89f820ec01">Commit.</a>
  </li>
  <li>
    Fix disabling autoscroll. <a href="http://commits.kde.org/kdenlive/1444cbd35987a57956330ab96a340b936490f89e">Commit.</a>
  </li>
  <li>
    Convert old custom effects to new customAudio/Video naming. <a href="http://commits.kde.org/kdenlive/4d0c9d43fb5e7ecffbf008763b4d30169e41db0e">Commit.</a>
  </li>
  <li>
    Fix group move sometimes moving clip very far from expected location. <a href="http://commits.kde.org/kdenlive/90d5443c7a0bed99fa1dc5a33e668074ce112bc9">Commit.</a>
  </li>
  <li>
    Ctrl resize in monitor effects keeps center position. <a href="http://commits.kde.org/kdenlive/60a1ba38540ce0a46bb700cc97a0f4c4cda02800">Commit.</a>
  </li>
  <li>
    Shift resize in monitor effect keeps aspect ratio. <a href="http://commits.kde.org/kdenlive/0e18e35c3cb1b9d6e5d748e1954a34a1b24b12fd">Commit.</a>
  </li>
  <li>
    Update appdata version. <a href="http://commits.kde.org/kdenlive/b603fa9ed8be9c0f82baaf40b10fb0691e65fed9">Commit.</a>
  </li>
  <li>
    Fix effect/composition list filter working on untranslated strings. <a href="http://commits.kde.org/kdenlive/2ce5ccdbe986febd3d36798dda2851a901888337">Commit.</a>
  </li>
  <li>
    Fix custom effects not recognized as audio. <a href="http://commits.kde.org/kdenlive/202bfe70285cb1959723ccf412f4d6fc308b1745">Commit.</a>
  </li>
  <li>
    Fix encoder speed ignored. <a href="http://commits.kde.org/kdenlive/6473cee38c4078ef8799ede4e5135ac12a2bad41">Commit.</a> Fixes bug <a href="https://bugs.kde.org/411000">#411000</a>
  </li>
  <li>
    Late update of version in appdata.. <a href="http://commits.kde.org/kdenlive/11ce38946ed8a77210869932d9f99c5ee897e26e">Commit.</a>
  </li>
  <li>
    Use the parameter readable and translatable name instead of its formal name for the color edit widget. <a href="http://commits.kde.org/kdenlive/c1d94545180236dda43ab819be899b0af4dd8283">Commit.</a>
  </li>
</ul>
