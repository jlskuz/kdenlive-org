---
author: Farid Abdelnour
date: 2016-09-08T02:23:48+00:00
aliases:
- /en/2016/09/kdenlive-16-08-1-released/
---

We are happy to announce a new dot release with some improvements and various fixes. We also celebrate some code contribution from Harald Albrecht (TheDive0) hoping to see more devs joining our team.

## Improved Workflow

### Persistent Directories

Now Kdenlive remembers where you last opened and/or saved a project.

### Responsive cursor

When inserting clips in the timeline the cursor in the timeline will go to the end of the inserted clip making the workflow more fluid and shortcut friendly.

### Configurable transition duration

The default duration of new transitions can now be adjusted

![Screenshot wiht input boxes for default durations in Kdenlive](default-duration.png)

## Interface

### Rounded corners

New option to make clip corners rounded or square.

### New profiles

Added 50fps and 60fps trancoding profiles


As usual sources can be downloaded KDE's servers at: http://download.kde.org/stable/applications/16.08.1/src/

Until we provide binary packages please as you distro's maintainers to update the packages.

Happy editing.
