---
title: Feature your productions in our website
author: Farid Abdelnour
date: 2018-06-19T15:48:27+00:00
aliases:
- /en/2018/06/feature-your-productions-in-our-website/
- /fr/2018/06/montrez-vos-productions-sur-notre-site-internet/
- /it/2018/06/2975/
categories:
  - Showcase
---

![](showcase-poster.jpg)

Hello editors

Besides the great refactoring that is going on, we are also revamping our website and adding a video gallery section to feature a selection of curated works/productions made with Kdenlive.

Visit our [forum][1] and share a link of your video along with a brief description, workflow and/or anything you find relevant to share with the community. (Only videos posted in the forum thread will be considered.)


 [1]: https://forum.kde.org/viewtopic.php?f=266&t=152894
