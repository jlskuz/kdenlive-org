---
title: After Sprint Café and new appointments
author: Massimo Stella
date: 2018-05-02T12:48:55+00:00
aliases:
- /en/2018/05/after-sprint-cafe-and-new-appointments/
- /fr/2018/05/apres-le-sprint-un-cafe-et-de-nouveaux-rendez-vous/
- /it/2018/05/2743/
categories:
  - Café
  - Event
---
After the [Sprint in Paris][1] and the Workshop at the Libre Graphic Meeting, it's time to move forward and to report what happened in the Kdenlive world during this intense week.

To do it in a participated way, we imagined that the best option could be a Special Cafè on Tuesday the 8th of May.

We also decided to a have a fixed date for the next Café events: starting from June our open meetings will be every first Tuesday of the month at 9 pm (UTC+2 during the Summer Daylight saving time and UTC+1 during the rest of the year). So you'll have no more to mind about pools to fill or late announcements. Just book on your calendar the event and participate!

I remind you that the Kdenlive Cafè is a monthly 2 hours informal open chat where everybody is invited to talk and discuss the future of our beloved Free Video Editor application and that the first 15 minutes are available to any kind of topic you propose.

To connect with us, you can use the Freenode IRC channel #kdenlive or our new [Community Channel on Telegram][2].

 [1]: https://kdenlive.org/en/2018/04/kdenlive-sprint-news-1/
 [2]: https://t.me/kdenlive
