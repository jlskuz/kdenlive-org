---
title: Bugsquashing Day
author: Farid Abdelnour
date: 2018-11-20T09:00:36+00:00
aliases:
- /en/2018/11/bugsquashing-day/
- /fr/2018/11/journee-chasse-aux-bugs/
- /it/2018/11/bug-squashing-day/
categories:
  - Event
---

![](bugsquash2018.png)

On the 2nd of December, the Kdenlive team will be having a bug squash day in preparation for the major refactoring release due in April 2019. This is a great opportunity for interested developers to participate in the project. The team has triaged hundreds of reports, closing more than a hundred of them in the past month. We have also  made a list of entry level bugs you can get started with. For the more seasoned developers, there are plenty of options – be it a shiny feature request or a challenge to polish some non-trivial edges. To hack Kdenlive you need to know C++, Qt, QML or KDE Frameworks. Those with knowledge of C can join the fun by improving [MLT][1], the multimedia framework Kdenlive runs on. Those with no programming experience can join in testing fixes and features, as well as triaging more bug reports.

You can find the list of proposed bugs to solve in our workboard under “low hanging / junior jobs” section [here][2].
 
Help spread the word and join us!

## Getting started

### Contact us

* IRC – #kdenlive on Freenode
* Telegram – https://t.me/kdenlive

### Build it

* GNU/Linux instructions can be found [here][3].
* Windows instructions can be found [here][4].

### Get a developer account

To participate in bug squashing, you need a [KDE Identity][5] account.

### Submit your patches

Here is some information on how to [submit a patch][6]submit a patch.

### Learn more about Phabricator

Get familiar with [Phabricator][7].

## Schedule (CET)

### 10h-10h30

Introduction of the event, presenting goals and tools, test the latest refactoring AppImage, how to help the project.

### 10h30-12h

Present yourself, get help in your development setup, choose your bug and work on it with help from the team.

### 12h-13h

Lunch break


### 13h-17h

Continue morning tasks or present yourself, get help in your development setup, choose your bug</span> and work on it with help from the team.

### 16h-17h

Brainstorming: [Timeline colors][8]

### 17h-18h

Closing thoughts, evaluation, general issues, future planning.

  [1]: https://www.mltframework.org/
  [2]: https://phabricator.kde.org/project/view/40/
  [3]: https://community.kde.org/Kdenlive/Development
  [4]: https://community.kde.org/Kdenlive/Development/WindowsBuild
  [5]: https://community.kde.org/Infrastructure/Get_a_Developer_Account
  [6]: https://community.kde.org/Get_Involved/development#Submitting_your_first_patch
  [7]: https://community.kde.org/Infrastructure/Phabricator
  [8]: https://phabricator.kde.org/T10085
