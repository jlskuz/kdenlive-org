---
title: Kdenlive in Paris
author: Jean-Baptiste Mardelle
date: 2018-04-16T06:58:45+00:00
aliases:
- /en/2018/04/kdenlive-in-paris/
categories:
  - Event
  - Sprint
---

![](paris.png)

The next weeks will be exciting for Kdenlive! First, there is a Kdenlive sprint, that will take place in Paris from the 25th to the 29th of april. We are very proud to be hosted at the [Carrefour Numérique][1] in the [Cité des Sciences][2], Paris. Our team will dedicate this time to discuss near and long term goals, review the application workflow with professional editors, work on the major 18.08 release and much more. You can expect frequent updates during this event, and we  will open the doors for two public events in Paris (entrance is free for the Carrefour Numérique, see [access map][3]):

  * **Friday, 27th of april, 4pm to 6pm**: open to anyone interested to get involved. Meet out team and learn how to join the project.
  * **Saturday, 28th of april at 2.45pm**: public presentation, discover Kdenlive as used by professional editors and insights into the new features.

Just after that, part of the team will fly to Seville to attend the [Libre Graphics Meeting][4] for a Kdenlive workshop on the 30th. So spread the word and come visit us in Paris or Seville!

This sprint was made possible through support by the KDE e.v., so if you want more, please consider a [donation][5].

If you are interested to follow Kdenlive, you are also welcome to join us in our [next Café][6], tomorrow, 17th of april at 9pm.

 [1]: http://www.cite-sciences.fr/en/explore/learn/carrefour-numerique2/
 [2]: http://www.cite-sciences.fr/en/home/
 [3]: http://www.cite-sciences.fr/en/visit-us/access/
 [4]: http://libregraphicsmeeting.org/2018/
 [5]: https://www.kde.org/applications/multimedia/kdenlive/
 [6]: /2018/03/kdenlive-cafe-27-and-28-you-cant-miss-it/
