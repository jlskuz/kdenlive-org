---
title: 'Kdenlive cafés #25 and #26 – Everybody is invited'
author: Massimo Stella
date: 2018-01-10T13:42:02+00:00
aliases:
- /en/2018/01/kdenlive-cafes-25-and-26-everybody-is-invited/
categories:
  - Café
---
Are you an enthusiastic video producer, a developer, a simple user or just a curious person about the Kdenlive project? Participate to our next on line meetings to stay updated, give your feedback, advises and opinions.

The first 20 minutes of our meetings are dedicated to an open discussion about any topic related to Kdenlive so everybody will have the chance to contribute to the discussion.

The Kdenlive community will be online on the Freenode IRC channel #kdenlive on Tuesday, January 16 and on Tuesday, February 20 at 9 pm UTC+1 (CET)

See you there!
