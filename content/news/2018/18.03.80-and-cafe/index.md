---
title: Kdenlive Café tonight and beta AppImage
author: Jean-Baptiste Mardelle
date: 2018-02-20T12:32:30+00:00
aliases:
- /news/releases/18.03.80/
- /en/2018/02/kdenlive-cafe-and-beta-appimage/
categories:
  - Café
  - Release
---

![](1804-beta.jpg)

The last months for Kdenlive have been very quiet from the outside – we were not very active on the bugtracker, did not make a lot of announcements, and the 17.12.x release cycle only contained very few minor bugfixes.

The main reason for this was the huge work that went behind the scenes for a major code refactoring that was required to allow further developments. So after more than a year working on it, we hope to get ready for the 18.04 release!

Tonight we will announce the first public testing beta release for the upcoming 18.04 version. There are some rough edges, it's not ready for production yet as compatibility with older project files is not finalized and a lot of work is still required to fix the many remaining quirks, but we are on good tracks to bring you a modern, stable and enjoyable open source editor.

So if you are interested and want to have a look at our roadmap or try the latest development code in an AppImage, join us **tonight** at 9pm (CET time – UTC+1) for our 26th [Kdenlive Café][1].

 [1]: https://notes.kde.org/public/kdenlive-cafe26
