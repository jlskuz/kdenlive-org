---
title: 'Kdenlive Paris sprint & LGM report'
author: Jean-Baptiste Mardelle
date: 2018-05-09T10:34:36+00:00
aliases:
- /en/2018/05/kdenlive-paris-sprint-lgm-report/
- /fr/2018/05/compte-rendu-du-sprint-de-paris-du-lgm-de-seville/
- /it/2018/05/2830/
categories:
  - Sprint
  - Event
---

Before we start we would like to thank the [KDE e.V.][1] (travel and hosting support) and the [Carrefour Numérique][2] (workspace) for helping make this events happen.

## Paris Sprint 2018

From the 25th to the 29th of April, [5 members of our team][3] had a sprint in Paris to focus on the future of Kdenlive. And it was fantastic! We met for the first time in person, made friends and worked a lot! But let's start with the beginning. We were warmly welcomed by Mathieu at the Carrefour Numérique, part of the Cité des Sciences in Paris.

On the first day after a brief introduction, the team started working on the vision of the project, defining objectives, discussing technical issues and schedules and by the end of the day came up with a roadmap (see below) with a clear set of short, mid and long term goals post the refactoring release.

![](board.jpeg)

One of the biggest changes proposed for the refactoring release is the deprecation of hybrid clips, which means that now there is a strict clip structure in which you can't move video clips to audio tracks and viceversa (see video below). Also a high priority list of tasks focused on stability (but with minor new features) was made for releasing the refactoring branch for the 18.08 release schedule which includes:

  1. Make a stable release.
  2. Don't break compatibility with older versions.
  3. Finish implementing strict mode.
  4. Folder import. (New feature)
  5. Sort effects+ make translatable.
  6. Speed control via Ctrl + resize. (New feature)
  7. Loop clip in timeline Shift + resize. (New feature)

Besides all the planning and **heavy bugfixing** of the refactoring branch during the 4 days of the sprint. We also took steps towards improving our communication infrastructure by adding multilingual support to the website and started translation in French and Italian, defined fixed dates for the [Kdenlive Cafés][4], created a bridged [Telegram][5] group with #kdenlive at IRC (also working on bridging it with Matrix) and a francophone [Telegram][6] group, and discussed a strategy for dealing with the bugtracker after the refactoring release.

**We also had 2 public events:**

  * A meeting for people interested to help the project, which resulted in a nice contribution by Camille: a rewrite of the [developer info in the wiki][7]. We also received a patch adding the possibility to manage and download keyboard shortcut templates.
  * A public presentation where we met several very enthusiasts users.

![](jbm-paris-meetup.jpg)

## Some highlights of things we did during the sprint:
  
Audio and video are now always split for a cleaner workflow. You can see a preview in the following video:
 
{{< video src=drop.mp4 autoplay="true" muted="true" loop="true" controls="false">}}
  
The monitors now have a nicer overlay toolbar and support multiple layout guides:

{{< video src=overlay.mp4 autoplay="true" muted="true" loop="true" controls="false">}}

## At Libre Graphics Meeting

After the sprint some of the team members headed to Seville-Spain to give a workshop at the [Libre Graphics Meeting][8].
  
![](/kdenlive-lgm-2.jpg)

![](/kdenlive-lgm.jpg) 

It was heartwarming to see how big is our community and meet with videomakers who use (or want to use) Kdenlive for their productions. The workshop also was a place to share all the sprint news, general Q&A, listen to feature requests and bug reports. LGM was also a place to meet the developers of other FLOSS programs like Gimp, Krita and Inkscape among others and explore possibilities of integration between our projects. An example is the conversations we had with [Timothee Giet][9] ([Krita][10]) and [Jehan Pagés][11] ([Gimp][12]) about the [Openraster][13] spec and possible methods of importing .ora files into Kdenlive. We also had a chance to meet community of FLOSS Manuals a started talks on a possible cooperation for writing a Kdenlive manual.  This is very important as the refactoring version will bring new workflows and some modifications in the user interface.
  
Our presence at LGM 2018 is a step forward towards the planned integration of Kdenlive with other programs eventually such as Blender, Natron and Ardour. See you at LGM 2019!
  
### Roadmap

The following is our proposed roadmap for the coming releases after the refactoring is finished.
  
#### Short term
  
* Show audiowave in monitor
* Trimming operations
* Nested timeline
* Same track transition
* Corruption fixes
* Check consistency before save
* Refresh button
* Info dialog for clips, to be able to set in/out points precisely
* Highlight on snap
* Clip locking

#### Mid term
  
* Audio workflow
* Multicam
* Fast render (multicore)
* Secondary color correction
  
#### Long term
  
* Cross platform
* GPU processing
* Great bit-depth color support
* OpenRaster support (Krita/Gimp)
* Blender/Natron integration
* Ardour Integration
* Frame caching
* Tracking/Stabilization
* AI powered features
* Style transfer
* Segmentation

 [1]: https://ev.kde.org/
 [2]: http://www.cite-sciences.fr/fr/au-programme/lieux-ressources/carrefour-numerique2/
 [3]: https://kdenlive.org/en/2018/04/kdenlive-sprint-news-1/
 [4]: https://kdenlive.org/en/2018/05/after-sprint-cafe-and-new-appointments/
 [5]: https://t.me/kdenlive
 [6]: https://t.me/kdenlivefr
 [7]: https://community.kde.org/Kdenlive/Development
 [8]: https://libregraphicsmeeting.org/
 [9]: https://www.timotheegiet.com/
 [10]: https://krita.org
 [11]: https://girinstud.io/en/
 [12]: https://gimp.org
 [13]: https://openraster.org/
