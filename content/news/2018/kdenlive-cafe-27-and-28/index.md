---
title: 'Kdenlive Café #27 and #28 – You can’t miss it'
author: Massimo Stella
date: 2018-03-16T13:24:40+00:00
aliases:
- /en/2018/03/kdenlive-cafe-27-and-28-you-cant-miss-it/
categories:
  - Café
---
Timeline refactoring, new Pro features, packages for fast and easy install, Windows version and a bunch of other activities are happening in the Kdenlive world NOW!

Be part of our growing community and bring your contribution to create the best video editor application ever.

Join us for the next Kdenlive café #27 and #28, on Tuesday the 20th of March and on Tuesday the 17th of April on the Freenode IRC channel #kdenlive at 9 pm UTC+2 (CEST).

We are waiting for you.
