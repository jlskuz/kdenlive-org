---
title: 'Kdenlive sprint news #1'
author: Farid Abdelnour
date: 2018-04-27T16:37:43+00:00
aliases:
- /en/2018/04/kdenlive-sprint-news-1/
- /fr/2018/04/rencontre-sprint-kdenlive-in-paris/
- /it/2018/04/2654/
categories:
  - Event
  - Sprint
---

![](IMG_8152-1080.jpg)

Part of the Kdenlive team is currently meeting in Paris at the La cité des sciences e de l'industrie to improve the project. We've tackled several goals, starting with being together. The magic of this kind of project leads to situations where we work together without meeting each other. Thus, we were able to live, share and especially spend good times to work together in a good mood. It was also useful for making important decisions after rich and lively discussions, exchanging varied points of view related to our respective experiences. Expect big changes very soon. Do you want to know more? [Come join us][1] at our new Telegram group!

{{< video src=ambiance.webm >}}

 [1]: https://t.me/kdenlive
