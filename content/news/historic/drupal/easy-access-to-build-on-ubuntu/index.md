---
title: 'Easy access to Kdenlive builds on *ubuntu'
author: Vincent Pinon
date: 2016-03-26T23:04:00+00:00
aliases:
- /node/9460
# Ported from https://web.archive.org/web/20160602023531/https://kdenlive.org/node/9460
---


Hurrah, Launchpad now handles git, and can generate daily builds directly from this clone!  
So ubuntu users now have 3 repositories to get the latest of Kdenlive:

* [ppa:kdenlive/kdenlive-master][1] is the development branch, with the very latest features additions, to give us feedback on the app evolution
* [ppa:kdenlive/kdenlive-testing][2] is the feature-frozen branch, starting from the first beta, with bugfix updates as soon as we find solutions
* [ppa:kdenlive/kdenlive-stable][3] is the last robust official release

As all these require Qt5/KF5, builds are activated for Wily (for some time) and Xenial (LTS to be released soon)... Live in the present ;)

This work is now owned by a team on Launchpad, you are welcome to join if you want to co-maintain the packages. I deleted my own unmaintained PPA, and invite users to switch to one of the above.

If daily builds are available on other distributions, maintainers are welcome to advertise their work on our wiki, and will be glad to relay the info here!

Happy testing and editing!


  [1]: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-master
  [2]: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-testing
  [3]: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-stable
