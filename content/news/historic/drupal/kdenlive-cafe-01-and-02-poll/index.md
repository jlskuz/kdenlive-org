---
title: 'Kdenlive Café #1 and #2 - Date selections'
author: Mario Fux
date: 2015-12-07T17:16:00+00:00
aliases:
- /node/9448
categories:
- Café
- Event
# Ported from https://web.archive.org/web/20160413140404/https://kdenlive.org/node/9448
---


We would like to try something new for the Kdenlive community: Kdenlive Cafés. These are one to two hour informal meetings on irc.freenode.net in channel #kdenlive. We might select certain topics for these Cafés but normally you can just chat about recent things in Kdenlive development, ask questions to developers and other users and talk about the progress of this great and free non-linear video editor.

So to select the dates and times for the first two Cafés please add yourself to this [date selection tool][1]. On Sunday, the 13th of December 2015 we will close the date selection and announce the first Café here, in the [forum][2] and on the [Kdenlive mailing list][3].

UPDATE: The timezone would be Central European Timezone - UTC+1.


  [1]: https://web.archive.org/web/20160413140404/http://dudle.inf.tu-dresden.de/lbmbyx5z/
  [2]: https://web.archive.org/web/20160413140404/https://forum.kde.org/viewforum.php?f=262
  [3]: https://mail.kde.org/mailman/listinfo/kdenlive
