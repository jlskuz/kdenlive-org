---
title: 'Track effects in Kdenlive 0.7.8'
author: Jean-Baptiste Mardelle
date: 2010-09-12T17:27:00+00:00
aliases:
- /users/j-b-m/track-effects-kdenlive-078
categories:
- Inside
# Ported from https://web.archive.org/web/20160430035006/https://kdenlive.org/users/j-b-m/track-effects-kdenlive-078
---


![](track_effects1.jpeg)A new feature introduced in Kdenlive is track effects. There are several situations where this can be useful, for example:

* You want to make a black and white movie
* You need to correct the audio balance of all your clips

So now, with Kdenlive 0.7.8, instead of applying the effect to all clips in timeline, you can simply add the effect to the track, and all clips in that track will have the effect applied.

To apply an effect on a track, just drag & drop an effect from the *Effect list* widget to your track header (see screenshot on the left). You can then adjust your effect settings just like usual (but note that because we had not enough time, keyframes are not yet supported on track effects).

![](track_effects2.jpeg)When an effect is applied on a track, the track displays a small star icon next to the track name so that you know that effects are applied on the track.

The second screenshot (on the right) shows a sepia effect on track 2. No effect was applied to the clip but since it is on track 2, the sepia effect is applied on it.

We hope that you will find this useful!
