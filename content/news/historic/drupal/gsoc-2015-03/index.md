---
title: 'GSoC 2015: Kdenlive Wrapping Things Up'
author: Gurjot Singh Bhatti
date: 2015-08-21T14:42:00+00:00
aliases:
- /node/9443
categories:
- GSoC
- Mentorship
# Ported from https://web.archive.org/web/20160408142344/https://kdenlive.org/node/9443
---


With GSoC coming to close, so does the project work. The goal of my project was to upgrade the animation capabilities to allow much simpler, smoother and more general animations than the traditional keyframes technology.

Kdenlive had support for two keyframe types, i.e. Simple Keyframes and Geometry Keyframes. Now with the introduction of Animation Property in MLT API, it is now possible to introduce robust animation keyframes.

I have created Animation Keyframe widget which now support all forms of interpolation:

* Discrete
* Linear
* Smooth Catmull-Rom spline
* Mix of the above three.

Take a look at the following screenshot, which shows this widget for Volume Effect.

![](s1.png)

The string value is "0=0; 100|=-250; 350~=300; 490=50", where discrete keyframes are represented as (|), linear interpolated keyframe as (=), and smooth spline keyframes as (~).

And this is the timeline view for all the keyframes.

![](s2.png)

The keyframe type can be changed from the combo box as shown in the widget. The third keyframe is of Smooth/Spline type and therefore the string value is "350~=300" where 350 is the frame number whereas 300 is the value at that keyframe. Therefore, the curve is as:

![](s3.png)

In this type, a bezier curve is calculated from all the keyframes but only the required portion of that complete curve is drawn, i.e only between two specific keyframes. The catmull points are used to convert the curve to the bezier curve.

Now if we change the type to discrete, we will have the string value as "350|=300" and the curve would change to:

![](s4_0.png)

In this type, two line segments are drawn between two keyframes (x1,y1) and (x2,y2) first one is from the (x1,y1) to the (x2,y1), keeping the y coordinate same, and second is from the (x2,y1) to the (x2,y2), keeping the x coordinate same.

And if it is changed to linear, the string value would be "350=300" and the curve as:

![](s5_0.png)

In this type, a line segment is drawn between two keyframes (x1,y1) and (x2,y2).

Other Features similar to the keyframe editor (widget):

* You can change the keyframe position, i.e. Frame and Level value from the widget using scroll or drag
* You can drag the keyframes directly on the clipitem on the timeline
* You can add, delete, reset keyframes

The animated keyframe property now can be applied to other supported effects in Kdenlive as well.

In this journey, I have learned a lot, I understood the importance of reading other people's code which is very important if we have to work on a large project. And also the debugging. I have dived in the MLT API which I think is pretty useful and powerful.  
I am very thankful to my mentor Vincent Pinon as well as Jean-Baptiste Mardelle for their guidance.  
It has been a wonderful experience working on Kdenlive in GSoC, and I will continue to be part of it. :-)

