---
title: 'News from Randa, Café and next release'
author: Jean-Baptiste Mardelle
date: 2016-07-05T09:34:00+00:00
aliases:
- /node/9469
categories:
- Randa
- Café
- Event
# Ported from https://web.archive.org/web/20160706163023/https://kdenlive.org/node/9469
---


## Café

We will have our monthly Kdenlive Café **today, 5th of july at 9pm** CEST (UTC+02), so feel free to join us on irc, #kdenlive channel. We will mostly discuss our plans for the upcoming 16.08 release which is coming soon!

## Development

The latest developments include a welcome cleanup of the Kdenlive's Wizard, this annoying dialog that popped up on the first start of Kdenlive. It will now appear only if a configuration problem is detected. So instead of forcing the first time user to choose several complicated options, Kdenlive now starts up picking the best default options (including NTSC/PAL framerate depending on your region). So I finally implemented this idea that was first mentionned by the [VDG in Randa 2015][1]!

## Speaking of Randa Meetings...

Randa 2016 was the occasion to meet Joseph who is currently working on the Kdenlive Windows port. The Windows port involves a lot of configuration fine-tuning and lengthy compilations, but is steadily progressing and we hope to have the first Kdenlive Windows built in the next days. Keep in mind that this will be an experimental version, and to have it really working will probably require a long term effort but still this is exciting!

The path to Windows is also a good opportunity to do some cleanup and improve Kdenlive on Linux in non KDE environments. On my side, I fixed several stability issues related to timeline refactoring, and also worked on icons to improve the user experience when using Kdenlive on non KDE Linux Desktops.

This was also the occasion to exchange with other developers about the recent packaging formats developments, since a major issue for us is to deliver a recent Kdenlive version with updated libraries to the end user.

![](randa-16.png)

Joseph, Grace and me in Randa during the afternoon trip

If you want to support us, please [donate][2] to help us keeping up these developer meetings!

[![](/images/banners/fundraising2016.png)][1]


  [1]: /node/9445
  [2]: https://web.archive.org/web/20160706163023/https://www.kde.org/fundraisers/randameetings2016/
