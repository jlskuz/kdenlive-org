---
title: 'We’re in Randa!'
author: Simon A. Eugster
date: 2011-06-05T16:53:00+00:00
aliases:
- /users/granjow/we-re-randa
categories:
- Randa
- Sprint
- Event
# Ported from https://web.archive.org/web/20160407190300/https://kdenlive.org/users/granjow/we-re-randa
---


At the moment nearly the whole Kdenlive team is hacking in Randa, Switzerland, at the KDE Sprint 2011. Don’t believe it? Here is the proof:

![](kdenlive_team_randa.jpg)

(From left to right, ttill, xzhayon, j-b-m, Granjow.) So, as you can see, we are totally busy coding and writing the manual. The main activities are:

* **Big refactoring** which will improve our code base in terms of maintainability and (for new coders) readability. We created a [new branch][1] for the refactoring. When refactoring is complete, we should also be able to write some automated tests.
* **User manual**; we are now definitely moving to KDE Userbase which is based on MediaWiki (you all know this from Wikipedia). It will also simplify translation. The first version of the [Quickstart tutorial][2] is already online.
* **GIT** will be our new version control system at KDE multimedia.

That’s it so far! We’ll go back coding now.  
— Your Kdenlive developers


  [1]: http://kdenlive.svn.sourceforge.net/viewvc/kdenlive/branches/refactor-0.8/
  [2]: https://userbase.kde.org/Kdenlive/Manual/QuickStart
