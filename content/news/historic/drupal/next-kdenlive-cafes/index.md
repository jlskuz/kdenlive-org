---
title: 'Next Kdenlive cafés'
author: Jean-Baptiste Mardelle
date: 2016-03-31T11:40:00+00:00
aliases:
- /node/9461
# Ported from https://web.archive.org/web/20160601202832/https://kdenlive.org/node/9461
---


After our successful 4th edition of the Kdenlive café[^1], we are preparing the next 2 meetings.  
If you are interested to join us to discuss the future of Kdenlive, you are welcome.  
Dates and times will be chosen this sunday, so add yourself to the date selection tool to get a chance to meet us: [https://dudle.inf.tu-dresden.de/Kdenlivecafe5and6/][1]

![](cafe-05.jpg)

Kdenlive 16.04 titler improvements : shadow, line and letter spacing, gradients.

Hope to meet you there, your Kdenlive team!

[^1]: Kdenlive café is a two hour informal meetings on irc.freenode.net in channel #kdenlive

  [1]: https://web.archive.org/web/20160601202832/https://dudle.inf.tu-dresden.de/Kdenlivecafe5and6/
