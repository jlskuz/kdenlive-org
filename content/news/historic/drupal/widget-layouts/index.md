---
title: 'Widget layouts in kdenlive'
author: Simon A. Eugster
date: 2011-01-11T20:11:00+00:00
aliases:
- /users/granjow/widget-layouts-kdenlive
categories:
- Tutorial
# Ported from https://web.archive.org/web/20160606041401/https://kdenlive.org/users/granjow/widget-layouts-kdenlive
---


You may probably have noticed that some additional widgets have been added to kdenlive recently. And you probably do not want your kdenlive to look like that:

![](kdenlive-layout-none.png)

(Notice that this is on a 1680×1050 screen. You absolutely don't want kdenlive to look like that on an even smaller screen.)

## Layouts in kdenlive

In 0.7.9 our main developer, Jean-Baptiste (j-b-m), has added a new menu point to save and restore custom layouts:

![](kdenlive-layout-select.png)

This way you can use specialized layouts for different tasks, showing as few widgets as possible and as few as necessary, to maximize the available and usable space.

Some example layouts I am currently using:

### Editing

![](kdenlive-layout-editing.png)

Tuned for moving clips around, applying effects and transitions. The timeline is as long as possible, the monitors are at maximum size. Widgets shown:

* Project/Clip/Record monitors
* Project Tree
* Effect Stack (you may also want to display the Effect List; I use to hide it when I already know which effects to use, and how they are called)
* Transition
* Notes (After rendering I usually take notes about details to improve, like: *2:54 → Show text longer* or *4:20 → Color correction missing*)

### Audio

![](kdenlive-layout-audio.png)

Since 0.7.9 will introduce audio scopes. This layout is, you guessed it, for making coffee. Please post your results below.

* Monitors
* Transition, Effect Stack (Audio effects want to be modified if e.g. clipping is detected)
* Spectrogram (Will now highlight everything that is above the given maximum value! At least if you chose it to in the context menu.)
* Audio Spectrum (Will now display the maximum of the past few samples for easier clipping detection! End of advertisments.)
* Audio Signal (Shows volume of all available channels)

### Colour Correction

![](kdenlive-layout-colorcorrection.png)

You know that this just had to come if you have read [my posts about colour correction][6].

* Monitors
* Transition, Effect Stack, Notes, Project Tree
* All Colour Scopes

## Thanks

for reading, and thanks jb for cleaning up my mess ;)  
— Simon A. Eugster *(Granjow)*


  [6]: https://web.archive.org/web/20160606041401/http://kdenlive.org/blogs/granjow
