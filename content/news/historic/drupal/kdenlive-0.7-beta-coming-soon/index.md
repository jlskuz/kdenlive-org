---
date: 2008-09-27
title: Kdenlive 0.7 beta coming soon
draft: false
author: Jean-Baptiste Mardelle
---

The Kdenlive 0.7 beta release should be ready around the 6th of october. This will be the first release of Kdenlive available for KDE4.

The proposed roadmap is:

Friday 3rd of October: string freeze

Monday 6th of October: release of Kdenlive 0.7 beta1

If no major issue is detected, the final Kdenlive 0.7 release should be ready by the 25th of October.
