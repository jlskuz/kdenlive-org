---
title: 'Improving effects workflow in Kdenlive'
author: Jean-Baptiste Mardelle
date: 2012-04-01T15:55:00+00:00
aliases:
- /users/j-b-m/improving-effects-workflow-kdenlive
categoires:
- Inside
# Ported from https://web.archive.org/web/20150905111114/https://kdenlive.org/users/j-b-m/improving-effects-workflow-kdenlive
---

![](effectstack.png)

First, I should say a big thank you to all the people that helped make our [Fundraising campaign][1] so successful, but more infos about that will come in the next days. It is anyways really motivating to see that you are supporting us!

On my side, I have been working on a rewrite of the Kdenlive's effect stack. This work has just been merged into master git, so users of sunab's experimental PPA will get it in the next update, probably in 1-2 days.

The highlights are:

* Adjust parameters for several effects without switching between effects
* Drag & drop effects from the effect stack to another clip or track
* Create effect groups that can be saved or dropped to easily pass effects to another clip or track

I tested the new features for some time now, there are probably a few bugs, but it is really worth it in my opinion, So I am waiting for your feedback & bug reports. And here is a small video demonstrating the new effect stack ([video link][2]):

{{< youtube RvKrzM_TWlE >}}

Jean-Baptiste Mardelle


  [1]: /users/ttill/kdenlive-fundraising-campaign
  [2]: https://youtu.be/RvKrzM_TWlE
