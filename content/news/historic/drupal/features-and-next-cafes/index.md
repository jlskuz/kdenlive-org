---
title: 'Kdenlive: features and next Cafés'
author: Jean-Baptiste Mardelle
date: 2016-05-22T21:13:00+00:00
aliases:
- /node/9465
categories:
- Café
- Event   
# Ported from https://web.archive.org/web/20160523103922/https://kdenlive.org/node/9465
---


Our monthly Kdenlive Cafés [^1] really helped us focusing the development on some awesome features. We now have a small team of really involved people that help us evolve towards the best free open source video editor for professionnals.

We have opened the poll for our next Café date selection, so if you want to meet us, add yourself to the list: [https://dudle.inf.tu-dresden.de/Kdenlivecafe7and8/][1]

And to give you an idea of what we do these days, here is a video of the last feature added in git master: timeline preview.

{{< video src-webm="timeline-preview.webm" muted="true" >}}

Sometimes, you just have too much effects or transitions in your timeline to be able to play it in realtime (you can see on the video, it can only play 13 frames per seconds). This is where this new feature helps you: it pre-renders parts of the timeline so that they can be played in realtime. And you can continue working on other parts of the timeline while Kdenlive is working in the background for you.

More exciting news will come in the next months, so stay tuned and join us for the café!

[^1]: Kdenlive café is a two hour informal meetings on irc.freenode.net in channel #kdenlive


  [1]: https://web.archive.org/web/20160523103922/https://dudle.inf.tu-dresden.de/Kdenlivecafe7and8/
