---
title: 'Kdenlive - latest developments'
author: Jean-Baptiste Mardelle
date: 2011-12-30T20:07:00+00:00
aliases:
- /users/j-b-m/kdenlive-latest-developments
categories:
- Inside
# Ported from https://web.archive.org/web/20160322233246/https://kdenlive.org/users/j-b-m/kdenlive-latest-developments
---


Here is a quick info on the features introduced in Kdenlive git since the release of Kdenlive 0.8.2.1.

## Audio Recording

![](audio_rec.jpeg)

Capture through MLT's avformat producer now supports audio only capture. It even works while playing the timeline, so that you can record a comment while viewing your project. You can see a screenshot of the updated capture widget.

## Clip Jobs

![](clip_job.jpeg)

A generic job framework was introduced, based on the proxy clip feature. Here are some of the consequences:

* It means first that the proxy clip got some UI improvements. Progress of the proxy job is now shown in a small progress bar at the bottom of the clip thumbnail, so that the operation is not distrubing your workflow.
* Then, the new job framework is intended to make it easier to introduce new operations on project clip, for example a new *Extract zone* feature allows to cut parts of a clip without re-encoding (using FFmpeg codec copy feature).
* Another related feature is the introduction of clip analysis for some effects. For now, it is available in the "Sox Gain" effect that now has a "Normalize" button. This will trigger an analysis of the clip to find the best gain correction.
* More jobs are expected to appear, for example the *Stabilize* feature could be ported to it.

## Online services integration

![](freesound.jpeg)

A new *Online Resource* widget made its appearance, and allows you to easily browse and download resources from the web. Currently, two services are implemented:

* The excellent [Freesound][1] free audio library allows you to search and import audio clips in a few clicks.
* The great [Open Clip Art][2] library allows you to import graphic files.

That's all for now, but I am quite happy with those newly introduces features, after all those months spent fixing bugs for the last release.

Interested testers can get the current git version from sunab's [experimental PPA][3], or through the [Kdenlive Build script][4].

Next, we have to work on the refactoring suff to improve our existing code, make it more modular and easier to contribute.

Thanks for reading, Jean-Baptiste Mardelle


  [1]: https://freesound.org/
  [2]: https://openclipart.org/
  [3]: https://launchpad.net/~sunab/+archive/kdenlive-svn
  [4]: https://www.mltframework.org/docs/buildscripts/
