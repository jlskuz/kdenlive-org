---
title: 'Hacking Kdenlive at Randa'
author: joejoshw
date: 2016-06-23T11:46:00+00:00
aliases:
- /node/9470
categories:
- Event
- Randa
# Ported from https://web.archive.org/web/20160624115939/https://kdenlive.org/node/9470
---


The Randa meetings 2016 just ended, and they were a big success for everyone involved (thanks to Mario and his team for organizing this).

We went there with an aim to work on Kdenlive's Windows port, and we managed to achieve more than 80% of the build process.

We discussed two approaches on how to achieve this:  
1. Using Emerge to compile natively on Windows  
2. Using MXE to cross compile on Linux

We decided to use MXE, as it gives us the flexibility of building everything on Linux, from compiling the binaries to packaging the Windows setup files (using NSIS).

The main achievements were:

## Setup MXE and build windows version of Qt on Linux

This was relatively easy as Qt5 makefiles already exist in MXE. Getting and using MXE is very straightforward, as all you have to do is clone it from Guthub ([https://github.com/mxe/mxe][1]), then compile any packages you need. For example to compile Qt5 you just run `make qt5` and MXE will compile Qt and all of its dependencies.

## Building MLT for Windows

MLT is a crucial component of Kdenlive, and we needed to add scripts for building it on MXE. Most ot MLT's dependencies are already on MXE except for ladspa-sdk and frei0r, which we added and also created an MXE build script for MLT. Vincent Pinon was of great help during this step.

## Build KDE Frameworks for Windows

Jean-Baptiste also came to Randa and provided his support in building of the frameworks.  
This is the current ongoing part of the project. We have managed to compile most of the frameworks, except 3 of them that rely on DocBookXML4 and DocBookXSL, which we have to add to MXE.

## Next Steps

The next steps now will be to finish the build, and then use NSIS to generate the windows setup files.

[![](/images/banners/fundraising2016.png)][2]


  [1]: https://github.com/mxe/mxe
  [2]: https://web.archive.org/web/20160624115939/https://www.kde.org/fundraisers/randameetings2016/
