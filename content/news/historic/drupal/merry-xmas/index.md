---
title: 'Merry Xmas!'
author: Simon A. Eugster
date: 2010-12-23T20:38:00+00:00
aliases:
- /users/granjow/merry-xmas
# Ported from https://web.archive.org/web/20160606041227/https://kdenlive.org/users/granjow/merry-xmas
---

{{< vimeo 18127754 >}}

[Merry Xmas!][1] from [Simon Eugster][2] on [Vimeo][3].


  [1]: https://vimeo.com/18127754
  [2]: https://vimeo.com/granjow
  [3]: https://vimeo.com/
