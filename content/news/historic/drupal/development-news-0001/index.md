---
date: 2009-03-04
title: Development news
draft: false
author: Jean-Baptiste Mardelle
categories:
- Inside
---

Hi everyone!

Here is a quick info on the recent development:

## Subversion move

For all those who are getting the Kdenlive source through subversion, we just moved the development to /trunk. If you have a copy of the KDE4 branch, go into it and do:

```
svn switch https://kdenlive.svn.sourceforge.net/svnroot/kdenlive/trunk/kdenlive
```

## New features

Some of the features that are implemented in the development version and will be part of the next Kdenlive 0.7.3 release:

- **Rendering scripts**: You can now create rendering scripts that will allow you to launch the rendering whenever you want by just starting a bash script (image 1)
- **Web updates**: We are implementing content update through the web. That means you will be able to download new rendering profiles from within Kdenlive, and sharing will be made easier, more info on that later (image 2)
- **Double pass encoding**: Rendering with 2 pass encoding is now available, just add "pass=2" to your rendering profile, feedback welcome
- **Track locking**: Tracks can be locked so that clips on it cannot be moved
- **Filters**: Some new filters were added by Dan Dennedy, including Color Balance and Cropping
- **Monitor background**: The monitor now clearly shows the video area, the surrounding area color can be defined by the user (image 3)
- And of course the usual bugfixes...


We hope to release Kdenlive 0.7.3 in a few weeks.

![Scripts](/images/screenshots/dev-2009-03-04-scripts.jpeg)
![Monitor](/images/screenshots/dev-2009-03-04-monitor.jpeg)
