---
title: Kdenlive's sprint report
author: Jean-Baptiste Mardelle
date: 2016-02-06T20:06:00+00:00
aliases:
- /node/9457
categories:
- Sprint
- Event
# Ported from https://web.archive.org/web/20160413180409/https://kdenlive.org/node/9457
---


Last week-end, Vincent and me met in Lausanne for a Kdenlive sprint. One of our goal was to merge Gurjot Singh Bhatti's GSoC work on curves for keyframes. This was more work than expected and we spent many hours trying fix the curves and make keyframes behave correctly. Not much time was left for sleep, but we still managed to get outside to make a group (!) picture in the woods above Lausanne.

![](sauvabelin.jpg)

The code is working and in git master but cannot be used yet - we want to make sure older projects convert properly to the new keyframe system before enabling it - hopefully in less than a week. Below is a small demo of the feature:

![](keyframes2.gif)
  
Currently, only 1 dimension parameters are supported, but we hope to extend this to the transitions for the 16.04 release, allowing to move and resize the video using smooth interpolation instead of the current linear way.

During our meeting, we also discussed several UI changes that were initiated at Randa 2015, and hope to make some progress on that for the april release.

Next friday, the 12th of february (at 11am CET time), the [3rd Kdenlive café][1] will be an occasion to discuss and exchange about Kdenlive's developments and community.


  [1]: /node/9456
