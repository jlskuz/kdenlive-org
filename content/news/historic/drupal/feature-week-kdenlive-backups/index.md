---
title: 'Feature of the week in Kdenlive: backups'
author: Jean-Baptiste Mardelle
date: 2011-06-07T09:49:00+00:00
aliases:
- /users/j-b-m/feature-week-kdenlive-backups
# Ported from https://web.archive.org/web/20150905182919/https://kdenlive.org/users/j-b-m/feature-week-kdenlive-backups
---


![](backup.png)

While [working hard in Randa][2], I implemented a feature that will make everyone happy: automatic backup of your project file!

We had a few reports on the forum of broken project files that were not recoverable. This is not a frequent issue, but losing all the work you have done on a video editing project is not fun! So after a short discussion with the Kdenlive team, here it is!

With this feature, every time you save your project file, instead of overwriting the existing version, it will create a dated backup of the previous version. In the *Project* menu, you will now have an *Open Backup File* entry that will bring up the dialog pictured here.

In the backup dialog, you see a list of all backuped versions of your project file with a nice screenshot of the timeline at the time the backup was made. You can then open any of the backup file to recover a previous version.

This is in svn and will be part of the next Kdenlive release.

A big thank you to all the people who made the Randa Sprint possible and so enjoyable!


  [2]: /users/granjow/we-re-randa
