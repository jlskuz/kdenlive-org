---
title: 'Next Kdenlive Cafés'
author: Jean-Baptiste Mardelle
date: 2016-01-19T23:10:00+00:00
aliases:
- /node/9455
# Ported from https://web.archive.org/web/20160413125358/https://kdenlive.org/node/9455
---


We want to continue the great experience we had with the Kdenlive café, a two hour informal meeting on irc.freenode.net in channel #kdenlive.

![](kdenlive-cafe.png)

This is an opportunity for Kdenlive developers and users to exchange ideas, talk about how we want to see the Kdenlive project evolve and also discuss how you can help us on that way!

As was done last time, we will select dates and times for the next two Cafés, so please add yourself to this date selection tool [https://dudle.inf.tu-dresden.de/Kdenlivecafe3and4][1].

On Sunday, the 24th of January 2016 we will close the date selection and announce the third and fourth Café here, on kdenlive.org and on the [Kdenlive mailing list][2].

The reference timezone is Central European Timezone (UTC+1).

Hope to see you there!


  [1]: https://web.archive.org/web/20160413125358/https://dudle.inf.tu-dresden.de/Kdenlivecafe3and4/
  [2]: https://web.archive.org/web/20160413125358/https://mail.kde.org/mailman/listinfo/kdenlive
