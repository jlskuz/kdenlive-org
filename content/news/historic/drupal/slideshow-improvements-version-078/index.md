---
title: 'Slideshow improvements coming in version 0.7.8'
author: Dan Dennedy
date: 2010-09-04T09:48:00+00:00
aliases:
- /users/ddennedy/slideshow-improvements-coming-version-078
categories:
- Inside
# Ported from https://web.archive.org/web/20160430030359/https://kdenlive.org/users/ddennedy/slideshow-improvements-coming-version-078
---


In addition to my work on MLT, I helped to add some things to Kdenlive. One of them is improvements in the Slideshow Clip. A lot of work to make this possible within MLT happened over the course of the year. The two main additions are: **Center crop** and **Animation**.

![](kdenlive-0.7.8-slideshow.png)

These features make it very easy to create something like this video. (Not especially great photos, just a random folder on my system.)

**Center crop** automatically fills the output video frame with the images while maintaining their aspect ratio by cropping equal amounts from a pair of edges. Said another way, it removes the black bars that will appear when the photo orientation or aspect does not match the video's. Speaking of orientation, [j-b-m][2] added EXIF support to auto-rotate our digital photos. Yay!

**Animation** adds preset slow smooth pan and zoom effects also known as the [Ken Burns Effect][3]. The choices are rather limited due to this late feature addition, but you can choose no animation, pans only, zooms only, or a combination of pans and zooms. Each option also has a low pass filter to reduce the noise in the images that may occur during this operation. Low pass filtering is much slower, so you should preview without it, and then enable it to render.


  [2]: https://web.archive.org/web/20160430030359/http://www.kdenlive.org/users/j-b-m
  [3]: https://en.wikipedia.org/wiki/Ken_Burns_Effect
