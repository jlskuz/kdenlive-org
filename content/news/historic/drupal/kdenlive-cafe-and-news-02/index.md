---
title: 'Kdenlive Café and News'
author: Jean-Baptiste Mardelle
date: 2016-06-08T11:09:00+00:00
aliases:
- /node/9466
categories:
- Café
- Event
# Ported from https://web.archive.org/web/20160609142120/https://kdenlive.org/node/9466
---


### Next Kdenlive Café Tomorrow

Our next monthly IRC meeting will be this thursday, the 9th of june at 9pm, Central European Timezone (UTC+1). Feel free to join us on irc.freenode.net, channel #kdenlive.

### Kdenlive development news

In the last weeks, we worked to improve the timeline preview (pre-rendering) feature, and added a few UI improvements, like a progress bar in the Render button, see screenshot.

![](render-progress.png)

We also started implementing a cached data manager. Kdenlive as a video editor can produce large amounts of temporary data. Until now, all these temporary files were saved in $HOME/kdenlive. We are now switching to an XDG compliant scheme, and try to give users more infos about how much data is stored, and allow for easy mangement.

![](cache.png)

Feel free to join us for the next Café!

