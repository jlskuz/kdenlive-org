---
title: Moving Kdenlive's forum
author: Jean-Baptiste Mardelle
date: 2013-07-03T19:33:00+00:00
aliases:
- /node/9161
# Ported from https://web.archive.org/web/20160714024925/https://kdenlive.org/node/9161
---


The Kdenlive forums, that were usually hosted on kdenlive.org have moved!  
Our forums are now hosted on [KDE's forum website][1].

Existing posts have now been migrated to the new forum.

This move was decided because our kdenlive.org server could not handle the load anymore, and also because for a long time we wanted to tighten our links with the KDE community.

This will also help our developers to concentrate on Kdenlive's code, not on website maintenance.  
So for now you can find the following Kdenlive resources on KDE's infrastructure:

* [Forum][1]
* [User documentation][2] on userbase.kde.org
* [Development coordination][3] on community.kde.org

We hope this will bring all users a better experience.

If you had an account on forum.kde.org and on kdenlive.org both registered to the same email address then your posts at kdenlive.org will have been merged into your forum.kde.org account. If you did not have an account on forum.kde.org then one will have been created for you on forum.kde.org with the same user name and email address as you had on forum.kde.org (Unless this user name was already in use at forum.kde.org. In this case a user will have been created with your kdenlive.org user name suffixed with \_drupal). The new accounts created by the forum migration will need a new password before you can send to them. Request this at [https://forum.kde.org/ucp.php?mode=sendpassword][4] .

If anyone got the "\_drupal" suffix and wants to change their username, just send a mail to the admins ([forum-admin@kde.org][5]).


  [1]: https://web.archive.org/web/20160714024925/http://forum.kde.org/viewforum.php?f=262
  [2]: https://userbase.kde.org/Kdenlive
  [3]: https://community.kde.org/Kdenlive
  [4]: https://web.archive.org/web/20160714024925/https://forum.kde.org/ucp.php?mode=sendpassword
  [5]: https://web.archive.org/web/20160714024925/mailto:forum-admin@kde.org
