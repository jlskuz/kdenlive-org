---
title: 'New mailing list kdenlive@kde.org!'
author: Vincent Pinon
date: 2014-10-31T22:57:00+00:00
aliases:
- /node/9186
# Ported from https://web.archive.org/web/20160507132858/https://kdenlive.org/node/9186
---


Our mailing list is also transferred now. For registration or archives consulting, go on to [info page][1].  
Thanks again to KDE Sysadmins for patience and help!


  [1]: https://mail.kde.org/mailman/listinfo/kdenlive
