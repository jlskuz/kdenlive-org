---
title: 'Kdenlive: spring cleaning'
author: Jean-Baptiste Mardelle
date: 2013-06-01T22:57:00+00:00
aliases:
- /users/j-b-m/kdenlive-spring-cleaning
categories:
- Inside
# Ported from https://web.archive.org/web/20160321055859/https://kdenlive.org/users/j-b-m/kdenlive-spring-cleaning
---


Here are some news on what is happening with Kdenlive's video editor. Last year, we launched a successful [IndieGoGo campaign][1] to sponsor Till Theato's work on Kdenlive.

I have finally found time to start merging his great work so that we can clean up Kdenlive and move towards the future.

![](refactor.png)

I will merge my work in the next days in Kdenlive's git master. This means that the master branch will become unusable for editing for a few months until we stabilize everything. So everyone using Kdenlive from git will need to switch to the v0.9 branch if they want to be able to do editing.

This work will bring in some exciting changes, like performance improvements and usability enhancements.  
Since we will also improve the integration of the [MLT framework][2] in the process, we will also benefit from exciting new features like the high quality accelerated [movit][3] filters, and the new spline based animation feature.

I will try to post updates on a regular basis, so that people can also join the development.

_UPDATE: For your information, the refactoring code has now been merged into master. A wiki page to track the progress or contribute has been set up here: https://community.kde.org/Kdenlive/Roadmap_


  [1]: https://web.archive.org/web/20160321055859/http://www.indiegogo.com/kdenlive-re
  [2]: https://www.mltframework.org/
  [3]: https://web.archive.org/web/20160321055859/http://libregraphicsworld.org/blog/entry/introducing-movit-free-library-for-gpu-side-video-processing
