---
title: 'Kdenlive: Café, Sprint and More'
author: Jean-Baptiste Mardelle
date: 2016-01-26T00:50:00+00:00
aliases:
- /node/9456
categories:
- Café
- Event  
# Ported from https://web.archive.org/web/20160413201206/https://kdenlive.org/node/9456
---


So following our poll, the next Kdenlive Café will happen on the following days:

Kdenlive Café #3: Friday, 12th of February, 11am (CET time)  
Kdenlive Café #4: Friday, 25th of March, 11am (CET time)

This will be another occasion for developers and users to meet, on freenode.net #kdenlive channel.  
The other Kdenlive events are a Kdenlive coding sprint this week-end in Lausanne (Switzerland) where our small team will amongst other challenges try to merge the "curves in keyframes" feature.

And for the pleasure, here is a screenshot of Kdenlive's clip monitor where you can see several of the new features that are currently being worked on for the 16.04 release. The monitor looks a bit cluttered like this but it's just for the demo - everything is configurable.

![](kdenlive-16-01.png)

You can see here:

* New audio meter widget
* Overlay of audio waveform at the bottom of the screen
* Safe zone
* On monitor toolbar allowing to zoom, switch fullscreen, seek to next marker, etc

More news will come this week-end from our coding sprint!

