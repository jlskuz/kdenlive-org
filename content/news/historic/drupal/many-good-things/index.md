---
title: 'Kdenlive news for 4 months... many good things!'
author: Vincent Pinon
date: 2015-02-16T07:35:00+00:00
aliases:
- /node/9428
categories:
- Inside
- Sprint
# Ported from https://web.archive.org/web/20160507061501/https://kdenlive.org/node/9428
---


Hello,

Yes, we definitely should blog more, even about small steps... all summed up the progress are not so negligible!

First, let me come back to September: we didn't talk about it here, and it's a shame: we all must sincerely thank Akademy Jury for designating JB for [Application Award][1]! We all were really touched, he can be very proud of it and deserves it for so many years of hard work... and he did rise to the bait, as this raised back his motivation to come help us again! So double, triple, infinite thanks for that prize.

Then, let's continue with chronology, after October and final steps completion in KDE incubation.

In late November, we organized a micro-sprint: JB came to my home over a week-end, where while walking on the Chartreuse trails, we mainly discussed about vision for the near and long-term future of Kdenlive. Sorry for not opening the discussion, we didn't really know in advance what would happen. We organized the tasks on the new [todo board][2]... and decided to start the port to Frameworks 5 right that evening!

![Vincent and JB, November 2014](vincent_jb_nov14.jpg)

In December, after 3 weeks and as a Christmas gift, the port was completely freed from kde4support.  
Without loosing momentum, JB started to work on the discussed refactoring before the new year eve giving the impulse for 2015.

In January, refactoring step 1 out of 3 has progressed well (project clips, then will come monitor and timeline). It is becoming usable for early tests, but small things still need to be cleaned.

Here comes February. As we talked on the list about preparing a Frameworks 5 release for 15.04 (with KDE Applications?), Mario Fux who is mentoring us focused our attention on getting in line for this. We just had our [first IRC meeting][3], very friendly and effective, and already addressed several points raised then.  
  
That lead me to submit our [ideas for SoC][4], as I offered to mentor a 1st student this year! So welcome to you young coders; it's just ideas, let's discuss what you would like to do!

An important point I want to mention here is that most recent informations are now on [KDE wiki][5]. I take this opportunity to warmly thank ttguy for his huge work to complete the Manual, and for the help he provides to users on forum.

There are certainly many points you would like more details, don't hesitate to ask on the list, and maybe we will write more here quickly?!


  [1]: https://dot.kde.org/2014/09/08/akademy-award-winners-2014
  [2]: https://web.archive.org/web/20160507061501/https://todo.kde.org/?controller=board&action=readonly&token=12c577c6fa43a6f57160b0c8e7763920c2e2f0ae058dc7a0fd061815a1ce
  [3]: https://web.archive.org/web/20160507061501/https://notes.kde.org/public/kdenlive-irc-meeting-01
  [4]: https://community.kde.org/GSoC/2015/Ideas#Kdenlive
  [5]: https://community.kde.org/Kdenlive
