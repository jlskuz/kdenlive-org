---
title: 'Cleaning things before next impulse'
author: Vincent Pinon
date: 2014-08-05T17:16:00+00:00
aliases:
- /node/9181
# Ported from https://web.archive.org/web/20160507082205/https://kdenlive.org/node/9181
---


At last it's coming: Randa sprint starts this weekend!

I expect these few days to be oriented towards Kdenlive future (some Frameworks 5 porting, some refactoring, some new features)... So before that I'm trying to leave the present things in the best possible state, preparing a v0.9.10 release.  
I have sorted the source files in the repository, so that it is easier to take his marks for a newcomer (like me few month back!), a new step towards code documentation ;-)  
I fixed most static analyzers warnings, and am still occupied with reproducing and fixing bugs... some old entries are still relevant or contain interesting suggestions!

Even if you are not a developer, you can help in this task by reproducing bugs with recent builds, cleaning obsolete entries... don't hesitate to contact us!

Another important thing is translations; even for old versions, many languages are still incomplete. If you want to get involved, get in touch with your local KDE "l10n" team, it is a good opportunity to help the project in general!

My target is to catch Debian Jessie (5. nov.; for Ubuntu Utopic it's too late)... letting time for packagers & release managers to do their work it means we should deliver everything around mid September!

If you have suggestions in mind for small improvements or big changes, it's also time to send us a comment, so that we can consider it in our discussions.

For sure there will be news soon, with pictures from the Alps!

