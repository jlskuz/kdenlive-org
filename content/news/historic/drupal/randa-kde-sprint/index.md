---
title: 'Randa KDE sprint'
author: Jean-Baptiste Mardelle
date: 2011-05-30T16:32:00+00:00
aliases:
- /users/j-b-m/randa-kde-sprint
categories:
- Randa
- Sprint
- Event
# Ported from https://web.archive.org/web/20150905175422/https://kdenlive.org/users/j-b-m/randa-kde-sprint
---

![](Banner_DekePatrik.png)

Several Kdenlive developers will meet in the next days in Randa ([which will host a big KDE sprint][1]) to exchange ideas and discuss about Kdenlive's evolution. I hope that we will come up with some great ideas, we will keep you informed!

Jean-Baptiste Mardelle


  [1]: https://web.archive.org/web/20150905175422/https://sprints.kde.org/sprint/10
