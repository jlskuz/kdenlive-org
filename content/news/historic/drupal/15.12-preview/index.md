---
title: 'Kdenlive 15.12, what to expect'
author: Jean-Baptiste Mardelle
date: 2015-12-10T20:47:00+00:00
aliases:
- /node/9449
# Ported from https://web.archive.org/web/20160310053328/https://kdenlive.org/node/9449
---


![](kdenlive-15-12.png)

We have been working hard over the past months to improve stability, polish the interface and continue our big code cleanup. Kdenlive 15.12.0 will be released next week, and here are some of the changes you will get with this new version:

## New Features

We managed to add a few nice features. here are a few of them:

* Timeline effects and Bin effects can be temporarily disabled and re-enabled in 2 clicks
* A basic implementation of a copy / paste feature has been added, which allows you to save a part of your timeline and import it later inside another project with all effects and transitions being editable
* Track transparency: a simple click allows you to enable / disable transparency for a given track
* Icons now automatically adapt their color scheme (light or dark) to the UI theme
* More infos on the features should hopefully be published soon...

## General Workflow Improvements

We did several small but nice changes to improve the workflow. Several context menus and buttons have been reviewed and changed to make the most useful features easier to find.  
You can also now drag an effect in the clip monitor to add it to the selected clip. Or define favorite effects that will be available from a quick pull down menu in the main toolbar... just to name a few of the changes.

## Stability

We fixed many crashes and timeline corruption problems, so we hope you will enjoy working with this version.

## Testing

Vincent Pinon started work on [packages][2] for Kdenlive's development version so that we can get more feedback before releasing a version.

## Team and Events

As previously said, the [Randa Meeting][3] really helped the Kdenlive development, and we also hope to organize a small Kdenlive coding sprint in the first months of 2016.  
Mario Fux stepped in to organize a [Kdenlive Café][4] which will be a great opportunity to exchange about the present and future of Kdenlive.

And as usual, several very motivated users really helped us improving Kdenlive by reporting issues, following and testing the fixes, proposing ideas, features and patches on the bugtracker, so a big thanks to them.


  [2]: /node/9447
  [3]: https://dot.kde.org/2015/12/07/randa-meetings-2015-huge-success-again
  [4]: /node/9448
