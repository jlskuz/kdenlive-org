---
date: 2008-10-28
title: Kdenlive 0.7 beta coming soon
author: Jean-Baptiste Mardelle
---

We are getting close to the Kdenlive 0.7 release. Many issues were recently fixed in Kdenlive and some others in MLT. We still have some pending bugs to fix for the 0.7 release, but hope to be able to release in something like ten days.

You can follow the progress on our [roadmap page][1]. Some of the minor bugs will probably be postponed to Kdenlive 0.7.1 so that we can do the release in the first week of November.

  [1]: https://web.archive.org/web/20090106115948/http://www.kdenlive.org/mantis/roadmap_page.php
