---
title: 'Shooting with your DSLR'
author: Simon A. Eugster
date: 2010-11-14T21:27:00+00:00
aliases:
- /users/granjow/shooting-your-dslr
categories:
- Tutorial
# Ported from https://web.archive.org/web/20160412064210/https://kdenlive.org/users/granjow/shooting-your-dslr
---


This article is going to give some tips regarding shooting Video with your DSLR and editing it in kdenlive afterwards.

## Camera hardware

Lens
:   ![](nikon-50mm.preview.jpg) ![](nikon-35mm.preview.jpg)]  
    Generally Primes are preferred over zooms in video. (Some people, like me, prefer it over zooms as well for photography — but this is a matter of taste and of how you work.) Why that? A psychological reason is that eyes (at least mine) cannot zoom either, so zooming is hardly ever used in video. The technical reason is that Primes are cheaper to build whilst offering better quality: Better sharpness, bigger [aperture][3] (for limiting the [Depth of Field][4]). Opening the aperture gives you a very nice look. (Please also read ArtInvent’s comment[^1] on this.)
:   Examples for very popular primes are the [Nikon 50mm f/1.8D][6] and the [Canon 50mm f/1.8 II][7].
:   If you own such a lens, just don’t forget that you should not *always* shoot at f/1.8. ;)

Filter
:   ![](nd-filter.jpg) Directly related to the previous point about lenses. Shooting with an open aperture works well as long as it is dark. In bright sunlight it will fail because there is too much light falling on the sensor. Furthermore you are forced to use a high shutter speed which makes movements look jerky; Most of the time you will want to have some kind of motion blur because it looks more natural to our eyes.
:   If you ever tried to follow a bird or another animal with your eyes in dawn, you will know that our eyes *do* support motion blur.
:   So the trick is to remove some light with a filter called [Neutral Density Filter][9]. You can see one on the right. (The piece of kneaded eraser is not part of the filter.)

## Shooting

Aperture, Shutter, and ISO
:   ![](motion-low.jpg) ![](motion-high.jpg)  
      
    The same as for shooting stills. Really? Not quite. As written above you will usually want to have the shutter speed lower than for photography in order to get motion blur — around 1/50 s. (This is just a rule of thumb, as all rules in video are; made to be broken.)
:   Also, some additional problems may arise due to the sensor being read out line-wise. One I would like to mention are [Rolling Shutter][12] effects. Longer exposure can, but need not, prevent such problems. It *does* if you are shooting with fluorescent lamps. Shooting at high shutter speed shows wave patterns from top to bottom of the screen, lowering it hides them if you hit the correct shutter speed.
:   On the right: Two images I shot with my Nikon D90, the left one at lowest ISO possible, the right one at highest possible.

Exposure
:   ![](low-ISO.jpg) ![](high-ISO.jpg)  
      
    The image should be exposed as bright as possible (without too much clipping!) if enough light is available. If you don’t need to boos the brightness too much in post-production, you can [avoid some noise in dark areas][15].

White Balance
:   ![](whitebalance-post.jpg) ![](whitebalance-pre.jpg)  
    The White Balance should be set as accurate as possible because DSLRs only support 8 bit per color channel (see also my article about the [Waveform Monitor][18]). If done wrong, much of the color information is lost.
:   DSLRs also offer different camera profiles with different Contrast/Saturation/etc. settings. Usually low saturation is preferred over high saturation — especially because raising the saturation can be done in post, and because high in-camera saturation settings can lead to color clipping.
:   In the example images on the right you can see the difference. The left one looked blueish due to wrong white balance and was corrected in post; much of the tonal range of the blue colors has been lost. The right one has been shot with proper white balance.

Autofocus
:   The in-camera autofocus may be fast enough to focus, but it will fail in the most important moment. It is useful for getting the initial focus point, but while shooting it should stay switched off.

## Camera specific tips

### Nikon D90

The Nikon D90 was the first DSLR offering video. 720/24p (AVI container).

D90 videos at 720p are scaled awfully. That’s why you can see stair-stepping in sharp, skew lines. If this becomes perturbing for a clip, you can apply the «Nikon D90 Stairstepping fix» frei0r filter. Written (but not invented) by me :) See our [Nikon D90 page][19] for an example of how stairstepping looks like (before and after correction).

There is an extensive overview over the D90 video function at dvxuser.com: [Understanding and Optimizing the Nikon D90 D-Movie Mode Image][20]

### Canon EOS 550D/Ti2 (and Co.)

These cameras shoot 1080p (H.264 encoded, MOV container) — but record video with a height of 1088 pixels. Prior to MLT 0.5.6 you have/had to crop the additional 8 pixels with a crop effect from the top or the bottom of the video, newer versions of MLT do this automatically.

## DSLR related links

One can find tons of information about shooting in the internet. Some helpful links listed below.

* [DSLR HD Video Tips: Shooting Basics][21] — Introduction to DSLR video shooting
* [Philip Bloom Gives Photographers A Basic Video Shooting Tip][22] — How to get from photo to video
* [7 Tips To Get Better Video from a DSLR Camera][23] — Tips on shooting (not tech only)
* [Hurlbut Visuals Camera Protocol][24] — Professional shooting workflow
* [Pro DSLR Video Tips from David Harry Stewart][25] — Interview containing several tips
* [Tips on Shooting Video With a D.S.L.R.][26] — Various tips
* [How To Guide For Shooting HD Video With A DSLR Camera][27] — Various tips

## Summary

Summary? There is no such thing. You need to read everything, really :)![](switzerland.png)

Have fun!  
Please drop your comments below.

*Simon A. Eugster (Granjow)*

  [^1]: **ArtInvent** It's nice to have a set of primes, sure, but most people still buy a zoom or two for their SLR and tend to use them more often as well. And there's nothing wrong with this, as modern zooms (at least the decent ones) are perfectly good glass. So I'm not sure that telling people they really ought to use primes is all that valid. Actually plenty of pros use zooms for still, film and video work. They just don't zoom during the shot. That's the main point: don't zoom while rolling. And that's not universally true either, a very slow zoom can be usefull and effective once in a while. 

  [3]: https://web.archive.org/web/20160412064210/http://www.digital-photography-school.com/aperture
  [4]: http://www.cambridgeincolour.com/tutorials/depth-of-field.htm
  [5]: /users/granjow/shooting-your-dslr#comment-12183
  [6]: https://web.archive.org/web/20160412064210/http://imaging.nikon.com/products/imaging/lineup/lens/singlefocal/normal/af_50mmf_18d/index.htm
  [7]: https://web.archive.org/web/20160412064210/http://www.canon-europe.com/For_Home/Product_Finder/Cameras/EF_Lenses/Fixed_Focal_Length/EF_50mm_f18II/
  [9]: http://www.dgrin.com/showpost.php?p=1126484&postcount=1/
  [12]: https://en.wikipedia.org/wiki/Rolling_shutter
  [15]: https://prolost.com/blog/2008/6/2/on-clipping-part-1.html
  [18]: /users/granjow/introducing-color-scopes-waveform-and-rgb-parade
  [19]: https://web.archive.org/web/20160412064210/http://kdenlive.org/video-editor/nikon-d90
  [20]: https://web.archive.org/web/20160412064210/http://www.dvxuser.com/V6/showthread.php?146661-Understanding-and-Optimizing-the-Nikon-D90-D-Movie-Mode-Image
  [21]: https://web.archive.org/web/20160412064210/http://video.nationalgeographic.com/video/player/specials/photography-specials/photo-tips/basics-dslr-hd-video-tips.html
  [22]: https://web.archive.org/web/20160412064210/http://photofocus.com/2009/11/10/philip-bloom-gives-photographers-a-basic-video-shooting-tip
  [23]: https://web.archive.org/web/20160412064210/http://www.sportsshooter.com/news/2376
  [24]: https://web.archive.org/web/20160412064210/http://vimeo.com/15635719
  [25]: https://web.archive.org/web/20160412064210/http://www.popphoto.com/video/2010/07/pro-dslr-video-tips-david-harry-stewart
  [26]: https://archive.nytimes.com/gadgetwise.blogs.nytimes.com/2010/04/15/tips-on-shooting-video-with-a-d-s-l-r/
  [27]: https://web.archive.org/web/20160412064210/http://www.reelseo.com/hd-video-dslr-camera/
