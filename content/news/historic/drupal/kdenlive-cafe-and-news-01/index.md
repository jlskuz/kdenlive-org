---
title: 'Kdenlive: Café, release and development'
author: Jean-Baptiste Mardelle
date: 2016-04-19T00:37:00+00:00 
aliases:
- /node/9462
categories:
- Café
- Event
# Ported from https://web.archive.org/web/20160714110744/https://kdenlive.org/
---

In a few days, we are going to celebrate the release of Kdenlive 16.04.0.
If you are interested in the project, you are welcome to join us in the next Kdenlive café, a monthly IRC meeting for users and developers.

Next café will be this wednesday, the 20th of April at 09:00pm (CET time), on irc.freenode.net in channel #kdenlive.

*Ubuntu users can test our latest versions (stable 15.x, testing 16.04 and git master ) in our official PPA: https://launchpad.net/~kdenlive

Also as a note to packagers, an important fix for a crash in the titler was recently committed to MLT's git master. A new version of MLT should be out in the next days.

Feature development continues as well, and an new OpenCV based Motion Tracker filter is currently being developed for a later release. Sample video below for some fun.

{{< video src-webm="deer.webm" muted="true" >}}

Extract from a video by Jeffrey Beach, Beachfront Productions (CC BY 3.0)

Enjoy and hope to meet you on wednesday for the café.

Jean-Baptiste Mardelle
