---
title: 'And the date winners are: Kdenlive Café #1 and #2'
author: Mario Fux
date: 2015-12-14T21:43:00+00:00
aliases:
- /node/9450
categories:
- Café
- Event
# Ported from https://web.archive.org/web/20160413125747/https://kdenlive.org/node/9450
---


So you [decided][1] and I picked ;-). The first Kdenlive Café will happen:  
- on the 22nd of December 2015 at 13.00h CET (UTC+1)

in #kdenlive on freenode.net. Occasional notes will happen in notes.kde.org and shared afterwards here.

The second Kdenlive Café will happen:  
- on the 15th of January 2016 at 11.00h CET (UTC+1)

See you there and happy christmas time everybody.


  [1]: https://web.archive.org/web/20160413125747/http://dudle.inf.tu-dresden.de/lbmbyx5z/
