---
title: 'Light Graffiti the 2nd'
author: Simon A. Eugster
date: 2011-02-22T19:36:00+00:00
aliases:
- /users/granjow/light-graffiti-2nd
categories:
- Showcase
# Ported from https://web.archive.org/web/20160413130944/https://kdenlive.org/users/granjow/light-graffiti-2nd
---


I’ve slightly improved the [Light Graffiti effect][1], especially for different colours painted over each other. Enjoy :)

{{< vimeo 20217266 >}}

[Light Painting 2][2] from [Simon Eugster][3] on [Vimeo][4].


  [1]: /users/granjow/writing-light-graffiti-effect
  [2]: https://vimeo.com/20217266
  [3]: https://vimeo.com/granjow
  [4]: https://vimeo.com/
