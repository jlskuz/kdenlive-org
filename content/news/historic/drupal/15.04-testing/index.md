---
title: 'Kdenlive to be released with KDE Applications 15.04'
author: Vincent Pinon
date: 2015-03-01T19:24:00+00:00
aliases:
- /node/9430
# Ported from https://web.archive.org/web/20160309234030/https://kdenlive.org/node/9430
---


... based on Frameworks 5!

So we met the freeze deadline to get our port to KF5 released with KDE Apps.

What does it change? you have a [changelog][1], but it doesn't explain what's behind.

Being based on KF5 makes Kdenlive future-proof and opens doors for potential new horizons
(platforms, design)...

Being part of KDE family changes things more "socially" than "technically" (quoting tsdgeo ;-)). Changes to the source are now sent for review systematically, releases preparation and delivery will now be handled by experts under a fixed planning, we are now mentored to take part to initiatives like SoC... and exchanges with other devs are now flowing much more naturally ;-)  
I personally feel the difference!

And for you users?

You will have to run a recent distribution offering KF5, this may be problematic at the beginning (you can stick to 0.9.10)...  
Then to reward you if you follow our progression, a few new features to test: ripple delete, stem audio export... Several bugs fixed, maybe some new introduced in the port process :-\

Please test a dev version or a beta (knowing it is a pre-release) and let us know!


  [1]: https://web.archive.org/web/20160303182542/http://quickgit.kde.org/?p=kdenlive.git&a=blob&h=cef2ab5d162c764172adac39eadbb1bad3c741d6&f=ChangeLog&o=plain
