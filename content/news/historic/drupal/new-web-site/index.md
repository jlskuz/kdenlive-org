---
date: 2008-09-30
title: New web site
draft: false
author: Jean-Baptiste Mardelle
---

As you can see, we just updated the Kdenlive web site. Our goal is to have a collaborative web site which allows users to put content on it and helps finding useful information. We also hope that it will bring other people into the project, because help is always welcome!

Several features are planned for this web site:

* Better integration of the forum
* Improve online collaborative documentation
* Allow users to upload tutorials

These features will hopefully be available in the near future, but we are currently busy to get the Kdenlive 0.7 beta release out in time.
