---
title: 'Kdenlive 15.12 knocking on the door... test it easily before release!'
author: Vincent Pinon
date: 2015-12-01T23:48:00+00:00
aliases:
- /node/9447
---


Hello,

We've had much work on 15.8 series, with its 3 monthly bugfix to repair the breaks from the new monitor backend & timeline rewrite... should be acceptable now :)

I won't say much more in this post, my topic here is to announce that application bundles are available again!

That's not all folks: for *ubuntu users, I'm also setting up a [new PPA][2] to test the latest Kdenlive without affecting the rest of your system.

All this is for the moment manually triggered, so maybe not updated very regularly... my next goal is to automate all this so that both get reliable sources.

Enjoy & don't get too angry with remaining bugs :)


  [1]: https://web.archive.org/web/20160413134615/http://files.kde.org/kdenlive/
  [2]: https://web.archive.org/web/20160413134615/https://launchpad.net/~vpinon/+archive/ubuntu/kdenlive-testing
