---
title: 'Welcome to KDE Git, Kdenlive!'
author: Alberto Villa
date: 2011-11-11T15:27:00+00:00
aliases:
- /users/avilla/welcome-kde-git-kdenlive
# Ported from https://web.archive.org/web/20160506053709/https://kdenlive.org/users/avilla/welcome-kde-git-kdenlive
---


[0.8.2 release][1] marked the time to complete our switch to KDE development infrastructure started in [Randa][2] a few months ago. On November 8th our old Subversion repository on SourceForge was discontinued, and we moved on to our new, shiny [KDE Git repository][3]!  
There are at least a couple of differences you - especially developers and translators - should be informed about.

* **Development workflow.** Git offers many features that Subversion doesn't. Thus, we're defining a workflow to guide development which will hopefully improve stability in next releases. We'll have it documented quite soon.
* **Translations.** Kdenlive translations are not stored anymore into its sources: they're now in KDE l10n module. Should you want to work on them, you'll have to get in touch with your [translation team][4]. Also - this is mostly for packagers -, we'll write a script to automatically fetch translations and populate appropriate directories for tarball releasing. Again, our documentation will be properly updated.

As a KDE project, Kdenlive is now open to a wide developer ecosystem. Just as we don't want to loose any active developer ([get a KDE Git account!][5]), we would be happy to welcome new members, occasional contributors and new translators to our team, maybe among those already committed to KDE. The road to Kdenlive is much shorter now, don't play hard to get! ;)


  [1]: /users/j-b-m/kdenlive-082-released
  [2]: /users/granjow/we-re-randa
  [3]: https://web.archive.org/web/20160506053709/http://quickgit.kde.org/?p=kdenlive.git
  [4]: https://i18n.kde.org/teams-list.php
  [5]: https://community.kde.org/Infrastructure/Get_a_Developer_Account
