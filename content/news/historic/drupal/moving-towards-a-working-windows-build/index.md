---
title: Moving towards a working Windows Build
author: Joshua Joseph
date: 2016-06-15T12:55:00+00:00 
aliases:
- /node/9468
categories:
- Randa
- Event
# Ported from https://web.archive.org/web/20160714110744/https://kdenlive.org/
---

This week I have been in Randa, for the annual KDE sprint. Working on porting Kdenlive to Windows is lots of fun and each day we move closer to the Windows release.

We're almost finishing the cross compilation of MLT, after which we will be able to build Kdenlive itself.
