---
title: 'Kdenlive at Randa meetings - first report'
author: Jean-Baptiste Mardelle
date: 2015-09-11T17:48:00+00:00
aliases:
- /node/9445
categories:
- Randa
- Event
- Sprint
# Ported from https://web.archive.org/web/20160413115033/https://kdenlive.org/node/9445
---


![](randa-postit.jpg)

We arrived here on wednesday, and started discussions about our plans in the train. Here is a short resume of what happened during these first days:

We decided to work on bug tracking during the first day, since the Kdenlive 15.08.1 release was imminent. We fixed quite a few bugs that will improve stability of the 15.08 branch.

The second important goal was to take advantage of having so many great people around us to improve Kdenlive. So we had contacts in these domains:

### Mission statement:

We started the work on our "Mission Statement" to better define which users / jobs Kdenlive is designed for. This will help us to focus on important tasks for our target audience. We are in contact with people from the VDG (KDE's Visual Design Group) to get feedback on that process.

### Ui design and workflow:

![](RandaUIreview.jpg)

We had a session with people from the VDG (KDE's Visual Design Group) to review Kdenlive. We also had the chance of having a user of professional video software who had never seen Kdenlive so he was the perfect test subject for our UI review session. We wrote a [wiki page on the ideas that came out of this][1]. We encourage you to visit that page and give us feedback (a few issues were already reported on the bugtracker). As time permits, we will try to implement these ideas to improve the workflow.

### Bugfixing:

There are a few issues/crashes in Kdenlive related to Qt5/KF5 bugs, and we made some contacts to get some help on this.

### Next features:

We discussed some of the features we want to integrate for the 15.12 release, and other long term goals.

The feature we would like to integrate for the december release are: merge the animation feature from GSoC, same track transition (eg. overlapping 2 clips to crossfade), and a way to copy/paste between projects.


  [1]: https://community.kde.org/Kdenlive/UI_Review
