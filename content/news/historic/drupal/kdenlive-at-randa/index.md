---
title: 'Kdenlive at Randa'
author: Till Theato
date: 2014-06-25T12:44:00+00:00
aliases:
- /node/9180
categories:
- Randa
- Event
- Sprint
# Ported from https://web.archive.org/web/20160529230807/https://kdenlive.org/node/9180
---


[Back in 2011][1] among other things the foundations for a renewal of Kdenlive's code were laid in a small village in the Swiss alps. In August again some members of the Kdenlive team will attend the KDE developer sprint in Randa. We want to use the week to push new energy into Kdenlive and to discuss future plans. Major topics will be:  
Further integration of the refactored code. The plans to rework our codebase were first discussed in 2011. In 2012 thanks to your generous donations major parts of the code could be rewritten. However they are still not being used in a released version of Kdenlive since since then developer activity was unfortunately rather low. In Randa we want to work out a plan to continue the efforts step by step and start implementing it.  
Additionally we want to figure out how to employ recent additions to the MLT framework. This includes further work on making the [Movit][2] GPU based filters available.  
Of course there will also be discussions on porting to [KDE Frameworks 5][3] since this is a major topic of the sprint in general.

If you want to support us and the the other attending KDE teams in getting together and pushing your favorite software please donate to the [Randa fundraiser][4].


  [1]: /users/granjow/we-re-randa
  [2]: https://movit.sesse.net/
  [3]: https://dot.kde.org/2013/09/25/frameworks-5
  [4]: https://www.kde.org/fundraisers/randameetings2014/
