---
title: 'Introducing Color Scopes: The Histogram'
author: Simon A. Eugster
date: 2010-08-30T23:10:00+00:00
aliases:
- /users/granjow/introducing-color-scopes-histogram
categories:
- Tutorial
# Ported from https://web.archive.org/web/20160412082711/https://kdenlive.org/users/granjow/introducing-color-scopes-histogram
---


[Russian translation online!][1]  
Translation by Александр Прокудин (Alexandre Prokoudine)

With kdenlive 0.7.8 I added some color scopes, used for displaying color information. In this section I will give a brief overviews over scopes in general and explain the most basic scope, the histogram, in detail.

**Color correction.** This is a really important topic in video editing. It starts with simple stretching of the tonal range if the brightness is not ideal, goes on with white balance to ensure that white remains white and not blue, and finally ends with creating looks which make your video look unique. (Remember the blueish [Minority Report][2]? The contrasty [The Departed][3]?)

For color correction we basically need two things, **Effects** for changing the colors and **Scopes** for monitoring the changes. The first scope I'm showing now is, as already mentioned, the histogram.

## Basic Scope Options

Let’s first take a look at the basic options available in all scopes.

![](kdenlive-scopes-basic-options.png)

* *Auto Refresh* automatically refreshes the scope if the project/clip monitor changes.

  During the process of color correction you’ll want to keep this option enabled. When not color correcting, it should be disabled as it usually heavily impacts the performance of playback. (There is a lot of calculations going on in the scopes.)
* *Realtime* tries to maintain a certain frame rate in the scopes by dropping part of the color information received (e.g. taking a look at every 8th pixel only instead of every single pixel).

Note that you can always update a scope by clicking on it.

So far about scopes in general. Now let’s take a closer look at the Histogram.

## How the Histogram works

When the Histogram receives an updated image from one of the monitors, each of these pixels consist of a Red, Green, and Blue component. Each of these values lies within a range of 0 and 255, which are the numbers you can represent with one Byte. 0 means that the component is not shining at all (i.e. it is black), 255 means that it is shining as bright as possible.

The Histogram is merely statistics; it shows how often a component of a certain brightness occurs. So what the Histogram then does is actually quite simple:

1. Take the first pixel
2. Look at the Red value (= x) of the pixel. Increase the height of the bar at position x of the histogram by 1.

   Example: If the red value is 0, increase the height of the bar at position 0 (that is at the very left) of the histogram by 1. If it is 42, increase bar 42 by 1. And so on.
3. Repeat the previous step with Green and Blue.
4. Look at R, G, and B together and calculate the Luma value. Luma is the perceived Luminance of this pixel. See further below how it is calculated (if you are interested).
5. Repeat these steps for all other pixels on the image.

## What the Histogram shows

The Histogram *only shows the distribution of the luminance* of the selected components – nothing more, nothing less. Also when looking at the RGB channels separately, instead of at the calculated Luma component only. You cannot really guess the colors in the image.

Really? Yes. Take a look at these two images.

![](kdenlive-colorscopes-histogram-gradient-bw.png)

![](kdenlive-colorscopes-histogram-gradient-color.png)

Exactly the same Histogram. Totally different colors. (What you *can* do is guessing the color tone; see below.) But what is the histogram good for now?

To answer this question, I would like to point an article from the «Cambridge in Colour»: [Understanding Digital Camera Histograms: Tones and Contrast][7] and the second part [Luminance & Color][8]. Although written for digital photo cameras, exactly the same applies for digital video cameras. Both articles are easy to read and understand (and may also be of interest for experienced users).

### Histogram example: Candlelight

![](kdenlive-colorscopes-histogram-candlelight.png)]

Two special things about this histogram.

* Most pixels are dark, according to the Luma component (white). Though there is no total black: Notice that the Luma component shows «min: 8».

  Nevertheless, the blue component does reach 0. This means that the darkest pixels are still slightly orange and didn’t lose all color information yet.
* There is quite some clipping. A lot of R values are sticking at the very right, at 255. Having a peak at 255 usually means that we lost information because some regions were too bright for the camera sensor with the current sensitivity settings. This could have been solved by lowering the sensitivity, but then the book and nearly everything else would be black.

  In this case the candles cause the clipping. (Not too bad here, because the lost detail isn’t important for the image.)

The RGB components also show very well that the shadows are not neutral grey but orange, otherwise the color heaps on the left would, as in the gradient histogram above, have their center at the same position. There isn’t a lot to correct here, what could be done is raising the shadows with a Curves effect, but this is a matter of taste and the intended mood for the final movie.

![](kdenlive-colorscopes-histogram-candlelight-shadows_raised.png)

### Histogram example: Underexposed ABC

![](kdenlive-colorscopes-histogram-abc.png)

We immediately notice two things:

* The RGB peaks are at the same position, near the middle. The white wall is the brightest part, so this peaks are from the white wall. As they are not shifted, the white balance should be okay (the image confirms that).

  Note that the Histogram is not very accurate for white balance. Later I will introduce a much more accurate scope.
* The image is too dark. The brightest component, red, only reaches a value of 170. The white wall is actually grey.

Monitoring correct exposure is the Histogram’s strength! The exposure can be corrected with curves as well, but this time I will use the *Levels* effect.

![](kdenlive-colorscopes-histogram-abc-exposure_corrected.png)

I’ve lowered the *Input white level* of the Luma channel until one of the RGB components reached 255. Lowering the input white level further would cause clipping on the wall and lost image information. (Which may be desired in certain circumstances!)

This process is called *Stretching* of the tonal range.

## Histogram options

In kdenlive 0.7.8 the histogram can be adjusted as follows:

* *Components* – They can be enabled individually. For example, you might only want to see the Luma component, or you want to hide the Sum display.
  + *Y* or *Luma* is the best known Histogram. Every digital camera shows it, [digikam][13], [GIMP][14], etc. know it. See below how it is calculated.
  + *Sum* is basically a quick overview over the individual RGB channels. If it shows e.g. 5 as the minimum value, you know that none of the RGB components goes lower than 5.
  + *RGB* show the Histogram for the individual channels.
* *Unscaled* (Context menu) – Does not scale the width of the histogram (unless the widget size is smaller). Just a goodie if you want to have it 256 px wide.
* *Luma mode* (Context menu) – This option defines how the [Luma value][15] of a pixel is calculated. Two options are available:
  + Rec. 601 uses the formula `Y' = 0.299 R' + 0.587 G' + 0.114 B'`
  + Rec. 709 uses `Y' = 0.2126 R' + 0.7152 G' + 0.0722 B'`

  Most of the time you will want to use Rec. 709 which is, as far as I know, mostly used in digital video today.

## Sample files

The sample files used above can be downloaded here:

* [Histogram-bw.png][16]
* [Histogram-col.png][17]
* [abc-underexposed.avi][18] (26 MB; 720/24p)
* [candlelight.avi][19] (14 MB; 720/24p)

## Summary

The Histogram is a great tool for exposure correction, together with the *Curves* and the *Levels* effects. It helps to avoid clipping (burned out areas) and crushed blacks (the opposite) when applying effects.![](switzerland.png)

Thanks for reading! Continue with [the Waveform and the RGB Parade][20].  
Please drop your comments below.

*Simon A. Eugster (Granjow)*

[![](/images/banners/cc-by-sa-88x31.png)][21]


  [1]: https://web.archive.org/web/20160412082711/http://linuxvideo.ru/articles.php?article_id=3
  [2]: https://en.wikipedia.org/wiki/File:Minority_Report_bleached.jpg
  [3]: https://thedeparted.warnerbros.com/
  [7]: https://www.cambridgeincolour.com/tutorials/histograms1.htm
  [8]: https://www.cambridgeincolour.com/tutorials/histograms2.htm
  [13]: https://www.digikam.org/
  [14]: https://www.gimp.org/
  [15]: https://en.wikipedia.org/wiki/Luma_(video)
  [16]: http://granjow.net/uploads/kdenlive/samples/Histogram-bw.png
  [17]: http://granjow.net/uploads/kdenlive/samples/Histogram-col.png
  [18]: http://granjow.net/uploads/kdenlive/samples/abc-underexposed.avi
  [19]: http://granjow.net/uploads/kdenlive/samples/candlelight.avi
  [20]: /users/granjow/introducing-color-scopes-waveform-and-rgb-parade
  [21]: https://creativecommons.org/licenses/by-sa/3.0
