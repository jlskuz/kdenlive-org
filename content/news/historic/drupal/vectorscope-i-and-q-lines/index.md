---
title: 'Vectorscope: What the I and the Q lines are good for'
author: Simon A. Eugster
date: 2010-11-26T18:05:00+00:00
aliases:
- /users/granjow/vectorscope-what-i-and-q-lines-are-good
categories:
- Tutorial
# Ported from https://web.archive.org/web/20160324111308/https://kdenlive.org/users/granjow/vectorscope-what-i-and-q-lines-are-good
---


In the next kdenlive version (or in the current SVN version, if you dare compile it yourself :)) you will find a new option for the vectorscope: To draw I/Q lines. What are they good for?

![](vectorscope-iq-lines.png)

## Where I/Q lines come from

You may remember from my [blog post about the Vectorscope][2] that the Vectorscope uses a color space different than RGB. In the image above it is [YUV][3], in the image below it is [YPbPr][4]. They both share the property that the Y component represents Luma only (i.e. how bright a pixel is), and the other two components represent Chroma (colour) by expressing deviations from neutral color on the red-green and yellow-blue axis. (These are complementary colours each, so mixing them in equal parts results in neutral again – which is why they can be used for the deviation.)

YUV is the standard color space for analog PAL television. NTSC, the american analog TV standard, uses a color space I did not mention yet: [YIQ][5]. The special thing about this color space is that the I component was chosen such that skin tones (also known as flesh tones) lie on the I line (orange-blue), and it was given more than four times as much bandwidth as the Q component (which represents the green-purple line; The human eye is also less sensitive for changes on this line).

![](vectorscope-iq-lines-skin.png)

## The Purpose of the I and the Q line

You might have guessed it already: The reason for displaying the Q and, especially, the I line is to help with skin tones. There is a rule of thumb in post production saying that all skin tones sould approximately lie on the I line. If it is not, you might want to [color-correct your clip][7].

Why? If skin tones do not lie on the I line, they are likely to look unnatural. Our eye is trained on skin tones ;) End of the story.

## Clip sources

Only one this time.

* [skin1.avi (720p, 5.1 MB)][8]

That's it! Thanks for reading.  
Feel free to post your comments below.

*Simon A. Eugster (Granjow)*![](switzerland.png)


  [2]: https://web.archive.org/web/20160324111308/http://kdenlive.org/users/granjow/introducing-color-scopes-vectorscope
  [3]: https://en.wikipedia.org/wiki/YUV
  [4]: https://en.wikipedia.org/wiki/YCbCr
  [5]: https://en.wikipedia.org/wiki/YIQ
  [7]: https://prolost.com/blog/2008/3/23/save-our-skins.html
  [8]: http://granjow.net/uploads/kdenlive/samples/skin1.avi
