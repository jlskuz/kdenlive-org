---
title: Getting ready for Kdenlive 0.8.2
author: Jean-Baptiste Mardelle
date: 2011-07-29T12:37:00+00:00
aliases:
- /users/j-b-m/getting-ready-kdenlive-082
---

![](kdenlive_081.png)

The release of Kdenlive 0.8.2 was slighlty delayed due to the discovery of a blocking issue that required last minute changes in MLT and Kdenlive.

I want to make sure that those changes don't cause regressions, so the idea is to ask users to test the latest Kdenlive development version (available for Ubuntu users on [sunab's experimental repository][2]). I am especially concerned about problems occuring when opening old Kdenlive project files.

Once we have some feedback and manage to fix a few remaining issues, we will release Kdenlive 0.8.2, which should hopefully happen around the 13th of august.

We are now in string freeze, so if you are interested to help translating Kdenlive in your language, [get involved][3].

More informations about the changes in the soon to be released 0.8.2 version can be found on the [Kdenlive 0.8.2 information page][4].

After that release, we will be moving to KDE's infrastructure to be part of KDE multimedia, and work on the refactoring branch to clean up the code and make it easier to understand / maintain / contribute.

For the Kdenlive team, Jean-Baptiste Mardelle

  [2]: https://launchpad.net/~sunab/+archive/kdenlive-svn
  [3]: https://www.kdenlive.org/contribution-manual/how-translate-kdenlive
  [4]: /discover/0.8.2
