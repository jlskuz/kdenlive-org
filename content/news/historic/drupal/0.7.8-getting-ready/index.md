---
title: Getting ready for Kdenlive 0.7.8 
author: Jean-Baptiste Mardelle
date: 2010-08-28T18:38:00+00:00
aliases:
- /users/j-b-m/getting-ready-kdenlive-078 
---

![](kdenlive-078.png)

We are now preparing the Kdenlive 0.7.8 release which should be ready around the 13th of september 2010.

A lot of issues were fixed and several new features were added. You can see the list of issues in our [0.7.8 presentation page][1]. In the next days, we will blog about the new features, so that you can discover what is waiting for you on the release day!

Everyone running the svn development version is encouraged to test Kdenlive and report issues on [our bugtracker][2] so that we fix a maximum of bugs for this release.

For the Kdenlive team,  
Jean-Baptiste Mardelle

 [1]: /discover/0.7.8
 [2]: https://kdenlive.org/mantis
