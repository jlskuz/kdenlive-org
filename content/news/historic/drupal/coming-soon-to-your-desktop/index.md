---
title: 'Coming soon to your Desktop...'
author: Jean-Baptiste Mardelle
date: 2010-10-09T03:37:00+00:00
aliases:
- /users/j-b-m/coming-soon-your-desktop
# Ported from https://web.archive.org/web/20160323093154/https://kdenlive.org/users/j-b-m/coming-soon-your-desktop
---


![](stopmotion.jpeg)

Just wanted to post a screenshot of Kdenlive's current development version that includes a great new feature: **a Stop motion utility**.  
The only drawback is that this feature currently depends on a specific hardware to capture from HDMI. So how does it work?

It's very easy: just plug your camcorder or digital camera into the HDMI port of a Linux compatible capture card, and you get a live preview of the camcorder.  
Click on the capture button to grab an image, you can also transparently overlay the last captured frame on the monitor to easily see the difference with current live feed.

So capture your frames, preview them with the built in preview feature and then click on the "Add sequence" button to add your animated stopmotion sequence to your project.

This is still a work in progress, but starts to be useful, and I must say that HDMI capture looks great!  
Currently, it is limited to still frames, but video capture should follow.

The new stopmotion utility is in the "Project" menu, under "Stopmotion animation".
