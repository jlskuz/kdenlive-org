---
title: 'Kdenlive news'
author: Jean-Baptiste Mardelle
date: 2016-05-09T23:45:00+00:00
aliases:
- /node/9464
categories:
- Café
- Event
# Ported from https://web.archive.org/web/20160511124107/https://kdenlive.org/node/9464
---


## Next Kdenlive Café

Our next monthly IRC meeting will be this wednesday, 11th of May at 9.00pm (CET).  
Feel free to join us on irc.freenode.net, channel #kdenlive.

## Releases

Kdenlive 16.04.1 should be released tomorrow with [several bugfixes][1].  
MLT (the video framework behind Kdenlive) [version 6.2.0][2] was also released recently, and we urge packagers to update to this new version since it fixes the longstanding titler crash issue in Kdenlive. This new MLT version also adds support for FFmpeg's libavfilter, which means that in the near future, Kdenlive should be able to use some of FFmpeg's filters.

## Development

Development on the master branch (which will become Kdenlive 16.08) continues, and we just finished implementing several editing improvements, enabling a much faster keyboard workflow. Native support for Krita images was also added.

![](kdenlive16041.png "Snapshot of the current development version")

A rather busy month as you can see, and we hope to continue expanding the great community around Kdenlive, so feel free to join us on wednesday for the café!


  [1]: /discover/16.04.1
  [2]: https://www.mltframework.org/blog/v6.2.0_released/
