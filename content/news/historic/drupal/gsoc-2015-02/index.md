---
title: 'GSoC: [Kdenlive] Animated Keyframe widget'
author: Gurjot Singh Bhatti
date: 2015-06-30T14:15:00+00:00
aliases:
- /node/9439
categories:
- GSoC
- Mentorship
# Ported from https://web.archive.org/web/20150905070223/https://kdenlive.org/node/9439
---


![](attach-ss.png)

After Dan Dennedy implemented Mlt::Animation API for use, I've made a separate widget for the new Animation Keyframes.

Now I've tested this for Volume Effect in Kdenlive with 'level' property set to the value of "0=0.5;100|=1;200~=0.5", where (|) represents a discrete keyframes, (=) represents linear interpolated keyframe and (~) represents smooth spline keyframes.

We have got a animated-keyframe-widget, where we can see the keyframes, add or remove them and even edit the values of existing ones.

The type of keyframe editing is in progress as well after that it will be display these keyframes on the clip on timeline. And to be able to edit them directly from the track, atleast the positions.
