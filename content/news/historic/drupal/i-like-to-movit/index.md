---
title: 'I like to Movit!'
author: Vincent Pinon
date: 2014-03-28T22:00:00+00:00
aliases:
- /node/9177
categories:
- Inside
# Ported from https://web.archive.org/web/20160507061922/https://kdenlive.org/node/9177
---


Dear Kdenlive users,

No news for one year, let's admit it was a low time for the project. But that's over: we are back, with renewed energy and cool new stuff to play with!

First of all, let's talk about the long waited GPU-accelerated effects, from [Movit library][1].  
Integrated for months in MLT, we had to activate GLSL backend in kdenlive to support this feature. Job done by Sesse (movit author) while preparing his brilliant [demo at FOSDEM][2].

Another task was to update Shuttle editing devices handling, due to new behavior in many distributions. EdRog made this work again, in a nicer way than before (thanks to mediactrl library from [Dan Dennedy][3]).

And as usual, we tried to fix the bugs you reported (JBM, Montel and I),  
thanks again to the adventurous users of development version... and sorry when we broke everything without warning :-/  
Should be better now, but remind that release version is always safer for urgent work!

And then, what's next?

Code cleaning (not so exciting for you? well, if it helps to avoid bugs and get a faster and more stable program...),  
preparing for Qt 5 & [KDE Framework 5][4] (could help for portability, welcome back poor proprietary OS slaves ;-)),  
integrating refactored code (your generous donations didn't go into the void!).  
Nothing strongly planned for now, it will depend on the moods (and free time)...

As usual, don't hesitate to come help us, contributing to code, documentation, forums, communication, whatever you like!

Thanks for your support!


  [1]: https://movit.sesse.net/
  [2]: https://www.youtube.com/watch?v=IYWKyqyA8Rw
  [3]: https://github.com/ddennedy
  [4]: https://dot.kde.org/2014/01/07/frameworks-5-tech-preview
