---
title: 'Audio effects improvements in version 0.7.8'
author: Dan Dennedy
date: 2010-09-09T07:24:00+00:00
aliases:
- /users/ddennedy/audio-effects-improvements-version-078
# Ported from https://web.archive.org/web/20150905140633/https://kdenlive.org/users/ddennedy/audio-effects-improvements-version-078
---

We added some basic audio channel manipulations. We have had a **Mono to stereo**, which basically copied the left channel to the right. This was expanded to allow copying right-to-left. In addition, now you can swap the left and right channels to correct stereo inversion using **Swap channels**.

![](kdenlive-0.7.8-channelcopy.png)


Next, we added **Balance** and **Pan** to affect the stereo "image." Both of these are keyframe-able. The difference is that **Balance** adjusts the apparent center, and **Pan** adjusts the left-right "position" of an individual channel.

![](kdenlive-0.7.10-pan.png)

Lastly, we fixed some of the SoX-based effects and removed some that were not fixable because either SoX removed them (Vibro) or they do not integrate well (Reverb and Pitch Shift) because they have time-related side effects.
