---
title: 'Kdenlive in Randa'
author: Jean-Baptiste Mardelle
date: 2016-06-14T08:32:00+00:00
aliases:
- /node/9467
categories:
- Event
- Randa
- Sprint
# Ported from https://web.archive.org/web/20160615125603/https://kdenlive.org/node/9467
---


On Thursday evening, I will join the Randa Meeting, which gathers around 40 KDE developers. This will allow me to spend some dedicated time on Kdenlive and exchange with the development community. I hope to spend some time with Joseph Joshua who is working on Kdenlive's Windows port and it will also be a great occasion to look into Flatpak, a Linux cross distro packaging system.

I will keep you updated when I will be there!

If you want to support us, please [donate][1] to help us keeping up these developer meetings!

[![](images/banners/fundraising2016.png)][1]


  [1]: https://web.archive.org/web/20160615125603/https://www.kde.org/fundraisers/randameetings2016/
