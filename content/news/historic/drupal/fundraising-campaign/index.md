---
title: 'Kdenlive fundraising campaign'
author: Till Theato
date: 2012-03-15T23:07:00+00:00
aliases:
- /users/ttill/kdenlive-fundraising-campaign
categories:
- Fundraising
# Ported from https://web.archive.org/web/20160318001247/https://kdenlive.org/users/ttill/kdenlive-fundraising-campaign
---


With an ever growing code base Kdenlive has become harder and harder to maintain and to develop further. To drastically improve this situation we worked out a plan to refactor Kdenlive's code at the [KDE developer sprint in Randa][1] last summer. However since we are all short of time you haven't seen any of promised work done yet.

But now you have the possibility to help. I will be able to work on Kdenlive full time in May and June. To do so however I need an income. We therefore created a funding campaign on IndieGoGo. While you might not be interested in the code improvements itself sure you are in the resulting benefits: When the code is easy to maintain less bugs occur. Developers have more time to work on new features. It's easier to attract new developers which accelerates progress.

So go ahead and donate to our [Kdenlive fundraising campaign][2]

Btw: The next release of Kdenlive will feature automatic audio alignment. This makes working with a separate audio recorder or a multi-cam setup just so much easier!


  [1]: /users/granjow/we-re-randa
  [2]: https://web.archive.org/web/20160318001247/http://www.indiegogo.com/kdenlive-re
