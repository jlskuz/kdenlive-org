---
title: 'Randa meeting 2014'
author: Simon A. Eugster
date: 2014-08-13T21:26:00+00:00
aliases:
- /node/9182
categories:
- Randa
- Sprint
- Event
# Ported from https://web.archive.org/web/20160324002217/https://kdenlive.org/node/9182
---


As you have read before, Vincent, Till, and me have met again this year at the [Randa Sprint][1] in the Swiss mountains. We have worked on Kdenlive and discussed + partly planned its future.

![](randa-2014.jpeg "“Somehow the trigger does not work anymore!” Left to right: Till, Simon, Vincent.")

The Randa meeting is always very valuable. This is the first time we met Vincent in person, and he is a very kind guy. Face-to-face discussions allowed us to discuss much more efficiently than via mailing list, too.

In the past days, Vincent has been fixing bugs, and Till has done refactoring work. Together we have outlined the **[roadmap][2]**. The long-term goal is to have the Kdenlive code completely refactored. In 2012, Till did 2 months of refactoring work after a very successful Indiegogo campaign; time was too short to integrate all of it into Kdenlive, so a lot remains to be done. To give developers an easier entrance, we have drawn the proposed architecture from the refactoring branch.

![](ArchitectureModel.png)

Originally, we wanted to port all features to the refactoring branch. This was close to a re-write. Unfortunately, we underestimated the task, and motivation to port features to get the branch at least usable again soon vanished. The plan has therefore changed. The *master* branch continues to receive bugfixes. On the *next* branch, classes are broken down first (there are multiples with 1000 to 7000 lines) and then step by step refactored according to the proposed architecture shown in the SVG and in the refactoring branch’s code. That way we can avoid master and refactoring to diverge—meaning work on new features would need to be duplicated in both master and refactoring—and still keep all features, progressing towards a Kdenlive that is maintainable and ready for the future again.

Everything fine? Well, not quite … refactoring is really crucial *and* a huge task. The team that has been to [Randa 3 years ago][4] does not have time for development anymore, for various reasons. Vincent cannot do it all alone. So, Kdenlive *needs* developers able and willing to spend some weeks on completing the refactoring. We will consider applying to Google Summer of Code. Another option is to start another crowdfunding campaign to pay a developer—if we find one with the same vision and the motivation to do it.

We also plan to become a KDE project, which would include migrating the bug tracker to Bugzilla, using their continuous integration system, and perhaps migrating the web site to a KDE server. These points are still subject to discussion, though.

That’s it for now.  
—Simon


  [1]: https://randa-meetings.ch/
  [2]: https://community.kde.org/Kdenlive/Roadmap
  [4]: /users/granjow/we-re-randa
