---
title: 'Work in progress'
author: Vincent Pinon
date: 2014-10-28T22:54:00+00:00
aliases:
- /node/9185
# Ported from https://web.archive.org/web/20160507070057/https://kdenlive.org/node/9185
---


So what's happening since last release one month back? If you look only at source code evolution, you may think I already entered hibernation... not completely!

I've been following the packaging progress of the new release, to be sure I didn't miss something as for my first time (re-shipped UI translations and docs; and there is a new MLT dependency due to stabilizer change).  
I'm also keeping an attentive eye on bug tracker and forums, to be sure nothing is breaking in users hands due to small changes introduced. Some very active Kdenlive fans already do a wonderful job, sometimes I feel I can add something, knowing the program's internals and backends.

In parallel, I'm trying to make some progress in migration to KDE project...  
For what purpose? With better words than mine, the [KDE manifesto][1] explains the advantages of being part of such a wide community. Important aspects, regarding Kdenlive history, is long-term access to all the data, and mutualizing efforts not only for coding but also for services hosting (homepage, forum, mailing list, bug tracker, build bot), and for documentation and translation teams, etc.  
The process is going on, with most data transferred to KDE servers, and completing the [incubation steps][2] one by one.

Last but not least, I'm spending lot of time in the code... but not writing a single line!  
As discussed several times, the first step towards Kdenlive evolutions is code reordering/cleaning/refactoring. So I am studying the existing code (big parts I've never read entirely, only jumping at lines pointed by backtraces or analyzers...) and the unfinished refactored code from previous years. I'm taking notes, considering how to join both ends, and not far from starting to put my mess!  
If you have advice on methods or tools for this step, please share!


  [1]: https://manifesto.kde.org/
  [2]: https://community.kde.org/Incubator/Projects/Kdenlive
