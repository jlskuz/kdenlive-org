---
title: 'Preparing for Kdenlive 15.08'
author: Jean-Baptiste Mardelle
date: 2015-07-20T01:02:00+00:00
aliases:
- /node/9440
categories:
- Akademy
# Ported from https://web.archive.org/web/20160713230432/https://kdenlive.org/node/9440
---


![](Banner2015.going.png)

Kdenlive 15.08 will be released with [KDE Applications 15.08][1]. Beta version should be released next week.

This version is the result of a hard refactoring work to make Kdenlive's code cleaner and easier to maintain. While the refactoring effort is not finished and a lot of cleanup if still required, I am proud of the result that finally integrates some of the work from [Till Theato][2] and ideas gathered in our [brainstorming][3] from last year.

Of course, there will probably be a few regressions, but I am confident that they can be solved quickly. We will try to post some videos and blogs about the new features that you can expect in this release, [Akademy 2015][4] will probably be a good time for that!

Hope to see some of you in Spain, and best regards from the Kdenlive team.


  [1]: https://community.kde.org/Schedules/Applications/15.08_Release_Schedule
  [2]: /users/ttill/kdenlive-fundraising-campaign
  [3]: /node/9428
  [4]: https://akademy.kde.org/2015
