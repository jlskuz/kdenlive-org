---
title: 'GSoC 2015: Kdenlive'
author: Gurjot Singh Bhatti
date: 2015-06-08T09:41:00+00:00
aliases:
- /node/9438
categories:
- GSoC
- Mentorship
# Ported from https://web.archive.org/web/20150905063049/https://kdenlive.org/node/9438
---


This is first post regarding GSoC project: "Add support for new Animation capabilities".

Right now, Kdenlive has the keyframe animation which only support linear interpolation which does not provide the flexibility to the user to exploit the robustness of animation.

And this project aims to upgrade the animation capabilities to allow much simpler, smoother and more general animations than the traditional keyframes technology and also intend to provide new widgets to edit these properties, and eventually evolve on-monitor interactions.

After discussion during Community Bonding period and a bit slow start (health issues), I'm now picking up the pace.

The first step is to upgrade Kdenlive to use newer Animation API of MLT Framework which is more powerful and provides all forms of interpolation:

1. Discrete  
2. Linear  
3. Smooth Catmull-Rom spline  
4. Mix of the above three.

More updates soon!

