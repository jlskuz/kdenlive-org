---
date: 2002-09-15
title: Timeline Progress
draft: false
#layout: release-notes
author: Jason Wood
---

OK, I have some free time again now. Progress on the timeline continues, it is now possible to zoom in/out! Fixed up timing issues with the timeline, there are a couple of outstanding issues with moving things about on the timeline that need to be addressed, and a nasty bug which crashes Kdenlive on exit which I haven't started tracking down yet. If all goes well (I know, I know, I said these exact words last month) then I should have something up and running by the end of next week. No promises this time though!
