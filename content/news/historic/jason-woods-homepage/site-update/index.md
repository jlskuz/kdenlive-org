---
date: 2002-05-01
title: Site update
draft: false
#layout: release-notes
author: Jason Wood
---

I have added a number of documents which some may find interesting on the status of the project. I will keep this up to date whenever something happens. Finally, I have added a mailing list for people interested in kdenlive development.
