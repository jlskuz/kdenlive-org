---
date: 2003-02-15
title: CVS Kdenlive/Piave now work together again
draft: false
#layout: release-notes
author: Jason Wood
---

Recently, the CVS versions of kdenlive and piave have been pretty much useless due to major incompatabilities and developments. These have now been fixed somewhat, so you can now check out the development version to see how much progress is being made.
