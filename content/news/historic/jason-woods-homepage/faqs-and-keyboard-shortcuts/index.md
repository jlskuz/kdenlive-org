---
date: 2003-08-10
title: FAQ's and keyboard shortcuts
draft: false
#layout: release-notes
author: Jason Wood
---

A new Frequently Asked Questions page has been added to the site. For the moment, there are three questions on there, but I shall be expanding this as and when people ask me questions more frequently :-) This page will not become one of those "witty" FAQ pages with questions made up by the author - only those questions that I get asked a lot will make it to the page.

It turned out that adding configurable keyboard shortcuts and configurable toolbars was much easier than I had anticipated - so instead of waiting for Kdenlive 0.3.0, it is not implemented and will make it into Kdenlive 0.2.3!
