---
date: 2003-07-21
title: Preparing for 0.2.3
draft: false
#layout: release-notes
author: Jason Wood
---

We are hoping to release a new version of kdenlive and piave in the next couple of weeks. This will not be the fabled 0.3.0 release, but 0.2.3, and will include all of the features currently working in cvs, including inpoints/outpoints and workspace/clip monitors, two important points missing from Kdenlive 0.2

Please please please file bug reports on the sourceforge bug tracker, as this is the easiest way for me to track which bugs are still outstanding and need to be dealt with. I am hoping to reduce the number of bugs on the tracker to zero by the 0.2.3 release :-)
