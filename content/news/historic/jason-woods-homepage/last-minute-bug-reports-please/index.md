---
date: 2003-10-08
title: Last minute bug reports, please!
draft: false
#layout: release-notes
author: Jason Wood
---

Well, although it's been almost two months since I was hoping to release Kdenlive/piave 0.2.3, we are finally almost almost ready for release. (Honest, this time). There were some significant memory leaks in piave that took some time to track down and fix, but everything seems to be working.

But before go about releasing the new version, I would ask that those who are capable download the cvs version, try it and report any issues you have. If nothing turns up, I will package Kdenlive up in two or three days time.

Thank you for your patience!
