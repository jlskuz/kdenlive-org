---
date: 2003-08-25
title: 1 week to 0.2.3 (hopefully!!!)
draft: false
#layout: release-notes
author: Jason Wood
---

We are almost ready to release Kdenlive/Piave 0.2.3 to the unsuspecting public. Gilles is finishing off the French translation this week, and except for the odd bug fix, everything is ready.
