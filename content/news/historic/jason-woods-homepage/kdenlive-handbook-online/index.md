---
date: 2003-09-17
title: Kdenlive Handbook Online!
draft: false
#layout: release-notes
author: Jason Wood
---

You can now view both the English and French translations of the Kdenlive handbook on the [Documentation](https://web.archive.org/web/20070622151723/http://www.uchian.pwp.blueyonder.co.uk/kdenlive_documentation.html) page. Rejoice!
