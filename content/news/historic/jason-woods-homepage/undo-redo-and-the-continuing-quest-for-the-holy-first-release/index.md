---
date: 2002-12-20
title: Integration underway
draft: false
#layout: release-notes
author: Jason Wood
---

Kdenlive has now gained multiple undo/redo functionality, and load/save support! If that's not enough for you, Piave now uses YUV overlays for all video output, and is threaded for lower latency. In fact, apart from some minor bugs that need to be sorted out, we should have a first source release out soon.

Of course, I've said that before ;-)
