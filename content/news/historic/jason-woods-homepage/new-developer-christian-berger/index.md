---
date: 2002-04-28
title: "New developer : Christian Berger"
draft: false
#layout: release-notes
author: Jason Wood
---

Christian Berger is the first developer to join kdenlive, and so we can now officially be called the kdenlive team! Christian wrote the deinterlacing effect for EffecTV. Hopefully development will go twice as fast now :-)
