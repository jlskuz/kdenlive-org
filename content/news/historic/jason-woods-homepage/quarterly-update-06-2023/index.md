---
date: 2003-06-09
title: Quarterly Update...
draft: false
#layout: release-notes
author: Jason Wood
---

Well, no I hope this doesn't become a regular thing, but I realise that I haven't udated the site for quite some time. Things are still ticking along at a sedate pace, an hour's coding here or there from myself, Rolf as been similarly lacking in time, tied up by his thesis, and the testers, well they're waiting for us to do some more stuff for them to test!

Kdenlive could really do with your help! If you like what you have seen so far and feel you'd like to help develop then please get in touch and let me know!

Anyway, the main plan of action is still to get the Gui up to scratch, I am currently working on making snap-to-grid functionality generic amongst all tools, which will aid development greatly. Once done, I will release Kdenlive 0.2.2, something that should have been done a while ago, except for lack of time. This will sync up with the piave of the same version number, and hopefully get rid of the weekly email talking about the abs() error in Kdenlive 0.2.0 :-)

Once the gui is sorted, I shall be moving on to add a few extra file formats to Piave, and making everything work with everything else.
