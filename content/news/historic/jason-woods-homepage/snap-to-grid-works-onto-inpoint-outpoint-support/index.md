---
date: 2003-07-21
title: Snap to grid works!, Onto inpoint/outpoint support
draft: false
#layout: release-notes
author: Jason Wood
---

As it says - I finally found the time to finish refactoring the snap to grid code. Now in cvs. Most of the results of this are completely from a development point of view, although it does mean that it is much more easy to add extra tools now - I'll prove this once I have scratched my next itch :-)

I am now working on making the inpoint/outpoint selection on the clip and workspace monitors working correctly. The idea is that you can select which part of a video you want to use in the clip monitor, and then drag that section of the clip to the timeline.
