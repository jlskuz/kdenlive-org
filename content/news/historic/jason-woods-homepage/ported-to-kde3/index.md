---
date: 2002-05-11
title: Ported to KDE 3
draft: false
#layout: release-notes
author: Jason Wood
---

Latest updates to CVS should make Kdenlive compile on KDE 3. It wasn't a major job porting it - mostly it was getting KDevelop to generate a new set of files for Automake/Autoconfig, and then adding a couple of headers here and there where they seem to have changed in QT. My next aim is to make the AVFile calculate the correct lengths of files using aRts, which is mainly a case of figuring out why aRts keeps crashing the program whenever I try and use it at the moment...
