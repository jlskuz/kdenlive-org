---
date: 2003-01-15
title: Looking to the future
draft: false
#layout: release-notes
author: Jason Wood
---

It's been a very pleasant two days after the release, watching the traffic flow through the website and getting a warm fuzzy feeling from the odd praising comment. Keep them coming! We are very interested in hearing any ideas or problems that you wish to share with us.

Looking forwards to our next release, I have updated the roadmap on the progress page to show where we are, and the features that we are going to be adding in the next release.

For the moment, transitions and effects are on the back-burner - we are aiming for better editing tools, better usability, and more file formats! Check out the progress page for the complete list.
