---
date: 2003-03-15
title: Kdenlive translated to French
draft: false
#layout: release-notes
author: Jason Wood
---

Things have been a bit slow on the development front recently, since I have started a new job, and Rolf has been attending a conference in California. This hasn't stopped Giles though, who has been busily translating Kdenlive into French. Check out the screenshots page!
