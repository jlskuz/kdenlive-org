---
date: 2002-06-20
title: Internal Design
draft: false
#layout: release-notes
author: Jason Wood
---

I think I've sorted the design out now for the clips, as soon as I have made this compile, I'll update the changes to CVS.

My next task is to start looking at drag'n'drop support again. I want to have it up and running by the end of next week, hopefully. I will then start building up the timeline. In particular, I want to be able to drag clips onto the timeline, and move them around and delete them again. I give myself another week to do that ;-)
