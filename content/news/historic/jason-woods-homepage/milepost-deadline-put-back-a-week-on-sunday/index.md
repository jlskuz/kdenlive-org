---
date: 2002-08-07
title:  Milepost deadline put back a week on Sunday
draft: false
#layout: release-notes
author: Jason Wood
---

I had hoped to have reached the first milepost by the end of tommorrow, that is, I would have all basic timeline functionality finished. However, since I am going to be out all day tommorrow, and because the code I need to write is relatively complex, that is not going to happen.

To reach this milepost, Kdenlive still needs to be capable of several things :

- Dragging those files onto the timeline
- Selecting and moving them around on the timeline
- Deleting them from the timeline
- Razoring them on the timeline (cutting a single clip into two clips
- Copying and pasting a clip on the timeline
- Cropping clips on the timeline
- Zooming in/out of the timeline.

The code to handle this is very tightly woven and more complex than I was anticipating, and so I need a few more days to make sure that I get it right. I want all this functionality added now, as it leaves a firm foundation to build from. Basically though, attempting to miss out some of this functionality will mean another rewrite of the timeline code to add it in at a later date.
