---
date: 2002-08-22
title: Removed deadline
draft: false
#layout: release-notes
author: Jason Wood
---

I haven't got as much time to work on kdenlive as I would like recently, and don't know how long it's going to take to reach the deadline. So for the moment, I've removed a set date for reaching the first milestone. All I can say is - it will happen soon!
