---
date: 2003-12-14
title: New features in cvs
draft: false
#layout: release-notes
author: Jason Wood
---

Just a heads-up on some of the new features that have been going into cvs recently. Initial support for snap markers has been added. When finished, this feature will let you accurately align clips to each other at any frame, not limited to their end points.

A new load/save framework has been implemented for projects. This will allow for better compatability across versions of Kdenlive, and opens up the possibility of supporting project file formats from other video editors in the future.

You can give clips descriptions; this will become more useful when subclips are implemented.

Finally, you can play the defined inpoint/outpoint section of a clip by pressing Ctrl-Space, or clicking the relevant button on the monitor.
