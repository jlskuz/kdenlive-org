---
date: 2002-06-12
title: Project Update
draft: false
#layout: release-notes
author: Jason Wood
---

Wow - a month since I've had time to do any real work on this. Mainly due to the transition of Mandrake Cooker from gcc 2.96 to gcc3.01 completely breakng my build system... Oh well, it's back up now, and I've finally managed to add some aRts support that doesn't crash, though it is incredibly slow at adding files at the moment due to the way it checks file lengths (and I mean _slow_). Will be looking for better solution sometime soon, but at the moment I am simply happy that it works.

My next major task is to sort out exactly how clips, tracks and video files are going to be stored within project. This is trickier than it first seems, as a clip can effectively include an entire project itself, and can then have effects overlayed on top of it... and of course, the user should see no difference in the use of a clip over the use of a video file, except that clips can be "expanded" to see what is in them.

Or to put it another way, any part of the timeline can be taken and reduced to a single clip, which can then be used like a single piece of video footage. This is great when you have some tightly edited title sequence which cannot be rendered down because it contains elements such as e.g. Titles which need updating per show, but when your editing a 30 minute long show you really don't want to have to look at the title sequence as 100 seperate clips, transitions and overlays...
