---
date: 2004-04-02
title: Preparing for Kdenlive 0.2.4
draft: false
#layout: release-notes
author: Jason Wood
---

Kdenlive cvs is now in feature freeze for 0.2.4; only show stopper bugs and translations remain before release. This is mainly a bug fix release, although there are several new features such as the new "marker" tool, and the ability to import Kino projects into Kdenlive.

If you know of any bugs in the cvs version of Kdenlive, let me know!

