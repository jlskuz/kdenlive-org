---
date: 2002-11-17
title: Kdenlive Update
draft: false
#layout: release-notes
author: Jason Wood
---

We've been busy recently - Kdenlive is coming along nicely, The timeline is capable of drag 'n' drop, multiple selection, snapping clips to each other, and more. Rolf's rendering engine, Piave, is gaining some interesting new features, and we have almost finalised the interface on how the two should communicate. Look in the document section for the latest version of the interface.

I finished tracking down the last known bug in the timeline code today, but have to wait for sourceforge CVS to come back on-line before I can commit it :-)

Our main priority now that all of the main architectural decisions have been made, is to get a version released that does something, and shows off what Kdenlive and Piave are capable of. Don't hold your breath, but I think we may have something out in about a month.
