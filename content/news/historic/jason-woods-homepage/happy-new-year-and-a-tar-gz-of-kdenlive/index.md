---
date: 2003-01-01
title: Happy New Year and a tar.gz of kdenlive
draft: false
#layout: release-notes
author: Jason Wood
---

Hope everyone out there had a happy new year! I think it is nice to start the year as you mean to go on, and so I have now uploaded the first alpha release of Kdenlive. Head over to the download page for more info, and [contact me](mailto:uchian@blueyonder.co.uk) if you run into any difficulties.
