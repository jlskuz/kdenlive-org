---
date: 2002-07-12
title:  Timeline & Edit File codec development
draft: false
#layout: release-notes
author: Jason Wood
---

Work on drag 'n' drop has gone as far as it can for the moment, and my next task is to develop the timeline. I want it to be as general as possible so that it can be re-used in similar programs in the future. My first step is to write a generic KRuler class which I already have need of twice in the interface. Visually, this is already complete. All I have to do now is to give it the functionality of multiple sliders by adding marks to the widget. The latest picture shows the ruler both on the timeline, and in the monitor window.

Christian has begun work on the editing file codec format. More news as and when it arrives.
