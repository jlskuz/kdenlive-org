---
date: 2002-06-26
title: Drag 'n' Drop on it's way soon
draft: false
#layout: release-notes
author: Jason Wood
---

A minor update, but I am now implementing drag 'n' drop... and realising that I am also implementing the saving and loading of clips in the process. I'm a touch unsure as to information on standards for mime type encoding for drag and drop, but I have decided that all data passed by drag 'n' drop will be as XML files, converted to strings using the toUTF8() method. If this is a bad idea, please contact me and let me know.

As the drag 'n' drop data is XML, and the file format for my project is XML, it suddenly made a great deal of sense to only write the XML code once, so with drag 'n' drop I also get half of my saving and loading code too.

The downside to all this reuse of code is that I have to write more than I had anticipated to get drag 'n' drop working in the first place, so whilst I still aim for finishin it by the end of the week, it may take longer.
