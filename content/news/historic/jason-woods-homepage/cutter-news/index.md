---
date: 2002-10-29
title: Cutter News
draft: false
#layout: release-notes
author: Jason Wood
---

A couple of interesting updates. Firstly and most importantly, Rolf Dubitzky is very interested in bringing his rendering engine together with the Kdenlive GUI to the mutual benefit of all involved!

Secondly, I have now uploaded a new module to CVS, nullcutter. This is a skeleton Cutter which will act as a proof-of-principle for the communication from Cutter and GUI. It will also act as a template for writing new cutter. At present, the interface has not been completed, but you can run nullcutter and kdenlive (in that order) and get a little "bouncing ball" embedded in the correct place in the GUI. It's not much yet, but I was happy to see it working!
