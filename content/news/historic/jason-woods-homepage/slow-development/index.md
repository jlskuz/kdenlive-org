---
date: 2002-09-08
title: Slow Development
draft: false
#layout: release-notes
author: Jason Wood
---

Just a note so that the site doesn't look dead - I have not had much time to work on Kdenlive recently, and neither has Christian. However, development is still in progress.
