---
date: 2003-01-07
title: Kdenlive uses KDockWidgets
draft: false
#layout: release-notes
author: Jason Wood
---

I have added the use of KDockWidgets to Kdenlive. Essentially, this means that you can lay out the application however you want to - just drag the various windows around with the use of the Dock bar on the top of each widget, and you can put widgets into tab views, gimp-style layouts, or just shove the timeline at the top of the window if you so desire. Check out the [screenshots](/historic-screenshots) to get a better idea of what I mean!
