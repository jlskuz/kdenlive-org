---
date: 2002-11-30
title: Integration underway
draft: false
#layout: release-notes
author: Jason Wood
---

The integration of Kdenlive and Piave is well underway! Piave can now embed itself into Kdenlive, and display clips. Kdenlive can then tell Piave to seek around the clips. Nice! At the moment, Piave is using it's own hard coded render tree, so the next step is to communicate the contents of the timeline to Piave. It's possible that we will have this working within the week, so stay tuned!
