---
date: 2003-02-05
title: Development Progress
draft: false
#layout: release-notes
author: Jason Wood
---

We are now well into the development of the next release of Kdenlive/Piave. Currently, we are working on the capabilities section of the interface (yeah, very internal and if your not a developer, quite boring!) Well, in simple terms, Piave (or any other renderer) will tell Kdenlive what it can do, what file formats it supports, what effects it can perform, etc. etc. etc, and Kdenlive will then make all this info available to the user.

(Almost) all of our development effort is on supporting multiple file formats. This has required large changes in Piave, since it has moved over to a plugin architecture for easy expansion in the future. For Kdenlive, the main problem is on generically displaying all of the potential formats, codecs and parameters that they contain in an easy to understand, pleasing manner without being forced to hand code thousands of dialogs. We're getting there!

I would like to mention that Gilles CAULIER has offered to translate Kdenlive into French, and as a MjpegTools contributor, he is interested in adding VCD, SVCD, XVCD, DVD and DIVX support to Piave. I hope he is completely and utterly successful!

CVS is currently unstable, since Kdenlive and Piave are not quite in sync with one another yet, but stay tuned, and if interested, join the development effort!
