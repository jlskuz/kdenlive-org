---
date: 2002-09-17
title: Soundcard Blues
draft: false
#layout: release-notes
author: Jason Wood
---

I have been chasing down the bugs in the aRts code, only to discover that most of the problems I have been experiencing are probably not my (or aRts') fault. The trouble is the dodgy drivers for the Aureal Vortex that I have. Damn. I'm going to have to get a new soundcard before I can effectively pick up any audio or movie related code again.
