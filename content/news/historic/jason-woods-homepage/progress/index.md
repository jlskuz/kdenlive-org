---
date: 2003-06-09
title: Progress
draft: false
#layout: release-notes
author: Jason Wood
---

Well, I still haven't managed to do much myself, but Rolf and Giles have been busy - Giles has been working on adding some polish to the interface - tooltips, information in the about box, etc. and has also started looking to i80n'ise Piave.

Rolf is currenlty busy working on getting version 0.3 of Piave ready; amongst other things, there is support for effects and it now plays AVI dv's. Great stuff! Only problem is that the communication between piave and kdenlive is not yet in place, so you are limited to using example scripts for the moment.

I'm still looking to finish work on snap-to-grid - not enough free time these days, grr...
