---
date: 2002-04-20
title: Kdenlive is stil-still alive
draft: false
#layout: release-notes
author: Jason Wood
---

Since the last news item, I have got a lot of stuff done. Most of the interface uses QT-designer now, which means it's easily maintained, and the timeline now works in principle - it displays correctly, except that anything to do with drag-n-drop is going to have to wait until I update to KDE 3 (see my diary entry). I've also had to remove all traces of arts code for the present, as it kept crashing - probably because I don't yet understand it well enough to be sure that I've included all of the necessary bits and pieces. Hopefully the upgrade to KDE3 will make that process a lot smoother as well.

I'm still looking into the progress of arts video - there are a couple of competing projects at the moment, but none of them have progressed far enough to be of any use to me yet. In the meantime, I want to make Kdenlive work as a fully-fledged sound editor. Hmmm... I might take a look at KWave and see if I can integrate it in some way. I hope to be able to take a look at video4linux integration in the future as well.
