---
date: 2008-02-17
title: KDE4 porting progress
draft: false
author: Jean-Baptiste Mardelle
---

![screenshot of Kdenlive](kde4-2.png)

Development is going on for the KDE4 version, see attached screenshot. A lot of work has still to be done, I hope to be able to release a fisrt KDE4 version in 2 months.
Meanwhile the current KDE3 development version should hopefully be released as Kdenlive 0.6 in march...
