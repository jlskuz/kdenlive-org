---
date: 2006-01-14
title: Kdenlive is currently being ported to the MLT video framework
draft: false
---

Kdenlive is currently being ported to the [MLT](https://www.mltframework.org/) video framework. A lot of work is being done in the cvs repository, you can see a recent screenshot [here](/historic-screenshots).
