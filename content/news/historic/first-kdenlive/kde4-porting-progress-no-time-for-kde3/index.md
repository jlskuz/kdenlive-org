---
date: 2008-06-26
title: KDE4 porting progress, no time for KDE3 version....
draft: false
author: Jean-Baptiste Mardelle
---

![screenshot of Kdenlive](kde4-3.png)

The KDE4 version is progressing nicely, and several improvements were made to the MLT video framework which should make the next release very attractive. Sadly, I don't have enough time to work on the KDE3 version, so no progress was made on that side.

I know it is frustrating that no release was made for a long time, but the port to KDE4 was almost a complete rewrite, and the KDE3 version, which was not originally written for MLT was too complex to maintain.

Here you can see a screenshot of current development version. What still needs to be done is some work on transitions and better loading/saving, then we will be ready for a beta release.
