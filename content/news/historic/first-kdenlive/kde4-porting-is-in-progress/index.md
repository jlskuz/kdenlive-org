---
date: 2008-01-06
title: KDE4 porting is in progress.
draft: false
author: Jean-Baptiste Mardelle
---

![screenshot of Kdenlive with KDE4 and QT4](kde4-1.png)

I think it is time to give some news about Kdenlive development. I am currently working on the KDE4/QT4 port of Kdenlive. If everything goes well, I hope to have all basic features working in february. This is also an occasion to do a big code cleanup, so hopefully, it will be easier for new developers to join the project.

More news as soon as Kdenlive for KDE4 is becomming ready. You can already have a look at the screenshot that is showing the current development state...
