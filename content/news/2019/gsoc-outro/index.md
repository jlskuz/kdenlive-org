---
title: GSoC 2019 comes to an end
author: Akhil K Gangadharan
date: 2019-08-28T12:08:39+00:00
aliases:
- /en/2019/08/gsoc-19-comes-to-an-end/
- /it/2019/08/gsoc-19-e-finito/
- /de/2019/08/gsoc-19-geht-zu-ende/
categories:
  - GSoC
  - Mentorship
---
GSoC period is officially over and here is a final report of my work in the past 3 months.

## QmlRenderer library

The library will be doing the heavy lifting by rendering QML templates to QImage frames using [QQuickRenderControl][1] in the new [MLT QML producer][2]. Parameters that can be manipulated are:

  * FPS
  * Duration
  * DPI
  * Image Format

The library can be tested using QmlRender (a CLI executable).

Example:

```bash
./QmlRender -i "/path/to/input/QML/file.qml" -o "/path/to/output/directory/for/frames"
```

`./QmlRender –help` reveals all the available options that may be manipulated.

## MLT QML Producer

What has been done so far?

  * A working and tested QmlRenderer library
  * Basic code to the QML MLT producer

What work needs to be done?

  * Full-fledged MLT QML producer
  * Basic titler on Kdenlive side to test

Check out the in full GSOC depth report [here][3].

The whole experience for the last 8 months, right from the first patch to the titler project, has been great with a steep learning curve but I have thoroughly enjoyed the whole process. I seek to continue improving Kdenlive and I'm really thankful to all the Kdenlive developers and the community for presenting me with this fine opportunity to work for the revamp of an important feature in our beloved editor.

Although GSoC is "officially" over, the new Titler as a project in whole is far from done and I will continue working on it. So nothing really changes. 😉

The next update will be when we get a working backend set up – until then!

 [1]: https://kdenlive.org/en/2019/06/gsoc-2019-week-2-with-the-titler-tool/
 [2]: https://github.com/akhilam512/mlt/commits?author=akhilam512
 [3]: https://community.kde.org/GSoC/2019/StatusReports/AkhilKGangadharan
