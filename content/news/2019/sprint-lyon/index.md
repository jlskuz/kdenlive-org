---
title: Sprint 2019 in Lyon
author: Farid Abdelnour
date: 2019-03-26T21:29:32+00:00
aliases:
- /en/2019/03/sprint-2019-in-lyon/
- /it/2019/03/sprint-2019-a-lione/
- /de/2019/03/sprint-2019-in-lyon-2/
categories:
  - Event
  - Sprint
---

![](photo5796667513029898073.jpg)

## Day 1

The sprint started with a brainstorming session on how to move forward with the refactoring version, since we had already decided to merge the code with the master branch. The result was a list of tasks that we would work on in the following days:

* Fix all known blocking bugs for the release.
* Test and fix new/hidden bugs.
* Decide which features to add after the release.
* Review the user interface and usability.


## Day 2

On the second day, we really threw ourselves into our work. While Jean-Baptiste, Nicolas and Vincent were hacking at the code and submitting patches, Massimo and Rémi fired up Kdenlive to test the new version and try to find new bugs. Massimo provided suggestions for speeding up the editing workflow, while Rémi gave feedback on the effects UI.

![](photo5787367362645831916.jpg)

![](photo5791870962273202242.jpg)

Vincent and Eugen concentrated on building Kdenlive for Windows on a slow Windows machine. After struggling for a bit, they managed to get it to run properly. Meanwhile, we updated the [instructions to build Kdenlive for Windows in our manual][1], and Nicolas investigated optimizing the fuzzy testing and other test routines to hunt for bugs. During all this, Jean-Baptiste carried on submitting fixes.

## Day 3

The third day of the sprint was dedicated to usability. Camille prepared the overall effects list, and we used it to evaluate which effects are working and how. Jean-Baptiste managed to fix errors in some of them. Massimo submitted [a report on which effects work and which are obsolete or broken][2], while Vincent implemented the export profile with alpha channel, and tried to improve the export render speed.

{{< youtube 93g3RXEA3cU >}}

## Day 4

After four days, we had committed 85 fixes and implemented some new and old features (such as the one-click effect). The team had a great time despite the long hours we spent working. The sprint boosted our team spirit, and we were very productive.

We would like to thank [KDE e.V.][3] for helping make this sprint happen, and Vincent's family for hosting us with their warm hospitality.


You can find our [daily build AppImage][4] on KDE's or try the [latest beta Windows version][5].

Oh! Did we mention we have stickers? 😉

![](photo5791870962273202213.jpg)


 [1]: https://community.kde.org/Kdenlive/Development/WindowsBuild
 [2]: https://lite.framacalc.org/kdenlive-effects
 [3]: https://ev.kde.org/
 [4]: https://binary-factory.kde.org/job/Kdenlive_Nightly_Appimage_Build/lastSuccessfulBuild/artifact/
 [5]: https://files.kde.org/kdenlive/unstable/kdenlive-19.04beta-git20190321.exe
