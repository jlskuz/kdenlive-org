---
title: Time to merge!
author: Farid Abdelnour
date: 2019-03-11T15:38:18+00:00
aliases:
- /en/2019/03/time-to-merge/
- /it/2019/03/e-tempo-di-unire/
---

After many delays, we finally think the Timeline Refactoring branch is ready for production. This means that in the next days major changes will land in Kdenlive's git Master branch scheduled for the KDE Applications 19.04 release.

**A message to contributors, packagers and translators**

A few extra dependencies have been added since we now rely on QML for the timeline as well as QtMultimedia to enable the new audio recording feature. Packagers or those compiling themselves, our [development info page][1] should give you all information to successfully build the new version.

We hope everything goes smoothly and will be having our second sprint near Lyon in France next week to fix the remaining issues.

We all hope you will enjoy this new version, more details will appear in the next weeks.

Stay tuned!

 [1]: https://community.kde.org/Kdenlive/Development#Installing_dependencies
