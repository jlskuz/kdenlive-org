---
title: My experience using Kdenlive on the 48 Hour Film Project
author: Bruno Santos
date: 2019-06-27T14:59:16+00:00
draft: true
categories:
  - Showcase
---

![](48HFP.jpg)

Film is a part of our lives, and I had been more interested with the process of film making since a few years ago. I had recently come to know of the [48 Hour Film Project][1] and with the knowledge that more than spending many hours reading and watching videos about the subject, there is nothing like getting your hands dirty to advance your know how, I thought it would be a great first time experience on actual film making.

Nevertheless, I had been putting it on hold until I had some equipment and some other people to work with. Besides getting together some equipment that I thought was good enough to start and learn on, I didn't have any connections in this area, and a friend that shared this interest was away traveling by bicycle in a different continent.

Fortunately, he returned of hist 2 years trip this year, and being in the process of waiting for a my son to be born, this was the perfect incentive to jump into this crazy weekend adventure.

This international contest is run during one weekend, with the starting event annoucing the two themes each team can use on their films, and the character, object and phrase that are shared to all teams.

To our bad luck our theme was Film de Femme (a 48HFP original theme), and Holiday Film, which was a challenge, since we weren't in a Holiday season, and we had access to few props for this theme, we had a problem. Since none of us was female, we had two problems. After contacting a few friends, we were able to convince a female friend to participate as an actress in our film.

We had to write our script in the meanwhile, so we used the open format [Fountain][2] language to do it. [This][3] is the result PDF of the script we used to film the next day.

We had our script, our lines and characters, we grabbed our camera, tripod, put it all in our bicycles and went to a public park to film. It all went as expected, we (as expected) should have got more coverage of some scenes, different angles, different kinds of shots, etc, but as the first experience we were satisfied with all we got and we cycled back home to film the rest of the scenes. Having a shot list extracted from the script helped a lot.

After all the parts were filmed, we copied our files to the computer, did some backups of the main files, organized everything the best we could at the moment, and imported it to kdenlive. The recent fixes to the folder importation would've been helpful with this task.

One of the first scenes we prepared was using [Natron][4] for day dreaming of Laura, where the dog silhouette appears in the clouds. The clouds were created using the Shadertoy node, and using a png of a dog silhouette and some keyframing, we kind of got something that resembles a dog shaped cloud being formed in the sky.

![](Screenshot_20190819_004504.png)

After having all the files in Kdenlive, we could start cutting the film. Our footage was captured with a Panasonic G6, so not a high bitrate codec, but that would also make editing and rendering easier.

We cut all the best performances of the scenes, and for this having the possibility to save the zones as sub clips would be very helpful. Not a feature we used on such a short time for the edit, but something we certainly found to be a great tool for future projects.

![](Screenshot_20190819_004236.png)

(…)

The timeline of our final project was like this:

![](Screenshot_20190819_002325.png)

With a team so small, we didn't have time to color correct and grade very well, but in the end would allow us to render the film a few times to review all the cuts and change anything that we thought it could be better. With the fast renders we got from Kdenlive we were able to have the film ready to deliver on time.

The film we delivered in the end was this:

{{< youtube Qf5S4qxJpsU >}}

Since then we made some changes to the final cut, and add some music and effects that we thought could improve the film, and our final version of the film is this:

{{< youtube zn9-f8kyUNU >}}

(…)

 [1]: http://48hourfilm.com
 [2]: https://fountain.io/
 [3]: guiao_ver02.pdf
 [4]: https://natrongithub.github.io/
