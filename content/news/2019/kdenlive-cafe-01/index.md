---
title: First Café post Refactoring
author: Farid Abdelnour
date: 2019-05-06T16:07:53+00:00
aliases:
- /en/2019/05/first-cafe-post-refactoring/
- /it/2019/05/primo-cafe-dopo-il-refactoring/
categories:
  - Café
  - Event
---
The first Kdenlive Café after the release of the refactored code will be held this Tuesday 7th of May at 9PM CEST. We will dedicate the first slot of the meeting to listen to the community's reaction and answer any doubts or issues you might have. As usual we'll be at #kdenlive on IRC and the Kdenlive Telegram group.

See you!
