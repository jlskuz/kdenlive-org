---
title: Building and Distributing Kdenlive on Windows and Mac
author: Vincent Pinon
date: 2019-01-29T22:15:16+00:00
alias:
- /en/2019/01/building-and-distributing-kdenlive-on-windows-and-mac/
categories:
- Inside
---

## Short story

As a new year gift, you can now easily build Kdenlive on Windows, Mac, and of course Linux & BSD, fetching up-to-date dependencies thanks to KDE [Craft][1] package manager. You can simply download our latest builds… or for hacking & building yourself, follow typical [Craft process][2]:

  * Having a [python 3][3] interpreter (preferably 64bits if your OS supports it)
    and for Windows additionaly PowerShell at least version 5 and .NET at least 4
  * Create a directory (eg `Craft` on Windows, preferably in C:\)
  * In this directory, download the [installer script][4] and run it (on windows choose compiler: mingw-w64, architecture: x64)
  * Source the shell environment: `. craft-master/craftenv.sh` on Unix/Mac, or `craft-master\craftenv.ps1` on Windows PowerShell (choose "Always run")
  * Bootstrap the package manager: `craft craft`
  * Get kdenlive and all dependencies: `craft kdenlive`
  * Enjoy!

![](Craft-Kdenlive.jpg)

## Background history

### First cross-builds with MXE

"Porting" Kdenlive to other platforms has been in the works for years now: old Qt4 version was in MacPorts, and we started looking at Windows version with our [GSoC student][5] back in Spring 2016. We anticipated we wouldn't have much to change in the code, as Kdenlive relies on the portable Qt5/KF5 combo, and MLT, which is already embedded in [Shotcut][6] cross-platform editor.

At that time, KDE didn't have Windows & Mac build servers yet, and I had no Windows machine available. I thought it would be more efficient to use the Linux machines. Moreover, the young Craft lacked multimedia libraries (audio and video codecs, effects), while [MXE][7] cross-build system had several ready. I liked MXE's use of GNU Make to resolve build dependencies, it's automated version update, simple build option overrides… So we went for it and built all the stack up to our final application!

The main difficulty has been deployment: build ends with several executables (kdenlive, melt used for rendering, ffmpeg for transcoding, dbus-daemon for notifications…), shared libraries, plugins (qt's, mlt's, frei0r's…), data files… and it didn't work out of the box! Finding a layout to please all these required several trials and tweaks, and that step didn't allow for direct testing while developing & recompiling.

At least with this work we could share Windows binaries, published in 2016 under "alpha" preview state, which rapidly found users despite of several shortcomings… And the situation remained so during more than a year, during which updates didn't concern platform specific bugs.

### Developing in Windows with MSYS2

The most annoying bug: the basic play/pause function was very unreliable! (how could people live with it?)
This seemed due to multi-threading specificity on Windows, but to solve this any developer would need a debug environment. Windows users sometimes offered to help, but they were immediately stuck by the "foreign" build process, and so was I.

At some point I traveled for my job with a Windows laptop: to feel home I set up a MSYS2 environment, with Bash and Pacman. I ported our Linux AppImage build script, and was able to compile Kdenlive and dependencies directly on Windows. Then after a few trials, the play/pause bug got fixed!

### Should have started here: Craft!

Next problem: closing Kdenlive was leaving the process running, locking files and occupying memory. This proved to be an issue known by KDE-Windows team, due to KF5 / Qt-DBus / Windows threading loop. The fix was available in Craft, along with other patches and special build options… Instead of importing all these and rebuild every update, it was more than time to use friends work and use Craft, and so benefit from updates & fixes, pre-built cache, automated builders… and most important, new friends to meet at Akademy or Randa Meetings.

I just had to install a recent PowerShell ([without admin rights][8]), import "a few" [library packages][9], and patch MLT to allow running Kdenlive without deployment phase. Finally, anyone wanting to simply build and a run Windows version can do it with the steps above.

Craft also comes with built-in installer generation, so `craft --package kdenlive` ends with an NSIS-type compact exe file, which Windows users are more used to that the raw archives available before.

Last but not least, Craft also works on Linux/*BSD… and MacOS! Though on the latter, the build succeeds but the program doesn't start, due to a DBus startup failure I had no time to debug (as I borrowed a Mac only for a few hours). But I believe we are really close to be able to feed DMG eaters.

# Conclusion

If you want to setup a Kdenlive environment with up-to-date libraries in a corner of your system, no problem, whatever the OS you choose!

Welcome in the Crafters friendship. 🙂

 [1]: https://community.kde.org/Craft
 [2]: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows
 [3]: https://www.python.org/downloads/
 [4]: https://cgit.kde.org/craft.git/plain/setup/CraftBootstrap.py
 [5]: https://community.kde.org/GSoC/2016/StatusReports/joshwambua
 [6]: https://shotcut.org
 [7]: https://mxe.cc
 [8]: https://github.com/PowerShell/PowerShell/releases/
 [9]: https://cgit.kde.org/craft-blueprints-kde.git/commit/?id=0ae1101d58d6fc71993962736cd0b7508a08c452
