---
title: Ready to test?
author: Jean-Baptiste Mardelle
type: post
date: 2019-02-21T18:10:47+00:00
aliases:
- /en/2019/02/ready-to-test/
categories:
- Event
---

![](Screenshot_kdenlive.png)

If you followed Kdenlive's activity these last years, you know that we dedicated all our energy into a major code refactoring. During this period, which is not the most exciting since our first goal was to simply restore all the stable version's features, we were extremely lucky to see new people joining the core team, and investing a lot of time in the project.

We are now considering to release the updated  version in April, with KDE Applications 19.04. There are still a few rough edges and missing features (with many new ones added as well), but we think it now reached the point where it is possible to start working with it.

## Testing day tomorrow 

This is why we are organizing a test day event tomorrow, **22nd of February 2019, from 17:00 to 22:00 CET**, so you can try it, report the problems you encounter and help us shape the future.

## How to join ?

  * Download the latest [AppImage][1]
  * Make it executable (in a terminal: chmod +x ) or from your file manager
  * Test it!

More tech-savy users can alternatively compile it with debug flags following [this guide][2] and send backtraces

## Give us your feedback

During the event, some team members will be reachable on our #kdenlive channel on irc/freenode or via the [kdenlive telegram group][3], and you can also leave your feedback in [this Phabricator task][4].

## Known issues

There are a couple of known problems that will be solved before release:

  * Audio thumbnails in timeline eat up lots of memory, and can cause a freeze if large audio files are used, you can disable them as a workaround in larger projects
  * Motion tracker not usable yet

 [1]: https://binary-factory.kde.org/job/Kdenlive_Nightly_Appimage_Build/lastSuccessfulBuild/artifact/
 [2]: https://community.kde.org/Kdenlive/Development#Getting_the_source_code
 [3]: https://t.me/kdenlive
 [4]: https://phabricator.kde.org/T10511
