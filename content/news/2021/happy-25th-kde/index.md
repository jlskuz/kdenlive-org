---
title: Happy 25th KDE
author: Farid Abdelnour
type: post
date: 2021-10-14T14:22:03+00:00
aliases:
- /en/2021/10/happy-25th-kde/
- /fr/2021/10/bon-anniversaire-kde/
- /it/2021/10/buon-compleanno-kde/
- /de/2021/10/herzlichen-glueckwunsch-kde/
---
![](lovekde.png)

Happy 25th birthday KDE. We love being part of this wonderful community! Join the party at: https://25years.kde.org/
