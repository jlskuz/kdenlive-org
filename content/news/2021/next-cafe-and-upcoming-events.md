---
title: Next Café and upcoming events
author: Farid Abdelnour
date: 2021-05-28T16:08:51+00:00
aliases:
- /en/2021/05/next-cafe-and-upcoming-events/
categories:
  - Café
  - Event
---
The Kdenlive Cafés will be on the second Tuesday of every month always starting at 9PM (Paris time). So the June Café will be on the 8th. Also don't miss the Kdenlive demo by Arkengheist at the [Libre Graphics Meeting][1] this Saturday 29 at 2PM (Paris time).

You can keep up with all the upcoming events at the [events page][2] or in the footer of the website.

 [1]: https://libregraphicsmeeting.org/2021/en/presentation18.html
 [2]: https://kdenlive.org/events/
