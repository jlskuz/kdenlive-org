---
title: First Video Café
author: Julius Künzel
date: 2021-05-03T18:09:57+00:00
aliases:
- /en/2021/05/first-video-cafe/
- /fr/2021/05/premier-video-cafe/
categories:
  - Café
  - Event
---
After a long period of silence on the Kdenlive Cafés they are now coming back in very special format: next Tuesday, the 4th of May at 9pm CEST (= 7pm UTC) we will have the first Kdenlive Video Café on KDE's Big Blue Button!! Join us for 2 hours of live videochat on video-editing, video-storytelling, plans, projects and all things Kdenlive. See you soon!

Update: correct link for the meeting: <https://meet.kde.org/b/far-twm-ebr>

Update: Here is a review of the meeting: <https://kdenlive.org/en/2021/05/review-of-the-first-kdenlive-video-cafe/>
