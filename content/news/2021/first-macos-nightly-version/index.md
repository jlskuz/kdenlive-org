---
title: Kdenlive comes to macOS (nightly version)
author: Julius Künzel
type: post
date: 2021-10-21T08:00:41+00:00
aliases:
- /en/2021/10/kdenlive-comes-to-macos-nightly-version/
- /fr/2021/10/kdenlive-arrive-sur-macos-versions-test/
- /it/2021/10/kdenlive-arriva-su-macos-versione-notturna/
- /de/2021/10/kdenlive-kommt-fuer-macos-taegliche-builds/
---

![](screenshot.png)

Every now and again users would ask for a macOS version of Kdenlive. Up until recently, the only thing we were able to offer was a very, very old [MacPorts version][1] (0.9.10).

But, after Vincent and I invested some time in it, we are happy to announce that we now have an up-to-date nightly build for macOS! However, since Kdenlive is a complex application with many dependencies, **it still needs some testing before we can call it officially stable.**

And that's where you come in: If you are a Mac user, please consider downloading the nightly macOS version and testing it!

## Download and test

Note that, when running this version of Kdenlive on macOS, you may get an "unidentified developer" warning. You can still open the app by following the instructions on Apple's support page [the Apple's support page][2].

<a class="button" href="https://binary-factory.kde.org/job/Kdenlive_Nightly_macos/">Download nightly DMG</a>

## Report issues

Please report any bugs you come across either to <https://bugs.kde.org>, or on [GitLab issue #993][3]. In your report, please remember to mention the number of the build you are testing. Also, keep in mind the general [bug report conventions][4].

### Known issues
  
  * By default the mac style is used instead of the dark breeze style which recommended for Kdenlive (you can change the style in the settings menu)
  * The layout switcher is not visible since a native menu bar is used on mac. You can still switch the layout from the view menu ([BUG 441033][5])

## Technical background

If you follow the Kdenlive project closely, you will remember that in the [first video cafe][6] we talked about macOS and that a big blocker was that DBus caused trouble with the dmg packaging. DBus is used for the communication between the render process and the application's main window.

Vincent [made it possible][7] to build Kdenlive with the cmake flag `-DNODBUS=ON`. When you use this flag, Kdenlive uses [QLocalSocket][8] and [QLocalServer][9] for communication instead of DBus.

<img style="width: 150px; height: 150px" src="craft.png"></img>

Once the DBus issue was solved, there were only some small issues left to fix. To build Kdenlive on macOS, we use [KDE Craft][10], a meta-build system and package manager that we have already been using for some time for our Windows builds.

The main job after was to adjust the so-called "blueprints", and tweak some parts of the Kdenlive code to use the right file paths to find all the plugins. It took a while to find and fix all these small issues, and the last one we were aware of was that the frei0r effects were missing.

This last problem has now been [solved][11].

## Related code changes

Note: these are only some of the most important changes

### KDE Craft Blueprints

  * Kdenlive master allows to avoid dbus [Commit][12]
  * Kdenlive beginning to work on Mac (in DMG) [Commit][13]
  * Kdenlive: fix finding frei0r, skip kf5filemetadata [Commit][14]
  * MLT: relocatable build (Linux/MacOS) [Commit][15]

### Kdenlive

  * Allow to use QLocalSocket/Server instead of DBus [Commit][7]
  * Fin MLT on macOS [Commit][16]
  * Fix install location on macOS [Commit][17]
  * Use .rcc icons on macOS too [Commit][18]

### MLT

  * CMake: fix MacOS build and allow relocatable build [Commit][19]

 [1]: https://ports.macports.org/port/kdenlive/details/
 [2]: https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac
 [3]: https://invent.kde.org/multimedia/kdenlive/-/issues/993
 [4]: https://kdenlive.org/en/bug-reports/
 [5]: https://bugs.kde.org/show_bug.cgi?id=441033
 [6]: https://kdenlive.org/en/2021/05/review-of-the-first-kdenlive-video-cafe/
 [7]: https://invent.kde.org/multimedia/kdenlive/-/commit/3c53bf3b59ddd93db0e82c03656f48689ff2b3d4
 [8]: https://doc.qt.io/qt-5/qlocalsocket.html
 [9]: https://doc.qt.io/qt-5/qlocalserver.html
 [10]: https://community.kde.org/Craft
 [11]: https://invent.kde.org/jlskuz/craft-blueprints-kde/-/commit/c05d427e462bd0fb6879fcb958198f58085fdbb1
 [12]: https://invent.kde.org/packaging/craft-blueprints-kde/-/commit/475daca1207056b80073b2972ad6687fe5151808
 [13]: https://invent.kde.org/packaging/craft-blueprints-kde/-/commit/069040fd586b8b5769b5c086b2b73cb30e1a20fc
 [14]: https://invent.kde.org/packaging/craft-blueprints-kde/-/commit/6507fc705883086780be0a49b04d07b4d8bfb591
 [15]: https://invent.kde.org/packaging/craft-blueprints-kde/-/commit/626149ffcc4ee56968373643544f62e35b150191
 [16]: https://invent.kde.org/multimedia/kdenlive/-/commit/afff6143184e4b8105451cb2bf196b8761b16715
 [17]: https://invent.kde.org/multimedia/kdenlive/-/commit/99781d53b8e93491ee6c92996cc61705f6945f8f
 [18]: https://invent.kde.org/multimedia/kdenlive/-/commit/289700b22f692da14010803d7ade48c5f6f8f25d
 [19]: https://github.com/mltframework/mlt/commit/c09a929cfedb517ac16ee76af4afc938a15d9e02
