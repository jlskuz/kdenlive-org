---
title: Review of the 2nd Kdenlive video café
author: Eugen Mohr
type: post
date: 2021-06-10T18:57:41+00:00
aliases:
- /en/2021/06/review-of-the-2nd-kdenlive-video-cafe/
categories:
  - Café Log
---
About 15 participants joined the second Kdenlive video café last Tuesday, 8th June 2021. We had a nice meeting of about 2 hours with inspiring discussions.

Here the short written review of the meeting:

## Roadmap: showcasing upcoming new features

Jean-Baptiste is currently working on [multiple timeline][1] and multi project load. He showcased the actual status of the integration which looks promising. The goal is to have multiple timelines for version 21.08 due in August.

Massimo showed the status of the [advanced trimming tools][2] with the first integration of slip trimming. The integration with the blue "ribbon" was much admired. Advanced trimming is being worked on by Julius and expected for the 21.12 release.

We also talked about the possibility of upgrading to MLT7 which will bring the much requested Time Mapping feature. 

## macOS Support

The question of MacOS support comes up again. We still have the packaging problem with the dbus system. One participant offers a Mac that the Kdenlive team can test further. We need also help as the developers are not familiar on the macOS platform. We are tracking the progress on this GitLab issue: <https://invent.kde.org/multimedia/kdenlive/-/issues/993>. There are the names who are working on it.

## Manual

Since the last café good progress has been made on the manual. An analysis show that since begin of the year 90'000 people checked the manual. 25% of the user looking for effect, 11% looking for the timeline, and 8% looking for either the Windows issue or the QuickStart. Another 5% looking either for Titles or project submenus or transitions.
We are surprised how important the manual is.

You can help updating the [manual][3] further, exchange outdated parts and missing features. If you want to give something back to the community helping in the manual work group is a great possibility. To get involved read <https://community.kde.org/Kdenlive/Workgroup/Documentation>.

## Marketing, Donations, Social Media,…

We heard several questions around this topic during the café: why is Kdenlive not present on social media platforms like Twitter, YouTube, Mastodon? Does donating for Kdenlive generates some taxes for US users?  
The main aspect is the work that is required for maintaining all the social media channels, donations, etc.: we are a small team of volunteers and at the moment we would not have the capacity to manage all of this without neglecting other important things. 

By the way: Kdenlive is already on [Telegram, Matrix, IRC][4], the [KDE Forum][5] and [reddit.][6]

## Don’t miss these events!

Kdenlive at Akademy: Massimo Stella presents the latest Kdenlive news at Akademy. More info: <https://conf.kde.org/event/1/contributions/15/> on **Saturday, 19th June 2021**.

## Other Topics

  * Wayland support for Kdenlive has not been looked at so far.
  * User feature requests during the Café:
    * Possibility to have keyboard shortcuts for adding keyframes. Just integrated check the nightly build.
    * Gang clip monitor with project monitor. This could be a MLT performance issue playing 2 monitors together.

If you want to report bugs or feature requests please use <https://bugs.kde.org> and read this instruction <https://kdenlive.org/en/bug-reports/>

 [1]: https://invent.kde.org/multimedia/kdenlive/-/issues/226
 [2]: https://invent.kde.org/multimedia/kdenlive/-/issues/1069
 [3]: https://userbase.kde.org/Kdenlive/Manual
 [4]: https://community.kde.org/Kdenlive#Contact
 [5]: https://forum.kde.org/viewforum.php?f=262
 [6]: https://www.reddit.com/r/kdenlive
