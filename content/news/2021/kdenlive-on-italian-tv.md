---
title: Kdenlive on TV for a main national Italian broadcaster.
author: Massimo Stella
date: 2021-10-15T13:12:47+00:00
aliases:
- /en/2021/10/kdenlive-on-tv-for-a-main-national-italian-broadcaster/
- /fr/2021/10/9157/
- /it/2021/10/kdenlive-in-tv-su-uno-dei-principali-canali-televisivi-italiani/
- /de/2021/10/kdenlive-im-fernsehen-fuer-einen-grossen-nationalen-italienischen-sender/
categories:
  - Showcase
---
On October the 13th, the Italian broadcaster Mediaset showed a 2-minute clip during the prime-time Comedy Show Honolulu on its Italia 1 channel. This is that clip:

<https://www.mediasetplay.mediaset.it/video/honolulu/alessandro-bianchi-il-tg-del-futuro_F311066901003C03>

The "News from the Future" segment was produced using Kdenlive, and that is a milestone for our community because, along with the recent inclusion of Kdenlive in ASWF's landscape catalog (<https://landscape.aswf.io/>), it places Kdenlive on a whole new level.

Our beloved application was able to deliver the content keeping to a deadline (which on TV is always very tight), at the requested quality standard, and in the required format. Also, Kdenlive allowed us to quickly carry out a lot of the modifications the network asked for to better adjust the content to their internal policy.

But this is not the end of our quest for quality and improvement, in fact, it is only the beginning. It is, however, a sign we are moving in the right direction. But we cannot carry on without you, our community. You help us improve, and we would love to share your recent productions with the world. Send us your work and help us and others learn how Kdenlive is being used and how the community is growing.

Thank you for being part of Kdenlive.
