---
title: Review of the first Kdenlive video café
author: Julius Künzel
date: 2021-05-08T12:29:57+00:00
summary: About 30 participants joined the first Kdenlive video café last Tuesday, the 4th of May 2021! Here is a review of the event…
aliases:
- /en/2021/05/review-of-the-first-kdenlive-video-cafe/
- /fr/2021/05/compte-rendu-du-video-cafe-de-kdenlive/
- /it/2021/05/resoconto-del-primo-video-cafe/
- /de/2021/05/rueckblick-auf-das-erste-kdenlive-video-cafe/
categories:
  - Café Log
---
About 30 participants joined the first Kdenlive video café last Tuesday, the 4th of May 2021! We had a nice meeting of about 2,5 hours with inspiring discussions. We got great feedback and we are already having plans to reintroduce community meetings more frequently…

Several people asked for a recording of the meeting e.g. because they were not able to join (the whole time), but we feared that this might have destroyed the informal atmosphere where people dared to speak freely. We have to think how we will do it in the future, but for this time here is a short written review of the meeting:

## macOS Support

MacOS Support is still a goal we want to reach and during the past month we saw some progress there, but we are currently facing a packaging problem with the dbus system. One of the reasons for the slow progress is that none of the Kdenlive team members has a Mac or advanced knowledge in packaging and development on the macOS platform. We are tracking the progress on this GitLab issue: [#993][14]

## Titler

As a constant topic we also talked about the titler. All in all the titler works well (some bugs have been recently fixed for 21.04) and in comparison to other video editors it is not bad, but there are still [many ideas for improvements][1]. The GSoC 2019 project for a QML Titler is not finally ready as there are still multi threading issues that are hard to solve – and it is only the backend, the frontend is another big task… To sum it up we have to say that improvements would be nice, but there are more important tasks to do first.

## Manual

We really need help on updating the [manual][2] as it is outdated and features are missing. In consequence users don't know how to use some features and there are again and again the same questions on Kdenlive's support channels. The written manual has a different purpose than videos (the manual is more about look for details whereas video tutorials are for "new comers" explaining whole workflows), both are important. If you want to give something back to the community helping in the manual work group is a great possibility. Even if you don't feel comfortable with writing texts you can e.g. update screenshots! To get involved read https://community.kde.org/Kdenlive/Workgroup/Documentation.

## Marketing, Donations, Social Media,…

We heard several questions around this topic during the café: why is Kdenlive not present on social media platforms like Twitter, YouTube, Mastodon? Why is there no way to donate to for Kdenlives development? What about a Showcase (like e.g. [Krita's][15])? Will Kdenlive come to the Microsoft Store?

For all these questions the answer is almost the same. All these things are nice to have and most of them are already on our todo list. However they are also related to questions concerning branding and legal things like a trademark. We want to solve these issues first before we go further. Another aspect is the work that is required for maintaining all the social media channels, donations, etc.: we are a small team of volunteers and at the moment we would not have the capacity to mange all of this without neglecting other important things.

By the way: Kdenlive is already on [Telegram, Matrix, IRC][3], the [KDE Forum][4] and [reddit][5]. Further more it is possible to [donate to KDE][6]. Kdenlives benefits form those donations as it uses the KDE infrastructure and things like the Sprints are payed by KDE. However if you don't want to donate to KDE we recommend putting the money aside to donate to Kdenlive as soon as possible in a hopefully not too distant future.

## Roadmap for the near future

Jean-Baptiste is currently working on a refactoring of the jobmanager to fix some major performance issues. A few days ago version 7 of Kdenlives media backend MLT [has been released][7] and we already made [some steps][8] to support the new version. MLT7 supports time remapping and we want to add this high requested feature soon. Two other features we are going to work on are multiple timelines and [advanced trimming tools][9]. During the past releases we had good experience with 1-2 big new features per release and beside that work on [polishing and bug fixing][10].

## Don't miss these events!

Community member Michael Tunnell will start a new series of video editing tutorials with Kdenlive soon and Massimo from the Kdenlive team has been invited to his [live video podcast][11] for **Sunday, May the 16th**.

Roxane is already known by the community for here [awesome kdenlive tutorials][12] and will give a Kdenlive live demo on **Saturday, May the 29th** at the [Libre Graphics Meeting 2021][13]!

## Other Topics

- **DVD Wizard**: We consider to remove the DVD Wizard as it is not well maintained, DVDs are deprecated and there are more specialized tools. During the café there have again not been objections to remove it. ([issue #1005][16])
- **Wayland** support for Kdenlive has not been faced yet, but as it becomes more mainstream we will probably face it soon.
- **Idea**: Ask the community to design new <strong>splash screens</strong> for each version.
- User <strong>feature requests</strong> during the Café:
  - Possibility to centering all effects/animation (resize)
  - Possibility to editing titles on the monitor
  - Option to make deprecated effects appear (hidden by default). Related to [issue #350][17]
  - Remove space should move guides (optionally) -> We just implemented an option for lock/unlock Guides.
  - Multiple Subtitle Tracks (different languages) and export to mkv ([issue #1039][18] and [issue #1022][19])
  - DCP support ([issue #1041][20])

If you want to report bugs or feature requests please use <https://bugs.kde.org> and read this instruction <https://kdenlive.org/en/bug-reports/>

 [1]: https://invent.kde.org/multimedia/kdenlive/-/issues/460
 [2]: https://userbase.kde.org/Kdenlive/Manual
 [3]: https://community.kde.org/Kdenlive#Contact
 [4]: https://forum.kde.org/viewforum.php?f=262
 [5]: https://www.reddit.com/r/kdenlive
 [6]: https://kde.org/community/donations/?app=kdenlive
 [7]: https://www.mltframework.org/blog/v7.0.0_released/
 [8]: https://invent.kde.org/multimedia/kdenlive/-/issues/991
 [9]: https://invent.kde.org/multimedia/kdenlive/-/issues/1069
 [10]: https://invent.kde.org/multimedia/kdenlive/-/issues/973
 [11]: https://destinationlinux.org
 [12]: https://www.youtube.com/channel/UCtkSBZ0x71aeHmR3NNBTWwg
 [13]: https://libregraphicsmeeting.org/2021/en/program.html
 [14]: https://invent.kde.org/multimedia/kdenlive/-/issues/993
 [15]: https://krita-artists.org/c/artwork/5/l/top
 [16]: https://invent.kde.org/multimedia/kdenlive/-/issues/1005
 [17]: https://invent.kde.org/multimedia/kdenlive/-/issues/350
 [18]: https://invent.kde.org/multimedia/kdenlive/-/issues/1039
 [19]: https://invent.kde.org/multimedia/kdenlive/-/issues/1022
 [20]: https://invent.kde.org/multimedia/kdenlive/-/issues/1041
