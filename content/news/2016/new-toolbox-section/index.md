---
title: New Toolbox Section
author: The DiveO
date: 2016-09-22T13:26:16+00:00
aliases:
- /en/2016/09/new-toolbox-section/
categories:
  - Tutorial
---

![](toolbox-banner.png)

We've added a new [Toolbox][1] section that has posts dedicated to new and improved functionality in Kdenlive. Check back regularily for updates.

 [1]: https://kdenlive.org/toolbox/
