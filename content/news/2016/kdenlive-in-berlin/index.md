---
title: Kdenlive in Berlin
author: Jean-Baptiste Mardelle
date: 2016-08-29T18:19:52+00:00
aliases:
- /en/2016/08/kdenlive-in-berlin/
categories:
  - Akademy
  - Event
---
Kdenlive 16.08.0 was released a few weeks ago and we are now preparing the bugfix 16.08.1 release that will be tagged on the 5th of September.

In the recent weeks I have not been able to spend much time on Kdenlive. Hopefully, in a few days I will be in Berlin for [Akademy][1], which is always a great opportunity to move forwards. For everyone interested in discovering Kdenlive, discussing about your successes or problems with it, or if you want to get involved, feel free to join the [Kdenlive's BoF session][2] on Monday, 5th of September.

Jean-Baptiste Mardelle

![](Going-To-Akademy-2016.png)

 [1]: https://akademy.kde.org/2016
 [2]: https://community.kde.org/Akademy/2016/Monday
