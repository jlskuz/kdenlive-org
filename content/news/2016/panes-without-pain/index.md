---
title: 'Kdenlive: Panes Without Pain'
author: Farid Abdelnour
date: 2016-06-12T16:42:41+00:00
aliases:
- /en/2016/06/108/
categories:
  - Tutorial
---

Kdenlive really is a formidable video editor, undoubtedly rated **P** for _powerful_ (pun on **S** rating flimflam intended). It actually is quite simple to use, yet focuses on its main duty: powerful video editing.

Kdenlive isn't a video effects toolkit, so you will need to get your fifteen uninspired movie templates elsewhere. Sorry, no 3D transitions between agonizing vertical video clips either. Kdenlive is designed to be driven by _your creativity_, it's not a substitute for any lack of creativity.

So what to expect…?

## Kdenlive: Panes Without Pain

Let's simply start with the «obligatory» screenshot showcasing some real-life project I did some time ago:

![](kdenlive-1.0.jpeg "Kdenlive (using dark Breeze theme and light Breeze icons)")

A nice touch is that you can easily configure Kdenlive to use a dark theme (in particular, _Breeze dark_) with suitably themed icons… and without having to change your overall desktop theme.

Dark themes may have the advantage that they help focusing on your main task: editing with video. Personally, I switched to the dark theme a year ago or so — after noticing that it benefits my work flow.

You can actually move all those different panes around, and save up to four pane layouts for later retrieval. This way, you can switch between different pane layouts depending on in which stage of your work flow you currently are. This is yet another nice professional touch.

Also clearly visible is the multi track pane: Kdenlive is a full-blown multi-track editor. You can even do simple compositing with ease, such as titling or stacking multiple, _erm_, «layers». For complex compositing there are dedicated tools, such as [Blender][4] and [Natron][5], just to name two other prominent open source projects.

### Newcomers' Pitfalls

While Kdenlive tries to be gentle to newcomers, typical stumbling blocks are:

  * there are **no fancy 3D effects, nor 3D titles**, or similar eye-candy in Kdenlive on purpose: there are much better tools for this. Kdenlive instead excels at editing video. It also does simple (multi-track) compositing well.

  * **[video tracks aren't layers][1]**: however, to smooth entry, Kdenlive now defaults to [transparent tracks][2] to create an illusion of layers. Unfortunately, this breaks down as soon as you do, for instance, a dissolve on a title clip. However, using transition actually is quite easy, you just need to start using them.

  * there are **no _in-track_ transitions yet** that fade between two adjacent clips on the same track. instead, in Kdenlive you fade between clips on _different_ tracks. As soon as you master this peculiarity, you'll be able to unlock Kdenlive's power for combining compositing with transitions: in particular, Kdenlive allows [non-linear compositing][3] in form of a compositing tree. This is something simple layering can't do.

  * Kdenlive's **«transitions» are more or less a misnomer:** most of Kdenlive's transitions are in fact compositing operations.

### Kdenlive is Open Source

_Kdenlive_ is an open source project. For its user interface, it uses KDE Frameworks 5; but you don't necessarily need a KDE-centric OS distribution. Unfortunately, many distributions distribute broken Kdenlive packages. In particular, auxiliary, yet important, libraries are often outdated, such as MLT (the Multimedia Lovers' Toolkit). As MLT is at the heart of Kdenlive this immediately destroys the Kdenlive experience. For (*)ubuntu-based distributions, please use the [Project Kdenlive PPA Repositories][6].

Please note that there is no Windows version yet, albeit there will be a Google Summer of Code (SoC) project later this year.

 [1]: https://thediveo-e.blogspot.com/2014/07/kdenlive-timeline-illustrated-part-1.html
 [2]: https://thediveo-e.blogspot.com/2016/03/kdenlive-ui-transparent-tracks-odds-and.html
 [3]: https://thediveo-e.blogspot.com/2014/07/kdenlive-timeline-illustrated-part-2.html
 [4]: https://www.blender.org/
 [5]: https://natron.fr/
 [6]: https://thediveo-e.blogspot.com/2016/04/new-ppas-for-kdenlive-on-ubuntu.html
