---
title: Kdenlive Café, 20th of september
author: Jean-Baptiste Mardelle
date: 2016-09-19T23:00:43+00:00
aliases:
- /en/2016/09/kdenlive-cafe-20th-of-september/
categories:
  - Café
---
![](cafe-sept-16.jpg)

Today we will start again our monthly Kdenlive Café after a summer pause. All interested users are invited to join in for this informal meeting.

The Café will take place this tuesday, 20th of september 2016, at 9pm Central European Summer Time (CEST = UTC+2), on our usual #kdenlive channel on irc.freenode.net.

Among the topics, we will make a quick evaluation of the current 16.08.x release, discuss the roadmap for 16.12 and also share infos on the current state of packaging, how to get involved and much more. Feel free to join us!

Jean-Baptiste Mardelle
