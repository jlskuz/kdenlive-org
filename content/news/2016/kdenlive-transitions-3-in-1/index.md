---
title: 'Kdenlive Transitions: 3-in-1'
author: The DiveO
date: 2016-10-03T09:43:41+00:00
aliases:
- /en/2016/10/kdenlive-transitions-3-in-1/
categories:
  - Tutorial
---

![](composite-transition-over.png)

Scratching your head what Kdenlive transitions really are? They are 3-in-1. Still scratching? Then our new toolbox post about [Kdenlive Transitions][1] may come to your rescue…


 [1]: /project/kdenlive-transitions/
