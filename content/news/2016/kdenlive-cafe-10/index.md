---
title: 'Next Kdenlive Café #10'
author: Jean-Baptiste Mardelle
date: 2016-10-18T07:31:12+00:00
aliases:
- /en/2016/10/next-cafe-10/
categories:
  - Café
---
Our monthly Kdenlive Café will take place this wednesday, 19th of October at 9pm (European summer time – UTC+2). You can meet us on irc.freenode.org to discuss about Kdenlive, and don't hesitate to come by if you want to get involved in the project!
