---
title: Kdenlive presentation and news
author: Jean-Baptiste Mardelle
date: 2016-11-14T07:39:20+00:00
aliases:
- /en/2016/11/kdenlive-presentation-and-news/
categories:
  - Café
  - Event
---

![](kdenlive-november-16.jpg)

## Kdenlive live presentation

I will make a presentation on Kdenlive next saturday, **19th of november 2016 in Toulouse** at the "[Capitole du Libre][1]" event. The Kdenlive event will be at 16h30, and lots of other interesting conferences / workshops / boots will be presenting various aspects of open source so hope to meet some of you there! Full schedule is [here][2].

## Kdenlive café

Another way to meet the development team is our monthly cafés, next one is scheduled on the **23rd of november** at 9pm, CET time (UTC + 1). Meeting will be as usual on irc.freenode.org, channel #kdenlive

## Latest developments for 16.12

  * Projects can now use a custom location to store temporary data like thumbnails, timeline preview and proxy clips
  * Easier portability: If you save your project file in the root directory of your video clips, you should now be able to simply move that directory to another computer and continue editing without having to fix paths. And if the project temporary folder is also inside that directory, your proxy clips and all temporary data should also be available.

## Updated AppImage / Snap

We are also preparing for the upcoming Kdenlive 16.12 release. Interested users can test the latest developments using our [AppImage / snap packages][3] that were recently updated. The AppImage (version alpha2) now uses a separate kdenlive config file so that it will not mess with your standard installed version.

Jean-Baptiste Mardelle


 [1]: https://2016.capitoledulibre.org/
 [2]: https://2016.capitoledulibre.org/programme.html
 [3]: /download/
