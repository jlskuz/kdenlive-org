---
title: Donate to Randa 2016
author: Farid Abdelnour
date: 2016-06-14T18:15:20+00:00
aliases:
- /en/2016/06/donate-to-randa-2016/
categories:
  - Randa
  - Event
---

![](21109560869_983ea46190_k.jpg)

On Thursday evening, I will join the Randa Meeting, which gathers around 40 KDE developers. This will allow me to spend some dedicated time on Kdenlive and exchange with the development community. I hope to spend some time with Joseph Joshua who is working on Kdenlive's Windows port and it will also be a great occasion to look into Flatpak, a Linux cross distro packaging system.

I will keep you updated when I will be there!

If you want to support us, please [donate][1] to help us keeping up these developer meetings!

[![](/images/banners/fundraising2016.png)][1]

 [1]: https://web.archive.org/web/20160624115939/https://www.kde.org/fundraisers/randameetings2016/
