---
title: 'Choose the Café #11 and #12 dates'
author: Farid Abdelnour
date: 2016-11-01T21:39:19+00:00
aliases:
- /en/2016/11/choose-the-cafe-11-and-12-dates/
categories:
  - Café
---
Time to choose the Kdenlive Café's #11 and #12 dates:  Select the dates visiting this site:

<https://dudle.inf.tu-dresden.de/kedenlivecafe11and12/>

Note that voting ends on the 6th of November.
