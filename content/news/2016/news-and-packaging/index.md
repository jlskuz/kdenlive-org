---
title: Kdenlive news and packaging
author: Jean-Baptiste Mardelle
date: 2016-09-26T07:34:12+00:00
aliases:
- /en/2016/09/kdenlive-news-and-packaging/
categories:
  - Café Log
  - Café
---

![](rolling.jpg)

Following our last week's [monthly Café][1], we decided to concentrate on [advanced trimming features][2] and if possible an audio mixer for the next Kdenlive 16.12 release. It was also mentioned that several users requested the comeback of the rotoscoping effect, which was lost in the KF5 port, preventing some users to upgrade their Kdenlive version.

So good news, I worked on it and rotoscoping is now back in git master, will be in the 16.12 release.

On the stability side, we just fixed a packaging issue on [our PPA][3] that caused frequent crashes, so if you experienced issues with our PPA, please update to enjoy a stable Kdenlive.

Next Café will be on the 19th of October, at 9pm (Central European Time). See you there!

Jean-Baptiste Mardelle

 [1]: /2016/09/kdenlive-cafe-20th-of-september/
 [2]: /video-editing-applications-handbook/#trim
 [3]: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-stable
