---
title: Kdenlive’s first bug squashing day
author: Jean-Baptiste Mardelle
date: 2016-11-28T23:59:35+00:00
aliases:
- /en/2016/11/kdenlives-first-bug-squashing-day/
categories:
  - Event
---

Kdenlive 16.12 will be released very soon, and we are trying to fix as many issues as possible. This is why we are organizing a **Bug squashing day, this friday, 2nd of december 2016 between 9am and 5 pm** (Central European Time – CET).

![](bugsquashing.png)

## Kdenlive needs you

There are several ways you can help us improve this release, depending on your skills or interests. During the bug squashing day, Kdenlive developers will be reachable on IRC at freenode.net, channel #kdenlive to answer your questions. A [collaborative notepad][1] has also been created to coordinate the efforts.

### If you have some interest / knowledge in coding:

You can download Kdenlive's source code and find instructions [on our wiki][2]. We will also be available on friday on IRC to help you setup your development environment. You can then select an '_easy bug_' from the [notepad list][1] and then look at the code to try to fix it. Feel free to ask your questions on IRC, the developers will guide you through the process, so that you can get familiar with the parts of the code you will be looking at.

### If you are a user and encounter a bug:

You can help us by testing the Kdenlive 16.12 RC version. Our easy to install AppImage and snap packages will be updated on the 1rst of december with the latest code (Ubuntu users can also use our [PPA][3]). This will allow you to install the latest version without messing with your system. You can then check if a bug is still there is the latest version, or let us know if it is fixed.

So feel free to join us this friday, this is your chance to help the world of free software video editing !

For the Kdenlive team,  
Jean-Baptiste Mardelle

 [1]: https://notes.kde.org/public/kdenlive-bugsquashing01
 [2]: https://community.kde.org/Kdenlive/Development/KF5
 [3]: https://launchpad.net/~kdenlive/+archive/ubuntu/kdenlive-master
