---
title: 'Kdenive Café #9 and #10 open for selection'
author: Farid Abdelnour
date: 2016-09-02T02:18:08+00:00
aliases:
- /en/2016/09/kdenive-cafe-9-and-10-open-for-selection/
categories:
  - Café
---
The dates for the Kdelive cafés&#8217; #9 and #10 are open for selection: <https://dudle.inf.tu-dresden.de/kedenlivecafe9and10/>

On Sunday October 11 we'll know the results.

See you then!
