---
title: Working With Kdenlive’s Library
author: The DiveO
date: 2016-09-28T06:37:06+00:00
aliases:
- /en/2016/09/working-with-kdenlives-library/
categories:
  - Tutorial
---

![](library-add-clip-to-bin.png)

We've put up a new toolbox page: [how to use Kdenlive's library][1]. We also cover the details about expanding library clips. (Part of our ongoing [Toolbox][2] series.)

 [1]: /project/the-library-copy-paste-between-projects/
 [2]: /toolbox/
