---
title: 'Kdenlive Café #11 and #12 dates set'
author: Farid Abdelnour
date: 2016-11-14T13:33:33+00:00
aliases:
- /en/2016/11/kdenlive-cafe-11-and-12-dates-set/
categories:
  - Café
---
Mark in your agenda the dates for the next cafés:

**Café #11:** November the 23rd at 21:00 CET

**Café #12:** December the 14th at 21:00 CET

_The reference timezone is always the Central European Time zone CET, UTC+1._
