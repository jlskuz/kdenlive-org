---
title: A Week into Kdenlive’s Fundraiser Campaign
author: Jean-Baptiste Mardelle
date: 2022-09-28T16:35:57+00:00
aliases:
- /en/2022/09/a-week-into-kdenlives-fundraiser-campaign/
- /it/2022/09/una-settimana-di-raccolta-fondi/
- /de/2022/09/eine-woche-kdenlive-spendenaktion/
categories:
  - Fundraising
  - Akademy
---

![](kdenlive-2208a.png)

We launched our first [Kdenlive fundraising campaign][1] one week ago and we already collected almost **two-thirds of the goal**! We would like to **thank everybody** for their massive support. Moving forward, we will keep you posted on our progress and the development of the new exciting features/improvements we'll be adding to Kdenlive with the funds.

We have also other fun announcements that are somewhat related to our campaign:

  * We now have official [Twitter][2] and [Mastodon][3] accounts – follow us and don't miss any of Kdenlive's news and updates!
  * We will have a booth at the [All-American High School Film Festival][4] that will be held in Times Square in New York. We will be at our booth all day on the 22nd of October 2022 showcasing Kdenlive. Come visit us (and get stickers)!
  * Kdenlive will be at [KDE’s Akademy 2022][5] that will be held in Barcelona this weekend (1 – 2 October 2022). During the event we will be giving a quick presentation about our campaign.

Stay tuned, as we will be announcing other cool things soon!

  [1]: /fund/
  [2]: https://twitter.com/kdenlive
  [3]: https://mastodon.technology/web/@kdenlive
  [4]: https://www.hsfilmfest.com/
  [5]: https://akademy.kde.org/2022
