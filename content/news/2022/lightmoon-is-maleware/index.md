---
title: 'SCAM: Lightmoon IS NOT Kdenlive. Lightmoon is MALWARE.'
author: Farid Abdelnour
date: 2022-06-01T17:03:19+00:00
aliases:
- /en/2022/06/scam-lightmoon-is-not-kdenlive-lightmoon-is-malware/
- /fr/2022/06/attention-lightmoon-nest-pas-kdenlive-lightmoon-est-un-malware/
- /de/2022/06/betrug-lightmoon-ist-nicht-kdenlive-lightmoon-ist-malware/
featured_image: security-warning.png
---

We have been notified of a site that is using Kdenlive's name and likeness to distribute malware to users. We will not be linking to the site to avoid accidental downloads, but if a search lands you on a site offering "lightmoon", "a free video editor" that looks in the screenshots identical to Kdenlive, this is malware.

We are also receiving notice that the creators of the lightmoon malware are sending out phishing emails encouraging users to download their infected software. Please ignore and trash these messages.

Remember: The only legitimate sources for Kdenlive's software are your distro, well-established app stores (such as FlatHub), and Kdenlive's own download page located at: [**kdenlive.org**][1]

 [1]: /download/
