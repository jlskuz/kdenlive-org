---
title: Do you use Kdenlive? Share your project and behind the scenes footage and stills!
author: Paul Brown
date: 2022-08-25T15:35:41+00:00
aliases:
- /en/2022/08/do-you-use-kdenlive-share-your-project-and-behind-the-scenes-footage-and-stills/
- /it/2022/08/usi-kdenlive-condividi-il-tuo-progetto-i-filmati-e-le-foto-dei-tuoi-backstage/
featured_image: kal-visuals-_5WivDo377A-unsplash.jpg
categories:
  - Showcase
---

Kdenlive will be carrying out a fundraiser soon and we would like to explain its place in the moviemaking ecosystem through the experience of the filmmakers that use it. **That is you**.

We would like to hear about your projects. If you have parts of your work you can share with us, behind the scenes stills or footage on set, shots of your team working on post-production using Kdenlive, and so on, we would love to see them.

If you would like to contribute material we can use in our campaign, We have enabled an upload folder on KDE's servers. Upload your images and clips there and include a text file along with the media containing attributions so we can credit you. Include links to the finished project so we can give your work some exposure, and, VERY IMPORTANT, a [copyleft][1] (CC By or CC By-SA) or [Public Domain-like][2] license so we can legally share the material.

And please remember: you can only legally share material you have full rights to!

Thank you!

<a href="https://collaborate.kde.org/s/XbkDdjwqRccc6z3" class="learn-more button">Upload</a>

 [1]: https://creativecommons.org/choose/
 [2]: https://creativecommons.org/share-your-work/public-domain/cc0/
 [3]: https://collaborate.kde.org/s/XbkDdjwqRccc6z3
