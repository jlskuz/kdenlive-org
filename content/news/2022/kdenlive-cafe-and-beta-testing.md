---
title: Kdenlive Café and Beta testing
author: Farid Abdelnour
date: 2022-11-15T18:28:12+00:00
aliases:
- /en/2022/11/kdenlive-cafe-and-beta-testing/
categories:
  - Café
  - Event
---

Join us tonight at 8PM UTC for our first Kdenlive Café of the year. Besides the usual community feedback, we'll be sharing news about the fundraiser, 22.12 release and the roadmap for future versions.

Today we also [release the 22.12 BETA][1], please give it a spin and let us know if you encounter any issues.

Café link: https://meet.kde.org/b/far-wbr

  [1]: /news/releases/22.11.80
