---
title: Bug Triaging
hideMeta: true
aliases:
- /it/valutazione-dei-bug
- /valutazione-dei-bug
- /fr/trier-les-bugs-signales
- /trier-les-bugs-signales
- /seguimiento-de-problemas
menu:
  main:
    parent: contribute
    weight: 23
---

There are many ways of helping Kdenlive, ranging from coding to writing tutorials, helping users on forums, to spreading the word and helping with fundraising. But let’s take a look at one task that is really important: bug triaging.

The goal is to save developers from doing this, which helps them fix bugs more quickly and use there time to develop exciting new features for Kdenlive instead of doing bug triage. That’s where you come in! If you’re a reasonably experienced Kdenlive user and want to help out, here’s how to get ready and set up! Help with bug triaging is a real and lasting contribution to Kdenlive! If you benefit from the work others did on Kdenlive it is a great opportunity to give something back to the project!

Are you read? Then read the instructions and let’s start!

https://community.kde.org/Kdenlive/Workgroup/Triaging


