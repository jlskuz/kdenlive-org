---
date: 2003-01-07
title: Historic Screenshots
draft: false
#layout: release-notes
---

# Kdenlive 0.5

![Screenshot of Kdenlive 0.5 main window](0.5.png)
Kdenlive main window

![Screenshot of Kdenlive 0.5 export dialog](0.5-export.png)
Kdenlive export dialog

# 21/10/2003
![Screenshot of Kdenlive](0.2.3-1.png)
![Screenshot of Kdenlive](0.2.3-2.png)
Here, we can see the latest screenshots from Kdenlive 0.2.3, showing single and double monitor layouts. Also, the astute at you will notice that the files I am using are DV avi's, rather than .dv files :-)

# 15/03/2003
![Screenshot of Kdenlive](dev-2003-03-15-1.png)
![Screenshot of Kdenlive](dev-2003-03-15-2.png)
![Screenshot of Kdenlive](dev-2003-03-15-3.png)
Here, we can see a recent version of Kdenlive CVS translated into french by Gilles Caulier! You can also see the recent additions to the application. There are now four layout buttons (marked 1-4) in the toolbar, which can be used to set up different screen layouts to be recalled instantly. This allows you to, for example, set up your screen into different "modes" of operation, such as for applying effects (which is still TO BE DONE"), straight editing, compositing, playback, or any other way that you wish to set them up. Modes are not forced upon the user, however - you are quite welcome to have everything onscreen at the same time if you so desire!

Also, you can see the new render dialog - this will expand to include multiple file formats, with multiple options and codec selection.

# 19/01/2003
![Screenshot of Kdenlive](0.2.0-beta.png)
The first screenshot that you can see is from Kdenlive V0.2.0 beta, this is what you will see if you compile the release version and use it.

![Screenshot of Kdenlive](dev-2003-12-19.png)
Now the other, altogether more tasty picture, shows the current development version. Yes, at the moment, the only difference you see is the two monitors instead of one, and don't worry, in the next stable release, you will be able to stack the monitors on top of each other if you so desire. One of the monitors show the currently selected clip, the other monitor shows the entire timeline and in the course of time will allow for the editing of translation/rotation transitions, cropping effects, etc. (Don't expect that for V0.3.0 though!)

# 07/01/2003
![Screenshot of Kdenlive](dev-2003-01-07-1.png)
![Screenshot of Kdenlive](dev-2003-01-07-2.png)
![Screenshot of Kdenlive](dev-2003-01-07-3.png)

First screenshots of they year, showing off the new usage of Dock Widgets. This allows the user to customize the display however they wish. At the moment, these display is not saved, but expect that to be added soon.

Shown off is the "usual" view of kdenlive, as well as one with the monitor in the center of the screen, and one for those who prefer the "gimp" style of work, or the borland style of IDE :-) (also useful for people who want to use multiple monitors, I daren't say!

# 30/11/2002
![Screenshot of Kdenlive](dev-2002-11-30.png)
Showing off Piave embedded into the monitor. Piave has told Kdenlive the length of the DV clip shown, and so the length is displayed n the project window. Other visible additions include the marker and vertical position indicator in the timeline.

# 29/10/2002
![Screenshot of Kdenlive](dev-2002-10-29.png)
Showing off how the cutter will be integrated (the blue rectangle on a black background is an SDL process that is embedded, and is controlled remotely via a client/server interface.) Also shows progress on the timeline.

# 23/07/2002
![Screenshot of Kdenlive with ruler almost finished](dev-2002-07-23.png)
Ruler almost finished

# 12/07/2002
![Screenshot of Kdenlive in KDE 2.0](dev-2002-07-12.jpg)
Ruler Works Better.

# 11/05/2002
![Screenshot of Kdenlive in KDE 3.0](dev-2002-05-11.jpg)
Kdenlive in KDE 3.0!

# 01/05/2002
![Screenshot of Kdenlive in KDE 2.0](dev-2002-05-01.jpg)
Kdenlive in KDE 2.0...
