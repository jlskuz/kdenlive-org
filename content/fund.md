---
title: Fund
hideMeta: true
aliases:
- /fr/fund-fr
- /fund-fr
- /de/kdenlive-spenden
- /kdenlive-spenden
- /it/raccolta-fondi-per-kdenlive
- /raccolta-fondi-per-kdenlive
menu:
  main:
    name: ❤️ Donate
    weight: 7
---



Kdenlive is a free and open source professional video editor that offers ease of use, powerful features, and tons of effects and transitions. Join the growing community of users who rely on Kdenlive to make their vlogs, commercials, documentaries, news segments, and films.

To keep building, improving and maintaining Kdenlive we need your support: Donate now, or, even better, become a Kdenlive patron and help us make Kdenlive the video editor for everybody!

## Let’s do More!

First goal reached: With the 15.000 € you have already generously donated we can take on implementing nested timelines, improved effects workflow, stabilize the code and boost performance.

Now it is time to set some stretch goals.

Based on feedback you have given us, we have set the following stretch goals for our fundraiser: when we reach 20.000 €, we will be able to take on making keyframing more powerful by adding new easing types and curves. If we reach 25.000 €, we will be able to improve support for videos with alpha, as well as for media with a greater variety of resolutions.

Our aim is to make Kdenlive the most useful and feature-rich video editor, while at the same time keeping it free and open for everyone.

Help us and keep the donations, feedback and support coming!


## What will your Donation Achieve?

We will use your donation to support members of the Kdenlive team, helping them continue working on the project to add major features and implement technologies required by the industry, keep the software stable and reliable, and maintain the code base.

Developers like Jean Baptiste, Julius and other contributors like Camille, Farid, Massimo, Eugen will be able to spend more time on Kdenlive and work to make it the best software it can be.

Some of the things you can look forward to:

- Nested timelines
- Improved effects workflow
- Performance boosts
- Improved keyframing with easing types and curves
- Better support for video formats with alpha
- Support for media with a greater variety of resolutions


## Nested Timelines

The nested timeline feature will allow you to open several timeline tabs. Each tab contains a separate timeline, all of them share the same project bin and will be saved inside the same project file. You will then be able to insert (or “nest”) one timeline within another, and it will act as a single clip. However, changing a sequence in a nested timeline will automatically update it in the container timelines. You might want to integrate, for example, a complex “Final credits” compositing sequence with 10 tracks at the end of your project. You could do this in a timeline tab and then insert that timeline as a single clip into your main project, allowing for a much cleaner workflow.

## Efficient Effects Workflow

Currently, if you want to tweak some basic parameters like the brightness or contrast of a clip, you need to go through several steps: First, you have to search through the effects list to find the one you need. Then you have to add it to the clip itself, and, finally, you have to adjust the parameters.

This is quite cumbersome for what is simple, everyday adjustments. To make your workflow much more fluid, we will introduce a new effects panel that gives you direct access to these parameters, allowing you to quickly and easily adjust levels, crop, transform and, for audio clips, volume, and pan.


## Performance Boost

Your donation will help us overhaul the code and improve Kdenlive’s general responsiveness. An in depth (and time-consuming) refactorization will help make Kdenlive snappier and more pleasurable to use overall.

We will be able to improve video and audio playback on the timeline, both at normal and slow speeds. This will help you when reviewing and tweaking clips, cuts, effects and transitions, making these tasks much easier, less prone to errors and more fluid.

Supporting us will also contribute to the harmonization of the rendering pipeline, which will positively affect proxy clips and timeline previews, as well as the final rendering of your work, making the encoding, previews and renders of your project faster, stabler and more reliable.

## Help us Achieve so Much More

And this is just the beginning. Your donation will help us keep working on Kdenlive and push to make Kdenlive the most robust and powerful (but still free) video editor in the industry.

