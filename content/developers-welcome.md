---
title: Kdenlive is looking for new developers!
hideMeta: true
aliases:
- /it/sviluppo
- /sviluppo
- /desarrollo
menu:
  main:
    name: Development
    parent: contribute
    weight: 21
---

Interested in coding ? Want to join the free software video community? Kdenlive’s community is growing and you can be a part of it!

[View Source Code](https://invent.kde.org/multimedia/kdenlive)

{{< technologies >}}

Kdenlive is based on the KDE frameworks and Qt. Most of the code is written in C++, with some parts in Qml.

We hope to hear from you soon!

[Checkout open tasks](https://invent.kde.org/multimedia/kdenlive/-/issues)

[Get in touch!](https://community.kde.org/Kdenlive#Contact)

[Read build instructions](https://community.kde.org/Kdenlive/Development)
