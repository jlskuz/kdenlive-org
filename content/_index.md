---
title: Kdenlive - Free and Open Source Video Editor
layout: home

learnmore:
  - title: User Manual
    link: https://docs.kdenlive.org
  - title: Chat Room (Matrix)
    link: https://matrix.to/#/#kdenlive:kde.org

---
