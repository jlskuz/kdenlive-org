---
title: Report Bugs
hideMeta: true
aliases:
- /fr/signaler-des-bugs
- /signaler-des-bugs
- /it/segnalare-un-bug
- /segnalare-un-bug
- /senalacion-de-problemas
menu:
  main:
    parent: contribute
    weight: 22
---

On this page you will learn how you can write a proper bug report that contains everything the developers need to know to fix it.

If you have a crash at Kdenlive startup or when trying to play a video file, please follow this steps (and read the rest of the page too)

1. If you compiled Kdenlive / MLT yourself, make sure you followed all steps described in [our instructions](https://community.kde.org/Kdenlive/Development).
2. Check that you don’t have several versions of MLT installed
3. Try playing your video file with FFmpeg’s player. From a terminal: `ffplay myvideo.mpg`
4. Try playing your video file with MLT’s player. From a terminal: `melt myvideo.mpg`

Include the results of the 4 above steps in your bug report, and always tell us which Kdenlive and MLT version you have!


## Step 1: Upgrade to Kdenlive latest release

Please upgrade to the [latest released versions](/download/) of Kdenlive and MLT. We do not answer bug reports for old Kdenlive versions (unless they are still reproducible in latest version).

## Step 2: Query open issues

[Query open issues](https://bugs.kde.org/buglist.cgi?bug_status=__open__&list_id=1248416&order=Importance&product=kdenlive) on [KDE’s bugtracker](https://bugs.kde.org/buglist.cgi?bug_status=__open__&list_id=1248416&order=Importance&product=kdenlive). Reading the bug page:

- _REPORTED_ or _NEEDSINFO_ is a reported bug, which needs more feedback.
- _CONFIRMED_ means that the bug is reproducible.
- _ASSIGNED_ means that a developer is handling the bug.
- _RESOLVED_ means that the bug was fixed in development version.

## Step 3: Report a bug

Before reporting bugs, read the user manual and search the forums for answers. Do not report bugs on Kdenlive versions less than 20.04.0, which are deprecated.

**For the bug report to be useful, please try to provide the following information:**

- Your Kdenlive and MLT version (menu About > About Kdenlive)
- Your OS (Windows, Ubuntu,…) and packaging source (Flatpak, Appimage,…)
- Precise steps to reproduce the bug
- If the bug crashes Kdenlive, **provide a backtrace**.
- If your report is not a bug, but a **feature request select severity: wishlist**!

## How to get useful crash information (backtrace)

### Linux

Please install the following packages: gdb, kdenlive-dbg, libmlt-dbg (package names may slightly change depending on your distro)

When Kdenlive crashes, if the KDE crash handler dialog pops up, you can copy the data it provides. Otherwise, start Kdenlive from a terminal like this:

1. Type `gdb kdenlive`
2. After gdb read debug symbols, type `run`

### Flatpak

First of all make sure the Flatpak debug symbols are installed by typing `flatpak install org.kde.kdenlive.Debug` to a command line.

Now you can start the Flatpak from a command line like this:

1. Start a shell inside the Kdenlive Flatpak sandbox: `flatpak run --command=sh --devel org.kde.kdenlive`
2. Type `gdb /app/bin/kdenlive`
3. After gdb read debug symbols, type `run`

For more details on Flatpak debugging see here: https://docs.flatpak.org/en/latest/debugging.html


### Windows

1. Build Kdenlive with KDE Craft locally as described [here](https://invent.kde.org/multimedia/kdenlive/-/blob/master/dev-docs/build.md#build-craft).
2. Type to the command line `cd C:/CraftRoot/mingw64/bin`
3. Start gdb with `gdb`
4. Start Kdenlive -> get the PID number
5. `attach 3288` (replace 3288 by the PID number)
6. wait on the (gdb) prompt
7. type `c`


Once you followed the platform specific instructions above to start Kdenlive, you can trigger the bug. When Kdenlive crashes, go to your terminal window and type:

`thread apply all bt full`

Then press enter until you see the full data. Copy the log to a file and attach it to the bug report.




