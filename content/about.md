---
title: The Story of Kdenlive
hideMeta: true
aliases:
- /zh/about-zh/
- /fr/a-propos/
- /it/cosa/
- /de/ueber-uns/
menu:
  main:
    parent: about_menu
    weight: 10
team:
  - username: mardelle
    role: Maintainer / Development
  - username: jlskuz
    role: Development
  - username: frdbr
    role: Promotion
  - username: massimostella
    role: User Support / Documentation
  - username: emohr
    role: User Support / Documentation
  - username: bjordan
    role: User Support / Documentation
---

{{< youtube g2Q6sluqc2I >}}

## History

Kdenlive is a community project which aims to deliver a free and open source video editing software application to allow everybody to produce high quality content in order to increase the democratization of the media. The application has a graphical user interface written in C++ with [Qt][8] and uses KDE libraries for the [MLT framework][6] written by Dan Dennedy, which relies on [FFmpeg][7] to decode and encode almost all the video and audio formats that are available today, and which hosts effects libraries like Frei0r and Movit for video, and LADSPA and SOX for audio. 

According to the official documents that we can find on the internet, the project was launched by Jason Wood, who released version [0.2.3 in October 2003][9]. Soon, Kdenlive 0.2.4 followed, but the community did not yet exist and the group was very small. 

The project stopped for two years. Before the end of 2005, Jean-Baptiste Mardelle, who heard that the project would be re-activated, offered his help. And in 2006, he signed the [post of the new release 0.3][10] of the program. From this moment on, he became the main reference point for the project. 

Version 0.4 and 0.5 are soon distributed, but there is an issue. A refactoring of the code is needed to go forward. The program has to be moved from KDE 3, which is not compatible with MLT, to KDE 4. The rewriting ends in 2008, followed by several new releases, but in 2011, a further migration from KDE 4 to KDE 5 was needed to allow the program to grow.

In 2012, a crowdfunding campaign is launched to fund the operation, and before the end of 2014, the goal is reached. Then finally, in 2015, Kdenlive becomes an official KDE application. 
Jean-Baptiste was invited to Akademy, KDE's annual world summit, to present ten years of activity. 

The new perspective is to make the project even bigger. Before the end of the year, the [first Kdenlive Café][11] is announced with the aim of getting more people involved. During this virtual meeting, it immediately became clear that in order to grow, Kdenlive needed to be cross-platform. The Windows version was [announced in 2016][12], began with a sprint meeting followed by the new logo and the new site. 

But as soon as the development of new features started, it was evident that the code for the timeline had to be rewritten, because it was too old and no longer fit for purpose. Everybody knew that the refactoring could take several months or years, but the community did not lose enthusiasm. 

In 2018, a roadmap was written, and in 2019, the refactored code was distributed, although some fine tuning was still needed. Then finally, since 2020, continuing up to now, the long-awaited new features along with a wide variety of new effects and dozens of important improvements have been continuously added to create the most powerful, free, and open source video editing application ever.

## Team

Kdenlive is developed by a small, mostly volunteer team from all over the world. New contributors are always welcome!

Here is the currently active core team:

{{< kde-users "team" >}}

## Technologies

{{< technologies >}}

Through the MLT framework, Kdenlive integrates many plugin effects for video and sound processing or creation. Furthermore, Kdenlive brings a powerful title editor, a subtitling solution, and can then be used as a complete studio for video creation.

Video effects are provided by [Frei0r][4] while audio effects use [LADSPA][5].

## License

This program is Free Software as described by the [Free Software Foundation][1] under the terms of the [GNU General Public License][2].

There are no license fees to pay, no registrations or subscriptions, and no premium features that require payment to be unlocked.

Kdenlive is part of the KDE Community. The legal entity behind the community is [KDE e.V.][3], a German non-profit organization.


  [1]: http://www.fsf.org/
  [2]: http://www.gnu.org/licenses/gpl-3.0.txt
  [3]: https://ev.kde.org
  [4]: https://www.dyne.org/software/frei0r/
  [5]: https://en.wikipedia.org/wiki/LADSPA
  [6]: https://mltframework.org
  [7]: https://ffmpeg.org
  [8]: https://qt.io
  [9]: /news/releases/0.2.3/
  [10]: /news/releases/0.3/
  [11]: /news/historic/drupal/kdenlive-cafe-01-and-02/
  [12]: /news/historic/drupal/moving-towards-a-working-windows-build/
