# Kdenlive.org website

This is the git repository for [kdenlive.org](https://kdenlive.org), the main website of Kdenlive.

As a (Hu)Go module, it requires both Hugo and Go to work.

## Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

Since this repo can be pretty big, you might want to clone with this command
```
git clone --filter=blob:none git@invent.kde.org:websites/kdenlive-org.git
```

This will perform a partial clone, reducing the amount of data that needs to be downloaded without usually impacting your workflow. Remove the `--filter=blob:none` part if you want to do a full clone.

Once you have a clone of this repo, you can run this command to try out the site on your local system:

```
hugo server --buildFuture --buildDrafts
```

The `--buildFuture` argument is needed to build content with a date set in the future, and `--buildDrafts` for content with `draft: true`.

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).

## Licensing
We assume new contributions to the content are licensed under CC-BY-4.0 and to the websites code under LGPL-3.0-or-later unless specified otherwise.


## Content

### Historic

This repo contains content from earlier revisions of this website:

- content/news/historic/jason-woods-homepage: The very first posts about Kdenlive from [Jason Wood's Homepage](https://web.archive.org/web/20070623015834/http://www.uchian.pwp.blueyonder.co.uk/kdenlive.html).
- content/news/historic/first-kdenlive: Posts from the [first own webpage for Kdenlive](https://web.archive.org/web/20080516230911/http://kdenlive.org/news.php) on kdenlive.org
- content/news/historic/drupal: Posts from the [drupal version](https://web.archive.org/web/20090106122629/http://www.kdenlive.org/blog) on kdenlive.org

